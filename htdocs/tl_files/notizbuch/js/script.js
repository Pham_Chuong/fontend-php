function TreeViewer(object_data){
	this.type = "treeviewer";
	
	var init = function initTree(){
		var json_data = [
					{
						"ItemId":"step1",
						"Title":"Aussehen, Bindung &amp; Umschlag",
						"Items":[
						{
							"ItemId":"binding",
							"Title":"Bindung",
							"Items":[
									{
											"ItemId":"binding_spiral",
											"ItemName":"binding",
											"Title":"Spiralbindung + Farbdruck",
											"Items":[
												{
													"ItemId":"binding_spiral_layout1",
													"Selected":1,
													"ItemName":"binding_layout",
													"Title":"Layout 1",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/upload-bindung-hard_10.png"
												},
												{
													"ItemId":"binding_spiral_layout2",
													"ItemName":"binding_layout",
													"Title":"Layout 2",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/upload-bindung-hard_12.png"
												},
												{
													"ItemId":"binding_spiral_layout3",
													"ItemName":"binding_layout",
													"Title":"Layout 3",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/upload-bindung-hard_14.png"
												},
												{
													"ItemId":"binding_spiral_layout4",
													"ItemName":"binding_layout",
													"Title":"Layout 4",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/bild-hard_1.png"
												}
											]
									},
									{
											"ItemId":"binding_hardcover_farbdruck",
											"ItemName":"binding",
											"Title":"Hardcover + Farbdruck",
											"Items":[
												{
													"ItemId":"binding_hardcover_farbdruck_layout1",
													"ItemName":"binding_layout",
													"Title":"Layout 1",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/upload-bindung-hard_10.png"
												},
												{
													"ItemId":"binding_hardcover_farbdruck_layout2",
													"ItemName":"binding_layout",
													"Title":"Layout 2",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/upload-bindung-hard_12.png"
												},
												{
													"ItemId":"binding_hardcover_farbdruck_layout3",
													"ItemName":"binding_layout",
													"Title":"Layout 3",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/upload-bindung-hard_14.png"
												},
												{
													"ItemId":"binding_hardcover_farbdruck_layout4",
													"ItemName":"binding_layout",
													"Title":"Layout 4",
													"ItemType":"image",
                                                    "ItemSrc":"imgs/bild-hard_1.png"
												}
											]
									},
									{
											"ItemId":"binding_hardcover_pragung",
											"ItemName":"binding",
											"Title":"Hardcover + Pragung",
											"Items":[
											{
												"ItemId":"binding_hardcover_pragung_leinen",
												"ItemName":"binding_layout",
												"Title":"Leinen",
												"ItemType":"image",
                                                "ItemSrc":"imgs/btn-material_leinen.png"
											},
											{
												"ItemId":"binding_hardcover_pragung_wintan",
												"ItemName":"binding_layout",
												"Title":"Wintan",
												"ItemType":"image",
                                                "ItemSrc":"imgs/btn-material-nabuka.png"
											},
											{
												"ItemId":"binding_hardcover_pragung_satin",
												"ItemName":"binding_layout",
												"Title":"Satin",
												"ItemType":"image",
                                                "ItemSrc":"imgs/btn-material-seide.png"
											}
										]
									}
									]
						},
						{
							"ItemId":"size",
							"Title":"Größe",
							"Items":[
								{
									"ItemId":"size_s",
									"Selected":1,
									"ItemName":"size",
									"Title":"S"
								},
								{
									"ItemId":"size_m",
									"ItemName":"size",
									"Title":"M"
								},
								{
									"ItemId":"size_l",
									"ItemName":"size",
									"Title":"L"
								},
								{
									"ItemId":"size_xl",
									"ItemName":"size",
									"Title":"XL"
								}
							]
						},
						{
							"ItemId":"color",
							"Title":"Farbe",
							"Items":[
							    {
							        "ItemId":"color_1",
							        "ItemName":"color",
							        "Title":" ",
							        "ItemType":"image",
							        "ItemSrc":"imgs/btn_414.png"
							    },
							    {
							        "ItemId":"color_2",
							        "ItemName":"color",
							        "Title":" ",
							        "ItemType":"image",
							        "ItemSrc":"imgs/btn_415.png"
							    },
							    {
							        "ItemId":"color_3",
							        "ItemName":"color",
							        "Title":" ",
							        "ItemType":"image",
							        "ItemSrc":"imgs/btn_416.png"
							    },
							    {
							        "ItemId":"color_4",
							        "ItemName":"color",
							        "Title":" ",
							        "ItemType":"image",
							        "ItemSrc":"imgs/btn_417.png"
							    },
							    {
							        "ItemId":"color_5",
							        "ItemName":"color",
							        "Title":" ",
							        "ItemType":"image",
							        "ItemSrc":"imgs/btn_418.png"
							    }
							]
						},
						{
							"ItemId":"title",
							"Title":"Titel",
							"Items":[
							    {
                                    "ItemId":"title_1",
                                    "ItemName":"title_1",
                                    "Title":"Ihr Name oder Titel",
                                    "ItemType":"text"
							    },
							    {
                                    "ItemId":"title_2",
                                    "ItemName":"title_2",
                                    "Title":"Ihr Untertitel",
                                    "ItemType":"text"
							    },
							    {
                                    "ItemId":"title_3",
                                    "ItemName":"title_3",
                                    "Title":"Jahr",
                                    "ItemType":"text"
							    }
							]
						},
						{
							"ItemId":"optional",
							"Title":"Optional"
						}
						]
					},
					{
						"ItemId":"step2",
						"Title":"Eigene Namen &amp; Widmung",
						"Items":[
						    {
						        "ItemId":"widmung_text",
						        "Title":"Geben Sie Ihre persoenliche Widmung ein",
						        "Items":[
						            {
                                        "ItemId":"Ihre Widmung",
                                        "ItemName":"widmung_text",
                                        "Title":"Ihre Widmung",
                                        "ItemType":"textarea"
                                    }
						        ]
						    },
						    {
						        "ItemId":"widmung_name_title",
						        "Title":"Name &amp; Titel",
						        "Items":[
						            {
						                "ItemId":"widmung_name_title_text",
						                "ItemName":"widmung_name_title_text",
						                "Title":"Name & Titel",
						                "ItemType":"text"
						            }
						        ]
						    },
						    {
						        "ItemId":"widmung_option",
						        "Title":"Optional",
						        "Items":[{"ItemId":"demo","Title":"Demo"}]
						    }
						]
					},
					{
						"ItemId":"step3",
						"Title":"Papier &amp; Layout",
						"Items":[{"ItemId":"demo2","Title":"Demo"}]
					}
			];
		
		var tree = $("#tree").jsonTree(json_data);
		
		$("input[name=hdnSelected]").change(function(){
            var obj_json = $.parseJSON($(this).val());
            $(obj_json).each(function(){
                var result = object_data.setData(this.key, this.value);
                if(result.reload){
                    tree.reloadData(result.reload);
                }
            });
		});
	};
	
	this.update = function(){
		var json_data = object_data.getData();
		var update_element = $("#tree li[value='" + json_data['selected_element'] + "'] > span");
		$("#tree span").removeClass("selectedItem");
		/*$(update_element).parent().parent().children("li").children("span").removeClass("selectedItem");
		$(update_element).parent().parent().parent().children("span").removeClass("selectedItem");*/
		$(update_element).addClass("selectedItem");
	}
	
	init();
};

function DataKeeper(){
	var available_sizes = ["s","m","l","xl"];
	var available_bindings = [
		{"title":"Spiralbindung + Farbdruck", "value":"spiral"},
		{"title":"Hardcover + Farbdruck", "value":"hardcover_farbdruck"},
		{"title":"Hardcover + Pragung", "value":"hardcover_pragung"}
	];
	var available_lineaturs = [
		{"title":"Blanko", "value":"blanko"},
		{"title":"Punktiert", "value":"punktiert"},
		{"title":"Liniert", "value":"liniert"}
	];
	var available_papers = [
		
	];
	
	var listenners = [];
	var data = {
		"selected_element":"",
		"size": "size_s",
		"binding": "binding_spiral",
		"Buchtitel":"Kalender",
		"font":5,
		"state_id" : 11, 
		"country_id" : 3,
		"sections_note": 465,
		"sections_individual": 402,
		"sections_legend": 410,
		"sections_month": 406,
		"sections_day": 432,
		"Hauptperson_NN":"",
		"Hauptperson_VN":"",
		"calendar_event_categories":[6,10],
		"year_order":"2014",
		"note_order":"",
		"locale_id":1,
		"paper":"white",
		"startdate":"2014-05-01 00:00:00",
		"widmung_text":"",
		"widmung_color":"black",
		"widmung_fontsize":13,
		"cover_title":"",
		"cover_subtitle":""
	};
	
	this.addListenner = function(obj){
		listenners.push(obj);
	};
	
	this.getData = function(){
		return data;
	};
	
	this.setData = function(key, value){ 
	    if(!key || key == 'undefined') key = 'selected_elment';
		data[key] = value;
		data['selected_element'] = value;
		console.log("set " + key + " = " + value);
		//rebuild data
		//TODO: later
		
		//notify
		$(listenners).each(function(){
			this.update();
		});
		
		//udpate price
		this.calculatePrice();
		
		//return
		var response = [];
		if(key == "binding_layout"){
		    if(value == "binding_spiral_layout1" || value == "binding_hardcover_farbdruck_layout1" )
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_1",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_414.png"
                            },
                            {
                                "ItemId":"color_2",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_415.png"
                            },
                            {
                                "ItemId":"color_3",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_416.png"
                            },
                            {
                                "ItemId":"color_4",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_417.png"
                            },
                            {
                                "ItemId":"color_5",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_418.png"
                            }
                        ]
		            }
		        }
		    }
		    
		    if(value == "binding_spiral_layout2" || value == "binding_spiral_layout3" 
		        || value == "binding_hardcover_farbdruck_layout2" 
		        || value == "binding_hardcover_farbdruck_layout3"
		        || value == "binding_spiral_layout4"
		        || value == "binding_hardcover_farbdruck_layout4"){
		        response = {
                    "reload":{
                        "name":"color",
                        "items":[
                            {
                                "ItemId":"color_D3130B",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_D3130B.png"
                            },
                            {
                                "ItemId":"color_D30B63",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_D30B63.png"
                            },
                            {
                                "ItemId":"color_F8B302",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_F8B302.png"
                            },
                            {
                                "ItemId":"color_43C08E",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_43C08E.png"
                            },
                            {
                                "ItemId":"color_387C5B",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_387C5B.png"
                            },
                            {
                                "ItemId":"color_22F1B2",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_22F1B2.png"
                            },
                            {
                                "ItemId":"color_1589AD",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_1589AD.png"
                            },
                            {
                                "ItemId":"color_0202FC",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_0202FC.png"
                            },
                            {
                                "ItemId":"color_A747AF",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_A747AF.png"
                            },
                            {
                                "ItemId":"color_5E5E5F",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_5E5E5F.png"
                            }
                        ]
                    }
                };
		    }
		    
		    //binding_hardcover_pragung_leinen
		    if(value == "binding_hardcover_pragung_leinen")
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_leinen_55",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_55.png"
                            },
                            {
                                "ItemId":"color_leinen_03",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_03.png"
                            },
                            {
                                "ItemId":"color_leinen_29",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_29.png"
                            },
                            {
                                "ItemId":"color_leinen_33",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_33.png"
                            },
                            {
                                "ItemId":"color_leinen_12",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_12.png"
                            },
                            {
                                "ItemId":"color_leinen_51",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_51.png"
                            }
                        ]
		            }
		        }
		    }
		    
		    //binding_hardcover_pragung_wintan
		    if(value == "binding_hardcover_pragung_wintan")
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_wintan_14",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-wintan_14.png"
                            },
                            {
                                "ItemId":"color_wintan_20",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-wintan_20.png"
                            },
                            {
                                "ItemId":"color_wintan_99",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-wintan_99.png"
                            }
                        ]
		            }
		        }
		    }
		    
		    //binding_hardcover_pragung_satin
		    if(value == "binding_hardcover_pragung_satin")
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_satin_27",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_27.png"
                            },
                            {
                                "ItemId":"color_satin_28",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_28.png"
                            },
                            {
                                "ItemId":"color_satin_15",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_15.png"
                            },
                            {
                                "ItemId":"color_satin_11",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_11.png"
                            },
                            {
                                "ItemId":"color_satin_10",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_10.png"
                            },
                            {
                                "ItemId":"color_satin_22",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_22.png"
                            }
                        ]
		            }
		        }
		    }
		    
		}
		
		return response;
		
	};
	
	this.sendData = function(){ 
		
		console.log("send data AJAX", data)
	};
	
	this.calculatePrice = function(){
	    var binding_price = {
	        "binding_spiral": 0,
	        "binding_hardcover_farbdruck": 3,
	        "binding_hardcover_pragung": 3
	    };
	    
	    var size_price = {
	        "size_s": 0,
	        "size_m": 3,
	        "size_l": 4,
	        "size_xl": 5
	    };
	    
	    var price = 24.95 + binding_price[data["binding"]] + size_price[data["size"]];
		$("#price span").html(price);
	};
	
	
}
