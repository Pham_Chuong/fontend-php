/**
 * 
 */

(function($) {
	var Cover = function(element, object_data) {
		var elem = $(element);
		var obj = this;

		var color_option_binding_spiral_layout_1 = [ {
			"layout" : "binding_spiral_layout1",
			"ItemId" : "color_1",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout1_color_1.png"
		}, {
			"layout" : "binding_spiral_layout1",
			"ItemId" : "color_2",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout1_color_2.png"
		}, {
			"layout" : "binding_spiral_layout1",
			"ItemId" : "color_3",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout1_color_3.png"
		}, {
			"layout" : "binding_spiral_layout1",
			"ItemId" : "color_4",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout1_color_4.png"
		}, {
			"layout" : "binding_spiral_layout1",
			"ItemId" : "color_5",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout1_color_5.png"
		} ];

		// Layout 2
		var color_option_binding_spiral_layout_2 = [ {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_0202FC",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_0202FC.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_387C5B",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_387C5B.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_A747AF",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_A747AF.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_5E5E5F",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_5E5E5F.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_1589AD",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_1589AD.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_F8B302",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_F8B302.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_43C08E",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_43C08E.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_22F1B2",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_22F1B2.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_D30B63",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_D30B63.png"
		}, {
			"layout" : "binding_spiral_layout2",
			"ItemId" : "color_D3130B",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout2_color_D3130B.png"
		} ];

		// Layout 3
		var color_option_binding_spiral_layout_3 = [ {
			"layout" : "layout3",
			"ItemId" : "color_0202FC",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_0202FC.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_387C5B",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_387C5B.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_A747AF",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_A747AF.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_5E5E5F",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_5E5E5F.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_1589AD",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_1589AD.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_F8B302",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_F8B302.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_43C08E",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_43C08E.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_22F1B2",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_22F1B2.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_D30B63",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_D30B63.png"
		}, {
			"layout" : "binding_spiral_layout3",
			"ItemId" : "color_D3130B",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_spiral_layout3_color_D3130B.png"
		}
		];
		
		var binding_hardcover_farbdruck_layout1 = [ {
			"layout" : "binding_hardcover_farbdruck_layout1",
			"ItemId" : "color_1",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout1_color_1.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout1",
			"ItemId" : "color_2",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout1_color_2.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout1",
			"ItemId" : "color_3",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout1_color_3.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout1",
			"ItemId" : "color_4",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout1_color_4.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout1",
			"ItemId" : "color_5",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout1_color_5.png"
		} ];
		
		var binding_hardcover_farbdruck_layout2 = [ {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_0202FC",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_0202FC.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_387C5B",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_387C5B.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_A747AF",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_A747AF.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_5E5E5F",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_5E5E5F.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_1589AD",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_1589AD.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_F8B302",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_F8B302.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_43C08E",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_43C08E.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_22F1B2",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_22F1B2.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_D30B63",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_D30B63.png"
		}, {
			"layout" : "binding_hardcover_farbdruck_layout2",
			"ItemId" : "color_D3130B",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/binding_hardcover_farbdruck_layout2_color_D3130B.png"
		} ];


		var binding_hardcover_pragung_leinen = [ {
			"ItemId" : "color_leinen_55",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/cover-hong.png"
		}, {
			"ItemId" : "color_leinen_03",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_415.png",
			"CoverSrc" : "imgs/cover-do.png"
		}, {
			"ItemId" : "color_leinen_29",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_416.png",
			"CoverSrc" : "imgs/cover-cam.png"
		}, {
			"ItemId" : "color_leinen_33",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_417.png",
			"CoverSrc" : "imgs/cover-luc.png"
		}, {
			"ItemId" : "color_leinen_12",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_418.png",
			"CoverSrc" : "imgs/cover-lam.png"
		}, {
			"ItemId" : "color_leinen_51",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_419.png",
			"CoverSrc" : "imgs/cover-trang.png"
		} ];

		var binding_hardcover_pragung_satin = [ {
			"ItemId" : "color_satin_27",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/satin_color_1.png"
		}, {
			"ItemId" : "color_satin_28",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_415.png",
			"CoverSrc" : "imgs/satin_color_2.png"
		}, {
			"ItemId" : "color_satin_15",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_416.png",
			"CoverSrc" : "imgs/satin_color_3.png"
		}, {
			"ItemId" : "color_satin_11",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_417.png",
			"CoverSrc" : "imgs/satin_color_4.png"
		}, {
			"ItemId" : "color_satin_10",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_418.png",
			"CoverSrc" : "imgs/satin_color_5.png"
		}, {
			"ItemId" : "color_satin_22",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_419.png",
			"CoverSrc" : "imgs/satin_color_6.png"
		} ];

		var binding_hardcover_pragung_wintan = [ {
			"ItemId" : "color_wintan_14",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_414.png",
			"CoverSrc" : "imgs/wintan_color_1.png"
		}, {
			"ItemId" : "color_wintan_20",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_415.png",
			"CoverSrc" : "imgs/wintan_color_2.png"
		}, {
			"ItemId" : "color_wintan_99",
			"ItemName" : "color",
			"Title" : " ",
			"ItemType" : "image",
			"ItemSrc" : "imgs/farbe_416.png",
			"CoverSrc" : "imgs/wintan_color_3.png"
		} ];

		this.initCover = function() {
			var step = [ 'step-bindung', 'step-grobe', 'step-farbe',
					'step-titel', 'step-gummiband', 'step-zeichenband' ];
			var html = '';
			for ( var i = 0; i < step.length; i++) {
				html = html + '<div id="' + step[i] + '" class="circle">'
						+ (i + 1);
				html = html + '<div  class="tip" id="option_' + step[i]
						+ '"></div>';
				html = html + '</div>';
			}

			elem.html(html
					+ '<img id="cover-img" src="./imgs/cover-blue.png" />');

		};

		this.updateCover = function(bindung_id, color, option) {
			for ( var i = 0; i < option.length; i++) {
				if (!color && bindung_id == 'binding_spiral_layout1') {
					color = 'color_1';
				}
				if (!color
						&& (bindung_id == 'binding_spiral_layout2' || bindung_id == 'binding_spiral_layout3')) {
					color = 'color_D3130B';
				}

				if (option[i]['ItemId'] == color) {
					$('#cover-img').attr('src', option[i]['CoverSrc']);
				}
			}
		};

		// get option cover step 1
		this.getBindung = function() {

			var option = [ {
				"title" : "Spiralbindung + Farbdruck",
				"value" : "spiral"
			}, {
				"title" : "Hardcover + Farbdruck",
				"value" : "hardcover_farbdruck"
			}, {
				"title" : "Hardcover + Pragung",
				"value" : "hardcover_pragung"
			} ];
			var elem = $('#option_step-bindung');
			var html = '';
			for ( var i = 0; i < option.length; i++) {
				html = html + '<div class="option" id="' + option[i]['value']
						+ '">' + option[i]['title'] + '</div>';
			}
			elem.html(html);

			$.each($('#option_step-bindung .option'), function() {
				$(this).click(
						function() {
							object_data.setData('binding', 'binding_'
									+ $(this).attr('id'));
						});
			});

		};

		// get option cover step 2
		this.getGrobe = function() {

			var option = [ {
				"ItemId" : "size_s",
				"Selected" : 1,
				"ItemName" : "size",
				"Title" : "S"
			}, {
				"ItemId" : "size_m",
				"ItemName" : "size",
				"Title" : "M"
			}, {
				"ItemId" : "size_l",
				"ItemName" : "size",
				"Title" : "L"
			}, {
				"ItemId" : "size_xl",
				"ItemName" : "size",
				"Title" : "XL"
			} ];
			var elem = $('#option_step-grobe');
			var html = '';
			for ( var i = 0; i < option.length; i++) {
				html = html + '<div class="option"  id="' + option[i]['ItemId']
						+ '">' + option[i]['Title'] + '</div>';
			}
			elem.html(html);

			$.each($('#option_step-grobe .option'), function() {
				$(this).click(function() {
					object_data.setData('size', $(this).attr('id'));
				});
			});

		};

		// get option cover step 3
		this.getFarbe = function() {

			var option = binding_hardcover_pragung_leinen;
			var elem = $('#option_step-farbe');
			var html = '';
			for ( var i = 0; i < option.length; i++) {
				html = html + '<div class="option"  id="' + option[i]['ItemId']
						+ '"><img src="' + option[i]['ItemSrc']
						+ '" alt="" /></div>';
			}
			elem.html(html);

			$.each($('#option_step-farbe .option'), function() {
				$(this).click(function() {
					object_data.setData('color', $(this).attr('id'));
				});
			});

		};

		// get option cover step 4
		this.getTitel = function() {

			var option = [

			{
				"ItemId" : "title_3",
				"ItemName" : "title_3",
				"Title" : "Jahr",
				"ItemType" : "text"
			}, {
				"ItemId" : "title_1",
				"ItemName" : "title_1",
				"Title" : "Ihr Name oder Titel",
				"ItemType" : "text"
			}, {
				"ItemId" : "title_2",
				"ItemName" : "title_2",
				"Title" : "Ihr Untertitel",
				"ItemType" : "text"
			} ];
			var elem = $('#option_step-titel');
			var html = '';
			for ( var i = 0; i < option.length; i++) {
				html = html + '<div class="option"><input type="text" id="'
						+ option[i]['ItemId'] + '" name="'
						+ option[i]['ItemName'] + '" placeholder="'
						+ option[i]['Title'] + '"></div>';
			}
			elem.html(html);

			$.each($('#option_step-titel .option input'), function() {
				$(this).keyup(function() {
					object_data.setData($(this).attr('id'), $(this).val());
				});
			});

		};

		// get option cover step 5
		this.getGummiband = function() {
			var option = [ {
				"ItemId" : "gummiband_1",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/gummiband_1.png"
			}, {
				"ItemId" : "gummiband_2",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/gummiband_2.png"
			}, {
				"ItemId" : "gummiband_3",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/gummiband_3.png"
			}, {
				"ItemId" : "gummiband_4",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/gummiband_4.png"
			}, {
				"ItemId" : "gummiband_5",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/gummiband_5.png"
			}, {
				"ItemId" : "gummiband_6",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/gummiband_6.png"
			} ];
			var elem = $('#option_step-gummiband');
			var html = '';
			for ( var i = 0; i < option.length; i++) {
				html = html + '<div class="option"  id="' + option[i]['ItemId']
						+ '"><img src="' + option[i]['ItemSrc']
						+ '" alt="" /></div>';
			}
			elem.html(html);

			$.each($('#option_step-gummiband .option'), function() {
				$(this).click(function() {
					object_data.setData('gummiband', $(this).attr('id'));
				});
			});
		};

		// get option cover step 6
		this.getZeichenband = function() {
			var option = [ {
				"ItemId" : "zeichenband_1",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/zeichenband_1.png"
			}, {
				"ItemId" : "zeichenband_2",
				"ItemName" : "color",
				"Title" : " ",
				"ItemType" : "image",
				"ItemSrc" : "imgs/zeichenband_2.png"
			} ];
			var elem = $('#option_step-zeichenband');
			var html = '';
			for ( var i = 0; i < option.length; i++) {
				html = html + '<div class="option"  id="' + option[i]['ItemId']
						+ '"><img src="' + option[i]['ItemSrc']
						+ '" alt="" /></div>';
			}
			elem.html(html);

			$.each($('#option_step-zeichenband .option'), function() {
				$(this).click(function() {
					object_data.setData('zeichenband', $(this).attr('id'));
				});
			});
		};

		this.update = function() {

			var json_data = object_data.getData();

			$('div.circle').fadeTo("fast", 0.4);
			$('.tip').removeClass('active');
			if (json_data['selected_element']) {
				if (json_data['selected_element'].indexOf("binding") >= 0) {
					$('#step-bindung').trigger('mouseenter');
					$('#step-bindung .tip').addClass(' active');
				}

				if (json_data['selected_element'].indexOf("size") >= 0) {
					$('#step-grobe').trigger('mouseenter');
					$('#step-grobe .tip').addClass(' active');
				}

				if (json_data['selected_element'].indexOf("color") >= 0) {
					$('#step-farbe').trigger('mouseenter');
					$('#step-farbe .tip').addClass(' active');
				}

				if (json_data['selected_element'].indexOf("title") >= 0) {
					$('#step-titel').trigger('mouseenter');
					$('#step-titel .tip').addClass(' active');
				}
			}

			// update bindung
			switch (json_data['binding_layout']) {
			case 'binding_spiral_layout1':
				this.updateCover(json_data['binding_layout'],json_data['color'],color_option_binding_spiral_layout_1);
				break;
			case 'binding_spiral_layout2':
				this.updateCover(json_data['binding_layout'],json_data['color'],color_option_binding_spiral_layout_2);
				break;
			case 'binding_spiral_layout3':
				this.updateCover(json_data['binding_layout'],json_data['color'],color_option_binding_spiral_layout_3);
				break;
			case 'binding_hardcover_pragung_satin':
				this.updateCover(json_data['binding_layout'],json_data['color'], binding_hardcover_pragung_satin);
				break;
			case 'binding_hardcover_pragung_leinen':
				this.updateCover(json_data['binding_layout'],json_data['color'], binding_hardcover_pragung_leinen);
				break;
			case 'binding_hardcover_pragung_leinen':
				this.updateCover(json_data['binding_layout'],json_data['color'], binding_hardcover_pragung_leinen);
				break;
			case 'binding_hardcover_pragung_wintan':
				this.updateCover(json_data['binding_layout'],json_data['color'], binding_hardcover_pragung_wintan);
				break;
			case 'binding_hardcover_farbdruck_layout1':
				this.updateCover(json_data['binding_layout'],json_data['color'], binding_hardcover_farbdruck_layout1);
				break;
			case 'binding_hardcover_farbdruck_layout2':
				this.updateCover(json_data['binding_layout'],json_data['color'], binding_hardcover_farbdruck_layout2);
				break;
				
			default:
				this.updateCover(json_data['binding_layout'],json_data['color'], binding_hardcover_pragung_leinen);
				break;
			}

		}

		this.getData = function() {

		};

		this.setData = function() {

		};
	};

	var Widmung = function(element, object_data) {
		var elem = $(element);
		var obj = this;

		// Public method
		this.initWidmung = function(text) {
			elem.html('<p>' + text + '</p>');
		};

		this.getData = function() {
			//
		};
		this.setData = function() {
			//
		};
	};

	var Layout = function(element, object_data) {
		var elem = $(element);
		var obj = this;

		// Public method
		this.initLayout = function() {

		};

		this.getData = function() {

		};
		this.setData = function() {

		};
	};

	$.fn.notizbuch = function(object_data) {
		return this.each(function() {
			var element = $(this);

			// Return early if this element already has a plugin instance
			if (element.data('widmung'))
				return;
			var widmung = new Widmung(this, object_data);
			// Store plugin object in this element's data
			element.data('widmung', widmung);

			if (element.data('cover'))
				return;
			var cover = new Cover(this, object_data);
			// Store plugin object in this element's data
			element.data('cover', cover);

			if (element.data('layout'))
				return;
			var layout = new Layout(this, object_data);
			// Store plugin object in this element's data
			element.data('layout', layout);
		});
	};
})(jQuery);
