<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Notizbuch</title>
	<meta name="description" content="" />
	<meta name="keywords" content="jquery plugin, image, effects, book, page flip, slider" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<script type="text/javascript" src="./js/jquery.js"></script>
	<script type="text/javascript" src="./js/notizbuch.js"></script>
	<script type="text/javascript" src="../wow_book/wow_book.min.js"></script>
	<link rel="stylesheet" href="../wow_book/wow_book.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="./css/wedding_album.css"></head>
	<link href="css/style.css" rel="stylesheet" />
    <script src="js/calendar.datakeeper.js"></script>
    <script src="js/calendar.tree.js"></script>
    <script type="text/javascript" src="js/jquery.colorbox.js"></script>
    <script type="text/javascript" src="js/jquery.uploadfile.min.js"></script>
    <script>
    $(document).ready(function(){
        var data_keeper = new DataKeeper();
        var left_tree = new CalendarTree(data_keeper);
        data_keeper.addListenner(left_tree);
        
		//Init Widmung
		$('#widmung').notizbuch(data_keeper);
		var widmung = $('#widmung').data('widmung');
		widmung.initWidmung('Ihre Widmung');

		//Init Cover
		$('#cover-step').notizbuch(data_keeper);
		var cover = $('#cover-step').data('cover');
		cover.initCover();
		cover.getBindung();
		cover.getGrobe();
		cover.getFarbe();
		cover.getTitel();
		cover.getGummiband();
		cover.getZeichenband();
		
		

		data_keeper.addListenner(cover);

		

		//Hover step Cover
		$( "div.circle" ).hover(function() {
		  $( this ).fadeTo( "fast", 1);
		});
		$( "div.circle" ).mouseleave(function() {
			  $( this ).fadeTo( "fast", 0.4);
			  $(this).children('.tip').removeClass('active');
		});
    
		$(".tree li[value=binding_spiral_layout4] span, .tree li[value=binding_hardcover_farbdruck_layout4] span").click(function(){
            $.colorbox({width:"480px", inline:true, href:"#upload"});
		});
		
		$("#uploadfile").uploadFile({
            url:"upload.php",
            allowedTypes:"png,gif,jpg,jpeg",
            fileName:"myfile",
            showStatusAfterSuccess:false,
            onSubmit:function(files)
            {
                $.colorbox({width:"480px",inline:true, href:"#upload"});
            },
            onSuccess:function(files,data,xhr)
            {
                data = $.parseJSON(data);
                var img = $("<img />").attr("src", "uploads/" + data[0]).attr("width", 440);
                $("#imagepreview").empty().append(img).show();
                $.colorbox({width:"480px",inline:true, href:"#upload"});
            },
            onError: function(files,status,errMsg)
            {
                $.colorbox({width:"480px",inline:true, href:"#upload"});
            }
        });
    })
    </script>
<body >
	<div id="leftmenu">
  	  <div id="tree"></div>
      <div id="price"><span>00,00</span></div>
	</div>
	<div id="container">
		<div id="book" class='album'>
			<div class='cover'>
				<div id="cover-step"></div>
			</div>
			
			<div class='cover' ></div>
			<div id='widmung'>
			</div>
			
			<div id='page1'>
			<img src='./imgs/page-left.png' />
			</div>
			
			<div>
				<img src='./imgs/page-right.png' />
			</div>


		</div>
	</div>
    <div style='display:none'>
    <div id="upload">
    <form id="SignUpForm" action="" method="post">
    <h1>Titelbild zuschneiden</h1>
    <div>
    <div id="uploadfile">Bild auswählen</div>
    <div id="imagepreview" style="border:1px solid #0BA1B5;width:440px;display:none;margin-top:10px"></div>
    </div>
    </form>
    </div>
    </div>
	<script>
		$(document).ready(function() {
			//leftmenu
			$('ul.tree').hide();
				$('.tree-toggle').click(function () {
				$(this).parent().children('ul.tree').toggle(200);
				$(this).parent().toggleClass("open");
			});


			
			
			//book
			$('#book').wowBook({
				 width  : 800
				,height : 540
				,centeredWhenClosed : true
				,hardcovers : true
				,thumbnails : true
				,thumbnailsPosition : 'bottom'
			    ,numberedPages   : false // range of pages that will be numbered. 
			    ,controls : {
			         back : '#back'
			        ,next : '#next'
			        //,thumbs : "#thumb_button"
				}
			    ,sections : '#widmung, #page1'
				,gutterShadow : false
				,flipSoundPath : '../wow_book/sound/'
			});
			
			//var book = $.wowBook("#book");
			//book.showThumbnails();
			//book.hideThumbnails();
			//book.toggleThumbnails(); // alternate between show / hide the thumbnails
			/*
			$("#guestbook").submit(function(){
				var name=$("#name").val(), 
				    message=$("#message").val(),
				    messageList = $("#guestbook-list"),
				    firstMessage = $("#guestbook-list li:first-child"),
				    newmessage = firstMessage.clone().hide();
				$('.guestbook-name',newmessage).text(name+" :");
				$('.guestbook-message',newmessage).text(message);
				newmessage.insertBefore(firstMessage).show('slow');
				return false;
			});
			*/
		});
	</script>

</body>
</html>

