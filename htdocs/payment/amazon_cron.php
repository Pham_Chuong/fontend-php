<?php
include(dirname(dirname(__FILE__)) . "/system/initialize.php");
include(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/class.pn.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/class.pn.amazon.cron.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Interface.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/RequestType.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Client.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Exception.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Model.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Model/ContentType.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Model/IdList.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Model/SubmitFeedRequest.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Model/SubmitFeedRequest.php");
require_once(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/MarketplaceWebService/Model/SubmitFeedResponse.php");

$control = new PnAmazonCron();
$control->confirmShipment();

?>
