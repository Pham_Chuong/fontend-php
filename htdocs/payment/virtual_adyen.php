<!DOCTYPE html>

<html>
    <head>
        <title></title>
        <style>
        input { margin: 10px 0 0 }
        label { width: 150px; float: left; }
        </style>
    </head>
    
    <body>
        <form action="" method="post">
            <div style="width: 400px">
                <p>
                    <label>Event Code</label>
                    <select name="eventCode">
                        <option value="AUTHORISATION">AUTHORISATION</option>
                    </select>
                </p>
                <hr />
                <p>
                    <label>Event Date</label>
                    <input type="text" name="eventDate" value="2012-10-30T08:08:00.405+01:00" />
                </p>
                <hr />
                <p>
                    <label>Merchant AccountCode</label>
                    <input type="text" name="merchantAccountCode" value="PersonalNOVELDE" />
                </p>
                <hr />
                <p>
                    <label>Merchant Reference</label>
                    <input type="text" name="merchantReference" value="Order1847164" />
                </p>
                <hr />
                <p>
                    <label>Currency</label>
                    <input type="text" name="currency" value="EUR" />
                </p>
                <hr />
                <p>
                    <label>Value</label>
                    <input type="text" name="value" value="0" />
                </p>
                <hr />
                <p>
                    <label>Payment Method</label>
                    <select name="paymentMethod">
                        <option value="visa">Visa</option>
                        <option value="mc">Master Card</option>
                        <option value="paypal">Paypal</option>
                        <option value="bank">Bank Tranfer</option>
                    </select>
                </p>
                <hr />
                <p>
                    <label>Success?</label>
                    <select name="success">
                        <option value="true">true</option>
                        <option value="false">false</option>
                    </select>
                </p>
                <hr />
                <input type="submit" name="submit" value="send Notification" />
            </div>
        </form>
    </body>
</html>

<?php

class NotificationRequest {
    public $notificationItems; 
    public $live; 
}

class NotificationRequestItem {
    public $amount;
    public $eventCode;
    public $eventDate;
    public $merchantAccountCode;
    public $merchantReference;
    public $originalReference;
    public $pspReference;
    public $reason;
    public $success;
    public $paymentMethod;
    public $operations;
    public $additionalData;
}

class Amount {
    public $currency;
    public $value;
}

$amount = new Amount();
$amount->currency = "EUR";
$amount->value = "2222";

$notificationRequestItem = new NotificationRequestItem();
$notificationRequestItem->amount              = $amount;
$notificationRequestItem->eventCode           = "AUTHORISATION";
$notificationRequestItem->eventDate           = "2012-12-08T17:50:01.185+01:00";
$notificationRequestItem->merchantAccountCode = "PersonalNOVELDE";
$notificationRequestItem->merchantReference   = "Order1847164";
//$notificationRequestItem->originalReference   = 
$notificationRequestItem->pspReference        = 8613549853459369;
//$notificationRequestItem->reason
$notificationRequestItem->success             = true;
$notificationRequestItem->paymentMethod       = "Paypal";
$notificationRequestItem->operations          = array(
                                                        0 => 'CANCEL',
                                                        1 => 'CAPTURE',
                                                        2 => 'REFUND'
                                                    );
//$notificationRequestItem->additionalData      = 

$notificationRequest = new NotificationRequest();
$notificationRequest->notificationItems = $notificationRequestItem;
$notificationRequest->live = false;

$login = "ws@Company.PersonalNOVEL";
$password = "test123456";

//$login = "tt-tech";
//$password = "tt-tech";

$request = array(
    'notification' => array(
        'notificationItems' => array(
                'amount' => array(
                        'currency' => 'EUR',
                        'value' => 199
                    )
            ),
         'eventCode' => 'AUTHORISATION',
         'eventDate' => '2012-10-30T08:08:00.405+01:00',
         'merchantAccountCode' => 'PersonalNOVELDE',
         'merchantReference' => 'ORDER1351580810274',
         'originalReference' => '',
         'pspReference' => '8813515808628443',
         'reason' => 'Refused',
         'success' => false,
         'paymentMethod' => 'visa',
         'operations' => '',
         'additionalData' => ''
         ),
     'live' => false
    );

$client = new SoapClient("http://personalnovel.local/payment/notification_server.php?wsdl", 
                            array(
                                "login" => $login,
                                "password" => $password,
                                'style' => SOAP_DOCUMENT,
                                'trace' => 1,
                                'encoding' => SOAP_LITERAL,
                                'cache_wsdl' => WSDL_CACHE_NONE
                                )
                            );

try
{
    $client->sendNotification($request);
}
catch(SoapFault $f)
{
    echo "<pre>";
    print_r($f);
    echo "</pre>";
}

echo "<pre>";
print_r($client);
echo "</pre>";

?>
