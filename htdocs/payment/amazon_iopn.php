<?php
include(dirname(dirname(__FILE__)) . "/system/initialize.php");
include(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/class.pn.php");
include(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/class.pn.amazon.iopn.php");

//Write log
$logdir = "/var/www/Live_Web_Personalnovel/logs/";
$fp = fopen($logdir . 'amazon_' . date('Y-m') . '.log', 'a');

fprintf($fp, "%s\n", date('Y/m/d H:i:s a', time()));
foreach($_POST as $k => $v)
{
    fprintf($fp, "%s => %s\n", $k, $v);
}

if (!function_exists('http_response_code')) {
    function http_response_code($code = NULL) {

        if ($code !== NULL) {

            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                break;
            }

            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

            header($protocol . ' ' . $code . ' ' . $text);

        } else {

            $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
        }

        return $code;

    }
}

if($_POST["AWSAccessKeyId"] == "AKIAJQCET33HDSTROFGA")
{
    if($_POST["NotificationType"])
    {
        $notification_data = $_POST["NotificationData"];
        $uuid = $_POST["UUID"];
        
        $event = new PnOrderNotification();
        $parser = simplexml_load_string($notification_data);
        
        if($_POST["NotificationType"] == "NewOrderNotification")
        {
            if(!$event->existUUID("amazon-newordernotification", $uuid))
            {
                //Update order detail
                fprintf($fp, "New Order Notification - UUID: %s\n", $uuid);
                $amazon_order_id = (string)$parser->ProcessedOrder->AmazonOrderID;
                $amazon_buyer_email = (string)$parser->ProcessedOrder->BuyerInfo->BuyerEmailAddress;
                
                $amazon_shipping_company = "";
                $amazon_shipping_address = (string)$parser->ProcessedOrder->ShippingAddress->AddressFieldOne;
                
                $address_field_one = (string)$parser->ProcessedOrder->ShippingAddress->AddressFieldOne;
                $address_field_two = (string)$parser->ProcessedOrder->ShippingAddress->AddressFieldTwo;
                
                if($address_field_one != "" && $address_field_two != "")
                {
                    $amazon_shipping_company = $address_field_one;
                    //Mac dinh lay address 2
                    $amazon_shipping_address = $address_field_two;
                    
                    $pattern1 = '/^\d+$/';//number only
                    $pattern2 = '/[A-Za-z]+/'; //contain word
                    
                    // Address 1 chi co so (packstation)
                    // hoac Address 2 ko co chu
                    // => address 1 + address 2
                    if(preg_match($pattern1, $address_field_one) || !preg_match($pattern2, $address_field_two))
                    {
                        $amazon_shipping_company = "";
                        $amazon_shipping_address = $address_field_one . " " . $address_field_two;
                    }
                }
                else 
                {
                    if($amazon_shipping_address == "")
                    {
                        $amazon_shipping_address = (string)$parser->ProcessedOrder->ShippingAddress->AddressFieldTwo;
                    }
                }
                
                $order = new PnOrder();
                $order->initWithAmazonOrderId($amazon_order_id);
                $order->edit(array("order_status" => 1));
                fprintf($fp, "Order %s changed status\n", $order->id);
                
                $customer = new PnCustomer();
                if($customer->existEmail($amazon_buyer_email))
                {
                    $order->edit(array("customer_id" => $customer->id));
                    fprintf($fp, "Old Customer: %s\n", $amazon_buyer_email);
                }
                else
                {
                    $customer->initWithId($order->customer_id);
                    $customer->edit(array("customer_email" => $amazon_buyer_email));
                    fprintf($fp, "New Customer: %s\n", $amazon_buyer_email);
                }
                
                $address = new PnAddress();
                $address->initWithId($order->shipping_address_id);
                $address->edit(array("address_address" => $amazon_shipping_address, "address_firma" => $amazon_shipping_company));
                fprintf($fp, "Updated shipping address: %s\n", $amazon_buyer_email);
                
                fprintf($fp, "Order %s updated success\n", $order->id);
                fprintf($fp, "----------------------------------------------------------------------\n");
                
                $event->writeLog("amazon-newordernotification", $uuid, $order->id);
            }
            else
            {
                fprintf($fp, "New Order Notification - UUID: %s has already handled\n", $uuid);
            }
        }
        elseif($_POST["NotificationType"] == "OrderReadyToShipNotification")
        {
            if(!$event->existUUID("amazon-orderreadytoshipnotification", $uuid))
            {
                //Set order paid, Ok2go
                fprintf($fp, "Order Ready to Ship Notifiction - UUID: %s\n", $uuid);
                $amazon_order_id = (string)$parser->ProcessedOrder->AmazonOrderID;
                $order = new PnOrder();
                $order->initWithAmazonOrderId($amazon_order_id);
                
                $order_price = number_format($order->getTotalPrice(), 2);
                $order->pay($order_price);
                fprintf($fp, "Order %s paid success\n", $order->id);
                
                $order->edit(
                    array(
                        "order_paid"  => date('Y/m/d H:i:s a', time()),
                        "order_ok2go" => date('Y/m/d H:i:s a', time()),
                        "order_blob"  => $order->blob . date('Y-m-d', time()) . ": ok2go gesetzt: Automatisch\n"
                        )
                );
                fprintf($fp, "Order %s set Ok2go success\n", $order->id);
                
                fprintf($fp, "Order %s updated success\n", $order->id);
                fprintf($fp, "----------------------------------------------------------------------\n");
                
                $event->writeLog("amazon-orderreadytoshipnotification", $uuid, $order->id);
            }
            else
            {
                fprintf($fp, "Order Ready to Ship Notifiction - UUID: %s has already handled\n", $uuid);
            }
        }
        elseif($_POST["NotificationType"] == "OrderCancelledNotification")
        {
            if(!$event->existUUID("amazon-ordercancellednotification", $uuid))
            {
                //Set order stornier, send mail admin
                fprintf($fp, "Order Cancelled Notification - UID: %s\n", $uuid);
                $amazon_order_id = (string)$parser->ProcessedOrder->AmazonOrderID;
                $order = new PnOrder();
                $order->initWithAmazonOrderId($amazon_order_id);
                $order->edit(
                    array(
                        "order_status" => -10,
                        "order_blob"  => $order->blob . date('Y-m-d', time()) . ": Amazon: Order's cancelled\n"
                        )
                    );
                fprintf($fp, "Order %s cancelled\n", $order->id);
                fprintf($fp, "----------------------------------------------------------------------\n");
                
                $event->writeLog("amazon-ordercancellednotification", $uuid, $order->id);
                //TODO: send mail admin
            }
            else
            {
                fprintf($fp, "Order Cancelled Notification - UID: %s has already handled\n", $uuid);
            }
        }
        else
        {
            fprintf($fp, "----------------------------------------------------------------------\n");
            return http_response_code(500);
        }
    }
    else
    {
        fprintf($fp, "----------------------------------------------------------------------\n");
        return http_response_code(500);
    }
}
else
{
    fprintf($fp, "----------------------------------------------------------------------\n");
    return http_response_code(403);
}

