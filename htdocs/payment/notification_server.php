<?php 
 
include(dirname(dirname(__FILE__)) . "/system/initialize.php");
include(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/class.pn.php");
include(dirname(dirname(__FILE__)) . "/system/modules/tt_manage_books/class/class.pn.soaphandler.php");
 
$classmap = array('NotificationRequest' => 'NotificationRequest', 
    'NotificationRequestItem' => 'NotificationRequestItem',
    'Amount' => 'Amount');

$server = new SoapServer("Notification.wsdl"); 
session_start();
$server->setClass("PnSoapHandler");
//$server->setPersistence(SOAP_PERSISTENCE_SESSION);
//$server->addFunction("sendNotification"); 
$server->handle();

?> 
