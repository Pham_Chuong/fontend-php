<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

$GLOBALS['TL_DCA']['tl_module']['palettes']['novel_simply_book_template'] 		= '{title_legend},name,type,pn_template_tools';
/*
 * new fields
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['pn_template_tools'] = array(
    'label'		      => array("Template",""),
    'exclude'                 => true,
    'inputType'               => 'select',
    'options_callback'        => array('tl_module_book_tools', 'getToolsTemplates'),
    'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50')
);

class tl_module_book_tools extends Backend
{
	public $name_genre = '';
	public function __construct()
	{
		
		parent::__construct();
	}
	
	public function getToolsTemplates(DataContainer $dc)
	{
		$intPid = $dc->activeRecord->pid;
	
		if ($this->Input->get('act') == 'overrideAll')
		{
			$intPid = $this->Input->get('id');
		}
	
		return $this->getTemplateGroup('mod_novelsimply', $intPid);
	}
	
}

?>
