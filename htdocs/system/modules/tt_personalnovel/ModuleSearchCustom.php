<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2012 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Leo Feyer 2005-2012
 * @author     Leo Feyer <http://www.contao.org>
 * @package    News
 * @license    LGPL
 * @filesource
 */


/**
 * Class ModuleSearchCustom
 *
 * @package    Controller
 */

class ModuleSearchCustom extends Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_search_custom';
    /**
     * @var int
     */
    protected $perPage = 10;

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### Search Custom ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module
     */
    protected function compile()
    {
        global $objPage;
		$this->strOrderUrl = preg_replace('/\?.*$/', '', $this->Environment->request);

		// Prepare the URL
		foreach (preg_split('/&(amp;)?/', $_SERVER['QUERY_STRING'], -1, PREG_SPLIT_NO_EMPTY) as $fragment)
		{
			if (strncasecmp($fragment, 'odr', 3) !== 0)
			{
				$this->strOrderUrl .= (!$blnQuery ? '?' : '&amp;') . $fragment;
				$blnQuery = true;
			}
		}

		$this->strVarConnector = $blnQuery ? '&amp;' : '?';
		
		$this->Template->relUrl = ampersand($this->strOrderUrl) . $this->strVarConnector . 'odr=rel';
		$this->Template->datUrl = ampersand($this->strOrderUrl) . $this->strVarConnector . 'odr=dat';
		
		$objPage->title = 'Suche: '.trim($this->Input->get('keywords'));

        // Remove insert tags
        $strKeywords = trim($this->Input->get('keywords'));
        $strKeywords = str_replace(array('´', '`', "'"), '"', $strKeywords);
        
        $strKeywords = preg_replace('/\{\{[^\}]*\}\}/', '', $strKeywords); 
        
        
		/*if (function_exists('mb_eregi_replace'))
		{
			$strKeywords = mb_eregi_replace('[^[:alnum:] \*\+\'"\.:,_-]|\. |\.$|: |:$|, |,$', ' ', $strKeywords);
		}
		else
		{
			$strKeywords = preg_replace(array('/\. /', '/\.$/', '/: /', '/:$/', '/, /', '/,$/', '/[^\pN\pL \*\+\'"\.:,_-]/iu'), ' ', $strKeywords);
		}*/
		
		
        if (substr($strKeywords, 0, 1) == '"')
        {
            $strKeywords = trim(substr($strKeywords, 1, -1));
        }

        $strKeywords = str_replace('gelungener', 'gelungen', $strKeywords);
        $arrKeywords = explode(' ', $strKeywords);

        $strOrderBy = trim($this->Input->get('odr'));
       
        $strComOrderBy = $strOrderBy=='dat' ? "ORDER BY tstamp DESC " : "ORDER BY postal_code ASC";
        $strArticleOrderBy = $strOrderBy=='dat' ? "ORDER BY release_tstamp DESC " : "ORDER BY headline ASC";
        
        

        $isUpper = ctype_upper(preg_replace("/[0-9]/", "", $strKeywords));
        
       
        

        $strTextQuery = "LOWER(%s) LIKE \"%%%%".implode('%%%%', $arrKeywords)."%%%%\"";
        $strTextQuery = $isUpper?" %s LIKE BINARY \"%%%%".$strKeywords." %%%%\"":$strTextQuery;

        $strExactlyQuery = " %s LIKE BINARY \"%%%% ".$strKeywords." %%%%\" "
        ." OR "." %s LIKE BINARY \"%%%%".implode('%%%%', $arrKeywords)." %%%%\" "
        ." OR "." %s LIKE BINARY \"%%%% ".implode('%%%%', $arrKeywords)."%%%%\" ";

        $strTagsQuery = "%s REGEXP '.*\"name\";s:[0-9]+:\"".$strKeywords."\".*'";

/*
        $objTotal = $this->Database->prepare("SELECT c.id ".
                                                "FROM tl_companies c ".
                                                    " LEFT OUTER JOIN tl_company_activities ca ON ca.pid=c.id ".
                                                    " LEFT OUTER JOIN tl_company_contacts cc ON cc.pid=c.id ".
                                                    " LEFT OUTER JOIN tl_activity_descriptions ad ON ad.company_user_id=c.id ".
                                                "WHERE c.status='A' AND ".
                                                        sprintf($strTextQuery, 'c.name') . " OR " . 
                                                        sprintf($strTextQuery, 'c.street') . " OR " . 
                                                        sprintf($strTextQuery, 'c.postal_code') . " OR " . 
                                                        sprintf($strTextQuery, 'c.city') . " OR " . 
                                                        sprintf($strTextQuery, 'c.desc_txt') . " OR " . 
                                                        sprintf($strTextQuery, 'cc.function') . " OR ".
                                                        sprintf($strTextQuery, 'cc.first_name') . " OR ".
                                                        sprintf($strTextQuery, 'cc.last_name') . " OR ".
                                                        sprintf($strTextQuery, 'ca.activity_descriptions') . " OR ".
                                                        sprintf($strTextQuery, 'ad.name') . " ".
                                                "GROUP BY c.id "
                                                )->execute();
        
        $total = $objTotal->numRows;
*/
        /*list($total, $strQuery) = $this->generateCompanyQuery($arrKeywords, array('c.name', 'c.street', 'c.postal_code', 'c.city', 'c.desc_txt', 'cc.function', 'cc.first_name', 'cc.last_name', 'ca.activity_descriptions', 'ad.name'));

        $arrPagination = $this->generatePagination($total, 'com_page');
        $this->Template->com_total = $total;
        $this->Template->com_pagination = $arrPagination['obj_pagination']?$arrPagination['obj_pagination']->generate("\n  "):'';

        $objCompanies = $this->Database->prepare("SELECT c.*, ".
                                                    "(SELECT tl_countries.name FROM tl_countries WHERE c.country_id=tl_countries.id) as country ".
                                                "FROM tl_companies c ".
                                                    " LEFT OUTER JOIN tl_company_activities ca ON ca.pid=c.id ".
                                                    " LEFT OUTER JOIN tl_company_contacts cc ON cc.pid=c.id ".
                                                    " LEFT OUTER JOIN tl_activity_descriptions ad ON ad.company_user_id=c.id ".
                                                "WHERE c.status='A' AND ".
                                                        $strQuery.*/
/*
                                                        sprintf($strTextQuery, 'c.name') . " OR " . 
                                                        sprintf($strTextQuery, 'c.street') . " OR " . 
                                                        sprintf($strTextQuery, 'c.postal_code') . " OR " . 
                                                        sprintf($strTextQuery, 'c.city') . " OR " . 
                                                        sprintf($strTextQuery, 'c.desc_txt') . " OR " . 
                                                        sprintf($strTextQuery, 'cc.function') . " OR ".
                                                        sprintf($strTextQuery, 'cc.first_name') . " OR ".
                                                        sprintf($strTextQuery, 'cc.last_name') . " OR ".
                                                        sprintf($strTextQuery, 'ca.activity_descriptions') . " OR ".
                                                        sprintf($strTextQuery, 'ad.name') . " ".
*/
                                                /*"GROUP BY c.id ".$strComOrderBy
                                                );*/
/*
        echo $objCompanies->query;
        echo '<br />';
*/
/*
        if ($arrPagination['limit'])
        {
            $objCompanies = $objCompanies->limit($arrPagination['limit'], $arrPagination['offset'])->execute();
        }
        else
        {
            $objCompanies = $objCompanies->execute();
        }

        $arrCompanyRes = array();
        while ($objCompanies->next())
        {
            $arrCompanyRes[] = array
            (
                'name' => $objCompanies->name,
                'postal_code' => $objCompanies->postal_code,
                'city' => $objCompanies->city,
                'link' => $this->generateUrl($objCompanies, 29)
            );
        }
        // Assign data to the template
        $this->Template->companies = $arrCompanyRes;
        */
        list($total, $strQuery) = $this->generateArticleQuery($arrKeywords, array('headline', 'subheadline', 'teaser', 'text'));

        $arrPagination = $this->generatePagination($total, 'article_page');
        $this->Template->article_total = $total;
        $this->Template->article_pagination = $arrPagination['obj_pagination']?$arrPagination['obj_pagination']->generate("\n  "):'';

        $objArticles = $this->Database->prepare("SELECT *, ".
    		                                          "(SELECT jumpTo FROM tl_news_archive WHERE tl_news_archive.id=tl_news.pid) AS parentJumpTo, ".
    		                                          "(SELECT alias FROM tl_news_archive WHERE tl_news_archive.id=tl_news.pid) AS parentAlias, ".
    		                                          "(SELECT title FROM tl_news_archive WHERE tl_news_archive.id=tl_news.pid) AS archive, ".
    		                                          " MATCH(`headline`, `text`) AGAINST ('".$strKeywords."' IN BOOLEAN MODE) AS relevance " .
                                                "FROM tl_news ".
                                                "WHERE ".
                                                    "(". $strQuery .
/*
                                                        ($isUpper?sprintf($strExactlyQuery, 'headline', 'headline', 'headline') . " OR ":" MATCH(`headline`) AGAINST ('".$strKeywords."' IN BOOLEAN MODE) OR ") .
                                                        sprintf($strTextQuery, 'subheadline') . " OR " . 
                                                        sprintf($strTextQuery, 'teaser') . " OR " . 
                                                        ($isUpper?sprintf($strExactlyQuery, 'text', 'text', 'text') . " OR ":" MATCH(`text`) AGAINST ('".$_strKeywords."' IN BOOLEAN MODE) OR ") .
                                                        sprintf($strTextQuery, 'companies') . " OR " . 
                                                        sprintf($strTextQuery, 'tags') .
*/
                                                    ") AND ".
                                                    "(published=1) ".
                                                "GROUP BY id ".($strArticleOrderBy?$strArticleOrderBy:" ORDER BY relevance DESC ")
                                                );
/*
        echo $objArticles->query;
        echo '<br />';
*/
        if ($arrPagination['limit'])
        {
            $objArticles = $objArticles->limit($arrPagination['limit'], $arrPagination['offset'])->execute();
        }
        else
        {
            $objArticles = $objArticles->execute();
        }

        $time = time();
        $arrArticles = array();
        while ($objArticles->next())
        {
            $pid = ($objArticles->start < $time && $time < $objArticles->stop) ? 26 : 27 ;
            $arrArticles[] = array
            (
                'id' => $objArticles->id,
                'category' => $objArticles->archive,
                'headline' => $objArticles->headline,
                'link' => $this->generateUrl($objArticles, $pid),
                'release_tstamp' => $objArticles->release_tstamp
            );
        }
        // Assign data to the template
        $this->Template->articles = $arrArticles;
    }

    protected function generateArticleQuery($arrKeywords, $arrFields)
    {
        $isUpper = ctype_upper(preg_replace("/[0-9]/", "", implode("",$arrKeywords)));

        $strPhrasesQuery = $this->generatePhrasesQuery($arrKeywords, $arrFields); // implode(" OR ", $arrPhrasesQuery);
        $strFullTextQuery = $this->generateFullTextQuery($arrKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery);
        $strIgnoreCaseQuery = $this->generateIgnoreCaseQuery($arrKeywords, $arrFields); // implode(" OR ", $arrIgnoreCaseQuery);
/*
        echo $strPhrasesQuery.(count($arrKeywords) < 3 ? " OR " . $strFullTextQuery : "");
        echo "<br />";
*/
        $objTotal = $this->Database->prepare("SELECT COUNT(*) AS total ".
                                                "FROM tl_news ".
                                                "WHERE ".
                                                    "(".$strPhrasesQuery.
/*
                                                        ($isUpper?sprintf($strExactlyQuery, 'headline', 'headline', 'headline') . " OR ":" MATCH(`headline`) AGAINST ('".$strKeywords."' IN BOOLEAN MODE) OR ") .
                                                        sprintf($strTextQuery, 'subheadline') . " OR " . 
                                                        sprintf($strTextQuery, 'teaser') . " OR " . 
                                                        ($isUpper?sprintf($strExactlyQuery, 'text', 'text', 'text') . " OR ":" MATCH(`text`) AGAINST ('".$_strKeywords."' IN BOOLEAN MODE) OR ") .
                                                        sprintf($strTextQuery, 'companies') . " OR " . 
                                                        sprintf($strTextQuery, 'tags') .
*/
                                                    ") ".(count($arrKeywords) < 3 && !$isUpper ? " OR " . $strFullTextQuery . " OR " .$strIgnoreCaseQuery : "")." AND ".
                                                    "(published=1) "
                                                )->execute();
        $strQuery = "(".$strPhrasesQuery.")".(count($arrKeywords) < 3 && !$isUpper ? " OR " . $strFullTextQuery . " OR " .$strIgnoreCaseQuery : "");

/*
        if (count($arrKeywords) < 3)
        {
            $strQuery
        }
*/
/*
        echo "strPhrasesQuery  <br />";
        echo $objTotal->total;
        echo "<br />";
        echo $strQuery;
        echo "<br />";
*/
        if (!$objTotal->total)
        {
            if (count($arrKeywords) >= 5)
            {
                $arrSubKeywords = array_slice($arrKeywords, 0, 4);
                $strPhrasesQuery = $this->generatePhrasesQuery($arrSubKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery);
                $objTotal = $this->Database->prepare("SELECT COUNT(*) AS total ".
                                                        "FROM tl_news ".
                                                        "WHERE ".
                                                            "(".$strPhrasesQuery.") AND ".
                                                            "(published=1) "
                                                        )->execute();
                $strQuery = $strPhrasesQuery;                
/*
                echo "strPhrasesQuery 4 words <br /> ";
                echo $strQuery;
                echo "<br />";
*/
                if (!$objTotal->total)
                {
                    $arrSubKeywords = array_slice($arrKeywords, 0, 3);
                    $strPhrasesQuery = $this->generatePhrasesQuery($arrSubKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery);
                    $objTotal = $this->Database->prepare("SELECT COUNT(*) AS total ".
                                                            "FROM tl_news ".
                                                            "WHERE ".
                                                                "(".$strPhrasesQuery.") AND ".
                                                                "(published=1) "
                                                            )->execute();
                    $strQuery = $strPhrasesQuery;
/*
                    echo "strPhrasesQuery 3 words <br /> ";
                    echo $strQuery;
                    echo "<br />";
*/
                    if (!$objTotal->total)
                    {
                        $strFullTextQuery = $this->generateFullTextQuery($arrSubKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery)                    
                    }
                }
            }
            
            if (!$objTotal->total)
            {            
                $objTotal = $this->Database->prepare("SELECT COUNT(*) AS total ".
                                                        "FROM tl_news ".
                                                        "WHERE ".
                                                            "(".$strFullTextQuery.") AND ".
                                                            "(published=1) "
                                                        )->execute();
                $strQuery = $strFullTextQuery;
/*
                echo "strFullTextQuery < 5 words <br /> ";
                echo $strQuery;
                echo "<br />";
*/
                if (!$objTotal->total && !$isUpper)
                {
                    if (count($arrKeywords) < 3)
                    {
                        $objTotal = $this->Database->prepare("SELECT COUNT(*) AS total ".
                                                                "FROM tl_news ".
                                                                "WHERE ".
                                                                    "(".$strIgnoreCaseQuery.") AND ".
                                                                    "(published=1) "
                                                                )->execute();
                        $strQuery = $strIgnoreCaseQuery;
/*
                        echo "strIgnoreCaseQuery 1 word <br /> ";
                        echo $strQuery;
                        echo "<br />";
*/
                    }
                }
            }
        }
        $total = $objTotal->total;
        return array($total, $strQuery);
    }

    protected function generateCompanyQuery($arrKeywords, $arrFields)
    {
        $isUpper = ctype_upper(preg_replace("/[0-9]/", "", implode("",$arrKeywords)));
        
        $strPhrasesQuery = $this->generatePhrasesQuery($arrKeywords, $arrFields); // implode(" OR ", $arrPhrasesQuery);
        $strFullTextQuery = $this->generateFullTextQuery($arrKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery);
        $strIgnoreCaseQuery = $this->generateIgnoreCaseQuery($arrKeywords, $arrFields); // implode(" OR ", $arrIgnoreCaseQuery);

        $objTotal = $this->Database->prepare("SELECT c.id ".
                                                "FROM tl_companies c ".
                                                    " LEFT OUTER JOIN tl_company_activities ca ON ca.pid=c.id ".
                                                    " LEFT OUTER JOIN tl_company_contacts cc ON cc.pid=c.id ".
                                                    " LEFT OUTER JOIN tl_activity_descriptions ad ON ad.company_user_id=c.id ".
                                                "WHERE c.status='A' AND ".
                                                    "(".$strPhrasesQuery.")".(count($arrKeywords) < 3 && !$isUpper ? " OR " . $strFullTextQuery . " OR " .$strIgnoreCaseQuery : "").
                                                "GROUP BY c.id "
                                                )->execute();

        $strQuery = "(".$strPhrasesQuery.")".(count($arrKeywords) < 3 && !$isUpper ? " OR " . $strFullTextQuery . " OR " .$strIgnoreCaseQuery : "");
/*
        echo "strPhrasesQuery  <br />";
        echo $strQuery;
        echo "<br />";
        echo $objTotal->numRows;
        echo "<br />";
*/
        if (!$objTotal->numRows)
        {
            if (count($arrKeywords) >= 5)
            {
                $arrSubKeywords = array_slice($arrKeywords, 0, 4);
                $strPhrasesQuery = $this->generatePhrasesQuery($arrSubKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery);
                $objTotal = $this->Database->prepare("SELECT c.id ".
                                                        "FROM tl_companies c ".
                                                            " LEFT OUTER JOIN tl_company_activities ca ON ca.pid=c.id ".
                                                            " LEFT OUTER JOIN tl_company_contacts cc ON cc.pid=c.id ".
                                                            " LEFT OUTER JOIN tl_activity_descriptions ad ON ad.company_user_id=c.id ".
                                                        "WHERE c.status='A' AND ".
                                                            $strPhrasesQuery.
                                                        "GROUP BY c.id "
                                                        )->execute();
                $strQuery = $strPhrasesQuery;
/*
                echo "strPhrasesQuery 4 words <br /> ";
                echo $strQuery;
                echo "<br />";
*/
                if (!$objTotal->numRows)
                {
                    $arrSubKeywords = array_slice($arrKeywords, 0, 3);
                    $strPhrasesQuery = $this->generatePhrasesQuery($arrSubKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery);
                    $objTotal = $this->Database->prepare("SELECT c.id ".
                                                            "FROM tl_companies c ".
                                                                " LEFT OUTER JOIN tl_company_activities ca ON ca.pid=c.id ".
                                                                " LEFT OUTER JOIN tl_company_contacts cc ON cc.pid=c.id ".
                                                                " LEFT OUTER JOIN tl_activity_descriptions ad ON ad.company_user_id=c.id ".
                                                            "WHERE c.status='A' AND ".
                                                                $strPhrasesQuery.
                                                            "GROUP BY c.id "
                                                            )->execute();
                    $strQuery = $strPhrasesQuery;
/*
                    echo "strPhrasesQuery 3 words <br /> ";
                    echo $strQuery;
                    echo "<br />";
*/
                    if (!$objTotal->numRows)
                    {
                        $strFullTextQuery = $this->generateFullTextQuery($arrSubKeywords, $arrFields); // implode(" OR ", $arrFullTextQuery)                    
                    }
                }
            }
            
            if (!$objTotal->numRows && !$isUpper)
            {
                $objTotal = $this->Database->prepare("SELECT c.id ".
                                                        "FROM tl_companies c ".
                                                            " LEFT OUTER JOIN tl_company_activities ca ON ca.pid=c.id ".
                                                            " LEFT OUTER JOIN tl_company_contacts cc ON cc.pid=c.id ".
                                                            " LEFT OUTER JOIN tl_activity_descriptions ad ON ad.company_user_id=c.id ".
                                                        "WHERE c.status='A' AND ".
                                                            $strFullTextQuery.
                                                        "GROUP BY c.id "
                                                        )->execute();
                $strQuery = $strFullTextQuery;
/*
                echo "strFullTextQuery < 5 words <br /> ";
                echo $strQuery;
                echo "<br />";
*/
                if (!$objTotal->numRows)
                {
                    if (count($arrKeywords) < 3)
                    {
                        $objTotal = $this->Database->prepare("SELECT c.id ".
                                                                "FROM tl_companies c ".
                                                                    " LEFT OUTER JOIN tl_company_activities ca ON ca.pid=c.id ".
                                                                    " LEFT OUTER JOIN tl_company_contacts cc ON cc.pid=c.id ".
                                                                    " LEFT OUTER JOIN tl_activity_descriptions ad ON ad.company_user_id=c.id ".
                                                                "WHERE c.status='A' AND ".
                                                                    $strIgnoreCaseQuery.
                                                                "GROUP BY c.id "
                                                                )->execute();
                        $strQuery = $strIgnoreCaseQuery;
/*
                        echo "strIgnoreCaseQuery 1 word <br /> ";
                        echo $strQuery;
                        echo "<br />";
*/
                    }
                }
            }
        }
        $total = $objTotal->numRows;
        return array($total, $strQuery);
    }

    protected function generatePhrasesQuery($arrKeywords, $arrFields)
    {
        $strKeywords = implode(' ', $arrKeywords);
		$strPhrases = '[[:<:]]' . str_replace(array(' ', '*'), array('[^[:alnum:]]+', ''), $strKeywords) . '[[:>:]]';
        // $strPhraseQuery = " %s REGEXP '$strPhrases' ";
        $strPhraseQuery = "%s LIKE BINARY '%%%% $strKeywords %%%%' OR %s LIKE BINARY '%%%% $strKeywords%%%%' OR %s LIKE BINARY '%%%%$strKeywords %%%%'";
        $arrPhrasesQuery = array();
        foreach ($arrFields as $field)
        {
            $arrPhrasesQuery[] = sprintf($strPhraseQuery, $field, $field, $field);
        }
        return implode(" OR ", $arrPhrasesQuery);
    }

    protected function generateFullTextQuery($arrKeywords, $arrFields)
    {
        $strKeywords = implode(' ', $arrKeywords);
        $arrFullTextQuery = array();
        foreach ($arrFields as $field)
        {
            $arrFullTextQuery[] = "(".
                "(".vsprintf(implode(' AND ', array_fill(0, count($arrKeywords), "$field LIKE BINARY '%%%% %s %%%%'")), $arrKeywords).")".
                " OR (".vsprintf(implode(' AND ', array_fill(0, count($arrKeywords), "$field LIKE BINARY '%%%% %s%%%%'")), $arrKeywords).")".
                " OR (".vsprintf(implode(' AND ', array_fill(0, count($arrKeywords), "$field LIKE BINARY '%%%%%s %%%%'")), $arrKeywords).")".
            ")";
        }
        return implode(" OR ", $arrFullTextQuery);
    }

    protected function generateIgnoreCaseQuery($arrKeywords, $arrFields)
    {
        $strKeywords = implode(' ', $arrKeywords);
        $arrIgnoreCaseQuery = array();
        foreach ($arrFields as $field)
        {
            $arrIgnoreCaseQuery[] = "(".
                "(".vsprintf(implode(' AND ', array_fill(0, count($arrKeywords), " $field REGEXP '[[:<:]]%s[[:>:]]'")), $arrKeywords).")".
/*
                "(".vsprintf(implode(' AND ', array_fill(0, count($arrKeywords), "LOWER($field) LIKE '%%%% %s %%%%'")), $arrKeywords).")".
                " OR (".vsprintf(implode(' AND ', array_fill(0, count($arrKeywords), "LOWER($field) LIKE '%%%% %s%%%%'")), $arrKeywords).")".
                " OR (".vsprintf(implode(' AND ', array_fill(0, count($arrKeywords), "LOWER($field) LIKE '%%%%%s %%%%'")), $arrKeywords).")".
*/
            ")";
        }
        return implode(" OR ", $arrIgnoreCaseQuery);
    }
        
    /**
     * Generate a URL and return it as string
     * @param object
     * @return string
     */
    protected function generatePagination($total, $param='page')
    {
        // Get the current page
        $page = $this->Input->get($param) ? $this->Input->get($param) : 1;

        // Do not index or cache the page if the page number is outside the range
        if ($page < 1 || $page > ceil($total/$this->perPage))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send a 404 header
            header('HTTP/1.1 404 Not Found');
            return;
        }
        // Set limit and offset
        $limit = $this->perPage;
        $offset = (max($page, 1) - 1) * $this->perPage;

        // Overall limit
        if ($offset + $limit > $total)
        {
            $limit = $total - $offset;
        }

        // Add the pagination menu
        return array 
        (
            'limit' => $limit,
            'offset' => $offset,
            'obj_pagination' => new PaginationCustom($total, $this->perPage, 5, $param)
        );
    }

    /**
     * Generate a URL and return it as string
     * @param object
     * @return string
     */
    protected function generateUrl(Database_Result $objArticle, $pid=1)
    {
        $objPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=?")
                                  ->limit(1)
                                  ->execute($pid);

        if ($objPage->numRows)
        {
            return ampersand($this->generateFrontendUrl($objPage->row(), '/items/' . ((!$GLOBALS['TL_CONFIG']['disableAlias'] && strlen($objArticle->alias)) ? $objArticle->alias : $objArticle->cid)));
        }
      return ampersand($this->Environment->request, true);
    }
}

?>