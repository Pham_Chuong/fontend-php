<?php

class ModuleNovelSimplyBook2 extends Module
{

  protected $strTemplate = 'mod_novelsimplyBook2';
  public function generate() {
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### MODULE NOVEL SIMPLY BACKGROUND BOOK - Personalisierung Kostenloses Testen###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

       if (TL_MODE == 'FE') {

            //$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';

        }

        return parent::generate();
    }


  protected function compile()
  {
	   if($_POST){
		   $this->Template->SIE 		= $_POST['SIE'];
		   $this->Template->ER 		= $_POST['ER'];
		   $this->Template->IHRHAAR 	= $_POST['IHRHAAR'];
		   $this->Template->SEINEAUGEN 	= $_POST['SEINEAUGEN'];
		   $this->Template->STADT 		= $_POST['STADT'];
		   $this->Template->text 		= $_POST['text'];
		   
		   $this->Template->reload_data 		= True;

	   }
	   
  }

}

?>