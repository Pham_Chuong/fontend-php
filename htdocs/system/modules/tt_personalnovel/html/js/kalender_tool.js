;(function($) {

    $.fn.PnKalenderTool = function(o) {
    	
    	var defaults        = {
    		rule : {},
    		seitenaufteilung:null,
    		liniert:null,
    		startdatum:null,
    		header:null,
    		footer:null,
    		interesse:null,
    		month:null,
    		weekno:null,
    		listInteresse:null,
    		zusatzmodul:null
    	};
    	var settings        = $.extend({}, defaults, o);
    	var wrapper         = this,
    		divInteresse	= $("<div id='ka-interesse' />"),
    		divMonth		= $("<div id='ka-month' />"),
    		divWeek			= $("<div id='ka-week' />"),
    		borderEvent		= $("<div id='border-event' />");
    		divChildH		= $("<div id='ka-header' />"),
    		divChildF		= $("<div id='ka-footer' />"),
    		headers 		= $("<div class='value k-header' />"),
    		footers 			= $("<div class='value k-footer' />"),
    		divImg			= $("<div id='vorschau' />"),
    		divImgChild		= $("<div class='image' />"),
    		img				= $("<img type='kalender' />"),
    		srcImg 			= "/tl_files/personalnovel_de/image/de/kalendertool/";
    	
    	var init = function() 
    	{
    		headers.html(settings.header);
    		footers.html(settings.footer);
    		divChildH.append(headers);
    		divChildF.append(footers);
    		wrapper.append(divChildH);
    		divImgChild.append(img);
    		divImg.append(divImgChild);
    		dochange();
    		wrapper.append(divImg);
    		dochangeInteresse();
    		dochangeMonth();
    		dochangeWeek();
    		wrapper.append(divInteresse);
    		wrapper.append(divMonth);
    		wrapper.append(divWeek);
    		wrapper.append(borderEvent);
    		wrapper.append(divChildF);
    	};
    	var change = function(key,value)
    	{
    		switch(key)
    		{
    		case "header":
    			headers.html(value);
    			break;
    		case "footer":
    			footers.html(value);
    			break;
    		case "seitenaufteilung":
    			settings.seitenaufteilung = value;
    			dochange();
    			break;
    		case "liniert" : 
    			settings.liniert = value;
    			dochange();
    			break;
    		case "interesse":
    			settings.interesse = value;
    			dochangeInteresse();
    			break;
    		case "month":
    			settings.month = value;
    			dochangeMonth(settings.month-1);
    			break;
    		case "weekno":
    			settings.weekno = value;
    			dochangeWeek(settings.weekno);
    			break;
    		}
    	};
    	var dochange  = function()
    	{
    		if(settings.seitenaufteilung == 'tag_seite'){
    			$('#ka-month').addClass('tag');
    			$('#ka-week').addClass('tag');
    			
    		}else{
    			$('#ka-month').removeClass('tag');
    			$('#ka-week').removeClass('tag');
    		}
    		var src_img = srcImg + "K_"+ settings.seitenaufteilung + "_" + settings.liniert + ".png";
    		img.attr("src",src_img);
    	};
    	
    	var dochangeInteresse = function(){
    		var array  = settings.listInteresse;
			var value = settings.listInteresse[settings.interesse];
			divInteresse.html(value);
    	};
    	
    	function y2k(number) { return (number < 1000) ? number + 1900 : number; }
    	
    	function getWeek(year,month,day) {
    	    var when = new Date(year,month,day);
    	    var newYear = new Date(year,0,1);
    	    var offset = 7 + 1 - newYear.getDay();
    	    if (offset == 8) offset = 1;
    	    var daynum = ((Date.UTC(y2k(year),when.getMonth(),when.getDate(),0,0,0) - Date.UTC(y2k(year),0,1,0,0,0)) /1000/60/60/24) + 1;
    	    var weeknum = Math.floor((daynum-offset+7)/7);
    	    if (weeknum == 0) {
    	        year--;
    	        var prevNewYear = new Date(year,0,1);
    	        var prevOffset = 7 + 1 - prevNewYear.getDay();
    	        if (prevOffset == 2 || prevOffset == 8) weeknum = 53; else weeknum = 52;
    	    }
    	    return weeknum;
    	}
    	
    	var dochangeMonth = function(month){
    		if(month==null){
    			var dateNow = new Date;
        		month = dateNow.getMonth();
    		}
    		var monthNames = ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'];
			divMonth.html(monthNames[month]);
    	};
    	
    	var dochangeWeek = function(date){
    		var weekno = null;
    		if(date==null){
    			var dateNow = new Date;
        		weekno = getWeek(y2k(dateNow.getYear()),dateNow.getMonth(),1);
    		}else{
    			var arr = date.split('.');
    			weekno = getWeek(y2k(arr[2]),parseInt(arr[1])-1,arr[0]);
    		}
			divWeek.html('KW ' + weekno);
    	};
    	
    	init();
    	return {
			change:change
			
    	}
    };
    $.fn.PnSelectTool = function(o) {
    	var defaults        = {
    		select:null
    	};
    	var settings        = $.extend({}, defaults, o);
    	var wrapper         = this;
    		
    	
    	var init = function() {
    		var dateNow = new Date;
    		var month = dateNow.getMonth() + 1;
    		var year = dateNow.getFullYear();
    		var select = '';
    		for (var i = month; i <= 12; i ++ )
				{
					var option			=$("<option>");
					select = '';
					var modth_ = i < 10?"0"+i:i;
					if(settings.select && settings.select == i) select = "selected='selected'"; 
					option.attr("selected",select);
					option.attr("value",i);
					option.html("01."+ modth_ + "." + year);
					wrapper.append(option);
				} 
    		if(month != 1)
    		{
    			for (var i = 1; i < month; i ++ )
    				{
    					var option			=$("<option>");
    					select = '';
    					var modth_ = i < 10?"0"+i:i;
						if(settings.select && settings.select == i) select = "selected='selected'"; 
						option.attr("selected",select);
						option.attr("value",i);
						option.html("01."+ modth_ + "." + (year + 1) );
						wrapper.append(option);
    				}
    		}
    	};
    	init();
    };
    
    $.fn.changeText = function(o){
			var nodes 		= $(this),
			defaults 		= {},
			settings 		= $.extend({}, defaults, o);
			var defaulttext = nodes.attr('data-default');
			if(nodes.val() == ''){
   		   	 	 nodes.val(defaulttext);
   		   	 	 nodes.css({"color":"#CCC"});
   		   	 	 nodes.keyup();
   		   	 }
   		   	 nodes.focus(function() {
						if (nodes.val() === defaulttext) {
							nodes.val('');
							nodes.css({"color":"#404040"});
							nodes.keyup();
						}
					  });
			 nodes.focusout(function() {
						if(nodes.val()==''){
							nodes.val(defaulttext);
							nodes.css({"color":"#CCC"});
							nodes.keyup();
						    }
					  });
			 nodes.click(function(){
			 		 if(nodes.val() == defaulttext){
			 		 	nodes.val(''); 
			 		 	nodes.css({"color":"#404040"});
			 			nodes.keyup();
			 		 }
			 });
		};
})(jQuery);

