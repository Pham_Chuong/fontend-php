jQuery.noConflict();
jQuery(document).ready(function() {

  //for input search
	var search_keyword_default = 'Hier Ihre Suche eingeben';
	jQuery('.gsc-input').val(search_keyword_default);
	jQuery('.gsc-input').focus(function() {
		if(jQuery(this).val() == search_keyword_default)
			jQuery(this).val('');
		})
	.blur(function() {
				if(jQuery(this).val() == '')
					jQuery(this).val(search_keyword_default);
		});
  // simple example, using all default options
  /***** apply the summarize() method to the "containing" element ****/
  if(jQuery('div.expand').length>0){
  	jQuery('div.expand').summarize(); 
  }
  if(jQuery('a.lightbox').length>0){
  jQuery('a.lightbox').lightBox({
    		overlayBgColor: '#000',
		imageLoading: '/system/modules/tt_manage_books/html/img/lightbox-ico-loading.gif',
		imageBtnClose: '/system/modules/tt_manage_books/html/img/lightbox-btn-close.gif',
		imageBtnPrev: '/system/modules/tt_manage_books/html/img/lightbox-btn-prev.gif',
		imageBtnNext: '/system/modules/tt_manage_books/html/img/lightbox-btn-next.gif',
		imageBlank: '/system/modules/tt_manage_books/html/img/lightbox-blank.gif',
		containerResizeSpeed: 350,
		txtImage: 'Bild',
		txtOf: 'von'
	});
  }
  
  if(jQuery('a.popup-link').length>0){
	jQuery('.popup-link').popupWindow({ 
	height:650, 
	width:600, 
	scrollbars: 1,
	top:50, 
	left:50 
	}); 
  }
  
  if(jQuery('article#anfrage textarea.nachricht').length>0){
  	  var text='Sehr geehrte Frau Mayer,\n\n'
  	  text += 'Wir interessieren uns für eine Bestellung Ihrer PersonalNovels als Firmengeschenke. Bitte senden Sie uns unverbindlich und kostenfrei weitere Informationen zu und/oder rufen uns unter oben genannter Telefonnummer zurück.\n\n';

  	  text += 'Am besten sind wir zu erreichen ___________________.\n\n'

  	  text += 'Wir planen, ca ___ Bücher zu bestellen und benötigen diese bis spätestens _________.';
  	  
  	  jQuery('article#anfrage textarea.nachricht').val(text);
  }
    
});


