<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/**
 * Class ModuleContact
 *
 * Front end module "Contact".
 * @package    Controller
 */
class ModuleContact extends Module
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_contact';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### NEWSLETTER SUBSCRIBE ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}


		return parent::generate();
	}


	/**
	 * Generate module
	 */
	protected function compile()
	{
		
		$blnHasError = false;
		
		if ($this->Input->post('FORM_SUBMIT') == 'tl_contact')
		{
			$sex = (int)$this->Input->post('sex', true);
			$mail = $this->idnaEncodeEmail($this->Input->post('email', true));
			$name = htmlspecialchars(trim(strip_tags($this->Input->post('name', true))));
			$message = htmlspecialchars(trim(strip_tags($this->Input->post('message', true))));
			$subject = htmlspecialchars(trim(strip_tags($this->Input->post('subject', true))));
			$your_order = htmlspecialchars(trim(strip_tags($this->Input->post('your_order', true))));
			if(!isset($_POST['sex']) || !$this->isValidEmailAddress($mail) ||
				empty($name) || empty($message) || empty($subject)){
				$blnHasError = true;
				$this->Template->messError = $GLOBALS['TL_LANG']['ERR']['required'];
			}else{
				$body = "Hi,$name \n\n $message";
				$headers = "From: $mail";
				if (mail($GLOBALS['TL_CONFIG']['adminEmail'], $subject, $body, $headers)) {
					$this->Template->messSucc = $GLOBALS['TL_LANG']['FMD']['send_succ'];
				} else {
					$blnHasError = true;
					$this->Template->messError = $GLOBALS['TL_LANG']['ERR']['send_err'];
				}
			}
		}

		// Default template variables
		$this->Template->email = '';
		$this->Template->showChannels = !$this->nl_hideChannels;
		$this->Template->submit = specialchars($GLOBALS['TL_LANG']['FMD']['submit']);
		$this->Template->subjectLabel = $GLOBALS['TL_LANG']['FMD']['subject'];
		$this->Template->your_orderLabel = $GLOBALS['TL_LANG']['FMD']['your_order'];
		$this->Template->your_messageLabel = $GLOBALS['TL_LANG']['FMD']['your_message'];
		$this->Template->sexLabel = $GLOBALS['TL_LANG']['FMD']['sex'];
		$this->Template->manLabel = $GLOBALS['TL_LANG']['FMD']['man'];
		$this->Template->womenLabel = $GLOBALS['TL_LANG']['FMD']['women'];
		$this->Template->your_messageLabel = $GLOBALS['TL_LANG']['FMD']['your_message'];
		$this->Template->required = $GLOBALS['TL_LANG']['FMD']['required'];
		$this->Template->formId = 'tl_contact';
		$this->Template->id = $this->id;
		$this->Template->hasError = $blnHasError;
		$this->Template->messError = $GLOBALS['TL_LANG']['ERR']['required'];
	}

}

?>