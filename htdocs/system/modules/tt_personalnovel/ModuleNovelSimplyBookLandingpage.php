<?php

class ModuleNovelSimplyBookLandingpage extends Module
{
  protected $strTemplate = 'mod_novelsimplyKinderbuchtool';
  public function generate() {
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### MODULE NOVEL SIMPLY BACKGROUND BOOK - Change background select template###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

       if (TL_MODE == 'FE') {
		
            //$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/order_kinder.js';
            if($this->pn_template_tools)
		$this->strTemplate = $this->pn_template_tools;
        }

        return parent::generate();
    }


  protected function compile()
  {
	//teaser simple   
	if($_POST){
		   $this->Template->SIE 		= $_POST['SIE'];
		   $this->Template->ER 		= $_POST['ER'];
		   $this->Template->IHRHAAR 	= $_POST['IHRHAAR'];
		   $this->Template->SEINEAUGEN 	= $_POST['SEINEAUGEN'];
		   $this->Template->STADT 		= $_POST['STADT'];
		   $this->Template->text 		= $_POST['text'];
		   
		   $this->Template->reload_data 		= True;

	   }
	  if($this->strTemplate == "mod_novelsimplyKalendertool"){
	  	  $bookItem 			= new PnBookItem();
	  	  $event = array();
	  	  $interesens = array(2,3,4,5,6,7,8,9,11);
          foreach($interesens as $value)
            {
            	   $events = $bookItem->getEventCategory($value,1);
            	   $event[$value] = $events[0];
            }
          $this->Template->event = json_encode($event);
	  }
  }

}

?>
