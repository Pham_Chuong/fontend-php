<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Coin Slider by Ivan Lazarevic &#40;http://workshop.rs&#41;
 * @author     Lionel Maccaud, David Imboden &#40;http://www.synergie-consulting.com&#41; 
 * @package    coinSlider 
 * @license    MIT 
 * @filesource
 */


/**
 * Back end modules
 */
 
$GLOBALS['TL_LANG']['MOD']['novel_simply'] = array('Novel Simply', 'Manage your Novel Simply.');
$GLOBALS['TL_LANG']['MOD']['rssblog'] = array('Rss Personal Blog','');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['novel_simply'] = array('Novel Simply','Manage your Novel Simply');
$GLOBALS['TL_LANG']['FMD']['rssblog'] = array('Rss Personal Blog','');

$GLOBALS['TL_LANG']['FMD']['sex'] = "Anrede";
$GLOBALS['TL_LANG']['FMD']['man'] = "Herr";
$GLOBALS['TL_LANG']['FMD']['women'] = "Frau";
$GLOBALS['TL_LANG']['FMD']['subject'] = "Betreff";
$GLOBALS['TL_LANG']['FMD']['your_order'] = "Ihre Bestellnummer";
$GLOBALS['TL_LANG']['FMD']['your_message'] = "Ihre Nachricht";
$GLOBALS['TL_LANG']['FMD']['submit'] = "Abschicken";
$GLOBALS['TL_LANG']['FMD']['required'] = "Pflichtfelder";
$GLOBALS['TL_LANG']['FMD']['send_succ'] = "E-Mails senden Fehler!";

$GLOBALS['TL_LANG']['ERR']['required'] = "Bitte geben Pflichtfeld!";
$GLOBALS['TL_LANG']['ERR']['send_err'] = "E-Mails senden Fehler!";

?>