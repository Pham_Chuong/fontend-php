<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class CustomListingElement extends ContentElement
{
    protected $strTemplate = 'ce_custom_listing';
    
    public function generate()
	{
	    if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
			$objTemplate->wildcard = '### PERSONALNOVEL CUSTOM LISTING ###';
			$objTemplate->title = 'PersonalGifts\' Books';
			return $objTemplate->parse();
		}
		
		return parent::generate();
	}
	
    protected function compile()
	{
	    $config = new PnConfig();
	    
	    $selected_ids = unserialize($this->custom_book_selected);
	    $selected_ids = implode(', ', $selected_ids);
	    
	    $ids    = "(" . $selected_ids . ")";
	    $query  = "SELECT * FROM " . $config->database . ".buecher WHERE buch_id IN " . $ids;
	    $obj_db = $this->Database->prepare($query)->execute();
	    $total  = $obj_db->numRows;
	    $offset = 0;
		$limit  = null;
	    
	    $per_page = $this->perPage;
	    if($per_page > 0)
	    {
	        $page = $this->Input->get('page') ? $this->Input->get('page') : 1;
	        if ($page > ($total / $per_page))
			{
				$page = ceil($total / $per_page);
			}
			
			$limit = $per_page;
			$offset = ($page - 1) * $per_page;
			
			$obj_pagination = new Pagination($total, $per_page);
			$this->Template->pagination = $obj_pagination->generate();
	    }
	    
	    $obj_result = $this->Database->prepare($query);
	    if (isset($limit))
		{
			$obj_result->limit($limit, $offset);
		}
		elseif ($total > 0)
		{
			$obj_result->limit($total, $offset);
		}
		
		$obj_result = $obj_result->execute();
	    
		
	    $books = array();
	    while($obj_result->next())
	    {
	        $book           = new PnBook();
	        $book->id       = $obj_result->buch_id;
	        $book->title    = $obj_result->buch_titel;
	        $book->author   = $obj_result->buch_autor;
	        $book->type_id  = $obj_result->type_id;
	        $book->genre_id = $obj_result->genre_id;
	        $book->text     = $obj_result->buch_text;
	        
	        $img = "/tl_files/thumbnail-book/" . standardize($obj_result->buch_titel) . "-" . $obj_result->buch_id . "-list.png";
	        $book->image = $img;
	        
	        $str_url    = sprintf("/%s/%s", $obj_result->buch_id, standardize($obj_result->buch_titel));
	        $detail_url = $this->generateFrontendUrl(array("alias" => "book") , $str_url);
	        if($this->book_detail_url) 
			{
			    $obj_jumpto  = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id = ?")->execute($this->book_detail_url);
			    $detail_url = $this->generateFrontendUrl($obj_jumpto->row(), $str_url);
			}
			
			$book->url = $detail_url;
	        $books[] = $book;
	    }
	    
	    $this->Template->books = $books;
	    $this->Template->result_from = $this->perPage * $page - $this->perPage + 1;;
	    $this->Template->result_to = ($this->perPage * $page) <= $total ? ($this->perPage * $page) : $total;
	    $this->Template->result_total = $total;
	}
}
