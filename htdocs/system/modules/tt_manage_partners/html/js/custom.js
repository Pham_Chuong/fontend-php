$(document).ready(function(){
	$(".tabsdiv").each(function(){
		$(this).tabs();
	});
	
	$(".field_group a.more").click(function(e){
        e.preventDefault();
        $(this).parent().children(".description").toggle(200);
    });
    
    $('.disclosure-widget strong').click(function(){
		$(this).parent().toggleClass("disclosed");
		$(this).parent().children("div").toggle(200);
	});
});