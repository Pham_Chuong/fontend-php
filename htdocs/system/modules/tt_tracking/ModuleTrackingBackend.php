<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

class ModuleTrackingBackend extends BackendModule
{
    protected $strTemplate = 'be_mod_tracking';
    
    protected function compile()
	{
	    if($_GET["viewid"])
	    {
	        $query = sprintf("SELECT * FROM tl_google_adwords_tracking
	                  WHERE clid = '%s'", $_GET["viewid"]);
	    }
	    else
	    {
	        $query = "SELECT * FROM (
                        SELECT * FROM tl_google_adwords_tracking
                        ORDER BY tstamp DESC
                    ) AS temp
                    GROUP BY clid
                    ORDER BY tstamp DESC";
	    }
	    
	    $objDb = $this->Database->prepare($query)->execute();
	        
	    $this->Template->listTracking = $objDb->fetchAllAssoc();
	}
}

?>