$(document).ready(function(){
	
	$('ul.tag-mega-dropdown .mega > span').click(function(){
		if($(this).parent().attr('class')=='mega'){
		var selector = $(this);
		setTimeout(function(){
			$(selector).parent().toggleClass('selected');
			$(selector).parent().children('.values').show();
		}, 50);
		}else{
		setTimeout(function(){
			$(selector).parent().removeClass('selected');
			$(selector).parent().children('.values').hide();
		}, 50);
		}
	});

	$(document).click(function(){
			$('ul.tag-mega-dropdown .mega').removeClass('selected');
			$('ul.tag-mega-dropdown .values').hide();
	});

	$("#perpage" ).change(function() {
		 var numlimit = $("#perpage").val();
		 if(limit(numlimit)){
			 var url = window.location.href;
			 url = removeParam('limit',url);
			 window.location.href = url + '&limit=' + numlimit;
		 }else{
			 window.location.href = window.location.href + '&limit=' + numlimit;
		 }
	});
});
function check_total_product( number ){
	if( number == 0 || number == '' ){
		alert( "Ausverkauft" );
		return false;
	}
	return true;
} 

function limit(number){
	var url = window.location.search;
	if (url.match("limit") && url.match("limit").length > 0) {
		return true;
	}
	return false;
}


function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param.indexOf(key) !== -1) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}
