$(document).ready(function(){
	$("#tabsdiv").tabs();
	$('a.lightbox').lightBox({
    		overlayBgColor: '#000',
		imageLoading: '/system/modules/tt_manage_books/html/img/lightbox-ico-loading.gif',
		imageBtnClose: '/system/modules/tt_manage_books/html/img/lightbox-btn-close.gif',
		imageBtnPrev: '/system/modules/tt_manage_books/html/img/lightbox-btn-prev.gif',
		imageBtnNext: '/system/modules/tt_manage_books/html/img/lightbox-btn-next.gif',
		imageBlank: '/system/modules/tt_manage_books/html/img/lightbox-blank.gif',
		containerResizeSpeed: 350,
		txtImage: 'Bild',
		txtOf: 'von'
	});
});

