$(document).ready(function(){
    /* Config confirm dialog */
    $.alerts.overlayOpacity = 0.3;
    $.alerts.overlayColor = "#000";
        
	//Hide error image	
	$("img").error(function(){
	$(this).hide();     
	});    

	$("#tabsdiv").tabs();
	$("#festtage").tabs();
	$('#step1 .disclosure-widget strong').click(function(){
		$(this).parent().toggleClass("disclosed");
		$(this).parent().children("div").toggle(200);
	});
	

	
    $("input.field").each(function(){
    	function a(){
    		var g=d.attr("id"),s=d.val();
    		d.removeClass("error");
    		s!==e?$("span."+g).add(d).removeClass("default"):$("span."+g).add(d).addClass("default");
    		$("span."+g).text(s)}var d=$(this),e=d.attr("data-default");
    		d.bind("keyup",a);
    		d.bind("focus",function(){d.val()===e&&d.val("")});
    		d.bind("blur",function(){
    			d.val()===""&&d.val(e);
    			a();
    		});
    		a();
    });
    
    $(".copy_me").click(function(e){
		e.preventDefault();
		var text = $(this).parent().children(".widmung").html();
		/*text = text.replace(/<br>/g, '\n');
		text = text.replace(/<br\/>/g, '\n');
		text = text.replace(/<br \/>/g, '\n');
		*/
		text = text.replace(/<br>/gi,'\r\n');
		text = text.replace(/<BR hideFocus style="_noFocusLine: true">/gi,'\r\n');
		$("#var_text").val(text);
		$("#var_text").removeClass("notext");
		changePreviewImageUrl();
    });
    
    $('a.lightbox').lightBox({
    	overlayBgColor: '#000',
		imageLoading: '/system/modules/tt_manage_books/html/img/lightbox-ico-loading.gif',
		imageBtnClose: '/system/modules/tt_manage_books/html/img/lightbox-btn-close.gif',
		imageBtnPrev: '/system/modules/tt_manage_books/html/img/lightbox-btn-prev.gif',
		imageBtnNext: '/system/modules/tt_manage_books/html/img/lightbox-btn-next.gif',
		imageBlank: '/system/modules/tt_manage_books/html/img/lightbox-blank.gif',
		containerResizeSpeed: 350,
		txtImage: 'Bild',
		txtOf: 'von'
    });
    
    /* set gia khi chon font */
    function updateWidmungPrice()
    {
        $("h1.book-price").html(formatPrice(roundNumber(parseFloat($("h1.book-price").attr("price")) 
    	    + parseFloat($(".step2 .bottom #widmungPrice").val()), 2), "de") + " EUR");
    	
    	var new_price = parseFloat($(".step2 .bottom #widmungPrice").val());
    	var old_price = $("h1.book-price").attr("old_price");
    	
    	var cartsum = $("#shoppingcartlogo .cartsum").attr("cartsum");
    	cartsum = parseFloat(cartsum) - parseFloat(old_price) + parseFloat(new_price);
    	$("#shoppingcartlogo .cartsum").html(formatPrice(roundNumber(cartsum, 2), "de"));
    }
    
    $(".step2 .top #var_fontface_0").click(function(){ 
        $(".step2 #artikel-0").attr("checked", "checked");
        $("input[name=var_fontcolor]").val("black");
        $(".step2 .bottom #widmungPrice").val("0");
        
        changePreviewImageUrl(); 
    });
    
    $(".step2 .bottom #var_fontface_0").click(function(){
        if($("input[name=var_fontcolor]").val() != "black") {
            if(parseInt($(".step2 .bottom #widmungPrice").val()) == 0) {
                jAlert('Widmung "Handschrift" '+ $(".step2 .bottom #widmungPrice").attr("price") + ' EUR'); 
            }
            $(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
        } else {
            $(".step2 .bottom #widmungPrice").val("0");
        }
        
        $(".step2 #artikel-1").attr("checked", "checked");
        changePreviewImageUrl();
    });
    
    $(".step2 .bottom #var_fontface_1, .step2 .bottom #var_fontface_2, .step2 .bottom #var_fontface_3").click(function(){
        if(parseInt($(".step2 .bottom #widmungPrice").val()) == 0){
            jAlert('Widmung "Handschrift" '+ $(".step2 .bottom #widmungPrice").attr("price") + ' EUR'); 
        }
        
        $(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
        $(".step2 #artikel-1").attr("checked", "checked");
        changePreviewImageUrl();
    });
    
    /* set gia khi chon mau */ 
    $("#fontcolor_blob_black").click(function(){ 
        $("input[name=var_fontcolor]").val("black");
    	if($(".step2 .top  #var_fontface_0").is(':checked') || $(".step2 .bottom  #var_fontface_0").is(':checked')) {
    		$(".step2 .bottom  #var_fontface_0").attr("checked", "checked");
    		$(".step2 .bottom #widmungPrice").val("0");
    	} else {
    	    $(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
    	}
    	
    	$(".step2 #artikel-1").attr("checked", "checked");
    	changePreviewImageUrl(); 
    });
    
    $("#fontcolor_blob_darkred").click(function(){ 
        $("input[name=var_fontcolor]").val("darkred"); 
    	if(parseInt($(".step2 .bottom #widmungPrice").val()) == 0)
    	{
    	    jAlert('Widmung "Handschrift" '+ $(".step2 .bottom #widmungPrice").attr("price") + ' EUR'); 
    	}
    	
    	$(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
    	$(".step2 #artikel-1").attr("checked", "checked");
    	if($(".step2 .top  #var_fontface_0").is(':checked')) {
    		$(".step2 .bottom  #var_fontface_0").attr("checked", "checked");
    	}
    	changePreviewImageUrl(); 
    });
    
    $("#fontcolor_blob_darkgreen").click(function(){ 
        $("input[name=var_fontcolor]").val("darkgreen");
    	if(parseInt($(".step2 .bottom #widmungPrice").val()) == 0)
    	{
    	    jAlert('Widmung "Handschrift" '+ $(".step2 .bottom #widmungPrice").attr("price") + ' EUR'); 
    	}
    	
    	$(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
    	$(".step2 #artikel-1").attr("checked", "checked");
    	if($(".step2 .top  #var_fontface_0").is(':checked')) {
    		$(".step2 .bottom  #var_fontface_0").attr("checked", "checked");
    	}
    	changePreviewImageUrl(); 
    });
    
    $("#fontcolor_blob_blue").click(function(){ 
        $("input[name=var_fontcolor]").val("blue"); 
    	if(parseInt($(".step2 .bottom #widmungPrice").val()) == 0)
    	{
    	    jAlert('Widmung "Handschrift" '+ $(".step2 .bottom #widmungPrice").attr("price") + ' EUR'); 
    	}
    	
    	$(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
    	$(".step2 #artikel-1").attr("checked", "checked");
    	if($(".step2 .top  #var_fontface_0").is(':checked')) {
    		$(".step2 .bottom  #var_fontface_0").attr("checked", "checked");
    	}
    	changePreviewImageUrl(); 
    });
    
    $("#fontcolor_blob_darkmagenta").click(function(){ $("input[name=var_fontcolor]").val("darkmagenta"); 
        if(parseInt($(".step2 .bottom #widmungPrice").val()) == 0)
    	{
    	    jAlert('Widmung "Handschrift" '+ $(".step2 .bottom #widmungPrice").attr("price") + ' EUR'); 
    	}
    	
        $(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
    	$(".step2 #artikel-1").attr("checked", "checked");
        if($(".step2 .top  #var_fontface_0").is(':checked')) {
    		$(".step2 .bottom  #var_fontface_0").attr("checked", "checked");
    	}
    	changePreviewImageUrl(); 
    });
    
    $(".step1 .formitem .field input").keyup(function(){
    		var className = $(this).attr("id").replace("vars-", "");
    		$("#preview ." + className).html($(this).val());
    });
    
    $(".step1 .formitem .field input").blur(function(){
    		var className = $(this).attr("id").replace("vars-", "");
    		$("#preview ." + className).html($(this).val());
    });
    
    $(".step1 .formitem .field input").each(function(){
    		var className = $(this).attr("id").replace("vars-", "");
    		$("#preview ." + className).html($(this).val());
    });
    
    $(".step2 .top #artikel-0").click(function(){
    		
    		$(".step2 .top #var_fontface_0").attr("checked", "checked");
    		$("input[name=var_fontcolor]").val("black");
    		$(".step2 .bottom #widmungPrice").val("0");
    		changePreviewImageUrl();
    });
    
    
    
    if($(".step2 #artikel-1").is(':checked')){
    	$(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
    } 
    
    
    $(".step2 #artikel-1").click(function(){
    		if($(".step2 .top #var_fontface_0").is(':checked')) {
    			$(".step2 .bottom #var_fontface_0").attr("checked", "checked");
    			//$(".step2 .bottom #widmungPrice").val($(".step2 .bottom #widmungPrice").attr("price"));
    			//jAlert('Widmung "Handschrift" '+ $(".step2 .bottom #widmungPrice").val() + ' EUR');
    			changePreviewImageUrl();
    		}
    });
    
    var widmung_image_url = "http://personalnovel.de/preview/WidmungItem/7118343/87bfde7fd52b178fe99321e56101801c.png?width=510";
    var widmung_image_url_pre = "http://personalnovel.de/preview/WidmungItem/7118343/87bfde7fd52b178fe99321e56101801c.png?width=750";
    if($("#widmung_image_url").length > 0)
    {
        widmung_image_url = $("#widmung_image_url").val() + "/preview/WidmungItem/7118343/87bfde7fd52b178fe99321e56101801c.png?width=510";
        widmung_image_url_pre = $("#widmung_image_url").val() + "/preview/WidmungItem/7118343/87bfde7fd52b178fe99321e56101801c.png?width=750";
    }
    /*$(".step2 .formitem textarea").change(function(){
    		changePreviewImageUrl();
    });*/
    
    $(".step2 .formitem textarea").typing({
        stop: changePreviewImageUrl,
        delay: 500
    });
    
    if($(".step2").length>0){
      changePreviewImageUrl();
    }
    
    $(".step4 .designartikel").click(function(){
         $(".step4 #designPrice").val($(this).attr("price"));     
    });
    
     $(".step4 .designartikel").each(function(){
     		if($(this).is(':checked')){
     			$(".step4 #designPrice").val($(this).attr("price"));	
     		}     
     });
    
    function changePreviewImageUrl(){
            var var_text = $(".step2 .formitem textarea");
    		var fontface = $("input:radio[name=var_fontface]:checked'").val() 
    		                  ? $("input:radio[name=var_fontface]:checked'").val()
    		                  : "Century Schoolbook L";
    		var text = $(".step2 .formitem textarea").val();
    		var fontcolor = $("input[name=var_fontcolor]").val() ? $("input[name=var_fontcolor]").val(): "black";
    		
    		if(text == "" || text == "Hier bitte Ihre Widmung eingeben")
    		{
    		    text = "";
    			fontcolor = "red";
    		}
    		
            fontsize = 14;
            if(fontface == "Century Schoolbook L") fontsize=10;
    		
    		var url = widmung_image_url + "&text=" + encodeURIComponent(text) + "&fontface=" + fontface + "&fontcolor=" + fontcolor + "&fontsize=" + fontsize;
    		var url_pre = widmung_image_url_pre + "&text=" + encodeURIComponent(text) + "&fontface=" + fontface + "&fontcolor=" + fontcolor + "&fontsize=10";
            $(".step2 .formitem .img_preview a.lightbox").attr("href", url_pre);
            
            $("#twirly").css("visibility", "visible");
    		$.preload([url], {
                onComplete: function (data) {
                  if (data.found) {
                      $(".step2 .formitem .img_preview img.img_widmung").attr("src", url);
                      $("#texttoolong").hide();
                      $("input[name=input_error]").val("");
                  } else {
                      $("#texttoolong").show();
                      $("input[name=input_error]").val("texttoolong");
                  }
                },
                onFinish: function(){
                  $("#twirly").css("visibility", "hidden");
                }
			});
    		
    		updateWidmungPrice();
    };
    
    $("#var_text").focus(function(){
        if ($(this).val() === "Hier bitte Ihre Widmung eingeben") {
            $(this).val('');
            $(this).removeClass("notext");
        }
    });
    
    $(".formitem .left .col1 input").click(function(){
		var selector = $(this);
		setTimeout(function(){
			$(".formitem .left .col2").removeClass("selected");
			selector.parent().parent().children(".col2").addClass("selected");
		}, 50);
    });
    
    $(".step6 .formitem input:checkbox").click(function(){
    		var price_tag = $(this).parent().parent().parent().children(".right").children(".addon-price");
    		if($(this).is(":checked")){
    			if($(this).attr('id') == 'addon-artikel-0' && $('#addon_anzahl').val() > 0){
    				 var number = $('#addon_anzahl').val();
    				$(price_tag).html(parseFloat($(this).attr("price"))*number + " EUR");
    			}else{
    				$(price_tag).html($(this).attr("price") + " EUR");
    			}
    			
    		}	
    		else
    			$(price_tag).html("0.00 EUR");
    		
    		totalAddonPrice();
    });
    
    function totalAddonPrice(){
    		var price = 0;
    		$(".step6 .formitem input:checked").each(function(){
    			if($(this).attr('id') == 'addon-artikel-0' && $('#addon_anzahl').val() > 0){
    				 var number = $('#addon_anzahl').val();
    				 price += parseFloat($(this).attr("price"))*number;
    			}else{
    				price += parseFloat($(this).attr("price"));
    			}
    			
    		});
    		
    		var totalPrice = price > 0 ? roundNumber(price, 2) + " EUR" : "0.00 EUR";
    		$(".formsummary .addon-price").html(totalPrice);
    }
    
    function formatPrice(price, locale) {
        price = price + "";
        if(locale == "de")
        {
            price = price.replace(".", ",");
        }
        
        return price;
    }
    
    totalAddonPrice();
    
    
    function checkFormitem(){
    	    	var isEdit=false;
		$(".formitem .fields input").each(function(){
			if($(this).attr("data-default") != $(this).val())
			{
				isEdit=true;
			}
		});
		$('input.formitem-edit').val(isEdit);
    }
    
    //Update Kinder Image
    function updateKinderImg()
    {
    	    var url = $(".step1 #vorschau .image img").attr('src').split("['width-375']-hintergrund_gender")[0];
    	    var type = $(".step1 #vorschau .image img").attr('type');
    	    var hairtype = $('#vars-hairtype option:selected').val();
    	    var augen = $('#vars-augen option:selected').val();
    	    var haare = $('#vars-haare option:selected').val();
    	    var src='';
    	    
    	    if(type=='poesiealben'){
    	    	    src = "['width-375']-hintergrund.jpg";
    	    }
    	    
    	    if(type=='maedchen'){
    	    	    src = url + src + "['width-375']-hintergrund_gender-maedchen_hairtype-" + hairtype + '_augen-' + augen + '_haare-' + haare + '.jpg';
    	    }
    	    
    	    if(type=='junge'){
    	    	    src = url + src + "['width-375']-hintergrund_gender-junge_hairtype-" + hairtype + '_augen-' + augen + '_haare-' + haare + '.jpg';
    	    }
    	    
    	    $(".step1 #vorschau .image img").attr('src',src);  
    	    $(".step1 #vorschau .image img").show();
    }
    
    $('#vars-hairtype').change(function() {
    	updateKinderImg()
    });
    
     $('#vars-augen').change(function() {
    	updateKinderImg()
    });
     
      $('#vars-haare').change(function() {
    	updateKinderImg()
    });
    
    
    
    function roundNumber(num, dec) {
		var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
		if (result.toFixed)
			result = result.toFixed(dec);
		return result;
	}
	
	$(".step1 button[name=submitter]").click(function(){
		var isValid = false;
		var isDefault = true;
		var isError = false;
		$(".formitem .fields input").each(function(){
			if($(this).attr("data-default") != $(this).val())
			{
				isValid = true;
				isDefault = false;
			}
		});
		
		$(".formitem .fields input").each(function(){
            if($(this).val().match(/[^A-Za-z0-9.\-öÖüÜÄäß&:_\ ]/))
			{
			    //isValid = false;
			    //isError = true;
			    //$(this).addClass("error");
			}
		});
		
		if(!isValid){
		    if(isError)
		    {
		        alert("Sonderzeichen werden nicht unterstützt");
		        return false;
		    }
		    if(isDefault)
		    {
                $.alerts.okButton = "Bestätigen & weiter";
                $.alerts.cancelButton = "Eingaben überprüfen ";
                jConfirm("Alle Angaben werden exakt so gedruckt, wie von Ihnen eingegeben.<br />Es werden keine Korrekturen von uns vorgenommen.", 
                    "confirm", function(r){
                    if(r) {
                        $(".step1 button[name=submitter]").unbind("click").click();
                    }
                });
                
                return false;
			}
		}
		
	    return true;	
	});
	
	$(".step1 button[name=quicksubmitter]").click(function(){
		var isValid = false;
		var isDefault = true;
		var isError = false;
		$(".formitem .fields input").each(function(){
			if($(this).attr("data-default") != $(this).val())
			{
				isValid = true;
				isDefault = false;
			}
		});
		
		$(".formitem .fields input").each(function(){
            if($(this).val().match(/[^A-Za-z0-9.\-öÖüÜÄäß&:_\ ]/))
			{
			    //isValid = false;
			    //isError = true;
			    //$(this).addClass("error");
			}
		});
		
		if(!isValid){
		    if(isError)
		    {
		        alert("Sonderzeichen werden nicht unterstützt");
		        return false;
		    }
		    if(isDefault)
		    {
                $.alerts.okButton = "Bestätigen & weiter";
                $.alerts.cancelButton = "Eingaben überprüfen ";
                jConfirm("Alle Angaben werden exakt so gedruckt, wie von Ihnen eingegeben.<br />Es werden keine Korrekturen von uns vorgenommen.", 
                    "confirm", function(r){
                    if(r) {
                        $(".step1 button[name=quicksubmitter]").unbind("click").click();
                    }
                });
                
                return false;
			}
		}
		
	    return true;
	});
	
	$(".type_literaturkklassiker button[name=submitter], .type_literaturkklassiker button[name=quicksubmitter]").unbind("click");
	$(".type_literaturkklassiker button[name=submitter], .type_literaturkklassiker button[name=quicksubmitter]").click(function(){
	
		$("input.field").removeClass("error");
		$(".errordesc").html("");
		
		var isValid = true;
		
		if($("#vars-Hauptperson_VN").val() == "")
		{
			$("#vars-Hauptperson_VN").addClass("error");
			$("#vars-Hauptperson_VN").parent().children(".errordesc").html("Achtung: Dieses Feld darf nicht leer sein.");
			isValid = false;
		}
		
		if($("#vars-Hauptperson_NN").val() == "")
		{
			$("#vars-Hauptperson_NN").addClass("error");
			$("#vars-Hauptperson_NN").parent().children(".errordesc").html("Achtung: Dieses Feld darf nicht leer sein.");
			isValid = false;
		}
		
		return isValid;
	
	});
	
	$(".step2 #nextwithout").click(function(){
		var isValid = false;
		if($("#var_text").val() == "" && $(".step2 .bottom #widmungPrice").val() == 0) {
			return true;
		//} else if ($(".step2 .bottom #widmungPrice").val() > 0) {
		//    $(".right .error").show();
		//    return false;
		} else{
			if(confirm("Sie haben bereits eine Widmung eingegeben.\nWollen Sie diese wirklich löschen?")) {
				$("#var_text").val("");
				$("#artikel-0").click();
				return true;
			} else {
				return false;
			}
		}
		
	});
	
	$(".step2 #kindernextpage").click(function(){
		if($("#var_text").val() == "" || $("#var_text").val() == "Hier bitte Ihre Widmung eingeben") {
			if(confirm("Sie haben noch keine Widmung eingegeben.\nWollen Sie wirklich zum nächsten Schritt weiter gehen?")) {
				//$("#form_error").show();
				return false;
			}
			return false;
		} else {
			//$("#form_error").hide();
			return true;
		}
	});
	
	$(".step2 #nextpage").click(function(){
		if($("#var_text").val() == "" || $("#var_text").val() == "Hier bitte Ihre Widmung eingeben") {
			if(confirm("Sie haben bereits eine Widmung eingegeben.\nWollen Sie diese wirklich löschen?")) {
				$("#form_error").show();
			}
			
			return false;
		} else {
			$("#form_error").hide();
			return true;
		}
	});
	
	$(".step3 input[name=artikel]").click(function(){
    	var currentprice = parseFloat($("h1.book-price").attr("price"));
    	var new_price = parseFloat($(this).attr("price"));
    	var old_price = $("h1.book-price").attr("old_price");
    	var preview_price = currentprice + new_price;
    	
    	var cartsum = $("#shoppingcartlogo .cartsum").attr("cartsum");
    	cartsum = parseFloat(cartsum) - parseFloat(old_price) + parseFloat(new_price);
    	$("#shoppingcartlogo .cartsum").html(formatPrice(roundNumber(cartsum, 2), "de"));
    	
    	$("h1.book-price").html(formatPrice(roundNumber(preview_price, 2), "de") + " EUR");
	});
	
	$(".step4 input[name=artikel]").click(function(){
	    /*var currentprice = parseFloat($("h1.book-price").attr("price"));
	    var price = parseFloat($(this).attr("price"));
	    var newprice = currentprice + price;
    	$("h1.book-price").html(formatPrice(roundNumber(newprice, 2), "de") + " EUR");
    	*/
    	var currentprice = parseFloat($("h1.book-price").attr("price"));
    	var new_price = parseFloat($(this).attr("price"));
    	var old_price = $("h1.book-price").attr("old_price");
    	var preview_price = currentprice + new_price;
    	
    	var cartsum = $("#shoppingcartlogo .cartsum").attr("cartsum");
    	cartsum = parseFloat(cartsum) - parseFloat(old_price) + parseFloat(new_price);
    	$("#shoppingcartlogo .cartsum").html(formatPrice(roundNumber(cartsum, 2), "de"));
    	
    	$("h1.book-price").html(formatPrice(roundNumber(preview_price, 2), "de") + " EUR");
	});
	
	$(".step6 input:checkbox").click(function(){
	    var addonprice = 0;
	    $("input:checkbox:checked").each(function(){
	        addonprice += parseFloat($(this).attr("price"));
	        if($(this).attr('id') == 'addon-artikel-0' && $('#addon_anzahl').val() == '0'){
	        	$('#addon_anzahl').val('1');
	        }
	    });
	    
	    if($('#addon-artikel-0').attr('checked')==false){
        	$('#addon_anzahl').val('0');
        }
	    
	    
	    var currentprice = parseFloat($(".step6 h1.book-price").attr("price"));
	    var preview_price = currentprice + addonprice;
	    if($(this).attr('id') == 'addon-artikel-0'){
	    	var number = $('#addon_anzahl').val();
	    	preview_price = currentprice + addonprice*number;
	    }
	    $("h1.book-price").attr("new_price", preview_price);
	    var new_price = parseFloat($("h1.book-price").attr("new_price"));
    	var old_price = parseFloat($("h1.book-price").attr("old_price"));
	    var delta = parseFloat(new_price - old_price);
	    var cartsum = $("#shoppingcartlogo .cartsum").attr("cartsum");
	    cartsum = parseFloat(cartsum) + delta;
	    $("#shoppingcartlogo .cartsum").html(formatPrice(roundNumber(cartsum, 2), "de"));
    	
	    $(".step6 h1.book-price").html(formatPrice(roundNumber(preview_price, 2), "de") + " EUR");
	    
	    
	});
	
	/* WINE */
	$(".wine #etiquette_tab_0").tabs();
	$(".wine #etiquette_tab_1").tabs();
	
	$('#addon_anzahl').keyup(function(){
		if($('#addon_anzahl').val() > 9 || $('#addon_anzahl').val() < 0){
			alert('Ansichtsexemplare: maximal 9');
			$('#addon_anzahl').val('9');
		}
		
		if($('#addon_anzahl').val() > 0 ){
			$('#addon-artikel-0').attr('checked', 'checked');
		}else{
			$('#addon-artikel-0').removeAttr('checked');
		}
		
		//caculator price
		var addonprice = 0;
		addonprice += parseFloat($('#addon-artikel-0').attr("price"));
		var currentprice = parseFloat($(".step6 h1.book-price").attr("price"));

	    var number = $('#addon_anzahl').val();
	    var	preview_price = currentprice + addonprice*number;

	    $("h1.book-price").attr("new_price", preview_price);
	    var new_price = parseFloat($("h1.book-price").attr("new_price"));
    	var old_price = parseFloat($("h1.book-price").attr("old_price"));
	    var delta = parseFloat(new_price - old_price);
	    var cartsum = $("#shoppingcartlogo .cartsum").attr("cartsum");
	    cartsum = parseFloat(cartsum) + delta;
	    $("#shoppingcartlogo .cartsum").html(formatPrice(roundNumber(cartsum, 2), "de"));
    	
	    $(".step6 h1.book-price").html(formatPrice(roundNumber(preview_price, 2), "de") + " EUR");
	    
	    
	    var price_tag = $('#addon-artikel-0').parent().parent().parent().children(".right").children(".addon-price");
		if($('#addon-artikel-0').is(":checked"))
			$(price_tag).html(parseFloat($('#addon-artikel-0').attr("price"))*number + " EUR");
		else
			$(price_tag).html("0.00 EUR");
		
		totalAddonPrice();
		
		
	});
	//$('#addon_anzahl').keyup();
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


