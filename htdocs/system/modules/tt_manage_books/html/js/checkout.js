$(document).ready(function(){
	$(".tabsdiv").each(function(){
		$(this).tabs();
	});
	
	$(".item_row input.n").keyup(function(){
		var number = parseInt($(this).val());
		number = isNaN(number) ? 1 : number;
		$(this).val(number)
		var price = parseFloat($(this).parent().children("input:hidden").val());
		var total = parseFloat(number * price);
		
		
		if (total.toFixed)
			total = total.toFixed(2);
		
		total = "" + total;
		total = total.replace(".", ",");
		$(this).parent().parent().children(".price_cell").children("span").html(total);
		
		var bookid = '#book-' + $(this).attr('bookid');
		$(this).parent().parent().parent().children().find('.geschen-price').each(function() {		
			var geschen = parseFloat($(this).val() / $(this).attr("pieces"));
			var geschental = parseFloat(number*geschen);
			if (geschental.toFixed)
				geschental = geschental.toFixed(2);
		 	geschental = "" + geschental;
			geschental=geschental.replace(".", ",");
			
			$(this).parent().parent().find('.geschen').html((geschental) +' EUR');
		});
		
		$(this).parent().parent().parent().children().children('.extra-price').each(function() {	
		    var extra = parseFloat($(this).val() / $(this).attr("pieces"));
		    var extratotal = parseFloat(number * extra);
		    if(extratotal.toFixed)
		        extratotal = extratotal.toFixed(2);
		    extratotal = "" + extratotal;
		    extratotal = extratotal.replace(".", ",");
		    
		    $(this).parent().children('.extra').html((extratotal) +' EUR');
		});
		
		$(this).parent().parent().parent().children().children().children().children('.extra-num').html(number);
		$(this).parent().parent().parent().children().children().children().children('.extra-num').attr("numaddon", number);
		
		$(this).parent().parent().parent().children().children().children().children('.geschen-num').html(number);
		$(this).parent().parent().parent().children().children().children().children('.geschen-num').attr("numaddon", number);
		var numaddon = 0;
		$('.numaddon').each(function(){
            numaddon += parseInt($(this).attr("numaddon"));
		});
		$(".sumaddoncart").html(numaddon);
		
		var numwine = 0;
		$(".numwine").each(function(){
            numwine += parseInt($(this).val());
		});
		$(".sumwinecart").html(numwine);
		
		var numbook = 0;
		$(".numbook").each(function(){
            numbook += parseInt($(this).val());
		});
		$(".sumbookcart").html(numbook);
		
		$("." + $(this).attr("id")).html($(this).val());
		
		total = total.replace(",", ".");
		total=parseFloat(total);
		
		var pricetotal = parseFloat(total);
		$(this).parent().parent().parent().children(".bookaddon").children(".price_cell").each(function(){
            var addon_price = $(this).text().replace(" EUR", "").replace(",", ".");
            addon_price = parseFloat(addon_price);
            pricetotal += addon_price;
		});
		
		$(this).parent().parent().parent().children(".bookextra").children(".price_cell").each(function(){
            var extra_price = $(this).text().replace(" EUR", "").replace(",", ".");
            extra_price = parseFloat(extra_price);
            pricetotal += extra_price;
		});
		
		if (pricetotal.toFixed)
			pricetotal = pricetotal.toFixed(2);
		
		$(bookid).attr("smallsum", pricetotal);
		pricetotal=""+pricetotal;
		pricetotal=pricetotal.replace(".", ",");
		$(bookid).html(pricetotal+" EUR");
		$("#overview-book-" + $(this).attr('bookid')).html(pricetotal+" EUR");
		
		var wineid = '#wine-' + $(this).attr('wineid');
		var winetotalprice = parseFloat(total);
		
		$(this).parent().parent().parent().children().children().children('.wineaddonprice').each(function(){
		    winetotalprice += parseFloat($(this).val());
		});
		
		$(wineid).attr("smallsum", winetotalprice);
		if (winetotalprice.toFixed)
			winetotalprice = winetotalprice.toFixed(2);
		
		winetotalprice = "" + winetotalprice;
		winetotalprice = winetotalprice.replace(".", ",");
		$(wineid).html(winetotalprice + " EUR");
		$("#overview-wine-" + $(this).attr("wineid")).html(winetotalprice + " EUR");
		
		var dynamic_sum = 0;
		$(".formfooter .smallsum").each(function(){
		    dynamic_sum += parseFloat($(this).attr("smallsum"));
		});
		
		var shipping_price = 0;
		if($(".shippingprice").length > 0)
		    shipping_price = parseFloat($(".shippingprice").val());
		
		var sum_inc_shipping = parseFloat(dynamic_sum + shipping_price);
		if(sum_inc_shipping.toFixed)
		    sum_inc_shipping = sum_inc_shipping.toFixed(2);
		
		sum_inc_shipping = "" + sum_inc_shipping;
		sum_inc_shipping = sum_inc_shipping.replace(".", ",");
		$("#shoppingcartlogo .cartsum").html(sum_inc_shipping);
		
		if(dynamic_sum.toFixed)
		    dynamic_sum = dynamic_sum.toFixed(2);
		
		dynamic_sum = dynamic_sum.replace(".", ",");
		$(".formheader .summary .price_cell strong").html(dynamic_sum + " EUR");
	});
	
	$("#billing_address-shipping_is_billing_address_").click(function(){
		if($(this).is(':checked'))
		{
			$("#billing_address").hide();
		}
		else
		{
			$("#billing_address").show();
		}
	});
	
	//Checkout 
	$("#checkout .step1 button[name=submitter]").click(function(){
		var isValid = true;
		var isValidTotal = true;
		$("#checkout .step1 input[name^=book-edit-]").each(function(){
			if($(this).val()=='false')
			{
				isValid = false;
				alert("Mindestens einer der Artikel in Ihrem Warenkorb hat:\n\n" +
					  "- keinen geänderten Untertitel/Textaufdruck und/oder\n" +
					  "- keine geänderten Personennamen (sondern vorgegebene Originalnamen)\n\n" +
					  "Bitte geben Sie diese ein, bevor Sie die Bestellung abschließen.");
				 
			}
		});
		
		$("#checkout .step1 input[name^=totalproduct-]").each(function(){
			var total_now  = parseInt($(this).parent().children(".numwine").val());
			var total_data = parseInt($(this).val());
			if(total_data < total_now)
			{
				isValidTotal = false;	
				alert("Mindestens einer der Artikel Wein in Ihrem Warenkorb hat:\n\n" +
						  "- Übertrifft die Anzahl der Etagen\n" +
						  "- Nur noch " + $(this).val() + " Stück auf Lager.\n\n" +
						  "Bitte geben Sie diese ein, bevor Sie die Bestellung abschließen.");
				$(this).parent().children(".numwine").focus();
			}
		});
		
		if( isValidTotal == false ){
			isValid = false;
				 
		}
		
		if(isValid==true){
			$("#step1").submit();
		}
			
	}); 
});
