<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

$GLOBALS['TL_LANG']['tl_module']['pn_booklang'] 	= array("Locale", "");
$GLOBALS['TL_LANG']['tl_module']['pn_type'] 		= array("Type", "");
$GLOBALS['TL_LANG']['tl_module']['pn_template'] 	= array("Template", "");
$GLOBALS['TL_LANG']['tl_module']['pn_image_height'] = array("Thumb height", "");
$GLOBALS['TL_LANG']['tl_module']['pn_image_shadow'] = array("Thumb shadow", "");
$GLOBALS['TL_LANG']['tl_module']['pn_genreid']      = array("Genre Id", "");
$GLOBALS['TL_LANG']['tl_module']['pn_booktitle']    = array("Book title end with", "");

/*
 * Field Type
 */
$GLOBALS['TL_LANG']['tl_module']['pn_type']['novels']				= "Romane";
$GLOBALS['TL_LANG']['tl_module']['pn_type']['children_books']		= "Kinderbücher";
$GLOBALS['TL_LANG']['tl_module']['pn_type']['gift_books']			= "Geschenkbücher";
$GLOBALS['TL_LANG']['tl_module']['pn_type']['classic_literature']	= "Literaturklassiker";
$GLOBALS['TL_LANG']['tl_module']['pn_type']['e_books']				= "E-Books";

$GLOBALS['TL_LANG']['tl_module']['teaser_step1_legend']     = "Step 1 teaser";
$GLOBALS['TL_LANG']['tl_module']['teaser_step2_legend']     = "Step 2 teaser";
$GLOBALS['TL_LANG']['tl_module']['teaser_step3_legend']     = "Step 3 teaser";
$GLOBALS['TL_LANG']['tl_module']['teaser_step4_legend']     = "Step 4 teaser";
$GLOBALS['TL_LANG']['tl_module']['teaser_step5_legend']     = "Step 5 teaser";
$GLOBALS['TL_LANG']['tl_module']['teaser_step6_legend']     = "Step 6 teaser";

$GLOBALS['TL_LANG']['tl_module']['step1_teaser']            = $GLOBALS['TL_LANG']['tl_module']['step2_teaser']
                                                            = $GLOBALS['TL_LANG']['tl_module']['step3_teaser']
                                                            = $GLOBALS['TL_LANG']['tl_module']['step4_teaser']
                                                            = $GLOBALS['TL_LANG']['tl_module']['step5_teaser']
                                                            = $GLOBALS['TL_LANG']['tl_module']['step6_teaser']
                                                            = array("Teaser","");
$GLOBALS['TL_LANG']['tl_module']['choose_image']					= 'Choose display random List';
$GLOBALS['TL_LANG']['tl_module']['pn_select_image']				= array('', '');

?>

