<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

$GLOBALS['TL_LANG']['FMD']['mod_manage_books']				= array('Personalnovel Module', '');
$GLOBALS['TL_LANG']['FMD']['book_listing'] 					= array('Personalnovel Listing', '');
$GLOBALS['TL_LANG']['FMD']['book_detail']						= array('Personalnovel Detail', '');
$GLOBALS['TL_LANG']['FMD']['wine_listing'] 					= array('Wines Listing', '');
$GLOBALS['TL_LANG']['FMD']['book_order']						= array('Personalnovel Order', '');
$GLOBALS['TL_LANG']['FMD']['book_checkout']					= array('Personalnovel Checkout', '');
$GLOBALS['TL_LANG']['FMD']['order_geschenkbox']					= array('Personalnovel Checkout Geschenkbox', '');
?>