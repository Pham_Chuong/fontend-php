<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

$GLOBALS['TL_LANG']['MOD']['book_listing']['one_person']			= 'Eine Person';
$GLOBALS['TL_LANG']['MOD']['book_listing']['num_person']			= 'bis zu %s Personen';
$GLOBALS['TL_LANG']['MOD']['book_listing']['more_about_book']		= 'Mehr über das Buch';
$GLOBALS['TL_LANG']['MOD']['book_listing']['Personalise_and_order']	= "Buch personalisieren und bestellen";
$GLOBALS['TL_LANG']['MOD']['book_listing']['Personalise_and_order_kalender']	= "Kalender personalisieren und bestellen";
$GLOBALS['TL_LANG']['MOD']['book_listing']['Personalise_and_download_ebook'] = "E-Book personalisieren und herunterladen";

$GLOBALS['TL_LANG']['CTE']['pnovelmodule']    					= array('Personalnovel Module', 'Includes a personalnovel module.');
$GLOBALS['TL_LANG']['MOD']['wine_listing']['price_per_bottle']	= '%s pro Flasche';
$GLOBALS['TL_LANG']['MOD']['wine_listing']['order']				= 'Bestellen';

?>