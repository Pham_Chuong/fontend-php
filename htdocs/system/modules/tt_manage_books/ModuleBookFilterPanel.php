<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class ModuleBookFilterPanel extends Module
{

	protected $strTemplate = 'mod_book_filter_panel';
	
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### BOOKS FILTER PANEL ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if (TL_MODE == 'FE') {
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/filter_panel.css|screen';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/filter_panel.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		$obj = $this->Database->prepare("Select * From tag_categories where class_id=2 order by category_pos")->execute();
		$objTags = $this->Database->prepare("select * from buch_tags2 b inner join tag_categories t on b.`category_id` = t.`category_id` where tag_active = 1")->execute();
		//$objPage = $this->Database->prepare("SELECT * FROM tl_page )->execute((is_numeric($pageId) ? $pageId : 0), $pageId);
		$arrTagItems = array();
		while($objTags->next()) {
			$arrTagItems[] = array(
				"id"			=> $objTags->tag_id,
				"value"			=> $objTags->tag_value,
				"value_en"		=> $objTags->tag_value_en,
				"category_id"	=> $objTags->category_id
			);
		}
		
		$arrTagCategories = array();
		while($obj->next()) {
			$arr				= array();
			$arr["id"]			= $obj->category_id;
			$arr["name"]		= $obj->category_name;
			$arr["name_en"]		= $obj->category_name_e;
			$arr["items"]		= array();
			
			foreach($arrTagItems as $item) {
				if($item["category_id"] == $arr["id"]) {
					$arr["items"][] = $item;
				}
			}
			
			$arrTagCategories[]	= $arr;
		}
		
		$this->Template->TagCategories = $arrTagCategories;
	}
	
}

?>