<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once("class/class.pn.php");

class ModuleBookDetail extends Module
{
	protected $strTemplate = 'mod_book_detail';
	public $pn_link_logo = '';
	public $pn_view_logo ='';
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### BOOKS DETAIL ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
			$template = array('mod_book_detail', 'mod_book_detail_backde', 'mod_book_notebook_detail', 'mod_book_detail_ebook');
			
			if(in_array($this->pn_template,$template))
				$this->strTemplate = $this->pn_template;
				
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/tabs.css|screen';
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.lightbox-0.5.css|screen';
			
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery-ui-1.8.16.custom.min.js';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.lightbox-0.5.pack.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/detail.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery.popupWindow.js';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.cycle.min.js';
			
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		//link logo for checkout
		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
		$link_logo = trim($this->pn_link_logo);
		$viewlogo  = $this->pn_view_logo;
		setcookie('urllogo', $link_logo."|".$viewlogo, time() + (86400 * 3), "/",$domain_cookie);
		
		$this->config = new PnConfig();
		
		$book_id = $this->Input->get("item") ? $this->Input->get("item") : $this->Input->get("items");
		
		$pn_book = new PnBook();
		//check black-Friday_2013
		if($GLOBALS['TL_CONFIG']['black_friday']){
			if($_GET['black-Friday-2013']){
				setcookie('black-Friday', $book_id, time() + (86400 * 3), "/",$domain_cookie);
			}
		}
		//Check if book exist
		$pn_book->initWithId($book_id);
		if(!$pn_book->id) gotoErrorPage();
		if(!$pn_book->public && $_GET["mode"] != "preview") gotoErrorPage();
		
		$pn_book->current($book_id);
		
		if($pn_book->id == "")
		    $pn_book->initWithId($book_id);
		
        $this->Template->book = $pn_book;
        
        //$pn_author = $pn_book->getAuthor();
        //$this->Template->author = $pn_author;
        
        $documents = $pn_book->getBookDocuments();
        $displayDocument = false;
        foreach($documents as $document)
        {
            if($document->readingSample)
                $displayDocument = true;
        }
        $href='';
        if( $pn_book->id != ""){
        	//custom
        	if($pn_book->public == '0'){
        		header("Location:/");
        	}
        	$title = standardize($pn_book->title);
        	$this->Template->url_cur 		= $this->getCurrentPageURL();//$this->getCurrent_url($pn_book->id,"book",standardize($pn_book->title));
        	
        	//facebook share
        	$this->Template->summary_face 	= urlencode(utf8_encode(html_entity_decode($pn_book->coverText_default)));
        	$this->Template->title_face 	= urlencode(preg_replace("/\s*-?\s*[FMW]\s*-\s*[FMW]\s*|\s*-?\s?[MW]\s*$/", "", $pn_book->title)." - PersonalNOVEL");
        	
        	//img book
        	$img_detail 					= $this->getUrlImage($pn_book->id,"detail" ,$pn_book->getPreviewUrl(array("shadow" => 5, "width" => 200)),standardize($pn_book->title));
        	$this->Template->img_face 		= $img_detail;
        	//$this->getUrlImage($pn_book->id,"face" ,$pn_book->getPreviewUrl(array("height" => 100)));
        	
        	//detail book
        	$GLOBALS['detail_book']=array();
        	$GLOBALS['detail_book']['urlWeb'] = $this->getCurrentPageURL();
        	$GLOBALS['detail_book']['urlImg'] = $this->getUrlImage($pn_book->id,"face" ,$pn_book->getPreviewUrl(array("shadow" => 1, "height" => 62)),standardize($pn_book->title));
        	$GLOBALS['objPage']->pageTitle 	  = $pn_book->title;
        	$GLOBALS['objPage']->description  = utf8_encode($pn_book->coverText_default);
        	
            $this->Template->img_book_detail  = $img_detail;
        	$this->Template->img_book_view 	  = $this->getUrlImage($pn_book->id,"view" ,$pn_book->getPreviewUrl(array("height" => 600)),standardize($pn_book->title));
        }
        $this->Template->url_domain		 ="http://".$domain_cookie;
        $this->Template->displayDocument = $displayDocument;
        $this->Template->documents       = $documents;
        $this->Template->entities        = $pn_book->getEntities();
        $this->Template->price	         = $pn_book->getLowestPrice();
        $this->Template->url_curback     = $this->getCurrentPageURL(); 
        $this->Template->reiter_namen    = $pn_book->getReiter();
        $this->Template->url_path        = "";//$this->getLastPathSegment($_SERVER['PHP_SELF']);
        $this->Template->book_public     = $pn_book->public;
        
        if($pn_book->isEbookLegallyAllowed())
            $this->Template->allowEbook = True;
	}
	
	
	public function getLastPathSegment($url) {
		$path = parse_url($url, PHP_URL_PATH); // to get the path from a whole URL
		$pathTrimmed = trim($path, '/'); // normalise with no leading or trailing slash
		$pathTokens = explode('/', $pathTrimmed); // get segments delimited by a slash
	
		if (substr($path, -1) !== '/') {
		    array_pop($pathTokens);
		}
		return end($pathTokens); // get the last segment
        }
        
	public function getCurrentPageURL() {
	    $pageURL = 'http';
	    if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}} 
	    $pageURL .= "://";
	    if ($_SERVER["SERVER_PORT"] != "80") {
	        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	    } else {
	        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	    }
	    return $pageURL;
	}  
	  
	public function getCurrent_url($id, $page, $name_book) {
		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
	    $pageURL = 'http';
	    if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}} 
	    $pageURL .= "://";
	    $pageURL .= $domain_cookie."/".$page."/".$id."/".$name_book;  
	    return $pageURL;
	} 
	  
	public function getUrlImage($id, $etx, $url_img,$title) {
		
		if(!in_array($etx, array('detail','view','face','list'))){
		$etx = "detail";
		}
		$check = true;
		$pageURL = 'http';
		if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
		
		$pageURL .= "://";
		$file_image = TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png";
		
		if(!is_file($file_image)){
			try {
				$file_headers = @get_headers($url_img);
				if($file_headers[0] != 'HTTP/1.1 404 Not Found' and $file_headers[0] != 'HTTP/1.1 500 INTERNAL SERVER ERROR'){
					copy($url_img."&".time(), TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png");
					}else{
						$check = false;
					}
			}
			catch (Exception $e) {
				$check = false;
			}
		}
		if($check != false)
		$url_img = $pageURL.$_SERVER["SERVER_NAME"]."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png";
		
		return $url_img;
	}
	
}

?>