<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/*
 * Pn Classes
 */
require_once("class/class.pn.php");

class ModuleWineOrder extends Module
{
	protected $strTemplate 		= 'mod_wine_order';
	protected $config;
	public    $domain;
    public $limitCopyImg = 12;
    public $flaLimit = 0;
	public function generate()
	{
	    $this->config = new PnConfig();
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### WINE ORDER ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/order.css|screen';
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.lightbox-0.5.css|screen';
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.alerts.css|screen';
			
			if($_GET['upload'])
			    $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery-1.9.1.min.js';
			else
			    $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery-ui-1.8.16.custom.min.js';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.lightbox-0.5.pack.js';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.preload-min.js';
			//$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.alerts.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery.Jcrop.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery.typing-0.2.0.min.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		$this->domain   = $GLOBALS['TL_CONFIG']['cookie_domain']? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
		 
		$article_id = $_GET['article'] ? $_GET['article'] : 0;
		$get_step = $_GET['step'] ? $_GET['step'] : 1;
		$step_id = $_POST['step'] ? $_POST['step'] : $get_step;
		$this->config = new PnConfig();
		
		$upload_dir           = $GLOBALS['TL_CONFIG']['customer_images_dir'];
		$upload_dir_frontend  = $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'];
		
		
		
		
		
		$this->Template->step = $step_id;
		if($_GET['upload'])
		{
		    $this->Template->upload = $_GET['upload'];
		    $item = new PnWineItem();
            $item->initWithId($_GET['wineitem']);
            $designitem = new PnItem();
            $designitem->initWithId($_GET['designitem']);
            
            $etiquette = new PnEtiquette();
            $etiquette->initWithId($item->etiquette_id);
            
            $etiquette_format = $etiquette->getFormat();
            $preview_dpi = intval(415 / mmtoinch($etiquette_format["width"]));
            $vcaspect = floatval($etiquette_format["height"]) / floatval($etiquette_format["width"]);
            $photo_x = mmtopx($etiquette->photo_x, $preview_dpi);
            $photo_y = mmtopx($etiquette->photo_y, $preview_dpi);
            $photo_w = mmtopx($etiquette->photo_w, $preview_dpi);
            $photo_h = mmtopx($etiquette->photo_h, $preview_dpi);
            $vcover_w = mmtopx($etiquette_format["width"], $preview_dpi);
            $vcover_h = $vcover_w * $vcaspect;
            $preview_overflow = 0;
            
            $this->Template->vcover_bg_url = $this->config->genericPreview("Etiquette", $item->etiquette_id, 
                array(
                    "fontface" => "Zapfino Extra LT",
                    "text" => "Ihr Name hier!", 
                    "width" => $vcover_w
                    )
                );
            
            $this->Template->vcover_w = $vcover_w;
            $this->Template->vcover_h = $vcover_h;
            $this->Template->mask_w   = $photo_w;
            $this->Template->mask_h   = $photo_h;
            $this->Template->mask_x   = $photo_x;
            $this->Template->mask_y   = $photo_y;
            $this->Template->aspect   = $etiquette->photo_w / $etiquette->photo_h;
            $this->Template->preview_overflow = $vcover_w;
            
            if($designitem->item_image1_present > 0) {
                $this->Template->isImageCached = true;
                if(file_exists($upload_dir_frontend . $_GET['order'] . "-" . $_GET['designitem'] . ".png.org.png"))
			    {
			        $imageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".png.org.png";
			        $cropImageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".png";
			    }
			    else if(file_exists($upload_dir_frontend . $_GET['order'] . "-" . $_GET['designitem'] . ".gif.org.gif"))
			    {
			        $imageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".gif.org.gif";
			        $cropimageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".gif";
			    }
			    else
			    {
			        $imageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".jpg.org.jpg";
			        $cropImageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".jpg";
			    }
			    $this->Template->imageCached    = $imageCached;
			    $this->Template->cropImageCached = $cropImageCached;
            }
		}
		
        if($step_id == 1)
        {
            //checkUrlToken();
            
            //$GLOBALS['TL_CONFIG']['customer_images_dir'] = "/var/www/Personalnovel/htdocs/tl_files/customer-images/";
            //Upload image
            if($_POST['uploader'] && $_GET['upload'] != "image")
		    {
		        $order = $this->getOrCreateOrder();
		        $wineitem = $this->getOrCreateWineItem($order->id);
		        $designitem = $this->getOrCreateDesignItem($order->id, $wineitem->id);
		        
		        $item_fields = array(
                                "text" => trim($_POST['var_text']),
                                "fontface" => $_POST['var_fontface'],
                                "fontcolor" => $_POST['var_fontcolor']
                            );
                    
                $wineitem->edit(
                            array
                            (
                                "item_fields" => json_encode($item_fields),
                                "etiquette_id" => $_POST['etiquette']
                            )
                        );
                
                $designitem->edit(
                            array
                            (
                                "artikel_id" => $_POST['design_item-artikel']
                            )
                        );
		        
                if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($order->id);
                    header("Location: /weinetiketten/order.html?step=1&upload=image&designitem=" . $designitem->id . "&wineitem=" . $wineitem->id . "&order=" . $order->id . "&token=". $token);
                }
                else
                {
                    header("Location: /weinetiketten/order.html?step=1&upload=image&designitem=" . $designitem->id . "&wineitem=" . $wineitem->id . "&order=" . $order->id);
                }
		        
		    }
		    
		    //Crop Image
			if($_POST['cropimage'])
			{
			    $upload_filename = "";
			    $upload_filename = $_POST['upload_filename'];
			    $order_id = $_GET['order'];
			    $design_id = $_GET['designitem'];
			    
			    if(file_exists($upload_dir_frontend . $order_id . "-" . $design_id . ".png.org.png"))
			    {
			        $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".png.org.png";
			    }
			    else if(file_exists($upload_dir_frontend . $order_id . "-" . $design_id . ".gif.org.gif"))
			    {
			        $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".gif.org.gif";
			    }
			    else
			    {
			        $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".jpg.org.jpg";
			    }
			    
			    $crop_filename   = $upload_dir . $order_id . "-" . $design_id . ".png";
                $crop_filename_frontend = $upload_dir_frontend . $order_id . "-" . $design_id . ".png";
			    
			    $filename = pathinfo($upload_filename);
			    $extension = $filename['extension'];
			    $imagesize = getimagesize($upload_filename);
			    $imagew    = $imagesize[0];
			    $imageh    = $imagesize[1];
			    
			    $targ_h = $_POST['targ_h'];
			    $targ_w = $_POST['targ_w'];
			    if($imagew > $imageh)
			    {
			        $targ_w = $imageh / $targ_h * $targ_w;
			        $targ_h = $imageh;
			    } else {
			        $targ_h = $imagew / $targ_w * $targ_h;
			        $targ_w = $imagew;
			    }
			    
			    //$quality = 90;
			    $src = $upload_filename;
			    $dst = $crop_filename;
			    switch($extension)
			    {
                    case "png":
                        $img_r = imagecreatefrompng($src);
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                            $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                        imagepng($dst_r, $dst);
                        break;
                    case "gif":
                        $img_r = imagecreatefromgif($src);
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                            $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                        imagepng($dst_r, $dst);
                        break;
                    default:
                        $img_r = imagecreatefromjpeg($src);
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                            $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                        
                        imagepng($dst_r, $dst);
                        break;
			    }
			    
			    copy($dst, $crop_filename_frontend);
			    
			    if($_GET["edit"] == 1)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken( $_GET['order']);
                        header("Location: /checkout.html?order=" . $_GET['order'] . "&token=" . $token);
                    }
                    else
                    {
                        header("Location: /checkout.html?order=" . $_GET['order']);
                    }
			        
			    }
			    else
			    {
				    if($_GET["fedit"] == 1)
				    {
				        if($GLOBALS['TL_CONFIG']['use_url_token'])
	                    {
	                        $token = generateToken( $_GET['order']);
	                        header("Location: /myorder?id=" . $_GET['order'] . "&token=" . $token);
	                    }
	                    else
	                    {
	                        header("Location: /myorder?id=" . $_GET['order']);
	                    }
				        
				    }else{
				        if($GLOBALS['TL_CONFIG']['use_url_token'])
	                    {
	                        $token = generateToken($_GET['order']);
	                        header("Location: /weinetiketten/order.html?step=1&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order'] . "&token=" . $token);
	                    }
	                    else
	                    {
	                        header("Location: /weinetiketten/order.html?step=1&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order']);
	                    }
				    }
			        
			    }
			}
		    
		    //Upload image
		    if($_POST['uploader'] && $_GET['upload'] == "image")
		    {
		        $upload_dir = $GLOBALS['TL_CONFIG']['customer_images_dir'] ? $GLOBALS['TL_CONFIG']['customer_images_dir'] : "/var/www/production/shared/customer-images/";
		        $file = str_replace(' ', '_', $_FILES['imagefile']['name']);
                $permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
                
                $order_id = $_GET['order'];
                $item_id = $_GET['designitem'];
                
                $file_extension = ".png.org.png";
                $file_crop_extension = ".png";
                if($_FILES['imagefile']['type'] == 'image/gif')
                {
                    $file_extension = ".gif.org.gif";
                }
                if($_FILES['imagefile']['type'] == 'image/jpeg' || $_FILES['imagefile']['type'] == 'image/pjpeg')
                {
                    $file_extension = ".jpg.org.jpg";
                }
                
                $file = $order_id . "-" . $item_id . $file_extension;
                $cropped_file = $order_id . "-" . $item_id . $file_crop_extension;
                if (in_array($_FILES['imagefile']['type'], $permitted))
                {
                    //Customer images
                    $cachefiles = glob($upload_dir . $_GET['order'] . "-" . $_GET['designitem'] . "*");
                    array_walk($cachefiles, function ($cachefile) {
                        unlink($cachefile);
                    });
                    
                    $cachefiles = glob($upload_dir_frontend . $_GET['order'] . "-" . $_GET['designitem'] . "*");
                    array_walk($cachefiles, function ($cachefile) {
                        unlink($cachefile);
                    });
                    
                    //Upload to folder print
                    if(move_uploaded_file($_FILES['imagefile']['tmp_name'], $upload_dir_frontend . $file))
                    {
                        //Default crop file
                        $filename  = pathinfo($upload_dir_frontend . $file);
                        $extension = $filename['extension'];
                        
                        $targ_h = $_POST['targ_h'];
                        $targ_w = $_POST['targ_w'];
                        $crop_x = 0;
                        $crop_w = $targ_w;
                        $crop_y = 0;
                        $crop_h = $targ_h;
                        
                        switch($extension)
                        {
                            case "png":
                                $img_r = imagecreatefrompng($upload_dir_frontend . $file);
                                break;
                            case "gif":
                                $img_r = imagecreatefromgif($upload_dir_frontend . $file);
                                break;
                            default:
                                $img_r = imagecreatefromjpeg($upload_dir_frontend . $file);
                                break;
                        }
                        
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r, $img_r, 0, 0, $crop_x, $crop_y, $targ_w, $targ_h, $crop_w, $crop_h);
                        imagepng($dst_r, $upload_dir . $cropped_file);
                        
                        copy($upload_dir . $cropped_file, $upload_dir_frontend . $cropped_file);
                    }
                    
                    $item = new PnItem();
                    $item->select($item_id);
                    $item->edit(
                            array (
                               "item_image1_present"  => 1,
                               "item_image1_cropped"  => 1
                            )
                        );
                    
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET['order']);
                        header("Location: /weinetiketten/order.html?step=1&upload=image&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order'] . "&token=" . $token);
                    }
                    else
                    {
                        header("Location: /weinetiketten/order.html?step=1&upload=image&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order']);
                    }
                    
                }
		    }
		    
		    if($_POST['back'])
		    {
		        if($_GET['edit'] == 1)
		        {
		            if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET['order']);
                        header("Location: /checkout.html?order=" . $_GET['order'] . "&token=" . $token);
                    }
                    else
                    {
                        header("Location: /checkout.html?order=" . $_GET['order']);
                    }
		            
		        }
		        else
		        {
			        if($_GET['fedit'] == 1)
			        {
			            if($GLOBALS['TL_CONFIG']['use_url_token'])
	                    {
	                        $token = generateToken($_GET['order']);
	                        header("Location: /myorder?id=" . $_GET['order'] . "&token=" . $token);
	                    }
	                    else
	                    {
	                        header("Location: /myorder?id=" . $_GET['order']);
	                    }
			            
			        }else{
			            if($GLOBALS['TL_CONFIG']['use_url_token'])
	                    {
	                        $token = generateToken($_GET['order']);
	                        header("Location: /weinetiketten/order.html?step=1&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order'] . "&token=" . $token);
	                    }
	                    else
	                    {
	                        header("Location: /weinetiketten/order.html?step=1&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order']);
	                    }
			        }
		        }
		    }
		    
		    if($_POST['backtoetikettt'])
		    {
		        if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($_GET['order']);
                    header("Location: /weinetiketten/order.html?step=1&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order'] . "&token=" . $token);
                }
                else
                {
                    header("Location: /weinetiketten/order.html?step=1&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order']);
                }
		        
		    }
		    
		    if($_POST['submitter'])
		    {
		        $order = $this->getOrCreateOrder();
                $wineitem = $this->getOrCreateWineItem($order->id);
                $designitem = $this->getOrCreateDesignItem($order->id, $wineitem->id);
		        
		        if($_GET["order"] && $_GET["wineitem"])
		        {
		            //Edit etiquette, font, text
		            $item_fields = array(
                                "text" => $_POST['var_text'],
                                "fontface" => $_POST['var_fontface'],
                                "fontcolor" => $_POST['var_fontcolor']
                            );
                    
                    $wineitem->edit(
                                array
                                (
                                    "item_fields" => json_encode($item_fields),
                                    "etiquette_id" => $_POST['etiquette']
                                )
                            );
                    
                    $designitem->edit(
                                array
                                (
                                    "artikel_id" => $_POST['design_item-artikel']
                                )
                            );
		        }
		        
		        if($_GET['edit'] == 1)
		        {
		            if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET['order']);
                        header("Location: /checkout.html?order=" . $_GET['order'] . "&token=" . $token);
                    }
                    else
                    {
                        header("Location: /checkout.html?order=" . $_GET['order']);
                    }
		            
		        }
		        else
		        {
			        if($_GET['fedit'] == 1)
			        {
			            if($GLOBALS['TL_CONFIG']['use_url_token'])
	                    {
	                        $token = generateToken($_GET['order']);
	                        header("Location: /myorder?id=" . $_GET['order'] . "&token=" . $token);
	                    }
	                    else
	                    {
	                        header("Location: /myorder?id=" . $_GET['order']);
	                    }
			            
			        }else{
			            if($GLOBALS['TL_CONFIG']['use_url_token'])
	                    {
	                        $token = generateToken($_GET['order']);
	                        header("Location: /weinetiketten/order.html?step=2&wineitem=" . $wineitem->id . "&designitem=". $designitem->id . "&order=" . $order->id . "&token=" . $token);
	                    }
	                    else
	                    {
	                        header("Location: /weinetiketten/order.html?step=2&wineitem=" . $wineitem->id . "&designitem=". $designitem->id . "&order=" . $order->id);
	                    }
			        }
		            
		        }
		    }
            
		    if($_GET['wineitem'])
            {
                $item = new PnWineItem();
                $item->initWithId($_GET['wineitem']);
                $article_id = $item->article_id;
                $designItem = $item->getDesignItem();
                
                $this->Template->selectedDesign = $designItem->article_id;
                $this->Template->selectedEtiquette = $item->etiquette_id;
                $itemfields = json_decode($item->fields);
                
                $this->Template->selectedFont = $itemfields->fontface;
                $this->Template->selectedText = $itemfields->text;
                $this->Template->selectedColor = $itemfields->fontcolor;
                
                $this->Template->wineitem_id = $_GET['wineitem'];
                $this->Template->config      = $this->config;
                
            }
            
            $articles = $this->Database->prepare("
                SELECT finanz_artikel.artikel_id as artikel_id, finanz_artikel.artikel_name as artikel_name,
                    prices.price_amount as price_amount
                FROM " . $this->config->database . ".artikel_artikel AS artikel_artikel_1, 
                " . $this->config->database . ".finanz_artikel JOIN " . $this->config->database . ".prices 
                ON finanz_artikel.artikel_id = prices.artikel_id 
                where finanz_artikel.mandant_id = 1
                AND finanz_artikel.artikel_active = 1 
                AND (finanz_artikel.artikel_validuntil IS NULL OR finanz_artikel.artikel_validuntil > now()) 
                AND artikel_artikel_1.child_id = finanz_artikel.artikel_id 
                AND artikel_artikel_1.parent_id = ?
                AND finanz_artikel.class_id IN (18)
                ORDER BY prices.price_amount ASC
                ")->execute($article_id);
                
            $designItems = array();
            while ($articles->next())
            {
                $item = new PnArticle();
                $item->id = $articles->artikel_id;
                $item->name = $articles->artikel_name;
                $item->price = $articles->price_amount;
                $designItems[] = $item;
            }
            
            $fonts = array(
                array(
                    "value" => "Century Schoolbook L",
                    "image" => "/system/modules/tt_manage_books/html/img/Schriftbeispiel_Century_Schoolbook_L.png"
                ),
                array(
                    "value" => "Zapfino Extra LT",
                    "image" => "/system/modules/tt_manage_books/html/img/Schriftbeispiel_Zapfino_Extra_LT.png"
                ),
                array(
                    "value" => "FreestyleScriptEF-Reg",
                    "image" => "/system/modules/tt_manage_books/html/img/Schriftbeispiel_FreestyleScriptEF-Reg.png"
                ),
                array(
                    "value" => "English157TT BT",
                    "image" => "/system/modules/tt_manage_books/html/img/Schriftbeispiel_English157TT_BT.png"
                ),
                array(
                    "value" => "Lucida Sans",
                    "image" => "/system/modules/tt_manage_books/html/img/Schriftbeispiel_Lucida_Sans.png"
                ),
                array(
                    "value" => "Kabel Book/Medium",
                    "image" => "/system/modules/tt_manage_books/html/img/Schriftbeispiel_Kabel_Book-Medium.png"
                )
            );
            
            $colors = array(
                array(
                    "color"     => "black",
                    "code"      => "black",
                    "display"   => true
                ),
                array(
                    "color"     => "darkred",
                    "code"      => "darkred",
                    "display"   => true
                ),
                array(
                    "color"     => "darkgreen",
                    "code"      => "darkgreen",
                    "display"   => true
                ),
                array(
                    "color"     => "blue",
                    "code"      => "blue",
                    "display"   => true
                ),
                array(
                    "color"     => "darkmagenta",
                    "code"      => "darkmagenta",
                    "display"   => true
                ),
                array(
                    "color"     => "white",
                    "code"      => "white",
                    "display"   => false
                ),
                array(
                    "color"     => "B3B3B3",
                    "code"      => "#B3B3B3",
                    "display"   => false
                ),
                array(
                    "color"     => "585858",
                    "code"      => "#585858",
                    "display"   => true
                ),
                array(
                    "color"     => "434343",
                    "code"      => "#434343",
                    "display"   => true
                ),
                array(
                    "color"     => "gold",
                    "code"      => "gold",
                    "display"   => false
                )
            );
            
            $article = new PnArticle();
            $article->initWithId($article_id);
            
            $object = $this->Database->prepare("
				SELECT a.asset_filename, a.asset_id FROM " . $this->config->database . ".assets a
				LEFT JOIN " . $this->config->database . ".extra_assets ex
				ON a.asset_id = ex.asset_id
				WHERE ex.artikel_id = ? AND a.asset_active = 1
				AND a.asset_type_id = 37
			")->execute($article_id);
			
			$image_filename = "";
			while($object->next())
			{
			    $image_filename = $object->asset_filename;
			}
			
			$etiquettes = $article->getEtiquettes();
			$list_etiquettes = array();
			foreach($etiquettes as $etiquette)
			{
			    $list_etiquettes[$etiquette->photo_allowed][$etiquette->getTagName()][] = $etiquette;
			}
			
            $image = $this->config->makeThumnail($image_filename,"", 768);
            /* fix bug server tam thoi */
            $thumb_url 			= "/thumbs/";
            $thumb_path			= implode("/", str_split(substr(str_replace(".", "", $image_filename), 0, 3)));
            $thumbnail_tag		= sprintf("h%s", 768);
            
            $thumbnail_name 	= $thumbnail_tag . "-" . md5("nilfEvdupjevOdMacKihikGedTymFemhebliesfabOjibyekew" . $thumbnail_tag . $image_filename) . "-" .$image_filename;
            $thumb_url	   	   .= $thumb_path . "/" .$thumbnail_name;
            //$image              = sprintf("https://pnovel.net" . "%s", $thumb_url);  
            
            $preview_url    = $GLOBALS['TL_CONFIG']['image_root'] . $thumb_url;
            $preview_url = $this->getImageUrl($article_id ,"detail",$preview_url, standardize($image_filename));
            
            /* remove code tren neu fix xong server */
            
            $this->Template->designItems = $designItems;
            $this->Template->fonts = $fonts;
            $this->Template->colors = $colors;
            $this->Template->image = $preview_url;
            $this->Template->etiquettes = $list_etiquettes;
            
            if($_GET['designitem'] && !$_GET['upload'])
            {
                $designItem = new PnItem();
                $designItem->initWithId($_GET['designitem']);
                if($designItem->item_image1_present > 0) {
                    $this->Template->isImageCached = true;
                    if(file_exists($GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] . $_GET['order'] . "-" . $_GET['designitem'] . ".png.org.png"))
                    {
                        $imageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".png.org.png";
                        //$cropImageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".png";
                    }
                    else if(file_exists($GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] . $_GET['order'] . "-" . $_GET['designitem'] . ".gif.org.gif"))
                    {
                        $imageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".gif.org.gif";
                        //$cropimageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".gif";
                    }
                    else
                    {
                        $imageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . "jpg.org.jpg";
                        //$cropImageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".jpg";
                    }
                    
                    $cropImageCached = "/tl_files/customer-images/" . $_GET['order'] . "-" . $_GET['designitem'] . ".png";
                    
                    $this->Template->imageCached    = $imageCached;
                    $this->Template->cropImageCached = $cropImageCached;
                    
                    $etiquette = new PnEtiquette();
                    $item = new PnWineItem();
                    $item->initWithId($_GET['wineitem']);
                    $etiquette->initWithId($item->etiquette_id);
                    
                    $etiquette_format = $etiquette->getFormat();
                    $preview_dpi = floatval(220 / mmtoinch($etiquette_format["width"]));
                    $photo_x = mmtopx($etiquette->photo_x, $preview_dpi);
                    $photo_y = mmtopx($etiquette->photo_y, $preview_dpi);
                    $photo_w = mmtopx($etiquette->photo_w, $preview_dpi);
                    $photo_h = mmtopx($etiquette->photo_h, $preview_dpi);
                    
                    $this->Template->mask_w   = $photo_w;
                    $this->Template->mask_h   = $photo_h;
                    $this->Template->mask_x   = $photo_x;
                    $this->Template->mask_y   = $photo_y;
                }
            }
            
            $this->Template->wineArtikelPreview = $this->config->genericPreview("WineArtikel", $article_id, array("width" => 220));
            /* Fix bug server tam thoi */
            $preview_url = sprintf($GLOBALS['TL_CONFIG']['image_root'] . "/preview/%s/%s/%s.png", "WineArtikel", $article_id, 
                            md5("nilfEvdupjevOdMacKihikGedTymFemhebliesfabOjibyekew" . "WineArtikel" . $article_id));
        
            $this->Template->wineArtikelPreview = $preview_url . "?width=220";
            /* remove code tren neu fix xong server */
        }
        
        if($step_id == 2)
        {
            if($_POST['buy'])
            {
                $article_id = $_POST['artikel'];
                $item = new PnItem();
                $item_id = $item->add(
                    array
                        (
                            "order_id"          => $_GET['order'],
                            "artikel_id"        => $article_id,
                            "class_id"          => 15,
                            "item_pieces"       => $_POST['quantity'],
                            "item_fields"       => "{}",
                            "parent_id"         => $_GET['wineitem'],
                            "item_created"      => date('Y/m/d H:i:s a', time()),
                            "item_updated"      => date('Y/m/d H:i:s a', time()),
                            "item_touched"      => date('Y/m/d H:i:s a', time())
                        )
                    );
                header('Location: '.$_SERVER['REQUEST_URI']);
            }
            
            if($_POST['edit'])
            {
                $item = new PnItem();
                $item->initWithId($_POST['itemid']);
                if($item->item_paper != 99){
					$item_id = $item->edit(
						array
							(
								"item_pieces" => $_POST['quantity'],
							)
						);
				}
                header('Location: '.$_SERVER['REQUEST_URI']);
            }
            
            if($_POST['delete'])
            {
                $item = new PnItem();
                $item->initWithId($_POST['itemid']);
                if($item->item_paper != 99){
					$item->removeChildItem();
					$item->remove();
                }
                header('Location: '.$_SERVER['REQUEST_URI']);
            }
            
            if($_POST['submitter'])
            {
                if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($_GET['order']);
                    header("Location: /checkout.html?order=" . $_GET['order'] . "&token=" . $token);
                }
                else
                {
                    header("Location: /checkout.html?order=" . $_GET['order']);
                }
                
            }
            
            if($_POST['engraving'])
            {
                if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($_GET['order']);
                    header("Location: /weinetiketten/order.html?step=engraving&wineitem=" . $_GET['wineitem'] 
                    . "&designitem=" . $_GET['designitem'] . "&order=" . $_GET['order']
                    . "&wineboxitem=" . $_POST['itemid'] . "&gravurarticle=" . $_POST['gravurarticle'] . "&token=" . $token);
                }
                else
                {
                    header("Location: /weinetiketten/order.html?step=engraving&wineitem=" . $_GET['wineitem'] 
                    . "&designitem=" . $_GET['designitem'] . "&order=" . $_GET['order']
                    . "&wineboxitem=" . $_POST['itemid'] . "&gravurarticle=" . $_POST['gravurarticle']);
                }
                
            }
            
            $wineitem = $_GET['wineitem'];
            
            $objDb = $this->Database->prepare("
                    SELECT * FROM " . $this->config->database . ".items
                    WHERE class_id = 15
                    AND parent_id = ?
                ")->execute($wineitem);
            
            $list_addons = array();
            while($objDb->next())
            {
                $wineboxitem = $objDb->item_id;
                $addon = array(
                    "item_id"   => $objDb->item_id,
                    "article_id" => $objDb->artikel_id,
                    "quantity"  => $objDb->item_pieces
                );
                $list_addons[] = $addon;
            }
            
            $this->Template->addons = $list_addons;
            $wineitem = new PnWineItem();
            $wineitem->initWithId($_GET['wineitem']);
            $article = new PnArticle();
            $article->initWithId($wineitem->article_id);
            $this->Template->children = $article->getAddons();
            $geschenkbox = $wineitem->item_paper==99?true:false;
            $this->Template->geschenkbox = $geschenkbox;
            
            $config = new PnConfig();
            $this->Template->config = $config;
            if(count($list_addons) > 0)
                $this->Template->preview_url = $this->config->genericPreview("WineBoxItem", $wineboxitem, array());
        }
        
        if($step_id == "engraving")
        {
            $wineboxitem = $_GET['wineboxitem'];
            $gravurarticle = $_GET['gravurarticle'] ? $_GET['gravurarticle'] : 14644;
            
            $item = new PnItem();
            $item_cached = $this->Database->prepare("
                    SELECT * FROM " . $this->config->database . ".items
                    WHERE parent_id = ?
                ")->execute($wineboxitem);
			//cant submit
            if($_POST['submitter-cant'])
            { 
	            if($item_cached->numRows > 0)
	            {
	                $itemid = 0;
	                while($item_cached->next())
	                {
	                    $itemid = $item_cached->item_id;
	                }
	                $item->initWithId($itemid);
	                $item->removeChildItem();
                	$item->remove();
	            }
	            
	            if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($_GET['order']);
                    header("Location: /weinetiketten/order.html?step=2&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order'] . "&token=" . $token);
                }
                else
                {
                    header("Location: /weinetiketten/order.html?step=2&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order']);
                }
	          
            } 
            //stop cant submit      
            if($item_cached->numRows > 0)
            {
                $itemid = 0;
                while($item_cached->next())
                {
                    $itemid = $item_cached->item_id;
                }
                
                $item->initWithId($itemid);
                $fields = json_decode($item->fields);
                $this->Template->text = $fields->text;
            }
            
            if($_POST['var_text'] && $_POST['var_text'] != "")
            {
                if($item_cached->numRows > 0)
                {
                    $item->edit(array(
                            "item_fields" => json_encode(array("text" => trim($_POST['var_text'])))
                        ));
                } 
                else
                {
                    $item->add(array(
                            "order_id"      => $_GET['order'],
                            "class_id"      => 20,
                            "parent_id"     => $wineboxitem,
                            "artikel_id"    => $gravurarticle,
                            "item_fields"   => json_encode(array("text" => trim($_POST['var_text']))),
                            "item_pieces"   => 1
                        ));
                }
            }
            else if($_POST)
            {
                if($item_cached->numRows > 0)
                {
                    $item->remove();
                }
            }
            
            if($_POST['submitter'])
            {
                if($_GET['edit'] == 1)
		        {
		            if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET['order']);
                        header("Location: /checkout.html?order=" . $_GET['order'] . "&token=" . $token);
                    }
                    else
                    {
                        header("Location: /checkout.html?order=" . $_GET['order']);
                    }
		            
		        }
		        else
		        {
		            if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET['order']);
                        header("Location: /weinetiketten/order.html?step=2&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order'] . "&token=" . $token);
                    }
                    else
                    {
                        header("Location: /weinetiketten/order.html?step=2&wineitem=" . $_GET['wineitem'] . "&designitem=". $_GET['designitem'] . "&order=" . $_GET['order']);
                    }
		            
                }
            }
            
            $this->Template->preview_url = $this->config->genericPreview("WineBoxItem", $wineboxitem, array());
        }
	}
	
	protected function editOrCreateAddon($article_id)
	{
	    //$article = new PnArticle
	    //later
	}
	
	protected function getOrCreateOrder()
	{
	    $order = new PnOrder();
	    
	    if($_COOKIE['PN_order_id'] != '' && $_COOKIE['PN_order_id'] != 0)
		{
			$order->initWithId($_COOKIE['PN_order_id']);
			if(count($order->getItems()) > 0){
				$bookItems   = $order->getBookItems();
				if(count($bookItems) > 0){
					foreach($bookItems as $bookItem){
						if($bookItem->item_paper == 99){
							header(sprintf("Location: /%s", $this->redirectJumpTo($this->jumpTo)));
							exit();
						}
					}
				}
				$wineItems   = $order->getWineItems();
				if(count($wineItems) > 0){
					foreach($wineItems as $wineItem){
						if($wineItem->item_paper == 99){
							header(sprintf("Location: /%s", $this->redirectJumpTo($this->jumpTo)));
							exit();
						}
					}
				}
			}
		}
		
		if($order->id == '' || $order->id == 0)
		{
			$order_id = $order->add(array(
								'debitor_id'          => 1,
								'order_invoice'       => 1,
								'order_status'        => 0,
					            'order_versand'       => 'Tracked',
								'label_id'	          => 'pn',
								'abtest_id'           => 1,
								'order_converted'     => 1,
								'order_payments_converted' => 0,
                                'ordertype_id'	      => 1,
                                'mandant_id'	      => 1,
                                "konto_id"            => $order->createKonto()->id,
                                'mahn_count'	      => 1,
                                'order_step'	      => 'first_step',
                                'locale_id'		      => 1,
                                'partner_id'	      => 1,
                                'order_version'	      => 0,
								'order_ok2go_manuell' => 0,
								"order_date"          => date('Y/m/d H:i:s a', time()),
                                "order_created"       => date('Y/m/d H:i:s a', time()),
                                "order_updated"       => date('Y/m/d H:i:s a', time())
							));
			
			setcookie('PN_order_id', $order_id, time() + (86400 * 7), "/",$this->domain);
			$order->initWithId($order_id);
		    $order->edit(array("konto_id" => $order->createKonto()->id));
		    $order->createRandomKey();
		}
	    
	    return $order;
	}
	
	protected function getOrCreateDesignItem($order_id, $parent_id)
	{
	    $item = new PnItem();
	    if($_GET["designitem"])
	    {
	        $item->initWithId($_GET["designitem"]);
	    }
	    else if($_POST["design_item-artikel"])
	    {
	        $item_id = $item->add(
                    array(
                        "order_id" => $order_id,
                        "class_id" => 18,
                        "artikel_id" => $_POST["design_item-artikel"],
                        "parent_id" => $parent_id,
                        "item_pieces" => 1,
                        "item_print" => 0,
                        "item_fields" => "{}",
                        "item_image1_present" => 0,
                        "item_image1_cropped" => 0,
                        "order_flow_id" => 20
                    )
	            );
	        
	        $item->select($item_id);
	    }
	    
	    return $item;
	}
	
	protected function getOrCreateWineItem($order_id)
	{
	    $item = new PnItem();
	    if($_GET["wineitem"])
	    {
	        $item->initWithId($_GET["wineitem"]);
	    }
	    else if($_GET["article"])
	    {
	        $item_fields = array(
                                "text" => $_POST['var_text'],
                                "fontface" => $_POST['var_fontface'],
                                "fontcolor" => $_POST['var_fontcolor']
                            );
            
	        $item_id = $item->add(
                    array(
                        "order_id" => $order_id,
                        "class_id" => 14,
                        "artikel_id" => $_GET["article"],
                        "item_pieces" => 1,
                        "item_print" => 0,
                        "item_fields" => json_encode($item_fields),
                        "etiquette_id" => $_POST['etiquette'],
                        "item_image1_present" => 0,
                        "item_image1_cropped" => 0,
                        "order_flow_id" => 20
                    )
	            );
	        
	        $item->select($item_id);
	    }
	    
	    return $item;
	}
	public function redirectJumpTo($jumpto)
	{
		$strRedirect = "/";
		if (strlen($jumpto))
		{
			$objNextPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=?")
										  ->limit(1)
										  ->execute($jumpto);
			if ($objNextPage->numRows)
			{
				$strRedirect = $this->generateFrontendUrl($objNextPage->fetchAssoc());
			}
		}
		return $strRedirect;
	}
    
    //Get thumbnail
    public function getImageUrl($id, $etx, $url_img, $title) {
        if($this->flaLimit != $this->limitCopyImg){
            
            if(!in_array($etx, array('detail','view','list'))){
                $etx = "detail";
            }
            $check = true;
            $pageURL = 'http';
            
            if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
            
            $pageURL .= "://";
            $file_image = TL_ROOT."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png";
            
            if(!is_file($file_image)){
                try{
                $file_headers = @get_headers($url_img);
                if($file_headers[0] != 'HTTP/1.1 404 Not Found' and $file_headers[0] != 'HTTP/1.1 500 INTERNAL SERVER ERROR')
                    copy($url_img, TL_ROOT."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png");
                    else{
                        $check = false;
                    }
                $this->flaLimit = $this->flaLimit + 1;
                }catch (Exception $e) {
                    $check = false;
                }
            }
            if($check != false )
            $url_img = $pageURL.$_SERVER["SERVER_NAME"]."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png";
            
            return $url_img;
        }
        return $url_img;
    }
}
