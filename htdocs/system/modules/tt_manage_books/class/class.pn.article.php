<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnArticle extends Controller
{
	public $id;
	public $name;
	public $name_en;
	public $shortname;
	public $productions_day;
	public $version_id;
	public $class_id;
	public $class_name;
	public $pieces_from_parent;
	public $book_id;
	public $giftbox_id;
	public $description;
	public $description_en;
	public $image_asset_id;
	public $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function select($id)
	{
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".finanz_artikel WHERE artikel_id = ?
				")->execute($id);
		
		while($object->next())
		{
			$this->id	= $object->artikel_id;
			$this->book_id = $object->buch_id;
			$this->design_id = $object->design_id;
			$this->version_id = $object->version_id;
			$this->class_id = $object->class_id;
			$this->name	= $object->artikel_name;
			$this->name_en = $object->artikel_name_en;
			$this->description = $object->artikel_desc;
			$this->description_en = $object->artikel_desc_en;
			$this->image_asset_id = $object->image_asset_id;
			
		}
	}
	
	public function initWithId($id)
	{
		$object = $this->Database->prepare("
					SELECT fa.*, ak.class_singular, ak.class_pieces_from_parent FROM " . $this->config->database . ".finanz_artikel fa
					LEFT JOIN " . $this->config->database . ".artikel_klassen ak
					ON fa.class_id = ak.class_id
					WHERE artikel_id = ?
				")->execute($id);
		
		while($object->next())
		{
			$this->id					= $object->artikel_id;
			$this->book_id 				= $object->buch_id;
			$this->design_id 			= $object->design_id;
			$this->version_id 			= $object->version_id;
			$this->class_id 			= $object->class_id;
			$this->giftbox_id           = $object->giftbox_id;
			$this->class_name 			= $object->class_singular;
			$this->konto_id             = $object->konto_id;
			$this->pieces_from_parent 	= $object->class_pieces_from_parent;
			$this->name					= $object->artikel_name;
			$this->name_en 				= $object->artikel_name_en;
			$this->description 			= $object->artikel_desc;
			$this->description_en 		= $object->artikel_desc_en;
			$this->image_asset_id 		= $object->image_asset_id;
			$arrname						= explode(":", $object->artikel_name);
			$this->shortname				= count($arrname) > 1 ? trim($arrname[1]) : $object->artikel_name;
			$this->type_id				= $object->type_id;
		}
	}
	
	public function all()
	{
		
	}
	
	public function getBookList($sitepath, $taglist=array())
	{
		$filter = "";
		if(count($taglist) > 0)
		{
			$str = "";
			$str_end = "";
			$i = 0;
			foreach($taglist as $k => $v)
			{
				$i++;
				$tag_category = $this->Database->prepare("SELECT category_id FROM " . $this->config->database . ".tag_categories WHERE category_name LIKE ? or category_name_en LIKE ?")->execute($k, $k);
				$cate_id = 0;
				$tag_id  = 0;
				while($tag_category->next())
				{
					$cate_id = $tag_category->category_id;
				}
				$tag_list = $this->Database->prepare("SELECT tag_id FROM " . $this->config->database . ".buch_tags2 WHERE category_id=? AND (tag_value = ? OR tag_value_en = ?)")->execute($cate_id, $v, $v);
				while($tag_list->next())
				{
					$arrTagId[] = $tag_list->tag_id;
					$tag_id		= $tag_list->tag_id;
				}
				
				$str .= sprintf(" (SELECT buch_id FROM " . $this->config->database . ".buch_buchtags WHERE tag_id = $tag_id ");
				if($i < count($taglist))
				{
					$str .= " AND finanz_artikel.buch_id in ";
				}
				$str_end .= ")";
			}
			
			$filter = " AND finanz_artikel.`buch_id` IN " . $str . $str_end;
		}
		
		$query = sprintf("
			SELECT DISTINCT (SELECT artikel_klassen.class_is_addon 
			FROM " . $this->config->database . ".artikel_klassen 
			WHERE artikel_klassen.class_id =
			finanz_artikel.class_id) 
			AS artikel_is_addon, 
			(
				SELECT artikel_klassen.class_is_upgrade 
				FROM " . $this->config->database . ".artikel_klassen 
				WHERE artikel_klassen.class_id = finanz_artikel.class_id
			) AS artikel_is_upgrade, 
			finanz_artikel.artikel_id AS finanz_artikel_artikel_id, 
			finanz_artikel.artikel_typ AS finanz_artikel_artikel_typ, 
			finanz_artikel.class_id AS finanz_artikel_class_id, 
			finanz_artikel.country_id AS finanz_artikel_country_id, 
			finanz_artikel.image_asset_id AS finanz_artikel_image_asset_id, 
			finanz_artikel.konto_id AS finanz_artikel_konto_id, 
			finanz_artikel.mandant_id AS finanz_artikel_mandant_id, 
			finanz_artikel.shipping_id AS finanz_artikel_shipping_id,
			finanz_artikel.artikel_datum_ueberschreiben AS finanz_artikel_artikel_datum_ueberschreiben,
			finanz_artikel.artikel_nebenrechnung AS finanz_artikel_artikel_nebenrechnung, 
			finanz_artikel.artikel_nummer AS finanz_artikel_artikel_nummer, 
			finanz_artikel.artikel_variante AS finanz_artikel_artikel_variante,
			finanz_artikel.artikel_einbandvariante AS finanz_artikel_artikel_einbandvariante,
			finanz_artikel.artikel_produktions_dauer AS finanz_artikel_artikel_produktions_dauer, 
			finanz_artikel.artikel_desc AS finanz_artikel_artikel_desc, 
			finanz_artikel.artikel_desc_en AS finanz_artikel_artikel_desc_en,
			finanz_artikel.artikel_long_desc AS finanz_artikel_artikel_long_desc, 
			finanz_artikel.artikel_long_desc_en AS finanz_artikel_artikel_long_desc_en, 
			finanz_artikel.artikel_name AS finanz_artikel_artikel_name,
			finanz_artikel.artikel_name_en AS finanz_artikel_artikel_name_en, 
			finanz_artikel.artikel_needs_coupon AS finanz_artikel_artikel_needs_coupon, 
			finanz_artikel.artikel_manuell AS finanz_artikel_artikel_manuell,
			finanz_artikel.artikel_created AS finanz_artikel_artikel_created, 
			finanz_artikel.artikel_updated AS finanz_artikel_artikel_updated, 
			finanz_artikel.artikel_available AS finanz_artikel_artikel_available,
			finanz_artikel.artikel_active AS finanz_artikel_artikel_active, 
			finanz_artikel.artikel_validuntil AS finanz_artikel_artikel_validuntil, 
			finanz_artikel.buch_id AS finanz_artikel_buch_id, 
			finanz_artikel.version_id AS finanz_artikel_version_id, 
			finanz_artikel.type_id AS finanz_artikel_type_id, 
			finanz_artikel.bookmark_id AS finanz_artikel_bookmark_id, 
			finanz_artikel.giftbox_id AS finanz_artikel_giftbox_id, 
			finanz_artikel.design_id AS finanz_artikel_design_id, 
			buecher.buch_lang != 'de' AS anon_1, 
			buecher.type_id AS buecher_type_id, 
			buecher.buch_created AS buecher_buch_created, 
			buecher.buch_id AS buecher_buch_id, 
			artikel_klassen_1.class_id AS artikel_klassen_1_class_id,
			artikel_klassen_1.parent_id AS artikel_klassen_1_parent_id, 
			artikel_klassen_1.class_type AS artikel_klassen_1_class_type, 
			artikel_klassen_1.class_description AS artikel_klassen_1_class_description,
			artikel_klassen_1.class_is_addon AS artikel_klassen_1_class_is_addon, 
			artikel_klassen_1.class_is_upgrade AS artikel_klassen_1_class_is_upgrade, 
			artikel_klassen_1.class_is_tangible AS artikel_klassen_1_class_is_tangible,
			artikel_klassen_1.class_pieces_from_parent AS artikel_klassen_1_class_pieces_from_parent,
			artikel_klassen_1.class_data_from_parent AS artikel_klassen_1_class_data_from_parent, 
			artikel_klassen_1.class_max_pieces AS artikel_klassen_1_class_max_pieces, 
			artikel_klassen_1.class_max_instances AS artikel_klassen_1_class_max_instances,
			artikel_klassen_1.class_singular AS artikel_klassen_1_class_singular, 
			artikel_klassen_1.class_plural AS artikel_klassen_1_class_plural, 
			artikel_klassen_1.class_available_threshold AS artikel_klassen_1_class_available_threshold, 
			artikel_klassen_1.class_mixed_taxes_allowed AS artikel_klassen_1_class_mixed_taxes_allowed 
			
			FROM " . $this->config->database . ".finanz_artikel 
			LEFT OUTER JOIN " . $this->config->database . ".buecher 
			ON buecher.buch_id = finanz_artikel.buch_id 
			
			LEFT OUTER JOIN " . $this->config->database . ".site_book 
			ON site_book.buch_id = buecher.buch_id 
			
			LEFT OUTER JOIN " . $this->config->database . ".sites 
			ON sites.site_id = site_book.site_id 
			
			INNER JOIN " . $this->config->database . ".artikel_buchtags AS artikel_buchtags_1 
			ON artikel_buchtags_1.artikel_id = finanz_artikel.artikel_id 
			
			INNER JOIN " . $this->config->database . ".buch_tags2 AS buch_tags2_1 
			ON buch_tags2_1.tag_id = artikel_buchtags_1.tag_id 
			
			INNER JOIN " . $this->config->database . ".tag_categories AS tag_categories_1 
			ON tag_categories_1.category_id = buch_tags2_1.category_id 
			
			LEFT OUTER JOIN " . $this->config->database . ".artikel_klassen AS artikel_klassen_1 
			ON artikel_klassen_1.class_id = finanz_artikel.class_id 
			
			WHERE 
			(
				buecher.buch_id IS NULL OR
				buecher.buch_pub = 1 AND buecher.buch_hidden = 0 AND buecher.type_id NOT IN (8, 10, 21) AND buecher.buch_id NOT IN
				(411)
			) 
			AND finanz_artikel.artikel_active = 1 
			AND 
			(
				finanz_artikel.artikel_validuntil IS NULL 
				OR finanz_artikel.artikel_validuntil > now()
			) 
			AND finanz_artikel.class_id = 2 
			AND 
			(
				sites.site_path IS NULL 
				OR sites.site_path = '%s'
			)
			AND finanz_artikel.version_id NOT IN (4, 5, 6, 1303, 1304, 1305) 
			
			%s
			
			GROUP BY buecher.buch_id
			ORDER BY buecher.buch_lang != 'de', buecher.type_id, buecher.buch_created DESC, buecher.buch_id DESC
		", $sitepath, $filter );
		
		$objects = $this->Database->prepare($query)->execute();
		$books = array();
		while($objects->next())
		{
			$book = new PnBook();
			$book->article_id = $objects->finanz_artikel_artikel_id;
			$book->id = $objects->finanz_artikel_buch_id;
			$book->title = $objects->finanz_artikel_artikel_name;
			$book->title_en = $objects->finanz_artikel_artikel_name_en;
			$books[] = $book;
		}
		
		return $books;
	}
	
	public function children()
	{
		$children = array();
		
		$objects = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".finanz_artikel fa
					LEFT JOIN " . $this->config->database . ".artikel_artikel aa
					ON fa.`artikel_id` = aa.`child_id`
					WHERE aa.`parent_id` = ?
					AND fa.`artikel_active` = 1
				")->execute($this->id);
		
		while($objects->next())
		{
			$article 					= new PnArticle();
			$article->id					= $objects->artikel_id;
			$article->book_id 			= $objects->buch_id;
			$article->design_id 			= $objects->design_id;
			$article->version_id 		= $objects->version_id;
			$article->class_id 			= $objects->class_id;
			$article->name				= $objects->artikel_name;
			$article->name_en 			= $objects->artikel_name_en;
			$article->description 		= $objects->artikel_desc;
			$article->description_en 	= $objects->artikel_desc_en;
			
			$children[] 				= $article;
		}
		
		return $children;
	}
	
	public function getAddons()
	{
		$addons = array();
		if($this->id != "")
		{
			if($this->type_id != 6 )
			{
			$objects = $this->Database->prepare("
						SELECT *, ak.`class_singular`, ak.`class_pieces_from_parent` 
						FROM " . $this->config->database . ".finanz_artikel fa 
						LEFT JOIN " . $this->config->database . ".artikel_artikel aa
						ON fa.artikel_id = aa.child_id
						LEFT JOIN " . $this->config->database . ".artikel_klassen ak
						ON fa.`class_id` = ak.`class_id` 
						WHERE aa.parent_id = ?
						AND fa.artikel_active = '1'
						AND (isnull(fa.artikel_available) OR fa.artikel_available > ak.class_available_threshold)
						AND fa.`class_id` IN (
							SELECT class_id FROM " . $this->config->database . ".artikel_klassen ak
							WHERE ak.class_is_addon = '1'
							AND ak.class_id != 11
						)
						ORDER BY fa.artikel_name
					")->execute($this->id);
			}
			else
			{
				$objects = $this->Database->prepare("
						SELECT *, ak.`class_singular`, ak.`class_pieces_from_parent` 
						FROM " . $this->config->database . ".finanz_artikel fa 
						LEFT JOIN " . $this->config->database . ".artikel_artikel aa
						ON fa.artikel_id = aa.child_id
						LEFT JOIN " . $this->config->database . ".artikel_klassen ak
						ON fa.`class_id` = ak.`class_id` 
						WHERE aa.parent_id = ?
						AND fa.artikel_active = '1'
						AND (isnull(fa.artikel_available) OR fa.artikel_available > ak.class_available_threshold)
						AND fa.`class_id` IN (
							SELECT class_id FROM " . $this->config->database . ".artikel_klassen ak
							WHERE ak.class_is_addon = '1'
						)
						ORDER BY fa.artikel_name
					")->execute($this->id);
			}
			while($objects->next())
			{
				$article = new PnArticle();
				$article->id				= $objects->artikel_id;
				$article->design_id 		= $objects->design_id;
				$article->version_id 	    = $objects->version_id;
				$article->class_id 		    = $objects->class_id;
				$article->class_name		= $objects->class_singular;
				$article->name			    = $objects->artikel_name;
				$article->name_en 		    = $objects->artikel_name_en;
				$article->description 	    = $objects->artikel_desc;
				$article->description_en    = $objects->artikel_desc_en;
				$article->image_asset_id    = $objects->image_asset_id;
				$arrname					= explode(":", $objects->artikel_name);
				$article->shortname		    = count($arrname) > 1 ? trim($arrname[1]) : $objects->artikel_name;
				$article->pieces_from_parent = $objects->class_pieces_from_parent;
				$addons[]				    = $article;
			}
		}
		
		return $addons;
	}
	
	public function isAddon()
	{
		return false;
	}
	
	public function isAvailable()
	{
		$object = $this->Database->prepare("
				SELECT * FROM finanz_artikel fa
				LEFT JOIN artikel_klassen ak
				ON fa.class_id = ak.class_id
				WHERE fa.artikel_id = ?
				AND (isnull(fa.artikel_available) OR fa.artikel_available > ak.class_available_threshold)
			")->execute($this->id);
		
		while($object->next())
		{
			if($object->artikel_id)
				return true;
		}
		
		return false;
	}
	
	public function isUpgrade()
	{
		return false;
	}
	
	public function isTangible()
	{
		return false;
	}
	
	public function getPrice($locale="de")
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".prices
				WHERE artikel_id = ?
				AND (isnull(price_validuntil) OR price_validuntil > now())
			")->execute($this->id);
			
		while($object->next())
		{
			if($locale == "de")
				return $object->price_amount;
			return $object->price_amount_uk;
		}
		
		return 0;
	}
	
	public function getAssets()
	{
		$files = array();
		
		$object = $this->Database->prepare("
				SELECT asset_id, asset_filename
				FROM " . $this->config->database . ".assets
				WHERE asset_id = ?
			")->execute($this->image_asset_id);
			
		while($object->next())
		{
			$files[$object->asset_id] = $object->asset_filename;
		}
		
		$object = $this->Database->prepare("
				SELECT a.asset_filename, a.asset_id FROM " . $this->config->database . ".assets a
				LEFT JOIN " . $this->config->database . ".extra_assets ex
				ON a.asset_id = ex.asset_id
				WHERE ex.artikel_id = ? AND a.asset_active = 1 AND a.asset_licence_bought = 0
			")->execute($this->id);
			
		while($object->next())
		{
			$files[$object->asset_id] = $object->asset_filename;
		}
		
		return $files;
	}
	
	public function getDefaultAsset()
	{
	    $objDb = $this->Database->prepare("
	            SELECT asset_filename
	            FROM " . $this->config->database . ".assets 
	            WHERE asset_id = ?
	        ")->execute($this->image_asset_id);
	        
	    while($objDb->next())
	    {
	        return $objDb->asset_filename;
	    }
	    
	    return "";
	}
	
	public function getEtiquettes()
	{
	    $etiquettes = array();
	    $objDb = $this->Database->prepare("
	            SELECT *, e.etiquette_id as etiquette_id
	            FROM " . $this->config->database . ".artikel_etiquettes ae
                LEFT JOIN " . $this->config->database . ".etiquettes e
                ON ae.etiquette_id = e.etiquette_id
                WHERE ae.artikel_id = ?
                AND e.etiquette_active = 1
                ORDER BY e.etiquette_updated
	        ")->execute($this->id);
	        
	    while($objDb->next())
	    {
	        $etiquette = new PnEtiquette();
	        $etiquette->id                    = $objDb->etiquette_id;
            $etiquette->name                  = $objDb->etiquette_name;
            $etiquette->bg_asset_id           = $objDb->etiquette_bg_asset_id;
            $etiquette->mask_asset_id         = $objDb->etiquette_mask_asset_id;
            $etiquette->glitter_asset_id      = $objDb->etiquette_glitter_asset_id;
            $etiquette->text_fontsize_pct     = $objDb->etiquette_text_fontsize_pct;
            $etiquette->active                = $objDb->etiquette_active;
            $etiquette->format_id             = $objDb->etiquette_format_id;
            $etiquette->text_margin_top       = $objDb->etiquette_text_margin_top;
            $etiquette->text_margin_right     = $objDb->etiquette_text_margin_right;
            $etiquette->text_margin_bottom    = $objDb->etiquette_text_margin_bottom;
            $etiquette->text_margin_left      = $objDb->etiquette_text_margin_left;
            $etiquette->updated               = $objDb->etiquette_updated;
            $etiquette->photo_allowed         = $objDb->etiquette_photo_allowed;
            $etiquette->photo_x               = $objDb->etiquette_photo_x;
            $etiquette->photo_y               = $objDb->etiquette_photo_y;
            $etiquette->photo_w               = $objDb->etiquette_photo_w;
            $etiquette->photo_h               = $objDb->etiquette_photo_h;
            $etiquette->default_fontcolor     = $objDb->etiquette_default_fontcolor;
            $etiquette->text_maxlines         = $objDb->etiquette_text_maxlines;
            
            $etiquettes[] = $etiquette;
	    }
	    
	    return $etiquettes;
	}
	
	public function getBookblockUpgrade()
	{
	    $objDb = $this->Database->prepare("
                SELECT fa.artikel_id, fa.artikel_name, fa.artikel_desc, fa.artikel_name_en, fa.image_asset_id 
                FROM " . $this->config->database . ".artikel_artikel aa
                JOIN " . $this->config->database . ".finanz_artikel fa
                ON aa.child_id = fa.artikel_id
                WHERE parent_id = ?
                AND fa.class_id = 12
                AND fa.artikel_active = 1
            ")->execute($this->id);
            
        $bookBlock = array();
        while($objDb->next())
        {
            $article = new PnArticle();
            $article->id            = $objDb->artikel_id;
            $article->name          = $objDb->artikel_name;
            $article->description   = $objDb->artikel_desc;
            $article->name_en       = $objDb->artikel_name_en;
            $article->image_asset_id = $objDb->image_asset_id;
            
            $bookBlock[] = $article;
        }
        
        return $bookBlock;
	}
	
	public function get_total_brutto()
	{
	    
	}
	
	public function getTotalBrutto()
	{
	    /*
	    def get_total_brutto(self, currency, country=None):
            price_column_name = {
                Currency.Euro.pk_id: 'price_amount',
                Currency.Pound.pk_id: 'price_amount_uk',
            }[currency.currency_id]
    
            amount = getattr(self.aktueller_preis, price_column_name)
    
            if self.aktueller_preis.price_netto:
                assert country is not None
                steuersatz = (self.steuerart
                              .for_country(country)
                              .saetze
                              .current()
                              .one().steuersatz_rate)
                return netto_to_brutto(amount, 1, float(steuersatz),
                                       rounder=round_decimal)
            else:
                return amount
	    
	    */
	}
	
	public function buy()
	{
	    
	}
	
	public function hasGiftbox()
	{
	    $addons = $this->getAddons();
	    foreach($addons as $addon)
	    {
	        if($addon->class_id == 4)
	            return true;
	    }
	    
	    return false;
	}
	
}

class PnBookArticle extends PnArticle
{
    public function visitBuy($item)
    {
        $order = $item->order;
        $item->book = "";
    }
    
    public function initWithId($articleId)
    {
        
    }
    
    
    public function initWithBookAndVersion($bookId, $versionId)
    {
        $db = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".finanz_artikel
                WHERE buch_id = ? AND version_id = ?
            ")->execute($bookId, $versionId);
            
        while($db->next())
		{
		    $this->id                  = $db->artikel_id;
		    $this->book_id             = $db->buch_id;
		    $this->design_id           = $db->design_id;
			$this->version_id          = $db->version_id;
			$this->class_id            = $db->class_id;
			$this->giftbox_id          = $db->giftbox_id;
			$this->class_name          = $db->class_singular;
			$this->konto_id            = $db->konto_id;
			$this->pieces_from_parent  = $db->class_pieces_from_parent;
			$this->name                = $db->artikel_name;
			$this->name_en             = $db->artikel_name_en;
			$this->description         = $db->artikel_desc;
			$this->description_en      = $db->artikel_desc_en;
			$this->image_asset_id      = $db->image_asset_id;
		}
    }
}

?>