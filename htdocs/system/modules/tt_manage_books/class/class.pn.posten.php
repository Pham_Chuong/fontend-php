<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnPosten extends Controller
{
    public $id;
    public $steuerart_id;
    public $zielkonto;
    public $konto_id;
    public $artikel_id;
    public $anzahl;
    public $einzelbetrag;
    public $netto;
    public $text;
    public $description;
    public $datum;
    public $manuell;
    public $created;
    public $updated;
    public $acc_id;
    public $item_id;
    public $order_id;
    public $rechnung_id;
    public $coupon_id;
    public $culprit_id;
    public $ust_id;
    public $currency_id;
    public $steuersatz_id;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".finanz_posten
	            WHERE posten_id = ?
	        ")->execute($id);
	        
        while($objDb->next())
        {
            $this->id            = $objDb->posten_id;
            $this->steuerart_id  = $objDb->steuerart_id;
            $this->zielkonto     = $objDb->posten_zielkonto;
            $this->konto_id      = $objDb->konto_id;
            $this->artikel_id    = $objDb->artikel_id;
            $this->anzahl        = $objDb->posten_anzahl;
            $this->einzelbetrag  = $objDb->posten_einzelbetrag;
            $this->netto         = $objDb->posten_netto;
            $this->text          = $objDb->posten_text;
            $this->description   = $objDb->posten_description;
            $this->datum         = $objDb->posten_datum;
            $this->manuell       = $objDb->posten_manuell;
            $this->created       = $objDb->posten_created;
            $this->updated       = $objDb->posten_updated;
            $this->acc_id        = $objDb->acc_id;
            $this->item_id       = $objDb->item_id;
            $this->order_id      = $objDb->order_id;
            $this->rechnung_id   = $objDb->rechnung_id;
            $this->coupon_id     = $objDb->coupon_id;
            $this->culprit_id    = $objDb->culprit_id;
            $this->ust_id        = $objDb->ust_id;
            $this->currency_id   = $objDb->currency_id;
            $this->steuersatz_id = $objDb->steuersatz_id;
        }
	}
	
	public function initWithItemId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".finanz_posten
	            WHERE item_id = ?
	        ")->execute($id);
	        
        while($objDb->next())
        {
            $this->id            = $objDb->posten_id;
            $this->steuerart_id  = $objDb->steuerart_id;
            $this->zielkonto     = $objDb->posten_zielkonto;
            $this->konto_id      = $objDb->konto_id;
            $this->artikel_id    = $objDb->artikel_id;
            $this->anzahl        = $objDb->posten_anzahl;
            $this->einzelbetrag  = $objDb->posten_einzelbetrag;
            $this->netto         = $objDb->posten_netto;
            $this->text          = $objDb->posten_text;
            $this->description   = $objDb->posten_description;
            $this->datum         = $objDb->posten_datum;
            $this->manuell       = $objDb->posten_manuell;
            $this->created       = $objDb->posten_created;
            $this->updated       = $objDb->posten_updated;
            $this->acc_id        = $objDb->acc_id;
            $this->item_id       = $objDb->item_id;
            $this->order_id      = $objDb->order_id;
            $this->rechnung_id   = $objDb->rechnung_id;
            $this->coupon_id     = $objDb->coupon_id;
            $this->culprit_id    = $objDb->culprit_id;
            $this->ust_id        = $objDb->ust_id;
            $this->currency_id   = $objDb->currency_id;
            $this->steuersatz_id = $objDb->steuersatz_id;
        }
	}
	
	public function initShippingPosten($order_id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * 
                FROM " . $this->config->database . ".finanz_posten fp
                LEFT JOIN " . $this->config->database . ".finanz_artikel fa
                ON fp.artikel_id = fa.artikel_id
                WHERE fa.class_id = 7
                AND fp.order_id = ?
	        ")->execute($order_id);
	        
	    while($objDb->next())
	    {
	        $this->id            = $objDb->posten_id;
            $this->steuerart_id  = $objDb->steuerart_id;
            $this->zielkonto     = $objDb->posten_zielkonto;
            $this->konto_id      = $objDb->konto_id;
            $this->artikel_id    = $objDb->artikel_id;
            $this->anzahl        = $objDb->posten_anzahl;
            $this->einzelbetrag  = $objDb->posten_einzelbetrag;
            $this->netto         = $objDb->posten_netto;
            $this->text          = $objDb->posten_text;
            $this->description   = $objDb->posten_description;
            $this->datum         = $objDb->posten_datum;
            $this->manuell       = $objDb->posten_manuell;
            $this->created       = $objDb->posten_created;
            $this->updated       = $objDb->posten_updated;
            $this->acc_id        = $objDb->acc_id;
            $this->item_id       = $objDb->item_id;
            $this->order_id      = $objDb->order_id;
            $this->rechnung_id   = $objDb->rechnung_id;
            $this->coupon_id     = $objDb->coupon_id;
            $this->culprit_id    = $objDb->culprit_id;
            $this->ust_id        = $objDb->ust_id;
            $this->currency_id   = $objDb->currency_id;
            $this->steuersatz_id = $objDb->steuersatz_id;
	    }
	}
	
	public function initCouponPosten($order_id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * 
                FROM " . $this->config->database . ".finanz_posten fp
                LEFT JOIN " . $this->config->database . ".finanz_artikel fa
                ON fp.artikel_id = fa.artikel_id
                WHERE fa.class_id IN (8, 19)
                AND fp.order_id = ?
	        ")->execute($order_id);
	        
	    while($objDb->next())
	    {
	        $this->id            = $objDb->posten_id;
            $this->steuerart_id  = $objDb->steuerart_id;
            $this->zielkonto     = $objDb->posten_zielkonto;
            $this->konto_id      = $objDb->konto_id;
            $this->artikel_id    = $objDb->artikel_id;
            $this->anzahl        = $objDb->posten_anzahl;
            $this->einzelbetrag  = $objDb->posten_einzelbetrag;
            $this->netto         = $objDb->posten_netto;
            $this->text          = $objDb->posten_text;
            $this->description   = $objDb->posten_description;
            $this->datum         = $objDb->posten_datum;
            $this->manuell       = $objDb->posten_manuell;
            $this->created       = $objDb->posten_created;
            $this->updated       = $objDb->posten_updated;
            $this->acc_id        = $objDb->acc_id;
            $this->item_id       = $objDb->item_id;
            $this->order_id      = $objDb->order_id;
            $this->rechnung_id   = $objDb->rechnung_id;
            $this->coupon_id     = $objDb->coupon_id;
            $this->culprit_id    = $objDb->culprit_id;
            $this->ust_id        = $objDb->ust_id;
            $this->currency_id   = $objDb->currency_id;
            $this->steuersatz_id = $objDb->steuersatz_id;
	    }
        
	}
	
	public function add($params = array())
	{
	    $key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = $v;
		}
		
		$objDb = $this->Database->prepare('
					INSERT INTO ' . $this->config->database . '.finanz_posten ('. implode(', ', $key) .') 
					VALUES("' . implode('", "', $value) . '")
				')->execute();
		
		return $objDb->insertId;
	}
	
	public function edit($params = array())
	{
	    $set = array();
		foreach ($params as $k => $v)
		{
			$set[] = $k . '  =  "' . $v . '"';
		}
		
		$objDb = $this->Database->prepare("
                    UPDATE " . $this->config->database . ".finanz_posten 
                    SET " . implode(", ", $set) . " WHERE posten_id = ?
                " )->execute($this->id);
		
		return $objDb->affectedRows;
	}
	
	public function remove()
	{
	    $objDb = $this->Database->prepare("
	            DELETE FROM " . $this->config->database . ".finanz_zahlungen
				WHERE posten_id = ?
	        ")->execute($this->id);
	    
	    $objDb = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".finanz_posten
				WHERE posten_id = ?
			")->execute($this->id);
	}
}
