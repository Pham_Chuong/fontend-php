<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnPayment extends Controller
{
	public $id;
	public $name;
	public $debitor_id;
	public $description;
	public $notes;
	public $html;
	public $javarequire;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".payment_methods
	            WHERE payment_method_id = ?
	        ")->execute($id);
	        
	    while($objDb->next())
	    {
	        $this->id          = $objDb->payment_method_id;
	        $this->name        = $objDb->payment_method_name;
	        $this->debitor_id  = $objDb->debitor_id;
	        $this->description = $objDb->payment_method_description;
	        $this->notes       = $objDb->payment_method_notes;
	        $this->html        = $objDb->payment_method_html;
	        $this->javarequire = $objDb->payment_method_javarequire;
	    }
	}
	
	public function getPayments($locale_id=1)
	{
		$objects = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".payment_methods
				WHERE locale_id = ? AND payment_method_active = 1
			")->execute($locale_id);
		
		$payments = array();
		while($objects->next())
		{
			$payment 				= new PnPayment();
			$payment->id 			= $objects->payment_method_id;
			$payment->name 			= $objects->payment_method_name;
			$payment->debitor_id		= $objects->debitor_id;
			$payment->description 	= $objects->payment_method_description;
			$payment->html			= $objects->payment_method_html;
			$payment->notes 			= $objects->payment_method_notes;
			$payment->javarequired	= $objects->payment_method_javarequired;
			
			$payments[] 				= $payment;
		}
		
		return $payments;
	}
}

?>
