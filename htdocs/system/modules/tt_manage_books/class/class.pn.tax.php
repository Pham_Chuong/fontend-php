<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class Steuerart extends Controller
{
    public $id;
    public $name;
    public $currency_id;
    public $country_id;
    public $typ;

    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	        SELECT * FROM " . $this->config->database . ".steuerart
	        WHERE steuerart_id = ?
        ")->execute($id);
        
        while($objDb->next())
        {
            $this->id           = $objDb->steuerart_id;
            $this->name         = $objDb->steuerart_name;
            $this->currency_id  = $objDb->currency_id;
            $this->country_id   = $objDb->country_id;
            $this->typ          = $objDb->steuerart_typ;
        }
	}
}

class PnTaxType extends Steuerart
{
    //Virtual
}

class Steuersatz extends Controller
{
    public $steuersatz_id;
    public $steuerart_id;
    public $steuersatz_rate;
    public $steuersatz_validfrom;
    public $steuersatz_validuntil;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	        SELECT * FROM " . $this->config->database . ".steuersatz
	        WHERE steuersatz_id = ?
        ")->execute($id);
        
        while($objDb->next())
        {
            $this->id            = $objDb->steuersatz_id;
            $this->steuerart_id  = $objDb->steuerart_id;
            $this->rate          = $objDb->steuersatz_rate;
            $this->validfrom     = $objDb->steuersatz_validfrom;
            $this->validuntil    = $objDb->steuersatz_validuntil;
        }
	}
	
	public function initWithTaxTypeId($taxtype)
	{
	    $objDb = $this->Database->prepare("
	        SELECT * FROM " . $this->config->database . ".steuersatz
	        WHERE steuerart_id = ?
        ")->execute($taxtype);
        
        while($objDb->next())
        {
            $this->id            = $objDb->steuersatz_id;
            $this->steuerart_id  = $objDb->steuerart_id;
            $this->rate          = $objDb->steuersatz_rate;
            $this->validfrom     = $objDb->steuersatz_validfrom;
            $this->validuntil    = $objDb->steuersatz_validuntil;
        }
	}
}

class PnTaxRate extends Steuersatz
{
    //Virtual
}

?>
