<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnKonto extends Controller
{
    public $id;
    public $ust_id;
    public $dispo;
    public $kredit;
    public $typ;
    public $kategorie;
    public $bebuchbar;
    public $menuell;
    public $unlimitert;
    public $ustidentnumber;
    public $sammelkonto;
    public $name;
    public $currency_id;
    public $importierbar;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".finanz_konten
	            WHERE konto_id = ?
	        ")->execute($id);
	        
	    while($objDb->next())
	    {
	        $this->id             = $objDb->konto_id;
	        $this->ust_id         = $objDb->ust_di;
	        $this->dispo          = $objDb->konto_dispo;
            $this->kredit         = $objDb->konto_kredit;
            $this->typ            = $objDb->konto_typ;
            $this->kategorie      = $objDb->konto_kategorie;
            $this->bebuchbar      = $objDb->konto_bebuchbar;
            $this->menuell        = $objDb->konto_menuell;
            $this->unlimitert     = $objDb->konto_unlimitert;
            $this->ustidentnumber = $objDb->konto_ustidentnumber;
            $this->sammelkonto    = $objDb->konto_sammelkonto;
            $this->name           = $objDb->konto_name;
            $this->currency_id    = $objDb->currency_id;
            $this->importierbar   = $objDb->konto_importierbar;
	    }
	}
	
	public function add($params = array())
	{
	    $key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = mysql_real_escape_string($v);
		}
		
		$objDb = $this->Database->prepare("
					INSERT INTO " . $this->config->database . ".finanz_konten (". implode(", ", $key) .") 
					VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $objDb->insertId;
	}
	
	public function edit($params = array())
	{
	    $set = array();
		foreach ($params as $k => $v)
		{
			$set[] = $k . "  =  '" . mysql_real_escape_string($v) . "'";
		}
		
		$objDb = $this->Database->prepare("
                    UPDATE " . $this->config->database . ".finanz_konten 
                    SET " . implode(", ", $set) . " WHERE konto_id = ?
                " )->execute($this->id);
		
		return $objDb->affectedRows;
	}
	
	public function remove($params = array())
	{
	    
	}
}