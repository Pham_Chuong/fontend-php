<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnAuthor extends Controller
{
	public $id;
	public $name;
	public $email;
	public $cc;
	public $gender;
	public $mwst;
	public $autor_per_book;
	public $pic_asset_id;
	public $description;
	public $catalogue;
	public $ustid;
	public $steuernummer;
	public $page_public;
	public $pseudonym;
	public $mailText;
	public $lieferantenummer;
	protected $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".autoren
				WHERE autor_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 				= $object->autor_id;
			$this->name 				= $object->autor_name;
			$this->email 			= $object->autor_email;
			$this->cc 				= $object->autor_cc;
			$this->gender 			= $object->autor_sex;
			$this->mwst 				= $object->autor_mwst;
			$this->autor_per_book 	= $object->autor_per_book;
			$this->pic_asset_id 		= $object->autor_pic_asset_id;
			$this->description 		= $object->autor_description;
			$this->catalogue 		= $object->autor_catalogue;
			$this->ustid 			= $object->autor_ustid;
			$this->steuernummer 		= $object->autor_steuernummer;
			$this->page_public 		= $object->autor_page_public;
			$this->pseudonym 		= $object->autor_pseudonym;
			$this->mailText 			= $object->autor_mailtext;
			$this->lieferantenummer 	= $object->autor_lieferantenummer;
		}
	}
	
	public function getImageUrl()
	{
		$image_file_name		= $this->getImageFileName();
		if($image_file_name != ''){
			$thumb_url 			= "/thumbs/";
			$thumb_path			= implode("/", str_split(substr(str_replace(".", "", $image_file_name), 0, 3)));
			$thumbnail_tag		= "w200";
			$thumbnail_name 		= $thumbnail_tag . "-" . md5($this->config->site_secret . $thumbnail_tag . $image_file_name) . "-" .$image_file_name;
			$thumb_url	   	   .= $thumb_path . "/" .$thumbnail_name;
			
			return sprintf($this->config->image_root . "%s", $thumb_url);
		}else{
			return '';
		}
	}
	
	function getImageFileName()
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".assets
				WHERE asset_id = ?
			")->execute($this->pic_asset_id);
			
		while($object->next())
		{
			return $object->asset_filename;
		}
		
		return "";
	}
}
