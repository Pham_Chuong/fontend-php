<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnAsset extends Controller
{
    public $id;
    public $thumb_asset_id;
    public $type_id;
    public $filename;
    public $title;
    public $xres;
    public $yres;
    public $created;
    public $updated;
    public $active;
    public $token;
    public $licence;
    public $licence_bought;
    public $locale_id;
    public $title_en;
    public $link_href;
    public $link_clicks;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".assets
                WHERE asset_id = ?
	        ")->execute($id);
	        
	    while($objDb->next())
	    {
	        $this->id              = $objDb->asset_id;
	        $this->thumb_asset_id  = $objDb->thumb_asset_id;
	        $this->type_id         = $objDb->asset_type_id;
	        $this->filename        = $objDb->asset_filename;
	        $this->title           = $objDb->asset_title;
	        $this->xres            = $objDb->asset_xres;
	        $this->yres            = $objDb->asset_yres;
	        $this->created         = $objDb->asset_created;
	        $this->updated         = $objDb->asset_updated;
	        $this->active          = $objDb->asset_active;
	        $this->token           = $objDb->asset_token;
	        $this->licence         = $objDb->asset_licence;
	        $this->licence_bought  = $objDb->asset_licence_bought;
	        $this->locale_id       = $objDb->locale_id;
	        $this->title_en        = $objDb->asset_title_en;
	        $this->link_href       = $objDb->asset_link_href;
	        $this->link_clicks     = $objDb->asset_link_clicks;
	    }
	    
	}
	
	public function getExlibrisAssets()
	{
	    $assets = array();
	    $objDb = $this->Database->prepare("
	            SELECT assets.asset_id, 
	            assets.thumb_asset_id, 
	            assets.asset_type_id,
	            assets.asset_licence,
	            assets.asset_licence_bought,
	            assets.asset_filename,
	            assets.asset_title,
	            assets.asset_title_en,
	            assets.asset_xres,
	            assets.asset_yres,
	            assets.asset_created,
	            assets.asset_updated,
	            assets.asset_active,
	            assets.asset_token,
	            assets.locale_id,
	            assets.asset_link_href,
	            assets.asset_link_clicks,
	            asset_types_1.asset_type_id AS asset_types_1_asset_type_id, 
	            asset_types_1.asset_type_name AS asset_types_1_asset_type_name, 
	            asset_types_1.asset_type_public AS asset_types_1_asset_type_public 
                FROM " . $this->config->database . ".assets 
                JOIN " . $this->config->database . ".asset_types AS asset_types_1 
                ON asset_types_1.asset_type_id = assets.asset_type_id 
                WHERE assets.asset_type_id = ?
                ORDER BY assets.asset_id
	        ")->execute(39);
	        
        while($objDb->next())
        {
            $asset = new PnAsset();
            $asset->id              = $objDb->asset_id;
	        $asset->thumb_asset_id  = $objDb->thumb_asset_id;
	        $asset->type_id         = $objDb->asset_type_id;
	        $asset->filename        = $objDb->asset_filename;
	        $asset->title           = $objDb->asset_title;
	        $asset->xres            = $objDb->asset_xres;
	        $asset->yres            = $objDb->asset_yres;
	        $asset->created         = $objDb->asset_created;
	        $asset->updated         = $objDb->asset_updated;
	        $asset->active          = $objDb->asset_active;
	        $asset->token           = $objDb->asset_token;
	        $asset->licence         = $objDb->asset_licence;
	        $asset->licence_bought  = $objDb->asset_licence_bought;
	        $asset->locale_id       = $objDb->locale_id;
	        $asset->title_en        = $objDb->asset_title_en;
	        $asset->link_href       = $objDb->asset_link_href;
	        $asset->link_clicks     = $objDb->asset_link_clicks;
	        
	        $assets[] = $asset;
        }
        
        return $assets;
	}
}

?>