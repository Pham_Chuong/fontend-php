<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnCoupon extends Controller
{
    public $id;
    public $order_id;
    public $konto_id;
    public $artikel_id;
    public $token;
    public $wert;
    public $created;
    public $expiredate;
    public $used;
    public $ausgegeben;
    public $ausgegeben_an;
    public $empfaenger;
    public $anzahlbuecher;
    public $errortext;
    public $multiple_use;
    public $constrained;
    public $customerlogo;
    public $coupontype_id;
    public $wert2;
    public $locale_id;
    public $barcode_id;
    public $partner_id;
    public $confirmed;
    public $template_id;
    public $prozent;
    public $mindestbestellwert;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".coupons
				WHERE coupon_id = ?
			")->execute($id);
			
        while($objDb->next())
        {
            $this->id                   = $objDb->coupon_id;
            $this->order_id             = $objDb->order_id;
            $this->konto_id             = $objDb->konto_id;
            $this->artikel_id           = $objDb->artikel_id;
            $this->token                = $objDb->coupon_token;
            $this->wert                 = $objDb->coupon_wert;
            $this->created              = $objDb->coupon_created;
            $this->expiredate           = $objDb->coupon_expiredate;
            $this->used                 = $objDb->coupon_used;
            $this->ausgegeben           = $objDb->coupon_ausgegeben;
            $this->ausgegeben_an        = $objDb->coupon_ausgegeben_an;
            $this->empfaenger           = $objDb->coupon_empfaenger;
            $this->anzahlbuecher        = $objDb->coupon_anzahlbuecher;
            $this->errortext            = $objDb->coupon_errortext;
            $this->multiple_use         = $objDb->coupon_multiple_use;
            $this->constrained          = $objDb->coupon_constrained;
            $this->customerlogo         = $objDb->coupon_customerlogo;
            $this->coupontype_id        = $objDb->coupontype_id;
            $this->wert2                = $objDb->coupon_wert2;
            $this->locale_id            = $objDb->locale_id;
            $this->barcode_id           = $objDb->barcode_id;
            $this->partner_id           = $objDb->partner_id;
            $this->confirmed            = $objDb->coupon_confirmed;
            $this->template_id          = $objDb->template_id;
            $this->prozent              = $objDb->coupon_prozent;
            $this->mindestbestellwert   = $objDb->coupon_mindestbestellwert;
        }
	}
	
	public function initWithToken($id)
	{
	    $objDb = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".coupons
				WHERE coupon_token = ?
			")->execute($id);
			
        while($objDb->next())
        {
            $this->id                   = $objDb->coupon_id;
            $this->order_id             = $objDb->order_id;
            $this->konto_id             = $objDb->konto_id;
            $this->artikel_id           = $objDb->artikel_id;
            $this->token                = $objDb->coupon_token;
            $this->wert                 = $objDb->coupon_wert;
            $this->created              = $objDb->coupon_created;
            $this->expiredate           = $objDb->coupon_expiredate;
            $this->used                 = $objDb->coupon_used;
            $this->ausgegeben           = $objDb->coupon_ausgegeben;
            $this->ausgegeben_an        = $objDb->coupon_ausgegeben_an;
            $this->empfaenger           = $objDb->coupon_empfaenger;
            $this->anzahlbuecher        = $objDb->coupon_anzahlbuecher;
            $this->errortext            = $objDb->coupon_errortext;
            $this->multiple_use         = $objDb->coupon_multiple_use;
            $this->constrained          = $objDb->coupon_constrained;
            $this->customerlogo         = $objDb->coupon_customerlogo;
            $this->coupontype_id        = $objDb->coupontype_id;
            $this->wert2                = $objDb->coupon_wert2;
            $this->locale_id            = $objDb->locale_id;
            $this->barcode_id           = $objDb->barcode_id;
            $this->partner_id           = $objDb->partner_id;
            $this->confirmed            = $objDb->coupon_confirmed;
            $this->template_id          = $objDb->template_id;
            $this->prozent              = $objDb->coupon_prozent;
            $this->mindestbestellwert   = $objDb->coupon_mindestbestellwert;
        }
	}
	public function checkArtikel()
	{
		if($this->artikel_id && $this->artikel_id != 0)
			return true;
		return false;
	}
	public function checkExist($token, $orderId="")
	{
	    $objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".coupons
                WHERE coupon_token = ?
	        ")->execute($token);
	    
	    if($objDb->numRows > 0)
	    {
	        while($objDb->next())
	        {
	            $existOrder = $objDb->order_id;
	        }
	        
	        if($existOrder == $orderId)
	            return false;
	        else
	            return true;
	    }
	    return false;
	}
	
	public function checkActive()
	{
	    $create_date = $this->created;
	    $today_date = date("Y-m-d");
	    
	    $today = strtotime($today_date);
	    $active_date = strtotime($create_date);
	    
	    if ($active_date > $today) 
	        return false;
	    return true;
	}
	
	public function checkExpire()
	{
	    $exp_date = $this->expiredate; 
	    $todays_date = date("Y-m-d"); 
	    
	    $today = strtotime($todays_date); 
	    $expiration_date = strtotime($exp_date); 
	    
	    if ($expiration_date >= $today) 
	        return false;
	    return true;
	}
	
	public function checkUsedUp($orderId="")
	{
	    if($this->multiple_use > 0)
	    {
	        return false;
	    }
	    else
	    {
	        $objDb = $this->Database->prepare("
	                SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
	                WHERE coupon_id = ?
	            ")->execute($this->id);
	            
            if($objDb->numRows > 0)
            {
                while($objDb->next())
                {
                    $existOrder = $objDb->order_id;
                }
                if($existOrder == $orderId)
                    return false;
                else
                    return true;
            }
            else
            {
                return false;
            }
	    }
	    
	    return true;
	}
	
	public function canUseForOrder($order)
	{
	    /* Su dung tam thoi cho rabatt PN50RABATT */
	    if($this->token == "PN50RABATT")
	    {
	        if($order->getNumOfBookItems() < 2)
	        {
	            return false;
	        }
	    }
	    
	    $supportLocales = $this->getSupportLocales();
	    if(count($supportLocales) > 0 && !in_array($order->locale_id, $supportLocales))
	        return false;
	    
	    //Check support country
	    $address = new PnAddress();
	    $address->initWithId($order->shipping_address_id);
	    $supportShippingCountries = $this->getSupportShippingCountries();
	    if(count($supportShippingCountries) > 0 && !in_array($address->country_id, $supportShippingCountries))
	        return false;
	    
	    //Minimum
	    $orderPrice = $order->getTotalPrice();
	    if($this->mindestbestellwert > 0 && $this->mindestbestellwert > $orderPrice)
	        return false;
	    
	    //Limited
	    if(!$this->constrained)
	        return true;
	    
	    $bookItems         = $order->getBookItems();
	    $wineItems         = $order->getWineItems();
	    $supportClasses    = $this->getSupportClasses();
	    $supportGiftboxes  = $this->getSupportGiftboxes();
	    $supportDesigns    = $this->getSupportDesigns();
	    $supportBooks      = $this->getSupportBooks();
	    $supportWidmungs   = $this->getSupportWidmungs();
	    $supportVersions   = $this->getSupportVersions();
	    $supportTypes      = $this->getSupportTypes();
	    $supportReftags    = $this->getSupportReftags();
	    
	    foreach($wineItems as $wineItem)
	    {
	        if(count($supportClasses) > 0 && !in_array($wineItem->class_id, $supportClasses))
	            return false;
	    }
	    
	    foreach($bookItems as $bookItem)
	    {
	        if(count($supportClasses) > 0 && !in_array($bookItem->class_id, $supportClasses))
	            return false;
	        
	        $childItems = $bookItem->getChildren();
	        foreach($childItems as $child)
	        {
	            if($child->class_id == 3)
	                $designItem = $child;
	            
	            if($child->class_id == 4)
	                $giftboxItem = $child;
	            
	            if($child->class_id == 10)
	                $widmungItem = $child;
	        }
	        
	        //Check support Giftbox
	        if($giftboxItem)
	        {
	            $giftbox = new PnArticle();
	            $giftbox->initWithId($giftboxItem->article_id);
                if(count($supportGiftboxes) > 0 && !in_array($giftbox->giftbox_id, $supportGiftboxes))
                    return false;
	        }
	        
	        //Check support Design
	        $design = new PnArticle();
	        $design->initWithId($designItem->article_id);
	        if($designItem)
	        {
                if(count($supportDesigns) > 0 && !in_array($design->design_id, $supportDesigns))
                    return false;
            }
            
            //Check support Type
            $book = new PnBook();
            $book->initWithId($bookItem->book_id);
            if(count($supportTypes) > 0 && !in_array($book->type_id, $supportTypes))
                return false;
            
            //Check support version
            $version = $bookItem->getBookVersion();
            if(count($supportVersions) > 0 && !in_array($version->id, $supportVersions))
                return false;
            
            //Check support Book
            if(count($supportBooks) > 0 && !in_array($bookItem->book_id, $supportBooks))
                return false;
            
            //Check support widmung
            if(count($supportWidmungs) > 0 && !in_array($widmungItem->article_id, $supportWidmungs))
                return false;
            
	    }
	    
	    //Check support reftag
	    $ref = new PnRef();
	    $ref->initWithCookie();
	    if(count($supportReftags) > 0 && !in_array($ref->id, $supportReftags))
	        return false;
	    
	    return true;
	}
	
	public function useForOrder($orderId)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
	            WHERE order_id = ?
	        ")->execute($orderId);
	    
	    if($objDb->numRows > 0)
	    {
	        $objDb = $this->Database->prepare("
                    UPDATE " . $this->config->database . ".coupons_used_on_orders
                    SET coupon_id = ?
                    WHERE order_id = ?
                ")->execute($this->id, $orderId);
	    }
	    else
	    {
            $objDb = $this->Database->prepare("
                    INSERT INTO " . $this->config->database . ".coupons_used_on_orders
                    (`coupon_id`, `order_id`) VALUES(?, ?)
                ")->execute($this->id, $orderId);
	    }
        return $objDb->affectedRows;
	}
	
	public function checkBlackFriday($order){
		if ($GLOBALS['TL_CONFIG']['black_friday']){
				//List items in array
				    $array_list_35 = $GLOBALS['TL_CONFIG']['list_book_black_friday'];
					$listItems = $order->getBookItems();
					if(count($listItems)>0){
						foreach($listItems as $items_new){
							if(in_array($items_new->book_id,$array_list_35)){
								return true;
							}
						}
					}
			}
		 return false;
	}
	
	public function recalc($order)
	{
	    $couponValue   = $this->wert;
	    if($this->coupontype_id == 3){
	    	$couponValue = $this->wert + $this->wert2;
	    }
	    $orderPrice    = $order->getTotalPriceWithoutCoupon("de", true); //Fixme
	    
	    if($this->prozent > 0)
	    {
	        $couponValue = $orderPrice * min(100, max(0, $this->prozent)) / 100;
	    }
	    
	    /* Su dung tam thoi cho rabatt PN50RABATT */
	    if($this->token == "PN50RABATT")
	    {
	        $couponValue = round($order->getSecondBookItemPrice() * 0.5, 2);
	    }
	    
	    $postenAmount  = $couponValue;
	    
	    if($couponValue > $orderPrice)
	        $postenAmount = $orderPrice;
	    
	    if($orderPrice <= 0)
	        return;
	    
	    $quantity = 1;
	    $konto_id = $order->konto_id;
	    
	    $posten = new PnPosten();
	    $zahlungen = new PnZahlungen();
	    $couponArticle = $this->getPnArticle();

	    
	    $address = new PnAddress();
	    $address->initWithId($order->billing_address_id);
	    $acticle_id = "";
	    $items = $order->getItems();
	    if(count($items)>0){
	    	foreach($items as $item){
	    		$acticle_id = $item->article_id;
	    	}
	    }
	    $item = new PnItem();
	    $taxRate          = $item->getTaxRateCoupon($address->country_id,$acticle_id);
	    $steuerart_id     = $taxRate->steuerart_id ? $taxRate->steuerart_id : 1;
	    $steuersatz_id    = $taxRate->id ? $taxRate->id : 1;
	    
	    
	    $posten->initCouponPosten($order->id);
	    
	    //Tru tien khuyen mai
	    $postenAmount = -1 * FloatVal($postenAmount);
	   if($GLOBALS['TL_CONFIG']['Counpon_Free'] || $this->checkBlackFriday($order))
	   { 
	    if(!$order->hasCouponNotGiftboxFree() && $order->getNumOfBookItems() > 0)
	    {
	        $posten_id = $posten->add(
                array(
                    "steuerart_id"          => $steuerart_id,
                    "konto_id"              => $order->konto_id,
                    "artikel_id"            => $order->coupon_item_id,
                    "posten_anzahl"         => 1,
                    "posten_einzelbetrag"   => 0,
                    "posten_netto"          => 0,
                    "posten_text"           => $order->coupon_name_de,
                    "posten_datum"          => date('Y/m/d H:i:s a', time()),
                    "posten_manuell"        => 0,
                    "posten_created"        => date('Y/m/d H:i:s a', time()),
                    "posten_updated"        => date('Y/m/d H:i:s a', time()),
                    "order_id"              => $order->id,
                    "currency_id"           => 1,
                    "steuersatz_id"         => $steuersatz_id
                )
            );
	    }
	   
	    if($order->hasCouponNotGiftboxFree() && $order->getNumOfBookItems() > 0)
        {
            $posten_id = $posten->add(
                array(
                    "steuerart_id"          => $steuerart_id,
                    "konto_id"              => $order->konto_id,
                    "artikel_id"            => $order->coupon_item_id,
                    "posten_anzahl"         => 1,
                    "posten_einzelbetrag"   => -1 * floatVal($order->coupon_prices_de),
                    "posten_netto"          => 0,
                    "posten_text"           => $order->coupon_name_de,
                    "posten_datum"          => date('Y/m/d H:i:s a', time()),
                    "posten_manuell"        => 0,
                    "posten_created"        => date('Y/m/d H:i:s a', time()),
                    "posten_updated"        => date('Y/m/d H:i:s a', time()),
                    "order_id"              => $order->id,
                    "currency_id"           => 1,
                    "steuersatz_id"         => $steuersatz_id
                )
            );
            
            $zahlung_parent_id = $zahlungen->add(
                array(
                   "steuerart_id" => $steuerart_id,
                   "journal_id" => 1,
                   "stapel_id" => 3,
                   "order_id" => $order->id,
                   "posten_id" => $posten_id,
                   "ust_id" => 1,
                   "konto_id" => 15526, //DUMMY KONTO
                   "zahlung_anzahl" => 1,
                   "zahlung_typ" => 12,
                   "zahlung_andereskonto" => 0,
                   "zahlung_stapel" => 1,
                   "zahlung_betrag" => -1 * floatVal($order->coupon_prices_de),
                   "zahlung_netto" => 0,
                   "zahlung_text" => "Coupon: giftbox free",
                   "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                   "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                   "zahlung_created" => date('Y/m/d H:i:s a', time()),
                   "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                   "currency_id" => 1,
                   "zahlung_steuersatz" => 0,
                   "steuersatz_id" => $steuersatz_id,
                   )
            );
            
            $zahlung_child_id = $zahlungen->add(
                array(
                   "steuerart_id" => $steuerart_id,
                   "journal_id" => 1,
                   "stapel_id" => 3,
                   "parent_id" => $zahlung_parent_id,
                   "order_id" => $order->id,
                   "posten_id" => $posten_id,
                   "ust_id" => 1,
                   "konto_id" => $order->konto_id,
                   "zahlung_anzahl" => 1,
                   "zahlung_typ" => 12,
                   "zahlung_andereskonto" => 0,
                   "zahlung_stapel" => 1,
                   "zahlung_betrag" => $order->coupon_prices_de,
                   "zahlung_netto" => 0,
                   "zahlung_text" => "Coupon: giftbox free",
                   "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                   "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                   "zahlung_created" => date('Y/m/d H:i:s a', time()),
                   "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                   "currency_id" => 1,
                   "zahlung_steuersatz" => 0,
                   "steuersatz_id" => $steuersatz_id,
                   )
            );
	    }
	    }
	    
	    if($posten->id !="" || $posten->id > 0)
        {
            
                $posten->edit(
                    array(
                        "steuerart_id" => $steuerart_id,
                        "artikel_id" => $couponArticle->id,
                        "posten_anzahl" => $quantity,
                        "posten_einzelbetrag" => $postenAmount,
                        "posten_netto" => 0,
                        "posten_text" => mysql_real_escape_string($couponArticle->name),
                        "posten_manuell" => 0,
                        "posten_updated" => date('Y/m/d H:i:s a', time()),
                        "order_id" => $order->id,
                        "currency_id" => 1,
                        "steuersatz_id" => $steuersatz_id
                    )
                );
                
                $zahlungen->initWithPostenId($posten->id);
    	        $zahlungen->edit(
                    array(
                        "steuerart_id" => $steuerart_id,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "ust_id" => 1,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => $postenAmount,
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($couponArticle->name),
                        "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => $steuersatz_id
                        )
                );
    	        
    	        $zahlungen->initChild();
    	        $zahlungen->edit(
                    array(
                        "steuerart_id" => $steuerart_id,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "order_id" => $order->id,
                        "ust_id" => 1,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => -1 * floatVal($postenAmount),
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($couponArticle->name),
                        "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => $steuersatz_id
                        )
                );
        }
        else
        {
            //add
            
                $posten_id = $posten->add(
                    array(
                        "steuerart_id" => $steuerart_id,
                        "konto_id" => $konto_id,
                        "artikel_id" => $couponArticle->id,
                        "posten_anzahl" => $quantity,
                        "posten_einzelbetrag" => $postenAmount,
                        "posten_netto" => 0,
                        "posten_text" => mysql_real_escape_string($couponArticle->name),
                        "posten_datum" => date('Y/m/d H:i:s a', time()),
                        "posten_manuell" => 0,
                        "posten_created" => date('Y/m/d H:i:s a', time()),
                        "posten_updated" => date('Y/m/d H:i:s a', time()),
                        "order_id" => $order->id,
                        "currency_id" => 1,
                        "steuersatz_id" => $steuersatz_id
                    )
                );
    	        
    	        $zahlung_parent_id = $zahlungen->add(
                    array(
                        "steuerart_id" => $steuerart_id,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "posten_id" => $posten_id,
                        "order_id" => $order->id,
                        "ust_id" => 1,
                        "konto_id" => $couponArticle->konto_id,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => $postenAmount,
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($couponArticle->name),
                        "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                        "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                        "zahlung_created" => date('Y/m/d H:i:s a', time()),
                        "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => $steuersatz_id
                        )
                );
                
                $zahlung_child_id = $zahlungen->add(
                    array(
                        "steuerart_id" => $steuerart_id,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "parent_id" => $zahlung_parent_id,
                        "posten_id" => $posten_id,
                        "order_id" => $order->id,
                        "ust_id" => 1,
                        "konto_id" => $konto_id,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => -1 * floatVal($postenAmount),
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($couponArticle->name),
                        "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                        "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                        "zahlung_created" => date('Y/m/d H:i:s a', time()),
                        "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => $steuersatz_id,
                        )
                );
        }
	}
   
	
	public function getPnArticle()
	{
	    $article = new PnArticle();
	    $article->initWithId($this->artikel_id);
	    
	    return $article;
	}
	
	function getSupportLocales()
	{
	    $objDb = $this->Database->prepare("
            SELECT locale_id FROM " . $this->config->database . ".coupon_locale
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $locales = array();
        while($objDb->next())
        {
            $locales[] = $objDb->locale_id;
        }
        
        return $locales;
	}
	
	function getSupportShippingCountries()
	{
	    $objDb = $this->Database->prepare("
            SELECT country_id FROM " . $this->config->database . ".coupon_shipping_countries
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $countries = array();
        while($objDb->next())
        {
            $countries[] = $objDb->country_id;
        }
        
        return $countries;
	}
	
	function getSupportDesigns()
	{
	    $objDb = $this->Database->prepare("
            SELECT design_id FROM " . $this->config->database . ".coupon_design
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $designs = array();
        while($objDb->next())
        {
            $designs[] = $objDb->design_id;
        }
        
        return $designs;
	}
	
	function getSupportGiftboxes()
	{
	    $objDb = $this->Database->prepare("
            SELECT giftbox_id FROM " . $this->config->database . ".coupon_giftbox
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $giftboxes = array();
        while($objDb->next())
        {
            $giftboxes[] = $objDb->giftbox_id;
        }
        
        return $giftboxes;
	}
	
	function getSupportBooks()
	{
	    $objDb = $this->Database->prepare("
            SELECT buch_id FROM " . $this->config->database . ".coupon_buch
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $books = array();
        while($objDb->next())
        {
            $books[] = $objDb->buch_id;
        }
        
        return $books;
	}
	
	function getSupportWidmungs()
	{
	    $objDb = $this->Database->prepare("
            SELECT artikel_id FROM " . $this->config->database . ".coupon_artikel
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $widmungs = array();
        while($objDb->next())
        {
            $widmungs[] = $objDb->artikel_id;
        }
        
        return $widmungs;
	}
	
	function getSupportClasses()
	{
	    $objDb = $this->Database->prepare("
            SELECT class_id FROM " . $this->config->database . ".coupon_artikel_klassen
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $classes = array();
        while($objDb->next())
        {
            $classes[] = $objDb->class_id;
        }
        
        return $classes;
	}
	
	function getSupportVersions()
	{
	    $objDb = $this->Database->prepare("
            SELECT version_id FROM " . $this->config->database . ".coupon_version
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $versions = array();
        while($objDb->next())
        {
            $versions[] = $objDb->version_id;
        }
        
        return $versions;
	}
	
	function getSupportTypes()
	{
	    $objDb = $this->Database->prepare("
            SELECT type_id FROM " . $this->config->database . ".coupon_type
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $types = array();
        while($objDb->next())
        {
            $types[] = $objDb->type_id;
        }
        
        return $types;
	}
	
	function getSupportReftags()
	{
	    $objDb= $this->Database->prepare("
	        SELECT ref_id FROM " . $this->config->database . ".coupon_reftag
            WHERE coupon_id = ?
        ")->execute($this->id);
        
        $refs = array();
        while($objDb->next())
        {
            $refs[] = $objDb->ref_id;
        }
        
        return $refs;
	}
	
	public function makeCoupon($order_id, $prefix="", $value=10)
	{
	    $invalid_token = true;
	    while($invalid_token)
	    {
	        $token = $prefix . $this->config->random_string(10);
	        $objDb = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".coupons
				WHERE coupon_token = ?
			")->execute($token);
	        
	        if($objDb->numRows == 0)
	            $invalid_token = false;
	    }
	    
	    $konto_id = 1089521; //Rabatt
	    $artikel_id = 17371; //Rabatt buch
	    $wert = $value;
	    $created = date('Y/m/d H:i:s a', time());
	    $expiredate = date('Y-m-d', strtotime("+6 months", strtotime($created)));;
	    $anzahlbuecher = 1;
	    $multiple_use = 0;
	    $constrained = 1;
	    $coupontype_id = 1;
	    $wert2 = 0.00;
	    $locale_id = 1;
	    $partner_id = 1;
	    $mindestbestellwert = 0.00;
	    
	    $coupon_id = $this->add(array(
	        "konto_id" => $konto_id,
	        "artikel_id" => $artikel_id,
	        "coupon_token" => $token,
	        "coupon_wert" => $wert,
	        "coupon_created" => $created,
	        "coupon_expiredate" => $expiredate,
	        "coupon_anzahlbuecher" => $anzahlbuecher,
	        "coupon_multiple_use" => $multiple_use,
	        "coupon_constrained" => $constrained,
	        "coupontype_id" => $coupontype_id,
	        "coupon_wert2" => $wert2,
	        "locale_id" => $locale_id,
	        "partner_id" => $partner_id,
	        "barcode_id" => $order_id,
	        "coupon_mindestbestellwert" => $mindestbestellwert
	        ));
	    
	    $this->Database->prepare("
				INSERT INTO " . $this->config->database . ".coupon_artikel_klassen 
				(coupon_id, class_id) VALUES(?, ?)
				")->execute($coupon_id, 2);
				
	    return $coupon_id;
	}
	
	public function add($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = mysql_real_escape_string($v);
		}
		
		$object = $this->Database->prepare("
				INSERT INTO " . $this->config->database . ".coupons (" . implode(", ", $key) .") VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $object->insertId;
	}
}

?>
