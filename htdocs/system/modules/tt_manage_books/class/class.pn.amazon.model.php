<?php

class PnAmazonModel
{
    protected  $_fields = array ();
    
    public function __construct($data = null)
    {
        if (!is_null($data)) {
            if ($this->_isAssociativeArray($data)) {
                $this->_fromAssociativeArray($data);
            } elseif ($this->_isDOMElement($data)) {
                $this->_fromDOMElement($data);
            } else {
                throw new Exception ("Unable to construct from provided data. 
                                Please be sure to pass associative array or DOMElement");
            }
            
        }
    }
    
    public function __get($propertyName)
    {
       $getter = "get$propertyName"; 
       return $this->$getter();
    }
    
    public function __set($propertyName, $propertyValue)
    {
       $setter = "set$propertyName";
       $this->$setter($propertyValue);
       return $this;
    }
    
    protected function _toXMLFragment() 
    {
        $xml = "";
        foreach ($this->_fields as $fieldName => $field) {
            $fieldValue = $field['FieldValue'];
            if (!is_null($fieldValue)) {
                $fieldType = $field['FieldType'];
                if (is_array($fieldType)) {
                    if ($this->_isComplexType($fieldType[0])) {
                        foreach ($fieldValue as $Item) {
                            $xml .= "<$fieldName>";
                            $xml .= $Item->_toXMLFragment();
                            $xml .= "</$fieldName>";
                        }
                    } else {
                        foreach ($fieldValue as $Item) {
                            $xml .= "<$fieldName>";
                            $xml .= $this->_escapeXML($Item);
                            $xml .= "</$fieldName>";
                        }
                    }
                } else {
                    if ($this->_isComplexType($fieldType)) {
                        $xml .= "<$fieldName>";
                        $xml .= $fieldValue->_toXMLFragment();
                        $xml .= "</$fieldName>";
                    } else {
                        $xml .= "<$fieldName>";
                        $xml .= $this->_escapeXML($fieldValue);
                        $xml .= "</$fieldName>";
                    }
                }
            }
        }
        return $xml;
    }
    
    private function _escapeXML($str) 
    {
        $from = array( "&", "<", ">", "'", "\""); 
        $to = array( "&amp;", "&lt;", "&gt;", "&#039;", "&quot;");
        return str_replace($from, $to, $str); 
    }
    
    private function _fromDOMElement(DOMElement $dom)
    {
        $xpath = new DOMXPath($dom->ownerDocument);
        $xpath->registerNamespace('a','http://payments.amazon.com/checkout/v2/2010-08-31/'); 
        
        foreach ($this->_fields as $fieldName => $field) {
            $fieldType = $field['FieldType'];   
            if (is_array($fieldType)) {
                if ($this->_isComplexType($fieldType[0])) {
                    $elements = $xpath->query("./a:$fieldName", $dom);
                    if ($elements->length >= 1) {
                        foreach ($elements as $element) {
                            $this->_fields[$fieldName]['FieldValue'][] = new $fieldType[0]($element);
                        }
                    } 
                } else {
                    $elements = $xpath->query("./a:$fieldName", $dom);
                    if ($elements->length >= 1) {
                        foreach ($elements as $element) {
                            $Text = $xpath->query('./text()', $element);
                            $this->_fields[$fieldName]['FieldValue'][] = $Text->item(0)->data;
                        }
                    }  
                }
            } else {
                if ($this->_isComplexType($fieldType)) {
                    $elements = $xpath->query("./a:$fieldName", $dom);
                    if ($elements->length == 1) {
                        $this->_fields[$fieldName]['FieldValue'] = new $fieldType($elements->item(0));
                    }   
                } else {
                    $element = $xpath->query("./a:$fieldName/text()", $dom);
                    if ($element->length == 1) {
                        $this->_fields[$fieldName]['FieldValue'] = $element->item(0)->data;
                    }
                }
            }
        }
    }
    
    private function _fromAssociativeArray(array $array)
    {
        foreach ($this->_fields as $fieldName => $field) {
            $fieldType = $field['FieldType'];   
            if (is_array($fieldType)) {
                if ($this->_isComplexType($fieldType[0])) {
                    if (array_key_exists($fieldName, $array)) { 
                        $elements = $array[$fieldName];
                        if (!$this->_isNumericArray($elements)) {
                            $elements =  array($elements);    
                        }
                        if (count ($elements) >= 1) {
                            require_once (str_replace('_', DIRECTORY_SEPARATOR, $fieldType[0]) . ".php");
                            foreach ($elements as $element) {
                                $this->_fields[$fieldName]['FieldValue'][] = new $fieldType[0]($element);
                            }
                        }
                    } 
                } else {
                    if (array_key_exists($fieldName, $array)) {
                        $elements = $array[$fieldName];
                        if (!$this->_isNumericArray($elements)) {
                            $elements =  array($elements);    
                            }
                        if (count ($elements) >= 1) {
                            foreach ($elements as $element) {
                                $this->_fields[$fieldName]['FieldValue'][] = $element;
                            }
                        }  
                    }
                }
            } else {
                if ($this->_isComplexType($fieldType)) {
                    if (array_key_exists($fieldName, $array)) {
                        require_once (str_replace('_', DIRECTORY_SEPARATOR, $fieldType) . ".php");
                        $this->_fields[$fieldName]['FieldValue'] = new $fieldType($array[$fieldName]);
                    }   
                } else {
                    if (array_key_exists($fieldName, $array)) {
                        $this->_fields[$fieldName]['FieldValue'] = $array[$fieldName];
                    }
                }
            }
        }
    }
    
    private function _isComplexType ($fieldType) 
    {
        return preg_match('/^PnAmazonModel/', $fieldType);
    }
    
    private function _isAssociativeArray($var) 
    {
        return is_array($var) && array_keys($var) !== range(0, sizeof($var) - 1);
    }
    
    private function _isDOMElement($var) 
    {
        return $var instanceof DOMElement;
    }
    
    protected function _isNumericArray($var) 
    {
        return is_array($var) && array_keys($var) === range(0, sizeof($var) - 1);
    }
}

class PnAmazonModelCreatePurchaseContractRequest extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
            'PurchaseContractMetadata' => array('FieldValue' => null, 'FieldType' => 'byte[]'),
        );
        parent::__construct($data);
    }
    
    public function getPurchaseContractMetadata() 
    {
        return $this->_fields['PurchaseContractMetadata']['FieldValue'];
    }
    
    public function setPurchaseContractMetadata($value) 
    {
        $this->_fields['PurchaseContractMetadata']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetPurchaseContractMetadata()
    {
        return !is_null($this->_fields['PurchaseContractMetadata']['FieldValue']);
    }
}

class PnAmazonModelGetPurchaseContractRequest extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
            'PurchaseContractId' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }
    
    public function getPurchaseContractId() 
    {
        return $this->_fields['PurchaseContractId']['FieldValue'];
    }
    
    public function setPurchaseContractId($value) 
    {
        $this->_fields['PurchaseContractId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetPurchaseContractId()
    {
        return !is_null($this->_fields['PurchaseContractId']['FieldValue']);
    }
}

class PnAmazonModelGetPurchaseContractResponse extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'GetPurchaseContractResult' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelGetPurchaseContractResult'),
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelResponseMetadata'),
        );
        parent::__construct($data);
    }
    
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
    	$xpath->registerNamespace('a','http://payments.amazon.com/checkout/v2/2010-08-31/'); 
        $response = $xpath->query('//a:GetPurchaseContractResponse');
        if ($response->length == 1) {
            return new PnAmazonModelGetPurchaseContractResponse(($response->item(0))); 
        } else {
            throw new Exception ("Unable to construct PnAmazonModelGetPurchaseContractResponse from provided XML. 
                                  Make sure that GetPurchaseContractResponse is a root element");
        }
    }
    
    public function getGetPurchaseContractResult() 
    {
        return $this->_fields['GetPurchaseContractResult']['FieldValue'];
    }
    
    public function setGetPurchaseContractResult($value) 
    {
        $this->_fields['GetPurchaseContractResult']['FieldValue'] = $value;
        return;
    }
    
    public function isSetGetPurchaseContractResult()
    {
        return !is_null($this->_fields['GetPurchaseContractResult']['FieldValue']);
    }
    
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }
    
    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }
    
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);
    }
    
    public function toXML() 
    {
        $xml = "";
        $xml .= "<GetPurchaseContractResponse xmlns=\"http://payments.amazon.com/checkout/v2/2010-08-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</GetPurchaseContractResponse>";
        return $xml;
    }
}

class PnAmazonModelGetPurchaseContractResult extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'PurchaseContract' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPurchaseContract'),
        );
        parent::__construct($data);
    }
    
    public function getPurchaseContract() 
    {
        return $this->_fields['PurchaseContract']['FieldValue'];
    }
    
    public function setPurchaseContract($value) 
    {
        $this->_fields['PurchaseContract']['FieldValue'] = $value;
        return;
    }
    
    public function isSetPurchaseContract()
    {
        return !is_null($this->_fields['PurchaseContract']['FieldValue']);
    }
}

class PnAmazonModelPurchaseContract extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Id' => array('FieldValue' => null, 'FieldType' => 'IdType'),
        'ExpirationTimeStamp' => array('FieldValue' => null, 'FieldType' => 'string'),
        'MerchantId' => array('FieldValue' => null, 'FieldType' => 'IdType'),
        'MarketplaceId' => array('FieldValue' => null, 'FieldType' => 'IdType'),
        'State' => array('FieldValue' => null, 'FieldType' => 'PurchaseContractState'),
        'Metadata' => array('FieldValue' => null, 'FieldType' => 'byte[]'),
        'Destinations' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelDestinationList'),
        'PurchaseItems' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelItemList'),
        'Charges' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelCharges'),
        );
        parent::__construct($data);
    }
    
    public function getId() 
    {
        return $this->_fields['Id']['FieldValue'];
    }
    
    public function setId($value) 
    {
        $this->_fields['Id']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetId()
    {
        return !is_null($this->_fields['Id']['FieldValue']);
    }
    
    public function getExpirationTimeStamp() 
    {
        return $this->_fields['ExpirationTimeStamp']['FieldValue'];
    }
    
    public function setExpirationTimeStamp($value) 
    {
        $this->_fields['ExpirationTimeStamp']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetExpirationTimeStamp()
    {
        return !is_null($this->_fields['ExpirationTimeStamp']['FieldValue']);
    }
    
    public function getMerchantId() 
    {
        return $this->_fields['MerchantId']['FieldValue'];
    }
    
    public function setMerchantId($value) 
    {
        $this->_fields['MerchantId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetMerchantId()
    {
        return !is_null($this->_fields['MerchantId']['FieldValue']);
    }
    
    public function getMarketplaceId() 
    {
        return $this->_fields['MarketplaceId']['FieldValue'];
    }
    
    public function setMarketplaceId($value) 
    {
        $this->_fields['MarketplaceId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetMarketplaceId()
    {
        return !is_null($this->_fields['MarketplaceId']['FieldValue']);
    }
    
    public function getState() 
    {
        return $this->_fields['State']['FieldValue'];
    }
    
    public function setState($value) 
    {
        $this->_fields['State']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetState()
    {
        return !is_null($this->_fields['State']['FieldValue']);
    }
    
    public function getMetadata() 
    {
        return $this->_fields['Metadata']['FieldValue'];
    }
    
    public function setMetadata($value) 
    {
        $this->_fields['Metadata']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetMetadata()
    {
        return !is_null($this->_fields['Metadata']['FieldValue']);
    }
    
    public function getDestinations() 
    {
        return $this->_fields['Destinations']['FieldValue'];
    }
    
    public function setDestinations($value) 
    {
        $this->_fields['Destinations']['FieldValue'] = $value;
        return;
    }
    
    public function isSetDestinations()
    {
        return !is_null($this->_fields['Destinations']['FieldValue']);

    }
    
    public function getPurchaseItems() 
    {
        return $this->_fields['PurchaseItems']['FieldValue'];
    }
    
    public function setPurchaseItems($value) 
    {
        $this->_fields['PurchaseItems']['FieldValue'] = $value;
        return;
    }
    
    public function isSetPurchaseItems()
    {
        return !is_null($this->_fields['PurchaseItems']['FieldValue']);

    }
    
    public function getCharges() 
    {
        return $this->_fields['Charges']['FieldValue'];
    }
    
    public function setCharges($value) 
    {
        $this->_fields['Charges']['FieldValue'] = $value;
        return;
    }
    
    public function isSetCharges()
    {
        return !is_null($this->_fields['Charges']['FieldValue']);

    }
}

class PnAmazonModelResponseMetadata extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'RequestId' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }
    
    public function getRequestId() 
    {
        return $this->_fields['RequestId']['FieldValue'];
    }
    
    public function setRequestId($value) 
    {
        $this->_fields['RequestId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetRequestId()
    {
        return !is_null($this->_fields['RequestId']['FieldValue']);
    }
}

class PnAmazonModelCreatePurchaseContractResponse extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'CreatePurchaseContractResult' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelCreatePurchaseContractResult'),
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelResponseMetadata'),
        );
        parent::__construct($data);
    }
    
    public static function fromXML($xml)
    {
        $Dom = new DOMDocument();
        $Dom->loadXML($xml);
        $Xpath = new DOMXPath($Dom);
    	$Xpath->registerNamespace('a','http://payments.amazon.com/checkout/v2/2010-08-31/'); 
        $Response = $Xpath->query('//a:CreatePurchaseContractResponse');
        if ($Response->length == 1) {
            return new PnAmazonModelCreatePurchaseContractResponse(($Response->item(0))); 
        } else {
            throw new Exception ("Unable to construct PnAmazonModelCreatePurchaseContractResponse from provided XML. 
                                  Make sure that CreatePurchaseContractResponse is a root element");
        }
          
    }
    
    public function getCreatePurchaseContractResult() 
    {
        return $this->_fields['CreatePurchaseContractResult']['FieldValue'];
    }
    
    public function setCreatePurchaseContractResult($value) 
    {
        $this->_fields['CreatePurchaseContractResult']['FieldValue'] = $value;
        return;
    }
    
    public function isSetCreatePurchaseContractResult()
    {
        return !is_null($this->_fields['CreatePurchaseContractResult']['FieldValue']);

    }
    
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }
    
    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }
    
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);

    }
    
    public function toXML() 
    {
        $xml = "";
        $xml .= "<CreatePurchaseContractResponse xmlns=\"http://payments.amazon.com/checkout/v2/2010-08-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</CreatePurchaseContractResponse>";
        return $xml;
    }
}

class PnAmazonModelDestinationList extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Destination' => array('FieldValue' => array(), 'FieldType' => array('PnAmazonModelDestination')),
        );
        parent::__construct($data);
    }
    
    public function getDestination() 
    {
        return $this->_fields['Destination']['FieldValue'];
    }
    
    public function setDestination($destination) 
    {
        if (!$this->_isNumericArray($destination)) {
            $destination =  array ($destination);    
        }
        $this->_fields['Destination']['FieldValue'] = $destination;
        return $this;
    }
    
    public function isSetDestination()
    {
        return count ($this->_fields['Destination']['FieldValue']) > 0;
    }
}

class PnAmazonModelDestination extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'DestinationName' => array('FieldValue' => null, 'FieldType' => 'string'),
        'DestinationType' => array('FieldValue' => null, 'FieldType' => 'DestinationType'),
        'PhysicalDestinationAttributes' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPhysicalDestinationAttributes'),
        'DigitalDestinationAttributes' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelDigitalDestinationAttributes'),
        );
        parent::__construct($data);
    }
    
    public function getDestinationName() 
    {
        return $this->_fields['DestinationName']['FieldValue'];
    }
    
    public function setDestinationName($value) 
    {
        $this->_fields['DestinationName']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetDestinationName()
    {
        return !is_null($this->_fields['DestinationName']['FieldValue']);
    }
    
    public function getDestinationType() 
    {
        return $this->_fields['DestinationType']['FieldValue'];
    }
    
    public function setDestinationType($value) 
    {
        $this->_fields['DestinationType']['FieldValue'] = $value;
        return $this;
    }
    
    public function withDestinationType($value)
    {
        $this->setDestinationType($value);
        return $this;
    }
    
    public function isSetDestinationType()
    {
        return !is_null($this->_fields['DestinationType']['FieldValue']);
    }
    
    public function getPhysicalDestinationAttributes() 
    {
        return $this->_fields['PhysicalDestinationAttributes']['FieldValue'];
    }
    
    public function setPhysicalDestinationAttributes($value) 
    {
        $this->_fields['PhysicalDestinationAttributes']['FieldValue'] = $value;
        return;
    }
    
    public function withPhysicalDestinationAttributes($value)
    {
        $this->setPhysicalDestinationAttributes($value);
        return $this;
    }
    
    public function isSetPhysicalDestinationAttributes()
    {
        return !is_null($this->_fields['PhysicalDestinationAttributes']['FieldValue']);
    }
    
    public function getDigitalDestinationAttributes() 
    {
        return $this->_fields['DigitalDestinationAttributes']['FieldValue'];
    }
    
    public function setDigitalDestinationAttributes($value) 
    {
        $this->_fields['DigitalDestinationAttributes']['FieldValue'] = $value;
        return;
    }
    
    public function withDigitalDestinationAttributes($value)
    {
        $this->setDigitalDestinationAttributes($value);
        return $this;
    }
    
    public function isSetDigitalDestinationAttributes()
    {
        return !is_null($this->_fields['DigitalDestinationAttributes']['FieldValue']);
    }
}

class PnAmazonModelPhysicalDestinationAttributes extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'ShippingAddress' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelShippingAddress'),
        );
        parent::__construct($data);
    }
    
    public function getShippingAddress() 
    {
        return $this->_fields['ShippingAddress']['FieldValue'];
    }
    
    public function setShippingAddress($value) 
    {
        $this->_fields['ShippingAddress']['FieldValue'] = $value;
        return;
    }
    
    public function isSetShippingAddress()
    {
        return !is_null($this->_fields['ShippingAddress']['FieldValue']);
    }
}

class PnAmazonModelShippingAddress extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Name' => array('FieldValue' => null, 'FieldType' => 'string'),
        'AddressLineOne' => array('FieldValue' => null, 'FieldType' => 'string'),
        'AddressLineTwo' => array('FieldValue' => null, 'FieldType' => 'string'),
        'AddressLineThree' => array('FieldValue' => null, 'FieldType' => 'string'),
        'City' => array('FieldValue' => null, 'FieldType' => 'string'),
        'StateOrProvinceCode' => array('FieldValue' => null, 'FieldType' => 'string'),
        'PostalCode' => array('FieldValue' => null, 'FieldType' => 'string'),
        'CountryCode' => array('FieldValue' => null, 'FieldType' => 'string'),
        'PhoneNumber' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }
    
    public function getName() 
    {
        return $this->_fields['Name']['FieldValue'];
    }
    
    public function setName($value) 
    {
        $this->_fields['Name']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetName()
    {
        return !is_null($this->_fields['Name']['FieldValue']);
    }
    
    public function getAddressLineOne() 
    {
        return $this->_fields['AddressLineOne']['FieldValue'];
    }
    
    public function setAddressLineOne($value) 
    {
        $this->_fields['AddressLineOne']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetAddressLineOne()
    {
        return !is_null($this->_fields['AddressLineOne']['FieldValue']);
    }
    
    public function getAddressLineTwo() 
    {
        return $this->_fields['AddressLineTwo']['FieldValue'];
    }
    
    public function setAddressLineTwo($value) 
    {
        $this->_fields['AddressLineTwo']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetAddressLineTwo()
    {
        return !is_null($this->_fields['AddressLineTwo']['FieldValue']);
    }
    
    public function getAddressLineThree() 
    {
        return $this->_fields['AddressLineThree']['FieldValue'];
    }
    
    public function setAddressLineThree($value) 
    {
        $this->_fields['AddressLineThree']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetAddressLineThree()
    {
        return !is_null($this->_fields['AddressLineThree']['FieldValue']);
    }
    
    public function getCity() 
    {
        return $this->_fields['City']['FieldValue'];
    }
    
    public function setCity($value) 
    {
        $this->_fields['City']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetCity()
    {
        return !is_null($this->_fields['City']['FieldValue']);
    }
    
    public function getStateOrProvinceCode() 
    {
        return $this->_fields['StateOrProvinceCode']['FieldValue'];
    }
    
    public function setStateOrProvinceCode($value) 
    {
        $this->_fields['StateOrProvinceCode']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetStateOrProvinceCode()
    {
        return !is_null($this->_fields['StateOrProvinceCode']['FieldValue']);
    }
    
    public function getPostalCode() 
    {
        return $this->_fields['PostalCode']['FieldValue'];
    }
    
    public function setPostalCode($value) 
    {
        $this->_fields['PostalCode']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetPostalCode()
    {
        return !is_null($this->_fields['PostalCode']['FieldValue']);
    }
    
    public function getCountryCode() 
    {
        return $this->_fields['CountryCode']['FieldValue'];
    }
    
    public function setCountryCode($value) 
    {
        $this->_fields['CountryCode']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetCountryCode()
    {
        return !is_null($this->_fields['CountryCode']['FieldValue']);
    }
    
    public function getPhoneNumber() 
    {
        return $this->_fields['PhoneNumber']['FieldValue'];
    }
    
    public function setPhoneNumber($value) 
    {
        $this->_fields['PhoneNumber']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetPhoneNumber()
    {
        return !is_null($this->_fields['PhoneNumber']['FieldValue']);
    }
}

class PnAmazonModelItemList extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'PurchaseItem' => array('FieldValue' => array(), 'FieldType' => array('PnAmazonModelPurchaseItem')),
        );
        parent::__construct($data);
    }
    
    public function getPurchaseItem() 
    {
        return $this->_fields['PurchaseItem']['FieldValue'];
    }
    
    public function setPurchaseItem($purchaseItem) 
    {
        if (!$this->_isNumericArray($purchaseItem)) 
        {
            $purchaseItem =  array ($purchaseItem);    
        }
        $this->_fields['PurchaseItem']['FieldValue'] = $purchaseItem;
        return $this;
    }
    
    public function isSetPurchaseItem()
    {
        return count ($this->_fields['PurchaseItem']['FieldValue']) > 0;
    }
    
    public function setPurchaseItemWithMerchantItemId($purchaseItem,$merchantItemID)
    {
         $this->_fields['PurchaseItem']['FieldValue'][$merchantItemID] = $purchaseItem;
    }
    
    public function getpurchaseItemWithMerchantItemId($merchantItemID)
    {
         return $this->_fields['PurchaseItem']['FieldValue'][$merchantItemID];
    }
    
    public function isSetPurchaseItemWithMerchantItemId($merchantItemID)
    {
        return count ($this->_fields['PurchaseItem']['FieldValue'][$merchantItemID]) > 0;
    }
    
    public function addItem($purchaseItem)
    {
        $merchantItemID = $purchaseItem->_fields['MerchantItemId']['FieldValue'];       
        $this->_fields['PurchaseItem']['FieldValue'][$merchantItemID] = $purchaseItem;
    }
}

class PnAmazonModelPurchaseItem extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'MerchantItemId' => array('FieldValue' => null, 'FieldType' => 'IdType'),
        'SKU' => array('FieldValue' => null, 'FieldType' => 'string'),
        'MerchantId' => array('FieldValue' => null, 'FieldType' => 'IdType'),
        'Title' => array('FieldValue' => null, 'FieldType' => 'string'),
        'Description' => array('FieldValue' => null, 'FieldType' => 'string'),
        'UnitPrice' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPrice'),
        'Quantity' => array('FieldValue' => null, 'FieldType' => 'PositiveInteger'),
        'URL' => array('FieldValue' => null, 'FieldType' => 'string'),
        'Category' => array('FieldValue' => null, 'FieldType' => 'string'),
        'FulfillmentNetwork' => array('FieldValue' => null, 'FieldType' => 'FulfillmentNetwork'),
        'ItemCustomData' => array('FieldValue' => null, 'FieldType' => 'string'),
        'ProductType' => array('FieldValue' => null, 'FieldType' => 'ProductType'),
        'PhysicalProductAttributes' => array('FieldValue' => null, 'FieldType' => 'AmazonModelPhysicalProductAttributes'),
        'DigitalProductAttributes' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelDigitalProductAttributes'),
        );
        parent::__construct($data);
    }
    
    public function getMerchantItemId() 
    {
        return $this->_fields['MerchantItemId']['FieldValue'];
    }
    
    public function setMerchantItemId($value) 
    {
        $this->_fields['MerchantItemId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetMerchantItemId()
    {
        return !is_null($this->_fields['MerchantItemId']['FieldValue']);
    }
    
    public function getSKU() 
    {
        return $this->_fields['SKU']['FieldValue'];
    }
    
    public function setSKU($value) 
    {
        $this->_fields['SKU']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetSKU()
    {
        return !is_null($this->_fields['SKU']['FieldValue']);
    }
    
    public function getMerchantId() 
    {
        return $this->_fields['MerchantId']['FieldValue'];
    }
    
    public function setMerchantId($value) 
    {
        $this->_fields['MerchantId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetMerchantId()
    {
        return !is_null($this->_fields['MerchantId']['FieldValue']);
    }
    
    public function getTitle() 
    {
        return $this->_fields['Title']['FieldValue'];
    }
    
    public function setTitle($value) 
    {
        $this->_fields['Title']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetTitle()
    {
        return !is_null($this->_fields['Title']['FieldValue']);
    }
    
    public function getDescription() 
    {
        return $this->_fields['Description']['FieldValue'];
    }
    
    public function setDescription($value) 
    {
        $this->_fields['Description']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetDescription()
    {
        return !is_null($this->_fields['Description']['FieldValue']);
    }
    
    public function getUnitPrice() 
    {
        return $this->_fields['UnitPrice']['FieldValue'];
    }
    
    public function setUnitPrice($value) 
    {
        $this->_fields['UnitPrice']['FieldValue'] = $value;
        return;
    }
    
    public function isSetUnitPrice()
    {
        return !is_null($this->_fields['UnitPrice']['FieldValue']);

    }
    
    public function getQuantity() 
    {
        return $this->_fields['Quantity']['FieldValue'];
    }
    
    public function setQuantity($value) 
    {
        $this->_fields['Quantity']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetQuantity()
    {
        return !is_null($this->_fields['Quantity']['FieldValue']);
    }
    
    public function getURL() 
    {
        return $this->_fields['URL']['FieldValue'];
    }
    
    public function setURL($value) 
    {
        $this->_fields['URL']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetURL()
    {
        return !is_null($this->_fields['URL']['FieldValue']);
    }
    
    public function getCategory() 
    {
        return $this->_fields['Category']['FieldValue'];
    }
    
    public function setCategory($value) 
    {
        $this->_fields['Category']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetCategory()
    {
        return !is_null($this->_fields['Category']['FieldValue']);
    }
    
    public function getFulfillmentNetwork() 
    {
        return $this->_fields['FulfillmentNetwork']['FieldValue'];
    }
    
    public function setFulfillmentNetwork($value) 
    {
        $this->_fields['FulfillmentNetwork']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetFulfillmentNetwork()
    {
        return !is_null($this->_fields['FulfillmentNetwork']['FieldValue']);
    }
    
    public function getItemCustomData() 
    {
        return $this->_fields['ItemCustomData']['FieldValue'];
    }
    
    public function setItemCustomData($value) 
    {
        $this->_fields['ItemCustomData']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetItemCustomData()
    {
        return !is_null($this->_fields['ItemCustomData']['FieldValue']);
    }
    
    public function getProductType() 
    {
        return $this->_fields['ProductType']['FieldValue'];
    }
    
    public function setProductType($value) 
    {
        $this->_fields['ProductType']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetProductType()
    {
        return !is_null($this->_fields['ProductType']['FieldValue']);
    }
    
    public function getPhysicalProductAttributes() 
    {
        return $this->_fields['PhysicalProductAttributes']['FieldValue'];
    }
    
    public function setPhysicalProductAttributes($value) 
    {
        $this->_fields['PhysicalProductAttributes']['FieldValue'] = $value;
        return;
    }
    
    public function isSetPhysicalProductAttributes()
    {
        return !is_null($this->_fields['PhysicalProductAttributes']['FieldValue']);

    }
    
    public function getDigitalProductAttributes() 
    {
        return $this->_fields['DigitalProductAttributes']['FieldValue'];
    }
    
    public function setDigitalProductAttributes($value) 
    {
        $this->_fields['DigitalProductAttributes']['FieldValue'] = $value;
        return;
    }
    
    public function isSetDigitalProductAttributes()
    {
        return !is_null($this->_fields['DigitalProductAttributes']['FieldValue']);

    }
    
    public function createItem($merchantItemId,$title,$unitPriceAmount)
    {
      $this->setMerchantId(PnAmazonConfig::getMerchantId());
      $this->setTitle($title);
      $this->setMerchantItemId($merchantItemId);
      $this->setUnitPrice(new PnAmazonModelPrice(array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(), 'Amount' => $unitPriceAmount)));
        return $this;
    }
    
    public function createPhysicalItem($merchantItemId,$title,$unitPriceAmount,$deliveryMethod)
    {
      $Item = $this->createItem($merchantItemId,$title,$unitPriceAmount);  
      $deliveryObject = new PnAmazonModelDeliveryMethod();
      $deliveryObject->setServiceLevel($deliveryMethod);
      $physicalAttrObject = new PnAmazonModelPhysicalProductAttributes();
      $physicalAttrObject->setDeliveryMethod($deliveryObject);
      $Item->setPhysicalProductAttributes($physicalAttrObject);
      return $Item;
    }
    
    public function setShippingLabel($label)
    {
        $this->getPhysicalProductAttributes()->getDeliveryMethod()->setDisplayableShippingLabel($label);        
        return $this;
    }
    
    public function setDestinationName($destinationName)
    {
        $this->getPhysicalProductAttributes()->getDeliveryMethod()->setDestinationName($destinationName);
        return $this;
    }
    
    public function setShippingCustomData($shippingCustomData)
    {
        $this->getPhysicalProductAttributes()->getDeliveryMethod()->setShippingCustomData($shippingCustomData);
        return $this;
    }
    
    public function setItemTax($taxAmount)
    {
        if($this->isSetPhysicalProductAttributes())
        {
            if($this->getPhysicalProductAttributes()->isSetItemCharges())
            {
                $this->getPhysicalProductAttributes()->getItemCharges()->setTax(new                                                   
                                   PnAmazonModelPrice(array
                    ('CurrencyCode' => CheckoutByAmazon_Service_MerchantValues::getInstance()->getCurrencyCode(), 'Amount' => $taxAmount)));
            }
            else
            {
                $chargesObject = new PnAmazonModelCharges();
                $chargesObject->setTax(new PnAmazonModelPrice(
                                                   array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(), 
                                                   'Amount' => $taxAmount)));
                $this->getPhysicalProductAttributes()->setItemCharges($chargesObject); 
            }
        }
        else
        {
            $physicalAttribsObj = new PnAmazonModelPhysicalProductAttributes();
            $this->setPhysicalProductAttributes($physicalAttribsObj);
            $chargesObject = new PnAmazonModelCharges();
            $chargesObject->setTax(new PnAmazonModelPrice(
                                                     array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(),
                                                     'Amount' => $taxAmount)));
            $this->getPhysicalProductAttributes()->setItemCharges($chargesObject);
 
        }
         return $this;
    }
    
    public function setItemShippingCharges($shippingAmount)
    {
        if($this->isSetPhysicalProductAttributes())
        {

            if($this->getPhysicalProductAttributes()->isSetItemCharges())
            {
                $this->getPhysicalProductAttributes()->getItemCharges()->setShipping(new
                        PnAmazonModelPrice(array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(),
                        'Amount' => $shippingAmount)));
            }                          
            else                       
            {
                $chargesObject = new PnAmazonModelCharges();
                $chargesObject->setShipping(new PnAmazonModelPrice(
                    array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(), 
                    'Amount' => $shippingAmount)));
                $this->getPhysicalProductAttributes()->setItemCharges($chargesObject); 
            }
        }
        else
        {
            $physicalAttribsObj = new PnAmazonModelPhysicalProductAttributes();
            $this->setPhysicalProductAttributes($physicalAttribsObj);
            $chargesObject = new PnAmazonModelCharges();
            $chargesObject->setShipping(new PnAmazonModelPrice(
                                                        array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(),
                                                        'Amount' => $shippingAmount)));
            $this->getPhysicalProductAttributes()->setItemCharges($chargesObject);

        }   
              
        return $this;
    }
    
    public function setItemPromotions($promotion)
    {
        if($this->isSetPhysicalProductAttributes())
        {

            if($this->getPhysicalProductAttributes()->isSetItemCharges())
            {                    
                $this->getPhysicalProductAttributes()->getItemCharges()->setPromotions($promotion);
            }                          
             else 
            {
                $chargesObject = new PnAmazonModelCharges();
                $chargesObject->setPromotions($promotion);
                $this->getPhysicalProductAttributes()->setItemCharges($chargesObject);
            }
        } 
        else
        {
             $physicalAttribsObj = new PnAmazonModelPhysicalProductAttributes();
             $this->setPhysicalProductAttributes($physicalAttribsObj);
             $chargesObject = new PnAmazonModelCharges();
             $chargesObject->setPromotions($promotion);
             $this->getPhysicalProductAttributes()->setItemCharges($chargesObject);

        } 
        return $this;
    }
    
    public function setWeight($weightValue)
    {
        $weightObject = new PnAmazonModelWeight();
        $weightObject->setValue($weightValue);
        $weightObject->setUnit(PnAmazonConfig::getWeightUnit());
        $this->getPhysicalProductAttributes()->setWeight($weightObject);
        return $this;
    }
    
    public function setCondition($condition)
    {
        $this->getPhysicalProductAttributes()->setCondition($condition);
        return $this;
    }
}

class PnAmazonModelPrice extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Amount' => array('FieldValue' => null, 'FieldType' => 'NonNegativeDouble'),
        'CurrencyCode' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }
    
    public function getAmount() 
    {
        return $this->_fields['Amount']['FieldValue'];
    }
    
    public function setAmount($value) 
    {
        $this->_fields['Amount']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetAmount()
    {
        return !is_null($this->_fields['Amount']['FieldValue']);
    }
    
    public function getCurrencyCode() 
    {
        return $this->_fields['CurrencyCode']['FieldValue'];
    }
    
    public function setCurrencyCode($value) 
    {
        $this->_fields['CurrencyCode']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetCurrencyCode()
    {
        return !is_null($this->_fields['CurrencyCode']['FieldValue']);
    }
}

class PnAmazonModelSetPurchaseItemsRequest extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'PurchaseContractId' => array('FieldValue' => null, 'FieldType' => 'string'),
        'PurchaseItems' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelItemList'),
        );
        parent::__construct($data);
    }
    
    public function getPurchaseContractId() 
    {
        return $this->_fields['PurchaseContractId']['FieldValue'];
    }
    
    public function setPurchaseContractId($value) 
    {
        $this->_fields['PurchaseContractId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetPurchaseContractId()
    {
        return !is_null($this->_fields['PurchaseContractId']['FieldValue']);
    }
    
    public function getPurchaseItems() 
    {
        return $this->_fields['PurchaseItems']['FieldValue'];
    }
    
    public function setPurchaseItems($value) 
    {
        $this->_fields['PurchaseItems']['FieldValue'] = $value;
        return;
    }
    
    public function isSetPurchaseItems()
    {
        return !is_null($this->_fields['PurchaseItems']['FieldValue']);

    }
}

class PnAmazonModelSetPurchaseItemsResponse extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelResponseMetadata'),
        );
        parent::__construct($data);
    }
    
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
    	$xpath->registerNamespace('a','http://payments.amazon.com/checkout/v2/2010-08-31/'); 
        $response = $xpath->query('//a:SetPurchaseItemsResponse');
        if ($response->length == 1) {
            return new PnAmazonModelSetPurchaseItemsResponse(($response->item(0))); 
        } else {
            throw new Exception ("Unable to construct CheckoutByAmazon_Service_Model_SetPurchaseItemsResponse from provided XML. 
                                  Make sure that SetPurchaseItemsResponse is a root element");
        }
    }
    
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }
    
    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }
    
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);
    }
    
    public function toXML() 
    {
        $xml = "";
        $xml .= "<SetPurchaseItemsResponse xmlns=\"http://payments.amazon.com/checkout/v2/2010-08-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</SetPurchaseItemsResponse>";
        return $xml;
    }
}

class PnAmazonModelCompletePurchaseContractRequest extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'PurchaseContractId' => array('FieldValue' => null, 'FieldType' => 'string'),
        'IntegratorId' => array('FieldValue' => null, 'FieldType' => 'string'),
        'IntegratorName' => array('FieldValue' => null, 'FieldType' => 'string'),
        'InstantOrderProcessingNotificationURLs' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelInstantOrderProcessingNotificationURLs'),
        );
        parent::__construct($data);
    }
    
    public function getPurchaseContractId() 
    {
        return $this->_fields['PurchaseContractId']['FieldValue'];
    }
    
    public function setPurchaseContractId($value) 
    {
        $this->_fields['PurchaseContractId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetPurchaseContractId()
    {
        return !is_null($this->_fields['PurchaseContractId']['FieldValue']);
    }
    
    public function getIntegratorId() 
    {
        return $this->_fields['IntegratorId']['FieldValue'];
    }
    
    public function setIntegratorId($value) 
    {
        $this->_fields['IntegratorId']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetIntegratorId()
    {
        return !is_null($this->_fields['IntegratorId']['FieldValue']);
    }
    
    public function getIntegratorName() 
    {
        return $this->_fields['IntegratorName']['FieldValue'];
    }
    
    public function setIntegratorName($value) 
    {
        $this->_fields['IntegratorName']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetIntegratorName()
    {
        return !is_null($this->_fields['IntegratorName']['FieldValue']);
    }
    
    public function getInstantOrderProcessingNotificationURLs() 
    {
        return $this->_fields['InstantOrderProcessingNotificationURLs']['FieldValue'];
    }
    
    public function setInstantOrderProcessingNotificationURLs($value) 
    {
        $this->_fields['InstantOrderProcessingNotificationURLs']['FieldValue'] = $value;
        return;
    }
    
    public function isSetInstantOrderProcessingNotificationURLs()
    {
        return !is_null($this->_fields['InstantOrderProcessingNotificationURLs']['FieldValue']);
    }
}

class PnAmazonModelCompletePurchaseContractResponse extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'CompletePurchaseContractResult' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelCompletePurchaseContractResult'),
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelResponseMetadata'),
        );
        parent::__construct($data);
    }
    
    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
    	$xpath->registerNamespace('a','http://payments.amazon.com/checkout/v2/2010-08-31/'); 
        $response = $xpath->query('//a:CompletePurchaseContractResponse');
        if ($response->length == 1) {
            return new PnAmazonModelCompletePurchaseContractResponse(($response->item(0))); 
        } else {
            throw new Exception ("Unable to construct CheckoutByAmazon_Service_Model_CompletePurchaseContractResponse from provided XML. 
                                  Make sure that CompletePurchaseContractResponse is a root element");
        }
    }
    
    public function getCompletePurchaseContractResult() 
    {
        return $this->_fields['CompletePurchaseContractResult']['FieldValue'];
    }
    
    public function setCompletePurchaseContractResult($value) 
    {
        $this->_fields['CompletePurchaseContractResult']['FieldValue'] = $value;
        return;
    }
    
    public function isSetCompletePurchaseContractResult()
    {
        return !is_null($this->_fields['CompletePurchaseContractResult']['FieldValue']);
    }
    
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }
    
    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }
    
    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);
    }
    
    public function toXML() 
    {
        $xml = "";
        $xml .= "<CompletePurchaseContractResponse xmlns=\"http://payments.amazon.com/checkout/v2/2010-08-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</CompletePurchaseContractResponse>";
        return $xml;
    }
}

class PnAmazonModelCompletePurchaseContractResult extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'OrderIds' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelOrderIdList'),
        );
        parent::__construct($data);
    }
    
    public function getOrderIds() 
    {
        return $this->_fields['OrderIds']['FieldValue'];
    }
    
    public function setOrderIds($value) 
    {
        $this->_fields['OrderIds']['FieldValue'] = $value;
        return;
    }
    
    public function isSetOrderIds()
    {
        return !is_null($this->_fields['OrderIds']['FieldValue']);
    }
}

class PnAmazonModelInstantOrderProcessingNotificationURLs extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'IntegratorURL' => array('FieldValue' => null, 'FieldType' => 'string'),
        'MerchantURL' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }
    
    public function getIntegratorURL() 
    {
        return $this->_fields['IntegratorURL']['FieldValue'];
    }
    
    public function setIntegratorURL($value) 
    {
        $this->_fields['IntegratorURL']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetIntegratorURL()
    {
        return !is_null($this->_fields['IntegratorURL']['FieldValue']);
    }
    
    public function getMerchantURL() 
    {
        return $this->_fields['MerchantURL']['FieldValue'];
    }
    
    public function setMerchantURL($value) 
    {
        $this->_fields['MerchantURL']['FieldValue'] = $value;
        return $this;
    }
    
    public function isSetMerchantURL()
    {
        return !is_null($this->_fields['MerchantURL']['FieldValue']);
    }
}

class PnAmazonModelOrderIdList extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'OrderId' => array('FieldValue' => array(), 'FieldType' => array('string')),
        );
        parent::__construct($data);
    }
    
    public function getOrderId() 
    {
        return $this->_fields['OrderId']['FieldValue'];
    }
    
    public function setOrderId($orderId) 
    {
        if (!$this->_isNumericArray($orderId)) {
            $orderId =  array ($orderId);    
        }
        $this->_fields['OrderId']['FieldValue'] = $orderId;
        return $this;
    }
    
    public function isSetOrderId()
    {
        return count ($this->_fields['OrderId']['FieldValue']) > 0;
    }
}

class PnAmazonModelDeliveryMethod extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'ServiceLevel' => array('FieldValue' => null, 'FieldType' => 'ShippingServiceLevel'),
        'DisplayableShippingLabel' => array('FieldValue' => null, 'FieldType' => 'string'),
        'DestinationName' => array('FieldValue' => null, 'FieldType' => 'string'),
        'ShippingCustomData' => array('FieldValue' => null, 'FieldType' => 'string'),
        );
        parent::__construct($data);
    }

    public function getServiceLevel() 
    {
        return $this->_fields['ServiceLevel']['FieldValue'];
    }

    public function setServiceLevel($value) 
    {
        $this->_fields['ServiceLevel']['FieldValue'] = $value;
        return $this;
    }

    public function isSetServiceLevel()
    {
        return !is_null($this->_fields['ServiceLevel']['FieldValue']);
    }

    public function getDisplayableShippingLabel() 
    {
        return $this->_fields['DisplayableShippingLabel']['FieldValue'];
    }
    
    public function setDisplayableShippingLabel($value) 
    {
        $this->_fields['DisplayableShippingLabel']['FieldValue'] = $value;
        return $this;
    }

    public function isSetDisplayableShippingLabel()
    {
        return !is_null($this->_fields['DisplayableShippingLabel']['FieldValue']);
    }

    public function getDestinationName() 
    {
        return $this->_fields['DestinationName']['FieldValue'];
    }

    public function setDestinationName($value) 
    {
        $this->_fields['DestinationName']['FieldValue'] = $value;
        return $this;
    }

    public function isSetDestinationName()
    {
        return !is_null($this->_fields['DestinationName']['FieldValue']);
    }

    public function getShippingCustomData() 
    {
        return $this->_fields['ShippingCustomData']['FieldValue'];
    }

    public function setShippingCustomData($value) 
    {
        $this->_fields['ShippingCustomData']['FieldValue'] = $value;
        return $this;
    }

    public function isSetShippingCustomData()
    {
        return !is_null($this->_fields['ShippingCustomData']['FieldValue']);
    }

}

class PnAmazonModelPhysicalProductAttributes extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Weight' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelWeight'),
        'Condition' => array('FieldValue' => null, 'FieldType' => 'string'),
        'GiftOption' => array('FieldValue' => null, 'FieldType' => 'string'),
        'GiftMessage' => array('FieldValue' => null, 'FieldType' => 'string'),
        'DeliveryMethod' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelDeliveryMethod'),
        'ItemCharges' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelCharges'),
        );
        parent::__construct($data);
    }

    public function getWeight() 
    {
        return $this->_fields['Weight']['FieldValue'];
    }

    public function setWeight($value) 
    {
        $this->_fields['Weight']['FieldValue'] = $value;
        return;
    }


    public function isSetWeight()
    {
        return !is_null($this->_fields['Weight']['FieldValue']);
    }

    public function getCondition() 
    {
        return $this->_fields['Condition']['FieldValue'];
    }

    public function setCondition($value) 
    {
        $this->_fields['Condition']['FieldValue'] = $value;
        return $this;
    }

    public function isSetCondition()
    {
        return !is_null($this->_fields['Condition']['FieldValue']);
    }

    
    public function getGiftOption() 
    {
        return $this->_fields['GiftOption']['FieldValue'];
    }

    
    public function setGiftOption($value) 
    {
        $this->_fields['GiftOption']['FieldValue'] = $value;
        return $this;
    }

    public function isSetGiftOption()
    {
        return !is_null($this->_fields['GiftOption']['FieldValue']);
    }

    public function getGiftMessage() 
    {
        return $this->_fields['GiftMessage']['FieldValue'];
    }

    public function setGiftMessage($value) 
    {
        $this->_fields['GiftMessage']['FieldValue'] = $value;
        return $this;
    }

    
    public function isSetGiftMessage()
    {
        return !is_null($this->_fields['GiftMessage']['FieldValue']);
    }

    
    public function getDeliveryMethod() 
    {
        return $this->_fields['DeliveryMethod']['FieldValue'];
    }

    public function setDeliveryMethod($value) 
    {
        $this->_fields['DeliveryMethod']['FieldValue'] = $value;
        return;
    }

    public function isSetDeliveryMethod()
    {
        return !is_null($this->_fields['DeliveryMethod']['FieldValue']);
    }

    
    public function getItemCharges() 
    {
        return $this->_fields['ItemCharges']['FieldValue'];
    }

    public function setItemCharges($value) 
    {
        $this->_fields['ItemCharges']['FieldValue'] = $value;
        return;
    }

    public function isSetItemCharges()
    {
        return !is_null($this->_fields['ItemCharges']['FieldValue']);
    }
}

class PnAmazonModelCharges extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Tax' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPrice'),
        'Shipping' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPrice'),
        'GiftWrap' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPrice'),
        'Promotions' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPromotionList'),
        );
        parent::__construct($data);
    }

    public function getTax() 
    {
        return $this->_fields['Tax']['FieldValue'];
    }

    public function setTax($value) 
    {
        $this->_fields['Tax']['FieldValue'] = $value;
        return;
    }

    public function isSetTax()
    {
        return !is_null($this->_fields['Tax']['FieldValue']);
    }

    public function getShipping() 
    {
        return $this->_fields['Shipping']['FieldValue'];
    }

    public function setShipping($value) 
    {
        $this->_fields['Shipping']['FieldValue'] = $value;
        return;
    }

    public function isSetShipping()
    {
        return !is_null($this->_fields['Shipping']['FieldValue']);
    }

    public function getGiftWrap() 
    {
        return $this->_fields['GiftWrap']['FieldValue'];
    }

    public function setGiftWrap($value) 
    {
        $this->_fields['GiftWrap']['FieldValue'] = $value;
        return;
    }

    public function isSetGiftWrap()
    {
        return !is_null($this->_fields['GiftWrap']['FieldValue']);
    }

    public function getPromotions() 
    {
        return $this->_fields['Promotions']['FieldValue'];
    }

    public function setPromotions($value) 
    {
        $this->_fields['Promotions']['FieldValue'] = $value;
        return;
    }

    public function isSetPromotions()
    {
        return !is_null($this->_fields['Promotions']['FieldValue']);
    }

}

class PnAmazonModelContractCharges extends PnAmazonModelCharges
{
    public function __construct($data = null)
    {
       parent::__construct($data);
    }
    
    public function setContractTax($amount)
    {
        parent::setTax( new PnAmazonModelPrice(array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(),  'Amount' => $amount)));
    }
    
    public function setContractShippingCharges($amount)
    {
        parent::setShipping( new PnAmazonModelPrice(array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(),'Amount' => $amount)));
    }
    
    public function setContractPromotions($value)
    {
        parent::setPromotions($value);
    }
}

class PnAmazonModelPromotion extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'PromotionId' => array('FieldValue' => null, 'FieldType' => 'IdType'),
        'Description' => array('FieldValue' => null, 'FieldType' => 'string'),
        'Discount' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelPrice'),
        );
        parent::__construct($data);
    }

    public function getPromotionId() 
    {
        return $this->_fields['PromotionId']['FieldValue'];
    }

    public function setPromotionId($value) 
    {
        $this->_fields['PromotionId']['FieldValue'] = $value;
        return $this;
    }

    public function isSetPromotionId()
    {
        return !is_null($this->_fields['PromotionId']['FieldValue']);
    }

    public function getDescription() 
    {
        return $this->_fields['Description']['FieldValue'];
    }

    public function setDescription($value) 
    {
        $this->_fields['Description']['FieldValue'] = $value;
        return $this;
    }

    public function isSetDescription()
    {
        return !is_null($this->_fields['Description']['FieldValue']);
    }

    public function getDiscount() 
    {
        return $this->_fields['Discount']['FieldValue'];
    }

    public function setDiscount($value) 
    {
        $this->_fields['Discount']['FieldValue'] = $value;
        return;
    }
    
    public function isSetDiscount()
    {
        return !is_null($this->_fields['Discount']['FieldValue']);

    }

    public function createPromotion($promotionId,$description,$amount)
    {
        $discountObject = new PnAmazonModelPrice(array('CurrencyCode' => PnAmazonConfig::getCurrencyCode(), 'Amount' => $amount));
        $this->setPromotionId($promotionId);
        $this->setDescription($description);
        $this->setDiscount($discountObject);
        return $this;
    }
}

class PnAmazonModelPromotionList extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'Promotion' => array('FieldValue' => array(), 'FieldType' => array('PnAmazonModelPromotion')),
        );
        parent::__construct($data);
    }

    public function getPromotion() 
    {
        return $this->_fields['Promotion']['FieldValue'];
    }

    public function setPromotion($promotion) 
    {
        if (!$this->_isNumericArray($promotion)) {
            $promotion =  array ($promotion);    
        }
        $this->_fields['Promotion']['FieldValue'] = $promotion;
        return $this;
    }

    public function isSetPromotion()
    {
        return count ($this->_fields['Promotion']['FieldValue']) > 0;
    }

    public function addPromotion($promotion)
    {
       $this->setPromotion($promotion);
       return $this;
    }
}

class PnAmazonModelSetContractChargesRequest extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'PurchaseContractId' => array('FieldValue' => null, 'FieldType' => 'string'),
        'Charges' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelCharges'),
        );
        parent::__construct($data);
    }

    public function getPurchaseContractId() 
    {
        return $this->_fields['PurchaseContractId']['FieldValue'];
    }

    public function setPurchaseContractId($value) 
    {
        $this->_fields['PurchaseContractId']['FieldValue'] = $value;
        return $this;
    }

    public function isSetPurchaseContractId()
    {
        return !is_null($this->_fields['PurchaseContractId']['FieldValue']);
    }

    public function getCharges() 
    {
        return $this->_fields['Charges']['FieldValue'];
    }

    public function setCharges($value) 
    {
        $this->_fields['Charges']['FieldValue'] = $value;
        return;
    }

    public function isSetCharges()
    {
        return !is_null($this->_fields['Charges']['FieldValue']);
    }
}

class PnAmazonModelSetContractChargesResponse extends PnAmazonModel
{
    public function __construct($data = null)
    {
        $this->_fields = array (
        'ResponseMetadata' => array('FieldValue' => null, 'FieldType' => 'PnAmazonModelResponseMetadata'),
        );
        parent::__construct($data);
    }

    public static function fromXML($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $xpath = new DOMXPath($dom);
    	$xpath->registerNamespace('a','http://payments.amazon.com/checkout/v2/2010-08-31/'); 
        $response = $xpath->query('//a:SetContractChargesResponse');
        if ($response->length == 1) {
            return new PnAmazonModelSetContractChargesResponse(($response->item(0))); 
        } else {
            throw new Exception ("Unable to construct CheckoutByAmazon_Service_Model_SetContractChargesResponse from provided XML. 
                                  Make sure that SetContractChargesResponse is a root element");
        }
          
    }
    
    public function getResponseMetadata() 
    {
        return $this->_fields['ResponseMetadata']['FieldValue'];
    }

    public function setResponseMetadata($value) 
    {
        $this->_fields['ResponseMetadata']['FieldValue'] = $value;
        return;
    }

    public function isSetResponseMetadata()
    {
        return !is_null($this->_fields['ResponseMetadata']['FieldValue']);
    }

    public function toXML() 
    {
        $xml = "";
        $xml .= "<SetContractChargesResponse xmlns=\"http://payments.amazon.com/checkout/v2/2010-08-31/\">";
        $xml .= $this->_toXMLFragment();
        $xml .= "</SetContractChargesResponse>";
        return $xml;
    }
}
