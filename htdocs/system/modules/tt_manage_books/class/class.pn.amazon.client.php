<?php

class PnAmazonClient
{
    private  $_aws_access_key_id = null;
    private  $_aws_secret_access_key = null;
    private  $_config = array ('ServiceURL' => null,
                               'UserAgent' => 'CBA_CheckoutAPI_Inline/1.1 (Language=PHP; ReleaseDate=10_2012)',
                               'SignatureVersion' => 2,
                               'SignatureMethod' => 'HmacSHA256',
                               'ProxyHost' => null,
                               'ProxyPort' => -1,
                               'MaxErrorRetry' => 3
                               );
    
    public function __construct($aws_access_key_id, $aws_secret_access_key)
    {
        iconv_set_encoding('output_encoding', 'UTF-8');
        iconv_set_encoding('input_encoding', 'UTF-8');
        iconv_set_encoding('internal_encoding', 'UTF-8');

        $this->_aws_access_key_id = $aws_access_key_id;
        $this->_aws_secret_access_key = $aws_secret_access_key;
        $config = array ('ServiceURL' =>PnAmazonConfig::getCbaServiceURL());
        $this->_config = array_merge($this->_config, $config);
    }
    
    public function createPurchaseContract($request)
    {
        if (!$request instanceof PnAmazonModelCreatePurchaseContractRequest) {
            $request = new PnAmazonModelCreatePurchaseContractRequest($request);
        }
        return PnAmazonModelCreatePurchaseContractResponse::fromXML($this->invoke($this->convertCreatePurchaseContract($request)));
    }
    
    public function getPurchaseContract($request)
    {
        if (!$request instanceof PnAmazonModelGetPurchaseContractRequest) 
        {
            $request = new PnAmazonModelGetPurchaseContractRequest($request);
        }
        return PnAmazonModelGetPurchaseContractResponse::fromXML($this->invoke($this->convertGetPurchaseContract($request)));
    }
    
    public function setPurchaseItems($request)
    {
        if (!$request instanceof PnAmazonModelSetPurchaseItemsRequest) {
            $request = new PnAmazonModelSetPurchaseItemsRequest($request);
        }
        return PnAmazonModelSetPurchaseItemsResponse::fromXML($this->invoke($this->convertSetPurchaseItems($request)));
    }
    
    public function completePurchaseContract($request)
    {
        if (!$request instanceof PnAmazonModelCompletePurchaseContractRequest) {
            $request = new PnAmazonModelCompletePurchaseContractRequest($request);
        }
        return PnAmazonModelCompletePurchaseContractResponse::fromXML($this->invoke($this->convertCompletePurchaseContract($request)));
    }
    
    public function setContractCharges($request)
    {
        if (!$request instanceof PnAmazonModelSetContractChargesRequest) {
            $request = new PnAmazonModelSetContractChargesRequest($request);
        }
        return PnAmazonModelSetContractChargesResponse::fromXML($this->invoke($this->convertSetContractCharges($request)));
    }
    
    protected function invoke(array $parameters)
    {
        $actionName = $parameters["Action"];
        $response = array();
        $responseBody = null;
        $statusCode = 200;

        try 
        {
            $parameters = $this->addRequiredParameters($parameters);
            $shouldRetry = true;
            $retries = 0;
            do 
            {
                try 
                {
                    $response = $this->httpPost($parameters);
                    if ($response['Status'] === 200) {
                        $shouldRetry = false;
                    } else {
                        if ($response['Status'] === 500 || $response['Status'] === 503) {
                            $shouldRetry = true;
                            $this->pauseOnRetry(++$retries, $response['Status']);
                        } else {
                            throw $this->reportAnyErrors($response['ResponseBody'], $response['Status']);
                        }
                    }
                } 
                catch (Exception $e) 
                {
                    throw $e;
                }
            } while ($shouldRetry);

        }
        catch (Exception $t) 
        {
            throw $t;
        }
        return $response['ResponseBody'];
    }
    
    protected function reportAnyErrors($responseBody, $status, Exception $e =  null)
    {
        print_r($responseBody);
        $ex = null;
        $xml = new SimpleXMLElement($responseBody);
        $doc = simplexml_load_string($responseBody);
        $message = $doc->Error->Message;
        $requestId = $doc->RequestId;
        $code = $doc->Error->Code;
        $type = $doc->Error->Type;
        if(is_null($type))
        {
            $type = 'Unknown';
        }
        
        if(!is_null($message))
        {
            //todo
        }
        else
        {
            //todo
        }
        
        return $ex;
    }
    
    protected function httpPost(array $parameters)
    {
        $CBAServiceEndpoint = $this->_config['ServiceURL'];
        $query = http_build_query($parameters, '', '&');
        $curlHandle = curl_init();

        curl_setopt($curlHandle, CURLOPT_URL, $CBAServiceEndpoint);
        curl_setopt($curlHandle, CURLOPT_USERAGENT, $this->_config['UserAgent']);
        curl_setopt($curlHandle, CURLOPT_POST, true);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $query);
        curl_setopt($curlHandle, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curlHandle, CURLOPT_MAXREDIRS, 0);
        curl_setopt($curlHandle, CURLOPT_HEADER, true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_NOSIGNAL, true);
        curl_setopt($curlHandle, CURLOPT_HEADER, false);
        
        $responseBody = curl_exec($curlHandle);
        $info = curl_getinfo($curlHandle);
        
        $code = $info["http_code"];

        curl_close($curlHandle);
        return array ('Status' => (int)$code, 'ResponseBody' => $responseBody);
    }
    
    protected function pauseOnRetry($retries, $status)
    {
        if ($retries <= $this->_config['MaxErrorRetry']) 
        {
            $delay = (int) (pow(4, $retries) * 100000) ;
            usleep($delay);
        }
        else
        {
        }
    }
    
    protected function addRequiredParameters(array $parameters)
    {
        $parameters['AWSAccessKeyId'] = $this->_aws_access_key_id;
        $parameters['Timestamp'] = $this->getFormattedTimestamp();
        $parameters['Version'] = PnAmazonConfig::getVersion();
        $parameters['SignatureVersion'] = $this->_config['SignatureVersion'];
        if ($parameters['SignatureVersion'] > 1) {
            $parameters['SignatureMethod'] = $this->_config['SignatureMethod'];
        }
        $parameters['Signature'] = $this->signParameters($parameters, $this->_aws_secret_access_key);

        return $parameters;
    }
    
    protected function getParametersAsString(array $parameters)
    {
        $queryParameters = array();
        foreach ($parameters as $key => $value) {
            $queryParameters[] = $key . '=' . $this->_urlencode($value);
        }
        return implode('&', $queryParameters);
    }
    
    protected function signParameters(array $parameters, $key) 
    {
        $signatureVersion = $parameters['SignatureVersion'];
        $algorithm = "HmacSHA1";
        $stringToSign = null;
        $algorithm = $this->_config['SignatureMethod'];
        $parameters['SignatureMethod'] = $algorithm;
        $stringToSign = $this->calculateStringToSignV2($parameters);

        return $this->sign($stringToSign, $key, $algorithm);
    }
    
    protected function calculateStringToSignV2(array $parameters) 
    {
        $data = 'POST';
        $data .= "\n";
        $endpoint = parse_url ($this->_config['ServiceURL']);
        $data .= $endpoint['host'];
        $data .= "\n";
        $uri = array_key_exists('path', $endpoint) ? $endpoint['path'] : null;
        if (!isset ($uri)) {
        	$uri = "/";
        }
		$uriencoded = implode("/", array_map(array($this, "_urlencode"), explode("/", $uri)));
        $data .= $uriencoded;
        $data .= "\n";
        uksort($parameters, 'strcmp');
        $data .= $this->getParametersAsString($parameters);
        return $data;
    }
    
    protected function _urlencode($value) 
    {
        return str_replace('%7E', '~', rawurlencode($value));
    }
    
    protected function sign($data, $key, $algorithm)
    {
        switch($algorithm)
        {
            case 'HmacSHA1' :
                $hash = 'sha1';
                break;
            case 'HmacSHA256' :
                 $hash = 'sha256';
                 break;
            default :
                throw new Exception ("Non-supported signing method specified");
        }
        
        return base64_encode(hash_hmac($hash, $data, $key, true));
    }
    
    protected function getFormattedTimestamp()
    {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    }
    
    protected function convertCreatePurchaseContract($request) 
    {
        $parameters = array();
        $parameters['Action'] = 'CreatePurchaseContract';
        if ($request->isSetPurchaseContractMetadata()) {
            $parameters['PurchaseContractMetadata'] =  $request->getPurchaseContractMetadata();
        }
        return $parameters;
    }
    
    protected function convertGetPurchaseContract($request) 
    {
        $parameters = array();
        $parameters['Action'] = 'GetPurchaseContract';
        if ($request->isSetPurchaseContractId()) {
            $parameters['PurchaseContractId'] =  $request->getPurchaseContractId();
        }
        return $parameters;
    }
    
    protected function convertSetPurchaseItems($request) 
    {
        $parameters = array();
        $parameters['Action'] = 'SetPurchaseItems';
        if ($request->isSetPurchaseContractId()) {
            $parameters['PurchaseContractId'] =  $request->getPurchaseContractId();
        }
        if ($request->isSetPurchaseItems()) {
            $purchaseItemssetPurchaseItemsRequest = $request->getPurchaseItems();
             $purchaseItempurchaseItemsIndex = 1;
            foreach ($purchaseItemssetPurchaseItemsRequest->getPurchaseItem() as $purchaseItempurchaseItemsIndex1 => $purchaseItempurchaseItems) {
                if ($purchaseItempurchaseItems->isSetMerchantItemId()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.MerchantItemId'] =  $purchaseItempurchaseItems->getMerchantItemId();
                }
                if ($purchaseItempurchaseItems->isSetSKU()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.SKU'] =  $purchaseItempurchaseItems->getSKU();
                }
                if ($purchaseItempurchaseItems->isSetMerchantId()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.MerchantId'] =  $purchaseItempurchaseItems->getMerchantId();
                }
                if ($purchaseItempurchaseItems->isSetTitle()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.Title'] =  $purchaseItempurchaseItems->getTitle();
                }
                if ($purchaseItempurchaseItems->isSetDescription()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.Description'] =  $purchaseItempurchaseItems->getDescription();
                }
                if ($purchaseItempurchaseItems->isSetUnitPrice()) {
                    $UnitPricepurchaseItem = $purchaseItempurchaseItems->getUnitPrice();
                    if ($UnitPricepurchaseItem->isSetAmount()) {
                        $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.UnitPrice.Amount'] =  $UnitPricepurchaseItem->getAmount();
                    }
                    if ($UnitPricepurchaseItem->isSetCurrencyCode()) {
                        $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.UnitPrice.CurrencyCode'] =  $UnitPricepurchaseItem->getCurrencyCode();
                    }
                }
                if ($purchaseItempurchaseItems->isSetQuantity()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.Quantity'] =  $purchaseItempurchaseItems->getQuantity();
                }
                if ($purchaseItempurchaseItems->isSetURL()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.URL'] =  $purchaseItempurchaseItems->getURL();
                }
                if ($purchaseItempurchaseItems->isSetCategory()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.Category'] =  $purchaseItempurchaseItems->getCategory();
                }
                if ($purchaseItempurchaseItems->isSetFulfillmentNetwork()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.FulfillmentNetwork'] =  $purchaseItempurchaseItems->getFulfillmentNetwork();
                }
                if ($purchaseItempurchaseItems->isSetItemCustomData()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.ItemCustomData'] =  $purchaseItempurchaseItems->getItemCustomData();
                }
                if ($purchaseItempurchaseItems->isSetProductType()) {
                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.ProductType'] =  $purchaseItempurchaseItems->getProductType();
                }
                if ($purchaseItempurchaseItems->isSetPhysicalProductAttributes()) {
                    $physicalProductAttributespurchaseItem = $purchaseItempurchaseItems->getPhysicalProductAttributes();
                    if ($physicalProductAttributespurchaseItem->isSetWeight()) {
                        $weightphysicalProductAttributes = $physicalProductAttributespurchaseItem->getWeight();
                        if ($weightphysicalProductAttributes->isSetValue()) {
                            $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.Weight.Value'] =  $weightphysicalProductAttributes->getValue();
                        }
                        if ($weightphysicalProductAttributes->isSetUnit()) {
                            $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.Weight.Unit'] =  $weightphysicalProductAttributes->getUnit();
                        }
                    }
                    if ($physicalProductAttributespurchaseItem->isSetCondition()) {
                        $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.Condition'] =  $physicalProductAttributespurchaseItem->getCondition();
                    }
                    if ($physicalProductAttributespurchaseItem->isSetDeliveryMethod()) {
                        $deliveryMethodphysicalProductAttributes = $physicalProductAttributespurchaseItem->getDeliveryMethod();
                        if ($deliveryMethodphysicalProductAttributes->isSetServiceLevel()) {
                            $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.DeliveryMethod.ServiceLevel'] =  $deliveryMethodphysicalProductAttributes->getServiceLevel();
                        }
                        if ($deliveryMethodphysicalProductAttributes->isSetDisplayableShippingLabel()) {
                            $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.DeliveryMethod.DisplayableShippingLabel'] =  $deliveryMethodphysicalProductAttributes->getDisplayableShippingLabel();
                        }
                        if ($deliveryMethodphysicalProductAttributes->isSetDestinationName()) {
                            $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.DeliveryMethod.DestinationName'] =  $deliveryMethodphysicalProductAttributes->getDestinationName();
                        }
                        if ($deliveryMethodphysicalProductAttributes->isSetShippingCustomData()) {
                            $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.DeliveryMethod.ShippingCustomData'] =  $deliveryMethodphysicalProductAttributes->getShippingCustomData();
                        }
                    }
                    if ($physicalProductAttributespurchaseItem->isSetItemCharges()) {
                        $itemChargesPhysicalProductAttributes = $physicalProductAttributespurchaseItem->getItemCharges();
                        if ($itemChargesPhysicalProductAttributes->isSetTax()) {
                            $taxItemCharges = $itemChargesPhysicalProductAttributes->getTax();
                            if ($taxItemCharges->isSetAmount()) {
                                $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Tax.Amount'] =  $taxItemCharges->getAmount();
                            }
                            if ($taxItemCharges->isSetCurrencyCode()) {
                                $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Tax.CurrencyCode'] =  $taxItemCharges->getCurrencyCode();
                            }
                        }
                        if ($itemChargesPhysicalProductAttributes->isSetShipping()) {
                            $shippingItemCharges = $itemChargesPhysicalProductAttributes->getShipping();
                            if ($shippingItemCharges->isSetAmount()) {
                                $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Shipping.Amount'] =  $shippingItemCharges->getAmount();
                            }
                            if ($shippingItemCharges->isSetCurrencyCode()) {
                                $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Shipping.CurrencyCode'] =  $shippingItemCharges->getCurrencyCode();
                            }
                        }
                        if ($itemChargesPhysicalProductAttributes->isSetPromotions()) {
                            $promotionsItemCharges = $itemChargesPhysicalProductAttributes->getPromotions();
                            foreach ($promotionsItemCharges->getPromotion() as $promotionPromotionsIndex1 => $promotionPromotions ) {
                                $promotionPromotionsIndex = $promotionPromotionsIndex1 + 1;
                                if ($promotionPromotions->isSetPromotionId()) {
                                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.'  . ($promotionPromotionsIndex) . '.PromotionId'] =  $promotionPromotions->getPromotionId();
                                }
                                if ($promotionPromotions->isSetDescription()) {
                                    $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.'  . ($promotionPromotionsIndex) . '.Description'] =  $promotionPromotions->getDescription();
                                }
                                if ($promotionPromotions->isSetDiscount()) {
                                    $discountPromotion = $promotionPromotions->getDiscount();
                                    if ($discountPromotion->isSetAmount()) {
                                        $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.'. ($promotionPromotionsIndex) . '.Discount.Amount'] =  $discountPromotion->getAmount();
                                    }
                                    if ($discountPromotion->isSetCurrencyCode()) {
                                        $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.PhysicalProductAttributes.ItemCharges.Promotions.Promotion.'  . ($promotionPromotionsIndex) . '.Discount.CurrencyCode'] =  $discountPromotion->getCurrencyCode();
                                    }
                                }

                            }
                        }
                    }
                }
                if ($purchaseItempurchaseItems->isSetDigitalProductAttributes()) {
                    $digitalProductAttributespurchaseItem = $purchaseItempurchaseItems->getDigitalProductAttributes();
                    if ($digitalProductAttributespurchaseItem->isSetdummyDigitalProperty()) {
                        $parameters['PurchaseItems.PurchaseItem.'  . ($purchaseItempurchaseItemsIndex) . '.DigitalProductAttributes.dummyDigitalProperty'] =  $digitalProductAttributespurchaseItem->getdummyDigitalProperty();
                    }
                }
                $purchaseItempurchaseItemsIndex++;
            }
        }

        return $parameters;
    }
    
    protected function convertCompletePurchaseContract($request) 
    {
        $parameters = array();
        $parameters['Action'] = 'CompletePurchaseContract';
        if ($request->isSetPurchaseContractId()) 
        {
            $parameters['PurchaseContractId'] =  $request->getPurchaseContractId();
        }
        if ($request->isSetIntegratorId()) 
        {
            $parameters['IntegratorId'] =  $request->getIntegratorId();
        }
        if ($request->isSetIntegratorName()) 
        {
            $parameters['IntegratorName'] =  $request->getIntegratorName();
        }
        if ($request->isSetInstantOrderProcessingNotificationURLs()) 
        {
            $instantOrderProcessingNotificationURLscompletePurchaseContractRequest = $request->getInstantOrderProcessingNotificationURLs();
            if ($instantOrderProcessingNotificationURLscompletePurchaseContractRequest->isSetIntegratorURL()) {
                $parameters['InstantOrderProcessingNotificationURLs.IntegratorURL'] =  $instantOrderProcessingNotificationURLscompletePurchaseContractRequest->getIntegratorURL();
            }
            if ($instantOrderProcessingNotificationURLscompletePurchaseContractRequest->isSetMerchantURL()) {
                $parameters['InstantOrderProcessingNotificationURLs.MerchantURL'] =  $instantOrderProcessingNotificationURLscompletePurchaseContractRequest->getMerchantURL();
            }
        }
        return $parameters;
    }
    
    protected function convertSetContractCharges($request) 
    {
        $parameters = array();
        $parameters['Action'] = 'SetContractCharges';
        if ($request->isSetPurchaseContractId()) {
            $parameters['PurchaseContractId'] =  $request->getPurchaseContractId();
        }
        if ($request->isSetCharges()) {
            $chargesSetContractChargesRequest = $request->getCharges();
            if ($chargesSetContractChargesRequest->isSetTax()) {
                $taxCharges = $chargesSetContractChargesRequest->getTax();
                if ($taxCharges->isSetAmount()) {
                    $parameters['Charges.Tax.Amount'] =  $taxCharges->getAmount();
                }
                if ($taxCharges->isSetCurrencyCode()) {
                    $parameters['Charges.Tax.CurrencyCode'] =  $taxCharges->getCurrencyCode();
                }
            }
            if ($chargesSetContractChargesRequest->isSetShipping()) {
                $shippingCharges = $chargesSetContractChargesRequest->getShipping();
                if ($shippingCharges->isSetAmount()) {
                    $parameters['Charges.Shipping.Amount'] =  $shippingCharges->getAmount();
                }
                if ($shippingCharges->isSetCurrencyCode()) {
                    $parameters['Charges.Shipping.CurrencyCode'] =  $shippingCharges->getCurrencyCode();
                }
            }
            if ($chargesSetContractChargesRequest->isSetPromotions()) {
                $promotionsCharges = $chargesSetContractChargesRequest->getPromotions();
                $promotionPromotionsIndex = 1;
                foreach ($promotionsCharges->getPromotion() as $promotionPromotionsIndex1 => $promotionPromotions) {
                    if ($promotionPromotions->isSetPromotionId()) {
                        
                        $parameters['Charges.Promotions.Promotion.'  . ($promotionPromotionsIndex) . '.PromotionId'] =  $promotionPromotions->getPromotionId();
                    }
                    if ($promotionPromotions->isSetDescription()) {
                        $parameters['Charges.Promotions.Promotion.' . ($promotionPromotionsIndex) . '.Description'] =  $promotionPromotions->getDescription();
                    }
                    if ($promotionPromotions->isSetDiscount()) {
                        $discountPromotion = $promotionPromotions->getDiscount();
                        if ($discountPromotion->isSetAmount()) {
                            $parameters['Charges.Promotions.Promotion.'  . ($promotionPromotionsIndex) . '.Discount.Amount'] =  $discountPromotion->getAmount();
                        }
                        if ($discountPromotion->isSetCurrencyCode()) {
                            $parameters['Charges.Promotions.Promotion.'  . ($promotionPromotionsIndex) . '.Discount.CurrencyCode'] =  $discountPromotion->getCurrencyCode();
                        }
                    }
                    $promotionPromotionsIndex++;
                }
            }
        }

        return $parameters;
    }
}
