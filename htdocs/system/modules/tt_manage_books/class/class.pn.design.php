<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnDesign extends Controller
{
	public $id;
	public $name;
	public $title;
	public $title_en;
	public $description;
	public $image;
	public $price;
	public $artikel_id;
	public $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWidthId($id)
	{
		$design = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".designs d
				LEFT JOIN " . $this->config->database . ".finanz_artikel fa
				ON d.design_id = fa.design_id
				WHERE d.design_id = ?
			")->execute($id);
			
		while($design->next())
		{
			$this->id = $design->design_id;
			$this->name = $design->design_name;
			$this->title = $design->design_title;
			$this->title_en = $design->design_title_en;
			$this->article_id = $design->artikel_id;
			$this->artikel_id = $design->artikel_id;
		}
	}
	
	function hasFeature($key, $value)
	{
		if($this->artikel_id != "")
		{
			$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".features_artikel fe
					LEFT JOIN " . $this->config->database . ".features f
					ON fe.`feature_id` = f.`feature_id`
					WHERE fe.`artikel_id` = ?
					AND f.`feature_key` = ?
					AND f.`feature_value` = ?
					")->execute($this->artikel_id, $key, $value);
				
			if($object->numRows > 0) return true;
		}
	
		return false;
	}
	
	public function hasImage()
	{
		return $this->hasFeature('image', 'u1');
	}
	
	public function hasCover()
	{
		return $this->hasFeature('cover', 'existing');
	}
	
	public function hasSchrift()
	{
		return $this->hasFeature('cover', 'Schriftupgrade');
	}
	
	public function hasPraegung()
	{
		return false;
	}
	
	public function hasPhotoAndCaption()
	{
	    return ($this->hasFeature('cover', 'Schriftupgrade') && $this->hasFeature('cover', 'Bildupgrade'));
	}
}

?>