<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnWidmung extends Controller
{
	public $id;
	public $text;
	public $tag_id;
	protected $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function getWidmung($widmungtag_id)
	{
		$arrWidumg = array();
		$objects = $this->Database->prepare("
					SELECT w.widmung_id, w.widmung_text, wt.widmung_tag_id 
					FROM " . $this->config->database . ".widmung w LEFT JOIN widmung_widmung_tag wt
					ON w.widmung_id = wt.widmung_id
					WHERE wt.widmung_tag_id = ?
				")->execute($widmungtag_id);
		
		while($objects->next())
		{
			$widmung 			= new PnWidmung();
			$widmung->id 		= $objects->id;
			$widmung->text 		= $objects->widmung_text;
			$widmung->tag_id 	= $objects->widmung_tag_id;
			$arrWidmung[] 		= $widmung;
		}
		
		return $arrWidmung;
	}
	
	public function getWidmungArticle()
	{
		$articles = array();
		
		$objects = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".finanz_artikel
				WHERE class_id = 10 AND mandant_id = 1 AND product_id IS NULL
			")->execute();
		
		while($objects->next())
		{
			$article 			= new PnArticle();
			$article->id 		= $objects->artikel_id;
			$article->class_id 	= $objects->class_id;
			$article->name 		= $objects->artikel_name;
			$article->name_en 	= $objects->artikel_name_en;
			$articles[]			= $article;
		}
		
		return $articles;
	}
	
	public function getWidmungColorKalendar()
	{
		$articles = '';
		
		$objects = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".finanz_artikel
				WHERE class_id = 10 AND mandant_id = 1 AND product_id IS NOT NULL
			")->execute();
		
		while($objects->next())
		{
			$articles 		= $objects->artikel_id;
		}
		
		$articles = $articles!=''?$articles:8833;
		
		return $articles;
	}
	
	public function getWidmungFonts()
	{
		$fonts = array(
				array(
					"id" => "var_fontface_0",
					"title" => "Century Schoolbook L"
				),
				array(
					"id" => "var_fontface_1",
					"title" => "Zapfino Extra LT"
				),
				array(
					"id" => "var_fontface_2",
					"title" => "FreestyleScriptEF-Reg"
				),
				array(
					"id" => "var_fontface_3",
					"title" => "English157TT BT"
				)
			);
		
		return $fonts;
	}
	
	public function getWidmungs($locale=1, $forkid=0)
	{
	    $objDb = $this->Database->prepare("
	            SELECT wt.widmung_tag_id, w.widmung_id, w.widmung_text, wt.widmung_tag_text  
                FROM " . $this->config->database . ".widmung w
                LEFT JOIN " . $this->config->database . ".widmung_widmung_tag wwt
                ON w.widmung_id = wwt.widmung_id
                LEFT JOIN " . $this->config->database . ".widmung_tag wt
                ON wwt.widmung_tag_id = wt.widmung_tag_id
                WHERE wt.locale_id = ?
                AND wt.widmung_tag_kinderbuch = ?
	        ")->execute($locale, $forkid);
	        
	    $list_widmungs = array();
	    while($objDb->next())
	    {
	        $text = preg_replace("/\n/", "<br />" , $objDb->widmung_text);
	        $list_widmungs[$objDb->widmung_tag_id][] = array ( "tag" => $objDb->widmung_tag_text, "widmung" => $text );
	    }
	    
	    return $list_widmungs;
	}
}

?>