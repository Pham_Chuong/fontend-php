<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnDocument extends Controller
{
	public $id;
	public $title;
	public $name;
	public $readingSample;
	public $file;
	public $created;
	public $updated;
	public $type;
	public $decisions;
	public $variant_id;
	protected $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWidthId($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->databse . ".buch_document
				WHERE document_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 				= $object->document_id;
			$this->title 			= $object->document_title;
			$this->name 			= $object->document_name;
			$this->readingSample 	= $object->document_readingsample;
			$this->file 			= $object->document_file;
			$this->created 			= $object->document_created;
			$this->updated 			= $object->document_updated;
			$this->type 			= $object->document_type;
			$this->decisions 		= $object->document_decisions;
			$this->variant_id 		= $object->variant_id;
		}
	}
	
}
