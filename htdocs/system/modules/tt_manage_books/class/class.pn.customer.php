<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnCustomer extends Controller
{
	public $id;
	public $firstName;
	public $lastName;
	public $email;
	public $anrede;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".customers
				WHERE customer_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 			= $object->customer_id;
			$this->firstName 	= $object->customer_vorname;
			$this->lastName 	= $object->customer_nachname;
			$this->anrede 		= $object->customer_anrede;
			$this->email 		= $object->customer_email;
		}
	}
	
	public function existEmail($email)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".customers
				WHERE customer_email = ?
			")->execute($email);
			
		while($object->next())
		{
			$this->id 			= $object->customer_id;
			$this->firstName 	= $object->customer_vorname;
			$this->lastName 	= $object->customer_nachname;
			$this->anrede 		= $object->customer_anrede;
			$this->email 		= $object->customer_email;
		}
		
		if($this->id > 0)
			return true;
		return false;
	}
	
	public function add($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = mysql_real_escape_string($v);
		}
		
		$object = $this->Database->prepare("
					INSERT INTO " . $this->config->database . ".customers (". implode(", ", $key) .") VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $object->insertId;
	}
	
	public function edit($params = array())
	{
		$set = array();
		foreach ($params as $k => $v)
		{
			$set[] = $k . "  =  '" . mysql_real_escape_string($v) . "'";
		}
		
		$object = $this->Database->prepare("UPDATE " . $this->config->database . ".customers SET " . implode(", ", $set) . " WHERE customer_id = ?" )->execute($this->id);
		
		return $object->affectedRows;
	}
}

class PnCustomerVoice extends Controller
{
    public $id;
    public $name;
    public $email;
    public $content;
    public $date;
    public $locale_id;
    public $regards_only_books;
    public $customervoicetype_id;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".customer_voices
	            WHERE cvid = ?
	        ")->execute($id);
	        
        while($objDb->next())
        {
            $this->id                   = $objDb->cvid;
            $this->name                 = $objDb->name;
            $this->email                = $objDb->email;
            $this->content              = $objDb->content;
            $this->date                 = $objDb->date;
            $this->locale_id            = $objDb->locale_id;
            $this->regards_only_books   = $objDb->regards_only_books;
            $this->customervoicetype_id = $objDb->customervoicetype_id;
        }
	}
	
	public function randomVoice($count, $locale_id, $type_id="", $regards_only_books=true)
	{
	    $query = "
	                 SELECT * FROM " . $this->config->database . ".customer_voices cv
	                 WHERE cv.date > '2010-01-01' AND length(cv.content) < 1000
	             ";
	             
	    if($locale_id)
	        $query .= " AND cv.locale_id = " . $locale_id;
	    
	    if($type_id)
	        $query .= " AND cv.customervoicetype_id = " . $type_id;
	    
	    if($regards_only_books)
	        $query .= " AND cv.regards_only_books = 1";
	    
	    if($count)
	        $query .= " ORDER BY RAND() LIMIT " . $count;
	    
	    $objDb = $this->Database->prepare($query)->execute();
	    
	    $voices = array();
	    while($objDb->next())
	    {
	        $voice                       = new PnCustomerVoice();
	        $voice->id                   = $objDb->cvid;
            $voice->name                 = $objDb->name;
            $voice->email                = $objDb->email;
            $voice->content              = $objDb->content;
            $voice->date                 = $objDb->date;
            $voice->locale_id            = $objDb->locale_id;
            $voice->regards_only_books   = $objDb->regards_only_books;
            $voice->customervoicetype_id = $objDb->customervoicetype_id;
            
            $voices[] = $voice;
	    }
	    
	    return $voices;
	}
}

?>
