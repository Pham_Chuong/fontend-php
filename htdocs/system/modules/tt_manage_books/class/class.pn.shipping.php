<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnShipping extends Controller
{
	public $id;
	public $name;
	public $title;
	public $title_en;
	public $price;
	public $description;
	public $description_en;
	protected $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function all()
	{
		$shippings = array();
		$objects = $this->Database->prepare("
				
					SELECT * FROM " . $this->config->database . ".shipping_options
					WHERE shipping_active = 1
				
				")->execute();
				
		while($objects->next())
		{
			$shipping 					= new PnShipping();
			$shipping->id 				= $objects->shipping_id;
			$shipping->name 				= $objects->shipping_name;
			$shipping->title 			= $objects->shipping_title;
			$shipping->title_en 			= $objects->shipping_title_en;
			$shipping->description 		= $objects->description;
			$shipping->description_en 	= $objects->description_en;
			
			$shippings[] 				= $shipping;
		}
		
		return $shippings;
	}
	
	public function select($id)
	{
		$object = $this->Database->prepare("
			
					SELECT * FROM " . $this->config->database . ".shipping_options
					WHERE shipping_id = ?
				
				")->execute($id);
				
		while($object->next())
		{
			$this->id = $object->shipping_id;
			$this->name = $object->shipping_name;
			$this->title = $object->shipping_title;
			$this->title_en = $object->shipping_title_en;
			$this->description = $object->shipping_description;
			$this->description_en = $object->shipping_description_en;
		}
	}
	
	public function initWithId($id)
	{
	    $object = $this->Database->prepare("
			
					SELECT * FROM " . $this->config->database . ".shipping_options
					WHERE shipping_id = ?
				
				")->execute($id);
				
		while($object->next())
		{
			$this->id = $object->shipping_id;
			$this->name = $object->shipping_name;
			$this->title = $object->shipping_title;
			$this->title_en = $object->shipping_title_en;
			$this->description = $object->shipping_description;
			$this->description_en = $object->shipping_description_en;
		}
	}
	
	public function get($order, $country_id)
	{
		/*$shippings = array();
		$shippingAddr = new PnAddress();
        $shippingAddr->initWithId($order->shipping_address_id);
        $country_id = $shippingAddr->country_id;*/
		
		$query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND (
                        shipping_options.shipping_id IN (1, 2, 12) 
                        OR shipping_options.shipping_id = 11
                        OR shipping_options.shipping_id = 21
                        ) 
                    AND shipping_options.shipping_id != 2 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 11 
                    AND shipping_options.shipping_id != 12 
                    ORDER BY shipping_pos
                ";
        if($order->getTotalPriceWithoutCoupon("de", false) > $GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN'] && $country_id == 2){
        	$country2 = true;
        }       
        if($country_id == 1 || $country2)
        {
        	$checkCountry = $country_id != 2?"AND shipping_options.shipping_id != 11":"AND shipping_options.shipping_id != 12 AND shipping_options.shipping_id != 2 AND shipping_options.shipping_id != 21";
            $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 11
                    AND shipping_options.shipping_id != 12 
                    ORDER BY shipping_pos
                ";
                
            if($order->getTotalPriceWithoutCoupon("de", false) > $GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN'])
            {
                $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4
                    ".$checkCountry."
                    AND shipping_options.shipping_id != 1 
                    ORDER BY shipping_pos
                ";
            }
            
            $flag = true;
            
            foreach($order->getTopLevelItems() as $item)
            {
                if($item->class_id != 14)
                    $flag = false;
            }
            
            if($flag)
            {
                $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7
                    AND shipping_options.shipping_id != 11
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 12 
                    ORDER BY shipping_pos
                ";
            }
            
            if(($order->getTotalPriceWithoutCoupon("de", false) > $GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN']) && $flag)
            {
                $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4
                    ".$checkCountry."
                    AND shipping_options.shipping_id != 1 
                    ORDER BY shipping_pos
                ";
            }
        }
		
		$objects = $this->Database->prepare($query)->execute();
		
		while($objects->next())
		{
			$shipping 					= new PnShipping();
			$shipping->id 				= $objects->shipping_id;
			$shipping->name 			= $objects->shipping_name;
			$shipping->title 			= $objects->shipping_title;
			$shipping->title_en 		= $objects->shipping_title_en;
			$shipping->description 		= $objects->shipping_description;
			$shipping->description_en 	= $objects->shipping_description_en;
			
			$shippings[]				= $shipping;
		}
		
		return $shippings;
	}
	
	public function getPrice($order, $locale="de")
	{
	    $shippingAddr = new PnAddress();
        $shippingAddr->initWithId($order->shipping_address_id);
        $country_id = $shippingAddr->country_id;
        
		$object = $this->Database->prepare("
				
					SELECT p.`price_amount`, p.`price_amount_uk`
					FROM " . $this->config->database . ".finanz_artikel fa
					LEFT JOIN " . $this->config->database . ".prices p
					ON fa.`artikel_id` = p.`artikel_id`
					WHERE fa.`class_id` = 7
					AND fa.`country_id` = ?
					AND fa.`shipping_id` = ?
					AND fa.`artikel_active` = 1
					AND ( ISNULL(p.`price_validuntil`) OR p.`price_validuntil` > now() )
				
				")->execute($country_id, $this->id);
		
		while($object->next())
		{
			if($locale == "de")
				return $object->price_amount;
			return $object->price_amount_uk;
		}
		
	}
	
	public function getPnArticle($order)
	{
	    $shippingAddr = new PnAddress();
        $shippingAddr->initWithId($order->shipping_address_id);
        $country_id = $shippingAddr->country_id;
        
        $objDb = $this->Database->prepare("
                SELECT *
                FROM " . $this->config->database . ".finanz_artikel fa
                WHERE fa.class_id = 7
                AND fa.country_id = ?
                AND fa.shipping_id = ?
                AND fa.artikel_active = 1
            ")->execute($country_id, $this->id);
            
        $article = new PnArticle();
        while($objDb->next())
        {
            $article->id					= $objDb->artikel_id;
			$article->book_id 				= $objDb->buch_id;
			$article->design_id 			= $objDb->design_id;
			$article->version_id 			= $objDb->version_id;
			$article->class_id 			    = $objDb->class_id;
			$article->class_name 			= $objDb->class_singular;
			$article->konto_id              = $objDb->konto_id;
			$article->pieces_from_parent 	= $objDb->class_pieces_from_parent;
			$article->name					= $objDb->artikel_name;
			$article->name_en 				= $objDb->artikel_name_en;
			$article->description 			= $objDb->artikel_desc;
			$article->description_en 		= $objDb->artikel_desc_en;
			$article->image_asset_id 		= $objDb->image_asset_id;
			$arrname						= explode(":", $objDb->artikel_name);
			$article->shortname				= count($arrname) > 1 ? trim($arrname[1]) : $objDb->artikel_name;
        }
        
        return $article;
	}
	
	public function recalc($order)
	{
	    $posten = new PnPosten();
	    $zahlungen = new PnZahlungen();
	    $shippingArticle = $this->getPnArticle($order);
	    $posten->initShippingPosten($order->id);
	    
	    $address = new PnAddress();
	    $address->initWithId($order->billing_address_id);
	    
	    $taxRate          = $this->getTaxRate($shippingArticle->id, $address->country_id);
	    $steuerart_id     = $taxRate->steuerart_id ? $taxRate->steuerart_id : 1;
	    $steuersatz_id    = $taxRate->id ? $taxRate->id : 1;
	    
	    $konto_id = $order->konto_id;
	    
	    $quantity = 1;
	    $price = $this->getPrice($order);
	    
	    if($posten->id !="" || $posten->id > 0)
	    {
	        $posten->edit(
	                array(
                        "steuerart_id" => $steuerart_id,
                        "artikel_id" => $shippingArticle->id,
                        "posten_anzahl" => $quantity,
                        "posten_einzelbetrag" => $price,
                        "posten_netto" => 0,
                        "posten_text" => mysql_real_escape_string($shippingArticle->name),
                        "posten_manuell" => 0,
                        "posten_updated" => date('Y/m/d H:i:s a', time()),
                        "order_id" => $order->id,
                        "currency_id" => 1,
                        "steuersatz_id" => $steuersatz_id
                    )
	            );
	        
	        $zahlungen->initWithPostenId($posten->id);
	        $zahlungen->edit(
	                array(
	                    "steuerart_id" => $steuerart_id,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "ust_id" => 1,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => $price,
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($shippingArticle->name),
                        "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => $steuersatz_id
	                    )
	            );
	        
	        $zahlungen->initChild();
	        $zahlungen->edit(
	                array(
	                    "steuerart_id" => $steuerart_id,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "order_id" => $order->id,
                        "ust_id" => 1,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => -1 * floatVal($price),
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($shippingArticle->name),
                        "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => $steuersatz_id
	                    )
	            );
	    }
	    else
	    {
	        $posten_id = $posten->add(
                    array(
                        "steuerart_id" => $steuerart_id,
                        "konto_id" => $konto_id,
                        "artikel_id" => $shippingArticle->id,
                        "posten_anzahl" => $quantity,
                        "posten_einzelbetrag" => $price,
                        "posten_netto" => 0,
                        "posten_text" => mysql_real_escape_string($shippingArticle->name),
                        "posten_datum" => date('Y/m/d H:i:s a', time()),
                        "posten_manuell" => 0,
                        "posten_created" => date('Y/m/d H:i:s a', time()),
                        "posten_updated" => date('Y/m/d H:i:s a', time()),
                        "order_id" => $order->id,
                        "currency_id" => 1,
                        "steuersatz_id" => $steuersatz_id
                    )
	            );
	        
	        $zahlung_parent_id = $zahlungen->add(
                        array(
                            "steuerart_id" => $steuerart_id,
                            "journal_id" => 1,
                            "stapel_id" => 3,
                            "posten_id" => $posten_id,
                            "order_id" => $order->id,
                            "ust_id" => 1,
                            "konto_id" => $shippingArticle->konto_id,
                            "zahlung_anzahl" => $quantity,
                            "zahlung_typ" => 9,
                            "zahlung_andereskonto" => 0,
                            "zahlung_stapel" => 1,
                            "zahlung_betrag" => $price,
                            "zahlung_netto" => 0,
                            "zahlung_text" => mysql_real_escape_string($shippingArticle->name),
                            "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                            "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                            "zahlung_created" => date('Y/m/d H:i:s a', time()),
                            "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                            "currency_id" => 1,
                            "zahlung_steuersatz" => 7,
                            "steuersatz_id" => $steuersatz_id
                            )
                    );
            
            $zahlung_child_id = $zahlungen->add(
                        array(
                            "steuerart_id" => $steuerart_id,
                            "journal_id" => 1,
                            "stapel_id" => 3,
                            "parent_id" => $zahlung_parent_id,
                            "posten_id" => $posten_id,
                            "order_id" => $order->id,
                            "ust_id" => 1,
                            "konto_id" => $konto_id,
                            "zahlung_anzahl" => $quantity,
                            "zahlung_typ" => 9,
                            "zahlung_andereskonto" => 0,
                            "zahlung_stapel" => 1,
                            "zahlung_betrag" => -1 * floatVal($price),
                            "zahlung_netto" => 0,
                            "zahlung_text" => mysql_real_escape_string($shippingArticle->name),
                            "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                            "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                            "zahlung_created" => date('Y/m/d H:i:s a', time()),
                            "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                            "currency_id" => 1,
                            "zahlung_steuersatz" => 7,
                            "steuersatz_id" => $steuersatz_id
                            )
                    );
	    }
	}
	
	public function getTaxRate($article_id, $country_id)
	{
	    $steuersatz = new Steuersatz();
	    
	    $objDb = $this->Database->prepare("
	        SELECT stz.steuersatz_id, stz.steuerart_id, stz.steuersatz_rate, stz.steuersatz_validfrom, stz.steuersatz_validuntil 
            FROM " . $this->config->database . ".artikel_steuerart ars
            LEFT JOIN " . $this->config->database . ".steuerart ste
            ON ars.`steuerart_id` = ste.`steuerart_id`
            LEFT JOIN " . $this->config->database . ".steuersatz stz
            ON ste.`steuerart_id` = stz.`steuerart_id`
            WHERE artikel_id = ?
            AND country_id = ?
            AND (stz.`steuersatz_validfrom` is null OR stz.`steuersatz_validfrom` <= now())
            AND (stz.`steuersatz_validuntil` is null OR stz.`steuersatz_validuntil` >= now())
        ")->execute($article_id, $country_id);
        
        while($objDb->next())
        {
            $steuersatz->id                     = $objDb->steuersatz_id;
            $steuersatz->steuerart_id           = $objDb->steuerart_id;
            $steuersatz->steuersatz_rate        = $objDb->steuersatz_rate;
            $steuersatz->steuersatz_validfrom   = $objDb->steuersatz_validfrom;
            $steuersatz->steuersatz_validuntil  = $objDb->steuersatz_validuntil;
            
            break; //get first row
        }
        
        return $steuersatz;
	}
}

?>