<?php

class PnAmazonCBAPurchaseContract
{
    protected $service;
    
    public function __construct()
    {
        $this->service = new PnAmazonClient(PnAmazonConfig::getAccessKey(), PnAmazonConfig::getSecretKey());
    }
    
    public function createPurchaseContract()
    {
        $request = new PnAmazonModelCreatePurChaseContractRequest();
        try
        {
            $response = $this->service->createPurchaseContract($request);
            if ($response->isSetCreatePurchaseContractResult())
            {
                $createPurchaseContractResult = $response->getCreatePurchaseContractResult();
                if ($createPurchaseContractResult->isSetPurchaseContractId())
                {
                    return $createPurchaseContractResult->getPurchaseContractId();  
                }
            }
        }
        catch (CheckoutByAmazon_Service_Exception $ex) 
        {
            //$this->createCbaServiceException($ex);
        }
    }
    
    public function getAddress($purchase_contract_id)
    {
        $request = new PnAmazonModelGetPurchaseContractRequest(array('PurchaseContractId' => $purchase_contract_id));
        $addressArray = array();
        $addressCount = 0;
        
        try
        {
            $response = $this->service->getPurchaseContract($request);
            if ($response->isSetGetPurchaseContractResult()) 
            {
                $getPurchaseContractResult = $response->getGetPurchaseContractResult();
                if ($getPurchaseContractResult->isSetPurchaseContract())
                {
                    $purchaseContract = $getPurchaseContractResult->getPurchaseContract();
                    if($purchaseContract->isSetDestinations())
                    {
                        $destinations = $purchaseContract->getDestinations();
                        $destinationList = $destinations->getDestination();     
                        foreach ($destinationList as $destination)
                        {
                            $state = $purchaseContract->getState();
                            $physicalDestinationAttributes = $destination->getPhysicalDestinationAttributes();
                            if ($physicalDestinationAttributes->isSetShippingAddress()) 
                            {
                                    $shippingAddressList[$addressCount++] = $physicalDestinationAttributes->getShippingAddress();
                            }
                        }
                    }
                }
            }
            return $shippingAddressList;
        }
        catch (CheckoutByAmazon_Service_Exception $ex)
        {
         //
        }
    }
    
    public function setItems($purchaseContractId,$itemList)
    {
         $request = new PnAmazonModelSetPurchaseItemsRequest();
         $request->setPurchaseContractId($purchaseContractId);
         $request->setPurchaseItems($itemList);
         try
         {
             $response = $this->service->setPurchaseItems($request);
             return 1;
         }
         catch (CheckoutByAmazon_Service_Exception $ex)
         {
             $this->createCbaServiceException($ex);
         }
    }
    
    public function completeOrder($purchaseContractId, $integratorId, $integratorName )
    {
        $request = new PnAmazonModelCompletePurchaseContractRequest();
        try 
        {
            $request->setPurchaseContractId($purchaseContractId);
            if($integratorId)
            {
                $request->setIntegratorId($integratorId);
            }
            if($integratorName)
            {
                $request->setIntegratorName($integratorName);        
            }
            
            $response = $this->service->completePurchaseContract($request);
            if ($response->isSetCompletePurchaseContractResult()) 
            {
                $completePurchaseContractResult = $response->getCompletePurchaseContractResult();
                if ($completePurchaseContractResult->isSetOrderIds()) 
                {
                    $orderIds = $completePurchaseContractResult->getOrderIds();
                    $orderIdList  =  $orderIds->getOrderId();
                    return $orderIdList;
                }
            }
        }
        catch (CheckoutByAmazon_Service_Exception $ex) 
        {
            $this->createCbaServiceException($ex);
        }
    }
    
    public function setContractCharges($purchase_contract_id, $charges)
    {
        $request = new PnAmazonModelSetContractChargesRequest();
        $request->setPurchaseContractId($purchase_contract_id);
        $request->setCharges($charges);
        try 
        {
            $response = $this->service->setContractCharges($request);
            return 1;
        }
        catch(Exception $ex)
        {
            print_r($ex);
        }
    }
}
