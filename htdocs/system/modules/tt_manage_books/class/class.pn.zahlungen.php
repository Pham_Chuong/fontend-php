<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnZahlungen extends Controller
{
    public $id;
    public $steuerart_id;
    public $journal_id;
    public $stapel_id;
    public $protokoll_id;
    public $parent_id;
    public $posten_id;
    public $order_id;
    public $ust_id;
    public $konto_id;
    public $coupon_id;
    public $anzahl;
    public $typ;
    public $andereskonto;
    public $stapel;
    public $betrag;
    public $netto;
    public $name;
    public $text;
    public $datum;
    public $aluta;
    public $created;
    public $updated;
    public $currency_id;
    public $steuersatz;
    public $total_netto;
    public $total_brutto;
    public $steuersatz_id;
    public $otherorder;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database .".finanz_zahlungen
	            WHERE zahlung_id = ?
	        ")->execute($id);
	        
        while($objDb->next())
        {
            $this->id                   = $objDb->zahlung_id;
            $this->steuerart_id         = $objDb->steuerart_id;
            $this->journal_id           = $objDb->journal_id;
            $this->stapel_id            = $objDb->stapel_id;
            $this->protokoll_id         = $objDb->protokoll_id;
            $this->parent_id            = $objDb->parent_id;
            $this->posten_id            = $objDb->posten_id;
            $this->order_id             = $objDb->order_id;
            $this->ust_id               = $objDb->ust_id;
            $this->konto_id             = $objDb->konto_id;
            $this->coupon_id            = $objDb->coupon_id;
            $this->anzahl               = $objDb->zahlung_anzahl;
            $this->typ                  = $objDb->zahlung_typ;
            $this->andereskonto         = $objDb->zahlung_andereskonto;
            $this->stapel               = $objDb->zahlung_stapel;
            $this->betrag               = $objDb->zahlung_betrag;
            $this->netto                = $objDb->zahlung_netto;
            $this->name                 = $objDb->zahlung_name;
            $this->text                 = $objDb->zahlung_text;
            $this->datum                = $objDb->zahlung_datum;
            $this->valuta               = $objDb->zahlung_valuta;
            $this->created              = $objDb->zahlung_created;
            $this->updated              = $objDb->zahlung_updated;
            $this->currency_id          = $objDb->currency_id;
            $this->steuersatz           = $objDb->zahlung_steuersatz;
            $this->total_netto          = $objDb->zahlung_total_netto;
            $this->total_brutto         = $objDb->zahlung_total_brutto;
            $this->steuersatz_id        = $objDb->steuersatz_id;
            $this->otherorder           = $objDb->zahlung_otherorder;
        }
	}
	
	public function get($params = array())
	{
	    $results = array();
	    
	    if(count($params) > 0)
	    {
	        
	    }
	    else
	    {
	        
	    }
	    
	    return $results;
	}
	
	public function initWithPostenId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database .".finanz_zahlungen
	            WHERE posten_id = ? AND isnull(parent_id)
	        ")->execute($id);
	        
        while($objDb->next())
        {
            $this->id                   = $objDb->zahlung_id;
            $this->steuerart_id         = $objDb->steuerart_id;
            $this->journal_id           = $objDb->journal_id;
            $this->stapel_id            = $objDb->stapel_id;
            $this->protokoll_id         = $objDb->protokoll_id;
            $this->parent_id            = $objDb->parent_id;
            $this->posten_id            = $objDb->posten_id;
            $this->order_id             = $objDb->order_id;
            $this->ust_id               = $objDb->ust_id;
            $this->konto_id             = $objDb->konto_id;
            $this->coupon_id            = $objDb->coupon_id;
            $this->anzahl               = $objDb->zahlung_anzahl;
            $this->typ                  = $objDb->zahlung_typ;
            $this->andereskonto         = $objDb->zahlung_andereskonto;
            $this->stapel               = $objDb->zahlung_stapel;
            $this->betrag               = $objDb->zahlung_betrag;
            $this->netto                = $objDb->zahlung_netto;
            $this->name                 = $objDb->zahlung_name;
            $this->text                 = $objDb->zahlung_text;
            $this->datum                = $objDb->zahlung_datum;
            $this->valuta               = $objDb->zahlung_valuta;
            $this->created              = $objDb->zahlung_created;
            $this->updated              = $objDb->zahlung_updated;
            $this->currency_id          = $objDb->currency_id;
            $this->steuersatz           = $objDb->zahlung_steuersatz;
            $this->total_netto          = $objDb->zahlung_total_netto;
            $this->total_brutto         = $objDb->zahlung_total_brutto;
            $this->steuersatz_id        = $objDb->steuersatz_id;
            $this->otherorder           = $objDb->zahlung_otherorder;
        }
        
	}
	
	public function initChild()
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database .".finanz_zahlungen
	            WHERE parent_id = ?
	        ")->execute($this->id);
	        
        while($objDb->next())
        {
            $this->id                   = $objDb->zahlung_id;
            $this->steuerart_id         = $objDb->steuerart_id;
            $this->journal_id           = $objDb->journal_id;
            $this->stapel_id            = $objDb->stapel_id;
            $this->protokoll_id         = $objDb->protokoll_id;
            $this->parent_id            = $objDb->parent_id;
            $this->posten_id            = $objDb->posten_id;
            $this->order_id             = $objDb->order_id;
            $this->ust_id               = $objDb->ust_id;
            $this->konto_id             = $objDb->konto_id;
            $this->coupon_id            = $objDb->coupon_id;
            $this->anzahl               = $objDb->zahlung_anzahl;
            $this->typ                  = $objDb->zahlung_typ;
            $this->andereskonto         = $objDb->zahlung_andereskonto;
            $this->stapel               = $objDb->zahlung_stapel;
            $this->betrag               = $objDb->zahlung_betrag;
            $this->netto                = $objDb->zahlung_netto;
            $this->name                 = $objDb->zahlung_name;
            $this->text                 = $objDb->zahlung_text;
            $this->datum                = $objDb->zahlung_datum;
            $this->valuta               = $objDb->zahlung_valuta;
            $this->created              = $objDb->zahlung_created;
            $this->updated              = $objDb->zahlung_updated;
            $this->currency_id          = $objDb->currency_id;
            $this->steuersatz           = $objDb->zahlung_steuersatz;
            $this->total_netto          = $objDb->zahlung_total_netto;
            $this->total_brutto         = $objDb->zahlung_total_brutto;
            $this->steuersatz_id        = $objDb->steuersatz_id;
            $this->otherorder           = $objDb->zahlung_otherorder;
        }
	}
	
	public function add($params = array())
	{
	    $key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = $v;
		}
		
		$objDb = $this->Database->prepare('
					INSERT INTO ' . $this->config->database . '.finanz_zahlungen ('. implode(', ', $key) .') 
					VALUES("' . implode('", "', $value) . '")
				')->execute();
		
		return $objDb->insertId;
	}
	
	public function edit($params = array())
	{
	    $set = array();
		foreach ($params as $k => $v)
		{
			$set[] = $k . '  =  "' . $v . '"';
		}
		
		$object = $this->Database->prepare("UPDATE " . $this->config->database . ".finanz_zahlungen SET " . implode(", ", $set) . " WHERE zahlung_id = ?" )->execute($this->id);
		
		return $object->affectedRows;
	}
	
	public function removeCoupon($order_id,$zahlung_text)
	{ 
      
		$object = $this->Database->prepare("DELETE FROM  " . $this->config->database . ".finanz_zahlungen WHERE order_id=? AND zahlung_text=?" )->executeUncached($order_id,$zahlung_text);
		$object->affectedRows;
	}
    public function selectCoupon($order_id,$zahlung_text)
    {
        $fla = 0;
        $objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database .".finanz_zahlungen
                WHERE order_id=? AND zahlung_text=?" )->executeUncached($order_id,$zahlung_text);

        while($objDb->next())
        {
            $fla++;
        }
        
        if($fla > 0)
            return true;
        return false;
    }
	
	public function remove()
	{
	    
	}
}

?>
