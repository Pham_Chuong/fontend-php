<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnEtiquette extends Controller
{
    public $id;
    public $name;
    public $bg_asset_id;
    public $mask_asset_id;
    public $glitter_asset_id;
    public $text_fontsize_pct;
    public $active;
    public $format_id;
    public $text_margin_top;
    public $text_margin_right;
    public $text_margin_bottom;
    public $text_margin_left;
    public $updated;
    public $photo_allowed;
    public $photo_x;
    public $photo_y;
    public $photo_w;
    public $photo_h;
    public $default_fontcolor;
    public $text_maxlines;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT *
	            FROM " . $this->config->database . ".etiquettes
	            WHERE etiquette_id = ?
	        ")->execute($id);
	        
	    while($objDb->next())
	    {
	        $this->id                    = $objDb->etiquette_id;
            $this->name                  = $objDb->etiquette_name;
            $this->bg_asset_id           = $objDb->etiquette_bg_asset_id;
            $this->mask_asset_id         = $objDb->etiquette_mask_asset_id;
            $this->glitter_asset_id      = $objDb->etiquette_glitter_asset_id;
            $this->text_fontsize_pct     = $objDb->etiquette_text_fontsize_pct;
            $this->active                = $objDb->etiquette_active;
            $this->format_id             = $objDb->etiquette_format_id;
            $this->text_margin_top       = $objDb->etiquette_text_margin_top;
            $this->text_margin_right     = $objDb->etiquette_text_margin_right;
            $this->text_margin_bottom    = $objDb->etiquette_text_margin_bottom;
            $this->text_margin_left      = $objDb->etiquette_text_margin_left;
            $this->updated               = $objDb->etiquette_updated;
            $this->photo_allowed         = $objDb->etiquette_photo_allowed;
            $this->photo_x               = $objDb->etiquette_photo_x;
            $this->photo_y               = $objDb->etiquette_photo_y;
            $this->photo_w               = $objDb->etiquette_photo_w;
            $this->photo_h               = $objDb->etiquette_photo_h;
            $this->default_fontcolor     = $objDb->etiquette_default_fontcolor;
            $this->text_maxlines         = $objDb->etiquette_text_maxlines;
	    }
	}
	
	public function getTagName()
	{
	   $objDb = $this->Database->prepare("
	            SELECT * 
	            FROM " . $this->config->database . ".etiquette_buchtags eb
	            LEFT JOIN " . $this->config->database . ".buch_tags2 bt
	            ON eb.tag_id = bt.tag_id
	            WHERE eb.etiquette_id = ?
	            AND bt.category_id = 17
	        ")->execute($this->id);
	        
	    $tagname = "Standard";
	    while($objDb->next())
	    {
	        $tagname = $objDb->tag_value;
	    }
	    return $tagname;
	    	    
	}
	
	public function getAllowColors()
	{
	    $objDb = $this->Database->prepare("
	            SELECT *
                FROM " . $this->config->database . ".etiquette_text_colors e
                LEFT JOIN " . $this->config->database . ".colors c
                ON e.color_id = c.color_id
                WHERE etiquette_id = ?
	        ")->execute($this->id);
	        
	    $colors = array();
	    while($objDb->next())
	    {
	        $colors[] = $objDb->color_value;
	    }
	    
	    return $colors;
	}
	
	public function getPreviewUrl($param)
	{
	    $preview_url = sprintf($GLOBALS['TL_CONFIG']['image_root'] . "/preview/%s/%s/%s.png", "Etiquette", $this->id, 
								md5("nilfEvdupjevOdMacKihikGedTymFemhebliesfabOjibyekew" . "Etiquette" . $this->id));
		
		if(count($param) > 0)
		{
			$qstr = array();
			foreach ($param as $k => $v)
			{
				$qstr[] = "$k=$v";
			}
			
			$preview_url .= "?" . implode('&', $qstr);
		}
		
		
		return $preview_url;
	    //return $this->config->genericPreview("Etiquette", $this->id, $param);
	}
	
	public function getFormat()
	{
	    $arrFormat = array("name" => "Wein", "width" => 85, "height" => 100);
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".etiquette_formats
	            WHERE etiquette_format_id = ?
	        ")->execute($this->format_id);
	        
	    while($objDb->next())
	    {
	        $arrFormat = array(
	            "name" => $objDb->etiquette_format_name,
	            "width" => $objDb->etiquette_format_width,
	            "height" => $objDb->etiquette_format_height
            );
	    }
	    return $arrFormat;
	}
}

?>