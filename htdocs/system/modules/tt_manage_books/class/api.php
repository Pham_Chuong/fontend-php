<!DOCTYPE html>

<html>
<head>
<title></title>
<style>
input { margin: 10px 0 0 }
</style>
</head>
<body>

<div style="width: 250px;" >
	<form method="post" name="myform" id="myform" action="">
		<table>
			<tr>
				<td><label>Request URL</label></td>
				<td><input type=text name="url" value="<?php echo $_POST['url'] ? $_POST['url'] : "pnovel.local/partner/Order/" ?>" /></td>
			</tr>
			<tr>
				<td><label>Username</label></td>
				<td><input type=text name="username" value="<?php echo $_POST['username'] ? $_POST['username'] : "nguyenson" ?>" /></td>
			</tr>
			<tr>
				<td><label>Password</label></td>
				<td><input type=password name="password" value="<?php echo $_POST['password'] ? $_POST['password'] : "123456789" ?>" /></td>
			</tr>
			<tr>
				<td colspan=2>
					<input type="submit" name="getOrder" value="Get Order" />
					<input type="submit" name="addOrder" value="Add Order" />
					<input type="submit" name="addItem" value="Add Item" />
				</td>
			</tr>
		</table>
	</form>
</div>
<hr />
<h2>Response</h2>
<div>

<?php 

// jSON URL which should be requested
$json_url = $_POST['url'];

$username = $_POST['username'];  // authentication
$password = $_POST['password'];  // authentication

// Initializing curl
$ch = curl_init( $json_url );

if($_POST['getOrder']) 
{
	// jSON String for request
	$json_string = json_encode(array("jsondata" => array("parent" => "993377")));
	
	// Configuring curl options
	$options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_USERPWD => $username . ":" . $password,   // authentication
		CURLOPT_HTTPHEADER => array('Content-type: application/json', 'Content-Length: ' . strlen($json_string), "Accept: application/json") ,
		CURLOPT_POSTFIELDS => $json_string,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_FOLLOWLOCATION => true,
	);
	
	// Setting curl options
	curl_setopt_array( $ch, $options );
}

if($_POST['addOrder'])
{
	$json_string = json_encode(
		array(
			"shipping_address" => '/partner/Address/250971/', 
			"billing_address" => '/partner/Address/250971/',
			"email" => "hoaisonng@gmail.com",
			"mandant" => "/partner/Mandant/2/"
			)
		);
	
	$options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_USERPWD => $username . ":" . $password,   // authentication
		CURLOPT_HTTPHEADER => array('Content-type: application/json', 'Content-Length: ' . strlen($json_string), "Accept: application/json") ,
		CURLOPT_POSTFIELDS => $json_string,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_FOLLOWLOCATION => true,
	);
	
	curl_setopt_array( $ch, $options );
}

if($_POST['addItem'])
{
	$json_string = json_encode(
		array(
			"parent" => "1835222",
			"email" =>"hoaisonng@gmail.com"
			)
		);
	
	$options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_USERPWD => $username . ":" . $password,   // authentication
		CURLOPT_HTTPHEADER => array('Content-type: application/json', 'Content-Length: ' . strlen($json_string), "Accept: application/json") ,
		CURLOPT_POSTFIELDS => $json_string,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_FOLLOWLOCATION => true,
	);
	
	curl_setopt_array( $ch, $options );
}

// Getting results
$result =  curl_exec($ch); // Getting jSON result string
print_r($result);
echo "<hr />";
echo "<h2>CURL INFO</h2>";
foreach(curl_getinfo($ch) as $k=>$v)
{
	echo $k . ": ";
	print_r($v);
	echo "<br />";
}

curl_close($ch);

?>


</div>

</body>
</html>

