<?php

class PnConfig
{
	public $site_secret;
	public $image_root;
	public $database;
	
	function __construct()
	{
		$this->site_secret = "nilfEvdupjevOdMacKihikGedTymFemhebliesfabOjibyekew";
		$this->image_root  = $GLOBALS['TL_CONFIG']['image_root'];
		$this->database    = "personalnovel_backend ";
	}
	
	public function random_string($length)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}
	
	public function startsWith($haystack, $needle)
	{
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}
	
	public function endsWith($haystack, $needle)
	{
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}
	
		return (substr($haystack, -$length) === $needle);
	}
	
	public function genericPreview($model, $id, $param=array())
	{
		$preview_url = sprintf($this->image_root . "/preview/%s/%s/%s.png", $model, $id, 
								md5($this->site_secret . $model . $id));
		
		if(count($param) > 0)
		{
			$qstr = array();
			foreach ($param as $k => $v)
			{
				$qstr[] = "$k=$v";
			}
			
			$preview_url .= "?" . implode('&', $qstr);
		}
		
		return $preview_url;
	}
	
	public function makeThumnail($image_file_name, $width="", $height="", $extension="png")
	{
		$thumb_url 			= "/thumbs/";
		$thumb_path			= implode("/", str_split(substr(str_replace(".", "", $image_file_name), 0, 3)));
		
		/* fix me: dont know why */
		if($image_file_name == "geschenkbox_rot_offen.jpg")
		    $image_file_name = "geschenkbox_rot_offen.png";
		
		if($height && $width)
			$thumbnail_tag		= sprintf("w%sxh%s", $width, $height);
		else if($height)
			$thumbnail_tag		= sprintf("h%s", $height);
		else
		    $thumbnail_tag		= sprintf("w%s", $width);
		    
		$thumbnail_name 		= $thumbnail_tag . "-" . md5($this->site_secret . $thumbnail_tag . $image_file_name) . "-" .$image_file_name;
		$thumb_url	   	   .= $thumb_path . "/" .$thumbnail_name;
		
		return sprintf($this->image_root . "%s", $thumb_url);
	}
	
}

?>
