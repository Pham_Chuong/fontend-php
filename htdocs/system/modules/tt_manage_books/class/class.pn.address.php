<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnAddress extends Controller
{
	public $id;
	public $firstName;
	public $lastName;
	public $company;
	public $address;
	public $address2;
	public $city;
	public $zip;
	public $phone;
	public $fax;
	public $country_id;
	public $anrede;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".addresses
				WHERE address_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 			= $object->address_id;
			$this->firstName 	= $object->address_vorname;
			$this->lastName 		= $object->address_nachname;
			$this->company 		= $object->address_firma;
			$this->address 		= $object->address_address;
			$this->city 			= $object->address_city;
			$this->zip 			= $object->address_zip;
			$this->country_id 	= $object->country_id;
			$this->anrede 		= $object->address_anrede;
		}
	}
	
	public function getCountry($locale="de")
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".countries
				WHERE country_id = ?
			")->execute($this->country_id);
			
		while($object->next())
		{
			if($locale=="de")
				return $object->country_name;
			return $object->country_name_en;
		}
		
		return "";
	}
	
	public function add($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = mysql_real_escape_string($v);
		}
		
		$object = $this->Database->prepare("
					INSERT INTO " . $this->config->database . ".addresses (". implode(", ", $key) .") 
					VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $object->insertId;
	}
	
	public function edit($params = array())
	{
		$set = array();
		foreach ($params as $k => $v)
		{
			$set[] = $k . "  =  '" . mysql_real_escape_string($v) . "'";
		}
		
		$object = $this->Database->prepare("UPDATE " . $this->config->database . ".addresses SET " . implode(", ", $set) . " WHERE address_id = ?" )->execute($this->id);
		
		return $object->affectedRows;
	}
}

?>
