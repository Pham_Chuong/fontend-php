<?php

class PnAmazonMerchantValues
{
    private $merchant_id;
    private $access_key;
    private $secret_key;
    private $weight_unit;
    private $currency_code;
    private $version;
    private $cba_service_url;
    private $ca_bundle_file;
    
    private function __construct()
    {
         $this->merchantId = PnAmazonConfig::getMerchantId();
         $this->accessKey  = ACCESS_KEY_ID;
         $this->secretKey  = SECRET_ACCESS_KEY;
         $this->weightUnit = WEIGHT_UNIT;
         $this->currencyCode = CURRENCY_CODE;
         $this->version = VERSION;
         $this->cbaServiceUrl = CBA_SERVICE_URL;
         $this->caBundleFile = "";
         
         if($this->merchantId  == "")
         {
             trigger_error("Merchant Id not set in the properties file ",E_USER_ERROR);
             exit(0);
         }
         if($this->accessKey == "")
         {
             trigger_error("AccessKey not set in the properties file ",E_USER_ERROR);
             exit(0);
         }
         if($this->secretKey == "")
         {
             trigger_error("Secret Key not set in the properties file ",E_USER_ERROR);
             exit(0);
         }
         if($this->weightUnit == "")
         {
             trigger_error("Weight Unit not set in the properties file ",E_USER_ERROR);
             exit(0);
         }
         if($this->currencyCode == "")
         {
             trigger_error("Currency Code not set in the properties file ",E_USER_ERROR);
             exit(0);
         }
         if($this->version == "")
         {
             trigger_error("Version Id not set in the properties file ",E_USER_ERROR);
             exit(0);
         }
         if($this->cbaServiceUrl == "")
         {
             trigger_error("CbaServiceUrl not set in the properties file ",E_USER_ERROR);
             exit(0);
         }
         if($this->caBundleFile == "")
         {
             $this->caBundleFile = null;
         }
    
     }
}
