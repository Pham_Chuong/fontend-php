<?php

class PnOrderNotification extends Controller
{
    function __construct()
	{
	    parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function writeLog($event_key, $event_text, $order_id)
	{
	    $db = $this->Database->prepare("
	        INSERT INTO " . $this->config->database .".events(`event_key`, `event_text`, `event_date`, `order_id`)
	        VALUES (?, ?, ?, ?)
        ")->execute($event_key, $event_text, date('Y/m/d H:i:s a', time()), $order_id);
	}
	
	public function existUUID($event_key, $uuid)
	{
	    $db = $this->Database->prepare("
	        SELECT event_id
	        FROM " . $this->config->database . ".events
	        WHERE event_key = ? AND event_text = ?
        ")->executeUncached($event_key, $event_text);
        
        if($db->numRows > 0)
        {
            return true;
        }
        
        return false;
	}
}
