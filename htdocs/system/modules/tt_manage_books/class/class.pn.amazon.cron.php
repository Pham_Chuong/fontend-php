<?php

define('APPLICATION_NAME', 'PersonalNOVEL APP');
define('APPLICATION_VERSION', 'v0.1');
define ('MERCHANT_ID', 'A3LK15BEHXD69X');

class PnAmazonCron extends Controller
{
    function __construct()
	{
	    parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function confirmShipment()
	{
	    $query_db = $this->Database->prepare("
                SELECT i.item_id, i.order_id, si.shipment_id, o.order_foreign_code, o.order_finis
                FROM " . $this->config->database . ".items i
                JOIN " . $this->config->database . ".orders o
                ON i.order_id = o.order_id
                JOIN " . $this->config->database . ".shipment_item si
                ON i.item_id = si.item_id
                
                WHERE o.payment_method_id = 12
                AND o.order_finis IS NOT NULL
                AND o.order_update IS NULL
                AND o.order_date > '2014-04-01'
                GROUP BY o.order_id
	        ")->executeUncached();
	        
        $feed = "order-id\tship-date\tcarrier-code\ttracking-number\n";
        
        if($query_db->numRows == 0) die();
        
        $order_ids = array();
        while($query_db->next())
        {
            $order_ids[] = $query_db->order_id;
            $amazon_order_id = str_replace("AMAZON-", "", $query_db->order_foreign_code);
            $shipdate = substr($query_db->order_finis, 0, 10);
            $carrier_code = "DHL";
            $tracking_number = $query_db->shipment_id;
            
            $feed .= sprintf("%s\t%s\t%s\t%s\n", $amazon_order_id, $shipdate, $carrier_code, $tracking_number);
        }
        
        $serviceUrl = "https://mws.amazonservices.co.uk";
        $config = array (
          'ServiceURL' => $serviceUrl,
          'ProxyHost' => null,
          'ProxyPort' => -1,
          'MaxErrorRetry' => 3,
        );
        
        $service = new MarketplaceWebService_Client(
            'AKIAJQCET33HDSTROFGA', 
            'c9uG2yFUsejAtYbhjAY+5teIdK0qnNLJbkZyOJ0h', 
            $config,APPLICATION_NAME,APPLICATION_VERSION
        );
        
        $marketplaceIdArray = array("Id" => array('A1OCY9REWJOCW5'));
        
        $feedHandle = fopen('php://temp', 'rw+');
        fwrite($feedHandle, $feed);
        rewind($feedHandle);
        
        $parameters = array (
          'Merchant' => MERCHANT_ID,
          'MarketplaceIdList' => $marketplaceIdArray,
          'FeedType' => '_POST_FLAT_FILE_FULFILLMENT_DATA_',
          'FeedContent' => $feedHandle,
          'PurgeAndReplace' => false,
          'ContentMd5' => base64_encode(md5(stream_get_contents($feedHandle), true)),
        );
        
        rewind($feedHandle);
        $request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
        
        try 
        {
            $response = $service->submitFeed($request);
            
            $order_update_date = date('Y-m-d H:i:s a', time());
            $execute_db = $this->Database->prepare("
                UPDATE " . $this->config->database . ".orders 
                SET order_update = ?
                WHERE order_id in (". implode(',', $order_ids) . ")
                ")->execute($order_update_date);
            
            echo ("Service Response\n");
            echo ("=============================================================================\n");

            echo("        SubmitFeedResponse\n");
            if ($response->isSetSubmitFeedResult()) 
            {
                echo("            SubmitFeedResult\n");
                $submitFeedResult = $response->getSubmitFeedResult();
                if ($submitFeedResult->isSetFeedSubmissionInfo()) 
                { 
                    echo("                FeedSubmissionInfo\n");
                    $feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
                    if ($feedSubmissionInfo->isSetFeedSubmissionId()) 
                    {
                        echo("                    FeedSubmissionId\n");
                        echo("                        " . $feedSubmissionInfo->getFeedSubmissionId() . "\n");
                    }
                    if ($feedSubmissionInfo->isSetFeedType()) 
                    {
                        echo("                    FeedType\n");
                        echo("                        " . $feedSubmissionInfo->getFeedType() . "\n");
                    }
                    if ($feedSubmissionInfo->isSetSubmittedDate()) 
                    {
                        echo("                    SubmittedDate\n");
                        echo("                        " . $feedSubmissionInfo->getSubmittedDate()->format(DATE_FORMAT) . "\n");
                    }
                    if ($feedSubmissionInfo->isSetFeedProcessingStatus()) 
                    {
                        echo("                    FeedProcessingStatus\n");
                        echo("                        " . $feedSubmissionInfo->getFeedProcessingStatus() . "\n");
                    }
                    if ($feedSubmissionInfo->isSetStartedProcessingDate()) 
                    {
                        echo("                    StartedProcessingDate\n");
                        echo("                        " . $feedSubmissionInfo->getStartedProcessingDate()->format(DATE_FORMAT) . "\n");
                    }
                    if ($feedSubmissionInfo->isSetCompletedProcessingDate()) 
                    {
                        echo("                    CompletedProcessingDate\n");
                        echo("                        " . $feedSubmissionInfo->getCompletedProcessingDate()->format(DATE_FORMAT) . "\n");
                    }
                }
            }
                
            if ($response->isSetResponseMetadata()) 
            { 
                echo("            ResponseMetadata\n");
                $responseMetadata = $response->getResponseMetadata();
                if ($responseMetadata->isSetRequestId()) 
                {
                    echo("                RequestId\n");
                    echo("                    " . $responseMetadata->getRequestId() . "\n");
                }
            }

            echo("            ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
        }
        catch (MarketplaceWebService_Exception $ex)
        {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
        
        fclose($feedHandle);
        
	}
}

