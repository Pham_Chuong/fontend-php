<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnColor extends Controller
{
    public $id;
    public $name;
    public $name_en;
    public $value;

    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT *
	            FROM " . $this->config->database . ".colors
	            WHERE colors.color_id = ?
	        ")->execute($id);
	        
	    while($objDb->next())
	    {
	        $this->id        = $objDb->color_id;
	        $this->name      = $objDb->color_name;
	        $this->name_en   = $objDb->color_name_en;
	        $this->value     = $objDb->color_value;
	    }
	}
}

class PnCoverColor extends Controller
{
    public $id;
    public $name;
    public $material;
    public $product_code;
    public $hexcolor;
    public $include_in_random;
    public $name_en;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
	            SELECT *
	            FROM " . $this->config->database . ".covercolors
	            WHERE covercolors.covercolor_id = ?
	        ")->execute($id);
	        
	    while($objDb->next())
	    {
	        $this->id                   = $objDb->covercolor_id;
	        $this->name                 = $objDb->covercolor_name;
	        $this->material             = $objDb->covercolor_material;
	        $this->product_code         = $objDb->covercolor_product_code;
	        $this->hexcolor             = $objDb->covercolor_hexcolor;
	        $this->include_in_random    = $objDb->covercolor_include_in_random;
	        $this->name_en              = $objDb->covercolor_name_en;
	    }
	}
	
	public function getThumbnail()
	{
	    $image_file_name    = $this->product_code . "-thumb.png";
	    $thumb_url 			= "/thumbs/";
		$thumb_path			= implode("/", str_split(substr(str_replace(".", "", $image_file_name), 0, 3)));
		
		$thumbnail_tag		= "original";
		
		$thumbnail_name 	= $thumbnail_tag . "-" . md5($this->config->site_secret . $thumbnail_tag . $image_file_name) . "-" .$image_file_name;
		$thumb_url	   	   .= $thumb_path . "/" .$thumbnail_name;
		
		return sprintf($this->config->image_root . "%s", $thumb_url);
	}
	
}

?>