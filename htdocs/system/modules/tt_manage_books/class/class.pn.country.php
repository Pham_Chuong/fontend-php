<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnCountry extends Controller
{
    public $id;
    public $name;
    public $code;
    public $in_eu;
    public $position;
    public $active;
    public $delivery_days;
    public $express;
    public $estimated_days;
    public $verhiclecode;
    public $code3;
    public $codenum;
    public $taxed_locally_since;
    public $name_en;
    protected $config;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
    public function initWithId($id)
    {
        $objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".countries
                WHERE country_id = ?
            ")->execute($id);
            
        while($objDb->next())
        {
            $this->id                   = $objDb->country_id;
            $this->name                 = $objDb->country_name;
            $this->code                 = $objDb->country_code;
            $this->in_eu                = $objDb->country_in_eu;
            $this->position             = $objDb->country_position;
            $this->active               = $objDb->country_active;
            $this->delivery_days        = $objDb->country_delivery_days;
            $this->express              = $objDb->country_express;
            $this->estimated_days       = $objDb->country_estimated_days;
            $this->verhiclecode         = $objDb->country_verhiclecode;
            $this->code3                = $objDb->country_code3;
            $this->codenum              = $objDb->country_codenum;
            $this->taxed_locally_since  = $objDb->country_taxed_locally_since;
            $this->name_en              = $objDb->country_name_en;
        }
    }
}
