<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnRef extends Controller
{
    public $id;
    public $reftype_id;
    public $tag;
    public $partner;
    public $mail;
    public $url;
    public $ttl;
    public $clearafter;
    public $mailafter;
    public $affiliate_id;
    public $affiliate_editlink;
    
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function initWithId($id)
	{
	    $objDb = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".refs
				WHERE ref_id = ?
			")->execute($id);
			
        while($objDb->next())
        {
            $this->mapDbToObj($objDb);
        }
	}
	
	public function initWithTag($tag)
	{
	    $objDb = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".refs
				WHERE ref_tag LIKE ?
			")->execute($tag);
			
        while($objDb->next())
        {
            $this->mapDbToObj($objDb);
        }
	}
	
	public function initWithCookie()
	{
	    if ($_COOKIE['referrer'])
	    {
	        $tag = $_COOKIE['referrer'];
	        
	        $this->initWithTag($tag);
	    }
	}
	
	private function mapDbToObj($objDb)
	{
	    $this->id           = $objDb->ref_id;
        $this->reftype_id   = $objDb->reftype_id;
        $this->tag          = $objDb->ref_tag;
        $this->partner      = $objDb->ref_partner;
        $this->mail         = $objDb->ref_mail;
        $this->url          = $objDb->ref_url;
        $this->ttl          = $objDb->ref_ttl;
        $this->clearafter   = $objDb->ref_clearafter;
        $this->mailafter    = $objDb->ref_mailafter;
        $this->affiliate_id = $objDb->ref_affiliate_id;
        $this->affiliate_editlink = $objDb->ref_affiliate_editlink;
	}
	
}
