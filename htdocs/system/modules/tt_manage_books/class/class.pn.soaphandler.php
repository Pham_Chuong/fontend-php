<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class NotificationRequest 
{
    public $notificationItems; 
    public $live; 
}

class NotificationRequestItem 
{
    public $amount;
    public $eventCode;
    public $eventDate;
    public $merchantAccountCode;
    public $merchantReference;
    public $originalReference;
    public $pspReference;
    public $reason;
    public $success;
    public $paymentMethod;
    public $operations;
    public $additionalData;
}

class Amount 
{
    public $currency;
    public $value;
}

class PnSoapHandler
{
    protected $logdir = "/var/www/Live_Web_Personalnovel/logs/";
    
	function __construct()
	{
		$this->config = new PnConfig();
	}
	
	public function sendNotification($request)
	{
	    $fp = fopen($this->logdir . 'adyen_' . date('Y-m') . '.log', 'a');
        fprintf($fp, '%s', $request);
		
		if (is_array($request->notification->notificationItems->NotificationRequestItem)) 
		{
			foreach( $request->notification->notificationItems->NotificationRequestItem as $item)
			{
				$orderId = $item->merchantReference;
				if($this->config->startsWith($orderId, "Order"))
				{
					$orderId = str_replace("Order", "", $orderId);
					$order = new PnOrder();
					$order->initWithId($orderId);
					if($order->id != "" && $order->id > 0)
					{
						if($item->success)
					    {
					        //save to database
                            $this->orderPaidSuccess($item);
                            if(!$order->paid || $order->paid == "")
                            {
                                $payment_amount = floatVal($item->amount->value) / 100;
                                $total_price = floatVal($order->getTotalPrice());
                                
                                if(($payment_amount + 1.01 < $order_price) || ($payment_amount - 1.02 > $order_price))
                                {
                                    //not balance
                                    $warning_email   = "technik@personalnovel.de";
                                    $warning_subject = "Adyen: Nicht ausgeglichene - Bestellung " . $order->number;
                                    $warning_content = sprintf("<h1>Bestellung %s</h1><div>Adyen received : %s <br />But the order's price is : %s</div>"
                                                            , $order->number, $payment_amount, $total_price);
                                    
                                    //$this->sendMailWarning($warning_email, $warning_subject, $warning_content);
                                    
                                    $order->edit(
                                        array(
                                            "order_paid" => date('Y/m/d H:i:s a', strtotime($item->eventDate)),
                                            "order_ok2go" => date('Y/m/d H:i:s a', time()),
                                            "order_blob"  => date('Y-m-d', time()) . $order->blob . ": ok2go gesetzt: Automatisch\n"
                                            )
                                    );
                                }
                                else
                                {
                                    $order->edit(
                                        array(
                                            "order_paid" => date('Y/m/d H:i:s a', strtotime($item->eventDate)),
                                            "order_ok2go" => date('Y/m/d H:i:s a', time()),
                                            "order_blob"  => date('Y-m-d', time()) . $order->blob . ": ok2go gesetzt: Automatisch\n"
                                            )
                                    );
                                }
                                
                                $order->pay(floatVal($item->amount->value) / 100);
                                
                                $pattern = "/freqmail: payment success - " . $item->pspReference ."/";
                                if(!preg_match($pattern, $order->blob))
                                    $this->sendMailConfirmBridge($orderId, $item->pspReference);
                            }
					    }
						else
						{
						    $this->orderPaidFail($item);
						    
						    $pattern = "/freqmail: changepaymentmethod - " . $item->pspReference . "/";
						    if(!preg_match($pattern, $order->blob))
						        $this->sendMailRepayBridge($orderId, $item->pspReference);
						}
					}
					else
					{
						//write error
						$this->orderNotFound($item);
					}
				}
				else
				{
					//write error
					$this->invalidOrder($item);
				}
				
				$this->storeItem($item, $fp);
			}
		}
		else
		{
			$item = $request->notification->notificationItems->NotificationRequestItem;
			$orderId = $item->merchantReference;
            if($this->config->startsWith($orderId, "Order"))
            {
                $orderId = str_replace("Order", "", $orderId);
                $order = new PnOrder();
                $order->initWithId($orderId);
                if($order->id != "" && $order->id > 0)
                {
                    if($item->success)
                    {
                        //save to database
                        $this->orderPaidSuccess($item);
                        if(!$order->paid || $order->paid == "")
                        {
                            $payment_amount = floatVal($item->amount->value) / 100;
                            $total_price = floatVal($order->getTotalPrice());
                            
                            if(($payment_amount + 1.01 < $order_price) || ($payment_amount - 1.02 > $order_price))
                            {
                                //not balance
                                $warning_email   = "technik@personalnovel.de";
                                $warning_subject = "Adyen: Nicht ausgeglichene - Bestellung " . $order->number;
                                $warning_content = sprintf("<h1>Bestellung %s</h1><div>Adyen received : %s <br />But the order's price is : %s</div>"
                                                        , $order->number, $payment_amount, $total_price);
                                
                                //$this->sendMailWarning($warning_email, $warning_subject, $warning_content);
                                
                                $order->edit(
                                    array(
                                        "order_paid" => date('Y/m/d H:i:s a', strtotime($item->eventDate)),
                                        "order_ok2go" => date('Y/m/d H:i:s a', time()),
                                        "order_blob"  => date('Y-m-d', time()) . $order->blob . ": ok2go gesetzt: Automatisch\n"
                                        )
                                );
                            }
                            else
                            {
                                $order->edit(
                                    array(
                                        "order_paid" => date('Y/m/d H:i:s a', strtotime($item->eventDate)),
                                        "order_ok2go" => date('Y/m/d H:i:s a', time()),
                                        "order_blob"  => date('Y-m-d', time()) . $order->blob . ": ok2go gesetzt: Automatisch\n"
                                        )
                                );
                            }
                            
                            $order->pay(floatVal($item->amount->value) / 100);
                            
                            $pattern = "/fregmail: payment success - " . $item->pspReference ."/";
                            if(!preg_match($pattern, $order->blob))
                                $this->sendMailConfirmBridge($orderId, $item->pspReference);
                        }
                    }
                    else
                    {
                        $this->orderPaidFail($item);
                        
                        $pattern = "/freqmail: changepaymentmethod - " . $item->pspReference . "/";
                        if(!preg_match($pattern, $order->blob))
                            $this->sendMailRepayBridge($orderId, $item->pspReference);
                    }
                }
                else
                {
                    //write error
                    $this->orderNotFound($item);
                }
            }
            else
            {
                //write error
                $this->invalidOrder($item);
            }
            
            $this->storeItem($item, $fp);
		}
		
		return array("notificationResponse" => "[accepted]");
	}
	
	function storeItem($item, $fp) 
	{ 
	  $output = $output . "\nNotification: \n" . 
		"Amount = " . $item->amount->currency . " " . $item->amount->value . "\n" .
		"Event Code = " . $item->eventCode . "\n" .
		"Event Date = " . $item->eventDate . "\n" .
		"Merchant Account Code = " . $item->merchantAccountCode . "\n" .
		"Merchant Reference = " . $item->merchantReference . "\n" .
		"Original Reference = " . $item->originalReference . "\n" .
		"Psp Reference = " . $item->pspReference . "\n" .
		"Reason = " . $item->reason . "\n" .
		"Success = " . $item->success . "\n".
		"Payment Method = " . $item->paymentMethod . "\n\n".
		
		"Available Operations: \n";
	
		if(is_array($item->operations->string)) {
		  foreach($item->operations->string as $operation) {
			$output = $output . "\t" . $operation . "\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->operations->string . "\n" ;
		}
	
		$output = $output . "\n\nAdditional Data:\n";
		if(is_array($item->additionalData->entry)) {
		  foreach($item->additionalData->entry as $entry) {
			$output = $output . "\t" . $entry->key ."=".$entry->value."\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->additionalData->entry->key ."=".$item->additionalData->entry->value."\n" ;
		}
	
		$output = $output . "\n";
	
		fprintf($fp, '%s', $output);
	}
	
	function invalidOrder($item)
	{
	    $fp = fopen($this->logdir . 'order_invalid.txt', 'a');
	    
	    $output = $output . "\nNotification: \n" . 
		"Amount = " . $item->amount->currency . " " . $item->amount->value . "\n" .
		"Event Code = " . $item->eventCode . "\n" .
		"Event Date = " . $item->eventDate . "\n" .
		"Merchant Account Code = " . $item->merchantAccountCode . "\n" .
		"Merchant Reference = " . $item->merchantReference . "\n" .
		"Original Reference = " . $item->originalReference . "\n" .
		"Psp Reference = " . $item->pspReference . "\n" .
		"Reason = " . $item->reason . "\n" .
		"Success = " . $item->success . "\n".
		"Payment Method = " . $item->paymentMethod . "\n\n".
		
		"Available Operations: \n";
	
		if(is_array($item->operations->string)) {
		  foreach($item->operations->string as $operation) {
			$output = $output . "\t" . $operation . "\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->operations->string . "\n" ;
		}
	
		$output = $output . "\n\nAdditional Data:\n";
		if(is_array($item->additionalData->entry)) {
		  foreach($item->additionalData->entry as $entry) {
			$output = $output . "\t" . $entry->key ."=".$entry->value."\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->additionalData->entry->key ."=".$item->additionalData->entry->value."\n" ;
		}
	
		$output = $output . "\n";
	
		fprintf($fp, '%s', $output);
	}
	
	function orderNotFound($item)
	{
	    $fp = fopen($this->logdir . 'order_notfound.txt', 'a');
	    
	    $output = $output . "\nNotification: \n" . 
		"Amount = " . $item->amount->currency . " " . $item->amount->value . "\n" .
		"Event Code = " . $item->eventCode . "\n" .
		"Event Date = " . $item->eventDate . "\n" .
		"Merchant Account Code = " . $item->merchantAccountCode . "\n" .
		"Merchant Reference = " . $item->merchantReference . "\n" .
		"Original Reference = " . $item->originalReference . "\n" .
		"Psp Reference = " . $item->pspReference . "\n" .
		"Reason = " . $item->reason . "\n" .
		"Success = " . $item->success . "\n".
		"Payment Method = " . $item->paymentMethod . "\n\n".
		
		"Available Operations: \n";
	
		if(is_array($item->operations->string)) {
		  foreach($item->operations->string as $operation) {
			$output = $output . "\t" . $operation . "\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->operations->string . "\n" ;
		}
	
		$output = $output . "\n\nAdditional Data:\n";
		if(is_array($item->additionalData->entry)) {
		  foreach($item->additionalData->entry as $entry) {
			$output = $output . "\t" . $entry->key ."=".$entry->value."\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->additionalData->entry->key ."=".$item->additionalData->entry->value."\n" ;
		}
	
		$output = $output . "\n";
	
		fprintf($fp, '%s', $output);
	}
	
	function orderPaidSuccess($item)
	{
	    $fp = fopen($this->logdir . 'order_paid_success.txt', 'a');
	    
	    $output = $output . "\nNotification: \n" . 
		"Amount = " . $item->amount->currency . " " . $item->amount->value . "\n" .
		"Event Code = " . $item->eventCode . "\n" .
		"Event Date = " . $item->eventDate . "\n" .
		"Merchant Account Code = " . $item->merchantAccountCode . "\n" .
		"Merchant Reference = " . $item->merchantReference . "\n" .
		"Original Reference = " . $item->originalReference . "\n" .
		"Psp Reference = " . $item->pspReference . "\n" .
		"Reason = " . $item->reason . "\n" .
		"Success = " . $item->success . "\n".
		"Payment Method = " . $item->paymentMethod . "\n\n".
		
		"Available Operations: \n";
	
		if(is_array($item->operations->string)) {
		  foreach($item->operations->string as $operation) {
			$output = $output . "\t" . $operation . "\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->operations->string . "\n" ;
		}
	
		$output = $output . "\n\nAdditional Data:\n";
		if(is_array($item->additionalData->entry)) {
		  foreach($item->additionalData->entry as $entry) {
			$output = $output . "\t" . $entry->key ."=".$entry->value."\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->additionalData->entry->key ."=".$item->additionalData->entry->value."\n" ;
		}
	
		$output = $output . "\n";
	
		fprintf($fp, '%s', $output);
	}
	
	function orderPaidFail($item)
	{
	    $fp = fopen($this->logdir . 'order_paid_fail.txt', 'a');
	    
	    $output = $output . "\nNotification: \n" . 
		"Amount = " . $item->amount->currency . " " . $item->amount->value . "\n" .
		"Event Code = " . $item->eventCode . "\n" .
		"Event Date = " . $item->eventDate . "\n" .
		"Merchant Account Code = " . $item->merchantAccountCode . "\n" .
		"Merchant Reference = " . $item->merchantReference . "\n" .
		"Original Reference = " . $item->originalReference . "\n" .
		"Psp Reference = " . $item->pspReference . "\n" .
		"Reason = " . $item->reason . "\n" .
		"Success = " . $item->success . "\n".
		"Payment Method = " . $item->paymentMethod . "\n\n".
		
		"Available Operations: \n";
	
		if(is_array($item->operations->string)) {
		  foreach($item->operations->string as $operation) {
			$output = $output . "\t" . $operation . "\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->operations->string . "\n" ;
		}
	
		$output = $output . "\n\nAdditional Data:\n";
		if(is_array($item->additionalData->entry)) {
		  foreach($item->additionalData->entry as $entry) {
			$output = $output . "\t" . $entry->key ."=".$entry->value."\n" ;
		  }
		} else {
		  $output = $output . "\t" . $item->additionalData->entry->key ."=".$item->additionalData->entry->value."\n" ;
		}
	
		$output = $output . "\n";
	
		fprintf($fp, '%s', $output);
	}
	
	public function sendMailRepayBridge($orderId, $psp = "")
	{
		$order    = new PnOrder();
	    $order->initWithId($orderId);
	    if($order->locale_id==2)
	    {
	    	$this->sendMailRepayUK($orderId, $psp);
	    }
	    else
	    {
	    	$this->sendMailRepay($orderId, $psp);
	    }
	}
	
	public function sendMailConfirmBridge($orderId, $psp = "")
	{
		$order    = new PnOrder();
	    $order->initWithId($orderId);
	    if($order->locale_id==2)
	    {
	    	$this->sendMailConfirmUK($orderId, $psp);
	    }
	    else
	    {
	    	$this->sendMailConfirm($orderId, $psp);
	    }
	}
	
	function sendMailRepay($orderId, $psp = "")
	{
	    $order    = new PnOrder();
	    $order->initWithId($orderId);
	    $order_cid = Verhoeff::generate($order->number);
	    
	    $customer = new PnCustomer();
        $customer->initWithId($order->customer_id);
        
	    $email        = $customer->email;
	    $subject      = "Zahlung fehlgeschlagen, " . $order_cid;
	    $objTemplate  = new BackendTemplate("mod_mail_payment_fail");
	    
	    $objTemplate->anrede      = $customer->anrede;
	    $objTemplate->lastName    = $customer->lastName;
	    $objTemplate->orderDate   = $order->updated;
	    $objTemplate->paymentId   = $order->payment_method_id;
	    
        $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
        $token = generateToken($orderId);
        $url            = sprintf("http://www.personalnovel.de%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
        $objTemplate->linkPayment = $url;
        
        $myorder_page = $GLOBALS['TL_CONFIG']['myorder_page'] ? $GLOBALS['TL_CONFIG']['myorder_page'] : "/myorder";
        $url_mypage   = sprintf("http://www.personalnovel.de%s?id=%s", $myorder_page, $order->id);
        $objTemplate->linkMyPage = $url_mypage;
        
	    $this->sendMailToCustomer($email, $subject, $objTemplate);
	    
	    $orderblob = $order->blob . "freqmail: changepaymentmethod - " . $psp . "\n";
	    $order->edit(array("order_blob" => $orderblob));
	}
	
	function sendMailConfirm($orderId, $psp = "")
	{
	    $order = new PnOrder();
	    $order->initWithId($orderId);
	    
	    $customer = new PnCustomer();
        $customer->initWithId($order->customer_id);
        
        $address = new PnAddress();
        $address->initWithId($order->shipping_address_id);
	    
        $order_cid = Verhoeff::generate($order->number);
        
	    $objTemplate = new BackendTemplate("mod_mail_confirmorder");
        $objTemplate->anrede          = $customer->anrede;
        $objTemplate->orderNumber     = $order_cid;
        $objTemplate->firstName       = $address->firstName;
        $objTemplate->lastName        = $address->lastName;
        
        $billingAddress               = new PnAddress();
        $billingAddress->initWithId($order->billing_address_id);
        $objTemplate->anrede          = $billingAddress->anrede ? $billingAddress->anrede : $customer->anrede;
        $objTemplate->billingFName    = $billingAddress->firstName;
        $objTemplate->billingLName    = $billingAddress->lastName;
        
        $orderDate                    = strtotime($order->date);
        $newLocale                    = setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
        $objTemplate->orderDate       = utf8_encode(strftime('%d. %B %Y um %H:%M Uhr.', $orderDate));
        $objTemplate->linkMyPage      = "http://" . $_SERVER['HTTP_HOST'] . "/myorder?id=" . $order->id;
        $objTemplate->paymentMethod   = $GLOBALS['TL_CONFIG']['payment_methods'][1][$order->payment_method_id]['html'];
        $objTemplate->paymentMethodId = $order->payment_method_id;
        $objTemplate->address         = $address->address;
        $objTemplate->company         = $address->company;
        $objTemplate->city            = $address->city;
        $objTemplate->zip             = $address->zip;
        
        $shipping_country             = new PnCountry();
        $shipping_country->initWithId($address->country_id);
        $objTemplate->country         = $shipping_country->name;
        $objTemplate->estimated_days  = $shipping_country->estimated_days;
        
        $postens = $order->getPosten();
       
        $objTemplate->postens = $postens;
        $objTemplate->customerEmail = $customer->email;
        $objTemplate->orderPrice = $order->getTotalPrice();
        $objTemplate->orderTax   = $order->getTaxPrice();
        $objTemplate->shippingId = $order->shipping_id;
        $objTemplate->order      = $order;
        
        $this->sendMailToCustomer($customer->email, sprintf("Ihre Bestellung, %s", $order_cid), $objTemplate);
        
        $orderblob = $order->blob . "freqmail: payment success - " . $psp . "\n";
        $order->edit(array("order_blob" => $orderblob));
	}
	
	function sendMailRepayUK($orderId, $psp = "")
	{
	    $order    = new PnOrder();
	    $order->initWithId($orderId);
	    $order_cid = Verhoeff::generate($order->number);
	    
	    $customer = new PnCustomer();
        $customer->initWithId($order->customer_id);
        
	    $email        = $customer->email;
	    $subject      = "Payment failed, " . $order_cid;
	    $objTemplate  = new BackendTemplate("mod_mail_payment_fail_uk");
	    
	    $objTemplate->anrede      = $customer->anrede=="Frau"?"Mrs.":"Mr.";
	    $objTemplate->lastName    = $customer->lastName;
	    $objTemplate->orderDate   = $order->updated;
	    $objTemplate->paymentId   = $order->payment_method_id;
	    
        $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
        $token = generateToken($orderId);
        $url            = sprintf("http://personalnovel.co.uk%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
        $objTemplate->linkPayment = $url;
        
        $myorder_page = $GLOBALS['TL_CONFIG']['myorder_page'] ? $GLOBALS['TL_CONFIG']['myorder_page'] : "/myorder";
        $url_mypage   = sprintf("http://personalnovel.co.uk%s?id=%s", $myorder_page, $order->id);
        $objTemplate->linkMyPage = $url_mypage;
        
	    $this->sendMailToCustomer($email, $subject, $objTemplate);
	    
	    $orderblob = $order->blob . "freqmail: changepaymentmethod - " . $psp . "\n";
	    $order->edit(array("order_blob" => $orderblob));
	}
	
	function sendMailConfirmUK($orderId, $psp = "")
	{
	    $order = new PnOrder();
	    $order->initWithId($orderId);
	    
	    $customer = new PnCustomer();
        $customer->initWithId($order->customer_id);
        
        $address = new PnAddress();
        $address->initWithId($order->shipping_address_id);
	    
        $order_cid = Verhoeff::generate($order->number);
        
	    $objTemplate = new BackendTemplate("mod_mail_confirmorder_uk");
        $objTemplate->anrede          = $customer->anrede;
        $objTemplate->orderNumber     = $order_cid;
        $objTemplate->firstName       = $address->firstName;
        $objTemplate->lastName        = $address->lastName;
        
        $billingAddress               = new PnAddress();
        $billingAddress->initWithId($order->billing_address_id);
        $objTemplate->anrede          = $billingAddress->anrede ? $billingAddress->anrede : $customer->anrede;
        $objTemplate->billingFName    = $billingAddress->firstName;
        $objTemplate->billingLName    = $billingAddress->lastName;
        
        $orderDate                    = strtotime($order->date);
        $newLocale                    = setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
        $objTemplate->orderDate       = utf8_encode(strftime('%d %B %Y.', $orderDate));
        $objTemplate->linkMyPage      = "http://personalnovel.co.uk/myorder?id=" . $order->id;
        $objTemplate->paymentMethod   = $GLOBALS['TL_CONFIG']['payment_methods'][2][$order->payment_method_id]['html'];
        $objTemplate->paymentMethodId = $order->payment_method_id;
        $objTemplate->address         = $address->address;
        $objTemplate->company         = $address->company;
        $objTemplate->city            = $address->city;
        $objTemplate->zip             = $address->zip;
        
        $shipping_country             = new PnCountry();
        $shipping_country->initWithId($address->country_id);
        $objTemplate->country         = $shipping_country->name_en;
        $objTemplate->estimated_days  = $shipping_country->estimated_days;
        
        $postens = $order->getPosten();
       
        $objTemplate->postens = $postens;
        $objTemplate->customerEmail = $customer->email;
        $objTemplate->orderPrice = $order->getTotalPrice();
        $objTemplate->orderTax   = $order->getTaxPrice();
        $objTemplate->shippingId = $order->shipping_id;
        $objTemplate->order      = $order;
        
        $this->sendMailToCustomer($customer->email, sprintf("Your Order, %s", $order_cid), $objTemplate);
        
        $orderblob = $order->blob . "freqmail: payment success - " . $psp . "\n";
        $order->edit(array("order_blob" => $orderblob));
	}
	
	function sendMailToCustomer($email, $subject, $objTemplate)
	{
	    $objEmail = new Email();
	    $objEmail->from = $GLOBALS['TL_CONFIG']['adminEmail'];
		$objEmail->subject = $subject;
		$objEmail->html = $objTemplate->parse();
		$objEmail->sendTo($email);
	}
	
	function sendMailWarning($email, $subject, $content)
	{
	    $objEmail = new Email();
	    $objEmail->from = $GLOBALS['TL_CONFIG']['adminEmail'];
		$objEmail->subject = $subject;
		$objEmail->html = $content;
		$objEmail->sendCc("dao@tt-tech.de");
		$objEmail->sendTo($email);
	}
}

?>
