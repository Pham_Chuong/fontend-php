<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/*
 * Pn Classes
 */
require_once("class/class.pn.php");
require_once("class/class.verhoeff.php");

class ModuleMyOrder extends Module
{
	protected $strTemplate 		= 'mod_my_order';
	
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### PREVIEW MY ORDER ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
		    $GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/checkout.css|screen';
		    $GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.lightbox-0.5.css|screen';
		    
		    $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery-ui-1.8.16.custom.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/checkout.js';
			
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.lightbox-0.5.pack.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
	    $orderid = $_GET["id"];
	    if($orderid)
	    {
	        $order = new PnOrder();
	        $order->initWithId($orderid);
	        if($order->id != "" && $order->id > 0)
	        {
	            $this->Template->order      = $order;
	            $this->Template->order_cid  = Verhoeff::generate($order->number);
	            
	            $orderDate                    = strtotime($order->date);
                $newLocale                    = setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
                $this->Template->order_date   = Encoding::toUTF8(strftime('%d. %B %Y um %H:%M Uhr.', $orderDate));
	            
	            $shipping_address = new PnAddress();
	            $shipping_address->initWithId($order->shipping_address_id);
	            $this->Template->shipping_address = $shipping_address;
	            
	            if($order->shipping_address_id != $order->billing_address_id)
	            {
	                $billing_address = new PnAddress();
	                $billing_address->initWithId($order->billing_address_id);
	                $this->Template->billing_address = $billing_address;
	            }
	            
	            $customer = new PnCustomer();
	            $customer->initWithId($order->customer_id);
	            $this->Template->customer = $customer;
	          
	             //check order email
	            $email_check = strtolower($customer->email);
	            if(isset($_POST['check_button']) and isset($_POST['myemail'])){
	            	//check type email 
	            	$email_input = strtolower(trim($_POST['myemail']));
	            	if($email_input==  $email_check){
	            		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
	            		setcookie('myorder', $email_check , time() + 3600, "/",$domain_cookie);
	            		$this->Template->checkEmail = false;
	            	}else{
	            		$this->Template->checkEmail = true;
	            	   	$this->Template->checkError = " Ihre Email ist falsch!";
	            	}
	            }elseif(isset($_COOKIE['myorder']) and $_COOKIE['myorder']==$email_check){
	            	echo "";
	            	$this->Template->checkEmail = false;
	            }
	            else{
	            	  $this->Template->checkEmail = true;
	            	   $this->Template->checkError = "";
	            }

	            //End check order email

	            if($order->paid)
	            {
	                $paidDate                     = strtotime($order->paid);
	                $newLocale                    = setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
	                $this->Template->payment_date = Encoding::toUTF8(strftime('%d. %B %Y um %H:%M Uhr.', $paidDate));
	            }
	            else
	            {
	                $this->Template->payment_date = "Wir warten auf den Zahlungseingang.";
	            }
	            
	            $this->Template->wineItems = $order->getWineItems();
	            $this->Template->bookItems = $order->getBookItems();
	            $this->Template->orderPaid = $order->paid;
	            $this->Template->orderPrinted = $order->print;
	            $this->Template->orderOk2Go = $order->ok2go;
	            $this->Template->orderId   = $order->id;
	            $this->Template->allowEdit = false;
	            $this->Template->free_coupon = $order->getFreeCoupon();
	            
	            if($_GET["number"])
	            {
	                if($_GET["number"] == $order->number)
	                {
	                    
	                }
	            }
	            
	            if($order->tracking)
	                $this->Template->trackingUrl = $order->getTrackingUrl();
	        }
	        else
	        {
	            gotoErrorPage();
	        }
	    }
	    else
	    {
	        gotoErrorPage();
	    }
	}
}

?>
