
-- 
-- Table `tl_module`
-- 

CREATE TABLE `tl_module` (
  `pn_type` int(10) NOT NULL default '0',
  `pn_select_image` blob NULL,
  `pn_booklang` varchar(2) NOT NULL default 'de',
  `pn_image_height` int(10) NOT NULL default '110',
  `pn_image_shadow` int(10) NOT NULL default '5',
  `pn_template` varchar(32) NOT NULL default '',
  `pn_template_wine` varchar(32) NOT NULL default '',
  `pn_genreid` int(10) NOT NULL default '1',
  `pn_booktitle` varchar(32) NOT NULL default '',
  `pn_sitepath` varchar(50) NOT NULL default '',
  `pn_page_cur` varchar(50) NULL default '',
  `pn_ebook` int(1) NOT NULL default '0',
  `step1_teaser` text NULL,
  `pn_filter_module` blob NULL,
  `step2_teaser` text NULL,
  `step3_teaser` text NULL,
  `step4_teaser` text NULL,
  `step5_teaser` text NULL,
  `step6_teaser` text NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Table `tl_content`
--

CREATE TABLE `tl_content` (
  `pn_module` int(10) unsigned NOT NULL default '0',
  `pn_filter` blob NULL,
  `pn_tab` blob NULL,
  `pn_genre` blob NULL,
  `pn_headline` text NULL,
  `pn_link_logo` text NULL,
  `pn_view_logo` blob NULL,
  `pn_sitepath` varchar(50) NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `tl_google_adwords_tracking` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tstamp` int(10) unsigned NOT NULL default '0',
  `clid` varchar(255) NOT NULL default '',
  `utmccn` varchar(255) NOT NULL default '',
  `utmcmd` varchar(255) NOT NULL default '',
  `utmctr` varchar(255) NOT NULL default '',
  `visit_page` varchar(255) NOT NULL default '',
  `ip_address` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`id`),
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
