<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

class PersonalnovelHooks extends Frontend
{
    public function addBookIdToUrl($arrFragments)
	{
	    $match = array();
	    if(preg_match("/^(.*)\/([0-9]+)\/[^\/]+$/", $arrFragments[0], $match)){
            return array($match[1], "items", $match[2]);
	    }
	    return $arrFragments;
	}
}

?>
