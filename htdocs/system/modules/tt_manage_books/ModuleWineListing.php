<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');
/*
 * Comment: Wine Listing
 * Logic:  Hien thi danh sach ruou 
 * Ten Task:  Menu Wine
 */

require_once("class/class.pn.php");

class ModuleWineListing extends Module
{
	protected $strTemplate 		= 'mod_wine_listing';
	public $arrFilters 			= array();
	protected $arrWines			= array();
	protected $arrTagCategories	= array();
	public $sitepath = "";
	public $limitCopyImg = 12;
	public $flaLimit = 0;
	
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### WINES LIST ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
			//$this->strTemplate = $this->pn_template;
			
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/filter_panel.css|screen';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/filter_panel.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		$config = new PnConfig();
		
		$taglist = $this->Database->prepare("select * from " . $config->database . ".tag_categories")->execute();
		$arrTaglist = array();
		while($taglist->next())
		{
			$arrTaglist[] = $taglist->category_name;
			$arrTaglist[] = $taglist->category_name_en;
		}
		
		$filterTaglist = "";
		
		foreach($_GET as $k=>$v)
		{
			$k = str_replace("_", " ", $k);
			if(in_array($k, $arrTaglist))
			{
				$this->arrFilters[$k] = $v;
			}
		}
		
		$str = "";
		$str_end = "";
		$i = 0;
		foreach($this->arrFilters as $k => $v)
		{
			$i++;
			$tag_category = $this->Database->prepare("select category_id from " . $config->database . ".tag_categories where category_name like ? or category_name_en like ?")->execute($k, $k);
			$cate_id = 0;
			$tag_id  = 0;
			while($tag_category->next())
			{
				$cate_id = $tag_category->category_id;
			}
			$tag_list = $this->Database->prepare("select tag_id from " . $config->database . ".buch_tags2 where category_id=? and (tag_value = ? or tag_value_en = ?)")->execute($cate_id, $v, $v);
			while($tag_list->next())
			{
				$tag_id		= $tag_list->tag_id;
			}
				
			$str .= sprintf(" (select artikel_id from " . $config->database . ".artikel_buchtags where tag_id = $tag_id ");
			if($i < count($this->arrFilters))
			{
				$str .= " and artikel_id in ";
			}
			$str_end .= ")";
		}

		if(count($this->arrFilters) > 0)
			$filterTaglist = " and f.`artikel_id` in " . $str . $str_end;
		
		$articlesTotal = $this->Database->prepare(
								  "select * from " . $config->database . ".finanz_artikel as f "
								. "left outer join " . $config->database . ".artikel_klassen as a on f.`class_id` = a.`class_id` "
								. "where ISNULL(f.`buch_id`) "
								. "and f.`artikel_active` = 1 "
								. "and f.`class_id` = 14 "
								. $filterTaglist
								)->execute();
		
		$offset = 0;
		$limit = null;
		$total = $articlesTotal->numRows;

		if ($this->perPage > 0)
		{
			$page = $this->Input->get('page') ? $this->Input->get('page') : 1;
			if ($page > ($total/$this->perPage))
			{
				$page = ceil($total/$this->perPage);
			}
			$limit = $this->perPage;
			$offset = ($page - 1) * $this->perPage;
			$objPagination = new Pagination($total, $this->perPage);
			$this->Template->pagination = $objPagination->generate();
		}
		
		$artiles = $this->Database->prepare(
								  "select * from " . $config->database . ".finanz_artikel as f "
								. "left outer join " . $config->database . ".artikel_klassen as a on f.`class_id` = a.`class_id` "
								. "left outer join " . $config->database . ".assets on f.`image_asset_id` = assets.`asset_id` "
								. "left outer join " . $config->database . ".prices p on f.`artikel_id` = p.`artikel_id` "
								. "where ISNULL(f.`buch_id`) "
								. "and f.`artikel_active` = 1 "
								. "and f.`class_id` = 14 "
								. "and (p.`price_validuntil` > now() or ISNULL(p.`price_validuntil`)) "
								. $filterTaglist
								);
		
		if (isset($limit))
		{
			$artiles->limit($limit, $offset);
		}
		elseif ($total > 0)
		{
			$artiles->limit($total, $offset);
		}
		
		$artiles = $artiles->execute();
		$arrArtikelIds = array(0);
		
		
		while($artiles->next())
		{
			$objWinzer = $this->Database->prepare("select b.`tag_value`, b.`tag_value_en` from " . $config->database . ".artikel_buchtags a inner join " . $config->database . ".buch_tags2 b on a.tag_id = b.tag_id where a.artikel_id = ? and b.`category_id` = 32")->execute($artiles->artikel_id);
			$winzer 		= "Winzer";
			while($objWinzer->next())
			{
				$winzer = $objWinzer->tag_value;
			}
			
			$thumb_url 			= "/thumbs/";
			$thumb_path			= implode("/", str_split(substr(str_replace(".", "", $artiles->asset_filename), 0, 3)));
			$thumbnail_tag		= "h160";
			if(in_array($artiles->artikel_id, array(11628, 11631, 11633)))
			{
				$thumbnail_tag		= "h150";
				$artiles->asset_filename = str_replace("png", "jpg", $artiles->asset_filename);
			}
			
			$thumbnail_name 	= $thumbnail_tag . "-" . md5($GLOBALS['TL_CONFIG']['site_secret'] . $thumbnail_tag . $artiles->asset_filename) . "-" .$artiles->asset_filename;
			$thumb_url	   	   .= $thumb_path . "/" .$thumbnail_name;
			
			$pk = $artiles->artikel_id;
			$preview_url	= $GLOBALS['TL_CONFIG']['image_root'] . $thumb_url;
			$preview_url = $this->getImageUrl($artiles->artikel_id,"list",$preview_url, standardize($artiles->artikel_name));
			
			$price_amount		= sprintf($GLOBALS['TL_LANG']['MOD']['wine_listing']['price_per_bottle'], $artiles->price_amount . " EUR ");
			if($this->currency && $this->currency == "2")
			{
				$price_amount = sprintf($GLOBALS['TL_LANG']['MOD']['wine_listing']['price_per_bottle'], $artiles->price_amount_uk . " GBP ");
			}
			
			$detail_url		= "/wine?item=" . $artiles->artikel_id;
			//print_r($this);
			if($this->jumpTo!=0){
				$detail_url="{{link_url::".$this->jumpTo."}}?item=" . $artiles->artikel_id;
			}
			
			$this->arrWines[] 	= array(
				'id'			=> $artiles->artikel_id,
				'name'			=> $artiles->artikel_name,
				'sub'			=> $winzer,
				'desc'			=> $artiles->artikel_desc,
				'preview_url' 	=> $preview_url,
				'detail_url'	=> $detail_url,
				'price'			=> $price_amount,
				'totalproduct'  => $artiles->artikel_available
			);
			
			$arrArtikelIds[] = $artiles->artikel_id;
		}
		
		/*
		 * get available tags
		 */
		$obj = $this->Database->prepare("Select * From " . $config->database . ".tag_categories where class_id=14 order by category_pos")->execute();
		$objTags = $this->Database->prepare("select * from " . $config->database . ".artikel_buchtags b inner join " . $config->database . ".buch_tags2 b2 on b.`tag_id` = b2.`tag_id` where 1 = 1 and b.`artikel_id` in (" . implode(",", $arrArtikelIds) . ") group by b.`tag_id`")->execute();
		
		$arrTagItems = array();
		while($objTags->next()) {
			$arrTagItems[] = array(
					"id"			=> $objTags->tag_id,
					"value"			=> $objTags->tag_value,
					"value_en"		=> $objTags->tag_value_en,
					"category_id"	=> $objTags->category_id
			);
		}
		
		while($obj->next()) {
			$arr				= array();
			$arr["id"]			= $obj->category_id;
			$arr["name"]		= $obj->category_name;
			$arr["name_en"]		= $obj->category_name_e;
			$arr["items"]		= array();
		
			foreach($arrTagItems as $item) {
				if($item["category_id"] == $arr["id"]) {
					$arr["items"][] = $item;
				}
			}
		
			$this->arrTagCategories[]	= $arr;
		}
		
		//sort champagner - prosecco
		
		$i=0;
		$c = count($this->arrWines);
		$tmp = array();
		 
		foreach ($this->arrWines as $wine){
		
			if(($wine['id'] == '15974' || $wine['id'] == '12030' || $wine['id'] == '11661' )){
				
				$tmp[] = $this->arrWines[$i];
				unset($this->arrWines[$i]);
			}
			$i++;
		}
		
		Sort($this->arrWines);
		
		foreach ($tmp as $wine){
			array_unshift($this->arrWines,$wine);
		}
		
		$this->Template->TagCategories = $this->arrTagCategories;
		$this->Template->wines = $this->arrWines;
	}
	
	//get thumbnail
	public function getImageUrl($id, $etx, $url_img, $title) {
		if($this->flaLimit != $this->limitCopyImg){
			
			if(!in_array($etx, array('detail','view','list'))){
				$etx = "detail";
			}
			$check = true;
			$pageURL = 'http';
			
			if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
			
			$pageURL .= "://";
			$file_image = TL_ROOT."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png";
			
			if(!is_file($file_image)){
				try{
				$file_headers = @get_headers($url_img);
				if($file_headers[0] != 'HTTP/1.1 404 Not Found' and $file_headers[0] != 'HTTP/1.1 500 INTERNAL SERVER ERROR')
					copy($url_img, TL_ROOT."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png");
					else{
						$check = false;
					}
				$this->flaLimit = $this->flaLimit + 1;
				}catch (Exception $e) {
					$check = false;
				}
			}
			if($check != false )
			$url_img = $pageURL.$_SERVER["SERVER_NAME"]."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png";
			
			return $url_img;
		}
		return $url_img;
	}
}

?>