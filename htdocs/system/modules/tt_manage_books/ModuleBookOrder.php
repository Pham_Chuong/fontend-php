<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/*
 * Pn Classes
 * Comment:Order 
 * Logic: Co 6 buoc
 * 1. Personen
 * 2. Widmung
 * 3. Einbande
 * 4. Umschlag
 * 5. Schritf / Extras
 * 6. Weitere Produkte
 * Ten Task
 * 1. Menu kinderbucher
 * 2. Menu Jugendbucher
 * 3. Menu Literaturklassiker
 * 4. Menu Geschenkbucher
 * 5. Menu Romane
 */
require_once("class/class.pn.php");

class ModuleBookOrder extends Module
{
	protected $strTemplate 		= 'mod_book_order';
	protected $config;
	public    $domain;
	public function generate()
	{
	    $this->config = new PnConfig();
	    
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### ORDER STEPS ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/order.css|screen';
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.lightbox-0.5.css|screen';
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.alerts.css|screen';
			
			if($_GET["step"] == "coverimage")
			{
			    $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery-1.9.1.min.js';
			    $GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery.Jcrop.min.js';
			}
			else
			{
                $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
                $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery-ui-1.8.16.custom.min.js';
                $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.lightbox-0.5.pack.js';
                $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.alerts.js';
                $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/order.js';
                $GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery.popupWindow.js';
                $GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery.typing-0.2.0.min.js';
                $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.preload-min.js';
			}
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
	    $upload_dir = $GLOBALS['TL_CONFIG']['customer_images_dir'] ? $GLOBALS['TL_CONFIG']['customer_images_dir'] : "/var/www/production/shared/customer-images/";
	    $upload_dir_frontend = $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] ? $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] : "/var/www/Personalnovel/htdocs/tl_files/customer-images/";
	    $customize_page = $GLOBALS['TL_CONFIG']['customize_page'] ? $GLOBALS['TL_CONFIG']['customize_page'] : "/customize";
	    $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
	    $this->domain   = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
	    
	    
	    $step = $_GET['step'] ? $_GET['step'] : $_POST['step'];
	    $step = $step ? $step : 1;
	    
	    /*
	     * goto MyOrderPage if order's done
	     */
	    if($_GET["order"])
	    {
	    	
	        //Sendmail
            if($_GET["fedit"] == 1){
	            $s = 'Date : '.date("F j, Y, g:i a") .'<br/>';
			    $s .= 'OrderID: '. $_GET["order"].'<br/><br/>';
			    $arData = array();
			    $arData['message'] = $s;
               	$this->sendMail($arData,'MyOrderPage ','MyOrder Page Edit ',$_GET["order"]);
            }
            
	        $myorder_page = $GLOBALS['TL_CONFIG']['myorder_page'] ? $GLOBALS['TL_CONFIG']['myorder_page'] : "/myorder";
	        $testorder = new PnOrder();
	        $testorder->initWithId($_GET["order"]);
	        if(($testorder->step == "done" && $_GET["fedit"] != 1) || ($_GET["fedit"] == 1 && !in_array($step, array(1, 2, 4, 'coverimage'))))
	        {
	            header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
	        }
	        
	        if($_GET["fedit"] == 1 && $step == 1 && $testorder->print)
	        {
	            header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
	        }
	        
	        if($_GET["fedit"] == 1 && $step == 2 && $testorder->paid)
	        {
	            header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
	        }
	        
	        if($_GET["fedit"] == 1 && $step == 4 && $testorder->print)
	        {
	            header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
	        }
	    }
	     
	    //Check token
	    checkUrlToken();
	    
	    /*
	     * Felder
	     */
	    if($_GET["ebook"] || $_GET['book'] || ($_GET['bookitem'] && $step == 1))
	    {
	        $this->step1();
	        
	        if($_POST['submitter'])
	        {
	            $order = $this->getOrCreateOrder();
	            
	            if($_GET["ebook"])
	            {
	                $book = new PnBook();
	                $book->initEbook($_GET['ebook']);
	                
	                //Create default item
	                $bookItem         = $this->createBookItem($order, $book);
	                if($bookItem)
	                {
	                    $widmungItem  = $this->createWidmungItem($order, $bookItem);
                        $designItem   = $this->createDesignItem($order, $bookItem);
                        
                        //goto next step
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            $order->edit(array("order_step" => "widmung"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&widmungitem=%s&step=%s&token=%s", $customize_page, $order->id, $bookItem->id, $widmungItem->id, 2, $token));
                        }
                        else
                        {
                            $order->edit(array("order_step" => "widmung"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&widmungitem=%s&step=%s", $customize_page, $order->id, $bookItem->id, $widmungItem->id, 2));
                        }
	                }
	            }
	            elseif($_GET['book'])
	            {
	                $book = new PnBook();
	                $book->current($_GET['book']);
	                
	                //Create default item
	                $bookItem         = $this->createBookItem($order, $book);
	                if($bookItem)
	                {
	                    $widmungItem  = $this->createWidmungItem($order, $bookItem);
                        $designItem   = $this->createDesignItem($order, $bookItem);
                        
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            $order->edit(array("order_step" => "widmung"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&widmungitem=%s&step=%s&token=%s", $customize_page, $order->id, $bookItem->id, $widmungItem->id, 2, $token));
                        }
                        else
                        {
                            $order->edit(array("order_step" => "widmung"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&widmungitem=%s&step=%s", $customize_page, $order->id, $bookItem->id, $widmungItem->id, 2));
                        }
	                }
	            }
	            else if($_GET['bookitem'])
	            {
	                $order = $this->getOrCreateOrder();
	                
	                //edit current book item
	                $bookItem = $this->editBookItem();
	                if($bookItem)
	                {
                        $widmungItem = $bookItem->getWidmungItem();
                        
                        if($_GET["edit"] && $_GET["edit"] == 1)
                        {
                            //goto basket
                            if($GLOBALS['TL_CONFIG']['use_url_token'])
                            {
                                $token = generateToken($order->id);
                                header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $order->id, $token));
                            }
                            else
                            {
                                header(sprintf("Location: %s?order=%s", $checkout_page, $order->id));
                            }
                        }
                        else if($_GET["fedit"] && $_GET["fedit"] == 1)
                        {
                            header(sprintf("Location: %s?order=%s&step=6", $checkout_page, $order->id));
                        }
                        else
                        {
                            //goto next step
                            if($GLOBALS['TL_CONFIG']['use_url_token'])
                            {
                                $token = generateToken($order->id);
                                header(sprintf("Location: %s?order=%s&bookitem=%s&widmungitem=%s&step=%s&token=%s", $customize_page, $order->id, $bookItem->id, $widmungItem->id, 2, $token));
                            }
                            else
                            {
                                header(sprintf("Location: %s?order=%s&bookitem=%s&widmungitem=%s&step=%s", $customize_page, $order->id, $bookItem->id, $widmungItem->id, 2));
                            }
                        }
	                }
	            }
	        }
	        
	        if($_POST['quicksubmitter'])
	        {
	            $order = $this->getOrCreateOrder();
	            
	            if($_GET['book'])
	            {
	                $book = new PnBook();
	                $book->current($_GET['book']);
	                
	                //Create default item
	                $bookItem     = $this->createBookItem($order, $book);
                    $widmungItem  = $this->createWidmungItem($order, $bookItem);
                    $designItem   = $this->createDesignItem($order, $bookItem);
	                
	                //goto basket
	                if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        $order->edit(array("order_step" => "basket"));
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $order->id, $token));
                    }
                    else
                    {
                        $order->edit(array("order_step" => "basket"));
                        header(sprintf("Location: %s?order=%s", $checkout_page, $order->id));
	                }
	            }
	            elseif($_GET['ebook'])
	            {
	                $book = new PnBook();
	                $book->initEbook($_GET['ebook']);
	                
	                //Create default item
	                $bookItem     = $this->createBookItem($order, $book);
                    $widmungItem  = $this->createWidmungItem($order, $bookItem);
                    $designItem   = $this->createDesignItem($order, $bookItem);
	                
	                //goto basket
	                if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        $order->edit(array("order_step" => "basket"));
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $order->id, $token));
                    }
                    else
                    {
                        $order->edit(array("order_step" => "basket"));
                        header(sprintf("Location: %s?order=%s", $checkout_page, $order->id));
                    }
	            }
	            else
	            {
	                gotoErrorPage();
	            }
	        }
	    }
	    
	    /*
	     * Widmung
	     */
	    else if($_GET["order"] && $_GET["widmungitem"] && $step == 2)
	    {
	        $this->step2();
	        
	        if($_POST['nextwithout'])
	        {
	            $widmungItem = new PnWidmungItem();
	            $widmungItem->initWithId($_GET["widmungitem"]);
	            
	            if($widmungItem->id == "" || $widmungItem->id == 0)
	            {
	                gotoErrorPage();
	            }
	            else
	            {
	                $widmungItem->edit(
	                                array(
                                        "item_fields"  => '{"text": "", "fontface": "Century Schoolbook L", "fontcolor": "black"}',
                                        "artikel_id"   => 8830
                                        )
                                    );
                    
                    //goto next step
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($widmungItem->order_id);
                        $order = new PnOrder();
                        $order->initWithId($widmungItem->order_id);
                        $order->edit(array("order_step" => "version"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                            $customize_page, $widmungItem->order_id, $widmungItem->parent_id, 3, $token));
                    }
                    else
                    {
                        $order = new PnOrder();
                        $order->initWithId($widmungItem->order_id);
                        $order->edit(array("order_step" => "version"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                            $customize_page, $widmungItem->order_id, $widmungItem->parent_id, 3));
                    }
	            }
	        }
	        
	        if($_POST["submitter"])
	        {
	            $widmungItem = new PnWidmungItem();
	            $widmungItem->initWithId($_GET["widmungitem"]);
	            
	            $fields	= array(
							'text' 		=> utf8_decode(utf8_encode($_POST['var_text'])),
							'fontface' 	=> $_POST['var_fontface'],
							'fontcolor'	=> $_POST['var_fontcolor']
					);
					
				if($widmungItem->id == "" || $widmungItem->id == 0)
	            {
	                gotoErrorPage();
	            }
	            else
	            {
	                $widmung_article = $_POST['artikel'];
	                
	                //Fixme: kinderbuch
	                if($widmung_article == '')
	                    $widmung_article = '8830';
	                
	                $bookItem = new PnBookItem();
	                $bookItem->initWithId($_GET["bookitem"]);
	                $book     = new PnBook();
	                $book->initWithId($bookItem->book_id);
	                
	                if(in_array($book->type_id , array(2, 13)))
	                {
	                    $arr = array();
	                    $vartext_len = strlen($_POST["var_text"]);
	                    $vartext_line = preg_match_all('/\n/', $_POST["var_text"], $arr) + 1;
	                    
	                    if($vartext_len > 300)
	                    {
	                        $errorText = true;
	                        $this->Template->errorTextLength = true;
	                    }
	                    
	                    if($vartext_line > 16)
	                    {
	                        $errorText = true;
	                        $this->Template->errorTextLine = true;
	                    }
	                }else{
	                /**/
	                $arr = array();
	                    $vartext_len = strlen($_POST["var_text"]);
	                    $vartext_line = preg_match_all('/\n/', $_POST["var_text"], $arr) + 1;
	                    
	                    if($vartext_len > 1400) //config max character for book
	                    {
	                        $errorText = true;
	                        $this->Template->errorTextLength = true;
	                    }
	                    
	                    if($vartext_line > 16)
	                    {
	                        $errorText = true;
	                        $this->Template->errorTextLine = true;
	                    }
	               /* */
	                }
	                
	                if($widmung_article == '8833')
	                {
	                    if($_POST["var_fontface"] == "Century Schoolbook L" && $_POST["var_fontcolor"] == "black")
	                    {
	                        $widmung_article = '8830';
	                    }
	                }
	                
                    $widmung_error = $_POST["input_error"];
                    $errorCharacters = getErrorCharacter($_POST['var_text']);
                    if($widmung_error == "texttoolong")
                    {
                        
                    }
                    else if(count($errorCharacters) > 0)
                    {
                        $this->Template->errorCharacters = implode(", ", $errorCharacters);
                    }
                    else if($_GET["edit"] && $_GET["edit"] == 1)
                    {
                        if(!$errorText)
                        {
                            $widmungItem->edit(
                                        array(
                                            "item_fields"  => json_encode($fields),
                                            "artikel_id"   => $widmung_article
                                            )
                                        );
                            //goto basket
                            if($GLOBALS['TL_CONFIG']['use_url_token'])
                            {
                                $token = generateToken($widmungItem->order_id);
                                header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $widmungItem->order_id, $token));
                            }
                            else
                            {
                                header(sprintf("Location: %s?order=%s", $checkout_page, $widmungItem->order_id));
                            }
                        }
                    }
                    else
                    {
                        if(!$errorText)
                        {
                            $widmungItem->edit(
                                        array(
                                            "item_fields"  => json_encode($fields),
                                            "artikel_id"   => $widmung_article
                                            )
                                        );
                            
                            $bookArticle = new PnArticle();
                            $bookArticle->initWithId($bookItem->article_id);
                            
                            //goto next step
                            if($bookArticle->version_id == 101)
                            {
                                $designItem  = $bookItem->getDesignItem();
                                if($GLOBALS['TL_CONFIG']['use_url_token'])
                                {
                                    $token = generateToken($bookItem->order_id);
                                    $order = new PnOrder();
                                    $order->initWithId($bookItem->order_id);
                                    $order->edit(array("order_step" => "design"));
                                    header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s&token=%s", 
                                        $customize_page, $bookItem->order_id, $bookItem->id, $designItem->id, 4, $token));
                                }
                                else
                                {
                                    $order = new PnOrder();
                                    $order->initWithId($bookItem->order_id);
                                    $order->edit(array("order_step" => "design"));
                                    header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                                        $customize_page, $bookItem->order_id, $bookItem->id, $designItem->id, 4));
                                }
                            }
                            else
                            {
                                if($GLOBALS['TL_CONFIG']['use_url_token'])
                                {
                                    $token = generateToken($widmungItem->order_id);
                                    $order = new PnOrder();
                                    $order->initWithId($widmungItem->order_id);
                                    $order->edit(array("order_step" => "version"));
                                    header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                                        $customize_page, $widmungItem->order_id, $widmungItem->parent_id, 3, $token));
                                }
                                else
                                {
                                    $order = new PnOrder();
                                    $order->initWithId($widmungItem->order_id);
                                    $order->edit(array("order_step" => "version"));
                                    header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                                        $customize_page, $widmungItem->order_id, $widmungItem->parent_id, 3));
                                }
                            }
                        }
	                }
	            }
	        }
	        
	        if($_POST["forceedit"])
	        {
	            $widmungItem = new PnWidmungItem();
	            $widmungItem->initWithId($_GET["widmungitem"]);
	            
	            $fields	= array(
							'text' 		=> utf8_decode(utf8_encode($_POST['var_text'])),
							'fontface' 	=> $_POST['var_fontface'],
							'fontcolor'	=> $_POST['var_fontcolor']
						  );
				
	            $widmung_article = $_POST['artikel'];
	            //Fixme: kinderbuch
                if($widmung_article == '')
                    $widmung_article = '8830';
                
                if($widmung_article == '8833')
                {
                    if($_POST["var_fontface"] == "Century Schoolbook L" && $_POST["var_fontcolor"] == "black")
                    {
                        $widmung_article = '8830';
                    }
                }
                
                $widmungItem->edit(
                                array(
                                    "item_fields"  => json_encode($fields),
                                    "artikel_id"   => $widmung_article
                                    )
                                );
                
                $order = new PnOrder();
                $order->initWithId($_GET["order"]);
                $order->recalc();  //Recalculation posten after editing
                
                $bookArticle = new PnArticle();
                $bookArticle->initWithId($bookItem->article_id);
                if($bookArticle->version_id == 101)
                {
                    $designItem  = $bookItem->getDesignItem();
                    
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s&token=%s", 
                            $customize_page, $bookItem->order_id, $bookItem->id, $designItem->id, 4, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                            $customize_page, $bookItem->order_id, $bookItem->id, $designItem->id, 4));
                    }
                }
                
                if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($widmungItem->order_id);
                    header(sprintf("Location: %s?order=%s&step=6&token=%s", $checkout_page, $widmungItem->order_id, $token));
                }
                else
                {
                    header(sprintf("Location: %s?order=%s&step=6", $checkout_page, $widmungItem->order_id));
                }
                
	        }
	        
	    }
	    
	    /*
	     * Einband
	     */
	    else if($_GET["order"] && $_GET["bookitem"] && $step == 3)
	    {
	        $this->step3();
	        
	        if($_POST["submitter"])
	        {
	            $bookItem = new PnBookItem();
	            $bookItem->initWithId($_GET["bookitem"]);
	            $bookItem->edit(array('artikel_id' => $_POST['artikel']));
	            
	            //Check available addons
	            $bookArticle = new PnArticle();
	            $bookArticle->initWithId($_POST['artikel']);
	            
	            $saved_addons = $bookItem->getAddonItems();
	            $addon_cached = array();
	            $addon_available = array();
	            $addon_will_remove = array();
	            if(count($saved_addons) > 0)
	            {
	                foreach($saved_addons as $k => $v)
	                {
	                    $addon_cached[] = $v->article_id;
	                }
	                foreach($bookArticle->getAddons() as $k => $v)
                    {
                        $addon_available[] = $v->id;
                    }
                    
                    foreach($addon_cached as $cached)
                    {
                        if(!in_array($cached, $addon_available))
                        {
                            $addon_will_remove[] = $cached;
                        }
                    }
                    
                    if(count($addon_will_remove) > 0)
                    {
                        $bookItem->removeChildItems($addon_will_remove);
                    }
	            }
	            
	            if($_POST['decocover'])
				{
                    //Save decocover
                    $bookItem->addDecocover(array("item_id" => $bookItem->id, "decocover_id" => $_POST['decocover']));
                    $bookItem->edit(array("covercolor_id" => null));
                }
                
                if($_POST['covercolor'] && !isset($_POST['decocover']))
                {
                    $bookItem->edit(array("covercolor_id" => $_POST['covercolor']));
                }
                
                if(isset($_GET['edit']) && $_GET['edit'] == 1)
                {
                    $book = new PnBook();
                    $book->initWithId($bookItem->book_id);
                    
                    $designs = $book->getBookDesigns($_POST['artikel']);
                    $arrArticle = array();
                    
                    foreach($designs as $design)
                    {
                        $arrArticle[] = $design->artikel_id;
                    }
                    
                    $designItem = $bookItem->getDesignItem();
                    if(!in_array($designItem->article_id, $arrArticle))
                    {
                        $designItem->edit(array("artikel_id" => $arrArticle[0]));
                    }
                    
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
					}
				}
				else
				{
				    $designItem = $bookItem->getDesignItem();
				    
				    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        $order = new PnOrder();
                        $order->initWithId($bookItem->order_id);
                        $order->edit(array("order_step" => "design"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s&token=%s", 
                            $customize_page, $bookItem->order_id, $bookItem->id, $designItem->id, 4, $token));
                    }
                    else
                    {
                        $order = new PnOrder();
                        $order->initWithId($bookItem->order_id);
                        $order->edit(array("order_step" => "design"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                            $customize_page, $bookItem->order_id, $bookItem->id, $designItem->id, 4));
                    }
				}
	        }
	    }
	    
	    /*
	     * Umschlag
	     */
	    else if($_GET["order"] && $_GET["designitem"] && $step == 4)
	    {
	        $this->step4();
	        
	        if($_POST["submitter"] || $_POST["uploader"])
	        {
	            $subtitle = $_POST['subtitle'] ? $_POST['subtitle'] : "";
	            $designItem = new PnDesignItem();
	            $designItem->initWithId($_GET["designitem"]);
	            $this->Templates->design_id =  $designItem->id;
	            $preview_cache = $GLOBALS['TL_CONFIG']['preview_cache'] ? $GLOBALS['TL_CONFIG']['preview_cache']
	                            : "/var/www/Staging_Backend_Personalnovel/production/shared/cache/preview/";
	            if(file_exists($preview_cache . $_GET['bookitem'] . '-' . $_GET['order'] . "-u1.png"))
                {
                    unlink($preview_cache . $_GET['bookitem'] . '-' . $_GET['order'] . "-u1.png");
                }
	            
	            if($designItem->id == "" || $designItem->id == 0)
	                gotoErrorPage();
	            else
	                $designItem->edit(
	                    array(
	                        "artikel_id"     => $_POST['artikel'], 
	                        "item_subtitle"  => $subtitle
	                        )
	                    );
	                
	            if($_POST["submitter"])
	            {
	                if($_GET["edit"] && $_GET["edit"] == 1)
	                {
	                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($_GET["order"]);
                            header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
	                    }
	                }
	                else
	                {
	                    $bookItem = new PnBookItem();
	                    $bookItem->initWithId($_GET["bookitem"]);
	                    
	                    $book = new PnBook();
	                    $book->initWithId($bookItem->book_id);
	                    
	                    //Ebook
	                    $bookArticle = new PnArticle();
                        $bookArticle->initWithId($bookItem->article_id);
                        if($bookArticle->version_id == 101)
                        {
                            if($GLOBALS['TL_CONFIG']['use_url_token'])
                            {
                                $token = generateToken($_GET["order"]);
                                $order = new PnOrder();
                                $order->initWithId($_GET["order"]);
                                $order->edit(array("order_step" => "basket"));
                                header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                            }
                            else
                            {
                                $order = new PnOrder();
                                $order->initWithId($_GET["order"]);
                                $order->edit(array("order_step" => "basket"));
                                header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
                            }
                        }
	                    
	                    //If childrenbook, giftbook
	                    elseif(in_array($book->type_id, array(2, 11, 12, 13)))
	                    {
	                        $bookArticle = new PnArticle();
                            $bookArticle->initWithId($bookItem->article_id);
                            
                            $addons = $bookArticle->getAddons();
                            if(count($addons) > 0)
                            {
                                if($GLOBALS['TL_CONFIG']['use_url_token'])
                                {
                                    $token = generateToken($designItem->order_id);
                                    $order = new PnOrder();
                                    $order->initWithId($designItem->order_id);
                                    $order->edit(array("order_step" => "addons"));
                                    header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                                        $customize_page, $designItem->order_id, $designItem->parent_id, 6, $token));
                                }
                                else
                                {
                                    $order = new PnOrder();
                                    $order->initWithId($designItem->order_id);
                                    $order->edit(array("order_step" => "addons"));
                                    header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                                        $customize_page, $designItem->order_id, $designItem->parent_id, 6));
                                }
                            }
                            else
                            {
                                if($GLOBALS['TL_CONFIG']['use_url_token'])
                                {
                                    $token = generateToken($designItem->order_id);
                                    $order = new PnOrder();
                                    $order->initWithId($designItem->order_id);
                                    $order->edit(array("order_step" => "basket"));
                                    header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $designItem->order_id, $token));
                                }
                                else
                                {
                                    $order = new PnOrder();
                                    $order->initWithId($designItem->order_id);
                                    $order->edit(array("order_step" => "basket"));
                                    header(sprintf("Location: %s?order=%s", $checkout_page, $designItem->order_id));
                                }
                            }
	                    }
	                    //If classic book
	                    else if(in_array($book->type_id, array(15, 18, 23)))
	                    {
	                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                            {
                                $token = generateToken($designItem->order_id);
                                $order = new PnOrder();
                                $order->initWithId($designItem->order_id);
                                $order->edit(array("order_step" => "upgrades"));
                                header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                                    $customize_page, $designItem->order_id, $designItem->parent_id, "extra", $token));
                            }
                            else
                            {
                                $order = new PnOrder();
                                $order->initWithId($designItem->order_id);
                                $order->edit(array("order_step" => "upgrades"));
                                header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                                    $customize_page, $designItem->order_id, $designItem->parent_id, "extra"));
                            }
	                    }
	                    else
	                    {
	                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                            {
                                $token = generateToken($designItem->order_id);
                                $order = new PnOrder();
                                $order->initWithId($designItem->order_id);
                                $order->edit(array("order_step" => "font"));
                                header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                                    $customize_page, $designItem->order_id, $designItem->parent_id, 5, $token));
                            }
                            else
                            {
                                $order = new PnOrder();
                                $order->initWithId($designItem->order_id);
                                $order->edit(array("order_step" => "font"));
                                header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                                    $customize_page, $designItem->order_id, $designItem->parent_id, 5));
                            }
	                    }
	                }
	            }
	            else
	            {
	                //Ebook
                    $bookArticle = new PnArticle();
                    $bookArticle->initWithId($bookItem->article_id);
                    if($bookArticle->version_id == 101)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($_GET["order"]);
                            $order = new PnOrder();
                            $order->initWithId($_GET["order"]);
                            $order->edit(array("order_step" => "basket"));
                            header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                        }
                        else
                        {
                            $order = new PnOrder();
                            $order->initWithId($_GET["order"]);
                            $order->edit(array("order_step" => "basket"));
                            header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
                        }
                    }
                    else
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($designItem->order_id);
                            $order = new PnOrder();
                            $order->initWithId($designItem->order_id);
                            $order->edit(array("order_step" => "grafik"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s&token=%s", 
                                $customize_page, $designItem->order_id, $designItem->parent_id, $designItem->id, "coverimage", $token));
                        }
                        else
                        {
                            $order = new PnOrder();
                            $order->initWithId($designItem->order_id);
                            $order->edit(array("order_step" => "grafik"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                                $customize_page, $designItem->order_id, $designItem->parent_id, $designItem->id, "coverimage"));
                        }
                    }
	            }
	        }
	    }
	    else if($_GET["order"] && $_GET["bookitem"] && $step == 5)
	    {
	        $this->step5();
	        
	        if($_POST["submitter"])
	        {
	            $bookItem = new PnBookItem();
	            $bookItem->initWithId($_GET["bookitem"]);
	            
	            $bookItem->edit(array("font_id" => $_POST["font"]));
	            
	            $bookArticle = new PnArticle();
	            $bookArticle->initWithId($bookItem->article_id);
	            $addons = $bookArticle->getAddons();
	            
	            if(isset($_GET['edit'])&& $_GET['edit'] == 1)
	            {
	                if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                    }
				} 
				else
				{
                    if(count($addons) > 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($bookItem->order_id);
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "addons"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                                $customize_page, $bookItem->order_id, $bookItem->id, 6, $token));
                        }
                        else
                        {
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "addons"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                                $customize_page, $bookItem->order_id, $bookItem->id, 6));
                        }
                    }
                    else
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($bookItem->order_id);
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "basket"));
                            header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                        }
                        else
                        {
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "basket"));
                            header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                        }
					}
				}
	        }
	    }
	    else if($_GET["order"] && $_GET["bookitem"] && $step == 6)
	    {
	        $this->step6();
	        
	        if($_POST["submitter"])
	        {
	            $bookItem = new PnBookItem();
	            $bookItem->initWithId($_GET["bookitem"]);
	            
	            $addons = $bookItem->getAddonItems();
				
				$addon_cached 		= array();
				$addon_pieces		= array();
				$addon_selected 	= array();
				$addon_exists 		= array();
				$addon_will_remove 	= array();
				$addon_will_addnew 	= array();
				
				//TODO : optimize later
				foreach($addons as $k => $v)
				{
					$addon_cached[] = $v->article_id;
				}
				
				if($_POST['Addon-artikel'])
				{
                    foreach($_POST['Addon-artikel'] as $k => $v)
                    {
                        $addon_selected[] = $v;
                    }
				}
				
				foreach($addon_cached as $old_addon)
				{
					if(in_array($old_addon, $addon_selected))
						$addon_exists[] = $old_addon;
					else
						$addon_will_remove[] = $old_addon;
				}
				
				foreach($addon_selected as $new_addon)
				{
					if(!in_array($new_addon, $addon_cached))
						$addon_will_addnew[] = $new_addon;
				}
				
				//Remove addon
				if(count($addon_will_remove) > 0)
				{
					$bookItem->removeChildItems($addon_will_remove);
				}
				
				//Add new 
				$item = new PnItem();
				
	        	if($_POST['addon_anzahl_id'] && $_POST['addon_anzahl_'.$_POST['addon_anzahl_id']]){
					$pieces = $_POST['addon_anzahl_'.$_POST['addon_anzahl_id']];
					$item->updateAddonPieces($pieces,$_POST['addon_anzahl_id'],$_GET["order"]);	
				}
				
				foreach($addon_will_addnew as $addon_new)
				{
					$article = new PnArticle();
					$article->initWithId($addon_new);

					if($article->class_id == 5)
					{
						$pieces = 1;
						if($_POST['addon_anzahl_'.$article->id]){
							$pieces = $_POST['addon_anzahl_'.$article->id];
						}
						
					    $addon_id = $item->add(array(
                                "artikel_id" 	=> $article->id,
                                "order_id" 		=> $bookItem->order_id,
                                "parent_id" 	=> $bookItem->id,
                                "item_pieces" 	=> $pieces,
                                "class_id" 		=> $article->class_id,
                                "item_fields" => $bookItem->fields
                            ));
					}
					else
					{
						$pieces = 1;
						if($_POST['addon_anzahl_'.$article->id]){
							$pieces = $_POST['addon_anzahl_'.$article->id];
						}

					    $addon_id = $item->add(array(
                                "artikel_id" 	=> $article->id,
                                "order_id" 		=> $bookItem->order_id,
                                "parent_id" 	=> $bookItem->id,
                                "item_pieces" 	=> $pieces,
                                "class_id" 		=> $article->class_id
                            ));
                            
					}
					
				}
				
				if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($bookItem->order_id);
                    $order = new PnOrder();
                    $order->initWithId($bookItem->order_id);
                    $order->edit(array("order_step" => "basket"));
                    header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                }
                else
                {
                    $order = new PnOrder();
                    $order->initWithId($bookItem->order_id);
                    $order->edit(array("order_step" => "basket"));
                    header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
				}
	        }
	    }
	    else if($_GET["order"] && $_GET["designitem"] && $step == "coverimage")
	    {
	        $this->step_coverimage();
	        
	        if($_POST["uploader"])
	        {
	            $file = str_replace(' ', '_', $_FILES['imagefile']['name']);
	            $permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
	            
	            $order_id  = $_GET["order"];
	            $design_id = $_GET["designitem"];
	            
	            $file_extension = ".png.org.png";
                $file_short_extension = ".jpg";
                
                if($_FILES['imagefile']['type'] == 'image/gif')
                {
                    $file_extension = ".gif.org.gif";
                }
                if($_FILES['imagefile']['type'] == 'image/jpeg' || $_FILES['imagefile']['type'] == 'image/pjpeg')
                {
                    $file_extension = ".jpg.org.jpg";
                }
                
                $file = $order_id . "-" . $design_id . $file_extension;
                if (in_array($_FILES['imagefile']['type'], $permitted))
                {
                    //Customer images
                    $cachefiles = glob($upload_dir . $order_id . "-" . $design_id . "*");
                    array_walk($cachefiles, function ($cachefile) {
                        unlink($cachefile);
                    });
                    
                    //Customer frontend images
                    $cachefiles = glob($upload_dir_frontend . $order_id . "-" . $design_id . "*");
                    array_walk($cachefiles, function ($cachefile) {
                        unlink($cachefile);
                    });
                    
                    //Upload to folder print
                    if(move_uploaded_file($_FILES['imagefile']['tmp_name'], $upload_dir_frontend . $file))
                    {
                        //Default crop file
                        $filename  = pathinfo($upload_dir_frontend . $file);
                        $extension = $filename['extension'];
                        
                        /*$imagesize = getimagesize($upload_dir_frontend . $file);
                        $imagew    = $imagesize[0];
                        $imageh    = $imagesize[1];*/
                        
                        $targ_h = $_POST['targ_h'];
                        $targ_w = $_POST['targ_w'];
                        $crop_x = 0;
                        $crop_w = $targ_w;
                        $crop_y = 0;
                        $crop_h = $targ_h;
                        
                        switch($extension)
                        {
                            case "png":
                                $img_r = imagecreatefrompng($upload_dir_frontend . $file);
                                break;
                            case "gif":
                                $img_r = imagecreatefromgif($upload_dir_frontend . $file);
                                break;
                            default:
                                $img_r = imagecreatefromjpeg($upload_dir_frontend . $file);
                                break;
                        }
                        
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r, $img_r, 0, 0, $crop_x, $crop_y, $targ_w, $targ_h, $crop_w, $crop_h);
                        imagejpeg($dst_r, $upload_dir . $order_id . "-" . $design_id . $file_short_extension);
                    }
                    
                    $designItem = new PnDesignItem();
                    $designItem->initWithId($_GET["designitem"]);
                    
                    $designItem->edit(
                                    array (
                                       "item_image1_present"  => 1,
                                       "item_image1_cropped"   => 1
                                    )
                                );
                    if($_GET["edit"] == 1) $fla = "&edit=1"; else $fla = '';
                    if($_GET["fedit"] == 1) $fla = "&fedit=1"; else $fla = "";
			   
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($designItem->order_id);
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s%s&token=%s", 
                            $customize_page, $designItem->order_id, $designItem->parent_id, $designItem->id, "coverimage",$fla, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s%s", 
                            $customize_page, $designItem->order_id, $designItem->parent_id, $designItem->id, "coverimage",$fla));
	                }
                }
	        }
	        
	        /*if($_POST["nextpage"])
	        {
	            //goto preview page
	            header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                    $customize_page, $_GET["order"], $_GET["bookitem"], 5));
	        }*/
	        
	        if($_POST["cropimage"])
	        {
	            //Crop image
	            $upload_filename = "";
	            $order_id = $_GET["order"];
	            $design_id = $_GET["designitem"];
	            $crop_filename   = $upload_dir . $order_id . "-" . $design_id . ".jpg";
			    if(file_exists($upload_dir_frontend . $order_id . "-" . $design_id . ".png.org.png"))
			    {
			        $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".png.org.png";
			    }
			    else if(file_exists($upload_dir_frontend . $order_id . "-" . $design_id . ".gif.org.gif"))
			    {
			        $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".gif.org.gif";
			    }
			    else
			    {
			        $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".jpg.org.jpg";
			    }
			    
			    $filename  = pathinfo($upload_filename);
			    $extension = $filename['extension'];
			    $imagesize = getimagesize($upload_filename);
			    $imagew    = $imagesize[0];
			    $imageh    = $imagesize[1];
			    
			    $targ_h = $_POST['targ_h'];
			    $targ_w = $_POST['targ_w'];
			    if($imagew > $imageh)
			    {
			        $targ_w = $imageh / $targ_h * $targ_w;
			        $targ_h = $imageh;
			    } else {
			        $targ_h = $imagew / $targ_w * $targ_h;
			        $targ_w = $imagew;
			    }
			    
			    //$quality = 90;
			    $src = $upload_filename;
			    $dst = $crop_filename;
			    switch($extension)
			    {
                    case "png":
                        $img_r = imagecreatefrompng($src);
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                            $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                        imagejpeg($dst_r, $dst);
                        break;
                    case "gif":
                        $img_r = imagecreatefromgif($src);
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                            $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                        imagejpeg($dst_r, $dst);
                        
                        break;
                    default:
                        $img_r = imagecreatefromjpeg($src);
                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                            $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                        
                        imagejpeg($dst_r, $dst);
                        break;
			    }
			    
			    if($_GET["edit"] == 1)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
                    }
			    }
			    else
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        $order = new PnOrder();
                        $order->initWithId($_GET["order"]);
                        $order->edit(array("order_step" => "design"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s&token=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], $_GET["designitem"], 4, $token));
                    }
                    else
                    {
                        $order = new PnOrder();
                        $order->initWithId($_GET["order"]);
                        $order->edit(array("order_step" => "design"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], $_GET["designitem"], 4));
                    }
                }
	        }
	        
	        if($_POST["dochkein"] || $_POST["nextpage"])
	        {
	        	if($_GET["edit"] && $_GET["edit"] == 1)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
                    }
			    }
			    else
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        $order = new PnOrder();
                        $order->initWithId($_GET["order"]);
                        $order->edit(array("order_step" => "design"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s&token=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], $_GET["designitem"], 4, $token));
                    }
                    else
                    {
                        $order = new PnOrder();
                        $order->initWithId($_GET["order"]);
                        $order->edit(array("order_step" => "design"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], $_GET["designitem"], 4));
                    }
			    }
	        }
	    }
	    else if($_GET["order"] && $_GET["bookitem"] && $step == "extra")
	    {
	        if($_POST["buy"])
	        {
	            $bookItem = new PnBookItem();
	            $bookItem->initWithId($_GET["bookitem"]);
	            
	            if(count($bookItem->getBookLockUpgradeItems()) == 0)
                {
                    $bookblock = new PnItem();
                    $booklock_id = $bookblock->add(array(
                            'order_id' 			=> $_GET["order"],
                            'class_id'			=> 12,
                            'parent_id'			=> $bookItem->id,
                            'item_pieces'		=> 1,
                            'item_print'		=> 0,
                            'item_testprint'	=> 0,
                            'item_fields'       => '{"color": "black", "text": ""}',
                            'artikel_id'        => $_POST['artikel']
                    ));
                    
                    $this->Database->prepare("
                            INSERT INTO " . $this->config->database . ".items_assets (`item_id`, `asset_id`)
                            VALUES (?, ?)
                        ")->execute($booklock_id, 1205);
                }
                
                if($_GET["edit"])
                {
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&edit=1&token=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra", $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&edit=1", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra"));
                    }
                }
                else
                {
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        $order = new PnOrder();
                        $order->initWithId($_GET["order"]);
                        $order->edit(array("order_step" => "upgrades"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra", $token));
                    }
                    else
                    {
                        $order = new PnOrder();
                        $order->initWithId($_GET["order"]);
                        $order->edit(array("order_step" => "upgrades"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra"));
                    }
                }
                
	        }
	        
	        if($_POST['exlibris'])
		    {
		        $bookItem = new PnBookItem();
	            $bookItem->initWithId($_GET["bookitem"]);
	            
	            if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($bookItem->order_id);
                    header(sprintf("Location: %s?order=%s&bookitem=%s&childitem=%s&step=%s&token=%s", 
                            $customize_page, $bookItem->order_id, $bookItem->id, $_POST['childitem'], "exlibris", $token));
                }
                else
                {
                    header(sprintf("Location: %s?order=%s&bookitem=%s&childitem=%s&step=%s", 
                            $customize_page, $bookItem->order_id, $bookItem->id, $_POST['childitem'], "exlibris"));
                }
		    }
		    
		    if($_POST['delete'] && $_POST['childitem'])
		    {
		        $this->Database->prepare("
		                DELETE FROM " . $this->config->database . ".items_assets
		                WHERE `item_id` = ?
		            ")->execute($_POST['childitem']);
		            
		        $booklock = new PnItem();
		        $booklock->initWithId($_POST['childitem']);
		        $booklock->remove();
		        
		        if($_GET["edit"])
                {
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&edit=1&token=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra", $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&edit=1", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra"));
                    }
                }
                else
                {
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra", $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra"));
                    }
                }
		    }
		    
		    if($_POST['submitter'])
		    {
		        $bookItem = new PnBookItem();
		        $bookItem->initWithId($_GET["bookitem"]);
		        
		        $bookArticle = new PnArticle();
				$bookArticle->initWithId($bookItem->article_id);
				$addons = $bookArticle->getAddons();
				
				if(isset($_GET['edit'])&& $_GET['edit'] == 1)
				{
				    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
					}
				}
				else
				{
                    if(count($addons) > 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($bookItem->order_id);
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "addons"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                                $customize_page, $bookItem->order_id, $bookItem->id, 6, $token));
                        }
                        else
                        {
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "addons"));
                            header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                                $customize_page, $bookItem->order_id, $bookItem->id, 6));
                        }
                    }
                    else
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($bookItem->order_id);
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "basket"));
                            header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                        }
                        else
                        {
                            $order = new PnOrder();
                            $order->initWithId($bookItem->order_id);
                            $order->edit(array("order_step" => "basket"));
                            header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                        }
                    }
				}
		    }
		    
		    $this->step_extra();
	    }
	    else if($_GET["order"] && $_GET["bookitem"] && $_GET["childitem"] && $step == "exlibris")
	    {
	        if($_POST['submitter'] || $_POST['backtocart'])
	        {
	            $booklock = new PnItem();
		        $booklock->initWithId($_GET['childitem']);
		        
		        $fields = array(
                                "text" => $_POST['var_text'],
                                "color" => $_POST['var_color']
                            );
                
                $booklock->edit(array('item_fields' => json_encode($fields)));
		        
		        $this->Database->prepare("
		                UPDATE " . $this->config->database . ".items_assets
		                SET asset_id = ?
		                WHERE item_id = ?
		            ")->execute($_POST["asset"], $booklock->id);
		            
		        if($_POST["submitter"])
		        {
		            if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($_GET["order"]);
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra", $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                            $customize_page, $_GET["order"], $_GET["bookitem"], "extra"));
                    }
                }
		        if($_POST["backtocart"])
		        {
		            if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($booklock->order_id);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $booklock->order_id, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $booklock->order_id));
                    }
		        }
	        }
	        
	        $this->step_exlibris();
	    }
	    else
	    {
	        gotoErrorPage(); //No step found
	    }
	    
	    $this->Template->step 	= $step;
	}
	
	function step1()
	{
	    $book = new PnBook();
	    
	    if($_GET["ebook"])
	    {
	    	//Get price for book
	    	$book->current($_GET['ebook']);
            $arrEntities = $book->getEntities();
            $this->Template->totalPriceBook = $book->getLowestPrice();
            
	        $book->initEbook($_GET["ebook"]);
            $arrEntities = $book->getEntities();
            $this->Template->totalPrice = $book->getLowestPrice();
	    }
	    elseif($_GET['book'])
	    {
            $book->current($_GET['book']);
            $arrEntities = $book->getEntities();
            
            $this->Template->totalPrice = $book->getLowestPrice();
	    }
	    else if($_GET['bookitem'])
	    {
	        $bookItem = new PnBookItem();
	        $bookItem->initWithId($_GET['bookitem']);
	        
	        $bookArticle = new PnArticle();
	        $bookArticle->initWithId($bookItem->article_id);
	        
	        if($bookArticle->version_id == 101)
	        {
	            $book->initEbook($bookItem->book_id);
	        }
	        else
	        {
	            $book->current($bookItem->book_id);
	        }
	        
	        $arrEntities = $book->getEntities();
	        $arrFields = json_decode($bookItem->fields);
			try
			{
				foreach($arrEntities as $k1 => $v1)
				{
					foreach($v1['fields'] as $k2 => $v2)
					{
						if(count($arrFields) > 0)
						{
							foreach($arrFields as $k3 =>$v3)
							{
								if($v2['field_name'] == $k3)
								{
									$arrEntities[$k1]['fields'][$k2]['field_custom'] = utf8_decode($v3);
								}
							}
						}
					}
				}
			}
			catch(Exception $e)
			{
				
			}
	        $grafik = json_decode($bookItem->getDesignGrafik()->item_fields);
            $arrayGrafik=array();
            if(count($grafik) > 0)
            {
                foreach($grafik as $k =>$v)
                {
                    $arrayGrafik[$k] = utf8_decode($v);
                }
            }
            
            $this->Template->totalPrice = $bookItem->getTotalPrice();
            $this->Template->document_id = $bookItem->document_id;
	    }
	    
	    //Error: no book found
	    if($book->id == "" || $book->id == 0)
	    {
	        gotoErrorPage();
	    }
	    else
	    {
	        $documents = $book->getBookDocuments();
	        $this->Template->documents  = $documents;
	        $this->Template->numOfDocs  = count($document);
	        $this->Template->book_showVarianten = $book->show_varianten;
	        
            $this->Template->book 	    = $book;
            $this->Template->src_thumbnail = "/tl_files/thumbnail-book/".standardize($book->title)."-".$book->id."-detail.png";
            $this->Template->entities   = $arrEntities;
            $this->Template->grafik     = $arrayGrafik;
            $this->Template->teaser     = $this->step1_teaser;
        }
	}
	
	function step2()
	{
	    $bookItem = new PnBookItem();
	    $bookItem->initWithId($_GET["bookitem"]);
	    
	    $book     = new PnBook();
	    $book->current($bookItem->book_id);
	        
	    $widmungItem = new PnWidmungItem();
	    $widmungItem->initWithId($_GET["widmungitem"]);
	    
	    if($widmungItem->id == "" || $widmungItem->id == 0)
	        gotoErrorPage();
	    
	    $widmung = new PnWidmung();
        if($book->type_id == 5 || $book->type_id == 2 || $book->type_id == 13)
            $this->Template->widmungs = $widmung->getWidmungs(1, 1); //For kid
        else
            $this->Template->widmungs = $widmung->getWidmungs();
            
        $this->Template->widmung         = $widmung;
        $this->Template->book            = $book;
		$this->Template->custom_widmung  = $widmungItem;
	    $this->Template->teaser          = $this->step2_teaser;
	    $this->Template->widmungPrice    = $widmungItem->getPrice();
	    $this->Template->totalPrice      = $bookItem->getTotalPrice();
	    
	    //send mail
	    if($_GET["step"]=='2'){
		    $arData = array();
		    $s ='';
		    $s .= 'Date : '.date("F j, Y, g:i a") .'<br/>';
		    $s .= 'OrderID: '. $_GET["order"].'<br/><br/>';
		            
	    	foreach($bookItem->getBookEntities() as $entity):
				foreach($entity['fields'] as $field):
				$s .= $field['field_description'].'	:  '.$field['field_name']. '<br/>';
				endforeach;
			endforeach;
			$arData['message'] = $s;
	        $this->sendMail($arData,'Custom:','custom beging ',$_GET["order"]);
	    }
	}
	
	function step3()
	{
	    $bookItem = new PnBookItem();
	    $bookItem->initWithId($_GET["bookitem"]);
	    
	    $book     = new PnBook();
	    $book->current($bookItem->book_id);
	    
	    $this->Template->book           = $book;
	    $this->Template->article_id     = $bookItem->article_id;
        $this->Template->color_id       = $bookItem->covercolor_id;
        $this->Template->decocover_id   = $bookItem->getDecocover();
        $this->Template->versionPrice   = $bookItem->getPrice();
        //$this->Template->totalPrice     = ($bookItem->getPrice() + $bookItem->getWidmungItem()->getPrice());
        $this->Template->totalPrice     = $bookItem->getTotalPrice();
        $this->Template->teaser         = $this->step3_teaser;
        $this->Template->config 		= $this->config;
	}
	
	function step4()
	{
	    $designItem = new PnDesignItem();
	    $designItem->initWithId($_GET["designitem"]);
	    
	    $bookItem = new PnBookItem();
	    $bookItem->initWithId($designItem->parent_id);
	    
	    $book = new PnBook();
	    $book->current($bookItem->book_id);
	    
	    //Load designs
	    $this->Template->book             = $book;
        $this->Template->article_id       = $designItem->article_id;
        $this->Template->book_article_id  = $bookItem->article_id;
        $this->Template->version 	      = $bookItem->getBookVersion();
        $this->Template->subtitle 	      = $designItem->subtitle;
        $this->Template->default_subtitle = $bookItem->getDefaultSubtitle();
        $this->Template->teaser           = $this->step4_teaser;
        $this->Template->decocover_id     = $bookItem->getDecocover();
        $this->Template->color_id         = $bookItem->covercolor_id;
        $this->Template->version_id       = $this->Template->version->id;
        $this->Template->config 		  = $this->config;
        if($designItem->item_image1_present > 0)
        {
            $this->Template->isImageCached  = true;
            $this->Template->book_item_id   = $bookItem->id;
        }
        
        $widmungitem  = $bookItem->getWidmungItem();
        $widmungprice = $widmungitem->getPrice();
        $designPrice  = $designItem->getPrice();
        
        $this->Template->designPrice    = $designPrice;
        //$this->Template->totalPrice     = $bookItem->getPrice() + $widmungprice + $designPrice;
        $this->Template->totalPrice     = $bookItem->getTotalPrice();
	}
	
	function step5()
	{
	    $bookItem = new PnBookItem();
	    $bookItem->initWithId($_GET["bookitem"]);
	    
	    $font = new PnFont();
        $this->Template->fonts      = $font->all();
        $this->Template->font_id    = $bookItem->font_id;
        //$this->Template->totalPrice = $bookItem->getPrice() + $bookItem->getWidmungItem()->getPrice() + $bookItem->getDesignItem()->getPrice();
        $this->Template->teaser     = $this->step5_teaser;
        $this->Template->totalPrice = $bookItem->getTotalPrice();
	}
	
	function step6()
	{
	    $bookItem = new PnBookItem();
	    $bookItem->initWithId($_GET["bookitem"]);
	    
	    //Load cached addons
        $article = new PnArticle();
        $article->initWithId($bookItem->article_id);
        $addon_cached = array();
        $addon_pieces = array();
        foreach($bookItem->getAddonItems() as $addon)
        {
            $addon_cached[] = $addon->article_id;
            $addon_pieces[$addon->article_id] = $addon->item_pieces;
            
        }
        
        $book = new PnBook();
        $book->initWithId($bookItem->book_id);
        $this->Template->book = $book;
        
        $this->Template->addon_cached 	= $addon_cached;
        $this->Template->addon_pieces 	= $addon_pieces;
        $this->Template->addons 		= $article->getAddons();
        $this->Template->order_id 		= $bookItem->parent_id;
        $this->Template->config 		= $this->config;
        $this->Template->book_id 		= $bookItem->book_id;
        $this->Template->decocover_id   = $bookItem->getDecocover();
        $this->Template->version_id     = $bookItem->getBookVersion()->id;
        
        $childPrice = 0;
        foreach($bookItem->getBookLockUpgradeItems() as $child)
        {
            $childPrice += $child->getPrice();
        }
        //$this->Template->totalPrice= $bookItem->getPrice() + $bookItem->getWidmungItem()->getPrice() + $bookItem->getDesignItem()->getPrice() + $childPrice;
        $this->Template->teaser         = $this->step6_teaser;
        $this->Template->totalPrice     = $bookItem->getTotalPrice();
	}
	
	function step_coverimage()
	{
	    $designItem   = new PnDesignItem();
	    $designItem->initWithId($_GET["designitem"]);
	    
	    $bookItem     = new PnBookItem();
	    $bookItem->initWithId($designItem->parent_id);
	    
	    $book         = new PnBook();
	    $book->initWithId($bookItem->book_id);
	    
	    $bookVersions = $book->getBookVersions();
        $version      = new PnVersion();
        if(count($bookVersions) > 0)
        {
            $version = $bookVersions[0];
        }

        $preview_dpi        = 72;
        $shadow             = 3;
        $page_w             = mmtopx($version->width, $preview_dpi) - 1;
        $page_h             = mmtopx($version->height, $preview_dpi) - 1;
        $coverpic_w         = cmtopx($version->picwidth, $preview_dpi);
        $coverpic_h         = cmtopx($version->picheight, $preview_dpi);
        
        $mask_x = $mask_y   = 0;
        $mask_w             = $coverpic_w - 2;
        $mask_h             = $coverpic_h + 2;
        $preview_overflow   = 0;
        
        if($book->type_id == 2)
        {
            $mask_x = 102;
            $mask_y = 153;
            $mask_w = $coverpic_w * 1.135;
            $mask_h = $coverpic_h * 1.155;
            $preview_overflow = 50;
        }
        
        if($book->type_id == 11)
        {
            $mask_x = 52;
            $mask_y = 184;
            $mask_w = $coverpic_w * 1.54;
            $mask_h = $coverpic_h * 1.54;
            $prever_overflow = 60;
        }
       
        $this->Template->color_background_id     = $bookItem->getDecocover();
        
        $this->Template->vcover_w   = $page_w + $shadow;
        $this->Template->vcover_h   = $page_h + $shadow;
        $this->Template->mask_w     = $mask_w;
        $this->Template->mask_h     = $mask_h;
        $this->Template->mask_x     = $mask_x;
        $this->Template->mask_y     = $mask_y;
        $this->Template->aspect     = floatval($version->picwidth / $version->picheight);
        $this->Template->version_id = $version->id;
        $this->Template->article_id = $designItem->article_id;
        $this->Template->config     = $this->config;
        $this->Template->book       = $book;
        
        if($designItem->item_image1_present > 0)
        {
            $upload_dir_frontend = $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] 
                                ? $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] 
                                : "/var/www/Personalnovel/htdocs/tl_files/customer-images/";
                                
            $preview_folder_cache = "tl_files/customer-images/";
            $this->Template->isImageCached  = true;
            if(file_exists($upload_dir_frontend . $_GET["order"] . "-" . $designItem->id . ".png.org.png"))
            {
                $imageCached = $preview_folder_cache . $_GET["order"] . "-" . $designItem->id . ".png.org.png";
            }
            else if(file_exists($upload_dir_frontend . $_GET["order"] . "-" . $designItem->id . ".gif.org.gif"))
            {
                $imageCached = $preview_folder_cache . $_GET["order"] . "-" . $designItem->id . ".gif.org.gif";
            }
            else
            {
                $imageCached = $preview_folder_cache . $_GET["order"] . "-" . $designItem->id . ".jpg.org.jpg";
            }
            $this->Template->imageCached    = $imageCached;
        }
        
	}
	
	function step_extra()
    {
        $bookItem = new PnBookItem();
        $bookItem->initWithId($_GET["bookitem"]);
        
        $book = new PnBook();
        $book->initWithId($bookItem->book_id);
        
        $buyAlready = false;
        $buyChildren = $bookItem->getBookLockUpgradeItems();
        
        if(count($buyChildren) > 0)
            $buyAlready = true;
        
        $bookArticle = new PnArticle();
        $bookArticle->initWithId($bookItem->article_id);
        $children = $bookArticle->getBookblockUpgrade();
        
        $childPrice = 0;
        foreach($buyChildren as $child)
        {
            $childPrice += $child->getPrice();
        }
        
        $this->Template->book        = $book;
        $this->Template->config      = $this->config;
        $this->Template->buyAlready  = $buyAlready;
        $this->Template->buyChildren = $buyChildren;
        $this->Template->children    = $children;
        //$this->Template->totalPrice= $bookItem->getPrice() + $bookItem->getWidmungItem()->getPrice() + $bookItem->getDesignItem()->getPrice() + $childPrice;
        $this->Template->totalPrice  = $bookItem->getTotalPrice();
    }
    
    function step_exlibris()
    {
        $bookItem = new PnBookItem();
        $bookItem->initWithId($_GET["bookitem"]);
        
        $child = new PnItem();
        $child->initWithId($_GET["childitem"]);
        
        $asset = new PnAsset();
        $this->Template->exlibrisAssets = $asset->getExlibrisAssets();
        
        $buyChildren = $bookItem->getBookLockUpgradeItems();
        $buyAsset = "";
        $buyColor = "black";
        $buyText = "";
        
        $objDb = $this->Database->prepare("
                    SELECT * FROM " . $this->config->database . ".items_assets
                    WHERE item_id = ?
                ")->execute($_GET["childitem"]);
                
        while($objDb->next())
        {
            $buyAsset = $objDb->asset_id;
        }
        
        $fields = json_decode($child->fields);
        $buyColor = $fields->color;
        $buyText = $fields->text;
        
        $dimens = array(
               '1205' => array(
                               'w' => '5,8',
                               'h' => '9',
                               'linelen' => '17',
                               'maxchars' => '51'
                               ),
               '1568' => array(
                               'w' => '5,8',
                               'h' => '5,8',
                               'linelen' => '14',
                               'maxchars' => '28'
                               ),
               '1569' => array(
                               'w' => '5,8',
                               'h' => '7,5',
                               'linelen' => '14',
                               'maxchars' => '28'
                               )
        );
        
        $childPrice = 0;
        foreach($bookItem->getBookLockUpgradeItems() as $child)
        {
            $childPrice += $child->getPrice();
        }
        
        $book = new PnBook();
        $book->initWithId($bookItem->book_id);
        $this->Template->book = $book;
        
        $this->Template->dimens     = $dimens;
        $this->Template->buyAsset   = $buyAsset;
        $this->Template->buyText    = $buyText;
        $this->Template->buyColor   = $buyColor;
        //$this->Template->totalPrice= $bookItem->getPrice() + $bookItem->getWidmungItem()->getPrice() + $bookItem->getDesignItem()->getPrice() + $childPrice;
        $this->Template->totalPrice = $bookItem->getTotalPrice();
    }
	
	function getOrCreateOrder()
	{
	    $order = new PnOrder();
	    
	    if($_GET['order'])
	    {
	        $order->initWithId($_GET['order']);
	        
	        //Error: find no order
	        if($order->id == "" || $order->id == 0)
	        {
	            gotoErrorPage();
	        }
	    } 
	    else if($_COOKIE['PN_order_id'] != '')
		{
			$order->initWithId($_COOKIE['PN_order_id']);
			if(count($order->getItems()) > 0){
				$bookItems   = $order->getBookItems();
				if(count($bookItems) > 0){
					foreach($bookItems as $bookItem){
						if($bookItem->item_paper == 99){
							header(sprintf("Location: /%s", $this->redirectJumpTo($this->jumpTo)));
							exit();
						}
					}
				}
				$wineItems   = $order->getWineItems();
				if(count($wineItems) > 0){
					foreach($wineItems as $wineItem){
						if($wineItem->item_paper == 99){
							header(sprintf("Location: /%s", $this->redirectJumpTo($this->jumpTo)));
							exit();
						}
					}
				}
			}
		}
		
		if($order->id == "" || $order->id == 0)
		{
		    $id = $order->add(
		                    array(
		                        "order_status"    => 0,
                                "order_date"      => date('Y/m/d H:i:s a', time()),
                                "order_created"   => date('Y/m/d H:i:s a', time()),
                                "order_updated"   => date('Y/m/d H:i:s a', time()),
                                "order_converted" => 1,
                                "order_version"   => 4,
                                "order_invoice"   => 1,
                                "locale_id"       => 1,
                                "mandant_id"      => 1,
                                "konto_id"        => $order->createKonto()->id,
                                "debitor_id"      => 1,
                                "partner_id"      => 1,
		                    	"payment_method_id"      => 1
                            )
						);
		    
		    $order->initWithId($id);
		    $order->edit(array("konto_id" => $order->createKonto()->id));
		    $order->createRandomKey();
		    
		    setcookie('PN_order_id', $order->id, time() + (86400 * 7), "/",$this->domain);
		}
		
		return $order;
	}
	
	function createDefaultItem($order, $book)
	{
	    $bookItem     = $this->createBookItem($order, $book);
	    $widmungItem  = $this->createWidmungItem($order, $bookItem);
	    $designItem   = $this->createDesignItem($order, $bookItem);
	}
	
	function createBookItem($order, $book)
	{
	    $fields = array();
	    $invalidChars = array();
	    $emptyFields  = array();
        foreach($_POST as $k => $v)
        {
            if($this->config->startsWith($k, 'vars-'))
            {
                if(is_array($v))
                {
                    foreach($v as $k1 => $v1)
                    {
                        $k1 = "[" . $k1 . "]";
                        $fields[utf8_decode(utf8_encode($k1))] = trim(utf8_decode(utf8_encode($v1)));
                        /*$invalidChar = getErrorCharacter($v1);
                        $invalidChars["vars-" . $k] = implode(", ", $invalidChar);*/
                    }
                }
                else
                {
                    $k = str_replace('vars-', '', $k);
                    $fields[utf8_decode(utf8_encode($k))] = trim(utf8_decode(utf8_encode($v)));
                    
                    $invalidChar = getErrorCharacter($v);
                    if(count($invalidChar) > 0)
                        $invalidChars[$k] = implode(", ", $invalidChar);
                    
                    if(trim($v) == "")
                        $emptyFields[$k] = "vars-" . $k;
                }
            }
        }
        
        if(count($invalidChars) > 0 || count($emptyFields) > 0)
        {
            $this->Template->emptyFields  = $emptyFields;
            $this->Template->invalidChars = $invalidChars;
            return false;
        }
        
        $bookDocuments = $book->getBookDocuments();
        if(count($bookDocuments) > 0)
        {
            $bookDocument = $bookDocuments[0];
            $bookDocumentId = $bookDocument->id;
            
            if($_POST["document"])
            {
                foreach($bookDocuments as $doc)
                {
                    if($_POST["document"] == $doc->id)
                        $bookDocumentId = $_POST["document"];
                }
            }
        }
        
        $class_id = 2;
        
        if($_GET["ebook"])
            $class_id = 21;
        
        $fields   = json_encode($fields);
        $bookItem = new PnBookItem();            
	    $book_id  = $bookItem->add(
	                      array(
                                    'order_id' 			=> $order->id,
                                    'buch_id'			=> $book->id,
                                    'class_id'			=> $class_id,
                                    'artikel_id'		=> $book->article_id,
                                    'font_id'			=> 1,
                                    'document_id'       => $bookDocumentId,
                                    'item_pieces'		=> 1,
                                    'item_print'		=> 0,
                                    'item_testprint'	=> 0,
                                    'item_fields'		=> $fields,
                                    'item_created'      => date('Y/m/d H:i:s a', time()),
                                    'item_updated'      => date('Y/m/d H:i:s a', time()),
                                    'item_touched'      => date('Y/m/d H:i:s a', time())
                              )
                      );
        
        $bookItem->initWithId($book_id);
        $bookItem->edit(array("item_fields" => $fields)); //Fix bug encoding. Don't know why?
        
        return $bookItem;
	}
	
	function createWidmungItem($order, $bookItem)
	{
	    $item     = new PnItem();
	    $item_id  = $item->add(
                       array(
                           "order_id"       => $order->id,
                           "class_id"       => 10,
                           "artikel_id"     => 8830,
                           "parent_id"      => $bookItem->id,
                           "item_pieces"    => 1,
                           "item_print"		=> 0,
                           "item_testprint"	=> 0,
                           "item_fields"	=> '{"text": "", "fontface": "Century Schoolbook L", "fontcolor": "black"}',
                           "item_created"   => date('Y/m/d H:i:s a', time()),
                           "item_updated"   => date('Y/m/d H:i:s a', time()),
                           "item_touched"   => date('Y/m/d H:i:s a', time())
                       )
	               );
	    
	    $item->initWithId($item_id);
	    return $item;
	}
	
	function createDesignItem($order, $bookItem)
	{
	    $book    = new PnBook();
	    $designs = $book->getBookDesigns($bookItem->article_id);
	    
	    if(count($designs) > 0)
	        $design  = $designs[0];
	    else
	        gotoErrorPage();
	    
	    $item    = new PnItem();
	    $field   = array(
	            
	        );
	    $item_id = $item->add(
                        array(
                            "order_id"       => $order->id,
                            "class_id"       => 3,
                            "artikel_id"     => $design->artikel_id,
                            "parent_id"      => $bookItem->id,
                            "item_pieces"    => 1,
                            "item_print"	 => 0,
                            "item_testprint" => 0,
                            "item_fields"	 => '{}',
                            "item_created"   => date('Y/m/d H:i:s a', time()),
                            "item_updated"   => date('Y/m/d H:i:s a', time()),
                            "item_touched"   => date('Y/m/d H:i:s a', time())
                        )
                    );
	}
	
	function editBookItem()
	{
	    $fields = array();
        foreach($_POST as $k => $v)
        {
            if($this->config->startsWith($k, 'vars-'))
            {
                if(is_array($v))
                {
                    foreach($v as $k1 => $v1)
                    {
                        $k1 = "[" . $k1 . "]";
                        $fields[utf8_decode(utf8_encode($k1))] = utf8_decode(utf8_encode($v1));
                    }
                }
                else
                {
                    $k = str_replace('vars-', '', $k);
                    $fields[utf8_decode(utf8_encode($k))] = utf8_decode(utf8_encode($v));
                    
                    $invalidChar = getErrorCharacter($v);
                    if(count($invalidChar) > 0)
                        $invalidChars[$k] = implode(", ", $invalidChar);
                    
                    if(trim($v) == "")
                        $emptyFields[$k] = "vars-" . $k;
                }
            }
        }
        
        if(count($invalidChars) > 0 || count($emptyFields) > 0)
        {
            $this->Template->emptyFields  = $emptyFields;
            $this->Template->invalidChars = $invalidChars;
            return false;
        }
        
	    $item = new PnBookItem();
	    $item->initWithId($_GET["bookitem"]);
	    
	    $item_id = $item->edit(
                        array(
                            "item_fields"	 => json_encode($fields),
                            "item_updated"   => date('Y/m/d H:i:s a', time()),
                            "item_touched"   => date('Y/m/d H:i:s a', time())
                        )
                    );
        
        if($_POST["document"])
        {
            $item->edit(
                array(
                    "document_id" => $_POST["document"]
                    )
                );
        }
        
        $addons = $item->getAddonItems();
        if(count($addons) > 0)
        {
            foreach($addons as $addon)
            {
                $article = new PnArticle();
                $article->initWithId($addon->article_id);
                if($article->class_id == 5)
                {
                    $addon->edit(array("item_fields"	 => json_encode($fields)));
                }
            }
        }
        
        $item->initWithId($_GET["bookitem"]);
        return $item;
	}
	public function redirectJumpTo($jumpto)
	{
		$strRedirect = "/";
		if (strlen($jumpto))
		{
			$objNextPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=?")
										  ->limit(1)
										  ->execute($jumpto);
			if ($objNextPage->numRows)
			{
				$strRedirect = $this->generateFrontendUrl($objNextPage->fetchAssoc());
			}
		}
		return $strRedirect;
	}
	
	public function sendMail($arData,$subject,$step,$orderID){

        $objEmail = new Email();
        $objEmail->from = $GLOBALS['TL_ADMIN_EMAIL'];
        $objEmail->fromName = ($arData) ? $arData['username']:$GLOBALS['TL_ADMIN_NAME'];
        $objEmail->subject = $subject.'- Order:'.$orderID.'-'.$step;
        $msg = '<p>'.($arData) ? $arData['message'] : ''.'</p>';
        $msg .= '<p>';
        
        $objEmail->html = $msg;

        $objEmail->sendTo('technik@personalnovel.de');
        return true;
    }
}

?>