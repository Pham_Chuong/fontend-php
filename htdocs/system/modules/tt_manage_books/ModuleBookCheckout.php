<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once("class/class.pn.php");
require_once("class/class.pn.soaphandler.php");
require_once("class/paypal.class.php");
require_once("class/paypal.adaptive.class.php");
require_once("class/class.pn.amazon.php");

class ModuleBookCheckout extends Module
{
	protected $strTemplate = 'mod_book_checkout';
	public   $domain;
	public function generate()
	{
	    $this->config = new PnConfig();
	    
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### BOOK CHECKOUT ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
	
		if(TL_MODE == 'FE')
		{
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/checkout.css|screen';
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.lightbox-0.5.css|screen';
				
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery-ui-1.8.16.custom.min.js';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.lightbox-0.5.pack.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/checkout.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
	    $customize_page = $GLOBALS['TL_CONFIG']['customize_page'] ? $GLOBALS['TL_CONFIG']['customize_page'] : "/customize";
	    $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
	    $this->domain   = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
	    
	    $arStepR = array(   	"Warenkorb"	=>1,
	    						"Bestellen"	=>2,
	    						"Versand"	=>3,
	    						"Bestaetigen"=>4,
	    						"Bezahlen"	=>5,
	    						"Fertig"	=>6
	    				);
	    $arStepC = array(1=>1,2,3,4,5,6);
	   	$arStepV = array( 1=>	"Warenkorb",
	    						"Bestellen",
	    						"Versand",
	    						"Bestaetigen",
	    						"Bezahlen",
	    						"Fertig"
	    				);
	    $this->Template->arrayStepV = $arStepV;
	   
	    /* start step */
	    $step = $_GET['step'] ? $_GET['step'] : $_POST['step'];
	    $step = $step ? $step : 1;
	    
	    if($arStepR[$step]){
	    	$step	= $arStepR[$step];
	    }elseif($arStepC[$step]){
	    	$step	= $arStepC[$step];
	    }
	    
		$this->Template->step = $step;
		
		/*  */
		$post_order = $_POST['order'] ? $_POST['order'] : $_COOKIE['PN_order_id'];
		$order_id = $_GET['order'] ? $_GET['order'] : $post_order;
		
		$order = new PnOrder();
		$order->initWithId($order_id);
		$this->Template->orderid_CP = $order_id;
		/*
	     * goto MyOrderPage if order's done
	     */
	    if($order->id && $step != "changepayment")
	    {
	        $myorder_page = $GLOBALS['TL_CONFIG']['myorder_page'] ? $GLOBALS['TL_CONFIG']['myorder_page'] : "/myorder";
	        if($order->step == "done")
	        {
	            header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
	        }
	    }
		
		if($step == 1)
		{
		    checkUrlToken();
		    
			if($_GET['action'] == "remove")
			{
			    if($_GET['bookitem'])
			    {
                    $item_id = $_GET['bookitem'];
                    $bookItem = new PnBookItem();
                    $bookItem->initWithId($item_id);
                    if($bookItem->item_paper == 99){
                    	$this->removeAllItems($order);
                    }
                    else{
						$bookItem->removeChildItem();
						$bookItem->remove();
					}
				}
				
				if($_GET['wineitem'])
				{
				    $item_id = $_GET['wineitem'];
                    $wineItem = new PnWineItem();
                    $wineItem->initWithId($item_id);
                    if($wineItem->item_paper == 99){
                    	$this->removeAllItems($order);
                    }
                    else{
						$wineItem->removeChildItem();
						$wineItem->remove();
                    }
				}
				//if item = 0 -> remove cache PN_order_id go to personalnovel.de or cache choose urllogo detail.
				if(count($order->getTopLevelItems()) == 0){
					setcookie ("PN_order_id", "", time() - 3600,"/",$this->domain);
					$urllogo 	= $_COOKIE['urllogo'] ? $_COOKIE['urllogo'] : "";
					$url_arr 	= explode("|",$urllogo);
					$url_show   = $url_arr[0] ? $url_arr[0] : "/";
					header("Location: ".$url_show);
				}
				else
				{
				    
				    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[1], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[1]));
                    }
					
				}
			}
			
			if($_POST)
			{
				foreach($_POST as $k => $v)
				{
					if($this->config->startsWith($k, 'item_pieces_'))
					{
						$item_id = str_replace('item_pieces_', '', $k);
						$item = new PnItem();
						
						$item->select($item_id);
						$item->edit(array("item_pieces" => $v));
					}
				}
				
				if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($order->id);
                    $order->edit(array("order_step" => "order"));
                    header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[2], $token));
                }
                else
                {
                    $order->edit(array("order_step" => "order"));
                    header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[2]));
                }
				
			}
			
			//Get Order Items
			$this->Template->bookItems   = $order->getBookItems();
			$this->Template->wineItems   = $order->getWineItems();
			$this->Template->shipping_id = $order->shipping_id;
			if($order->shipping_id && $order->shipping_id > 0)
			{
			    $shipping = new PnShipping();
			    $shipping->initWithId($order->shipping_id);
			    $this->Template->shipping = $shipping;
			}
			$this->Template->totalPrice  = $order->getTotalPriceWithoutCoupon("", false); //Sum without shipping price
			$this->Template->order       = $order;
            
			if($_GET["step"]=='Bestellen'){
	            //sendmail         
	            $s ='';
	            $arData = array();
	            $s .= 'Date : '.date("F j, Y, g:i a") .'<br/>';
	            $s .='OrderID: '. $order->id.'<br/>';
	            $s .= 'Total Price :' .$order->getTotalPriceWithoutCoupon("", false).'<br/>';
	             
	            $arData['message'] = $s;
	            
	            $this->sendMail($arData,'Checkout: ','start checkout ',$order->id);
			}
		}
		
		if($step == 2)
		{
		    checkUrlToken();
		    if(count($order->getTopLevelItems()) == 0){
                setcookie ("PN_order_id", "", time() - 3600,"/",$this->domain);
                $urllogo 	= $_COOKIE['urllogo'] ? $_COOKIE['urllogo'] : "";
                $url_arr 	= explode("|",$urllogo);
                $url_show   = $url_arr[0] ? $url_arr[0] : "/";
                header("Location: ".$url_show);
            }
            
            //Allow Amazon checkout ?
            $this->Template->allow_amazon_payment = true;
            $num_of_ebooks = count($order->getEbookItems());
            $num_of_wines  = count($order->getWineItems());
            if($num_of_ebooks > 0 || $num_of_wines > 0)
            {
                $this->Template->allow_amazon_payment = false;
                
                if($order->payment_method_id == 12)
                {
                    $order->update(array('payment_method_id' => 1));
                    $order->payment_method_id = 1;
                }
            }
            
		    //Check payment amazon
		    if($order->payment_method_id == 12)
		    {
		        header(sprintf("Location: %s?order=%s&step=%s&session=%s", $checkout_page, $order->id, "amazon", $_SESSION['amazon_session']));
		    }
		    
		    //coupon black friday checkBlackFriday
		    $coupon_35 = '';
		    if($_COOKIE['black-Friday']){
				if($this->checkBlackFriday($order)){
					$coupon_35 = $GLOBALS['TL_CONFIG']['value_coupon_black_friday'];
				}
			}
		   $this->Template->Coupon_black = $coupon_35;
		   $this->Template->Coupon_black_true = $this->checkBlackFriday($order);
		   //-------------
		    if($_POST['coupons_used_validate'])
		    {
		        //Validate Coupon
			    if($_POST["coupons_used"])
			    {
			        $token = $_POST["coupons_used"];
			        $coupon= new PnCoupon();
			        if(!$coupon->checkExist($token, $order->id))
                    {
                        $error = true;
                        $this->Template->errorCoupon = "Es wurde leider kein Gutschein mit diesem Code gefunden.";
                    }
                    else
                    {
                        $coupon->initWithToken($token);
                        if($coupon->checkExpire())
                        {
                            $error = true;
                            $this->Template->errorCoupon = "Dieser Gutschein ist leider bereits abgelaufen.";
                        }
                        else if(!$coupon->checkActive())
                        {
                            $error = true;
                            $this->Template->errorCoupon = "Dieser Gutschein ist inaktiv.";
                        }
                        else
                        {
                            if($coupon->checkUsedUp($order->id))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein wurde leider bereits verwendet.";
                            }
                            else if(!$coupon->canUseForOrder($order))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                            else if(!$this->checkBlackFriday($order) && $token==$GLOBALS['TL_CONFIG']['value_coupon_black_friday']){
                            	$error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }elseif(!$coupon->checkArtikel()){
                            	    $error = true;
                            	    $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                        }
                    }
                    
                    if(!$this->Template->errorCoupon)
                        $this->Template->couponMsg = "Ihr Gutschein wurde akzeptiert!<br />Der Betrag wird auf der übernächsten Seite abgezogen.";
			    }
		    }
		    
			if($_POST['submitter'])
			{
			    $error = false;
			    
			    //Validate Coupon
			    if($_POST["coupons_used"])
			    {
			        $token = $_POST["coupons_used"];
			        $coupon= new PnCoupon();
			        if(!$coupon->checkExist($token, $order->id))
                    {
                        $error = true;
                        $this->Template->errorCoupon = "Es wurde leider kein Gutschein mit diesem Code gefunden.";
                    } 
                    else
                    {
                        $coupon->initWithToken($token);
                        if($coupon->checkExpire())
                        {
                            $error = true;
                            $this->Template->errorCoupon = "Dieser Gutschein ist leider bereits abgelaufen.";
                        }
                        else
                        {
                            if($coupon->checkUsedUp($order->id))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein wurde leider bereits verwendet.";
                            }
                            else if(!$coupon->canUseForOrder($order))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                            else if(!$this->checkBlackFriday($order) && $token==$GLOBALS['TL_CONFIG']['value_coupon_black_friday']){
                            	$error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                            else if(!$coupon->checkArtikel()){
                            	$error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                        }
                    }
                    
                    if(!$this->Template->errorCoupon)
                        $this->Template->couponMsg = "Ihr Gutschein wurde akzeptiert!<br />Der Betrag wird auf der übernächsten Seite abgezogen.";
			    }
			    else
			    {
			        
			    }
			    
			    //Validate Email
			    if(!$_POST['customer-customer_email'] || preg_match("/[.+a-zA-Z0-9_-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $_POST['customer-customer_email']) == 0)
			    {
			        $error = true;
			        $this->Template->errorEmail = true;
			        $this->Template->email = trim($_POST['customer-customer_email']);
			        $this->Template->email_repeat = trim($_POST['customer-customer_email_repeat']);
			    }
			    else
			    {
			        $this->Template->email = trim($_POST['customer-customer_email']);
			        $this->Template->email_repeat = trim($_POST['customer-customer_email_repeat']);
			        
			        if(trim($_POST['customer-customer_email']) != trim($_POST['customer-customer_email_repeat'])){
			        	$error = true;
				        $this->Template->errorEmailrepeat = true;
			        }

			    }

			 
			    //Validate Vorname
			    if(!$_POST['shipping_address-address_vorname'])
			    {
			        $error = true;
			        $this->Template->errorFirstname = true;
			    }
			    else
			    {
			        
			        $invalidFirstnameChars = getErrorCharacter($_POST['shipping_address-address_vorname']);
                    if(count($invalidFirstnameChars) > 0)
                    {
                        $error = true;
                        $this->Template->invalidFirstnameChars = implode(", ", $invalidFirstnameChars);
                    }
			    }
			    
			    //Validate Nachname
			    if(!$_POST['shipping_address-address_nachname'])
			    {
			        $error = true;
			        $this->Template->errorLastname = true;
			    }
			    else
			    {
			        $invalidLastnameChars = getErrorCharacter($_POST['shipping_address-address_nachname']);
                    if(count($invalidLastnameChars) > 0)
                    {
                        $error = true;
                        $this->Template->invalidLastnameChars = implode(", ", $invalidLastnameChars);
                    }
			    }
			    
			    //Validate Street
			    if(!$_POST['shipping_address-address_address'])
			    {
			        $error = true;
			        $this->Template->errorAddress = true;
			    }
			    else
			    {
			        $invalidStreetChars = getErrorCharacter($_POST['shipping_address-address_address']);
                    if(count($invalidStreetChars) > 0)
                    {
                        $error = true;
                        $this->Template->invalidStreetChars = implode(", ", $invalidStreetChars); 
                    }
			    }
			    
			    //Validate plz
			    if(!$_POST['shipping_address-address_zip'])
			    {
			        $error = true;
			        $this->Template->errorZip = true;
			    }
			    else
			    {
			        $invalidZipChars = getErrorCharacter($_POST['shipping_address-address_zip']);
                    if(count($invalidZipChars) > 0)
                    {
                        $error = true;
                        $this->Template->invalidZipChars = implode(", ", $invalidZipChars);
                    }
			    }
			    
			    //Validate ort
			    if(!$_POST['shipping_address-address_city'])
			    {
			        $error = true;
			        $this->Template->errorCity = true;
			    }
			    else
			    {
			        $invalidCityChars = getErrorCharacter($_POST["shipping_address-address_city"]);
                    if(count($invalidCityChars) > 0)
                    {
                        $error = true;
                        $this->Template->invalidCityChars = implode(", ", $invalidCityChars);
                    }
			    }
			    
			    //Validate company
			    if($_POST['shipping_address-address_firma'])
                {
                    $invalidCompanyChars = getErrorCharacter($_POST['shipping_address-address_firma']);
                    if(count($invalidCompanyChars) > 0)
                    {
                        $error = true;
                        $this->Template->invalidCompanyChars = implode(", ", $invalidCompanyChars);
                    }
                }
			    
			    //Validate land
			    if($_POST['shipping_address-country'] == "__None")
			    {
			        $error = true;
			        $this->Template->errorCountry = true;
			    }
			    else
			    {
			        if($_POST['shipping_address-address_zip'])
			        {
			            $zipstr = $_POST['shipping_address-address_zip'];
			            $zipreg = $GLOBALS['TL_CONFIG']['zipcode_pattern'];
			            $country = new PnCountry();
			            $country->initWithId($_POST['shipping_address-country']);
			            $pattern = $zipreg[$country->code];
			            
			            if(!$zipreg[$country->code])
			                $pattern = ".+";
			            
			            if(!preg_match("/^" . $pattern . "$/i", $zipstr))
			            {
			                $error = true;
			                $this->Template->invalidZipcode = true;
			            }
			        }
			        
			        $this->Template->country = $_POST['shipping_address-country'];
			    }
			    
			    /*
			     * Validate billing address
			     */
			    if(!$_POST["billing_address-shipping_is_billing_address_"])
			    {
			        //Validate Vorname
                    if(!$_POST['billing_address-address_vorname'])
                    {
                        $error = true;
                        $this->Template->errorBillingFirstname = true;
                    }
                    else
                    {
                        $invalidBillingFirstnameChars = getErrorCharacter($_POST['billing_address-address_vorname']);
                        if(count($invalidBillingFirstnameChars) > 0)
                        {
                            $error = true;
                            $this->Template->invalidBillingFirstnameChars = implode(", ", $invalidBillingFirstnameChars);
                        }
                    }
                    
                    //Validate Nachname
                    if(!$_POST['billing_address-address_nachname'])
                    {
                        $error = true;
                        $this->Template->errorBillingLastname = true;
                    }
                    else
                    {
                        $invalidBillingLastnameChars = getErrorCharacter($_POST['billing_address-address_nachname']);
                        if(count($invalidBillingLastnameChars) > 0)
                        {
                            $error = true;
                            $this->Template->invalidBillingLastnameChars = implode(", ", $invalidBillingLastnameChars);
                        }
                    }
                    
                    //Validate Street
                    if(!$_POST['billing_address-address_address'])
                    {
                        $error = true;
                        $this->Template->errorBillingAddress = true;
                    }
                    else
                    {
                        $invalidBillingStreetChars = getErrorCharacter($_POST['billing_address-address_address']);
                        if(count($invalidBillingStreetChars) > 0)
                        {
                            $error = true;
                            $this->Template->invalidBillingStreetChars = implode(", ", $invalidBillingStreetChars); 
                        }
                    }
                    
                    //Validate plz
                    if(!$_POST['billing_address-address_zip'])
                    {
                        $error = true;
                        $this->Template->errorBillingZip = true;
                    }
                    else
                    {
                        $invalidBillingZipChars = getErrorCharacter($_POST['billing_address-address_zip']);
                        if(count($invalidBillingZipChars) > 0)
                        {
                            $error = true;
                            $this->Template->invalidBillingZipChars = implode(", ", $invalidBillingZipChars);
                        }
                    }
                    
                    //Validate ort
                    if(!$_POST['billing_address-address_city'])
                    {
                        $error = true;
                        $this->Template->errorBillingCity = true;
                    }
                    else
                    {
                        $invalidBillingCityChars = getErrorCharacter($_POST["billing_address-address_city"]);
                        if(count($invalidBillingCityChars) > 0)
                        {
                            $error = true;
                            $this->Template->invalidBillingCityChars = implode(", ", $invalidBillingCityChars);
                        }
                    }
                    
                    //Validate company
                    if($_POST['billing_address-address_firma'])
                    {
                        $invalidBillingCompanyChars = getErrorCharacter($_POST['billing_address-address_firma']);
                        if(count($invalidBillingCompanyChars) > 0)
                        {
                            $error = true;
                            $this->Template->invalidBillingCompanyChars = implode(", ", $invalidBillingCompanyChars);
                        }
                    }
                    
                    //Validate land
                    if($_POST['billing_address-country'] == "__None")
                    {
                        $error = true;
                        $this->Template->errorBillingCountry = true;
                    }
                    else
                    {
                        if($_POST['billing_address-address_zip'])
                        {
                            $zipstr = $_POST['billing_address-address_zip'];
                            $zipreg = $GLOBALS['TL_CONFIG']['zipcode_pattern'];
                            $country = new PnCountry();
                            $country->initWithId($_POST['billing_address-country']);
                            $pattern = $zipreg[$country->code];
                            
                            if(!$zipreg[$country->code])
                                $pattern = ".+";
                            
                            if(!preg_match("/^" . $pattern . "$/i", $zipstr))
                            {
                                $error = true;
                                $this->Template->invalidBillingZipcode = true;
                            }
                        }
                    }
                    
			    }
			    
			    if(!$error)
			    {
			    	if($_POST['payment_method'] && is_numeric($_POST['payment_method']))
			        	$order->update(array('payment_method_id' => $_POST['payment_method']));
			        $email 			= trim($_POST['customer-customer_email']);
					$anrede 		= $_POST['shipping_address-address_anrede'];
					$firstName 		= utf8_decode(utf8_encode($_POST['shipping_address-address_vorname']));
					$lastName 		= utf8_decode(utf8_encode($_POST['shipping_address-address_nachname']));
					$company 		= utf8_decode(utf8_encode($_POST['shipping_address-address_firma']));
					$address 		= utf8_decode(utf8_encode($_POST['shipping_address-address_address']));
					$zip 			= $_POST['shipping_address-address_zip'];
					$city 			= utf8_decode(utf8_encode($_POST['shipping_address-address_city']));
					$country_id 	= $_POST['shipping_address-country'];
					
					$customer = new PnCustomer();
					if($order->customer_id != '' && $order->customer_id > 0)
					{
						$customer_id = $order->customer_id;
					}
					else
					{
						if($customer->existEmail($email))
						{
							$customer_id = $customer->id;
						}
						else
						{
						    $customer_vorname  = !$_POST["billing_address-shipping_is_billing_address_"] ? $_POST['billing_address-address_vorname'] : $firstName;
						    $customer_nachname = !$_POST["billing_address-shipping_is_billing_address_"] ? $_POST['billing_address-address_nachname'] : $lastName;
						    $anrede            = !$_POST["billing_address-shipping_is_billing_address_"] ? $_POST['billing_address-address_anrede'] : $anrede;
						    
							$customer_id = $customer->add(array(
									"customer_vorname" 	=> $customer_vorname,
									"customer_nachname"	=> $customer_nachname,
									"customer_email" 	=> $email,
									"customer_anrede" 	=> $anrede
								));
						}
					}
					
					$objAddress = new PnAddress();
					if($order->shipping_address_id != '' && $order->shipping_address_id > 0)
					{
						//edit
						$shipping = new PnAddress();
						$shipping->initWithId($order->shipping_address_id);
						
						$shipping->edit(array(
								"address_vorname" 		=> $firstName,
								"address_nachname" 		=> $lastName,
								"address_firma" 		=> $company,
								"address_address" 		=> $address,
								"address_city" 			=> $city,
								"address_zip" 			=> $zip,
								"country_id" 			=> $country_id,
								"address_anrede" 		=> $_POST['shipping_address-address_anrede']
							));
						
						if($_POST["billing_address-shipping_is_billing_address_"])
						{
						    $order->update(array(
						        "billing_address_id"	=> $order->shipping_address_id
							));
						}
						else
						{
						    $objBillingAddress = new PnAddress();
						    if($order->billing_address_id && $order->billing_address_id > 0 
						        && $order->billing_address_id != $order->shipping_address_id)
						    {
						        $objBillingAddress->initWithId($order->billing_address_id);
						        $objBillingAddress->edit(array(
                                    "address_vorname" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_vorname"])),
                                    "address_nachname" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_nachname"])),
                                    "address_firma" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_firma"])),
                                    "address_address" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_address"])),
                                    "address_city" 			=> utf8_decode(utf8_encode($_POST["billing_address-address_city"])),
                                    "address_zip" 			=> $_POST["billing_address-address_zip"],
                                    "country_id" 			=> $_POST["billing_address-country"],
                                    "address_anrede" 		=> $_POST["billing_address-address_anrede"]
                                ));
						    }
						    else
						    {
                                $billing_id = $objBillingAddress->add(array(
                                    "address_vorname" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_vorname"])),
                                    "address_nachname" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_nachname"])),
                                    "address_firma" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_firma"])),
                                    "address_address" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_address"])),
                                    "address_city" 			=> utf8_decode(utf8_encode($_POST["billing_address-address_city"])),
                                    "address_zip" 			=> $_POST["billing_address-address_zip"],
                                    "country_id" 			=> $_POST["billing_address-country"],
                                    "address_anrede" 		=> $_POST["billing_address-address_anrede"]
                                ));
                                
                                $order->update(array(
                                    "billing_address_id"	=> $billing_id
                                ));
                            }
						}
					}
					else
					{
						//add
						$shipping_id = $objAddress->add(array(
								"address_vorname" 		=> $firstName,
								"address_nachname" 		=> $lastName,
								"address_firma" 		=> $company,
								"address_address" 		=> $address,
								"address_city" 			=> $city,
								"address_zip" 			=> $zip,
								"country_id" 			=> $country_id,
								"address_anrede" 		=> $_POST['shipping_address-address_anrede']
							));
						
						if($_POST["billing_address-shipping_is_billing_address_"])
						{
						    $billing_id = $shipping_id;
						}
						else
						{
						    $objBillingAddress = new PnAddress();
						    $billing_id = $objBillingAddress->add(array(
						        "address_vorname" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_vorname"])),
								"address_nachname" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_nachname"])),
								"address_firma" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_firma"])),
								"address_address" 		=> utf8_decode(utf8_encode($_POST["billing_address-address_address"])),
								"address_city" 			=> utf8_decode(utf8_encode($_POST["billing_address-address_city"])),
								"address_zip" 			=> $_POST["billing_address-address_zip"],
								"country_id" 			=> $_POST["billing_address-country"],
								"address_anrede" 		=> $_POST["billing_address-address_anrede"]
                            ));
						}
						
						$order->update(array(
							"shipping_address_id" 	=> $shipping_id,
							"billing_address_id"	=> $billing_id,
							"customer_id" 			=> $customer_id
						));
						
					}
					
					/*
					 * prepare shipping options
					 */
                    $shipOptions = array();
                    $shippingOption = new PnShipping();
                    $shippingAddr = new PnAddress();
                    $shippingAddr->initWithId($shipping_id);
                    $country_id = $_POST['shipping_address-country'];
                    foreach($shippingOption->get($order, $country_id) as $shipOpt)
                    {
                        $shipOptions[] = $shipOpt->id;
                    }
                    
                    if(!in_array($order->shipping_id, $shipOptions))
                    {
                        $order->update(array("shipping_id" => $shipOptions[0]));
                    }
                    
                    $num_of_books = count($order->getBookItems(true));
                    $num_of_wines = count($order->getWineItems());
                    
                    if($num_of_books == 0 && $num_of_wines == 0)
                    {
                        $order->update(array("shipping_id" => 11));
                    }
                    
                    // if only Wine, set shipping method : Selbstabholung, Abholung im PersonalNOVEL Showroom, München
			    	if($num_of_books == 0 && $num_of_wines > 0)
                    {
                        $order->update(array("shipping_id" => 21));
                    }
                    
                    $order->update(array("order_found" => $_POST["order_found"] ));
                    
                    //use coupon
                    if($_POST["coupons_used"])
                    {
                        $token  = $_POST["coupons_used"];
                        $coupon = new PnCoupon();
                        $coupon->initWithToken($token);
                        $coupon->useForOrder($order->id);
                    }
                    else
                    {
                        $objDb = $this->Database->prepare("
                            SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                            WHERE order_id = ?
                        ")->execute($order->id);
                        
                        if($objDb->numRows > 0)
                        {
                            $objDb = $this->Database->prepare("
                                DELETE FROM " . $this->config->database . ".coupons_used_on_orders
                                WHERE order_id = ?
                            ")->execute($order->id);
                            
                            $order->recalc();
                        }
                    }
                    
                    //Trust shop
                    $objDb = $this->Database->prepare("
                        SELECT * FROM " . $this->config->database . ".items
                        WHERE class_id = 6 AND order_id = ?
                    ")->execute($order->id);
                    
                    if($objDb->numRows > 0)
                    {
                        if(!$_POST["protection_yes"])
                        {
                            //delete
                            $this->Database->prepare("
                                DELETE FROM " . $this->config->database . ".items
                                WHERE item_id = ?
                            ")->execute($objDb->first()->item_id);
                            
                            $this->Database->prepare("
                                DELETE FROM " . $this->config->database . ".protections
                                WHERE order_id = ?
                            ")->execute($order->id);
                        }
                    }
                    else if($_POST["protection_yes"])
                    {
                        //Get trust shop article
                        //Fix me
                        $tsItem = new PnItem();
                        $tsItem->add(
                            array(
                                    "order_id"       => $order->id,
                                    "class_id"       => 6,
                                    "artikel_id"     => 8826,
                                    "item_pieces"    => 1,
                                    "item_print"	 => 0,
                                    "item_testprint" => 0,
                                    "item_created"   => date('Y/m/d H:i:s a', time()),
                                    "item_updated"   => date('Y/m/d H:i:s a', time()),
                                    "item_touched"   => date('Y/m/d H:i:s a', time())
                                )
                        );
                        
                        $objTrustshop = $this->Database->prepare("
                                INSERT INTO " . $this->config->database . ".protections(order_id, artikel_id)
                                VALUES (?, ?)
                            ")->execute($order->id, 8826);
                    }
                    
                    //Order Invoice
                    if($_POST["order_invoice"])
                    {
                        $order->edit(array("order_invoice" => 0));
                    }
                    else
                    {
                        $order->edit(array("order_invoice" => 1));
                    }
                    
                    //Newsletter
                    $objDbNewsletter = $this->Database->prepare("
                            SELECT * FROM " . $this->config->database . ".newsletter
                            WHERE news_email = ? 
                    ")->execute($email);
                    
                    if($_POST["customer-newsletter"])
                    {
                        if($objDbNewsletter->numRows > 0)
                        {
                            $this->Database->prepare("
                                UPDATE " . $this->config->database . ".newsletter
                                SET news_unsubscribed = NULL
                                WHERE news_email = ?
                            ")->execute($email);
                        }
                        else
                        {
                            $this->Database->prepare("
                                INSERT INTO " . $this->config->database . ".newsletter
                                (news_id, customer_id, news_email, news_submitted, news_confirmed)
                                VALUES (?, ?, ?, ?, ?)
                            ")->execute("", $order->customer_id, $email, date('Y/m/d H:i:s a', time()), date('Y/m/d H:i:s a', time()));
                        }
                    }
                    else
                    {
                        if($objDbNewsletter->numRows > 0)
                        {
                            $this->Database->prepare("
                                UPDATE " . $this->config->database . ".newsletter
                                SET news_unsubscribed = ?
                                WHERE news_email = ?
                            ")->execute(date('Y/m/d H:i:s a', time()), $email);
                        }
                    }
                    if($GLOBALS['TL_CONFIG']['Counpon_Free'] || $this->checkBlackFriday($order)){
                        //Check coupon
                        if($order->hasCoupon() && $order->getNumOfBookItems() > 0)
                        {
                            //Check giftbox exist
                            if(!$order->hasGiftbox())
                            {
                                if($order->hasCouponNotGiftboxFree())
                                {
                                    $item = new PnItem();
                                    $parent_id = $item->add(array(
                                        "artikel_id" 	=> $order->coupon_item_id,
                                        "order_id" 		=> $order->id,
                                        "item_pieces" 	=> 1,
                                        "class_id" 		=> 4
                                    ));
                                    $item->editBox($parent_id);
                                }
                            }
                            else
                            {
                                $objDb = $this->Database->prepare("
                                        DELETE FROM " . $this->config->database . ".items
                                        WHERE order_id = ?
                                        AND class_id = 4
                                        AND artikel_id = ?
                                    ")->execute($order->id, $order->coupon_item_id);
                            }
                        }
                        else
                        {
                            if(!$order->hasCouponNotGiftboxFree())
                            {
                                //remove cache
                                $objDb = $this->Database->prepare("
                                        DELETE FROM " . $this->config->database . ".items
                                        WHERE order_id = ?
                                        AND class_id = 4
                                        AND artikel_id = ?
                                    ")->execute($order->id, $order->coupon_item_id);
                            }
                        }
                    }
                    
                    if($num_of_books == 0 & $num_of_wines == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            $order->edit(array("order_step" => "confirm"));
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[4], $token));
                        }
                        else
                        {
                            $order->edit(array("order_step" => "confirm"));
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[4]));
                        }
                    }
                    else
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            $order->edit(array("order_step" => "shipping"));
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[3], $token));
                        }
                        else
                        {
                            $order->edit(array("order_step" => "shipping"));
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[3]));
                        }
                    }
			    }
			}
			
			if($order->customer_id && $order->customer_id > 0)
			{
			    $customer = new PnCustomer();
			    $customer->initWithId($order->customer_id);
			    $this->Template->email = $customer->email;
			}
			
			//Load cached info
            if($order->shipping_address_id && $order->shipping_address_id > 0)
            {
                $cached_shipping = new PnAddress();
                $cached_shipping->initWithId($order->shipping_address_id);
                
                $this->Template->shipping_address_id = $cached_shipping->id;
                $this->Template->firstName           = $cached_shipping->firstName;
                $this->Template->lastName            = $cached_shipping->lastName;
                $this->Template->firma               = $cached_shipping->company;
                $this->Template->address             = $cached_shipping->address;
                $this->Template->address2            = $cached_shipping->address2;
                $this->Template->city                = $cached_shipping->city;
                $this->Template->zip                 = $cached_shipping->zip;
                $this->Template->phone               = $cached_shipping->phone;
                $this->Template->fax                 = $cached_shipping->fax;
                $this->Template->country_id          = $cached_shipping->country_id;
                $this->Template->anrede              = $cached_shipping->anrede;
                
                $cached_billing = new PnAddress();
                $cached_billing->initWithId($order->billing_address_id);
                
                $this->Template->billing_address_id = $cached_billing->id;
                $this->Template->billing_firstName  = $cached_billing->firstName;
                $this->Template->billing_lastName   = $cached_billing->lastName;
                $this->Template->billing_firma      = $cached_billing->company;
                $this->Template->billing_address    = $cached_billing->address;
                $this->Template->billing_address2   = $cached_billing->address2;
                $this->Template->billing_city       = $cached_billing->city;
                $this->Template->billing_zip        = $cached_billing->zip;
                $this->Template->billing_phone      = $cached_billing->phone;
                $this->Template->billing_fax        = $cached_billing->fax;
                $this->Template->billing_country_id = $cached_billing->country_id;
                $this->Template->billing_anrede     = $cached_billing->anrede;
                
                $coupon = new PnCoupon();
                $objCoupon = $this->Database->prepare("
                        SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                        WHERE order_id = ?
                    ")->execute($order->id);
                while($objCoupon->next())
                {
                    $coupon_id = $objCoupon->coupon_id;
                }
                if($coupon_id > 0)
                {
                    $coupon->initWithId($coupon_id);
                    $this->Template->couponCode = $coupon->token;
                    $couponValue = $coupon->wert;
                    if($coupon->coupontype_id == 3){
						$couponValue = $coupon->wert + $coupon->wert2;
					}
                    $orderPriceWithoutCoupon = $order->getTotalPriceWithoutCoupon("de", true);

                    if($coupon->prozent > 0)
                    {
                        $couponValue = round($orderPriceWithoutCoupon * min(100, max(0, $coupon->prozent)) / 100, 2);
                    }
                    
                    if($couponValue > $orderPriceWithoutCoupon)
                        $couponValue = $orderPriceWithoutCoupon;
                    
                    $this->Template->couponToken = $coupon->token;
                    $this->Template->couponValue = -1 * floatVal($couponValue);
                    if(!$this->Template->errorCoupon)
                        $this->Template->couponMsg = "Ihr Gutschein wurde akzeptiert!<br />Der Betrag wird auf der übernächsten Seite abgezogen.";
                }
            }
            
            $objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".items
                WHERE class_id = 6 AND order_id = ?
            ")->execute($order->id);
            
            if($objDb->numRows > 0)
            {
                $this->Template->trustshop      = true;
                $this->Template->trustshopName  = "Käuferschutz Trusted Shops bis 500 Euro";
                $this->Template->trustshopPrice = 0.98;
            }
            
            if(!$order->invoice || $order->invoice == 0)
            {
                $this->Template->invoice   = true;
            }
            
            $objNewsletter = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".newsletter
                WHERE news_email = ? AND news_unsubscribed IS NULL
            ")->execute($customer->email);
            
            if($objNewsletter->numRows > 0)
                $this->Template->newsletter = true;
			
			$payment = new PnPayment();
			$this->Template->bookItems     = $order->getBookItems();
			$this->Template->payments      = $payment->getPayments();
			$this->Template->payment_id    = $order->payment_method_id;
			$this->Template->totalPrice    = $order->getTotalPriceWithoutCoupon("", false);
			$this->Template->wineItems     = $order->getWineItems();
			$this->Template->shippingPrice = $order->getShippingPrice();
			$this->Template->order_found   = $order->found;
			$this->Template->taxPrice      = $order->getTaxPrice();
			if($GLOBALS['TL_CONFIG']['Counpon_Free'] || $this->checkBlackFriday($order)){
				if($order->hasCoupon() && $order->getNumOfBookItems() > 0)
				{
				    if($order->hasGiftbox())
				    {
					$this->Template->couponGiftboxName  = $order->coupon_name_de;
					$this->Template->couponGiftboxPrice = $order->coupon_prices_de * -1;
				    }
				}
			}
		}
		
		if($step == 3)
		{
		    checkUrlToken();
		    if(count($order->getTopLevelItems()) == 0){
                setcookie ("PN_order_id", "", time() - 3600,"/",$this->domain);
                $urllogo 	= $_COOKIE['urllogo'] ? $_COOKIE['urllogo'] : "";
                $url_arr 	= explode("|",$urllogo);
                $url_show   = $url_arr[0] ? $url_arr[0] : "/";
                header("Location: ".$url_show);
            }
            
			if($_POST['shipping'])
			{
				$order->update(array("shipping_id" => $_POST['shipping']));
				
				if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($order->id);
                    $order->edit(array("order_step" => "confirm"));
                    header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[4], $token));
                }
                else
                {
                    $order->edit(array("order_step" => "confirm"));
                    header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[4]));
                }
				
			}
			
			$shipping 	                = new PnShipping();
			
			$this->Template->shipping   = $order->shipping_id;
			$this->Template->totalPrice = $order->getTotalPrice("", false);
			$this->Template->order      = $order;
			
			$address = new PnAddress();
			$address->initWithId($order->shipping_address_id);
			$country = new PnCountry();
			$country->initWithId($address->country_id);
			
			$this->Template->shippings       = $shipping->get($order, $address->country_id);
			$this->Template->shippingCountry = $country->name;
		}
		
		if($step == 4)
		{
		    checkUrlToken();
		    if(count($order->getTopLevelItems()) == 0)
		    {
                setcookie ("PN_order_id", "", time() - 3600,"/",$this->domain);
                $urllogo 	= $_COOKIE['urllogo'] ? $_COOKIE['urllogo'] : "";
                $url_arr 	= explode("|",$urllogo);
                $url_show   = $url_arr[0] ? $url_arr[0] : "/";
                header("Location: ".$url_show);
            }
		    
			//Load address
			$objAddress = new PnAddress();
			
			$objAddress->initWithId($order->shipping_address_id);
			$this->Template->ShippingAddress = $objAddress;
			
			$objBillingAddr = new PnAddress();
			$objBillingAddr->initWithId($order->billing_address_id);
			
			$this->Template->BillingAddress	   = $objBillingAddr;
			
			$customer = new PnCustomer();
            $customer->initWithId($order->customer_id);
			$this->Template->customer_email    = $customer->email;
			
			$this->Template->bookItems         = $order->getBookItems();
			$this->Template->shippingPrice     = $order->getShippingPrice();
			$this->Template->totalPrice        = $order->getTotalPriceWithoutCoupon("", false);
			$this->Template->wineItems         = $order->getWineItems();
			$this->Template->orderid           = $order->id;
			$this->Template->taxPrice          = $order->getTaxPrice();
			
			$this->Template->Coupon_black_true = $this->checkBlackFriday($order);
			
			if($GLOBALS['TL_CONFIG']['Counpon_Free'] || $this->checkBlackFriday($order)){
				if($order->hasCoupon() && $order->getNumOfBookItems() > 0)
				{
				    if($order->hasGiftbox())
				    {
					$this->Template->couponGiftboxName  = $order->coupon_name_de;
					$this->Template->couponGiftboxPrice = $order->coupon_prices_de * -1;
				    }
				}
			}
			
			$shipping = new PnShipping();
			$shipping->initWithId($order->shipping_id);
			$this->Template->shipping = $shipping;
			
			$payment = new PnPayment();
			$payment->initWithId($order->payment_method_id);
			$this->Template->payment = $payment;
			
			$coupon = new PnCoupon();
            $objCoupon = $this->Database->prepare("
                    SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                    WHERE order_id = ?
                ")->execute($order->id);
            while($objCoupon->next())
            {
                $coupon_id = $objCoupon->coupon_id;
            }
            if($coupon_id > 0)
            {
                $coupon->initWithId($coupon_id);
                $this->Template->couponCode = $coupon->token;
                $couponValue = $coupon->wert;
                if($coupon->coupontype_id == 3){
					$couponValue = $coupon->wert + $coupon->wert2;
				}
                $orderPriceWithoutCoupon = $order->getTotalPriceWithoutCoupon("de", true);
                if($coupon->prozent > 0)
                {
                    $couponValue = round($orderPriceWithoutCoupon * min(100, max(0, $coupon->prozent)) / 100, 2);
                }
                
                if($couponValue > $orderPriceWithoutCoupon)
                    $couponValue = $orderPriceWithoutCoupon;
                
                /* Su dung tam thoi cho rabatt PN50RABATT */
                if($coupon->token == "PN50RABATT")
                {
                    $couponValue = round($order->getSecondBookItemPrice() * 0.5, 2);
                }
		    
                $this->Template->couponToken = $coupon->token;
                $this->Template->couponValue = -1 * floatVal($couponValue);
            }
            
            $objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".items
                WHERE class_id = 6 AND order_id = ?
            ")->execute($order->id);
            
            if($objDb->numRows > 0)
            {
                $this->Template->trustshop      = true;
                $this->Template->trustshopName  = "Käuferschutz Trusted Shops bis 500 Euro";
                $this->Template->trustshopPrice = 0.98;
            }
			
			if($_POST['step'])
			{
			    if(!$_POST["datenschutz_"])
			    {
			        $this->Template->error_datenschutz_ = true;
			    }
			    
                if(!$_POST["agb_"])
                {
                    $this->Template->error_agb_ = true;
                }
                
                if(!$_POST["widerruf_"])
                {
                    $this->Template->error_widerruf_ = true;
                }
			    
			    if($_POST["datenschutz_"] && $_POST["agb_"] && $_POST["widerruf_"])
			    {
			        $order->recalc();
                    $order->createOrderNumber();
                    $order->edit(array("order_status" => 1));
                    
                    setcookie ("PN_order_id", "", time() - 3600,"/",$this->domain);
                    
                    $objDb = $this->Database->prepare("
                        SELECT order_id FROM " . $this->config->database . ".order_status
                        WHERE order_id = ?
                    ")->execute($order->id);
                    
                    if($objDb->numRows == 0)
                    {
                        $this->Database->prepare("
                            INSERT INTO " . $this->config->database . ".order_status(`order_id`, `status_id`, `status_set`)
                            VALUE (?, ?, ?)
                        ")->execute($order->id, 1, date('Y/m/d H:i:s a', time()));
                    }
                    
                    $order->update(array("order_date" => date('Y/m/d H:i:s a', time())));
                    
                    //Generate coupon if has ebooks
                    if(count($order->getEbookItems()) > 0)
                    {
                        $new_coupon = new PnCoupon();
                        $new_coupon->makeCoupon($order->id, "EB", 10);
                    }
                    
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        $order->edit(array("order_step" => "payment"));
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[5], $token));
                    }
                    else
                    {
                        $order->edit(array("order_step" => "payment"));
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[5]));
                    }
                    
			    }
			    else
			    {
			        $this->Template->error = true;
			    }
			}
		}
		
		if($step == 5)
		{
		    $amount = round($order->getTotalPrice() * 100, 0);
		    
            $currency = $GLOBALS['TL_CONFIG']['currency'] ? $GLOBALS['TL_CONFIG']['currency'] : "EUR";
            $shipBeforeDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")+1));
            $sessionValidity = date(DATE_ATOM, mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")+1));
            $merchantref = "Order".$order->id;
            $skinCode = $GLOBALS['TL_CONFIG']['skinCode'] ? $GLOBALS['TL_CONFIG']['skinCode'] : "9Bg7A81t";
            $merchantAccount = $GLOBALS['TL_CONFIG']['merchantAccount'] ? $GLOBALS['TL_CONFIG']['merchantAccount'] : "PersonalNOVELDE";
            $shopperEmail = "";
            $allowedMethods = "visa,paypal,mc,bankTransfer";
            $blockedMethods = "";
            $signature = $GLOBALS['TL_CONFIG']['signature'] ? $GLOBALS['TL_CONFIG']['signature'] : "pnovel.2012";
            $shopperLocale = "DE";
            $countryCode = $GLOBALS['TL_CONFIG']['countryCode'] ? $GLOBALS['TL_CONFIG']['countryCode'] : "DE";
            $testOrLive = $GLOBALS['TL_CONFIG']['adyen_test_live'] ? $GLOBALS['TL_CONFIG']['adyen_test_live'] : "test";
            
            $Crypt_HMAC = new Crypt_HMAC($signature, 'sha1');
            
			$payment_id = $order->payment_method_id;
			
			if($amount == 0)
            {
                $order->edit(
                    array(
                        "order_paid"  => date('Y/m/d H:i:s a', time()),
                        "order_ok2go" => date('Y/m/d H:i:s a', time()),
                        "order_blob"  => date('Y-m-d', time()) . ": ok2go gesetzt: Automatisch\n"
                        )
                    );
            }
			
			if($payment_id == 1)
			{
			    if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($order->id);
                    //$order->edit(array("order_step" => "done"));
                    header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                }
                else
                {
                    //$order->edit(array("order_step" => "done"));
                    header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                }
			    
			    /*
			    $allowedMethods = "bankTransfer";
			    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
    
			    header("Location: " . $url);
			    */
			}
			
			if($payment_id == 2)
			{
			    if($amount == 0)
                {
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        //$order->edit(array("order_step" => "done"));
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        //$order->edit(array("order_step" => "done"));
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
                    
                }
                else
                {
                    $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                    $order->edit(array("order_blob" => $order->blob . $blob));
                    
                    $allowedMethods = "visa,mc";
                    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                    $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                    $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
        
                    header("Location: " . $url);
			    }
			}
			
			if($payment_id == 3)
			{
			    if($amount == 0)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
                    
			    }
			    else
			    {
			        if($GLOBALS['TL_CONFIG']['paypal']['active'])
			        {
			            $sandbox = $GLOBALS['TL_CONFIG']['paypal']['sandbox'];
                        $domain  = $sandbox ? "http://local-web.personalnovel.local" : "http://personalnovel.de";
                        
                        $api_version   = '95.0';
                        $api_username  = $sandbox ? 'jangoetze-facilitator_api1.personalnovel.de' : 'payment_api1.personalnovel.de';
                        $api_password  = $sandbox ? '1393924764' : 'CSGC49BKF6XUWBJ4';
                        $api_signature = $sandbox ? 'AquYCoadgQ2s38zw1OqcZmqzFvtQA83xcfJ8XMH4utDAaJQy8vhduzRU' : 'AFcWxV21C7fd0v3bYYYRCpSSRl31AjJ57mq2gb0z7cUV3fCyw0dMSY1H';
                        
                        $paypal_config = array(
                            'Sandbox' => $sandbox,
                            'APIUsername'  => $api_username,
                            'APIPassword'  => $api_password,
                            'APISignature' => $api_signature, 
                            'APIVersion'   => '87.0', 
                            'APISubject'   => 'jangoetze@personalnovel.de'
                        );
                            
                        $paypal = new PayPal($paypal_config);
                            
			            if($_GET["process"] == "paypal")
			            {
			                $gecd_result = $paypal->GetExpressCheckoutDetails($_SESSION['PayPalResult']['TOKEN']);
			                
			                $decp_fields = array(
                                'token' => $_SESSION['PayPalResult']['TOKEN'],
                                'payerid' => $gecd_result['PAYERID'],
                                'returnfmfdetails' => '1',
                            );
                            
                            $payments = array();
                            $payment_order_items = array();
                            
                            /*
                            //book items
                            $book_items = $order->getBookItems();
                            foreach($book_items as $book_item)
                            {
                                $book_name = $book_item->getName();
                                //$book_amount = $book_item->item_pieces;
                                $book_amount = 1;
                                $book_price = number_format($book_item->getTotalPrice() / $book_amount, 2);
                                
                                $item = array(
                                    'name' => $book_name,
                                    'qty' => $book_amount,
                                    'amt' => $book_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            //wine items
                            $wine_items = $order->getWineItems();
                            foreach($wine_items as $wine_item)
                            {
                                $wine_name = $wine_item->getName();
                                //$wine_amount = $wine_item->item_pieces;
                                $wine_amount = 1;
                                $wine_price = number_format($wine_item->getTotalPrice() / $wine_amount, 2);
                                
                                $item = array(
                                    'name' => $wine_name,
                                    'qty' => $wine_amount,
                                    'amt' => $wine_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            //voucher
                            $coupon = new PnCoupon();
                            $query_coupon = $this->Database->prepare("
                                SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                                WHERE order_id = ?
                            ")->execute($order->id);
                            
                            while($query_coupon->next())
                            {
                                $coupon_id = $query_coupon->coupon_id;
                            }
                            
                            if($coupon_id > 0)
                            {
                                $coupon->initWithId($coupon_id);
                                $coupon_name = $coupon->token;
                                $coupon_value = $coupon->wert;
                                if($coupon->coupontype_id == 3){
                                    $coupon_value = $coupon->wert + $coupon->wert2;
                                }
                                $order_price_without_coupon = $order->getTotalPriceWithoutCoupon("de", true);
                                if($coupon->prozent > 0)
                                {
                                    $coupon_value = round($order_price_without_coupon * min(100, max(0, $coupon->prozent)) / 100, 2);
                                }
                                
                                if($coupon_value > $order_price_without_coupon)
                                    $coupon_value = $order_price_without_coupon;
                                
                                $coupon_value = number_format($coupon_value * -1, 2);
                                $coupon->initWithId($coupon_id);
                                $item = array(
                                    'name' => $coupon->token,
                                    'qty' => 1,
                                    'amt' => $coupon_value
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            $query_trustshop = $this->Database->prepare("
                                SELECT * FROM " . $this->config->database . ".items
                                WHERE class_id = 6 AND order_id = ?
                            ")->execute($order->id);
                            
                            if($query_trustshop->numRows > 0)
                            {
                                $trustshop_name = "Käuferschutz Trusted Shops bis 500 Euro";
                                $trustshop_price = 0.98;
                                $item = array(
                                    'name' => $trustshop_name,
                                    'qty' => 1,
                                    'amt' => $trustshop_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            //shipping
                            $shipping_price = number_format($order->getShippingPrice(), 2);
                            if($shipping_price > 0)
                            {
                                $shipping_name = "zzgl Versandkosten";
                                $item = array(
                                    'name' => $shipping_name,
                                    'qty' => 1,
                                    'amt' => $shipping_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }*/
                            
                            $order_price = number_format($order->getTotalPrice(), 2);
                            
                            $item = array(
                                        'name' => "Order$order->id",
                                        'qty' => 1,
                                        'amt' => $order_price
                                    );
                                    
                            array_push($payment_order_items, $item);
                                
                            $payment = array(
                                'amt' => $order_price,
                                'currencycode' => 'EUR',
                                'itemamt' => $order_price,
                                'desc' => '',
                                'notetext' => "Order Nr: $order->number",
                                'paymentaction' => 'Sale' 
                            );
                            
                            $payment['order_items'] = $payment_order_items;
                            array_push($payments, $payment);
                            
                            $paypal_request = array(
                                'DECPFields' => $decp_fields, 
                                'Payments' => $payments
                            );
                            
                            $_SESSION['PayPalResult'] = $paypal->DoExpressCheckoutPayment($paypal_request);
                            
                            if(count($_SESSION['PayPalResult']['ERRORS']))
                            {
                                //Error, change payment method
                                $sender = new PnSoapHandler();
                                $sender->sendMailRepayBridge($order->id);
                                
                                $token = generateToken($order->id);
                                $url = sprintf("%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
                            }
                            else
                            {
                                $order->pay($order_price);
                                $order->edit(
                                    array(
                                        "order_paid"  => date('Y/m/d H:i:s a', time()),
                                        "order_ok2go" => date('Y/m/d H:i:s a', time()),
                                        "order_blob"  => date('Y-m-d', time()) . ": ok2go gesetzt: Automatisch\n"
                                        )
                                );
                                
                                $sender = new PnSoapHandler();
                                $sender->sendMailConfirmBridge($order->id);
                                
                                //Ok, go to step done
                                $url = sprintf("%s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]);
                            }
                            
			            }
			            else
			            {
			                $token = generateToken($order->id);
                            $return_url = sprintf("%s?order=%s&step=%s&process=%s", $checkout_page, $order->id, $arStepV[5], "paypal");
                            $cancel_url = sprintf("%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
                            $sec_fields = array(
                                'maxamt' => '2000000.00',
                                'returnurl' => $domain . $return_url,
                                'cancelurl' => $domain . $cancel_url,
                                'allownote' => '1',
                                'localecode' => 'DE',
                                'hdrimg' => 'http://personalnovel.de/tl_files/personalnovel_de/static/assets/header/logo.png',
                                'skipdetails' => '1',
                                'solutiontype' => 'Sole',
                                'brandname' => 'PersonalNOVEL',
                                'customerservicenumber' => '555-555-5555',
                                'buyeremailoptionenable' => '1',
                                'buyerregistrationdate' => '2012-07-14T00:00:00Z'
                            );
                            
                            $payments = array();
                            $payment_order_items = array();
                            /*
                            //book items
                            $book_items = $order->getBookItems();
                            foreach($book_items as $book_item)
                            {
                                $book_name = $book_item->getName();
                                //$book_amount = $book_item->item_pieces;
                                $book_amount = 1; //bug lam tron 2 so
                                $book_price = number_format($book_item->getTotalPrice() / $book_amount, 2);
                                
                                $item = array(
                                    'name' => $book_name,
                                    'qty' => $book_amount,
                                    'amt' => $book_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            //wine items
                            $wine_items = $order->getWineItems();
                            foreach($wine_items as $wine_item)
                            {
                                $wine_name = $wine_item->getName();
                                //$wine_amount = $wine_item->item_pieces;
                                $wine_amount = 1; //bug lam tron 2 so
                                $wine_price = number_format($wine_item->getTotalPrice() / $wine_amount, 2);
                                
                                $item = array(
                                    'name' => $wine_name,
                                    'qty' => $wine_amount,
                                    'amt' => $wine_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            //voucher
                            $coupon = new PnCoupon();
                            $query_coupon = $this->Database->prepare("
                                SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                                WHERE order_id = ?
                            ")->execute($order->id);
                            
                            while($query_coupon->next())
                            {
                                $coupon_id = $query_coupon->coupon_id;
                            }
                            
                            if($coupon_id > 0)
                            {
                                $coupon->initWithId($coupon_id);
                                $coupon_name = $coupon->token;
                                $coupon_value = $coupon->wert;
                                if($coupon->coupontype_id == 3){
                                    $coupon_value = $coupon->wert + $coupon->wert2;
                                }
                                $order_price_without_coupon = $order->getTotalPriceWithoutCoupon("de", true);
                                if($coupon->prozent > 0)
                                {
                                    $coupon_value = round($order_price_without_coupon * min(100, max(0, $coupon->prozent)) / 100, 2);
                                }
                                
                                if($coupon_value > $order_price_without_coupon)
                                    $coupon_value = $order_price_without_coupon;
                                
                                $coupon_value = number_format($coupon_value  * -1, 2);
                                $coupon->initWithId($coupon_id);
                                $item = array(
                                    'name' => "Abzüglich Gutschein " . $coupon->token,
                                    'qty' => 1,
                                    'amt' => $coupon_value
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            $query_trustshop = $this->Database->prepare("
                                SELECT * FROM " . $this->config->database . ".items
                                WHERE class_id = 6 AND order_id = ?
                            ")->execute($order->id);
                            
                            if($query_trustshop->numRows > 0)
                            {
                                $trustshop_name = "Käuferschutz Trusted Shops bis 500 Euro";
                                $trustshop_price = 0.98;
                                $item = array(
                                    'name' => $trustshop_name,
                                    'qty' => 1,
                                    'amt' => $trustshop_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            
                            //shipping
                            $shipping_price = number_format($order->getShippingPrice(), 2);
                            if($shipping_price > 0)
                            {
                                $shipping_name = "zzgl Versandkosten";
                                $item = array(
                                    'name' => $shipping_name,
                                    'qty' => 1,
                                    'amt' => $shipping_price
                                );
                                
                                array_push($payment_order_items, $item);
                            }
                            */
                            $order_price = number_format($order->getTotalPrice(), 2);
                            
                            $item = array(
                                        'name' => "Order$order->id",
                                        'qty' => 1,
                                        'amt' => $order_price
                                    );
                            
                            array_push($payment_order_items, $item);
                            
                            $payment = array(
                                'amt' => $order_price,
                                'currencycode' => 'EUR',
                                'itemamt' => $order_price,
                                'desc' => '',
                                'notetext' => "Order Nr: $order->number",
                                'paymentaction' => 'Sale' 
                            );
                            
                            $payment['order_items'] = $payment_order_items;
                            array_push($payments, $payment);
                            
                            $paypal_request = array(
                               'SECFields' => $sec_fields, 
                               'Payments' => $payments
                            );
                            
                            $_SESSION['PayPalResult'] = $paypal->SetExpressCheckout($paypal_request);
                            if(count($_SESSION['PayPalResult']['ERRORS']))
                            {
                                //Error, change payment method
                                $token = generateToken($order->id);
                                $url = sprintf("%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
                            }
                            else
                            {
                                //Ok, do express checkout payment
                                $url = $_SESSION['PayPalResult']['REDIRECTURL'];
                            }
                            
                        }
			        }
			        else
			        {
                        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                            date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                        $order->edit(array("order_blob" => $order->blob . $blob));
                        
                        $allowedMethods = "paypal";
                        $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                        $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                        $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
                    }
                    
                    header("Location: " . $url);
			    }
			}
			if($payment_id == 6)
			{
			    if($amount == 0)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
                    
			    }
			     else
			    {
			        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                    $order->edit(array("order_blob" => $order->blob . $blob));
                    
                    $allowedMethods = "elv";
                    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                    $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                    $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
        
                    header("Location: " . $url);
			    }
			}
			if($payment_id == 7)
			{
			    if($amount == 0)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
			    }
			     else
			    {
			        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                    $order->edit(array("order_blob" => $order->blob . $blob));
                    $allowedMethods = "amex";
                    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                    $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                    $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
        
                    header("Location: " . $url);
			    }
			}
			if($payment_id == 8)
			{
			    if($amount == 0)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
                    
			    }
			    else
			    {
			        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                    $order->edit(array("order_blob" => $order->blob . $blob));
                    $allowedMethods = "directEbanking";
                    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                    $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                    $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
        
                    header("Location: " . $url);
			    }
			}
			if($payment_id == 9)
			{
			    if($amount == 0)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
                    
			    } 
			    else
			    {
			        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                    $order->edit(array("order_blob" => $order->blob . $blob));
                    
                    $allowedMethods = "giropay";
                    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                    $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                    $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
        
                    header("Location: " . $url);
			    }
			}
			if($payment_id == 10)
			{
			    if($amount == 0)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
                    
			    }
			     else
			    {
			        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                    $order->edit(array("order_blob" => $order->blob . $blob));
                    
                    $allowedMethods = "maestro";
                    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                    $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                    $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
        
                    header("Location: " . $url);
			    }
			}
			
			//Amazon payment
			if($payment_id == 12)
			{
			    if($amount == 0)
			    {
			        if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
			    }
			    else
			    {
			        $lib = new PnAmazonCBAPurchaseContract();
			        $purchase_contract_id = $_SESSION['amazon_session'];
			        $item_list = new PnAmazonModelItemList();
			        $charges = new PnAmazonModelContractCharges();
			        
			        //Book Items
			        foreach($order->getBookItems() as $book_item)
			        {
			            $purchase_item = new PnAmazonModelPurchaseItem();
			            $purchase_item_name = sprintf("%s (%s)", $book_item->getName(), $book_item->item_pieces);
			            $purchase_item->createPhysicalItem($book_item->id, $purchase_item_name, $book_item->getTotalPrice(), 'Standard');
			            
			            $purchase_item->setSKU($book_item->article_id);
			            
			            $item_list->addItem($purchase_item);
			        }
			        
			        //Wine Items
			        $wine_items = $order->getWineItems();
                    foreach($wine_items as $wine_item)
                    {
                        $purchase_item = new PnAmazonModelPurchaseItem();
                        $purchase_item_name = sprintf("%s (%s)", $wine_item->getName(), $wine_item->item_pieces);
                        $purchase_item->createPhysicalItem($wine_item->id, $purchase_item_name, $wine_item->getTotalPrice(), 'Standard');
                        
			            $purchase_item->setSKU($wine_item->article_id);
			            
			            $item_list->addItem($purchase_item);
                    }
                    
                    $set_items_status = $lib->setItems($purchase_contract_id, $item_list);
                    
                    //voucher
                    $coupon = new PnCoupon();
                    $query_coupon = $this->Database->prepare("
                        SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                        WHERE order_id = ?
                    ")->execute($order->id);
                    
                    while($query_coupon->next())
                    {
                        $coupon_id = $query_coupon->coupon_id;
                    }
                    
                    if($coupon_id > 0)
                    {
                        $coupon->initWithId($coupon_id);
                        $coupon_name = $coupon->token;
                        $coupon_value = $coupon->wert;
                        if($coupon->coupontype_id == 3){
                            $coupon_value = $coupon->wert + $coupon->wert2;
                        }
                        $order_price_without_coupon = $order->getTotalPriceWithoutCoupon("de", true);
                        if($coupon->prozent > 0)
                        {
                            $coupon_value = round($order_price_without_coupon * min(100, max(0, $coupon->prozent)) / 100, 2);
                        }
                        
                        if($coupon_value > $order_price_without_coupon)
                            $coupon_value = $order_price_without_coupon;
                        
                        $coupon_value = number_format($coupon_value, 2);
                        $coupon->initWithId($coupon_id);
                        
                        $promotion = new PnAmazonModelPromotion();
                        $promotion->createPromotion("Abzüglich Gutschein", "Abzüglich Gutschein $coupon->token", $coupon_value);
                        $promotion_list_object = new PnAmazonModelPromotionList();
                        $promotion_list_object->addPromotion($promotion);
                        
                        $charges->setContractPromotions($promotion_list_object);
                    }
                    
                    $charges->setContractShippingCharges($order->getShippingPrice());
                    $set_contract_charges_status = $lib->setContractCharges($purchase_contract_id, $charges);
                    
                    if($set_items_status == 1)
                    {
                        $order_id_list = $lib->completeOrder($purchase_contract_id, "A136CGJE3412", "Test Cart Integrator");
                    
                        if(!is_null($order_id_list))
                        {
                            foreach ($order_id_list as $order_id) 
                            {
                                $order->update(
                                    array(
                                        "order_foreign_code" => "AMAZON-$order_id",
                                        "order_status" => -15,
                                        //"order_ok2go" => date('Y/m/d H:i:s a', time()),
                                        "order_blob"  => $order->blob . date('Y-m-d', time()) . ": Send order to Amazon\n"
                                        )
                                    );
                                //$order_price = number_format($order->getTotalPrice(), 2);
                                //$order->pay($order_price);
                            }
                            
                            /*$sender = new PnSoapHandler();
                            $sender->sendMailConfirmBridge($order->id);*/
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                    }
			    }
			}
		}
		
		if($step == 6)
		{
			/* */
			if($_COOKIE['black-Friday']){
				setcookie('black-Friday', '', time() - (86400 * 3), "/",$this->domain);
		    	}
		    /* */
		    if($_GET["merchantReference"])
		    {
		        $orderid = str_replace("Order", "", $_GET["merchantReference"]);
		        $order->initWithId($orderid);
		    }
		    else if($_GET["order"])
		    {
		        $order->initWithId($_GET["order"]);
		    }
		    else
		    {
		        gotoErrorPage();
		    }
		    
		    $address = new PnAddress();
		    $address->initWithId($order->shipping_address_id);
		    
		    $order_cid = Verhoeff::generate($order->number);
		    
		    if($order->step != "done")
		    {
		        $order->update(array("order_step" => "done"));
		        $customer = new PnCustomer();
                $customer->initWithId($order->customer_id);
                
                $objTemplate = new BackendTemplate("mod_mail_confirmorder");
                $objTemplate->anrede          = $customer->anrede;
                $objTemplate->orderNumber     = $order_cid;
                $objTemplate->firstName       = $address->firstName;
                $objTemplate->lastName        = $address->lastName;
                
                $billingAddress               = new PnAddress();
                $billingAddress->initWithId($order->billing_address_id);
                $objTemplate->anrede          = $billingAddress->anrede ? $billingAddress->anrede : $customer->anrede;
                $objTemplate->billingFName    = $billingAddress->firstName;
                $objTemplate->billingLName    = $billingAddress->lastName;
                
                $orderDate                    = strtotime($order->date);
                $newLocale                    = setlocale(LC_TIME, 'de_DE', 'de_DE.UTF-8');
                $objTemplate->orderDate       = utf8_encode(strftime('%d. %B %Y um %H:%M Uhr.', $orderDate));
                $objTemplate->linkMyPage      = "http://" . $_SERVER['HTTP_HOST'] . "/myorder?id=" . $order->id;
                $objTemplate->paymentMethod   = $GLOBALS['TL_CONFIG']['payment_methods'][1][$order->payment_method_id]['html'];
                $objTemplate->paymentMethodId = $order->payment_method_id;
                $objTemplate->address         = $address->address;
                $objTemplate->company         = $address->company;
                $objTemplate->city            = $address->city;
                $objTemplate->zip             = $address->zip;
                
                $shipping_country             = new PnCountry();
                $shipping_country->initWithId($address->country_id);
                $objTemplate->country         = $shipping_country->name;
                $objTemplate->estimated_days  = $shipping_country->estimated_days;
                $TotalPrice = $order->getTotalPrice();
                
                if($TotalPrice == 0)
                {
                    $order->edit(array("order_paid" => date('Y/m/d H:i:s a', time())));
                }
                
                if($order->payment_method_id == 1)
                {
                    $objDb = $this->Database->prepare("
                            SELECT * FROM " . $this->config->database . ".finanz_posten
                            WHERE order_id = ?
                        ")->execute($order->id);
                        
                    $postens = array();
                    while($objDb->next())
                    {
                        $posten                = new PnPosten();
                        $posten->id            = $objDb->posten_id;
                        $posten->steuerart_id  = $objDb->steuerart_id;
                        $posten->zielkonto     = $objDb->posten_zielkonto;
                        $posten->konto_id      = $objDb->konto_id;
                        $posten->artikel_id    = $objDb->artikel_id;
                        $posten->anzahl        = $objDb->posten_anzahl;
                        $posten->einzelbetrag  = $objDb->posten_einzelbetrag;
                        $posten->netto         = $objDb->posten_netto;
                        $posten->text          = $objDb->posten_text;
                        $posten->description   = $objDb->posten_description;
                        $posten->datum         = $objDb->posten_datum;
                        $posten->manuell       = $objDb->posten_manuell;
                        $posten->created       = $objDb->posten_created;
                        $posten->updated       = $objDb->posten_updated;
                        $posten->acc_id        = $objDb->acc_id;
                        $posten->item_id       = $objDb->item_id;
                        $posten->order_id      = $objDb->order_id;
                        $posten->rechnung_id   = $objDb->rechnung_id;
                        $posten->coupon_id     = $objDb->coupon_id;
                        $posten->culprit_id    = $objDb->culprit_id;
                        $posten->ust_id        = $objDb->ust_id;
                        $posten->currency_id   = $objDb->currency_id;
                        $posten->steuersatz_id = $objDb->steuersatz_id;
                        
                        $postens[]             = $posten;
                    }
                
                    $objTemplate->postens = $postens;
                    $objTemplate->customerEmail = $customer->email;
                    
                    if($GLOBALS['TL_CONFIG']['Counpon_Free'] || $this->checkBlackFriday($order)){
                        if($order->hasCoupon() && $order->getNumOfBookItems() > 0 && $order->hasGiftbox() )
                        {
                            $objTemplate->hasCouponName = $order->coupon_name_de;
                            $objTemplate->hasCouponPrice = $order->coupon_prices_de * (-1);
                        }
                    
                        if($order->hasCoupon() && $order->getNumOfBookItems() > 0 && !$order->hasCouponNotGiftboxFree() )
                        {
                            $objTemplate->hasCouponName = $order->coupon_name_de;
                            $objTemplate->hasCouponPrice = 0;
                        }
                    }
                    
                    //Check coupon Coconut
                    if($order->hasCoupon())
                    {
                        $coupon_query = $this->Database->prepare("
                            SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                            WHERE order_id = ?
                        ")->execute($order->id);
                        
                        while($coupon_query->next())
                        {
                            $coupon_id = $coupon_query->coupon_id;
                        }
                        
                        $coupon = new PnCoupon();
                        $coupon->initWithId($coupon_id);
                        if($coupon->partner_id == 44)
                        {
                            $this->Database->prepare("
                                INSERT INTO " . $this->config->database . ".order_refs(`order_id`, `ref_id`, `confirmation_status`)
                                VALUES(?, 3, 1)
                            ")->execute($order->id);
                            
                            //Request Coconut API
                            $ch = curl_init( 'https://api.bonberry.de/partner/2mm0aq7heqkjtdzj18n1cgpe08isiihk/cntr/v1' );
                            $api_data = array(
                                    "OID" => $order_cid,
                                    "CODE" => $coupon->token,
                                    "NC" => 1,
                                    "OV" => intVal($TotalPrice * 100)
                                );
                            
                            $options = array(CURLOPT_POSTFIELDS =>$api_data);
                            curl_setopt_array( $ch, $options );
                            curl_exec($ch);
                            curl_close($ch);
                        }
                    }
                    
                    $objTemplate->orderPrice = $TotalPrice;
                    $objTemplate->orderTax   = $order->getTaxPrice();
                    $objTemplate->shippingId = $order->shipping_id;
                    $objTemplate->order      = $order;
                    $wein_item  = $order->getWineItemsNomal();
                    $wein_class = new PnWineItem();
                    if(count($wein_item) > 0)
                    foreach($wein_item as $wein)
                    {
                    	   $total_now = $wein_class->getTotalproduct($wein['article_id']);
                    	   $wein['item_pieces']= $total_now - $wein['item_pieces'];
                    	   $wein_class->updateTotalwein($wein);
                    }
                    
                    $this->sendMailToCustomer($customer->email, sprintf("Ihre Bestellung, %s", $order_cid), $objTemplate);
                    $order->addMessageCheckSendMail();
                    $note = sprintf(": Freqmail \"Ihre Bestellung, %s (Vorauskasse)\"\n", $order_cid);
                    $order->edit(
					  array(
							 "order_blob"  => $order->blob.date('Y-m-d', time()) . $note
						  )
					  );
                    
                }
                
                //Sendmail
                $s .= 'Date : '.date("F j, Y, g:i a") .'<br/>';
	            $s .= 'OrderID: '. $order->id.'<br/>';
	            $s .= 'Total Price :' .$TotalPrice.'<br/>';
	             
	            $arData['message'] = $s;
                 $this->sendMail($arData,'Checkout: ','checkout done ',$order->id);
                	
		    }
		    
		    if($_COOKIE["referrer"])
            {
                $objDb = $this->Database->prepare("
                    SELECT * FROM " . $this->config->database . ".refs
                    WHERE ref_tag like ?
                ")->execute($_COOKIE["referrer"]);
                
                if($objDb->numRows > 0)
                {
                    $this->Database->prepare("
                        INSERT INTO " . $this->config->database . ".order_refs(`order_id`, `ref_id`, `confirmation_status`)
                        VALUES(?, ?, 1)
                    ")->execute($order->id, $objDb->first()->ref_id);
                }
                
                //setcookie ("referrer", "", time() - 3600);
                //setcookie ("ref_zanpid", "", time() - 3600);
            }
		    
			$this->Template->order                = $order;
			$this->Template->order_cid            = $order_cid;
			$this->Template->customer_firstName   = $address->firstName;
            $this->Template->customer_lastName    = $address->lastName;
            $this->Template->customer_address     = $address->address;
            $this->Template->customer_company     = $address->company;
            $this->Template->customer_city        = $address->city;
            $this->Template->customer_zip         = $address->zip;
            
            $customer_country                     = new PnCountry();
            $customer_country->initWithId($address->country_id);
            $this->Template->customer_country     = $customer_country->name;
            $this->Template->delivery_days        = $customer_country->delivery_days;
            
		}
		
		if($step == "changepayment")
		{
		    checkUrlToken();
		    if(count($order->getTopLevelItems()) == 0){
                setcookie ("PN_order_id", "", time() - 3600,"/",$this->domain);
                $urllogo 	= $_COOKIE['urllogo'] ? $_COOKIE['urllogo'] : "";
                $url_arr 	= explode("|",$urllogo);
                $url_show   = $url_arr[0] ? $url_arr[0] : "/";
                header("Location: ".$url_show);
            }
            
            
            
			 //Allow Amazon checkout ?
		      $this->Template->allow_amazon_payment = true;
		      $num_of_ebooks = count($order->getEbookItems());
		      $num_of_wines  = count($order->getWineItems());
		      if($num_of_ebooks > 0 || $num_of_wines > 0)
		      {
		          $this->Template->allow_amazon_payment = false;
		          
		          if($order->payment_method_id == 12)
		          {
		              $order->update(array('payment_method_id' => 1));
		              $order->payment_method_id = 1;
		          }
		      }
		      
			  //Check payment amazon
			  if($order->payment_method_id == 12)
			  {
			    header(sprintf("Location: %s?order=%s&step=%s&session=%s", $checkout_page, $order->id, "amazon", $_SESSION['amazon_session']));
			  }
            
		    
		    $this->changePaymentMethod();
		    
		    if($_POST["submitter"])
		    {
		        $order = new PnOrder();
		        $order->initWithId($_GET["order"]);
		        $order->update(
                            array(
                                "payment_method_id" => $_POST["payment_method"],
                                "order_step"        => ""
                            )
                        );
		        
		        $payment_id = $_POST["payment_method"];
		        
		        if($payment_id == 1)
		        {
		            if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                    }
		            
		        }
		        
		       	$amount = round($order->getTotalPrice() * 100, 0);
		        
                $currency = $GLOBALS['TL_CONFIG']['currency'] ? $GLOBALS['TL_CONFIG']['currency'] : "EUR";
                $shipBeforeDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")+1));
                $sessionValidity = date(DATE_ATOM, mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")+1));
                $merchantref = "Order".$order->id;
                $skinCode = $GLOBALS['TL_CONFIG']['skinCode'] ? $GLOBALS['TL_CONFIG']['skinCode'] : "9Bg7A81t";
                $merchantAccount = $GLOBALS['TL_CONFIG']['merchantAccount'] ? $GLOBALS['TL_CONFIG']['merchantAccount'] : "PersonalNOVELDE";
                $shopperEmail = "";
                $allowedMethods = "visa,paypal,mc,bankTransfer";
                $blockedMethods = "";
                $signature = $GLOBALS['TL_CONFIG']['signature'] ? $GLOBALS['TL_CONFIG']['signature'] : "pnovel.2012";
                $shopperLocale = "DE";
                $countryCode = $GLOBALS['TL_CONFIG']['countryCode'] ? $GLOBALS['TL_CONFIG']['countryCode'] : "DE";
                $testOrLive = $GLOBALS['TL_CONFIG']['adyen_test_live'] ? $GLOBALS['TL_CONFIG']['adyen_test_live'] : "test";
                
                $Crypt_HMAC = new Crypt_HMAC($signature, 'sha1');
                
                if($payment_id == 2)
                {
                    if($amount == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                        
                    }
                    else
                    {
                        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                        $order->edit(array("order_blob" => $order->blob . $blob));
                    
                        $allowedMethods = "visa,mc";
                        $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                        $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                        $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
            
                        header("Location: " . $url);
                    }
                }
                
                if($payment_id == 3)
                {
                    if($amount == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                        
                    }
                    else
                    {
                        if($GLOBALS['TL_CONFIG']['paypal']['active'])
                        {
                            $sandbox = $GLOBALS['TL_CONFIG']['paypal']['sandbox'];
                            $domain  = $sandbox ? "http://local-web.personalnovel.local" : "http://personalnovel.de";
                            
                            $api_version   = '95.0';
                            $api_username  = $sandbox ? 'jangoetze-facilitator_api1.personalnovel.de' : 'payment_api1.personalnovel.de';
                            $api_password  = $sandbox ? '1393924764' : 'CSGC49BKF6XUWBJ4';
                            $api_signature = $sandbox ? 'AquYCoadgQ2s38zw1OqcZmqzFvtQA83xcfJ8XMH4utDAaJQy8vhduzRU' : 'AFcWxV21C7fd0v3bYYYRCpSSRl31AjJ57mq2gb0z7cUV3fCyw0dMSY1H';
                            
                            $paypal_config = array(
                                'Sandbox' => $sandbox,
                                'APIUsername'  => $api_username,
                                'APIPassword'  => $api_password,
                                'APISignature' => $api_signature, 
                                'APIVersion'   => '87.0', 
                                'APISubject'   => 'jangoetze@personalnovel.de'
                            );
                                
                            $paypal = new PayPal($paypal_config);
                                
                            if($_GET["process"] == "paypal")
                            {
                                $gecd_result = $paypal->GetExpressCheckoutDetails($_SESSION['PayPalResult']['TOKEN']);
                                
                                $decp_fields = array(
                                    'token' => $_SESSION['PayPalResult']['TOKEN'],
                                    'payerid' => $gecd_result['PAYERID'],
                                    'returnfmfdetails' => '1',
                                );
                                
                                $payments = array();
                                $payment_order_items = array();
                                
                                $order_price = number_format($order->getTotalPrice(), 2);
                                
                                $item = array(
                                        'name' => "Order$order->id",
                                        'qty' => 1,
                                        'amt' => $order_price
                                    );
                                    
                                array_push($payment_order_items, $item);
                                
                                $payment = array(
                                    'amt' => $order_price,
                                    'currencycode' => 'EUR',
                                    'itemamt' => $order_price,
                                    'desc' => '',
                                    'notetext' => "Order Nr: $order->number",
                                    'paymentaction' => 'Sale' 
                                );
                                
                                $payment['order_items'] = $payment_order_items;
                                array_push($payments, $payment);
                                
                                $paypal_request = array(
                                    'DECPFields' => $decp_fields, 
                                    'Payments' => $payments
                                );
                                
                                $_SESSION['PayPalResult'] = $paypal->DoExpressCheckoutPayment($paypal_request);
                                
                                if(count($_SESSION['PayPalResult']['ERRORS']))
                                {
                                    //Error, change payment method
                                    $sender = new PnSoapHandler();
                                    $sender->sendMailRepayBridge($order->id);
                                
                                    $token = generateToken($order->id);
                                    $url = sprintf("%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
                                }
                                else
                                {
                                    $order->pay($order_price);
                                    $order->edit(
                                        array(
                                            "order_paid"  => date('Y/m/d H:i:s a', time()),
                                            "order_ok2go" => date('Y/m/d H:i:s a', time()),
                                            "order_blob"  => date('Y-m-d', time()) . ": ok2go gesetzt: Automatisch\n"
                                            )
                                    );
                                    
                                    $sender = new PnSoapHandler();
                                    $sender->sendMailConfirmBridge($order->id);
                                    
                                    //Ok, go to step done
                                    $url = sprintf("%s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]);
                                }
                                
                            }
                            else
                            {
                                $token = generateToken($order->id);
                                $return_url = sprintf("%s?order=%s&step=%s&process=%s", $checkout_page, $order->id, $arStepV[5], "paypal");
                                $cancel_url = sprintf("%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
                                $sec_fields = array(
                                    'maxamt' => '2000.00',
                                    'returnurl' => $domain . $return_url,
                                    'cancelurl' => $domain . $cancel_url,
                                    'allownote' => '1',
                                    'localecode' => 'DE',
                                    'hdrimg' => 'http://personalnovel.de/tl_files/personalnovel_de/static/assets/header/logo.png',
                                    'skipdetails' => '1',
                                    'solutiontype' => 'Sole',
                                    'brandname' => 'PersonalNOVEL',
                                    'customerservicenumber' => '555-555-5555',
                                    'buyeremailoptionenable' => '1',
                                    'buyerregistrationdate' => '2012-07-14T00:00:00Z'
                                );
                                
                                $payments = array();
                                $payment_order_items = array();
                                
                                $order_price = number_format($order->getTotalPrice(), 2);
                                
                                $item = array(
                                        'name' => "Order$order->id",
                                        'qty' => 1,
                                        'amt' => $order_price
                                    );
                                    
                                array_push($payment_order_items, $item);
                                
                                $payment = array(
                                    'amt' => $order_price,
                                    'currencycode' => 'EUR',
                                    'itemamt' => $order_price,
                                    'desc' => '',
                                    'notetext' => "Order Nr: $order->number",
                                    'paymentaction' => 'Sale' 
                                );
                                
                                $payment['order_items'] = $payment_order_items;
                                array_push($payments, $payment);
                                
                                $paypal_request = array(
                                   'SECFields' => $sec_fields, 
                                   'Payments' => $payments
                                );
                                
                                $_SESSION['PayPalResult'] = $paypal->SetExpressCheckout($paypal_request);
                                if(count($_SESSION['PayPalResult']['ERRORS']))
                                {
                                    //Error, change payment method
                                    $token = generateToken($order->id);
                                    $url = sprintf("%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "changepayment", $token);
                                }
                                else
                                {
                                    //Ok, do express checkout payment
                                    $url = $_SESSION['PayPalResult']['REDIRECTURL'];
                                }
                                
                            }
                        }
                        else
                        {
                            $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                            date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                            $order->edit(array("order_blob" => $order->blob . $blob));
                        
                            $allowedMethods = "paypal";
                            $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                            $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                            $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
                        }
                        
                        header("Location: " . $url);
                    }
                }
		    	if($payment_id == 6)
		        {
		        	
		        	if($amount == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                        
                    }
                    else
                    {
                        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                        $order->edit(array("order_blob" => $order->blob . $blob));
                    
                        $allowedMethods = "elv";
                        $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                        $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                        $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
            
                        header("Location: " . $url);
                    }
		        }
		    	if($payment_id == 7)
		        {
		        	if($amount == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                        
                    }
                    else
                    {
                        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                        $order->edit(array("order_blob" => $order->blob . $blob));
                    
                        $allowedMethods = "amex";
                        $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                        $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                        $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
            
                        header("Location: " . $url);
                    }
		        }
		    	if($payment_id == 8)
		        {
		        	if($amount == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                        
                    }
                    else
                    {
                        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                        $order->edit(array("order_blob" => $order->blob . $blob));
                    
                        $allowedMethods = "directEbanking";
                        $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                        $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                        $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
            
                        header("Location: " . $url);
                    }
		        }
		    	if($payment_id == 9)
		        {
		        	if($amount == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                        
                    }
                    else
                    {
                        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                        $order->edit(array("order_blob" => $order->blob . $blob));
                    
                        $allowedMethods = "giropay";
                        $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                        $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                        $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
            
                        header("Location: " . $url);
                    }
		        }
		    	if($payment_id == 10)
		        {
		        	if($amount == 0)
                    {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[6], $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[6]));
                        }
                        
                    }
                    else
                    {
                        $blob = sprintf("%s Adyen - calculate value: %s\n%s Adyen - request value: %s\n", 
                                        date('Y-m-d', time()), $order->getTotalPrice(), date('Y-m-d', time()), $amount);
                        $order->edit(array("order_blob" => $order->blob . $blob));
                        
                        $allowedMethods = "maestro";
                        $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                        $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                        $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
            
                        header("Location: " . $url);
                    }
		        }
		    	
		    }
		}
		
		
		//Amazon checkout
		if($step == "amazon")
		{
		    $order = new PnOrder();
		    $order->initWithId($_GET["order"]);
		    
		    if(count($order->getTopLevelItems()) == 0)
		    {
                setcookie ("PN_order_id", "", time() - 3600,"/",$this->domain);
                $urllogo 	= $_COOKIE['urllogo'] ? $_COOKIE['urllogo'] : "";
                $url_arr 	= explode("|",$urllogo);
                $url_show   = $url_arr[0] ? $url_arr[0] : "/";
                header("Location: ".$url_show);
            }
		    
		    $this->Template->bookItems = $order->getBookItems();
		    $this->Template->wineItems = $order->getWineItems();
		    $this->Template->totalPrice    = $order->getTotalPriceWithoutCoupon("", false);
		    $this->Template->shippingPrice = $order->getShippingPrice();
		    $this->Template->taxPrice      = $order->getTaxPrice();
		    
		    /*$objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".items
                WHERE class_id = 6 AND order_id = ?
            ")->execute($order->id);
            
            if($objDb->numRows > 0)
            {
                $this->Template->trustshop      = true;
                $this->Template->trustshopName  = "Käuferschutz Trusted Shops bis 500 Euro";
                $this->Template->trustshopPrice = 0.98;
            }*/
            
            if($_POST["coupons_used_validate"])
            {
                if($_POST["coupons_used"])
			    {
			        $token = $_POST["coupons_used"];
			        $coupon= new PnCoupon();
			        if(!$coupon->checkExist($token, $order->id))
                    {
                        $error = true;
                        $this->Template->errorCoupon = "Es wurde leider kein Gutschein mit diesem Code gefunden.";
                    }
                    else
                    {
                        $coupon->initWithToken($token);
                        if($coupon->checkExpire())
                        {
                            $error = true;
                            $this->Template->errorCoupon = "Dieser Gutschein ist leider bereits abgelaufen.";
                        }
                        else if(!$coupon->checkActive())
                        {
                            $error = true;
                            $this->Template->errorCoupon = "Dieser Gutschein ist inaktiv.";
                        }
                        else
                        {
                            if($coupon->checkUsedUp($order->id))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein wurde leider bereits verwendet.";
                            }
                            else if(!$coupon->canUseForOrder($order))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                            else if(!$this->checkBlackFriday($order) && $token==$GLOBALS['TL_CONFIG']['value_coupon_black_friday']){
                            	$error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }elseif(!$coupon->checkArtikel()){
                            	    $error = true;
                            	    $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                        }
                    }
                    
                    if(!$this->Template->errorCoupon)
                        $this->Template->couponMsg = "Ihr Gutschein wurde akzeptiert!<br />Der Betrag wird auf der übernächsten Seite abgezogen.";
			    }
            }
            
            if($_POST["submitter"])
            {
                $error = false;
                
                //Validate Coupon
			    if($_POST["coupons_used"])
			    {
			        $token = $_POST["coupons_used"];
			        $coupon= new PnCoupon();
			        if(!$coupon->checkExist($token, $order->id))
                    {
                        $error = true;
                        $this->Template->errorCoupon = "Es wurde leider kein Gutschein mit diesem Code gefunden.";
                    } 
                    else
                    {
                        $coupon->initWithToken($token);
                        if($coupon->checkExpire())
                        {
                            $error = true;
                            $this->Template->errorCoupon = "Dieser Gutschein ist leider bereits abgelaufen.";
                        }
                        else
                        {
                            if($coupon->checkUsedUp($order->id))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein wurde leider bereits verwendet.";
                            }
                            else if(!$coupon->canUseForOrder($order))
                            {
                                $error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                            else if(!$this->checkBlackFriday($order) && $token==$GLOBALS['TL_CONFIG']['value_coupon_black_friday']){
                            	$error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                            else if(!$coupon->checkArtikel()){
                            	$error = true;
                                $this->Template->errorCoupon = "Dieser Gutschein ist auf Ihre Bestellung leider nicht einlösbar. 
                                                                Bitte beachten Sie die Einlösebedingungen, welche Ihnen bei Erhalt des 
                                                                Gutscheines mitgeteilt wurden. Sollte dies ein Fehler sein, wenden Sie 
                                                                sich bitte an unseren Kundendienst";
                            }
                        }
                    }
                    
                    if(!$this->Template->errorCoupon)
                        $this->Template->couponMsg = "Ihr Gutschein wurde akzeptiert!<br />Der Betrag wird auf der übernächsten Seite abgezogen.";
			    }
			    
			    //Validate Email
			    /*$this->Template->email = $_POST['customer-customer_email'];
			    if(!$_POST['customer-customer_email'] || preg_match("/[.+a-zA-Z0-9_-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $_POST['customer-customer_email']) == 0)
			    {
			        $error = true;
			        $this->Template->errorEmail = true;
			        $this->Template->email = $_POST['customer-customer_email'];
			    }*/
                
			    if(!$error)
			    {
                    //Save customer info
                    $customer_name = "";
                    $customer_address = "";
                    $customer_city = "";
                    $customer_state = "";
                    $customer_postal_code = "";
                    $customer_country = "";
                    $customer_phone = "";
                    //$customer_email = $_POST["customer-customer_email"];
                    $purchase_contract_id = $_GET["session"] != "" ? $_GET["session"] : $_SESSION['amazon_session'];
                    $_SESSION['amazon_session'] = $purchase_contract_id;
                    
                    try 
                    {
                        $lib = new PnAmazonCBAPurchaseContract();
                        $address_list = $lib->getAddress($purchase_contract_id);
                        foreach( $address_list as $address)
                        {
                            $customer_name = $address->getName();
                            $customer_address = $address->getAddressLineOne();
                            $customer_city = $address->getCity();
                            $customer_state = $address->getStateOrProvinceCode();
                            $customer_postal_code = $address->getPostalCode();
                            $customer_country = $address->getCountryCode();
                            $customer_phone = $address->getPhoneNumber();
                        }
                    }
                    catch(Exception $ex)
                    {
                        throw $ex;
                    }
                    
                    $customer = new PnCustomer();
                    if($customer->existEmail($customer_email))
                    {
                        $customer_id = $customer->id;
                    }
                    else
                    {
                        $customer_id = $customer->add(array(
                                        "customer_vorname" 	=> $customer_name,
                                        "customer_nachname"	=> "",
                                        //"customer_email" 	=> $customer_email,
                                        "customer_anrede" 	=> ""
                                    ));
                    }
                    
                    //Get country id from country code
                    $db_country = $this->Database->prepare("
                        SELECT country_id FROM " . $this->config->database . ".countries
                        WHERE country_code = ? AND country_active = 1
                    ")->execute($customer_country);
                    
                    if($db_country->numRows > 0)
                    {
                        while($db_country->next())
                        {
                            $customer_country_id = $db_country->country_id;
                        }
                        
                        $address = new PnAddress();
                        
                        //Address exist -> update
                        if($order->shipping_address_id != '' && $order->shipping_address_id > 0)
                        {
                            $address->initWithId($order->shipping_address_id);
                            $address->edit(
                                array(
                                        "address_vorname" 		=> $customer_name,
                                        "address_nachname" 		=> "",
                                        "address_firma" 		=> "",
                                        "address_address" 		=> $customer_address,
                                        "address_city" 			=> $customer_city,
                                        "address_zip" 			=> $customer_postal_code,
                                        "country_id" 			=> $customer_country_id,
                                        "address_anrede" 		=> ""
                                    )
                                );
                            
                            $address_id = $address->id;
                        }
                        else //Add new
                        {
                            $address_id = $address->add(
                                array(
                                        "address_vorname" 		=> $customer_name,
                                        "address_nachname" 		=> "",
                                        "address_firma" 		=> "",
                                        "address_address" 		=> $customer_address,
                                        "address_city" 			=> $customer_city,
                                        "address_zip" 			=> $customer_postal_code,
                                        "country_id" 			=> $customer_country_id,
                                        "address_anrede" 		=> ""
                                    )
                                );
                        }
                        
                        $order->update(
                                array(
                                    "customer_id" => $customer_id,
                                    "billing_address_id" => $address_id,
                                    "shipping_address_id" => $address_id,
                                    "payment_method_id" => 12,
                                    "shipping_id" => 1
                                    )
                                );
                        
                        //Coupon
                        if($_POST["coupons_used"])
                        {
                            $token  = $_POST["coupons_used"];
                            $coupon = new PnCoupon();
                            $coupon->initWithToken($token);
                            $coupon->useForOrder($order->id);
                        }
                        else
                        {
                            $objDb = $this->Database->prepare("
                                SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                                WHERE order_id = ?
                            ")->execute($order->id);
                            
                            if($objDb->numRows > 0)
                            {
                                $objDb = $this->Database->prepare("
                                    DELETE FROM " . $this->config->database . ".coupons_used_on_orders
                                    WHERE order_id = ?
                                ")->execute($order->id);
                                
                                $order->recalc();
                            }
                        }
                        
                        //Order Invoice
                        if($_POST["order_invoice"])
                        {
                            $order->edit(array("order_invoice" => 0));
                        }
                        else
                        {
                            $order->edit(array("order_invoice" => 1));
                        }
                        
                        
                        //Newsletter
                        $db_newsletter = $this->Database->prepare("
                                                SELECT * FROM " . $this->config->database . ".newsletter
                                                WHERE news_email = ? 
                                        ")->execute($customer_email);
                        
                        if($_POST["customer-newsletter"])
                        {
                            if($db_newsletter->numRows > 0)
                            {
                                $this->Database->prepare("
                                    UPDATE " . $this->config->database . ".newsletter
                                    SET news_unsubscribed = NULL
                                    WHERE news_email = ?
                                ")->execute($customer_email);
                            }
                            else
                            {
                                $this->Database->prepare("
                                    INSERT INTO " . $this->config->database . ".newsletter
                                    (news_id, customer_id, news_email, news_submitted, news_confirmed)
                                    VALUES (?, ?, ?, ?, ?)
                                ")->execute("", $order->customer_id, $customer_email, date('Y/m/d H:i:s a', time()), date('Y/m/d H:i:s a', time()));
                            }
                        }
                        else
                        {
                            if($db_newsletter->numRows > 0)
                            {
                                $this->Database->prepare("
                                    UPDATE " . $this->config->database . ".newsletter
                                    SET news_unsubscribed = ?
                                    WHERE news_email = ?
                                ")->execute(date('Y/m/d H:i:s a', time()), $customer_email);
                            }
                        }
                        
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($order->id);
                            $order->edit(array("order_step" => "confirm"));
                            header(sprintf("Location: %s?order=%s&step=%s&token=%s", $checkout_page, $order->id, $arStepV[3], $token));
                        }
                        else
                        {
                            $order->edit(array("order_step" => "confirm"));
                            header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, $arStepV[3]));
                        }
                    }
                    else
                    {
                        $this->Template->error_shipping_address = true;
                    }
                }
            }
            
            //Load cached info
            if($order->customer_id && $order->customer_id > 0)
			{
			    $customer = new PnCustomer();
			    $customer->initWithId($order->customer_id);
			    $this->Template->email = $customer->email;
			    
			    $coupon = new PnCoupon();
                $objCoupon = $this->Database->prepare("
                        SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                        WHERE order_id = ?
                    ")->execute($order->id);
                while($objCoupon->next())
                {
                    $coupon_id = $objCoupon->coupon_id;
                }
                if($coupon_id > 0)
                {
                    $coupon->initWithId($coupon_id);
                    $this->Template->couponCode = $coupon->token;
                    $couponValue = $coupon->wert;
                    if($coupon->coupontype_id == 3){
						$couponValue = $coupon->wert + $coupon->wert2;
					}
                    $orderPriceWithoutCoupon = $order->getTotalPriceWithoutCoupon("de", true);

                    if($coupon->prozent > 0)
                    {
                        $couponValue = round($orderPriceWithoutCoupon * min(100, max(0, $coupon->prozent)) / 100, 2);
                    }
                    
                    if($couponValue > $orderPriceWithoutCoupon)
                        $couponValue = $orderPriceWithoutCoupon;
                    
                    $this->Template->couponToken = $coupon->token;
                    $this->Template->couponValue = -1 * floatVal($couponValue);
                    if(!$this->Template->errorCoupon)
                        $this->Template->couponMsg = "Ihr Gutschein wurde akzeptiert!<br />Der Betrag wird auf der übernächsten Seite abgezogen.";
                }
                
                if(!$order->invoice || $order->invoice == 0)
                {
                    $this->Template->invoice   = true;
                }
                
                $objNewsletter = $this->Database->prepare("
                    SELECT * FROM " . $this->config->database . ".newsletter
                    WHERE news_email = ? AND news_unsubscribed IS NULL
                ")->execute($customer->email);
                
                if($objNewsletter->numRows > 0)
                    $this->Template->newsletter = true;
            }
		}
	}
	
	function sendMailToCustomer($email, $subject, $objTemplate)
	{
	    $objEmail = new Email();
	    $objEmail->from = $GLOBALS['TL_CONFIG']['adminEmail'];
		$objEmail->subject = $subject;
		$objEmail->html = $objTemplate->parse();
		$objEmail->sendTo($email);
	}
	
	function changePaymentMethod()
	{
	    $order = new PnOrder();
	    $order->initWithId($_GET["order"]);
	    
	    if($order->payment_method_id == 12)
	        header("Location: /"); //Amazon not allow
	    
	    if($order->paid)
	    {
	        $myorder_page = $GLOBALS['TL_CONFIG']['myorder_page'] ? $GLOBALS['TL_CONFIG']['myorder_page'] : "/myorder";
            header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
	    }
	    
	    if(!$order->id || $order->id == 0)
	        gotoErrorPage();
	    
	    $this->Template->payment_id = $order->payment_method_id;
	}
	function checkBlackFriday($order){
		if ($GLOBALS['TL_CONFIG']['black_friday']){
				//List items in array
				    $array_list_35 = $GLOBALS['TL_CONFIG']['list_book_black_friday'];
					$listItems = $order->getBookItems();
					if(count($listItems)>0){
						foreach($listItems as $items_new){
							if(in_array($items_new->book_id,$array_list_35)){
								return true;
							}
						}
					}
			}
		 return false;
	}
	public function removeAllItems($order){
		if(count($order->getItems()) > 0){
			$bookItems   = $order->getBookItems();
			if(count($bookItems) > 0){
				foreach($bookItems as $bookItem_child){
					if($bookItem_child->item_paper == 99){
						$bookItem = new PnBookItem();
						$bookItem->initWithId($bookItem_child->id);
						$bookItem->removeChildItem();
						$bookItem->remove();
					}
				}
			}
			$wineItems   = $order->getWineItems();
			if(count($wineItems) > 0){
				foreach($wineItems as $wineItem_child){
					if($wineItem_child->item_paper == 99){
						$wineItem = new PnWineItem();
						$wineItem->initWithId($wineItem_child->id);
						$wineItem->removeChildItem();
						$wineItem->remove();
					}
				}
			}
		}
	}
    
    //Sendmail
    public function sendMail($arData,$subject,$step,$orderID){

        $objEmail = new Email();
        $objEmail->from = $GLOBALS['TL_ADMIN_EMAIL'];
        $objEmail->fromName = ($arData ) ? $arData['username']:$GLOBALS['TL_ADMIN_NAME'];
        $objEmail->subject = $subject.'- Order:'.$orderID.'-'.$step;
        $msg = '<p>'.($arData) ? $arData['message']:''.'</p>';
        $msg .= '<p>';
        
        $objEmail->html = $msg;

        $objEmail->sendTo('technik@personalnovel.de');
        return true;
    }
}

?>
