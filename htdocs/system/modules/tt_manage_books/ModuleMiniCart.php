<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/*
 * Pn Classes
 */
require_once("class/class.pn.php");

class ModuleMiniCart extends Module
{
	protected $strTemplate 		= 'mod_mini_cart';
	protected $config;
	
	public function generate()
	{
	    $this->config = new PnConfig();
	    
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### MINI CART ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
		    
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
		$order = new PnOrder();
		if($_COOKIE['PN_order_id'] != '')
		{
			$order->initWithId($_COOKIE['PN_order_id']);
		}
		
		if(!$order->id || $order->id == "" || $order->id == 0)
		{
		    //Remove cookie
		    setcookie ("PN_order_id", "", time() - 3600, "/",$domain_cookie);
		}
		
		$this->Template->order            = $order;
		/*$this->Template->numOfBookItems   = $order->getNumOfBookItems();
		$this->Template->numOfAddonItems  = $order->getNumOfAddonItems();
		$this->Template->numOfWineItems   = $order->getNumOfWineItems();*/
		$this->Template->totalPrice       = $order->getTotalPriceWithoutCoupon();
		$this->Template->bookItems        = $order->getBookItems();
        $this->Template->wineItems        = $order->getWineItems();
        if($order->shipping_id && $order->shipping_id > 0)
        {
            $shipping = new PnShipping();
            $shipping->initWithId($order->shipping_id);
            $this->Template->shipping = $shipping;
        }
		
		$objDb = $this->Database->prepare("
		        SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
		        WHERE order_id = ?
		    ")->execute($order->id);
		    
		if($objDb->numRows > 0)
		    $this->Template->coupon = true;
		
		$checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
		
		if($GLOBALS['TL_CONFIG']['use_url_token'])
        {
            $token = generateToken($order->id);
            $this->Template->linkCheckout = "http://".$domain_cookie.sprintf("%s?order=%s&step=%s&token=%s", $checkout_page, $order->id, "Warenkorb", $token);
        }
        else
        {
            $this->Template->linkCheckout = "http://".$domain_cookie.sprintf("%s?order=%s&step=%s", $checkout_page, $order->id, "Warenkorb");
		}
		
		//Hide link cart when custom step 2
		if(!isset($_GET['edit']) && !isset($_GET["fedit"]) && isset($_GET['step']) && intval($_GET['step']) > 0 ){
			 $this->Template->linkCheckout = '';
		}
		
	}
}

?>
