<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once("class/class.pn.php");

class ModuleCheckoutLogo extends Module
{
	protected $strTemplate = 'mod_checkout_logo';
	
	public function generate()
	{
		return parent::generate();
	}
	
	protected function compile()
	{
		$url 		= $_COOKIE['urllogo']?$_COOKIE['urllogo']:"";
		$url_arr 	= explode("|",$url);
		
        $this->Template->link_logo     = $url_arr[0]?$url_arr[0]:'';
        $this->Template->view_logo     = $url_arr[1]?$url_arr[1]:'';
	}
	
}

?>