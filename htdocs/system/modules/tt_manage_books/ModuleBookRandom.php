<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');
/*
 * Comment: Book Listing
 * Logic:  Hien thi danh sach book o tung menu
 * Ten Task
 * 1. Menu kinderbucher
 * 2. Menu Jugendbucher
 * 3. Menu Literaturklassiker
 * 4. Menu Geschenkbucher
 * 5. Menu Romane
 */

require_once("class/class.pn.php");

class ModuleBookRandom extends Module
{
	public $strTemplate = 'mod_book_random';
	protected $arrTagCategories = array();
	protected $arrBooks = array();
	public $arrFilters = array();
	public $sitepath = "";
	public $limitCopyImg = 12;
	public $flaLimit = 0;
	
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BOOKS RANDOM LIST ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		if(TL_MODE == 'FE')
		{
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			//$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/PerspectiveTransform.js';
			
		}

		return parent::generate();
	}
	
	protected function compile()
	{
		$this->config = new PnConfig();
		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
		$domain        = "http://".$domain_cookie; 
		
		$page 			= $this->pn_page_cur ? "/".$this->pn_page_cur : "";
		$this->sitepath = $this->pn_sitepath ? $this->pn_sitepath : 1;
		$objSelect 		= "AND s.site_id = ".$this->sitepath;
		
		
		//get array filter
		$arrTmp					= deserialize($this->pn_filter_module);
		if(count($arrTmp)>0){
			foreach($arrTmp as $k=>$v)
			{
				if($v[0] != "")
					$this->arrFilters[$v[0]][] = $v[1];
			}
		}
		
		/*
		 * Get tag params
		 */
		
		$i = 1;
		$filterTaglist = "";
		$filterTaglistWhere = "";
		if(count($this->arrFilters) > 0)
		{
			$filterTaglist .= " INNER JOIN " . $this->config->database . ".artikel_buchtags AS artikel_buchtags_total";
	    	$filterTaglist .= " ON artikel_buchtags_total.artikel_id = f.artikel_id";
	    	
	    	$filterTaglist .= " INNER JOIN " . $this->config->database . ".buch_tags2 AS buch_tags2_total";
	    	$filterTaglist .= " ON buch_tags2_total.tag_id = artikel_buchtags_total.tag_id";
	    	
	    	$filterTaglist .= " INNER JOIN " . $this->config->database . ".tag_categories AS tag_categories_total";
	    	$filterTaglist .= " ON tag_categories_total.category_id = buch_tags2_total.category_id";
		    	
		    foreach($this->arrFilters as $k => $v)
		    {
		        foreach($v as $k1 => $v1)
		        {
		        	if($i > 1){
			           $filterTaglistWhere .= " OR (buch_tags2_total.tag_value = '" . $v1 . "' AND tag_categories_total.category_name = '" . $k . "' )";
		        	}
		        	else
		        	{
		        		$filterTaglistWhere .= "(buch_tags2_total.tag_value = '" . $v1 . "' AND tag_categories_total.category_name = '" . $k . "' )";
		        	}
		            
		            $i++;
		        }
		    }
		}
		
		if($filterTaglistWhere != '') $filterTaglistWhere = "(".$filterTaglistWhere.") AND";
		
		
		$query =sprintf("
				SELECT *, b.buch_id
				FROM " . $this->config->database . ".finanz_artikel f
				LEFT OUTER JOIN " . $this->config->database . ".buecher b
				ON f.buch_id = b.buch_id
				LEFT OUTER  JOIN " . $this->config->database . ".site_book sb
				ON b.buch_id = sb.`buch_id`
				LEFT OUTER JOIN " . $this->config->database . ".sites s
				ON sb.site_id = s.site_id
				%s
				WHERE 
				%s
				(
					b.buch_id IS NULL OR
					b.buch_pub = 1 
					%s
					AND b.buch_hidden = 0 
					AND b.buch_id NOT IN (411)
				)
				AND f.artikel_active = 1 
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				
				AND f.class_id = 2 
				
				AND f.version_id NOT IN (4, 5, 6, 1303, 1304, 1305)
				
				GROUP BY b.buch_id
				ORDER BY b.buch_lang != 'de', b.type_id, b.buch_created DESC, b.buch_id DESC
				LIMIT 0,20
			",$filterTaglist,$filterTaglistWhere,$objSelect);
		
		//print_r($query);die();
		$obj = $this->Database->prepare($query);
		//print_r($obj);die();
		$obj = $obj->execute();
		//print_r($obj);die();
		while ($obj->next())
		{
			$objSubHeadline = $this->Database->prepare("select * from " . $this->config->database . ".buch_entities b where b.`variant_id` in (select b.`variant_id` from " . $this->config->database . ".buch_variant b inner join " . $this->config->database . ".`buch_buchvariant` bb on b.`variant_id` = bb.`variant_id` where b.`variant_public`=1  and bb.`buch_id`=?)")->execute($obj->buch_id);	
			$objpersonen = $this->Database->prepare("select * from " . $this->config->database . ".book_data where buch_id=?")->execute($obj->buch_id);
			$subheadline	= $GLOBALS['TL_LANG']['MOD']['book_listing']['one_person'];
			$personen = $objpersonen->personen ? $objpersonen->personen : $objSubHeadline->numRows;
			if($objSubHeadline->numRows > 1 || $objpersonen->personen>1) {
				$subheadline 	= sprintf($GLOBALS['TL_LANG']['MOD']['book_listing']['num_person'], $personen);
			}
			
			if($obj->buch_untertitel_webseite != "") {
				$subheadline = $obj->buch_untertitel_webseite;
			}
			
			$detail_url		= $domain.$page."/book/" . $obj->buch_id."/".standardize($obj->buch_titel);
			$pk = $obj->artikel_id;
			$preview_url2	= sprintf($this->config->image_root . "/preview/BookArtikel/%s/%s.png?height=%s&shadow=%s", $pk, md5($GLOBALS['TL_CONFIG']['site_secret'] . "BookArtikel" . $pk), 190, 0);
			
			$preview_url = $this->getImageUrl($obj->buch_id,"detail",$preview_url2, standardize($obj->buch_titel));
			
			$this->arrBooks[] = array(
				'id' 			=> $obj->buch_id,
				'headline'		=> $obj->buch_titel,
				'subheadline'	=> $subheadline,
				'preview_url'	=> $preview_url,
				'detail_url'	=> $detail_url
			);
			
		}
		$this->Template->books = $this->arrBooks;
	
		
	}
	public function getImageUrl($id, $etx, $url_img, $title) {
		if($this->flaLimit != $this->limitCopyImg){
			
			if(!in_array($etx, array('detail','view','list'))){
				$etx = "detail";
			}
			$check = true;
			$pageURL = 'http';
			
			if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
			
			$pageURL .= "://";
			$file_image = TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png";
			
			if(!is_file($file_image)){
				try{
				$file_headers = @get_headers($url_img);
				if($file_headers[0] != 'HTTP/1.1 404 Not Found' and $file_headers[0] != 'HTTP/1.1 500 INTERNAL SERVER ERROR')
					copy($url_img, TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png");
					else{
						$check = false;
					}
				$this->flaLimit = $this->flaLimit + 1;
				}catch (Exception $e) {
					$check = false;
				}
			}
			if($check != false )
			$url_img = $pageURL.$_SERVER["SERVER_NAME"]."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png";
			
			return $url_img;
		}
		return $url_img;
	}
	
}

?>