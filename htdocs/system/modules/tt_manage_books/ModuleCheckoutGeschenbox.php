<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once("class/class.pn.php");

class ModuleCheckoutGeschenbox extends Module
{
	protected $strTemplate = 'mod_book_checkout_geschenkbox';
	public $domain;
	public function generate()
	{
		if (TL_MODE == 'BE')
			{
				$objTemplate = new BackendTemplate('be_wildcard');
				$objTemplate->wildcard = '### GESCHENKBOX CHECKOUT ###';
			}
		if(TL_MODE == 'FE')
			{
			
			}
		return parent::generate();
	}
	
	protected function compile()
	{
		$checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
		$this->domain   = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
		$redirectTo = $this->redirectJumpTo($this->jumpTo);
		if($_GET["order"]){
			header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
			exit();
		}
		/*create items book and wine*/
		if($_GET['wine'] && $_GET['book']){
			$order = $this->getOrCreateOrder();
			if(count($order->getItems()) > 0){
				$this->removeAllItems($order);
			}
			/*create book default*/
			if($_GET['book'])
	            {
	                $book = new PnBook();
	                $book->current($_GET['book']);
	                
	                //Create default item
	                $bookItem         = $this->createBookItem($order, $book);
	                if($bookItem)
	                {
	                    $widmungItem  = $this->createWidmungItem($order, $bookItem);
                        $designItem   = $this->createDesignItem($order, $bookItem);
	                }
	            }
	        /*create wine default*/
			if($_GET['wine']){
				$wineitem = $this->getOrCreateWineItem($order->id,$_GET['wine']);
		        $designitem = $this->getOrCreateDesignItem($order->id, $wineitem->id,$_GET['wine']);
			}
			/*redirect checkout for config again items geschenkbox*/
			header(sprintf("Location: %s?order=%s", $checkout_page, $order->id));
			exit();
		}
	}
	function getOrCreateOrder()
	{
	    $order = new PnOrder();
	    
	    if($_COOKIE['PN_order_id'] != '')
		{
			$order->initWithId($_COOKIE['PN_order_id']);
		}
		
		if($order->id == "" || $order->id == 0)
		{
		    $id = $order->add(
		                    array(
		                        "order_status"    => 0,
                                "order_date"      => date('Y/m/d H:i:s a', time()),
                                "order_created"   => date('Y/m/d H:i:s a', time()),
                                "order_updated"   => date('Y/m/d H:i:s a', time()),
                                "order_converted" => 1,
                                "order_version"   => 4,
                                "order_invoice"   => 1,
                                "locale_id"       => 1,
                                "mandant_id"      => 1,
                                "konto_id"        => $order->createKonto()->id,
                                "debitor_id"      => 1,
                                "partner_id"      => 1,
		                    	"payment_method_id"      => 3
                            )
						);
		    
		    $order->initWithId($id);
		    $order->edit(array("konto_id" => $order->createKonto()->id));
		    $order->createRandomKey();
		    
		    setcookie('PN_order_id', $order->id, time() + (86400 * 7), "/",$this->domain);
		}
		
		return $order;
	}
	function createBookItem($order, $book)
	{
	    $fields = array();
	    $invalidChars = array();
	    $emptyFields  = array();
	    
	    
        $arrEntities = $book->getEntities();
        foreach($arrEntities as $entity){
        	if(count($entity['fields']) > 0){
        		foreach($entity['fields'] as $field){
        			$name_field = $field['field_name'];
        			$default_value = $field['field_default'];
        			if(is_array($default_value))
					{
						foreach($default_value as $k1 => $v1)
						{
							$k1 = "[" . $k1 . "]";
							$fields[utf8_decode(utf8_encode($k1))] = utf8_decode(utf8_encode($v1));
						}
					}
					else
					{
						$fields[utf8_decode(utf8_encode($name_field))] = utf8_decode(utf8_encode($default_value));
					}
        		}
        	}
        }
        
        $bookDocuments = $book->getBookDocuments();
        if(count($bookDocuments) > 0)
        {
            $bookDocument = $bookDocuments[0];
            $bookDocumentId = $bookDocument->id;
        }
        
        $class_id = 2;
        $fields   = json_encode($fields);
        $bookItem = new PnBookItem();            
	    $book_id  = $bookItem->add(
	                      array(
                                    'order_id' 			=> $order->id,
                                    'buch_id'			=> $book->id,
                                    'class_id'			=> $class_id,
                                    'artikel_id'		=> $book->article_id,
                                    'font_id'			=> 1,
                                    'document_id'       => $bookDocumentId,
                                    'item_pieces'		=> 1,
                                    'item_print'		=> 0,
                                    'item_paper'		=> 99,
                                    'item_testprint'	=> 0,
                                    'item_fields'		=> $fields,
                                    'item_created'      => date('Y/m/d H:i:s a', time()),
                                    'item_updated'      => date('Y/m/d H:i:s a', time()),
                                    'item_touched'      => date('Y/m/d H:i:s a', time())
                              )
                      );
        
        $bookItem->initWithId($book_id);
        $bookItem->edit(array("item_fields" => $fields)); //Fix bug encoding. Don't know why?
        
        return $bookItem;
	}
	
	function createWidmungItem($order, $bookItem)
	{
	    $item     = new PnItem();
	    $item_id  = $item->add(
                       array(
                           "order_id"       => $order->id,
                           "class_id"       => 10,
                           "artikel_id"     => 8830,
                           "parent_id"      => $bookItem->id,
                           "item_pieces"    => 1,
                           "item_print"		=> 0,
                           "item_testprint"	=> 0,
                           "item_fields"	=> '{"text": "", "fontface": "Century Schoolbook L", "fontcolor": "black"}',
                           "item_created"   => date('Y/m/d H:i:s a', time()),
                           "item_updated"   => date('Y/m/d H:i:s a', time()),
                           "item_touched"   => date('Y/m/d H:i:s a', time())
                       )
	               );
	    
	    $item->initWithId($item_id);
	    return $item;
	}
	
	function createDesignItem($order, $bookItem)
	{
	    $book    = new PnBook();
	    $designs = $book->getBookDesigns($bookItem->article_id);
	    
	    if(count($designs) > 0)
	        $design  = $designs[0];
	    else
	        gotoErrorPage();
	    
	    $item    = new PnItem();
	    $field   = array(
	            
	        );
	    $item_id = $item->add(
                        array(
                            "order_id"       => $order->id,
                            "class_id"       => 3,
                            "artikel_id"     => $design->artikel_id,
                            "parent_id"      => $bookItem->id,
                            "item_pieces"    => 1,
                            "item_print"	 => 0,
                            "item_testprint" => 0,
                            "item_fields"	 => '{}',
                            "item_created"   => date('Y/m/d H:i:s a', time()),
                            "item_updated"   => date('Y/m/d H:i:s a', time()),
                            "item_touched"   => date('Y/m/d H:i:s a', time())
                        )
                    );
	}
	
	protected function getOrCreateWineItem($order_id,$artikel_id=false)
	{
	    $item = new PnItem();
	    if($artikel_id)
	    {
	        $item_fields = array(
                                "text" => "",
                                "fontface" => "Century Schoolbook L",
                                "fontcolor" => "black"
                            );
            $article = new PnArticle();
            $article->initWithId($artikel_id);
            $etiquettes = $article->getEtiquettes();
            $list_etiquettes = array();
            foreach($etiquettes as $etiquette)
			{
				$list_etiquettes[$etiquette->photo_allowed][$etiquette->getTagName()][] = $etiquette;
			}
			$etiquette_id ='';
			$i = 0;
			foreach($list_etiquettes as $k => $v){
				if($i<1){
					foreach($v as $k1 => $v1){
						if($i<1){
							foreach($v1 as $k2 => $v2){
								$etiquette_id = $v2->id;
								$i++;
								break;
							}
						}
					}
				}
			}
			
	        $item_id = $item->add(
                    array(
                        "order_id" => $order_id,
                        "class_id" => 14,
                        "artikel_id" => $artikel_id,
                        "item_pieces" => 1,
                        "item_print" => 0,
                        "item_fields" => json_encode($item_fields),
                        "etiquette_id" => $etiquette_id,
                        "item_image1_present" => 0,
                        "item_image1_cropped" => 0,
                        "order_flow_id" => 20,
                        "item_paper"	=> 99
                    )
	            );
	        $item_id2 = $item->add(
                    array
                        (
                            "order_id"          => $order_id,
                            "artikel_id"        => 850000,
                            "class_id"          => 15,
                            "item_pieces"       => 1,
                            "item_fields"       => "{}",
                            "parent_id"         => $item_id,
                            "item_created"      => date('Y/m/d H:i:s a', time()),
                            "item_updated"      => date('Y/m/d H:i:s a', time()),
                            "item_touched"      => date('Y/m/d H:i:s a', time()),
                            "item_paper"		=> 99
                        )
                    );
	        
	        $item->select($item_id);
	    }
	    
	    return $item;
	}
	protected function getOrCreateDesignItem($order_id, $parent_id,$artikel_id)
	{
	    	$item = new PnItem();
	        $item_id = $item->add(
                    array(
                        "order_id" => $order_id,
                        "class_id" => 18,
                        "artikel_id" => $item->getDesignItemWineDefault($artikel_id),
                        "parent_id" => $parent_id,
                        "item_pieces" => 1,
                        "item_print" => 0,
                        "item_fields" => "{}",
                        "item_image1_present" => 0,
                        "item_image1_cropped" => 0,
                        "order_flow_id" => 20
                    )
	            );
	        
	        $item->select($item_id);
	    return $item;
	}
	public function redirectJumpTo($jumpto)
	{
		$strRedirect = "/";
		if (strlen($jumpto))
		{
			$objNextPage = $this->Database->prepare("SELECT id, alias FROM tl_page WHERE id=?")
										  ->limit(1)
										  ->execute($jumpto);
			if ($objNextPage->numRows)
			{
				$strRedirect = $this->generateFrontendUrl($objNextPage->fetchAssoc());
			}
		}
		return $strRedirect;
	}
	public function removeAllItems($order){
		if(count($order->getItems()) > 0){
			$bookItems   = $order->getBookItems();
			if(count($bookItems) > 0){
				foreach($bookItems as $bookItem_child){
						$bookItem = new PnBookItem();
						$bookItem->initWithId($bookItem_child->id);
						$bookItem->removeChildItem();
						$bookItem->remove();
				}
			}
			$wineItems   = $order->getWineItems();
			if(count($wineItems) > 0){
				foreach($wineItems as $wineItem_child){
						$wineItem = new PnWineItem();
						$wineItem->initWithId($wineItem_child->id);
						$wineItem->removeChildItem();
						$wineItem->remove();
				}
			}
		}
	}
	
}

?>
