<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

/*
 * Pn Classes
 */
require_once("class/class.pn.php");

class ModuleBookGenre extends Module
{
	protected $strTemplate 		= 'mod_book_genre';
	
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### BOOK GENRE ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		$this->config = new PnConfig();
		$query = sprintf("
		        SELECT *
		        FROM " . $this->config->database . ".buecher b
		        WHERE b.genre_id = %s
		        AND b.buch_titel LIKE '%%%s'
		        AND b.buch_pub = 1
		    ", $this->pn_genreid, $this->pn_booktitle);
		
		$books = $this->Database->prepare($query)->execute();
		
		$list_book = array();
		while($books->next())
		{
		    $book              = new PnBook();
		    $book->id          = $books->buch_id;
		    $book->title       = $books->buch_titel;
		    $book->author      = $books->buch_autor;
		    $book->type_id     = $books->type_id;
		    
		    $detail_url		   = "/index.php/book.html?item=" . $books->buch_id;
			if($this->jumpTo!=0){
				$detail_url    = "{{link_url::".$this->jumpTo."}}?item=" . $books->buch_id;
			}
			$book->detail_url  = $detail_url;
			
			$preview_url	   = sprintf($GLOBALS['TL_CONFIG']['image_root'] . "/preview/Book/%s/%s.png?height=%s&shadow=%s", $book->id, 
			    md5($GLOBALS['TL_CONFIG']['site_secret'] . "Book" . $book->id), $this->pn_image_height, $this->pn_image_shadow);
			$book->preview_url = $preview_url;
		    
		    $list_book[]       = $book;
		}
		
		$this->Template->books = $list_book;
	}
}

?>
