<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');
/*
 * Comment: Book Listing
 * Logic:  Hien thi danh sach book o tung menu
 * Ten Task
 * 1. Menu kinderbucher
 * 2. Menu Jugendbucher
 * 3. Menu Literaturklassiker
 * 4. Menu Geschenkbucher
 * 5. Menu Romane
 */

require_once("class/class.pn.php");

class ModuleBookListing extends Module
{
	public $strTemplate = 'mod_book_listing';
	protected $arrTagCategories = array();
	protected $arrBooks = array();
	public $arrFilters = array();
	public $sitepath = "";
	public $limitCopyImg = 12;
	public $flaLimit = 0;
	
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BOOKS LIST ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
			$this->strTemplate = $this->pn_template;
			
			if($this->strTemplate != "mod_book_listing_teaser")
			{
                $GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/filter_panel.css|screen';
                $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
                $GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/filter_panel.js';
			}
		}

		return parent::generate();
	}
	
	protected function compile()
	{
		$this->config = new PnConfig();
		if($this->pn_ebook and $this->pn_ebook != 0){
			$this->Template->allow_ebook = true;
		}
		$taglist = $this->Database->prepare("select * from " . $this->config->database . ".tag_categories")->execute();
		$arrTaglist = array();
		while($taglist->next()) 
		{
			$arrTaglist[] = $taglist->category_name;
			$arrTaglist[] = $taglist->category_name_en;
		}

		/*
		 * Get tag params
		 */
		$arrTagId = array(0);
		$filterTaglist = "";
		$request_query_string = $_SERVER['QUERY_STRING'];
		$request_query_string = str_replace("=", "[]=", $request_query_string);
		
		$arrParse = array();
		parse_str($request_query_string, $arrParse);
		
		foreach($arrParse as $k => $v)
		{
		    $k = str_replace("_", " ", $k);
		    $arrtemp = array();
		    if(in_array($k, $arrTaglist))
			{
                foreach($v as $v1)
                {
                    $arrtemp[] = $v1;
                }
                
                $this->arrFilters[$k] = array_unique($arrtemp);
                
		    }
		}
		
		$i = 1;
		$filterTaglist = "";
		if($this->arrFilters > 0)
		{
		    foreach($this->arrFilters as $k => $v)
		    {
		        foreach($v as $k1 => $v1)
		        {
		            $filterTaglist .= "
		                INNER JOIN " . $this->config->database . ".artikel_buchtags AS artikel_buchtags_" . $i . " 
                        ON artikel_buchtags_" . $i . ".artikel_id = f.artikel_id 
                        
                        INNER JOIN " . $this->config->database . ".buch_tags2 AS buch_tags2_" . $i . " 
                        ON buch_tags2_" . $i . ".tag_id = artikel_buchtags_" . $i . ".tag_id AND buch_tags2_" . $i . ".tag_value = '" . $v1 . "' 
                        
                        INNER JOIN " . $this->config->database . ".tag_categories AS tag_categories_" . $i . " 
                        ON tag_categories_" . $i . ".category_id = buch_tags2_" . $i . ".category_id AND tag_categories_" . $i . ".category_name = '" . $k . "' 
		            ";
		            
		            $i++;
		        }
		    }
		}
		
		/*$book_type = "(1, 3, 4, 5, 7, 9, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)";
		if($this->pn_type == 1) { //Novels
			$book_type = "(1, 3, 4, 5, 7, 9, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)";
		} else if ($this->pn_type == 2) { //Children books
			$book_type = "(2, 13)";
		} else if ($this->pn_type == 3) { //gift_books
			$book_type = "(11, 12)";
		} else if ($this->pn_type == 4) { //classic_literature
			$book_type = "(15, 18, 23)";
		} else if ($this->pn_type == 5) { //E-Books
			$book_type = "(18, 23)";
		}*/
		
		$site_path_condition = sprintf("
                                AND 
                                (
                                    s.site_path IS NULL 
                                    OR s.site_path = '%s'
                                )
                                ", $this->sitepath);
                                
		if($this->sitepath != "romane")
		{
		    $site_path_condition = sprintf("AND 
                                    (
                                        s.site_path = '%s'
                                    )
                                    ", $this->sitepath);
		}
		
		$query = sprintf("
				SELECT *, b.buch_id
				FROM " . $this->config->database . ".finanz_artikel f
				LEFT OUTER JOIN " . $this->config->database . ".buecher b
				ON f.buch_id = b.buch_id
				LEFT OUTER  JOIN " . $this->config->database . ".site_book sb
				ON b.buch_id = sb.`buch_id`
				LEFT OUTER JOIN " . $this->config->database . ".sites s
				ON sb.site_id = s.site_id
				
				%s

				WHERE 
				(
					b.buch_id IS NULL OR
					b.buch_pub = 1 
					AND b.buch_hidden = 0 
					AND b.type_id NOT IN (8, 10, 21) 
					AND b.buch_id NOT IN (411)
				)
				AND f.artikel_active = 1 
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				
				AND f.class_id = 2 
				
				%s
				
				AND f.version_id NOT IN (4, 5, 6, 1303, 1304, 1305)
				
				GROUP BY b.buch_id
				ORDER BY b.buch_lang != 'de', b.type_id, b.buch_created DESC, b.buch_id DESC
			", $filterTaglist, $site_path_condition );
		
		
		if($this->pn_type == 5)
		{
		    $query = sprintf("
				SELECT *, b.buch_id
				FROM " . $this->config->database . ".finanz_artikel f
				LEFT OUTER JOIN " . $this->config->database . ".buecher b
				ON f.buch_id = b.buch_id
				LEFT OUTER  JOIN " . $this->config->database . ".site_book sb
				ON b.buch_id = sb.`buch_id`
				LEFT OUTER JOIN " . $this->config->database . ".sites s
				ON sb.site_id = s.site_id
				
				%s

				WHERE 
				(
					b.buch_id IS NULL OR
					b.buch_pub = 1 
					AND b.buch_hidden = 0 
					AND b.type_id NOT IN (8, 10, 21) 
					AND b.buch_id NOT IN (411)
					AND b.buch_ebook_legally_allowed = 1
				)
				AND f.artikel_active = 1 
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				
				AND f.class_id = 21 
				
				AND f.version_id = 101
				
				GROUP BY b.buch_id
				ORDER BY b.buch_lang != 'de', b.type_id, b.buch_created DESC, b.buch_id DESC
			", $filterTaglist);
			
		}
		
		$articlesTotal = $this->Database->prepare($query)->execute();
		$time = time();
		$offset = 0;
		$limit = null;
		$arrBookIds = array(0);
		$total = $articlesTotal->numRows;
		
		while($articlesTotal->next()) {
			if($articlesTotal->buch_id != "")
			{
				$arrBookIds[] = $articlesTotal->buch_id;
			}
		}
		
		$limit = $this->perPage;
		if(isset($_GET['limit'])){
			if($_GET['limit']=='all'){
				$limit = '9999';
			}else{
				$limit = $_GET['limit'];
			}
			
		}
		
		if ($limit > 0)
		{
			$page = $this->Input->get('page') ? $this->Input->get('page') : 1;
			// Check the maximum page number
			if ($page > ($total/$limit))
			{
				$page = ceil($total/$limit);
			}
			// Limit and offset
			//$limit = $this->perPage;
			$offset = ($page - 1) * $limit;
			// Add the pagination menu
			$objPagination = new Pagination($total, $limit);
			$this->Template->pagination = $objPagination->generate();
		}
		
		$obj = $this->Database->prepare($query);
		
		// Limit the result
		if (isset($limit))
		{
			$obj->limit($limit, $offset);
		}
		elseif ($total > 0)
		{
			$obj->limit($total, $offset);
		}
		
		$obj = $obj->execute();
		
		while ($obj->next())
		{
			$objSubHeadline = $this->Database->prepare("select * from " . $this->config->database . ".buch_entities b where b.`variant_id` in (select b.`variant_id` from " . $this->config->database . ".buch_variant b inner join " . $this->config->database . ".`buch_buchvariant` bb on b.`variant_id` = bb.`variant_id` where b.`variant_public`=1  and bb.`buch_id`=?)")->execute($obj->buch_id);	
			$objpersonen = $this->Database->prepare("select * from " . $this->config->database . ".book_data where buch_id=?")->execute($obj->buch_id);
			$subheadline	= $GLOBALS['TL_LANG']['MOD']['book_listing']['one_person'];
			$personen = $objpersonen->personen ? $objpersonen->personen : $objSubHeadline->numRows;
			if($objSubHeadline->numRows > 1 || $objpersonen->personen>1) {
				$subheadline 	= sprintf($GLOBALS['TL_LANG']['MOD']['book_listing']['num_person'], $personen);
			}
			
			if($obj->buch_untertitel_webseite != "") {
				$subheadline = $obj->buch_untertitel_webseite;
			}
			
			$teaser 	= "";
			$str_url    = sprintf("/%s/%s", $obj->buch_id, standardize($obj->buch_titel)); 
			$detail_url = $this->generateFrontendUrl(array("alias" => "book") , $str_url);
			
			if($this->jumpTo != 0) 
			{
			    $objJumpto  = $this->Database->prepare("select id, alias from tl_page where id = ?")->execute($this->jumpTo);
			    $detail_url = $this->generateFrontendUrl($objJumpto->row(), $str_url);
			}
			
			$objTeaser = $this->Database->prepare("select `exp_id`, `exp_text` from " . $this->config->database . ".expose as e where e.`exp_name` like 'Teaser' and e.`buch_id` = ?")->execute($obj->buch_id);
			while($objTeaser->next())
			{
				$teaser = $objTeaser->exp_text;
			}
			
			$pk = $obj->artikel_id;
			$preview_url	= sprintf($this->config->image_root . "/preview/BookArtikel/%s/%s.png?height=%s&shadow=%s", $pk, md5($GLOBALS['TL_CONFIG']['site_secret'] . "BookArtikel" . $pk), $this->pn_image_height, $this->pn_image_shadow);
			$preview_url = $this->getImageUrl($obj->buch_id,"list",$preview_url, standardize($obj->buch_titel));
			$this->arrBooks[] = array(
				'id' 			=> $obj->buch_id,
				'headline'		=> $obj->buch_titel,
				'subheadline'	=> $subheadline,
				'teaser'		=> $teaser,
				'preview_url'	=> $preview_url,
				'detail_url'	=> $detail_url,
				'ebook'		=>$obj->buch_ebook_legally_allowed
			);
		}
		
		$obj = $this->Database->prepare("
			SELECT `tag_categories`.`category_name`, `tag_categories`.`category_id`, `tag_categories`.`category_name_en`
			FROM " . $this->config->database . ".buch_tags2 
			INNER JOIN " . $this->config->database . ".tag_categories 
			ON tag_categories.category_id = buch_tags2.category_id 
			WHERE buch_tags2.tag_value
			!= '' AND tag_categories.category_name != ''
				AND tag_categories.category_name != 'Buchkategorie'
			group by `tag_categories`.`category_name`
			order by `tag_categories`.`category_name`
			")->execute();
			
		$objTags = $this->Database->prepare("
		    select DISTINCT bb.tag_id , count(bb.tag_id) as num_books, bt.tag_value, bt.category_id
            from " . $this->config->database . ".buch_buchtags bb
            left join " . $this->config->database . ".buch_tags2 bt
            on bb.tag_id = bt.tag_id
            where buch_id in (" . implode(",", $arrBookIds) . ") 
            and bb.`tag_id` not in (". implode(",", $arrTagId) .") 
            group by tag_id
		    ")->execute();

		$arrTagItems = array();
		while($objTags->next()) {
			$arrTagItems[] = array(
					"id"			=> $objTags->tag_id,
					"value"			=> $objTags->tag_value,
					"value_en"		=> $objTags->tag_value_en,
					"category_id"	=> $objTags->category_id,
					"num_books"     => $objTags->num_books
			);
		}
		
		while($obj->next()) {
			$arr				= array();
			$arr["id"]			= $obj->category_id;
			$arr["name"]		= $obj->category_name;
			$arr["name_en"]		= $obj->category_name_en;
			$arr["items"]		= array();
			$arr["num_books"]   = 0;
				
			foreach($arrTagItems as $item) {
				if($item["category_id"] == $arr["id"]) {
					$arr["items"][] = $item;
					if($item["num_books"] > $arr["num_books"])
					    $arr["num_books"] = $item["num_books"];
				}
			}

			$this->arrTagCategories[]	= $arr;
		}
		
		//TODO: sort tag categories
		/*
		* Fixme: hien tai sai 3 trang
		*/
		$arrCol1 = array();
		$arrCol2 = array();
		$arrCol3 = array();
		$arrCol4 = array();
		
		$arrTemp = array();
		$numItem = 0;
		foreach($this->arrTagCategories as $temp)
		{
			if(count($temp["items"]) > 1)
			{
				$arrTemp[] = $temp;
			}
		}
		
		$numRows = ceil(count($arrTemp) / 4);

		$count1 = 0;
		for($j = 0; $j < $numRows; $j++)
		{
			$arrCol1[] = $arrTemp[$j];
			$count1++;
		}
		

		
		$count2 = $count1;
		for($j = $count1; $j < $numRows + $count1; $j++)
		{
			$arrCol2[] = $arrTemp[$j];
			$count2++;
		}

		$count3 = $count2;
		for($j = $count2; $j < $numRows + $count2; $j++)
		{
			$arrCol3[] = $arrTemp[$j];
			$count3++;
		}
		
		for($j = $count3; $j < $numRows + $count3; $j++)
		{
			$arrCol4[] = $arrTemp[$j];
		}
		
		for($i = 0; $i < $numRows; $i++)
		{	
			$arrMerge[] = $arrCol1[$i]?$arrCol1[$i]:'';
			$arrMerge[] = $arrCol2[$i]?$arrCol2[$i]:'';
			$arrMerge[] = $arrCol3[$i]?$arrCol3[$i]:'';
			$arrMerge[] = $arrCol4[$i]?$arrCol4[$i]:'';
		}
		
		//save image if don't isset
		if(count($this->arrBooks)>0){
			foreach ($this->arrBooks as $books){
				$url_image 	= 	$books['preview_url'];
				$id_book 	= 	$books['id'];
				$ext		=	"list";
				//$link_image =	$this->getUrlImage($id_book,$ext,$url_image);
			}
		}
		//end
		$this->Template->TagCategories = $arrMerge;
		$this->Template->books = $this->arrBooks;
		$this->Template->result_from = $limit * $page - $limit + 1;
		$this->Template->result_to   = ($limit * $page) <= $total ? ($limit * $page) : $total;
		$this->Template->result_total= $total;
		$this->Template->result_tags= $this->arrFilters;
		$this->Template->url_path =$this->getLastPathSegment($_SERVER['PHP_SELF']);
		
	}
	
	public function getLastPathSegment($url) {
		$path = parse_url($url, PHP_URL_PATH); // to get the path from a whole URL
		$pathTrimmed = trim($path, '/'); // normalise with no leading or trailing slash
		$pathTokens = explode('/', $pathTrimmed); // get segments delimited by a slash
	
		if (substr($path, -1) !== '/') {
		    array_pop($pathTokens);
		}
		return end($pathTokens); // get the last segment
    }
	public function getImageUrl($id, $etx, $url_img, $title) {
		if($this->flaLimit != $this->limitCopyImg){
			
			if(!in_array($etx, array('detail','view','list'))){
				$etx = "detail";
			}
			$check = true;
			$pageURL = 'http';
			
			if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
			
			$pageURL .= "://";
			$file_image = TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png";
			
			if(!is_file($file_image)){
				try{
				$file_headers = @get_headers($url_img);
				if($file_headers[0] != 'HTTP/1.1 404 Not Found' and $file_headers[0] != 'HTTP/1.1 500 INTERNAL SERVER ERROR')
					copy($url_img, TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png");
					else{
						$check = false;
					}
				$this->flaLimit = $this->flaLimit + 1;
				}catch (Exception $e) {
					$check = false;
				}
			}
			if($check != false )
			$url_img = $pageURL.$_SERVER["SERVER_NAME"]."/tl_files/thumbnail-book/".$title."-".$id."-".$etx.".png";
			
			return $url_img;
		}
		return $url_img;
	}
}

?>