<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once("class/class.pn.php");

class ModuleRefTag extends Module
{
	protected $strTemplate 		= 'mod_ref_tag';
	protected $config;
	
    public function generate()
	{
	    $this->config = new PnConfig();
	    
	    if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### MODULE REF TAG ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
		    
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		
		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
	    $pattern = '/^[:][a-zA-Z0-9\-]+$/';
	    //Belboon -> wrong
		if($_COOKIE['belboonpda11937'] || $_COOKIE['belboon11937']){
			$reftag ='';
	    	if($_COOKIE['belboonpda11937'])
	        	$reftag = 'belboonpda11937';
	        if($_COOKIE['belboon11937'])
	        	$reftag ='belboon11937';
			setcookie('referrer', $reftag, time() + (86400 * 3), "/",$domain_cookie);
			$this->Template->refTag = $reftag;
		}
		
		if($_COOKIE['PV'] || $_COOKIE['PC'])
		{
		    if($_COOKIE['PV'] == 'belboonpda11937')
		    {
		        $reftag = 'belboonpda11937';
		    }
		    
		    if($_COOKIE['PC'] == 'belboon11937')
		    {
		        $reftag = 'belboon11937';
		    }
		    
		    setcookie('referrer', $reftag, time() + (86400 * 3), "/",$domain_cookie);
		    $this->Template->refTag = $reftag;
		}
		
	    foreach($_GET as $k => $v)
	    {
	        if(preg_match($pattern, $k))
	        {
	            $objDb = $this->Database->prepare("
	                    SELECT ref_id FROM " . $this->config->database . ".refs WHERE ref_tag LIKE ?
	                ")->execute(str_replace(":", "", $k));
	                
	            if($objDb->numRows > 0)
	            {
	                $reftag = str_replace(":", "", $k);
	                $url    = $this->parseQueryString($_SERVER["REQUEST_URI"], $k);
	                
	                setcookie('referrer', $reftag, time() + (86400 * 3), "/",$domain_cookie);
	                if($reftag == "zanox")
	                {
	                    $partnerId = $_GET["zanpid"];
	                    setcookie('ref_zanpid', $partnerId, time() + (86400 * 3), "/",$domain_cookie);
	                    $url = $this->parseQueryString($url, "zanpid");
	                }
	                
	                if($reftag == "affilinet")
	                {
	                    setcookie('ref_ref', $_GET['ref'], time() + (86400 * 3), "/",$domain_cookie);
	                    setcookie('ref_affmt', $_GET['affmt'], time() + (86400 * 3), "/",$domain_cookie);
	                    setcookie('ref_affmn', $_GET['affmn'], time() + (86400 * 3), "/",$domain_cookie);
	                    
	                    $url    = $this->parseQueryString($url, 'ref');
	                    $url    = $this->parseQueryString($url, 'affmt');
	                    $url    = $this->parseQueryString($url, 'affmn');
	                }
	                
	                if(in_array($reftag, array("belboon-standard", "belboon-kinder", "adbutler", "kn-adbutler")))
	                {
	                    setcookie('ref_belboon', $_GET['belboon'], time() + (86400 * 3), "/",$domain_cookie);
	                    $url    = $this->parseQueryString($url, 'belboon');
	                }
	                
	                if(in_array($reftag, array("adcell-standard")))
                    {
                        // Set cookie expire - up to 75 days
                        setcookie('referrer', $reftag, time() + (86400 * 75), "/",$domain_cookie);
                    }
	                
                    break;
	            }
	        }
	    }
	    
	    $orderId = 0;
	    if($_GET["order"])
	    {
	        $orderId = $_GET["order"];
	    }
	    
	    if($_GET["merchantReference"])
	    {
	        $orderId = str_replace("Order", "", $_GET["merchantReference"]);
	    }
	    
	    if($orderId > 0 && $_COOKIE['referrer'])
	    {
	        $reftag = $_COOKIE['referrer'];
	        $order  = new PnOrder();
	        $order->initWithId($orderId);
	        
	        if($order->number > 0 && $order->status == 1)
	        {
                if($order->id && $order->id != 0)
                {
                    $this->Template->orderNumber = $order->number;
                    $this->Template->customerId  = $order->customer_id;
                    $this->Template->totalPrice  = number_format($order->getTotalPrice(), 2);
                    $this->Template->partnerId   = $partnerId;
                    $this->Template->refTag      = $reftag;
                    $this->Template->totalPriceWithoutShipping = number_format($order->getTotalPrice("de", false), 2);
                    
                    $this->Template->totalNetto    = number_format($order->getTotalNetto(), 2);
                    $this->Template->shippingNetto = number_format($order->getShippingNetto(), 2);
                    $this->Template->totalNettoWithoutShipping = number_format($this->Template->totalNetto - $this->Template->shippingNetto, 2);
                    
                    $this->Template->bookItems  = $order->getBookItems();
                    $this->Template->wineItems  = $order->getWineItems();
                    
                    $this->Template->order_step = "done";
                    
                    $customer = new PnCustomer();
                    $customer->initWithId($order->customer_id);
                    $this->Template->customerName = $customer->firstName . " " . $customer->lastName;
                }
	        }
	        
	        if($order->step == "done")
	        {
	            setcookie ("referrer", "", time() - 3600, "/", $domain_cookie);
	            setcookie ("ref_zanpid", "", time() - 3600, "/", $domain_cookie);
	        }
	    }
	    
		if($orderId > 0 && !($_COOKIE['referrer']))
	    {
	        $reftag = '';
	        $order  = new PnOrder();
	        $order->initWithId($orderId);
	        
	        if($order->number > 0 && $order->status == 1)
	        {
                if($order->id && $order->id != 0)
                {
                    $this->Template->orderNumber = $order->number;
                    $this->Template->customerId  = $order->customer_id;
                    $this->Template->totalPrice  = number_format($order->getTotalPrice(), 2);
                    $this->Template->partnerId   = $partnerId;
                    $this->Template->refTag      = $reftag;
                    $this->Template->totalPriceWithoutShipping = number_format($order->getTotalPrice("de", false), 2);
                    
                    $this->Template->totalNetto    = number_format($order->getTotalNetto(), 2);
                    $this->Template->shippingNetto = number_format($order->getShippingNetto(), 2);
                    $this->Template->totalNettoWithoutShipping = number_format($this->Template->totalNetto - $this->Template->shippingNetto, 2);
                    
                    $this->Template->bookItems  = $order->getBookItems();
                    $this->Template->wineItems  = $order->getWineItems();
                    
                    $this->Template->order_step = "done";
                    
                    $customer = new PnCustomer();
                    $customer->initWithId($order->customer_id);
                    $this->Template->customerName = $customer->firstName . " " . $customer->lastName;
                }
	        }
	    }
	    
	    /* reftag VE Interaktive */
        if($reftag == "ve-interaktive")
        {
            $this->Template->refTag = $reftag;
        }
        
        if($_COOKIE['referrer'] == "ve-interaktive")
        {
            $this->Template->refTag = $_COOKIE['referrer'];
        }
        
        /* affiliate window */
        if($reftag == "affiliatewindow")
        {
            $this->Template->refTag = $reftag;
        }
        
        if($_COOKIE['referrer'] == "affiliatewindow")
        {
            $this->Template->refTag = $_COOKIE['referrer'];
        }
	    
	    if($orderId > 0)
	    {
	        $order  = new PnOrder();
	        $order->initWithId($orderId);
	        if($order->number > 0 && $order->step != "done" && $order->status == 1)
	        {
                if($order->id && $order->id != 0)
                {
                    $this->Template->orderNumber = $order->number;
                    $this->Template->customerId  = $order->customer_id;
                    $this->Template->totalPrice  = number_format($order->getTotalPrice(), 2);
                    $this->Template->totalPriceWithoutShipping = number_format($order->getTotalPrice("de", false), 2);
                    
                    $this->Template->totalNetto    = number_format($order->getTotalNetto(), 2);
                    $this->Template->shippingNetto = number_format($order->getShippingNetto(), 2);
                    $this->Template->totalNettoWithoutShipping = number_format($this->Template->totalNetto - $this->Template->shippingNetto, 2);
                    
                    $this->Template->bookItems     = $order->getBookItems();
                    $this->Template->wineItems     = $order->getWineItems();
                    
                    $customer = new PnCustomer();
                    $customer->initWithId($order->customer_id);
                    $this->Template->customerName = $customer->firstName . " " . $customer->lastName;
                }
	        }
	    }
	    
	    //Visitor from googleadwords
	    if($_COOKIE['__utmz'])
	    {
	        if(preg_match('/^.*utmgclid=.*/', $_COOKIE['__utmz']))
	        {
	            $visit_page = $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
	            
	            $parts = explode("|", $_COOKIE['__utmz']);
	            foreach($parts as $part)
                {
                    if(preg_match('/^.*utmgclid=.*/', $part))
                    {
                        $matchs = array();
                        preg_match_all('/^.*utmgclid=(.*)/', $part, $matchs);
                        
                        if(count($matchs) > 1)
                        {
                            $google_click_id = $matchs[1][0];
                        }
                    }
                    
                    if(preg_match('/^utmccn=.*$/', $part))
                    {
                        $matchs = array();
                        preg_match_all('/^utmccn=(.*$)/', $part, $matchs);
                        if(count($matchs) > 1)
                        {
                            $google_utmccn = $matchs[1][0];
                        }
                    }
                    
                    if(preg_match('/^utmcmd=.*$/', $part))
                    {
                        $matchs = array();
                        preg_match_all('/^utmcmd=(.*$)/', $part, $matchs);
                        if(count($matchs) > 1)
                        {
                            $google_utmcmd = $matchs[1][0];
                        }
                    }
                    
                    if(preg_match('/^utmctr=.*$/', $part))
                    {
                        $matchs = array();
                        preg_match_all('/^utmctr=(.*$)/', $part, $matchs);
                        if(count($matchs) > 1)
                        {
                            $google_utmctr = $matchs[1][0];
                        }
                    }
                }
                
                if($google_click_id != "")
                {
                    if(!$_COOKIE['referrer'] && !preg_match('/google/', $_COOKIE['referrer']))
                    {
                        setcookie('referrer', "google-1112-book", time() + (86400 * 3), "/",$domain_cookie);
                    }
                    
                    /*
                    chinh sua bug khong chay duoc tren ipad
                    $this->Database->prepare("
                            INSERT INTO tl_google_adwords_tracking(tstamp, clid, utmccn, utmcmd, utmctr, visit_page, ip_address)
                            VALUES (?, ?, ?, ?, ?, ?, ?)
                        ")->execute(time(), $google_click_id, $google_utmccn, $google_utmcmd, $google_utmctr, $visit_page, $_SERVER['REMOTE_ADDR']);
                    */
                }
	        }
	    }
	    
	}
	
	function parseQueryString($url,$remove)
	{
        $infos=parse_url($url);
        $str=$infos["query"];
        $op = array();
        $pairs = explode("&", $str);
        foreach ($pairs as $pair) {
           list($k, $v) = array_map("urldecode", explode("=", $pair));
            $op[$k] = $v;
        }
        if(isset($op[$remove])){
            unset($op[$remove]);
        }
    
        return str_replace($str,http_build_query($op),$url);
    
    }
}

?>
