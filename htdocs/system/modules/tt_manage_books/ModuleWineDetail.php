<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once("class/class.pn.php");

class ModuleWineDetail extends Module
{
	protected $strTemplate = 'mod_wine_detail';
	public $pn_link_logo = '';
	public $pn_view_logo ='';
	public $limitCopyImg = 12;
	public $flaLimit = 0;
	
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### WINES DETAIL ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
	
		if(TL_MODE == 'FE')
		{
			$this->strTemplate = $this->pn_template_wine ? $this->pn_template_wine : 'mod_wine_detail';
			
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/tabs.css|screen';
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/jquery.lightbox-0.5.css|screen';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery-ui-1.8.16.custom.min.js';
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.lightbox-0.5.pack.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/detail.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
		//if($this->pn_template_wine)
			//		$this->strTemplate = $this->pn_template_wine;
			
		//link logo for checkout
		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
		$link_logo = trim($this->pn_link_logo);
		$viewlogo  = $this->pn_view_logo;
		setcookie('urllogo', $link_logo."|".$viewlogo, time() + (86400 * 3), "/",$domain_cookie);
		
		$config = new PnConfig();
		
		$article_id = $this->Input->get("item") ? $this->Input->get("item") : 0;
		
		$article = $this->Database->prepare(
				"select * from " . $config->database . ".finanz_artikel as f "
				. "left outer join " . $config->database . ".artikel_klassen as a on f.`class_id` = a.`class_id` "
				. "left outer join " . $config->database . ".assets on f.`image_asset_id` = assets.`asset_id` "
				. "left outer join " . $config->database . ".prices p on f.`artikel_id` = p.`artikel_id` "
				. "where f.`artikel_id` = ?"
		)->execute($article_id);
		
		$article1 = new PnArticle();
        $article1->initWithId($article_id);
		$etiquettes = $article1->getEtiquettes();
		$list_etiquettes = array();
		foreach($etiquettes as $etiquette)
		{
			$list_etiquettes[$etiquette->photo_allowed][$etiquette->getTagName()][] = $etiquette;
		}

		$this->Template->etiquettes = $list_etiquettes;
			
		while($article->next())
		{
			if($article->artikel_available == 0)
			{
				header("Location: /");
				exit;
			}

			$thumb_url 			= "/thumbs/";
			$thumb_path			= implode("/", str_split(substr(str_replace(".", "", $article->asset_filename), 0, 3)));
			$thumbnail_tag		= "w150";
			$thumbnail_name 	= $thumbnail_tag . "-" .$article->asset_filename;
			$thumb_url	   	   .= $thumb_path . "/" .$thumbnail_name;
			
			//$pk = $article->artikel_id;
			$preview_url	= $GLOBALS['TL_CONFIG']['image_root'] . $thumb_url;
			$preview_url = $this->getImageUrl($article->artikel_id,"detail",$preview_url, standardize($article->artikel_name));
			
			
			$asset = $this->Database->prepare("select * from " . $config->database . ".assets a inner join " . $config->database . ".extra_assets e on a.`asset_id` = e.`asset_id` where e.`artikel_id` = ? and a.`asset_type_id` <> ?")
									->execute($article->artikel_id, $article->asset_type_id);

			while($asset->next($asset))
			{
				$bg_url 			= "/thumbs/";
				$bg_path			= implode("/", str_split(substr(str_replace(".", "", $asset->asset_filename), 0, 3)));
				$bg_tag				= "w768";
				$bg_name 			= $bg_tag . "-" . md5($GLOBALS['TL_CONFIG']['site_secret'] . $bg_tag . $asset->asset_filename) . "-" .$asset->asset_filename;
				$bg_url	   	   	   .= $bg_path . "/" .$bg_name;
				
				//$asset_id = $asset->asset_id;
				$preview_bg	= $GLOBALS['TL_CONFIG']['image_root'] . $bg_url;
				$preview_bg = $this->getImageUrl($asset->asset_id,"detail",$preview_bg, standardize($asset->asset_filename));
			
			
				$this->Template->bg = $preview_bg;
			}
			
			$objWinzer = $this->Database->prepare("select b.`tag_value`, b.`tag_value_en`, b.`tag_desc` from " . $config->database . ".artikel_buchtags a inner join " . $config->database . ".buch_tags2 b on a.tag_id = b.tag_id where a.artikel_id = ? and b.`category_id` = 32")->execute($article->artikel_id);
			$winzer 		= "Winzer";
			while($objWinzer->next())
			{
				$winzer = $objWinzer->tag_value;
				$winzer_desc = $objWinzer->tag_desc;
				$this->Template->winzer = $winzer;
				$this->Template->winzer_desc = $winzer_desc;
			}
			
			$objRebsorte = $this->Database->prepare("select b.`tag_value`, b.`tag_value_en`, b.`tag_desc` from " . $config->database . ".artikel_buchtags a inner join " . $config->database . ".buch_tags2 b on a.tag_id = b.tag_id where a.artikel_id = ? and b.`category_id` = 33")->execute($article->artikel_id);
			$rebsorte = "";
			while($objRebsorte->next())
			{
				$rebsorte 		.= sprintf("<h2>%s</h2>", $objRebsorte->tag_value);
				$rebsorte		.= $objRebsorte->tag_desc;
			}
			
			
            
			$this->Template->url_domain		= "http://".$domain_cookie;
			$this->Template->rebsorte 		= $rebsorte;
			$this->Template->wine = $article;
			$this->Template->preview_url = $preview_url;
		
		}
	}
	
	//Get thumbnail
	public function getImageUrl($id, $etx, $url_img, $title) {
		if($this->flaLimit != $this->limitCopyImg){
			
			if(!in_array($etx, array('detail','view','list'))){
				$etx = "detail";
			}
			$check = true;
			$pageURL = 'http';
			
			if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
			
			$pageURL .= "://";
			$file_image = TL_ROOT."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png";
			
			if(!is_file($file_image)){
				try{
				$file_headers = @get_headers($url_img);
				if($file_headers[0] != 'HTTP/1.1 404 Not Found' and $file_headers[0] != 'HTTP/1.1 500 INTERNAL SERVER ERROR')
					copy($url_img, TL_ROOT."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png");
					else{
						$check = false;
					}
				$this->flaLimit = $this->flaLimit + 1;
				}catch (Exception $e) {
					$check = false;
				}
			}
			if($check != false )
			$url_img = $pageURL.$_SERVER["SERVER_NAME"]."/tl_files/thumbnail-wine/".$title."-".$id."-".$etx.".png";
			
			return $url_img;
		}
		return $url_img;
	}
}

?>