<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

$GLOBALS['TL_DCA']['tl_module']['palettes']['book_listing'] 		= '{title_legend},name,type,perPage,pn_type,pn_image_height,pn_image_shadow,pn_template,{redirect_legend},jumpTo;pn_ebook';
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_filter_panel'] 	= '{title_legend},name,headline,type';
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_detail'] 			= '{title_legend},name,type,pn_template';
$GLOBALS['TL_DCA']['tl_module']['palettes']['wine_detail'] 			= '{title_legend},name,type,pn_template_wine';
$GLOBALS['TL_DCA']['tl_module']['palettes']['wine_listing']			= '{title_legend},name,type,perPage,pn_template_wine;{redirect_legend},jumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_listing_backde'] 	= '{title_legend},name,type,perPage,pn_type,pn_image_height,pn_image_shadow,pn_template,{redirect_legend},jumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_genre']           = '{title_legend},name,type,headline,pn_image_height,pn_image_shadow,pn_genreid,pn_booktitle,{redirect_legend},jumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_order']           = '{title_legend},name,type;
                                                                        {teaser_step1_legend:hide},step1_teaser;{teaser_step2_legend:hide},step2_teaser;
                                                                        {teaser_step3_legend:hide},step3_teaser;{teaser_step4_legend:hide},step4_teaser;
                                                                        {teaser_step5_legend:hide},step5_teaser;{teaser_step6_legend:hide},step6_teaser;
                                                                        {Redirect page error},jumpTo';
                                                                        
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_random'] 		    = '{title_legend},name,type;{choose_image:show},pn_sitepath,pn_filter_module,pn_page_cur';
$GLOBALS['TL_DCA']['tl_module']['palettes']['order_geschenkbox']	= '{title_legend},name,type;{Redirect page error},jumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['wine_order']			= '{title_legend},name,type;{Redirect page error},jumpTo';
/*
 * new fields
 */
 
 $GLOBALS['TL_DCA']['tl_module']['palettes']['novel_simply_book_template'] 		= '{title_legend},name,type,pn_template_tools';
/*
 * new fields
 */

 //create new field for book_random
 $GLOBALS['TL_DCA']['tl_module']['fields']['pn_filter_module'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['pn_filter'],
	'inputType'				  => 'multitextWizard',
	'eval'      => array
	(
		'style'=>'width:75%;',
		'columns' => array
		(
				array
				(
						'name' => 'filter_key', // optional
						'label' => "Key",
						'mandatory' => false, // optional
				),
				array
				(
						'name' => 'filter_value',  // optional
						'label' => "Value",
						'mandatory' => false, // optional
				)
		)
	)
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_sitepath'] = array
(
	'label'                   => array("Site path", ""),
	'inputType'				  => 'select',
	'options_callback'		  => array('tl_module_book_listing', 'getSitePaths')
);
$GLOBALS['TL_DCA']['tl_module']['fields']['pn_page_cur'] = array(
    'label'					=> array("Page main detail shows", ""),
    'inputType'             => 'text',
    'eval'                  => array('mandatory'=>false, 'tl_class'=>'w50')
);
$GLOBALS['TL_DCA']['tl_module']['fields']['pn_ebook'] = array(
    'label'					=> array("Allow E-book", ""),
    'inputType'             => 'checkbox',
    'eval'                  => array('mandatory'=>false, 'tl_class'=>'w50')
);
$GLOBALS['TL_DCA']['tl_module']['fields']['pn_select_image'] = array
(
	'label'                 => &$GLOBALS['TL_LANG']['tl_module']['pn_select_image'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'options_callback'        => array('tl_module_book_listing', 'getNameBook'),
	'eval'                    => array('mandatory'=>false, 'multiple'=>true)
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_booklang'] = array(
	'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_booklang'],
	'inputType'             => 'select',
	'options_callback'      => array('tl_module_book_listing', 'getPnLanguage'),
	'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_type'] = array(
	'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_type'],
	'inputType'             => 'select',
	'options_callback'      => array('tl_module_book_listing', 'getPnType'),
	'eval'                  => array('mandatory'=>true,'submitOnChange'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_image_height'] = array(
    'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_image_height'],
    'inputType'             => 'text',
    'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_image_shadow'] = array(
    'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_image_shadow'],
    'inputType'             => 'text',
    'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_template'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['pn_template'],
    'default'                 => 'mod_book_listing',
    'exclude'                 => true,
    'inputType'               => 'select',
    'options_callback'        => array('tl_module_book_listing', 'getBooksListingTemplates'),
    'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50')
);
$GLOBALS['TL_DCA']['tl_module']['fields']['pn_template_wine'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['pn_template'],
    'default'                 => 'mod_wine_detail',
    'exclude'                 => true,
    'inputType'               => 'select',
    'options_callback'        => array('tl_module_book_listing', 'getWinesListingTemplates'),
    'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_genreid'] = array(
    'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_genreid'],
    'inputType'             => 'text',
    'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_booktitle'] = array(
    'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_booktitle'],
    'inputType'             => 'text',
    'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['step1_teaser'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['step1_teaser'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['step2_teaser'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['step1_teaser'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['step3_teaser'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['step1_teaser'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['step4_teaser'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['step1_teaser'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['step5_teaser'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['step1_teaser'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['step6_teaser'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['step1_teaser'],
    'exclude'                 => true,
    'inputType'               => 'textarea',
    'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr')
);


require_once(dirname(dirname(__FILE__)) . "/class/class.pn.php");
class tl_module_book_listing extends Backend
{
	public $name_genre = '';
	public function __construct()
	{
		
		parent::__construct();
	}
	
	
	public function getPnType()
	{
		/*
			Romane = constructor(type_id=1)
			Bilderbuecher = constructor(type_id=2)
			FrecheMaedchen = constructor(type_id=3)
			Cora = constructor(type_id=4)
			ThienemannBuchpiraten = constructor(type_id=5)
			PrivateNOTE = constructor(type_id=6)
			ThienemannBuchpiratenJugendbuecher = constructor(type_id=7)
			PersonalBiz = constructor(type_id=8)
			CoraAndPersonalNovel = constructor(type_id=9)
			NovelOnDemand = constructor(type_id=10)
			Geschenkbuch = constructor(type_id=11)
			Geschenkbuch22x22 = constructor(type_id=12)
			Kinderbuch19x23 = constructor(type_id=13)
			MusterdruckRomane = constructor(type_id=14)
			Klassiker = constructor(type_id=15)
			StatischesCover = constructor(type_id=16)
			Montana = constructor(type_id=17)
			ClassicNovels = constructor(type_id=18)
			SkalierteRomane = constructor(type_id=20)
			PersonalPockets = constructor(type_id=21)
			StatischePDFs = constructor(type_id=22)
			CameoClassicNovels = constructor(type_id=23)
			
			# Type-ID-HACKs zentralisiert, um sie irgendwann
			# in eine bessere Abstraktion umzuwandeln.
			ROMANOID_TYPE_IDS = frozenset([1, 3, 4, 5, 7, 9, 14, 15, 16, 17, 18, 19,
			                               20, 21, 22, 23])
			KINDERBUCH_TYPE_IDS = frozenset([2, 13])
			JUGENDBUCH_TYPE_IDS = frozenset([3, 5, 7])
			GESCHENKBUCH_TYPE_IDS = frozenset([11, 12])
			KLASSIKER_TYPE_IDS = frozenset([15, 18, 23])
			
			NO_PHOTO_UPGRADE_TYPE_IDS = frozenset([3, 4, 5, 7, 9, 15])
			
			# Types, deren B�cher das ProofBook unterst�tzen
			# (Weltbild und Montana haben keine Taschenb�cher)
			PROOFABLE_TYPE_IDS = ROMANOID_TYPE_IDS - frozenset([16, 17])
			
			EBOOKABLE_TYPE_IDS = ROMANOID_TYPE_IDS - set([18, 23])
			
			# Types, welche die Widmung in Handschrift und Farbe unterst�tzen
			FANCY_WIDMUNG_TYPE_IDS = ROMANOID_TYPE_IDS | GESCHENKBUCH_TYPE_IDS
		 */
		
		return array(
			"1"	=> $GLOBALS['TL_LANG']['tl_module']['pn_type']['novels'],
			"2"	=> $GLOBALS['TL_LANG']['tl_module']['pn_type']['children_books'],
			"3" => $GLOBALS['TL_LANG']['tl_module']['pn_type']['gift_books'],
			"4" => $GLOBALS['TL_LANG']['tl_module']['pn_type']['classic_literature'],
			"5" => $GLOBALS['TL_LANG']['tl_module']['pn_type']['e_books']
		);
	}
	
	public function getPnLanguage()
	{
		return array(
			'de' 	=> "DE",
			'en' 	=> "EN",
			'bo'	=> "Both"
		);
	}
	
	public function getNameBook(){
		$arrBooks = array();
		$config = new PnConfig();
		
		$book_type = "(1, 3, 4, 5, 7, 9, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)";
		if($this->pn_type == 1) { //Novels
			$book_type = "(1, 3, 4, 5, 7, 9, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)";
		} else if ($this->pn_type == 2) { //Children books
			$book_type = "(2, 13)";
		} else if ($this->pn_type == 3) { //gift_books
			$book_type = "(11, 12)";
		} else if ($this->pn_type == 4) { //classic_literature
			$book_type = "(15, 18, 23)";
		} else if ($this->pn_type == 5) { //E-Books
			$book_type = "(18, 23)";
		}	
		
		$query =sprintf("
				SELECT *, b.buch_id AS id_book,b.buch_titel AS title_book
				FROM " . $config->database . ".finanz_artikel f
				LEFT OUTER JOIN " . $config->database . ".buecher b
				ON f.buch_id = b.buch_id
				LEFT OUTER  JOIN " . $config->database . ".site_book sb
				ON b.buch_id = sb.`buch_id`
				LEFT OUTER JOIN " . $config->database . ".sites s
				ON sb.site_id = s.site_id

				WHERE 
				(
					b.buch_id IS NULL OR
					b.buch_pub = 1 
					AND b.buch_hidden = 0
					AND b.type_id IN %s  
					AND b.buch_id NOT IN (411)
				)
				AND f.artikel_active = 1 
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				AND 
				(
					f.artikel_validuntil IS NULL 
					OR f.artikel_validuntil > now()
				)
				
				AND f.class_id = 2 
				
				AND f.version_id NOT IN (4, 5, 6, 1303, 1304, 1305)
				
				GROUP BY b.buch_id
				ORDER BY b.buch_lang != 'de', b.type_id, b.buch_created DESC, b.buch_id DESC
			",$book_type);

		$objBooks = $this->Database->execute($query);

		while ($objBooks->next())
		{
				$arrBooks[$objBooks->id_book] = $objBooks->title_book;
		}
		
		return $arrBooks;
	}
	
	public function getBooksListingTemplates(DataContainer $dc)
	{
		$intPid = $dc->activeRecord->pid;
	
		if ($this->Input->get('act') == 'overrideAll')
		{
			$intPid = $this->Input->get('id');
		}
	
		return $this->getTemplateGroup('mod_book_', $intPid);
	}
	public function getWinesListingTemplates(DataContainer $dc)
	{
		$intPid = $dc->activeRecord->pid;
	
		if ($this->Input->get('act') == 'overrideAll')
		{
			$intPid = $this->Input->get('id');
		}
	
		return $this->getTemplateGroup('mod_wine_', $intPid);
	}
	public function getSitePaths()
	{
		$arr = array();
		$config = new PnConfig();
		$obj = $this->Database->prepare("
				SELECT * FROM " . $config->database . ".sites s
				LEFT JOIN " . $config->database . ".locales l
				ON s.locale_id = l.locale_id
				WHERE s.site_listed = 1
				ORDER BY s.locale_id
			")->execute();
			
		while($obj->next())
		{
			$arr[$obj->site_id] = $obj->locale_code . " - " . $obj->site_name;
		}
		
		return $arr;
	}
}

?>