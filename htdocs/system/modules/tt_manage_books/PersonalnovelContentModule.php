<?php

class PersonalnovelContentModule extends ContentModule
{
	/**
	 * Parse the template
	 * @return string
	 */
	public function generate()
	{
		$objModule = $this->Database->prepare("SELECT * FROM tl_module WHERE id=?")
		->limit(1)
		->execute($this->pn_module);

		if ($objModule->numRows < 1)
		{
			return '';
		}
	
		$strClass = $this->findFrontendModule($objModule->type);
	
		if (!$this->classFileExists($strClass))
		{
			return '';
		}
	
		$objModule->typePrefix = 'ce_';
		$objModule = new $strClass($objModule);

		// Overwrite spacing and CSS ID
		$objModule->space 		= $this->space;
		$objModule->cssID 		= $this->cssID;
		
		$objModule->sitepath 	= $this->pn_sitepath;
		
		$objModule->headline 	= $this->pn_headline;
		
		$objModule->pn_link_logo 	= $this->pn_link_logo;
		$objModule->pn_view_logo 	= $this->pn_view_logo;
		$arrTmp					= deserialize($this->pn_filter);
		if(count($arrTmp)>0){
			foreach($arrTmp as $k=>$v)
			{
				if($v[0] != "")
					$objModule->arrFilters[$v[0]][] = $v[1];
			}
		}
		$arrTabtm					= deserialize($this->pn_tab);
		if(count($arrTabtm)>0){
			foreach($arrTabtm as $k=>$v)
			{
				if($v[0] != "")
					$objModule->arrTab[$v[0]][] = $v[1];
			}
		}
		
		$arrGenre					= deserialize($this->pn_genre);
		if(count($arrGenre)>0){
			foreach($arrGenre as $k=>$v)
			{
				if($v[0] != "")
					$objModule->arrGenre[$v[0]][] = $v[1];
			}
		}
		
		$objTemplate = new FrontendTemplate($objModule->strTemplate);
		$objTemplate->parse();
		return $objModule->generate();
	}
	
	/**
	 * Generate content element
	 */
	protected function compile()
	{
		return;
	}
}

?>