<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * PHP version 5
 * @copyright  TT-TECH 2012
 * @author     TT-TECH <http://tt-tech.de>
 * @package    Wine Banner
 */

// Back end modules
array_insert($GLOBALS['BE_MOD']['content'], 3, array
(
	'winebanner' => array
	(
		'tables' => array('tl_winebanner_category', 'tl_winebanner'),
		'icon'   => 'system/modules/tt_winebanner/html/icon.png'
	)
));


// Front end modules
array_insert($GLOBALS['FE_MOD']['miscellaneous'], 0, array
(
	'tt_winebanner' => 'ModuleWineBanner'
));

?>