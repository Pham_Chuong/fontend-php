-- **********************************************************
-- *                                                        *
-- * IMPORTANT NOTE                                         *
-- *                                                        *
-- * Do not import this file manually but use the TYPOlight *
-- * install tool to create and maintain database tables!   *
-- *                                                        *
-- **********************************************************


-- --------------------------------------------------------

-- 
-- Table `tl_slideshow2`
-- 

CREATE TABLE `tl_slideshow2` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `sorting` int(10) unsigned NOT NULL default '0',
  `tstamp` int(10) unsigned NOT NULL default '0',
  
  `title` varchar(255) NOT NULL default '',
  `slideshow2_size` varchar(255) NOT NULL default '',
  
  `rotation` char(1) NOT NULL default '',
  `rotation_interval` int(10) NOT NULL default '3000',
  
  `effect_type` varchar(255) NOT NULL default '',
  `effect_duration` int(10) NOT NULL default '1000',
  
  `effects_extended` char(1) NOT NULL default '',
  `effect_transition` varchar(64) NOT NULL default '',
  `effect_ease` varchar(64) NOT NULL default '',
  
  `controls` char(1) NOT NULL default '',
  `controls_type` tinyint(2) NOT NULL default '1',

  `play_random` char(1) NOT NULL default '',
  `play_paused` char(1) NOT NULL default '',
  `play_loop` char(1) NOT NULL default '',
  `play_image` char(1) NOT NULL default '',
  
  `thumbnails` char(1) NOT NULL default '',
  `thumbnails_size` varchar(255) NOT NULL default '',
  `thumbnails_padding_width` int(10) NOT NULL default '10',
  `thumbnails_padding_height` int(10) NOT NULL default '10',
  
  `captions` char(1) NOT NULL default '',
  `captions_meta` char(1) NOT NULL default '',
  
  `template_css` varchar(32) NOT NULL default '',
  `template_js` varchar(32) NOT NULL default '',
  
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

-- 
-- Table `tl_slideshow2_elements`
-- 

CREATE TABLE `tl_slideshow2_elements` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `pid` int(10) unsigned NOT NULL default '0',
  `sorting` int(10) unsigned NOT NULL default '0',
  `tstamp` int(10) unsigned NOT NULL default '0',
  
  `alt` varchar(255) NOT NULL default '',
  `description` mediumtext NULL,
  `src` blob NULL,
  `img` char(1) NOT NULL default '',
  `img_size` varchar(255) NOT NULL default '',

  `url` char(1) NOT NULL default '',
  `url_link` varchar(255) NOT NULL default '',
  `url_title` varchar(255) NOT NULL default '',
  `url_window` char(1) NOT NULL default '',
  `url_fullsize` char(1) NOT NULL default '',
  
  PRIMARY KEY  (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table `tl_module`
-- 

CREATE TABLE `tl_module` (
  `slideshow2` int(10) unsigned NOT NULL default '0',
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- 
-- Table `tl_content`
-- 

CREATE TABLE `tl_content` (
  `slideshow2` int(10) unsigned NOT NULL default '0',
) ENGINE=MyISAM DEFAULT CHARSET=utf8;