<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010 
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de> / Aeron Glemann <http://www.electricprism.com/aeron/>
 * @package    Slideshow2 
 * @license    LGPL 
 * @filesource
 */


/**
 * Table tl_slideshow2 
 */
$GLOBALS['TL_DCA']['tl_slideshow2'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'ctable'                      => array('tl_slideshow2_elements'),
		'switchToEdit'                => true,
		'enableVersioning'            => true
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
			'fields'                  => array('title'),
			'flag'                    => 1,
			'panelLayout'             => 'filter;search,limit'
		),
		'label' => array
		(
			'fields'                  => array('title'),
			'format'                  => '%s',
			'label_callback'	      => array('tl_slideshow2', 'createLabel')
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['edit'],
				'href'                => 'table=tl_slideshow2_elements',
				'icon'                => 'edit.gif'
			),
			'editheader' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['editheader'],
				'href'                => 'act=edit',
				'icon'                => 'header.gif',
				'attributes'          => 'class="edit-header"'
			),			
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'	=>	array('controls', 'effects_extended', 'thumbnails'),
		
		'default'       => '{title_legend},title,slideshow2_size;
							{rotation_legend},rotation_interval,effect_duration;
							{effects_legend},effect_type,effects_extended;
							{play_legend:hide},play_random,play_paused,play_loop,play_image;
							{controls_legend:hide},controls;
							{thumbnails_legend:hide},thumbnails;
							{captions_legend:hide},captions,captions_meta;
							{template_legend:hide},template_css,template_js;
							'
	),

	// Subpalettes
	'subpalettes' => array (
		'controls'				=>	'controls_type',
		'effects_extended'		=>	'effect_transition,effect_ease',
		'thumbnails'			=>	'thumbnails_size,thumbnails_padding_width,thumbnails_padding_height'
	),

	// Fields
	'fields' => array
	(
	
	//-- Title, Slidesize
		
		'title' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['title'],
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50')
		),
		'slideshow2_size' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['slideshow2_size'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'multiple'=>true, 'size'=>2, 'rgxp'=>'digit', 'nospace'=>true)
		),
	
	//-- Rotation		
    
      	'rotation_interval' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['rotation_interval'],
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'nospace'=>true, 'tl_class'=>'w50')
		),
			
	//-- Effects
	
      	'effect_type' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['effect_type'],
			'inputType'               => 'radio',
			'default'                 => '1',
			'options'                 => array('Alpha','Push','Flash','Fold','KenBurns'),
			'eval'                    => array('mandatory'=>true)
		),
        'effect_duration' => array
        (
			'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['effect_duration'],
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true,'nospace'=>true)
        ),	
		
	//-- Effects
	
		'effects_extended' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['effects_extended'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'				=> array('submitOnChange'=>true, 'tl_class'=>'clr')
		),
			
	        'effect_transition' => array
	        (
				'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['effect_transition'],
				'inputType'               => 'select',
				'options'                 => array('Quad', 'Cubic', 'Quart', 'Quint', 'Sine', 'Expo', 'Circ', 'Bounce', 'Back', 'Elastic'),
				'reference'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['effect_transition'],
				'eval'                    => array('tl_class'=>'w50')
	        ),
			'effect_ease' => array
			(
				'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['effect_ease'],
				'default'                 => '1',
				'inputType'               => 'radio',
				'options'                 => array('in', 'out', 'in:out'),
				'reference'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['effect_ease']
			),
			
	//-- Controls
	
		'controls' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['controls'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'				=> array('submitOnChange'=>true)
		),
	      	'controls_type' => array
			(
				'label'                   => &$GLOBALS['TL_LANG']['tl_slideshow2']['controls_type'],
				'default'                 => 0,
				'inputType'               => 'radio',
				'options'                 => array(0, 1, 2),
				'reference'               => &$GLOBALS['TL_LANG']['tl_slideshow2']['ref_control_type']
			),
			
	//-- Play
	
		'play_random' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['play_random'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'              => array('tl_class'=>'w50')
		),
		
		'play_paused' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['play_paused'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'              => array('tl_class'=>'w50')
		),
		
		'play_loop' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['play_loop'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'              => array('tl_class'=>'w50')
		),
		'play_image' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['play_image'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'              => array('tl_class'=>'w50')
		),		
		
	//-- Thumbnails
	
		'thumbnails' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'				=> array('submitOnChange'=>true)
		),
		
			'thumbnails_size' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_size'],
				'exclude'           => true,
				'inputType'         => 'text',
				'eval'              => array('mandatory'=>true, 'multiple'=>true, 'size'=>2, 'rgxp'=>'digit', 'nospace'=>true)
			),
			
			'thumbnails_padding_width' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_padding_width'],
				'exclude'           => true,
				'inputType'         => 'text',
				'eval'              => array('mandatory'=>true, 'rgxp'=>'digit', 'nospace'=>true, 'style'=>'width:50px;', 'tl_class'=>'w50')
			),	
			'thumbnails_padding_height' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_padding_height'],
				'exclude'           => true,
				'inputType'         => 'text',
				'eval'              => array('mandatory'=>true, 'rgxp'=>'digit', 'nospace'=>true, 'style'=>'width:50px;')
			),				
			
	
	//-- captions
	
		'captions' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['captions'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'              => array('tl_class'=>'w50')
		),
		'captions_meta' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['captions_meta'],
			'exclude'			=> true,
			'inputType'			=> 'checkbox',
			'eval'              => array('tl_class'=>'w50')
		),
		

	//-- templates
		
		'template_css' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['template_css'],
			'default'           => 'slideshow2_css_standart',
			'exclude'           => true,
			'inputType'         => 'select',
			'options'           => $this->getTemplateGroup('slideshow2_css_'),
			'eval'              => array('tl_class'=>'w50')
		),
		
		'template_js' => array
		(
			'label'				=> &$GLOBALS['TL_LANG']['tl_slideshow2']['template_js'],
			'default'           => 'slideshow2_js_standart',
			'exclude'           => true,
			'inputType'         => 'select',
			'options'           => $this->getTemplateGroup('slideshow2_js_'),
			'eval'              => array('tl_class'=>'w50')
		),
		
	)
);


/**
 * Class tl_slideshow2
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009 
 * @author     Sabri Karadayi 
 * @package    Controller
 */
class tl_slideshow2 extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}

/*----- label_callback  -----------------------------------------------------------------*/

	/**
	 * List a particular record
	 * @param array
	 * @return string
	 */
	
	public function createLabel($arrRow, $strLabel)
	{
		$objElements = $this->Database->prepare("SELECT * FROM tl_slideshow2_elements WHERE pid=? ORDER by sorting ASC")
					   ->execute($arrRow['id']);
					   
					   
		if ($objElements->numRows > 0)
		{
			while ($objElements->next())
			{
				if (strncmp($objElements->src, '.', 1) === 0)
				{
					continue;
				}
	
				// Directory
				if (is_dir(TL_ROOT . '/' . $objElements->src))
				{
					$subfiles = scan(TL_ROOT . '/' . $objElements->src);
					foreach ($subfiles as $subfile)
					{
						if (strncmp($subfile, '.', 1) === 0 || is_dir(TL_ROOT . '/' . $objElements->src . '/' . $subfile))
						{
							continue;
						}
		
						$objFile = new File($objElements->src . '/' . $subfile);
						if ($objFile->isGdImage)
						{
							$arrElements[$x]['src'] = @$this->getImage($objElements->src . '/' . $subfile, 100, 50);
							$x++;
						}
					}
					continue;
				}
		
				// File
				if (is_file(TL_ROOT . '/' . $objElements->src))
				{
					$objFile = new File($objElements->src);
					if ($objFile->isGdImage)
					{
						$arrElements[$x]['src'] = @$this->getImage($objElements->src, 100, 50);
						$x++;
					}
				}
	
			}			   
			
			if (is_array($arrElements))
			{
				foreach ($arrElements as $element)
				{
					$OutputImages .= '<img src="' . $element['src'] . '" alt="' . $element['alt'] . '" />';
				}
			}

		}
		else
		{
			$OutputImages = $GLOBALS['TL_LANG']['tl_slideshow2']['misc_noimages'];
		}
					   
		return '<div class="labelbox">
		<div class="heading">' . $arrRow['title'] . ' (' . count($arrElements) . ' ' . $GLOBALS['TL_LANG']['tl_slideshow2']['misc_images'] . ')</div>
		<div class="limit_height' . (!$GLOBALS['TL_CONFIG']['doNotCollapse'] ? ' h64' : '') . ' block">
			' . $OutputImages . '
		</div>
		</div>';
	}
	
}

?>