<?php
/**
 * TL_ROOT/system/modules/slideshow2/languages/cs/tl_content.php 
 * 
 * Contao extension: slideshow2 2.0.0 beta1 
 * Czech translation file 
 * 
 * Copyright : &copy 2009-2011 Die Kommunikationsabteilung - Fauth und Gundlach GmbH / Aeron Glemann 
 * License   : LGPL 
 * Author    : Sabri Karadayi (dieka), http://www.kommunikationsabteilung.de 
 * Translator: Tomas Petrlik (frogzone), www.frogzone.cz 
 * 
 * This file was created automatically be the Contao extension repository translation module.
 * Do not edit this file manually. Contact the author or translator for this module to establish 
 * permanent text corrections which are update-safe. 
 */
 
$GLOBALS['TL_LANG']['tl_content']['slideshow2']['0'] = "Slideshow2";
$GLOBALS['TL_LANG']['tl_content']['slideshow2']['1'] = "Prosím, vyberte slideshow prezentaci.";
 
?>
