<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010 
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de> / Aeron Glemann <http://www.electricprism.com/aeron/>
 * @package    Slideshow2 
 * @license    LGPL 
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['title']				= array('Name der Slideshow', 'Ist nur im Backend sichtbar');
$GLOBALS['TL_LANG']['tl_slideshow2']['slideshow2_size']		= array('Breite und -h&ouml;he der Slideshow', 'Bilder werden sofern die Größe nicht mit den angebenben Werten über einstimmt vergrößert oder verkleinert.');

$GLOBALS['TL_LANG']['tl_slideshow2']['rotation_interval']  	= array('Einblendungsdauer', 'Angabe in Millisekunden.');

$GLOBALS['TL_LANG']['tl_slideshow2']['effect_type']     	= array('Effekt Art', 'Bitte wählen Sie eine Effekt Art aus');
$GLOBALS['TL_LANG']['tl_slideshow2']['effect_duration'] 	= array('Effektdauer', 'Angabe in Millisekunden.');

$GLOBALS['TL_LANG']['tl_slideshow2']['effects_extended']   	= array('Effekt Bewegung', 'Basierend auf der Mootools Fx.Transitions Library');
$GLOBALS['TL_LANG']['tl_slideshow2']['effect_transition']  	= array('Bewegung', 'siehe http://demos111.mootools.net/Fx.Transitions f&uuml;r Beispiele');
$GLOBALS['TL_LANG']['tl_slideshow2']['effect_ease']  		= array('Ease', 'Damit wird der Effekt weicher dargestellt');

$GLOBALS['TL_LANG']['tl_slideshow2']['controls']     		= array('Buttons aktivieren', 'Ermöglicht dem Nutzer die Slideshow zu steuern');
$GLOBALS['TL_LANG']['tl_slideshow2']['controls_type']     	= array('Transition speed', 'Hier kann die Geschwindigkeit beeinflusst werden wenn ein Nutzer eine Aktion durchführt.');

$GLOBALS['TL_LANG']['tl_slideshow2']['play_random']  		= array('Zufällige Reihenfolge', 'Die Reihenfolge der Bilder ist zufällig');
$GLOBALS['TL_LANG']['tl_slideshow2']['play_paused']  		= array('Autoplay deaktivieren', 'Die Slideshow läuft nicht automatisch');
$GLOBALS['TL_LANG']['tl_slideshow2']['play_loop']  			= array('Nur einmal durchlaufen', 'Die Slideshow läuft nur einmal durch und hält dann an');
$GLOBALS['TL_LANG']['tl_slideshow2']['play_image']  		= array('Startbild aktivieren', 'Beim Aufruf der Seite wird statt dem Ladesymbol ein Bild eingeblendet');

$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails']  		= array('Thumbnails aktivieren', 'Blendet Thumbnails unter dem Bild ein über welche der Nutzer navigieren kann');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_size']		= array('Bildbreite und -h&ouml;he der Thumbnails', 'Achtung! Es muss die Breite und Höhe festgelegt werden sonst können Darstellungsfehler auftreten.');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_padding_width'] 	= array('Thumbnail Abstand in der Breite', 'Standartmäßig fügt die Slideshow 5px an jeder Seite des Thumbnails dran');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_padding_height']	= array('Thumbnail Abstand in der Höhe', 'Wenn Sie diesen Abstand ändern können Sie den Wert hier anpassen.');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_overlay']	= array('Thumbnail Übergang', 'Blendet links und rechts im Thumbnailbereich weiche Übergänge ein. Im Ordner plugins/slideshow2/img/ können die Übergänge angepasst werden. Dateien sind thumbnails-a.png und thumbnails-b.png.');

$GLOBALS['TL_LANG']['tl_slideshow2']['captions']  			= array('Bilduntertitel aktivieren', 'Blendet Bilduntertitel unter dem Bild ein die aus dem ALT-Tag genommen werden');
$GLOBALS['TL_LANG']['tl_slideshow2']['captions_meta']  		= array('Meta-Angaben verwenden', 'Verwendet für die Beschreibung und den ALT-Tag Daten aus der meta.txt');


$GLOBALS['TL_LANG']['tl_slideshow2']['template_css']     	= array('CSS Template', 'Die Gestaltung der Slideshow kann über das Template angepasst werden.');
$GLOBALS['TL_LANG']['tl_slideshow2']['template_js'] 		= array('JS Template', 'Das Slideshow2 Skript bietet noch weitere Funktionen die nicht alle über das Backend abgedeckt sind. Eine Liste der Möglichkeiten finden Sie hier: http://code.google.com/p/slideshow/wiki/Slideshow');


/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['title_legend']     	= 'Titel und Größe';
$GLOBALS['TL_LANG']['tl_slideshow2']['rotation_legend']    	= 'Rotation';
$GLOBALS['TL_LANG']['tl_slideshow2']['effects_legend']     	= 'Effekte';
$GLOBALS['TL_LANG']['tl_slideshow2']['play_legend']    		= 'Abspielverhalten';
$GLOBALS['TL_LANG']['tl_slideshow2']['controls_legend']    	= 'Kontrolle';
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_legend']   = 'Thumbnails';
$GLOBALS['TL_LANG']['tl_slideshow2']['captions_legend']    	= 'Bilduntertitel';
$GLOBALS['TL_LANG']['tl_slideshow2']['template_legend']     = 'Templates';

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_control_type'][0]	= 'slow - use transitions';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_control_type'][1]	= 'normal - skip transitions if paused';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_control_type'][2]	= 'fast - skip all transitions and update the slide change instantly';

$GLOBALS['TL_LANG']['tl_slideshow2']['sequence'][1]			= 'hintereinander';
$GLOBALS['TL_LANG']['tl_slideshow2']['sequence'][2]			= 'zuf&auml;llig';

$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][1]	= 'Alpha Fade';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][2]	= 'Flash';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][3]	= 'Fold';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][4]	= 'Push';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][5]	= 'Ken Burns';

/**
 * misc
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['misc_images'] 		= 'Bilder';
$GLOBALS['TL_LANG']['tl_slideshow2']['misc_noimages']		= 'Bitte wählen Sie Bilder für die Slideshow aus. <br />(Auf editieren klicken und dann ein neues Element erstellen.)';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['new']    				= array('Neue Bildrotation', 'Eine neue Bildrotation anlegen');
$GLOBALS['TL_LANG']['tl_slideshow2']['edit']   				= array('Bildrotation editieren', 'Bildrotation ID %s editieren');
$GLOBALS['TL_LANG']['tl_slideshow2']['copy']   				= array('Bildrotation kopieren', 'Bildrotation ID %s kopieren');
$GLOBALS['TL_LANG']['tl_slideshow2']['delete'] 				= array('Bildrotation l&ouml;schen', 'Bildrotation ID %s l&ouml;schen');
$GLOBALS['TL_LANG']['tl_slideshow2']['show']   				= array('Bildrotation Details', 'Zeige Details der Bildrotation ID %s');

?>