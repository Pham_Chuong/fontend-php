<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010 
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de> / Aeron Glemann <http://www.electricprism.com/aeron/>
 * @package    Slideshow2 
 * @license    LGPL 
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['description']	= array('Beschreibung', 'Diese Beschreibung wird bei aktivierten Untertiteln angezeigt.');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['alt']   			= array('Alternativer Text', 'Eine barrierefreie Webseite sollte immer einen alternativen Text für Bilder mit einer kurzen Beschreibung deren Inhalts enthalten.');

$GLOBALS['TL_LANG']['tl_slideshow2_elements']['src']     		= array('Bildauswahl', 'Sie können Bilder und Ordner auswählen.');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['img']     		= array('Bildgr&ouml;&szlig;e anpassen', 'Achtung! Die Slideshow passt die Bildbreite und -höhe automatisch auf die Maße der Haupteinstellungen an. Daher sollte diese Option nur aktiviert werden wenn die Dateigröße verringert werden soll, da es zu Qualitätsverlusten führt.');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['img_size']  		= array('Bildbreite und Bildh&ouml;he', 'Die verwendete Einheit sind Pixel.');

$GLOBALS['TL_LANG']['tl_slideshow2_elements']['url']      		= array('Rotation verlinken', 'Einen Link hinzufügen');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['url_link']  		= array('Link-Adresse', 'Eine eigene Link-Adresse überschreibt den Lightbox-Link, so dass das Bild nicht mehr in der Großansicht dargestellt werden kann.');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['url_title']  	= array('Titel', 'Wird beim Mouseover auf dem Link angezeigt');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['url_window']  	= array('Neues Fenster', 'Der Link wird XHTML-konform mittels Javascript im neuen Fenster ge&ouml;ffnet');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['url_fullsize']   = array('Großansicht', 'Einen Lightbox-Link zur Großansicht des Bildes hinzufügen.');


/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['title_legend']  = 'Titel';
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['src_legend']    = 'Bildauswahl';
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['url_legend']    = 'Verlinkung';


/**
 * Label
 */
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['label_file'] = 'Datei';
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['label_folder'] = 'Verzeichnis';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['new']    = array('Neues Element', 'Eine neues Element anlegen');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['edit']   = array('Element editieren', 'Element ID %s editieren');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['copy']   = array('Element kopieren', 'Element ID %s kopieren');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['delete'] = array('Element l&ouml;schen', 'Element ID %s l&ouml;schen');
$GLOBALS['TL_LANG']['tl_slideshow2_elements']['show']   = array('Element Details', 'Zeige Details des Element ID %s');

?>