<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010 
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de> / Aeron Glemann <http://www.electricprism.com/aeron/>
 * @package    Slideshow2 
 * @license    LGPL 
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['title']				= array('Name of the Slideshow', 'Only viewable in the backend');
$GLOBALS['TL_LANG']['tl_slideshow2']['slideshow2_size']		= array('Slidewidth and -height', 'If the imagesize is not the same as the show they will be automatically resized by the slideshow2');

$GLOBALS['TL_LANG']['tl_slideshow2']['rotation_interval']   = array('Rotation interval', 'Value in milliseconds.');

$GLOBALS['TL_LANG']['tl_slideshow2']['effect_type']     	= array('Effect type', 'Please select one effect');
$GLOBALS['TL_LANG']['tl_slideshow2']['effect_duration'] 	= array('Effect duration', 'Value in milliseconds.');

$GLOBALS['TL_LANG']['tl_slideshow2']['effects_extended']    = array('Extended Effects', 'Based on the mootools Fx.Transitions library');
$GLOBALS['TL_LANG']['tl_slideshow2']['effect_transition']   = array('Effect transition', 'see http://demos111.mootools.net/Fx.Transitions for examples');
$GLOBALS['TL_LANG']['tl_slideshow2']['effect_ease']  		= array('Ease', 'For a natural tweening');

$GLOBALS['TL_LANG']['tl_slideshow2']['controls']     		= array('Activate controls', 'Gives the user the ability to control the slides');
$GLOBALS['TL_LANG']['tl_slideshow2']['controls_type']     	= array('Transition speed', 'Affects how the show behaves when the user is navigating via the controller or keyboard');

$GLOBALS['TL_LANG']['tl_slideshow2']['play_random']  		= array('Random imageorder', 'The order of the images is randomly');
$GLOBALS['TL_LANG']['tl_slideshow2']['play_paused']  		= array('Autoplay deactivated', 'The Slideshow doesnt start automatically');
$GLOBALS['TL_LANG']['tl_slideshow2']['play_loop']  			= array('Play only once', 'The Slideshow shows all images and stops');
$GLOBALS['TL_LANG']['tl_slideshow2']['play_image']  		= array('Show start-picture', 'During the loading of the page an image will be shown.');

$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails']  		= array('Activate thumbnails', 'Shows Thumbnails which can be used to navigate through the show');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_size']		= array('Imagewidth and -height of the thumbnails', 'Thumbnails will be shown in this size');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_padding_width'] 	= array('Thumbnail padding in width', 'As a standart value the slideshow adds 5px padding around each thumbnail.');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_padding_height']	= array('Thumbnail padding in height', 'To fit this with the other sizes the padding should be added here to get a correct layout.');
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_overlay']	= array('Thumbnail overlay', 'Adds a smoother edge over the thumbnail area. To adjust it files can be found in folder plugins/slideshow2/img/. Files are thumbnails-a.png and thumbnails-b.png.');

$GLOBALS['TL_LANG']['tl_slideshow2']['captions']  			= array('Activate captions', 'Shows captions under each image which will be taken from the image description');
$GLOBALS['TL_LANG']['tl_slideshow2']['captions_meta']  		= array('Use Meta-Data', 'Uses the information from the meta.txt if its available.');

$GLOBALS['TL_LANG']['tl_slideshow2']['template_css']     	= array('CSS Template', 'The styling of the show can be customized with a template.');
$GLOBALS['TL_LANG']['tl_slideshow2']['template_js'] 		= array('JS Template', 'Not all functions being offered by the Slideshow2 script can be selected with this backend interface. An overview of additional options can be found here: http://code.google.com/p/slideshow/wiki/Slideshow');


/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['title_legend']     	= 'title and size';
$GLOBALS['TL_LANG']['tl_slideshow2']['rotation_legend']    	= 'rotation';
$GLOBALS['TL_LANG']['tl_slideshow2']['effects_legend']     	= 'effects';
$GLOBALS['TL_LANG']['tl_slideshow2']['play_legend']    		= 'play';
$GLOBALS['TL_LANG']['tl_slideshow2']['controls_legend']    	= 'controls';
$GLOBALS['TL_LANG']['tl_slideshow2']['thumbnails_legend']   = 'thumbnails';
$GLOBALS['TL_LANG']['tl_slideshow2']['captions_legend']    	= 'captions';
$GLOBALS['TL_LANG']['tl_slideshow2']['template_legend']     = 'template';

/**
 * Reference
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_control_type'][0]	= 'slow - use transitions';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_control_type'][1]	= 'normal - skip transitions if paused';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_control_type'][2]	= 'fast - skip all transitions and update the slide change instantly';

$GLOBALS['TL_LANG']['tl_slideshow2']['sequence'][1]			= 'hintereinander';
$GLOBALS['TL_LANG']['tl_slideshow2']['sequence'][2]			= 'zuf&auml;llig';

$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][1]	= 'Alpha Fade';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][2]	= 'Flash';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][3]	= 'Fold';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][4]	= 'Push';
$GLOBALS['TL_LANG']['tl_slideshow2']['ref_effect_type'][5]	= 'Ken Burns';

/**
 * misc
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['misc_images'] 		= 'images';
$GLOBALS['TL_LANG']['tl_slideshow2']['misc_noimages']		= 'Please select images for the slideshow. <br />(Click on edit and create a new element.)';



/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_slideshow2']['new']    = array('New imagerotation', 'Create a new imagerotation');
$GLOBALS['TL_LANG']['tl_slideshow2']['edit']   = array('Edit imagerotation', 'Edit imagerotation ID %s');
$GLOBALS['TL_LANG']['tl_slideshow2']['copy']   = array('Copy imagerotation', 'Copy imagerotation ID %s');
$GLOBALS['TL_LANG']['tl_slideshow2']['delete'] = array('Delete imagerotation', 'Delete imagerotation ID %s');
$GLOBALS['TL_LANG']['tl_slideshow2']['show']   = array('Details imagerotation', 'Show deatils for imagerotation ID %s');

?>