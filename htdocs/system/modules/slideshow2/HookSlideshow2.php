<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010 
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de> / Aeron Glemann <http://www.electricprism.com/aeron/>
 * @package    Slideshow2 
 * @license    LGPL 
 * @filesource
 */


/**
 * Class HookSlideshow2
 * 
 * Hook for insert-tag based on Felix Pfeiffer's Hook for photoshow
 *  
 * @copyright  Die Kommunikationsabteilung - Fauth und Gundlach GmbH - 2009-2010 / Felix Pfeiffer : Neue Medien 
 * @author     Sabri Karadayi <karadayi@kommunikationsabteilung.de> / Felix Pfeiffer <info@felixpfeiffer.com> 
 * @package    Controller
 */
class HookSlideshow2 extends Controller
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_slideshow2_insert-tag';

	/**
	 * External Files
	 * @var string
	 */
	protected $strJSFile = 'plugins/slideshow2/js/slideshow.js';	
	protected $strCSSFile = 'plugins/slideshow2/css/slideshow.css';	
		
	/**
	 * Script container because Global var is available
	 * @var string
	 */
	protected $scripts = '';
	
	/**
	 * Generate module
	 */
	public function slideshow2($strTag)
	{
		
		//echo $this->strTemplate;
		
		$elements = explode('::', $strTag);
		
		if (TL_MODE == 'BE' || $elements[0] != 'slideshow2')
		{
			return false;
		}
		
		$this->import('Database');
		$this->import('FrontendTemplate');
		$this->import('String');

		$Template = new FrontendTemplate($this->strTemplate);
		$Template->class = 'mod_slideshow2';
		
		$objSettings 	= $this->Database->prepare("SELECT * FROM tl_slideshow2 WHERE id=?")
						  ->execute($elements[1]);
								
		
		if($objSettings->numRows > 0)
		{
			
		//-- Create Data Arrays
			$objElements 	= $this->Database->prepare("SELECT * FROM tl_slideshow2_elements WHERE pid=? ORDER by sorting ASC")
						   	  ->execute($objSettings->id);
			
			//-- Prepare Data
			$arrSlideshow2Size 			= unserialize($objSettings->slideshow2_size);
			
			if($objSettings->thumbnails) 
			$arrSlideshow2ThumbSize     = unserialize($objSettings->thumbnails_size);
			
			$this->scripts .= '<script type="text/javascript" src="'. $this->strJSFile . '"></script>';
			$this->scripts .= '<link rel="stylesheet" href="'. $this->strCSSFile . '" type="text/css" media="screen" />';
			
		 /** --------------------------------------------------------------------
		  * Generate HTML Image Array 
		  * -------------------------------------------------------------------- */
			
			//-- Reset Variables
			$x = 0;
			$url_link = false;
			$url_fullsize = false;
			
			//-- Get Data from all selected elements
			while ($objElements->next())
			{
				if (strncmp($objElements->src, '.', 1) === 0)
				{
					continue;
				}
	
			 /** --------------------------------------------------------------------
			  * img_src is Directory
			  * -------------------------------------------------------------------- */
				if (is_dir(TL_ROOT . '/' . $objElements->src))
				{
					$subfiles = scan(TL_ROOT . '/' . $objElements->src);
		
					foreach ($subfiles as $subfile)
					{
						if (strncmp($subfile, '.', 1) === 0 || is_dir(TL_ROOT . '/' . $objElements->src . '/' . $subfile))
						{
							continue;
						}
		
						$objFile = new File($objElements->src . '/' . $subfile);
		
						if ($objFile->isGdImage)
						{
							//-- Imagenumber
							$arrElements[$x]['id'] = $x;
							
							//-- Thumbnails
							if ($objSettings->thumbnails)
							{
								$arrElements[$x]['thumb_src'] = $this->getImage($objElements->src . '/' . $subfile, $arrSlideshow2ThumbSize[0], $arrSlideshow2ThumbSize[1]); 
								$arrElements[$x]['thumb_size'] = $arrSlideshow2ThumbSize;
							}
							
							//-- Image resizing
							if ($objElements->img)
							{
								$arrElements[$x]['img_size'] = unserialize($objElements->img_size);
								$arrElements[$x]['src'] = $this->getImage($objElements->src . '/' . $subfile, $arrElements[$x]['img_size'][0], $arrElements[$x]['img_size'][1]);
							}
							else
							{
								$arrElements[$x]['img_size'] = array($objFile->width,$objFile->height);
								$arrElements[$x]['src'] = $objElements->src . '/' . $subfile;
							}
							
							//-- Link
							$arrElements[$x]['url'] 		 = $objElements->url;
							
							if ($objElements->url_link && $objElements->url)
							{
								$arrElements[$x]['url_link'] 		= ($objElements->url_link);
								$arrElements[$x]['url_title']		= $objElements->url_title;
								$arrElements[$x]['url_window']		= $objElements->url_window;
								
								$arrElements[$x]['url_fullsize'] 	= false;
								
								$url_link = true;
							}
							elseif ($objElements->url_fullsize && $objElements->url)
							{
								$arrElements[$x]['url_link'] 		= $objElements->src . '/' . $subfile;
								$arrElements[$x]['url_fullsize'] 	= true;
								
								$url_fullsize = true;
							}
							
							// Description and alt 
							if ($objSettings->captions_meta)
							{
								//-- Use Typolight Function to parse meta
								$this->parseMetaFile($objElements->src);
								$arrMeta = $this->arrMeta[$objFile->basename];
								
								// Take filename if there is no meta info for that file
								if ($arrMeta[2] == '')
									$arrMeta[0] = str_replace('_', ' ', preg_replace('/^[0-9]+_/', '', $objFile->filename));
									
								$arrElements[$x]['description'] = $arrMeta[0];
								$arrElements[$x]['alt'] = $arrMeta[0];	
							}
							else
							{
								$arrElements[$x]['description'] = $objElements->description;
								$arrElements[$x]['alt'] = $objElements->alt;
							}
												
							//-- count image
							$x++;
						}
					}
	
					continue;
				}
	
			 /** --------------------------------------------------------------------
			  * img_src is File
			  * -------------------------------------------------------------------- */
				
				if (is_file(TL_ROOT . '/' . $objElements->src))
				{
					$objFile = new File($objElements->src);
		
					if ($objFile->isGdImage)
					{
						//-- Imagenumber
						$arrElements[$x]['id'] = $x;
						
						//-- Thumbnails
						if ($objSettings->thumbnails)
						{
							$arrElements[$x]['thumb_src'] = $this->getImage($objElements->src, $arrSlideshow2ThumbSize[0], $arrSlideshow2ThumbSize[1]); 
							$arrElements[$x]['thumb_size'] = unserialize($objSettings->thumbnails_size);
						}
						
						//-- Image resizing
						if ($objElements->img)
						{
							$arrElements[$x]['img_size'] = unserialize($objElements->img_size);
							$arrElements[$x]['src'] = $this->getImage($objElements->src, $arrElements[$x]['img_size'][0], $arrElements[$x]['img_size'][1]);
						}
						else
						{
							$arrElements[$x]['img_size'] = array($objFile->width,$objFile->height);
							$arrElements[$x]['src'] = $objElements->src;
						}
						
						//-- Link
						$arrElements[$x]['url'] = $objElements->url;
						
						if ($objElements->url_link && $objElements->url)
						{
							$arrElements[$x]['url_link'] 		= ($objElements->url_link);
							$arrElements[$x]['url_title']		= $objElements->url_title;
							$arrElements[$x]['url_window']		= $objElements->url_window;
							
							$arrElements[$x]['url_fullsize'] 	= false;
							
							$url_link = true;
						}
						elseif ($objElements->url_fullsize && $objElements->url)
						{
							$arrElements[$x]['url_link'] 		= $objElements->src;
							$arrElements[$x]['url_fullsize'] 	= true;
							
							$url_fullsize = true;
						}
						
						//echo dirname($objElements->src);
						
						// Description with or without meta file
						if ($objSettings->captions_meta)
						{
							//-- Use Typolight Function to parse meta
							$this->parseMetaFile(dirname($objElements->src));
							$arrMeta = $this->arrMeta[$objFile->basename];
	
							//print_r($arrMeta);
							
							// Take filename if there is no meta info for that file
							if ($arrMeta[2] == '')
								$arrMeta[0] = str_replace('_', ' ', preg_replace('/^[0-9]+_/', '', $objFile->filename));
									
							$arrElements[$x]['description'] = $arrMeta[0];
							$arrElements[$x]['alt'] = $arrMeta[0];	
						}
						else
						{
							$arrElements[$x]['description'] = $objElements->description;
							$arrElements[$x]['alt'] = $objElements->alt;
						}
						
						//-- count image
						$x++;
					}
				}
			}
	
			
		 /** --------------------------------------------------------------------
		  * CSS Template
		  * --------------------------------------------------------------------*/ 
		
			/* Create CSS Template */
			$objTplCSS = new FrontendTemplate($objSettings->template_css);
		    
			$objTplCSS->id 						= $objSettings->id;
			$objTplCSS->arrSlideshow2Size 		= $arrSlideshow2Size;
			$objTplCSS->arrSlideshow2ThumbSize 	= $arrSlideshow2ThumbSize;
			$objTplCSS->totalElements			= count($arrElements)+1;
			
			if ($objSettings->controls) 		$objTplCSS->controls = true;
			if ($objSettings->captions) 		$objTplCSS->captions = true;
			
			if ($objSettings->thumbnails) 
			{
				$objTplCSS->thumbnails 						= true;
				$objTplCSS->arrSlideshow2ThumbPaddingWidth	= $objSettings->thumbnails_padding_width;
				$objTplCSS->arrSlideshow2ThumbPaddingHeight	= $objSettings->thumbnails_padding_height;				
			}
			
			/* Add CSS in HTML head 
			 * str_replace(array("\r\n", "\n", "\r", "\t"), "", $objTplCSS->parse());
			 * */
			$this->scripts .= $objTplCSS->parse();
	
	
	
		 /** --------------------------------------------------------------------
		  * JS Template
		  * --------------------------------------------------------------------*/
	
			//-- Create Image Array
			$elementsTotal = count($arrElements); 
			$x = 1;

			if (is_array($arrElements))
			{		
				foreach ($arrElements as $element)
				{
					$imgExtended = '';
					
					//-- Additional image data (Thumbnail and link)
					if ($objSettings->thumbnails) $imgExtended = ",thumbnail:'" . $element['thumb_src'] . "'";
					if ($element['url'] && $url_link)
					{
						$imgExtended .= ",href:'" . $element['url_link'] . "'";
					}
					else if($element['url'] && $url_fullsize)
					{ 
						$imgExtended .= ",href:'" . $element['src'] . "'";
					}
					
					$imgElements .= "'" .  $element['src']. "':{caption:'" . str_replace('/', '\/', $element['description']) . "'" . $imgExtended . "}";
					
					if ($x != $elementsTotal) $imgElements .= ","; 
					
					$x++;	
				}
			}
			
			//-- Type
			if ($objSettings->effect_type != 'Alpha')
			{
				$EffectType = '.' . $objSettings->effect_type;
				$this->scripts .= '<script type="text/javascript" src="plugins/slideshow2/js/slideshow.' . strtolower($objSettings->effect_type) . '.js"></script>';
			}
			
			//-- Extended
			if ($objSettings->effects_extended)
			{
				$EffectsExtended = 'transition: \'' . strtolower($objSettings->effect_transition) . ':' . $objSettings->effect_ease . '\',';
			}
	
			//-- Create JS Template 
			$objTplJS = new FrontendTemplate($objSettings->template_js);
		    
			$objTplJS->id 					= $objSettings->id;
	
			$objTplJS->imgElements 			= $imgElements;
			$objTplJS->arrSlideshow2Size 	= $arrSlideshow2Size;
			$objTplJS->EffectType 			= $EffectType;
			$objTplJS->effect_duration 		= $objSettings->effect_duration;
			$objTplJS->EffectsExtended 		= $EffectsExtended;
			$objTplJS->rotation_interval 	= $objSettings->rotation_interval;
			$objTplJS->controls_type		= $objSettings->controls_type;
	
	 		$objTplJS->play_loop	= ($objSettings->play_loop)		? 	'false' : 'true' ;
			$objTplJS->play_paused	= ($objSettings->play_paused)	? 	'true' : 'false' ;
			$objTplJS->play_random	= ($objSettings->play_random)	? 	'true' : 'false' ;
			$objTplJS->play_image	= ($objSettings->play_image)	?	'true' : 'false' ;
			$objTplJS->controls		= ($objSettings->controls)		? 	'true' : 'false' ;
			$objTplJS->thumbnails	= ($objSettings->thumbnails)	? 	'true' : 'false' ;
			$objTplJS->captions		= ($objSettings->captions)		? 	'true' : 'false' ;
	
			/* Add JS in HTML head 
			 * str_replace(array("\r\n", "\n", "\r", "\t"), "", $objTplJS->parse());
			 * $objTplJS->parse();	
			 * 
			 * */
			$this->scripts .= $objTplJS->parse();		
	
	
		 /** --------------------------------------------------------------------
		  * mod_slideshow2 template
		  * -------------------------------------------------------------------- */
			
			$Template->scripts = $this->scripts;
			$Template->fullsize = $url_fullsize;
			$Template->id = $objSettings->id;
			$Template->element = $arrElements[0];
				

		}
		
		return $Template->parse();

	}
}

?>