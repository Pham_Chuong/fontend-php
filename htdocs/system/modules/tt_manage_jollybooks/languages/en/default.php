<?php 

$GLOBALS['TL_LANG']['CTE']['pnovel_element'] = array('PersonalNOVEL Elements', 'PersonalNOVEL Elements Manager');
$GLOBALS['TL_LANG']['CTE']['custom_listing'] = array('Custom Book Listing', 'PersonalNOVEL Custom Book Listing');
$GLOBALS['TL_LANG']['tl_content']['book_detail_url'] = array('Detail Url', 'Link to detail page');
$GLOBALS['TL_LANG']['tl_content']['custom_book_selected'] = array('Books selected', 'Choose books');
$GLOBALS['TL_LANG']['tl_content']['select_legend'] = 'Select';
