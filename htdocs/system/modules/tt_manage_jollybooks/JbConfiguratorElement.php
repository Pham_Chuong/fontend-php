<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class JbConfiguratorElement extends ContentElement
{
    protected $strTemplate = 'ce_jb_configurator';
    protected $config;
    public   $domain;
    
    public function generate()
	{
	    if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
			$objTemplate->wildcard = '### PERSONAL JOLLYBOOKS CONFIGURATOR ###';
			$objTemplate->title = 'Personal JollyBooks\' Configurator';
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
		    $GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_jollybooks/html/css/style.css';
			
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_jollybooks/html/js/script.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_jollybooks/html/js/custom.js';
			
			$this->config = new PnConfig();
		}
		
		return parent::generate();
	}
	
	protected function compile()
	{
	    $configurator_page = $GLOBALS['TL_CONFIG']['configurator_page'] ? $GLOBALS['TL_CONFIG']['configurator_page'] : '/jollyconfigurator';
	    $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
	    $this->domain   = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.de";
	    
	    $step = $_GET['step'] ? $_GET['step'] : $_POST['step'];
        $step = $step ? $step : 1;
        $this->Template->step = $step;
        
        //Step 1
        if($step == 1)
        {
            if($_GET['book'])
            {
                $book = new PnBook();
                $book->current($_GET['book']);
                
                //Check if JollyBooks
                if($book->type_id == 25)
                {
                    $order     = $this->getOrCreateOrder();
                    $book_item = $this->createBookItem($order, $book);
                    
                    if($book_item)
                    {
                        $token = generateToken($order->id);
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", $configurator_page, $order->id, $book_item->id, 1, $token));
                    }
                }
                else
                {
                    //gotoErrorPage();
                }
            }
            
            $this->step1();
            
            if($_POST['submitter'])
            {
                $book_item = $this->editBookItem();
                if($book_item)
                {
                    $token = generateToken($book_item->order_id);
                    header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&token=%s", $configurator_page, $book_item->order_id, $book_item->id, 2, $token));
                }
            }
            
            if($_POST['quicksubmitter'] || $_POST['submit_to_cart'])
            {
                $book_item = $this->editBookItem();
                if($book_item)
                {
                    $token = generateToken($book_item->order_id);
                    header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $book_item->order_id, $token));
                }
            }
        }
        
        //Step 2
        if($step == 2)
        {
            $this->step2();
            
            if($_POST['submitter'] || $_POST['submit_to_cart'])
            {
                $book_item = new PnBookItem();
	            $book_item->initWithId($_GET["bookitem"]);
	            $book_item->edit(array('artikel_id' => $_POST['article']));
	            
	            $token = generateToken($book_item->order_id);
	            header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $book_item->order_id, $token));
            }
        }
	}
	
	function getOrCreateOrder()
	{
	    $order = new PnOrder();
	    
	    if($_GET['order'])
        {
            $order->initWithId($_GET['order']);
            
            if($order->id == "" || $order->id == 0)
            {
                gotoErrorPage();
            }
        }
        elseif($_COOKIE['PN_order_id'] != '')
        {
            $order->initWithId($_COOKIE['PN_order_id']);
        }
        
        if($order->id == "" || $order->id == 0)
        {
            $id = $order->add(
                            array(
                                "order_status"      => 0,
                                "order_date"        => date('Y/m/d H:i:s a', time()),
                                "order_created"     => date('Y/m/d H:i:s a', time()),
                                "order_updated"     => date('Y/m/d H:i:s a', time()),
                                "order_converted"   => 1,
                                "order_version"     => 4,
                                "order_invoice"     => 1,
                                "locale_id"         => 1,
                                "mandant_id"        => 1,
                                "konto_id"          => $order->createKonto()->id,
                                "debitor_id"        => 1,
                                "partner_id"        => 1,
                                "payment_method_id" => 1
                            )
                        );
            
            $order->initWithId($id);
            $order->edit(array("konto_id" => $order->createKonto()->id));
            $order->createRandomKey();
            
            setcookie('PN_order_id', $order->id, time() + (86400 * 7), "/",$this->domain);
        }
        return $order;
	}
	
	function createBookItem($order, $book)
	{
	    $versions = 0;
	    $fields   = array();
	    $class_id = 2;
	    $book_documents = $book->getBookDocuments();
	    
	    if(count($bookDocuments) > 0)
        {
            $book_document = $book_documents[0];
        }
	    
	    //Set field value
	    foreach($_POST as $k => $v)
        {
            if($this->config->startsWith($k, 'vars-'))
            {
                $k = str_replace('vars-', '', $k);
                $fields[utf8_decode(utf8_encode($k))] = utf8_decode(utf8_encode($v));
            }
        }
        
        $fields    = json_encode($fields);
        $book_item = new PnBookItem(); 
        $item_id   = $book_item->add(
                        array(
                                    'order_id' 			=> $order->id,
                                    'buch_id'			=> $book->id,
                                    'class_id'			=> $class_id,
                                    'artikel_id'		=> $book->article_id,
                                    'font_id'			=> 1,
                                    'document_id'       => $book_document->id,
                                    'item_pieces'		=> 1,
                                    'item_print'		=> 0,
                                    'item_testprint'	=> 0,
                                    'item_fields'		=> $fields,
                                    'item_created'      => date('Y/m/d H:i:s a', time()),
                                    'item_updated'      => date('Y/m/d H:i:s a', time()),
                                    'item_touched'      => date('Y/m/d H:i:s a', time())
                              )
                        );
        
        $book_item->initWithId($item_id);
        return $book_item;
	}
	
	function editBookItem()
	{
	    $fields = array();
	    $empty_fields = array();
	    
	    $item = new PnBookItem();
	    $item->initWithId($_GET["bookitem"]);
        $fieldsNotValidate = $this->notValidateField($item->book_id);
        
	    
        foreach($_POST as $k => $v)
        {
            if($this->config->startsWith($k, 'vars-'))
            {
                $k = str_replace('vars-', '', $k);
                $fields[utf8_decode(utf8_encode($k))] = utf8_decode(utf8_encode($v));
                
                if(trim($v) == "" && !(in_array($k,$fieldsNotValidate)))
                    $empty_fields[$k] = "vars-" . $k;
            }
        }
        
        if(count($empty_fields) > 0)
        {
            $this->Template->empty_fields  = $empty_fields;
            return false;
        }
        
	    
	    $item->edit(
                array(
                    "item_fields"	 => json_encode($fields),
                    "item_updated"   => date('Y/m/d H:i:s a', time()),
                    "item_touched"   => date('Y/m/d H:i:s a', time())
                )
            );
        
        $item->initWithId($_GET["bookitem"]);
        
        return $item;
	}
	
	//Validate field value Personal Gift
	function notValidateField($bookId){
		switch ($bookId) {
			default:
				return array();
			break;
		}
	}
	
	function step1()
	{
	    checkUrlToken();
	    
	    if($_GET["bookitem"])
	    {
	        $order = new PnOrder();
	        $order->initWithId($_GET["order"]);
	        
	        $book_item = new PnBookItem();
            $book_item->initWithId($_GET["bookitem"]);
            
            $book = new PnBook();
            $book->current($book_item->book_id);
            
            $entities     = $book->getEntities();
            $fields       = json_decode($book_item->fields);
            
			try
			{
				foreach($entities as $k1 => $v1)
				{
					foreach($v1['fields'] as $k2 => $v2)
					{
						if(count($fields) > 0)
						{
							foreach($fields as $k3 =>$v3)
							{
								if($v2['field_name'] == $k3)
								{
									$entities[$k1]['fields'][$k2]['field_custom'] = utf8_decode($v3);
								}
							}
						}
					}
				}
			}
			catch(Exception $e)
			{
				
			}
            
            $this->Template->entities = $entities;
            $this->Template->book = $book;
            $this->Template->empty_fields = array();
            $this->Template->totalPrice = $order->getTotalPrice();
            
            $img = "/tl_files/thumbnail-book/" . standardize($book->title) . "-" . $book->id . "-detail.png";
            $this->Template->src_thumbnail = $img;
	    }
	    else
	    {
	        //gotoErrorPage();
	    }
	}
	
	function step2()
	{
	    checkUrlToken();
	    
	    if($_GET["bookitem"])
	    {
	        $order = new PnOrder();
	        $order->initWithId($_GET["order"]);
	        
	        $book_item = new PnBookItem();
            $book_item->initWithId($_GET["bookitem"]);
            
            $book = new PnBook();
            $book->initWithId($book_item->book_id);
            
            $this->Template->book = $book;
            $this->Template->selected_article_id = $book_item->article_id;
            $this->Template->totalPrice = $order->getTotalPrice();
	    }
	    else
	    {
	        gotoErrorPage();
	    }
	}
}
