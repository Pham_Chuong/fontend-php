function DataKeeper(){
	var available_sizes = ["s","m","l","xl"];
	var available_bindings = [
		{"title":"Spiralbindung + Farbdruck", "value":"spiral"},
		{"title":"Hardcover + Farbdruck", "value":"hardcover_farbdruck"},
		{"title":"Hardcover + Pragung", "value":"hardcover_pragung"}
	];
	var available_lineaturs = [
		{"title":"Blanko", "value":"blanko"},
		{"title":"Punktiert", "value":"punktiert"},
		{"title":"Liniert", "value":"liniert"}
	];
	var available_papers = [
		
	];
	
	var listenners = [];
	var data = {
		"selected_element":"",
		"size": "size_s",
		"binding": "binding_spiral",
		"Buchtitel":"Kalender",
		"font":5,
		"state_id" : 11, 
		"country_id" : 3,
		"sections_note": 465,
		"sections_individual": 402,
		"sections_legend": 410,
		"sections_month": 406,
		"sections_day": 432,
		"Hauptperson_NN":"",
		"Hauptperson_VN":"",
		"calendar_event_categories":[6,10],
		"year_order":"2014",
		"note_order":"",
		"locale_id":1,
		"paper":"white",
		"startdate":"2014-05-01 00:00:00",
		"widmung_text":"",
		"widmung_color":"black",
		"widmung_fontsize":13,
		"cover_title":"",
		"cover_subtitle":""
	};
	
	this.addListenner = function(obj){
		listenners.push(obj);
	};
	
	this.getData = function(){
		return data;
	};
	
	this.setData = function(key, value){ 
	    if(!key || key == 'undefined') key = 'selected_elment';
		data[key] = value;
		data['selected_element'] = value;
		console.log("set " + key + " = " + value);
		//rebuild data
		//TODO: later
		
		//notify
		$(listenners).each(function(){
			this.update();
		});
		
		//udpate price
		this.calculatePrice();
		
		//return
		var response = [];
		if(key == "binding_layout"){
		    if(value == "binding_spiral_layout1" || value == "binding_hardcover_farbdruck_layout1" )
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_1",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_414.png"
                            },
                            {
                                "ItemId":"color_2",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_415.png"
                            },
                            {
                                "ItemId":"color_3",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_416.png"
                            },
                            {
                                "ItemId":"color_4",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_417.png"
                            },
                            {
                                "ItemId":"color_5",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn_418.png"
                            }
                        ]
		            }
		        }
		    }
		    
		    if(value == "binding_spiral_layout2" || value == "binding_spiral_layout3" 
		        || value == "binding_hardcover_farbdruck_layout2" 
		        || value == "binding_hardcover_farbdruck_layout3"
		        || value == "binding_spiral_layout4"
		        || value == "binding_hardcover_farbdruck_layout4"){
		        response = {
                    "reload":{
                        "name":"color",
                        "items":[
                            {
                                "ItemId":"color_D3130B",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_D3130B.png"
                            },
                            {
                                "ItemId":"color_D30B63",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_D30B63.png"
                            },
                            {
                                "ItemId":"color_F8B302",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_F8B302.png"
                            },
                            {
                                "ItemId":"color_43C08E",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_43C08E.png"
                            },
                            {
                                "ItemId":"color_387C5B",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_387C5B.png"
                            },
                            {
                                "ItemId":"color_22F1B2",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_22F1B2.png"
                            },
                            {
                                "ItemId":"color_1589AD",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_1589AD.png"
                            },
                            {
                                "ItemId":"color_0202FC",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_0202FC.png"
                            },
                            {
                                "ItemId":"color_A747AF",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_A747AF.png"
                            },
                            {
                                "ItemId":"color_5E5E5F",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btn-color_5E5E5F.png"
                            }
                        ]
                    }
                };
		    }
		    
		    //binding_hardcover_pragung_leinen
		    if(value == "binding_hardcover_pragung_leinen")
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_leinen_55",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_55.png"
                            },
                            {
                                "ItemId":"color_leinen_03",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_03.png"
                            },
                            {
                                "ItemId":"color_leinen_29",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_29.png"
                            },
                            {
                                "ItemId":"color_leinen_33",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_33.png"
                            },
                            {
                                "ItemId":"color_leinen_12",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_12.png"
                            },
                            {
                                "ItemId":"color_leinen_51",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-leinen_51.png"
                            }
                        ]
		            }
		        }
		    }
		    
		    //binding_hardcover_pragung_wintan
		    if(value == "binding_hardcover_pragung_wintan")
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_wintan_14",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-wintan_14.png"
                            },
                            {
                                "ItemId":"color_wintan_20",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-wintan_20.png"
                            },
                            {
                                "ItemId":"color_wintan_99",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-wintan_99.png"
                            }
                        ]
		            }
		        }
		    }
		    
		    //binding_hardcover_pragung_satin
		    if(value == "binding_hardcover_pragung_satin")
		    {
		        response = {
		            "reload":{
		                "name":"color",
                        "items":[
                            {
                                "ItemId":"color_satin_27",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_27.png"
                            },
                            {
                                "ItemId":"color_satin_28",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_28.png"
                            },
                            {
                                "ItemId":"color_satin_15",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_15.png"
                            },
                            {
                                "ItemId":"color_satin_11",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_11.png"
                            },
                            {
                                "ItemId":"color_satin_10",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_10.png"
                            },
                            {
                                "ItemId":"color_satin_22",
                                "ItemName":"color",
                                "Title":" ",
                                "ItemType":"image",
                                "ItemSrc":"imgs/btnfarbe-satin_22.png"
                            }
                        ]
		            }
		        }
		    }
		    
		}
		
		return response;
		
	};
	
	this.sendData = function(){ 
		
		console.log("send data AJAX", data)
	};
	
	this.calculatePrice = function(){
	    var binding_price = {
	        "binding_spiral": 0,
	        "binding_hardcover_farbdruck": 3,
	        "binding_hardcover_pragung": 3
	    };
	    
	    var size_price = {
	        "size_s": 0,
	        "size_m": 3,
	        "size_l": 4,
	        "size_xl": 5
	    };
	    
	    var price = 24.95 + binding_price[data["binding"]] + size_price[data["size"]];
		$("#price span").html(price);
	};
}
