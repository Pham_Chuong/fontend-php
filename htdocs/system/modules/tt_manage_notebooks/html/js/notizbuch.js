/**
 * 
 */

(function($){
   var Cover = function(element,object_data)
   {
       var elem = $(element);
       var obj = this;

       this.initCover = function()
       {
           var step = ['step-bindung','step-grobe','step-farbe','step-titel','step-gummiband','step-zeichenband'];
           var html = '';
           for(var i=0;i<step.length;i++){
        	   html = html + '<div id="' + step[i] + '" class="circle">' + (i+1) ;
        	   html = html + '<div  class="tip" id="option_' + step[i] + '"></div>';
        	   html = html + '</div>';
           }
           
           elem.html( html + '<img src="./imgs/cover-blue.png" />');
           
           
       };
       
      //get option cover step 1
       this.getBindung = function(){
    	   
    	   var option = [
    	         		{"title":"Spiralbindung + Farbdruck", "value":"spiral"},
    	        		{"title":"Hardcover + Farbdruck", "value":"hardcover_farbdruck"},
    	        		{"title":"Hardcover + Pragung", "value":"hardcover_pragung"}
    	        	];
    	   var elem = $('#option_step-bindung');
    	   var html = '';
    	   for(var i=0;i<option.length;i++){
    		   html = html + '<div class="option" id="' + option[i]['value'] + '">'+ option[i]['title'] + '</div>';
    	   }
    	   elem.html(html);
    	   
    	   $.each($('#option_step-bindung .option'),function(){
    		   $(this).click(function(){
    			   object_data.setData('binding','binding_'+ $(this).attr('id'));
    		   });
    	   });
    	   
    	   
       };
       
      //get option cover step 2
       this.getGrobe = function(){
    	   
    	   var option = [
	         				{
								"ItemId":"size_s",
								"Selected":1,
								"ItemName":"size",
								"Title":"S"
							},
							{
								"ItemId":"size_m",
								"ItemName":"size",
								"Title":"M"
							},
							{
								"ItemId":"size_l",
								"ItemName":"size",
								"Title":"L"
							},
							{
								"ItemId":"size_xl",
								"ItemName":"size",
								"Title":"XL"
							}
    	        	];
    	   var elem = $('#option_step-grobe');
    	   var html = '';
    	   for(var i=0;i<option.length;i++){
    		   html = html + '<div class="option"  id="' + option[i]['ItemId'] + '">'+ option[i]['Title'] + '</div>';
    	   }
    	   elem.html(html);
    	   
    	   $.each($('#option_step-grobe .option'),function(){
    		   $(this).click(function(){
    			   object_data.setData('size',$(this).attr('id'));
    		   });
    	   });
    	   
    	   
       };
       
     //get option cover step 3
       this.getFarbe = function(){
    	   
    	   var option = [
	         				{
							    "ItemId":"color_1",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/farbe_414.png"
							},
							{
							    "ItemId":"color_2",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/farbe_415.png"
							},
							{
							    "ItemId":"color_3",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/farbe_416.png"
							},
							{
							    "ItemId":"color_4",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/farbe_417.png"
							},
							{
							    "ItemId":"color_5",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/farbe_418.png"
							},
							{
							    "ItemId":"color_6",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/farbe_419.png"
							}
 	        	];
	 	   var elem = $('#option_step-farbe');
	 	   var html = '';
	 	   for(var i=0;i<option.length;i++){
	 		   html = html + '<div class="option"  id="' + option[i]['ItemId'] + '"><img src="'+ option[i]['ItemSrc'] + '" alt="" /></div>';
	 	   }
	 	   elem.html(html);
	 	   
	 	   $.each($('#option_step-farbe .option'),function(){
	 		   $(this).click(function(){
	 			   object_data.setData('color',$(this).attr('id'));
	 		   });
	 	   });
	   
       };
       
       //get option cover step 4
       this.getTitel = function(){
    	   
    	   var option = [
	         				
							{
							  "ItemId":"title_3",
							  "ItemName":"title_3",
							  "Title":"Jahr",
							  "ItemType":"text"
							},
							{
							  "ItemId":"title_1",
							  "ItemName":"title_1",
							  "Title":"Ihr Name oder Titel",
							  "ItemType":"text"
							},
							{
							  "ItemId":"title_2",
							  "ItemName":"title_2",
							  "Title":"Ihr Untertitel",
							  "ItemType":"text"
							}
        	];
	 	   var elem = $('#option_step-titel');
	 	   var html = '';
	 	   for(var i=0;i<option.length;i++){
	 		   html = html + '<div class="option"><input type="text" id="' + option[i]['ItemId'] + '" name="' + option[i]['ItemName'] + '" placeholder="' + option[i]['Title'] + '"></div>';
	 	   }
	 	   elem.html(html);
	 	   
	 	   $.each($('#option_step-titel .option input'),function(){
	 		   $(this).keyup(function(){
	 			   object_data.setData($(this).attr('id'),$(this).val());
	 		   });
	 	   });
    	   
       }; 
       
       //get option cover step 5
       this.getGummiband = function(){
    	   var option = [
	         				{
							    "ItemId":"gummiband_1",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/gummiband_1.png"
							},
							{
							    "ItemId":"gummiband_2",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/gummiband_2.png"
							},
							{
							    "ItemId":"gummiband_3",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/gummiband_3.png"
							},
							{
							    "ItemId":"gummiband_4",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/gummiband_4.png"
							},
							{
							    "ItemId":"gummiband_5",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/gummiband_5.png"
							},
							{
							    "ItemId":"gummiband_6",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/gummiband_6.png"
							}
	        	];
	 	   var elem = $('#option_step-gummiband');
	 	   var html = '';
	 	   for(var i=0;i<option.length;i++){
	 		   html = html + '<div class="option"  id="' + option[i]['ItemId'] + '"><img src="'+ option[i]['ItemSrc'] + '" alt="" /></div>';
	 	   }
	 	   elem.html(html);
	 	   
	 	   $.each($('#option_step-gummiband .option'),function(){
	 		   $(this).click(function(){
	 			   object_data.setData('gummiband',$(this).attr('id'));
	 		   });
	 	   }); 
       };
       
       //get option cover step 6
       this.getZeichenband = function(){
    	   var option = [
	         				{
							    "ItemId":"zeichenband_1",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/zeichenband_1.png"
							},
							{
							    "ItemId":"zeichenband_2",
							    "ItemName":"color",
							    "Title":" ",
							    "ItemType":"image",
							    "ItemSrc":"imgs/zeichenband_2.png"
							}
	        	];
	 	   var elem = $('#option_step-zeichenband');
	 	   var html = '';
	 	   for(var i=0;i<option.length;i++){
	 		   html = html + '<div class="option"  id="' + option[i]['ItemId'] + '"><img src="'+ option[i]['ItemSrc'] + '" alt="" /></div>';
	 	   }
	 	   elem.html(html);
	 	   
	 	   $.each($('#option_step-zeichenband .option'),function(){
	 		   $(this).click(function(){
	 			   object_data.setData('zeichenband',$(this).attr('id'));
	 		   });
	 	   });
       };
       
       this.update = function(){
    	 
   		var json_data = object_data.getData();

   		$( 'div.circle' ).fadeTo( "fast", 0.4);
   		$('.tip').removeClass('active');
   		
   		if(json_data['selected_element'].indexOf("binding") >= 0){
   			$('#step-bindung').trigger('mouseenter');
   	   		$('#step-bindung .tip').addClass(' active');
   		}
   		
   		if(json_data['selected_element'].indexOf("size") >= 0){
   			$('#step-grobe').trigger('mouseenter');
   	   		$('#step-grobe .tip').addClass(' active');
   		}
   		
   		if(json_data['selected_element'].indexOf("color") >= 0){
   			$('#step-farbe').trigger('mouseenter');
   	   		$('#step-farbe .tip').addClass(' active');
   		}
   		
   		if(json_data['selected_element'].indexOf("title") >= 0){
   			$('#step-titel').trigger('mouseenter');
   	   		$('#step-titel .tip').addClass(' active');
   		}
   		
   		
   		
   	   }
       
       this.getData = function()
       {
           
       };
       
       this.setData = function()
       {
           
       };
   };
   
   var Widmung = function(element,object_data)
   {
       var elem = $(element);
       var obj = this;

       // Public method
       this.initWidmung = function(text)
       {
           elem.html('<p>'+ text +'</p>');
       };
       
       this.getData = function()
       {
           //
       };
       this.setData = function()
       {
           //
       };
   };
   
   var Layout = function(element,object_data)
   {
       var elem = $(element);
       var obj = this;

       // Public method
       this.initLayout = function()
       {
           
       };
       
       this.getData = function()
       {
          
       };
       this.setData = function()
       {
          
       };
   };
   

   $.fn.notizbuch = function(object_data)
   {
       return this.each(function()
       {
           var element = $(this);
          
           // Return early if this element already has a plugin instance
           if (element.data('widmung')) return;
           var widmung = new Widmung(this,object_data);
           // Store plugin object in this element's data
           element.data('widmung', widmung);
           
           if (element.data('cover')) return;
           var cover = new Cover(this,object_data);
           // Store plugin object in this element's data
           element.data('cover', cover);
           
           if (element.data('layout')) return;
           var layout = new Layout(this,object_data);
           // Store plugin object in this element's data
           element.data('layout', layout);
       });
   };
})(jQuery);
