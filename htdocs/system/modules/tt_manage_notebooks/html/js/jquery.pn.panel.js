(function($) {
$.fn.preview = function(name,value,only,number){
		var el = this;
		var num = this.attr('title');
		if(num==null) 
			this.attr('title',1);
			else
			this.attr('title',parseInt(num) + 1);
		if(num >= number || num == null){
			var src = '/system/modules/tt_manage_notebooks/html/img/';
			folder = this.attr('value')?this.attr('value') + '/':'';
			src  = src + folder;
			var img = $("<img />");
			if(only == null || only == 'only')
			{
				this.children().remove();
				src  = src + name + "-" + value + ".png";
				img.attr({"src": src});
				el.append(img);	
			}
			else
			if(only == 'muti')
			{
				var btn = $('.'+ name);
				if(btn != null){
					var flaImg = false;
					$.each(btn,function(){
							var1 = $(this).children('input[type=hidden]').attr('name');
							var2 = $(this).children('input[type=hidden]').attr('value');
							src = src + var1 + '-' + var2 + '_';
							if(var1 == null && var2 == null){
								flaImg = true;
							}
					});
					if(flaImg == false){
						this.children().remove();
						src = src + '.png';
						img.attr({"src": src});
						el.append(img);
					}
				}
			}
			
		}
		
	};
})(jQuery);

(function($){
$.fn.changewiddmung = function(name,value){
	switch(name){
		case 'font':
			$(this).css("font-family",value);
			break;
		case 'color':
			$(this).css("color",value);
			break;
		case 'widdmung':
			$(this).html(value);
			break;
	}	
};

})(jQuery);

(function($){
$.fn.startmonat = function(element_select){
	var el = this;
	var d=new Date();
	var month=['Januar','Januar','Februar','M&auml;rz','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'];
	var dateArray = element_select.split("-");
	
	var n = d.getMonth() + 1;
	var year = dateArray[0]? dateArray[0] : d.getFullYear();
	
	var el_s = dateArray[1] ? dateArray[1] : n;
	
	if(el_s != "10") el_s = el_s.replace("0",'');
	
	
	for (var i = el_s; i <= 12; i++){
		var nn = i < 10 ? "0"+i:i;
		var date = year + "-"+ nn +"-01 00:00:00";
		var select = el_s == i?'selected="selected"':'';
		this.append("<option value='"+ date +"' "+ select +">"+month[i] + " " + year +"</option>");
	}
	if(el_s > 1){
		year++;
		for (var y = 1; y < el_s; y++){
			var nn = y < 10 ? "0"+y:y;
			var date = year + "-"+ nn +"-01 00:00:00";
			var select = el_s==y?'selected="selected"':'';
			this.append("<option value='"+ date +"' "+ select +">"+month[y] + " " + year +"</option>");
		}
	}
	
	
};

})(jQuery);

(function($){
$.fn.slideDesign = function(){
	
	var tabContents = $(this).children('div');
	var divControl = $('<div />').append($("<div class='controls' />"));
	that = $(this);
	
	var createControls = function(){
		var i=0;
		$.each(tabContents,function(){ 
			var divTitleTab = $(this).attr('value')?$(this).attr('value'):"Tab Control "+i;
			var nameTab = $(this).attr('id')? $(this).attr('id'):"tab_"+i;
			var divControlTab = "<div value='"+ nameTab +"'>"+ divTitleTab +"</div>";
			divControl.children('.controls').append(divControlTab);
			i++;
		});
		
	};
	var buildPlugin = function(){
		createControls();
		var tabContentHTML = that.html();
		that.html("");
		var htmlBuild = divControl.html() + "<div style='clear:both'></div>" + tabContentHTML;
		that.html(htmlBuild);
	};
	var allHide = function (){
		$.each(that.children('div.contenttab'),function(){ 
			$(this).addClass('hidden');
		});
	};
	var addChild = function (){
		$.each(tabContents,function(){ 
			$(this).addClass('contenttab');
		});
	};
	var showfirst = function(){
		that.children('.contenttab:first').removeClass('hidden');
		that.children('.controls').children('div:first').addClass('selectTab');
	};
	var removeSelectTab = function(){
		that.children('.controls').children('div').removeClass('selectTab');
	};
	var createEvent = function(){
		that.children('.controls').children('div').on({
				click:function(){
					allHide();
					removeSelectTab();
					var tab = '#'+$(this).attr('value');
					$(this).addClass('selectTab');
					that.children(tab).removeClass('hidden');
				}
		})
	};
	var runmain = function (){
		addChild();
		allHide();
		buildPlugin();
		showfirst();
		createEvent();
	};	
	return {
		runNode:runmain
	}
	
};

})(jQuery);

(function($){
	$.fn.zusatzartikel = function(o){
		
		var defaults = {penloop:"null",colorpenloop:'transparent',rubberband:"null",colorrubberband:"transparent",penloop2:"null",colorpenloop2:'transparent'};
		var settings        = $.extend({}, defaults, o);
		var wrapper = this,
		    penloop = $("<div id='penloop' />"),
		    penloop2 = $("<div id='penloop2' />"),
		    rubberband = $("<div id='rubberband' />"),
		    imgload			= $("<img src='/system/modules/tt_manage_books/html/img/twirly_circleball_white_big.gif' id='twirly' style='visibility: hidden; ' />"),
		    divimg	= $("<div class='bottle-etiquette' />");
		    img = $(this).children('img');
		var init = function() {
			var h_imgload = 54,
				w_imgload = 55;
			img.addClass('bottle-etiquette');
			divimg.append(img);
			wrapper.html(divimg);
			divimg.append(penloop2);
			divimg.append(penloop);
			divimg.append(rubberband);
			divimg.before(imgload);
			
			imgload.css({
    				"float": "left",
					"position": "absolute",
					"margin":"0px",
					"padding":"0px",
					"width": w_imgload +"px",
					"height": h_imgload + "px",
					"left": (wrapper.width()-w_imgload)/2 + "px",
					"top": (wrapper.height()-h_imgload)/2 + "px"
    		});
    		
			//setdisplay();
			setbackground();
			reload();
		};
		
		var reload = function(){
			var image_url = img.attr('src');
			divimg.hide();
    		imgload.css("visibility", "visible");
    		$.preload([image_url], {
			  onComplete: function (data) {
				  if (data.found) {
					  img.attr("src", image_url);
				  } else {
				  	  divimg.hide();
				  }
			  },
			  onFinish: function(){
			  	  imgload.css("visibility", "hidden");
				  divimg.show();
			  }
			});
		};
		
		var clickchange = function(divid,rule){
			var value = $("#"+divid).children('input[type=hidden]').attr('value');
			var color = $("#"+divid).children('input[type=hidden]').attr('color');
			var colorArray = color?color.split(" "):Array('','transparent');
			var checkcolor = "transparent";
			if(colorArray.length > 1){
				checkcolor = colorArray[1].replace("-","");
			}
			if(checkcolor == "transparent") checkcolor = "white";
			//if(value=="NULL"){
				//runmain(rule,"null");
			//}
			//else{
				runmain(rule,"1");
				runmain("color"+rule,checkcolor);
			//}
			
			//setdisplay();
			setbackground();
		};
		
		var setdisplay = function(){
			//if(settings.penloop == "null")
				//penloop.css({"display":"none"});
			//else
				//penloop.css({"display":"block"});
			
			//if(settings.penloop2 == "null")
				//penloop2.css({"display":"none"});
			//else
				//penloop2.css({"display":"block"});
			
			//if(settings.rubberband=="null")
				//rubberband.css({"display":"none"});
			//else
				//rubberband.css({"display":"block"});
		};
		var setbackground = function(){
			if(settings.colorpenloop == "transparent") settings.colorpenloop = "white";
			if(settings.colorpenloop2 == "transparent") settings.colorpenloop2 = "white";
			if(settings.colorrubberband == "transparent") settings.colorrubberband = "white";
			penloop.css({"background-color":""+settings.colorpenloop+""});
			penloop2.css({"background-color":""+settings.colorpenloop2+""});
			rubberband.css({"background-color":""+settings.colorrubberband+""});
		};
		var runmain = function(keyrule,valrule){
				
				switch (keyrule) {
				case "penloop":
					settings.penloop = valrule;
					break;
				case "penloop2":
					settings.penloop2 = valrule;
					break;
				case "colorpenloop":
					settings.colorpenloop = valrule;
					break;
				case "colorpenloop2":
					settings.colorpenloop2 = valrule;
					break;
				case "rubberband":
					settings.rubberband = valrule;
					break;
				case "colorrubberband":
					settings.colorrubberband = valrule;
					break;
				}
		};
		init();
		return {
			runmain:runmain,
			clickchange:clickchange,
			setbackground:setbackground
		}
	};
})(jQuery);	

(function($){
$.fn.updateRightPriceStep = function(o){
	var defaults = {tabValue:null,titleTab:null,priceTab:null,info:{}},
	    settings = $.extend({}, defaults, o),
	    wrapper  = this,
	    titleTab = $("<span />"),
	    priceInfo = $("<span class='price-info' />"),
	    priceValueDiv = $("<div class='price-value' />"),
	    priceValue = $("<span class='value' />"),
	    eur        = $("<span>&nbsp;EUR</span>"),
	    info = Array();
	var init = function(){
		updateFirst();
		wrapper.addClass("step-price-value");
		if(settings.titleTab){
			titleTab.html(settings.titleTab);
			wrapper.append(titleTab);
		}
		if(settings.tabValue){
			priceInfo.html(settings.tabValue);
			wrapper.append(priceInfo);
		}
		if(settings.priceTab){
			priceValue.html(settings.priceTab);
			priceValueDiv.append(priceValue);
			priceValueDiv.append(eur);
			wrapper.append(priceValueDiv);
		}
	};
	
	var changePrice = function(inputName,tabName){
		var value = $("#"+inputName+" input[type=hidden]");
		var price = value.attr("price")?value.attr("price"):"0,00";
		var infoValue = value.attr("value")?value.attr("value"):"";
		price = price.replace(".",",");
		switch(tabName){
		case 1:
			updateTab1(infoValue,price);
			break;
		case 2:
			updateTab2(infoValue,price);
			break;
		case 4:
			var infoValue2 = "a No.";
			if(infoValue != "NULL")
			   infoValue2 = value.attr("color");
			updateTab4(infoValue2,price);
			break;
		}
		updateMain();
	};
	
	var changeText = function(infoValue){
		settings.tabValue = infoValue;
		updateMain();
	};

	var changeTextPrice = function(infoValue,price){
		settings.tabValue = infoValue;
		settings.priceTab = price;
		updateMain();
	};
	
	var updateFirst = function(){
		switch(settings.Tab){
		case 1:
			updateTab1(settings.tabValue,settings.priceTab);
			break;
		case 2:
			updateTab2(settings.tabValue,settings.priceTab);
			break;
		case 4:
			updateTab4(settings.tabValue,settings.priceTab);
			break;
		}
	}
	var updateTab1 = function(infoValue,price){
		settings.tabValue = infoValue;
		settings.priceTab = price;
	};
	
	var updateTab2 = function(infoValue,price){
		if(settings.info[infoValue])
			settings.tabValue = settings.info[infoValue];
		else
			settings.tabValue = "&nbsp;";
		
		settings.priceTab = price;
	};
	
	var updateTab4 = function(infoValue,price){
		var info4 = infoValue?infoValue.split(" "):"";
		settings.tabValue = info4[1];
		settings.priceTab = price;
	};
	
	var updateMain = function(){
		priceInfo.html(settings.tabValue);
		priceValue.html(settings.priceTab);
	};
	init();
	return {
		changePrice:changePrice,
		changeText:changeText,
		changeTextPrice:changeTextPrice
	}
	
};
})(jQuery);

(function($){
		$.fn.updateValueDefault = function(o){
			var wrapper 		= $(this),
				defaults 		= {tab:null,value:null,price:null},
				settings 		= $.extend({}, defaults, o),
				divSelect 		= null,
				inputDivSelect	= null;
			var init = function(){
				divSelect = $("#" + settings.tab);
				inputDivSelect = divSelect.children("input[type=hidden]");
			};
			var changeDefault = function(){
				divSelect.children("div").removeClass("selected");
				inputDivSelect.attr("value",settings.value);
				inputDivSelect.attr("price",settings.price);
				wrapper.addClass("selected");
			};
			
			var changePreview = function(){
						changeDefault();
			};
			init();
			return {
				changePreview:changePreview
			}
				
		};
})(jQuery);
(function($){
		$.fn.changeText = function(o){
			var nodes 		= $(this),
			defaults 		= {},
			settings 		= $.extend({}, defaults, o);
			var defaulttext = nodes.attr('data-default');
			if(nodes.val() == ''){
   		   	 	 nodes.val(defaulttext);
   		   	 	 nodes.keyup();
   		   	 }
   		   	 nodes.focus(function() {
						if (nodes.val() === defaulttext) {
							nodes.val('');
							nodes.keyup();
						}
					  });
			 nodes.focusout(function() {
						if(nodes.val()==''){
							nodes.val(defaulttext);
							nodes.keyup();
						    }
					  });
			 nodes.click(function(){
			 		 if(nodes.val() == defaulttext){
			 		 	nodes.val(''); 
			 			nodes.keyup();
			 		 }
			 });
		};
})(jQuery);

(function($){
		$.fn.changeTextArea = function(o){
			var nodes 		= $(this),
			defaults 		= {},
			settings 		= $.extend({}, defaults, o);
			var defaulttext = nodes.attr('data-default');
			if(nodes.html() == ''){
   		   	 	 nodes.hmtl(defaulttext);
   		   	 	 nodes.keyup();
   		   	 }
   		   	 nodes.focus(function() {
						if (nodes.html() === defaulttext) {
							nodes.html('');
							nodes.keyup();
						}
					  });
			 nodes.focusout(function() {
						if(nodes.html()==''){
							nodes.html(defaulttext);
							nodes.keyup();
						    }
					  });
			 nodes.click(function(){
			 		 if(nodes.html() == defaulttext){
			 		 	nodes.html(''); 
			 			nodes.keyup();
			 		 }
			 });
		};
})(jQuery);

