var jq = jQuery.noConflict();
//function closePreview(){
//	jq('.popupInfo').html('').addClass('hidden');
//}
jq(document).ready(function($){
	//scroll top
   $(window).scrollTop($("#logo").offset().top);
   //fix z-index IE 7
   if(Browser.Engine.trident){
	var zIndexNumber = 1000;
	$$('div').each(function(el,i){
		el.setStyle('z-index',zIndexNumber);
		zIndexNumber -= 10;
	});
   };
	
   //-------step3
   
   $('.step3').each(function(){
   	if($(this).length > 0){
   		var widdmung = $('#preview-widdmung');
   		var var_textArea =  $("textarea[name=var_text]");
   		    var_textArea.changeTextArea();
   	        var_textArea.bind("keyup",function(){
			   var text = $(this).val().replace(/\n/g,"<br>");
			   var line = $(this).val().split("\n").length;
			   if(text.length < 550 && line < 16){
				   $('#preview-widdmung').changewiddmung("widdmung",text);
				   $('.error').hide();
			   }
			   else{
				   if(text.length > 550 ) $('#texttoolong').show();
				   if(line > 16 ) $('#texterrorline').show();
			   }
			   var height_f = $('#preview-widdmung').height();
			   var height_b = $('.border-preview-widdmung').height();
			   widdmung.css('padding-top',(height_b-height_f)/2-20);
	   });
	   $( ".option-radio input" ).on( "click", function() {
		  widdmung.changewiddmung("font",$("input:checked" ).val());
	   });
	   
	   
	   $('.step3 #preview-widdmung').each(function(){
			   if($(this).length > 0){
				   var el = widdmung;
				   var text = $('.var_text').val().replace(/\n/g,"<br>");
				   var font = $("input:checked" ).val();
				   el.changewiddmung("font",font);
				   el.changewiddmung("widdmung",text);
				   var height_f = el.height();
				   var height_b = $('.border-preview-widdmung').height();
				   el.css('padding-top',(height_b-height_f)/2);
			   }
	   });
	 }
   });
   
   //-------------------Step4
   
   $('.step4').each(function(){
   		   if($(this).length > 0){
   		   	   $('.startmonat').startmonat($('.startmonat').attr('data-default'));
   		   }
   });
   
   //-------------------Step5
   
   $('.step5').each(function(){
   		   if($(this).length > 0){
   		   	   
   		   }
   });
   
   $(".btn_info").hover(function(){
   		   $(this).children("div").show();
   },function(){
   	           $(this).children("div").hide();
   });
});
//---------

   function updateStep1Price()
   {
    	var new_price = 0;
    	var pNotebooks = parseFloat(jq('input[name=Notebooks]').attr('price'));
    	var pEinband = parseFloat(jq('input[name=Einband]').attr('price'));
    	new_price = pNotebooks + pEinband ;
    	updatePrice(new_price);
   }
   function updateStep2Price()
   {
   	var pLayout = parseFloat(jq('input[name=Layout]').attr('price'));
    	var new_price = pLayout;
    	updatePrice(new_price);
   }
   function updateStep3Price()
   {
    	var new_price = 0;
    	var color = parseFloat(jq('input[name=color-option]').attr('price'));
    	new_price = color;
    	updatePrice(new_price);
   }
   function updateStep4Price()
   {
    	var new_price = 0;
    	var pModule = parseFloat(jq('input[name=weitere-inhalte]').attr('price'));
    	var Interessen = jq('#main-interessen input:radio[name=interesenv_value]:checked');
    	var pInteressen = 0;
    	if(Interessen.length > 0) {
    		pInteressen = parseFloat(Interessen.attr('price'));
    	}	
    	new_price = pModule + pInteressen;
    	updatePrice(new_price);
   }
   function updateStep5Price()
   {
    	var new_price = 0;
    	var pUmschlag = parseFloat(jq('input[name=Umschlag]').attr('price'));
    	new_price = pUmschlag;
    	updatePrice(new_price);
   }
   function updateStep6Price()
   {
    	var new_price = 0;
    	var price_1 = parseFloat(jq('input[name=Lesezeichen]').attr('price'));
    	var price_2 = 0;//parseFloat(jq('input[name=Stiftschlaufe]').attr('price'));
    	var price_3 = parseFloat(jq('input[name=Gummiband]').attr('price'));
    	var price_4 = parseFloat(jq('input[name=Froschtasche]').attr('price'));
    	new_price = price_1 + price_2 + price_3 + price_4;
    	updatePrice(new_price);
   }
   
   function updatePrice(new_price)
    {
    	var currentprice = jq(".formheader h1.book-price").attr("price");
    	// value price old select
    	var oldPrice = jq(".formheader h1.book-price").attr("old_price");
    	// value now select
    	var newPrice = parseFloat(currentprice) + parseFloat(new_price) - parseFloat(oldPrice);
    	// total value price order
    	var cartsum = jq("#shoppingcartlogo .cartsum").attr("cartsum");
    	cartsum = parseFloat(cartsum) + parseFloat(new_price) - parseFloat(oldPrice);
    	
    	jq(".formheader h1.book-price").html(formatPrice(roundNumber(newPrice, 2), "de") + " EUR");
    	jq("#shoppingcartlogo .cartsum").html(formatPrice(roundNumber(cartsum, 2), "de"));
    }
    
   function formatPrice(price, locale) {
        price = price + "";
        if(locale == "de")
        {
            price = price.replace(".", ",");
        }
        
        return price;
    }
    
   function roundNumber(num, dec) {
		var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
		if (result.toFixed)
			result = result.toFixed(dec);
		return result;
   }
   
