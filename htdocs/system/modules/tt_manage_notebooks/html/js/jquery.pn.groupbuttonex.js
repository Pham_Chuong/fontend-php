(function($) {
$.widget('pn.groupbuttonex', {
	
	hiddenvalue : null,
	options: {
		name: "groupbuttonex",
		value: null,
		block: null,
		checkbox: null,
		price: "0.00"
  	},
  	
	_create: function(){
		var hidden = $("input[name="+ this.options.name +"]");
		if(hidden.length > 0){
			this.hiddenvalue = hidden;
		}
		else
		{
			this.hiddenvalue = $("<input type=hidden />");
			var el = this.element;
			el.append(this.hiddenvalue);
			
			this._setOptions({
				"value": this.options.value,
				"name": this.options.name,
				"block": this.options.block,
				"checkbox": this.options.checkbox,
				"price": this.options.price
				
			});
		}
	},
	
	_destroy: function(){
		this.element.empty();
	},
	
	_setOption: function(option, value){
		var el = this.element;
		
		switch (option) {
			case "value":
				this.hiddenvalue.attr({"value": value});
				break;
				
			case "name":
				this.hiddenvalue.attr({"name": value});
				break;
			case "block":
				this.options.block = value;
				break;
			case "checkbox":
				this.options.checkbox = value;
				break;
			case "price":
				this.hiddenvalue.attr({"price": value});
				break;
		}
	},
	
	addControl: function(obj) {
		this.element.append(obj);
	},
	
	addControls: function(objs) {
		var el = this.element;
		var self = this;
		$(objs).each(function() {
			var _value = $(this).data("buttonex").getValue();
			var _price = $(this).data("buttonex").getPrice();
			var _color = $(this).data("buttonex").getColor();
			if(!self._getValue()) 
			{
				self._setValue(_value);
				self._setPrice(_price);
				if(self.options.block==null){
					$(this).addClass("selected");
					if(self.options.checkbox==true){
						self._setColor(_color);
					}
				}
				self._trigger("changed", null, self._getValue());
			}
			else 
			if(_value == self._getValue()) {
				if(self.options.block==null){
					$(this).addClass("selected");
					if(self.options.checkbox==true){
						self._setColor(_color);
					}
					self._setPrice(_price);
				}
			}
			
			this.click(function(){ 
				if(self.options.block==null){
					if(self.options.checkbox==true){
						$(this).parent().children().removeClass("selected");
							$(this).addClass("selected");
							self._setValue(_value);
							self._setPrice(_price);
							self._setColor(_color);
						self._trigger("changed", null, self._getValue());
					}else{
						self._setValue(_value);
						self._setPrice(_price);
						$(this).parent().children().removeClass("selected");
						$(this).addClass("selected");
						self._trigger("changed", null, self._getValue());
					}
					
					
				}
			});
			
			el.append(this);
		});
		
	},
	changeDefault: function() {
	    var self = this;
	    self._trigger("changed", null, self._getValue());
	},
	changeValue: function(value) {
	    var self = this;
	    self._setValue(value);
	    self._trigger("changed", null, self._getValue());
	},
	loadButtons: function(json) {
	    var el = this.element;
	    var self = this;
	    $(json).each(function() {
            var button = $("<div/>").buttonex(this);
            self.addControls([button]);
	    });
	},
	
	_setValue: function(_value) {
		this.hiddenvalue.attr({"value": _value});
	},
	_setPrice: function(_value) {
		this.hiddenvalue.attr({"price": _value});
	},
	_setColor: function(_value) {
		this.hiddenvalue.attr({"color": _value});
	},
	
	_getValue: function() { return this.hiddenvalue.attr("value") },
	getValue: function() { return this._getValue }
	
});
})(jQuery);