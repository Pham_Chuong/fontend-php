;(function($) {

    "use strict";
    
    $.fn.PnPreview = function(o) {
    	
    	var defaults        = {
    				rule : {},
    				binding: null,
    				style: {},
    				background: null,
    				header:null,
    				footer:null,
    				infoPosition:null,
    				covertitle:null,
    				coveryear:null,
    				covernote:null,
    				nameBook:null,
    				shownameBook:null,
    				showzusa:null,
    				penloop:"null",
    				colorpenloop:'transparent',
    				rubberband:"null",
    				colorrubberband:"transparent",
    				penloop2:"null",
    				colorpenloop2:'transparent',
    				maskbackground:null,
    				imgUpload:null,
    				imgUploadBg:null,
    				font:null,
    				groupFont:null,
    				groupEvent:null,
    				event:null,
    				eventShow:null
    				};
    	var settings        = $.extend({}, defaults, o);
    	var wrapper         = this,
    		slider			= wrapper.find("ul"),
    		slides			= slider.children('li'),
    		inside			= $("<div />"),
    		img				= $("<img />"),
    		binding         = $("<img />"),
    		imgload			= $("<img src='/system/modules/tt_manage_books/html/img/twirly_circleball_white_big.gif' id='twirly' style='visibility: hidden; ' />"),
    		binding_src     = ["/system/modules/tt_manage_notebooks/html/img/binding-preview-0.png",
    		                   "/system/modules/tt_manage_notebooks/html/img/binding-preview-1.png"],
    		binding_bg = $("<div />"),
    	    divheaderRight = $('<div />'),
	        divheaderLeft = $('<div />'),
	        divfooterRight = $('<div />'),
	        divfooterLeft = $('<div />'),
	        widdmung = $("#preview-Beschriftung"),
	        titlecover = $('<div />'),
	        yearcover = $('<div />'),
	        notecover = $('<div />'),
	        penloop = $("<div id='penloop' />"),
		    penloop2 = $("<div id='penloop2' />"),
		    rubberband = $("<div id='rubberband' />"),
		    imgload			= $("<img src='/system/modules/tt_manage_books/html/img/twirly_circleball_white_big.gif' id='twirly' style='visibility: hidden; ' />"),
		    divimg	= $("<div class='bottle-etiquette' />"),
		    maskbg = $("<img />"),
		    imgUpload = $("<img />"),
		    eventLeft = $("<div />"),
		    eventRight = $("<div />");
	
        penloop.css({
        	"z-index":"5"	
        });
        penloop2.css({
        	"z-index":"5"	
        });
        rubberband.css({
        	"z-index":"5"	
        });
	divheaderRight.css({
			"float": "left",
			"position": "absolute",
			"left": "341px",
			"top": "18px",
			"text-align": "center",
			"line-height": "20px",
			"height": "20px",
			"font-size": "10px",
			"font-family": "Time new roman",
			"width": "220px",
			"z-index":"3"
	});
	
	eventRight.css({
			"float": "left",
			"position": "absolute",
			"left": "324px",
			"top": "45px",
			"text-align": "right",
			"line-height": "8px",
			"height": "50px",
			"font-size": "7px",
			"font-family": "Time new roman",
			"width": "155px",
			"padding-right": "5px",
			"border-right": "1px solid #CCC",
			"z-index":"3"
	});
	eventLeft.css({
			"float": "left",
            "position": "absolute",
            "left": "104px",
            "top": "45px",
            "text-align": "left",
            "line-height": "8px",
            "height": "50px",
            "font-size": "7px",
            "font-family": 'Times New Roman',
            "width": "155px",
            "padding-left": "5px",
            "border-left": "1px solid #CCC",
            "z-index": 3
	});
	
	divheaderLeft.css({
			"float": "left",
			"position": "absolute",
			"left": "58px",
			"top": "18px",
			"text-align": "center",
			"line-height": "20px",
			"height": "20px",
			"font-size": "10px",
			"font-family": "Time new roman",
			"width": "220px",
			"z-index":"3"
	});
	
	divfooterLeft.css({
			"float": "left",
			"position": "absolute",
			"left": "58px",
			"top": "395px",
			"text-align": "center",
			"line-height": "20px",
			"height": "20px",
			"font-size": "8px",
			"font-family": "Time new roman",
			"width": "220px",
			"z-index":"3"
	});
	
	divfooterRight.css({
			"float": "left",
			"position": "absolute",
			"left": "341px",
			"top": "395px",
			"text-align": "center",
			"line-height": "20px",
			"height": "20px",
			"font-size": "8px",
			"font-family": "Time new roman",
			"width": "220px",
			"z-index":"3"
	});
	titlecover.css({
			"float": "left",
			"position": "absolute",
			"z-index":"3"
	});
	yearcover.css({
			"float": "left",
			"position": "absolute",
			"z-index":"3"
	});
	notecover.css({
			"float": "left",
			"position": "absolute",
			"z-index":"3"
	});
	imgUpload.css({
		"float": "left",
		"position": "absolute",
		"height" : "225px",
		"width" : "269px",
		"left"  : "166px",
		"top"  : "19px",
		"z-index":"2"
	});
	
	
    	var init = function() {
    		if(settings.imgUpload != null && settings.imgUpload != ''){
    			imgUpload.attr("src",settings.imgUpload);
    			inside.append(imgUpload);
    		}
    		inside.append(img);
    		if(settings.imgUploadBg){
    			reload_bg("true");
    		}
    		if(settings.maskbackground != null){
    			maskbg.attr("src",settings.maskbackground);
    			maskbg.css({
    				"float": "left",
					"position": "absolute",
					"left":"0px",
					"top":"0px",
					"z-index":"3"
				});
				maskbg.css(settings.style);
    			inside.append(maskbg);
    		}
    		inside.addClass("preview_inside");
    		inside.addClass('bottle-etiquette');
    		wrapper.append(imgload);
    		wrapper.append(inside);
    		wrapper.css(settings.style);
    		img.height(wrapper.height());
    		img.width(wrapper.width());
    		img.css({
    				"position": "absolute",
    				"float": "left",
    				"top" : "0px",
    				"left": "0px",
    				"z-index":"2"
    		});
    		imgload.css({
    				"float": "left",
					"position": "absolute",
					"left": (wrapper.width()-imgload.width())/2 + "px",
					"top": (wrapper.height()-imgload.height())/2 + "px",
					"z-index":"3"
    		});
    		img.addClass('bottle-etiquette');
    		
    		if(settings.background) wrapper.css("background", settings.background);
    		
    		if(settings.header!=null){
    			addHeader();
    		}
    		if(settings.footer!=null){
    			addFooter();
    		}
    		
    		if(settings.covertitle!=null){
    			titlecover.html(settings.covertitle);
    			wrapper.append(titlecover);
    		}
    		
    		if(settings.coveryear!=null){
    			var name = settings.nameBook?settings.nameBook + " ":"";
    			yearcover.html(name + settings.coveryear);
    			wrapper.append(yearcover);
    		}
    		
    		if(settings.covernote!=null){
    			notecover.html(settings.covernote);
    			wrapper.append(notecover);
    		}
    		
    		if(settings.showzusa!=null){
    			divimg.append(penloop2);
    			//after burm when BoD accept.
				//divimg.append(penloop);
				divimg.append(rubberband);	
				wrapper.append(divimg);
    		}
    		
    		if(settings.groupFont != null && settings.font != null){
    			changeFront();
    		}
    		
    		if(settings.groupEvent){
    		    eventLeft.css({"font-family": settings.groupFont[settings.font]});
    		    eventRight.css({"font-family": settings.groupFont[settings.font]});
    		    if(settings.event && settings.event != "NULL"){
    		         eventLeft.css({"border-left": "1px solid #CCC"});
    		         eventRight.css({"border-right": "1px solid #CCC"});
    		    }
    		    else
    		    {
    		         eventLeft.css({"border-left": "0"});
    		         eventRight.css({"border-right": "0"});
    		    }
    		    wrapper.append(eventLeft);
    		    wrapper.append(eventRight);
    		}
    		
    		if(settings.event){
    		    updateHmlEvent();
    		}
    		
    		slider.hide();
    		wrapper.hide();
    		
    		dochange();

    	};
    	var reload_bg = function(fla){
    		if(fla=="true")
    			wrapper.css({"background-image":"url('"+settings.imgUploadBg+"')"});
    		else
    			wrapper.css({"background-image":"none"});
    	};
    	var changeFront = function(){
			divheaderRight.css({"font-family": settings.groupFont[settings.font]});
			divheaderLeft.css({"font-family": settings.groupFont[settings.font]});
			divfooterRight.css({"font-family": settings.groupFont[settings.font]});
			divfooterLeft.css({"font-family": settings.groupFont[settings.font]});
    	}
    	
    	var setFontchange = function(value){
    		settings.font = value;
    		changeFront();
    	}
    	
    	var HideimgUpload = function(){
    		imgUpload.hide();
    	}
    	
    	var ShowimgUpload = function(){
    		imgUpload.show();
    	}
    	
    	var updateHmlEvent = function(){
    	    if(settings.eventShow == 3)
    	          eventRight.hide();
    	      else
    	          eventRight.show();
    	    if(settings.event != "NULL" && settings.event){
    	        
    	        eventLeft.css({"border-left": "1px solid #CCC"});
    		    eventRight.css({"border-right": "1px solid #CCC"});
    	        
    	        if(settings.groupEvent[settings.event][0])
    	            eventLeft.html(settings.groupEvent[settings.event][0]);
    	        else
    	            eventLeft.html("");
    	        
    	        if(settings.groupEvent[settings.event][1])
    	            eventRight.html(settings.groupEvent[settings.event][1]);
    	        else
    	            eventRight.html("");
    	       
    	    }
    	    else
    	    {
    	        
    	        eventLeft.css({"border-left": "0"});
    		    eventRight.css({"border-right": "0"});
    	        eventLeft.html("");
    	        eventRight.html("");
    	    }
    	    
    	}
    	
    	var changEvent = function(eventValue){
    	    settings.event = eventValue;
    	    updateHmlEvent();
    	}
    	
    	var changeImgBg = function(src){
    		img.css({"background-image":"url('"+src+"')"});
    	}
    	
    	var addHeader = function(){
    		if(settings.infoPosition == 'right' || settings.infoPosition == 'both'){
			divheaderRight.addClass("preview_header_right");
			divheaderRight.html(settings.header);
			wrapper.append(divheaderRight);
		}
    		if(settings.infoPosition == 'left' || settings.infoPosition == 'both'){
			divheaderLeft.addClass("preview_header_left");
			divheaderLeft.html(settings.header);
			wrapper.append(divheaderLeft);
    		}
    	};
    	
    	var dochangeHeader = function(){
    		if(settings.infoPosition == 'right' || settings.infoPosition == 'both'){
    			divheaderRight.html(settings.header);
    		}
    		if(settings.infoPosition == 'left' || settings.infoPosition == 'both'){
    			divheaderLeft.html(settings.header);
    		}
    	};
    	
    	var addFooter = function(){
    		if(settings.infoPosition == 'right' || settings.infoPosition == 'both'){
			divfooterRight.addClass("preview_footer_right");
			divfooterRight.html(settings.footer);
			wrapper.append(divfooterRight);
		}
		
    		if(settings.infoPosition == 'left' || settings.infoPosition == 'both'){
			divfooterLeft.addClass("preview_footer_left");
			divfooterLeft.html(settings.footer);
			wrapper.append(divfooterLeft);
    		}
    	};
    	var dochangeFooter = function(){
    		if(settings.infoPosition == 'right' || settings.infoPosition == 'both'){
    			divfooterRight.html(settings.footer);
    		}
    		if(settings.infoPosition == 'left' || settings.infoPosition == 'both'){
    			divfooterLeft.html(settings.header);
    		}
    	};
    	var changeHeader = function(valrule){
    		settings.header = valrule;
    		dochangeHeader();
    	};
    	
    	var changeFooter = function(valrule){
    		settings.footer = valrule;
    		dochangeFooter();
    	};
    	
    	var changeimage = function(keyrule, valrule){

    		for (var key in settings.rule)
    		{
    			var attrName = key;
    			if(attrName == keyrule) {
    				settings.rule[attrName] = valrule;
    			}
    		}
    		
    		dochange();
    	};
    	
    	var changebackground = function(color) {
    	    settings.background = color;
    	    wrapper.css("background", color);
    	}
    	var hideAll = function(){
    		if(widdmung.length > 0){
    			widdmung.hide();
    		}
    		if(settings.header){
    			divheaderLeft.hide();
    			divheaderRight.hide();
    		}
    		if(settings.footer){
    			divfooterLeft.hide();
    			divfooterRight.hide();
    		}
    		reload_bg("false");
    		divimg.hide();
    		yearcover.hide();
			titlecover.hide();
			notecover.hide();
    		inside.hide();
    	};
    	var showAll = function(){
    		if(widdmung.length > 0){
					widdmung.show();
			  }
			 
			  yearcover.show();
			  titlecover.show();
			  notecover.show();
			  if(settings.header){
					divheaderLeft.show();
					divheaderRight.show();
			  }
			  if(settings.footer){
					divfooterLeft.show();
					divfooterRight.show();
			  }
			  reload_bg("true");
			  divimg.show();
			  imgload.css("visibility", "hidden");
			  inside.show();
    	};
    	var dochange = function() {
    		var classname = "";
    		var binding_value = settings.binding?settings.binding:'';
    		if(binding_value == 2) binding_value = 1;
    		for (var key in settings.rule)
    		{
    			var attrName = key;
            	var attrValue = settings.rule[key];
            	
            	classname += attrName + "-" + attrValue + "_";
    		}
    		classname +=binding_value;
    		//console.log(classname);
    		
    		var imageholder = $("." + classname);
    		
    		titlecover.css("color","");
    		yearcover.css("color","")
    		notecover.css("color","");
    		
    		if(imageholder.attr("colorTitle")){
    		    var color = imageholder.attr("colorTitle");
    		    titlecover.css("color",color);
    		}
    		if(imageholder.attr("colorYear")){
    		    var color = imageholder.attr("colorYear");
    		    yearcover.css("color",color);
    		}
    		if(imageholder.attr("colorNote")){
    		    var color = imageholder.attr("colorNote");
    		    notecover.css("color",color);
    		}
    		var image_url = imageholder.text();
    		//console.log(image_url);
    		hideAll();
    		
    		imgload.css("visibility", "visible");
    		$.preload([image_url], {
			  onComplete: function (data) {
				  if (data.found) {
					  img.attr("src", image_url);
				  } else {
				  	  //
				  }
			  },
			  onFinish: function(){
			  	  showAll();
			  }
			});
    		
    		
    	};
    	
    	var dohide = function() { wrapper.hide() };
    	
    	var addClassCover = function(classValue){
    		titlecover.attr('class','');
    		yearcover.attr('class','');
    		notecover.attr('class','');
    		
    		titlecover.addClass('title-'+classValue);
    		yearcover.addClass('year-'+classValue);
    		notecover.addClass('note-'+classValue);
    	};
    	
    	var doshow = function() { wrapper.show() };
    	
    	var dochangebinding = function(b) {
    		if(b == 2) b = 1;
    	    settings.binding = b;
    	    
            dochange();
    	};
    	
    	var changeCoverTitle = function(valrule){
    		settings.covertitle = valrule;
    		titlecover.html(settings.covertitle);
    	};
    	
    	var changeCoverYear = function(valrule){
    		var name = settings.shownameBook!=null?settings.nameBook + " ":"";
    		settings.coveryear = valrule;
    		yearcover.html(name + settings.coveryear);
    	};
    	
    	var changeCoverNote = function(valrule){
    		settings.covernote = valrule;
    		notecover.html(settings.covernote);
    	};
    	
    	var shownameBook = function(valrule){
    		if(valrule == "NULL")
    			settings.shownameBook = null;
    		else
    			settings.shownameBook = valrule;
    		
    		var name = settings.shownameBook!=null?settings.nameBook + " ":"";
    		yearcover.html(name + settings.coveryear);
    	};
    	
    	var clickchange = function(divid,rule){
			var value = $("#"+divid).children('input[type=hidden]').attr('value');
			var color = $("#"+divid).children('input[type=hidden]').attr('color');
			var colorArray = color?color.split(" "):Array('','transparent');
			var checkcolor = "transparent";
			if(colorArray.length > 1){
				checkcolor = colorArray[1].replace("-","");
			}
			//if(checkcolor == "transparent") checkcolor = "white";
			//if(value=="NULL"){
				//runmain(rule,"null");
			//}
			//else{
				runmain(rule,"1");
				runmain("color"+rule,checkcolor);
			//}
			
			//setdisplay();
			setbackground();
		};
		
		var setdisplay = function(){
			//if(settings.penloop == "null")
				//penloop.css({"display":"none"});
			//else
				//penloop.css({"display":"block"});
			
			//if(settings.penloop2 == "null")
				//penloop2.css({"display":"none"});
			//else
				//penloop2.css({"display":"block"});
			
			//if(settings.rubberband=="null")
				//rubberband.css({"display":"none"});
			//else
				//rubberband.css({"display":"block"});
		};
		
		var setbackground = function(){
			if(settings.colorpenloop == "transparent") 
				penloop.hide();
			else
				penloop.show();
			if(settings.colorpenloop2 == "transparent") 
				penloop2.hide();
			else
				penloop2.show();
			if(settings.colorrubberband == "transparent")
				rubberband.hide();
			else
				rubberband.show();
			
			penloop.css({"background-color":""+settings.colorpenloop+""});
			penloop2.css({"background-color":""+settings.colorpenloop2+""});
			rubberband.css({"background-color":""+settings.colorrubberband+""});
		};
		
		var runmain = function(keyrule,valrule){
				
				switch (keyrule) {
				case "penloop":
					settings.penloop = valrule;
					break;
				case "penloop2":
					settings.penloop2 = valrule;
					break;
				case "colorpenloop":
					settings.colorpenloop = valrule;
					break;
				case "colorpenloop2":
					settings.colorpenloop2 = valrule;
					break;
				case "rubberband":
					settings.rubberband = valrule;
					break;
				case "colorrubberband":
					settings.colorrubberband = valrule;
					break;
				}
		};
    	
    	init();
    	
    	return {
    		changeimage: changeimage,
    		changebackground: changebackground,
    		hide: dohide,
    		show: doshow,
    		changebinding: dochangebinding,
    		changeHeader:changeHeader,
    		changeFooter:changeFooter,
    		changeCoverTitle:changeCoverTitle,
    		changeCoverYear:changeCoverYear,
    		changeCoverNote:changeCoverNote,
    		addClassCover:addClassCover,
    		shownameBook:shownameBook,
    		runmain:runmain,
			clickchange:clickchange,
			setbackground:setbackground,
			setFontchange:setFontchange,
			changEvent:changEvent,
			changeImgBg:changeImgBg,
			HideimgUpload:HideimgUpload,
			ShowimgUpload:ShowimgUpload
    	}
    }
    
})(jQuery);
