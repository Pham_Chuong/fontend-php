<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnCalendar extends Controller
{
    function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public static function getCalendarDesigns($bookitemId)
    {
        $bookItem = new PnBookItem();
        $bookItem->initWithId($bookitemId);
        
        $book = new PnBook();
        $book->initWithId($bookItem->book_id);
        
        $designs = $book->getBookDesigns($bookItem->article_id,1);
        
        $i = 1;
        $arr = array();
        foreach($designs as $design)
        {
            $arr[$i] = $design;
            $i++;
        }
        return $arr;
    }
	public static function getColorVersion($version){
		$versions = new PnVersion();
        $version->initWithId($version->id);
        //print_r($version->id);
        print_r($version->getDecoColors());
        
	}
	public static function getSelectedCalendarDesignId($articleId, $bookitemId,$decocover_id)
    {
        $layouts  = PnCalendar::getCalendarDesignsDecocover($bookitemId,$decocover_id);
        foreach($layouts as $k => $v)
        {
            if($v->artikel_id == $articleId)
            {
                return $k;
            }
        }
        
        return 1;
    }
	
	public static function getSelectedCalendarMaterial($versionId)
    {
        $versions  = PnCalendar::getCalendarVersions();
        foreach($versions as $k => $v)
        {
            foreach($v as $k1 => $v1)
            {
                foreach($v1 as $k2 => $v2)
                {
                    if ($v2 == $versionId)
                    {
                        return $k1;
                    }
                }
            }
        }
        
        return "Druck";
    }
	
	public static function getSelectedCalendarSize($versionId)
    {
        $versions  = PnCalendar::getCalendarVersions();
        foreach($versions as $k => $v)
        {
            foreach($v as $k1 => $v1)
            {
                foreach($v1 as $k2 => $v2)
                {
                    if ($v2 == $versionId)
                    {
                        return $k;
                    }
                }
            }
        }
        
        return "130x190";
    }
    
    public static function getCalendarDesignsDecocover($bookitemId,$decocover_id)
    {
        $bookItem = new PnBookItem();
        $bookItem->initWithId($bookitemId);
        
        $designArrayQuery  = PnCalendar::getDesign($decocover_id);
        
        $book = new PnBook();
        $book->initWithId($bookItem->book_id);
        
        $designs = $book->getBookDesignsDecocover($bookItem->article_id,$designArrayQuery,1);
        
        $i = 1;
        $arr = array();
        foreach($designs as $design)
        {
            $arr[$i] = $design;
            $i++;
        }
        return $arr;
    }
    
    public static function getDesign($decocover_id)
    {
    	$arrayTypeDesign = array(
    			1 => 1,
    			2 => 1,
    			3 => 1,
    			4 => 2
    		);
    	$decocovers= PnCalendar::getCalendarColors();
    	foreach($decocovers as $k => $v){
    		foreach($v as $k2 => $v2){
    			if($decocover_id == $v2['id']){
    				return $arrayTypeDesign[$k];
    			}
    		}
    	}
    	return 3;
    }
	
	public static function getCalendarVersions()
	{
	    return array(
                "130x190" => array(
                        "Druck"  => array(2001, 2002,2033),
                        "Leinen" => array(2009, 2010,2037),
                        "Nabuka" => array(2017, 2018,2041),
                        "Seide"  => array(2025, 2026,2045)
                    ),
                "148x210" => array(
                        "Druck"  => array(2003, 2004,2034),
                        "Leinen" => array(2011, 2012,2038),
                        "Nabuka" => array(2019, 2020,2042),
                        "Seide"  => array(2027, 2028,2046)
                     ),
                "167x244" => array(
                        "Druck"  => array(2005, 2006,2035),
                        "Leinen" => array(2013, 2014,2039),
                        "Nabuka" => array(2021, 2022,2043),
                        "Seide"  => array(2029, 2030,2047)
                    ),
                "185x262" => array(
                        "Druck"  => array(2007, 2008,2036),
                        "Leinen" => array(2015, 2016,2040),
                        "Nabuka" => array(2023, 2024,2044),
                        "Seide"  => array(2031, 2032,2048)
                    )
            );
	}
	
	public static function getPriceVersionsStep5()
	{
		return array(
						"Druck"  => "0.00",
                        "Leinen" => "3.00",
                        "Nabuka" => "4.00",
                        "Seide"  => "5.00"
			);
	}
	
	public static function getCalendarBindings()
	{
	    return array(
                    "spiral" => "0",
                    "Hardcover" => "1",
                    "Leinen" => "2"
                    );
	}
	
	public static function getCalendarDoublePages()
	{
	    $arr = array(
                "130x190" => array(
                    "individual" => 401, 
                    "month"      => 405, 
                    "legend"     => 409, 
                    "day"        => array(
                            "4"    => array(
                                    "blanko"    => 413,
                                    "liniert"   => 414,
                                    "punktiert" => 415
                                ),
                            "2" => array(
                                    "blanko"    => 416,
                                    "liniert"   => 417,
                                    "punktiert" => 418
                                ),
                            "3"   => array(
                                    "blanko"    => 419,
                                    "liniert"   => 420,
                                    "punktiert" => 421
                                ),
                            "1"  => array(
                                    "blanko"    => 422,
                                    "liniert"   => 423,
                                    "punktiert" => 424
                                )
                        ), 
                    "note"      => array(
                            "blanko"    => 461,
                            "liniert"   => 462,
                            "punktiert" => 463,
                            "kariert"   => 464
                        )
                    ),
                
                "148x210" => array(
                    "individual" => 402, 
                    "month"      => 406, 
                    "legend"     => 410, 
                    "day"        => array(
                            "4"    => array(
                                    "blanko"    => 425,
                                    "liniert"   => 426,
                                    "punktiert" => 427
                                ),
                            "2" => array(
                                    "blanko"    => 428,
                                    "liniert"   => 429,
                                    "punktiert" => 430
                                ),
                            "3"   => array(
                                    "blanko"    => 431,
                                    "liniert"   => 432,
                                    "punktiert" => 433
                                ),
                            "1"  => array(
                                    "blanko"    => 434,
                                    "liniert"   => 435,
                                    "punktiert" => 436
                                )
                        ), 
                    "note"      => array(
                            "blanko"    => 465,
                            "liniert"   => 466,
                            "punktiert" => 467,
                            "kariert"   => 468
                        )
                    ),
                
                "167x244" => array(
                    "individual" => 403, 
                    "month"      => 407, 
                    "legend"     => 411, 
                    "day"        => array(
                            "4"    => array(
                                    "blanko"    => 437,
                                    "liniert"   => 438,
                                    "punktiert" => 439
                                ),
                            "2" => array(
                                    "blanko"    => 440,
                                    "liniert"   => 441,
                                    "punktiert" => 442
                                ),
                            "3"   => array(
                                    "blanko"    => 443,
                                    "liniert"   => 444,
                                    "punktiert" => 445
                                ),
                            "1"  => array(
                                    "blanko"    => 446,
                                    "liniert"   => 447,
                                    "punktiert" => 448
                                )
                        ), 
                    "note"      => array(
                            "blanko"    => 469,
                            "liniert"   => 470,
                            "punktiert" => 471,
                            "kariert"   => 472
                        )
                    ),
                
                "185x262" => array(
                    "individual" => 404, 
                    "month"      => 408, 
                    "legend"     => 412, 
                    "day"        => array(
                            "4"    => array(
                                    "blanko"    => 449,
                                    "liniert"   => 450,
                                    "punktiert" => 451
                                ),
                            "2" => array(
                                    "blanko"    => 452,
                                    "liniert"   => 453,
                                    "punktiert" => 454
                                ),
                            "3"   => array(
                                    "blanko"    => 455,
                                    "liniert"   => 456,
                                    "punktiert" => 457
                                ),
                            "1"  => array(
                                    "blanko"    => 458,
                                    "liniert"   => 459,
                                    "punktiert" => 460
                                )
                        ), 
                    "note"      => array(
                            "blanko"    => 473,
                            "liniert"   => 474,
                            "punktiert" => 475,
                            "kariert"   => 476
                        )
                    )
            );
        
        return $arr;
	}
	public static function getCalendarColorsV1(){
		$colorVersion = array();
			 $colorVersion['duck'] = array(
			 	 	array(
						"title" => "Red",
						"classDiv" => "border_select",
						"label" => "",
						"value" => 13,
						"description" => "",
						"background" => "#b9222f"
			 	 		)
			 	 	
			 	 );
			 $colorVersion['leinen'] = array(
			 	 	array(
						"title" => "deeppink",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-leinen_55.png' >",
						"value" => 14,
						"description" => "",
						"preview" => "TOILE-DU-MARAIS-TDM035155.png"
						//"background" => "#FD1090"
			 	 		),
			 	 	array(
						"title" => "red",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-leinen_03.png' >",
						"value" => 33,
						"description" => "",
						"preview" => "TOILE-DU-MARAIS-TDM035103.png"
						//"background" => "#FC0202"
			 	 		),
			 	 	array(
						"title" => "darkred",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-leinen_29.png' >",
						"value" => 35,
						"description" => "",
						"preview" => "TOILE-DU-MARAIS-TDM035829.png"
						//"background" => "#AB212E"
			 	 		),
			 	 	array(
						"title" => "green",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-leinen_33.png' >",
						"value" => 19,
						"description" => "",
						"preview" => "TOILE-DU-MARAIS-TDM035833.png"
						//"background" => "#018101"
			 	 		),
			 	 	array(
						"title" => "blue",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-leinen_12.png' >",
						"value" => 34,
						"description" => "",
						"preview" => "TOILE-DU-MARAIS-TDM035012.png"
						//"background" => "#0000FD"
			 	 		),
			 	 	array(
						"title" => "white",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-leinen_51.png' >",
						"value" => 18,
						"description" => "",
						"preview" => "TOILE-DU-MARAIS-TDM035051.png"
						//"background" => "#FFFFFF"
			 	 		)
			 	 	
			 	 );
			 $colorVersion['satin'] = array(
			 	 	array(
						"title" => "darkred",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-satin_27.png' >",
						"value" => 20,
						"description" => "",
						"preview" => "SILTEX-STXPL27.png"
						//"background" => "#AB212E"
			 	 		),
			 	 	array(
						"title" => "purple",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-satin_28.png' >",
						"value" => 21,
						"description" => "",
						"preview" => "SILTEX-STXPL28.png"
						//"background" => "#800180"
			 	 		),
			 	 	array(
						"title" => "olive",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-satin_15.png' >",
						"value" => 22,
						"description" => "",
						"preview" => "SILTEX-STXPL15.png"
						//"background" => "#7C7C01"
			 	 		),
			 	 	array(
						"title" => "darkslategray",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-satin_11.png' >",
						"value" => 23,
						"description" => "",
						"preview" => "SILTEX-STXPL11.png"
						//"background" => "#2E4E4E"
			 	 		),
			 	 	array(
						"title" => "midnightblue",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-satin_10.png' >",
						"value" => 36,
						"description" => "",
						"preview" => "SILTEX-STXPL10.png"
						//"background" => "#111150"
			 	 		),
			 	 	array(
						"title" => "white",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-satin_22.png' >",
						"value" => 37,
						"description" => "",
						"preview" => "SILTEX-STXPL22.png"
						//"background" => "#FFFFFF"
			 	 		)
			 	 	
			 	 );
			 
			 $colorVersion['witan'] = array(
			 	 	array(
						"title" => "red",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-wintan_14.png' >",
						"value" => 26,
						"description" => "",
						"preview" => "WINTAN-HYDRA-THERMO-WHT14.png"
						//"background" => "#FC0202"
			 	 		),
			 	 	array(
						"title" => "darkred",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-wintan_20.png' >",
						"value" => 27,
						"description" => "",
						"preview" => "WINTAN-HYDRA-THERMO-WHT20.png"
						//"background" => "#AB212E"
			 	 		),
			 	 	array(
						"title" => "darkgreen",
						"classDiv" => "border_select",
						"label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/btnfarbe-wintan_99.png' >",
						"value" => 30,
						"description" => "",
						"preview" => "WINTAN-HYDRA-THERMO-WHT99.png"
						//"background" => "#2F5050"
			 	 		)
			 	 );
			 	 
		return $colorVersion;
	}
	public static function getCalendarColors()
	{
	    return array(
	    	1 => array(
	            array(
                    "id" => 415,
                    "name" => "red",
                    "code" => "D3130B",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/btn_414.png",
                    "preview1" => "/Layout1/Layout1_spiral_pink.png",
                    "preview2" => "/Layout1/Layout1_hard_pink.png"
	                ),
	            array(
                    "id" => 414,
                    "name" => "pink",
                    "code" => "D30B63",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/btn_415.png",
                    "preview1" => "/Layout1/Layout1_spiral_rot.png",
                    "preview2" => "/Layout1/Layout1_hard_rot.png"
	                ),
	            array(
                    "id" => 416,
                    "name" => "orange",
                    "code" => "F8B302",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/btn_416.png",
                    "preview1" => "/Layout1/Layout1_spiral_hellgruen.png",
                    "preview2" => "/Layout1/Layout1_hard_hellgruen.png"
	                ),
	            array(
                    "id" => 417,
                    "name" => "brightblue",
                    "code" => "43C08E",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/btn_417.png",
                    "preview1" => "/Layout1/Layout1_spiral_tuerkis.png",
                    "preview2" => "/Layout1/Layout1_hard_tuerkis.png"
	                ),
	            array(
                    "id" => 418,
                    "name" => "green",
                    "code" => "387C5B",	
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/btn_418.png",
                    "preview1" => "/Layout1/Layout1_spiral_blau.png",
                    "preview2" => "/Layout1/Layout1_hard_blau.png"
	                )
	            ),
	        2 => array(
	            array(
                    "id" => 419,
                    "name" => "red",
                    "code" => "D3130B",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-1.png",
                    "preview1" => "/Layout2/2_spiral_rot.png",
                    "preview2" => "/Layout2/2_hard_rot.png"
	                ),
	            array(
                    "id" => 420,
                    "name" => "pink",
                    "code" => "D30B63",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-2.png",
                    "preview1" => "/Layout2/2_spiral_pink.png",
                    "preview2" => "/Layout2/2_hard_pink.png"
	                ),
	            array(
                    "id" => 421,
                    "name" => "orange",
                    "code" => "F8B302",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-3.png",
                    "preview1" => "/Layout2/2_spiral_orange.png",
                    "preview2" => "/Layout2/2_hard_orange.png"
	                ),
	            array(
                    "id" => 422,
                    "name" => "blue",
                    "code" => "43C08E",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-4.png",
                    "preview1" => "/Layout2/2_spiral_hellgruen.png",
                    "preview2" => "/Layout2/2_hard_hellgruen.png"
	                ),
	            array(
                    "id" => 423,
                    "name" => "green",
                    "code" => "387C5B",	
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-5.png",
                    "preview1" => "/Layout2/2_spiral_gruen.png",
                    "preview2" => "/Layout2/2_hard_gruen.png"
	                ),
	            array(
                    "id" => 424,
                    "name" => "tuerkis",
                    "code" => "22F1B2",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-6.png",
                    "preview1" => "/Layout2/2_spiral_tuerkis.png",
                    "preview2" => "/Layout2/2_hard_tuerkis.png"
	                ),
	            array(
                    "id" => 425,
                    "name" => "petrol",
                    "code" => "1589AD",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-7.png",
                    "preview1" => "/Layout2/2_spiral_petrol.png",
                    "preview2" => "/Layout2/2_hard_petrol.png"
	                ),
	            array(
                    "id" => 426,
                    "name" => "blue",
                    "code" => "0202FC",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-8.png",
                    "preview1" => "/Layout2/2_spiral_blau.png",
                    "preview2" => "/Layout2/2_hard_blau.png"
	                ),
	            array(
                    "id" => 427,
                    "name" => "lila",
                    "code" => "A747AF",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-9.png",
                    "preview1" => "/Layout2/2_spiral_lila.png",
                    "preview2" => "/Layout2/2_hard_lila.png"
	                ),
	            array(
                    "id" => 428,
                    "name" => "brown",
                    "code" => "5E5E5F",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-10.png",
                    "preview1" => "/Layout2/2_spiral_grau.png",
                    "preview2" => "/Layout2/2_hard_grau.png"
	                )
	            ),
	        3 => array(
	            array(
                    "id" => 429,
                    "name" => "red",
                    "code" => "D3130B",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-1.png",
                    "preview1" => "/Layout3/3_spiral_rot.png",
                    "preview2" => "/Layout3/3_hard_rot.png"
	                ),
	            array(
                    "id" => 430,
                    "name" => "pink",
                    "code" => "D30B63",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-2.png",
                    "preview1" => "/Layout3/3_spiral_pink.png",
                    "preview2" => "/Layout3/3_hard_pink.png"
	                ),
	            array(
                    "id" => 431,
                    "name" => "orange",
                    "code" => "F8B302",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-3.png",
                    "preview1" => "/Layout3/3_spiral_orange.png",
                    "preview2" => "/Layout3/3_hard_orange.png"
	                ),
	            array(
                    "id" => 432,
                    "name" => "brightblue",
                    "code" => "43C08E",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-4.png",
                    "preview1" => "/Layout3/3_spiral_hellgruen.png",
                    "preview2" => "/Layout3/3_hard_hellgruen.png"
	                ),
	            array(
                    "id" => 433,
                    "name" => "green",
                    "code" => "387C5B",	
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-5.png",
                    "preview1" => "/Layout3/3_spiral_gruen.png",
                    "preview2" => "/Layout3/3_hard_gruen.png"
	                ),
	            array(
                    "id" => 434,
                    "name" => "tuerkis",
                    "code" => "22F1B2",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-6.png",
                    "preview1" => "/Layout3/3_spiral_tuerkis.png",
                    "preview2" => "/Layout3/3_hard_tuerkis.png"
	                ),
	            array(
                    "id" => 435,
                    "name" => "petrol",
                    "code" => "1589AD",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-7.png",
                    "preview1" => "/Layout3/3_spiral_petrol.png",
                    "preview2" => "/Layout3/3_hard_petrol.png"
	                ),
	            array(
                    "id" => 436,
                    "name" => "blue",
                    "code" => "0202FC",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-8.png",
                    "preview1" => "/Layout3/3_spiral_blau.png",
                    "preview2" => "/Layout3/3_hard_blau.png"
	                ),
	            array(
                    "id" => 437,
                    "name" => "lila",
                    "code" => "A747AF",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-9.png",
                    "preview1" => "/Layout3/3_spiral_lila.png",
                    "preview2" => "/Layout3/3_hard_lila.png"
	                ),
	            array(
                    "id" => 438,
                    "name" => "brown",
                    "code" => "5E5E5F",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-10.png",
                    "preview1" => "/Layout3/3_spiral_grau.png",
                    "preview2" => "/Layout3/3_hard_grau.png"
	                )
	            ),
	        4 => array(
	            array(
                    "id" => 439,
                    "name" => "red",
                    "code" => "D3130B",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-1.png",
                    "preview1" => "/Layout4/bild_spiral_rot.png",
                    "preview2" => "/Layout4/bild_hard_rot.png"
	                ),
	            array(
                    "id" => 440,
                    "name" => "pink",
                    "code" => "D30B63",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-2.png",
                    "preview1" => "/Layout4/bild_spiral_pink.png",
                    "preview2" => "/Layout4/bild_hard_pink.png"
	                ),
	            array(
                    "id" => 441,
                    "name" => "orange",
                    "code" => "F8B302",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-3.png",
                    "preview1" => "/Layout4/bild_spiral_orange.png",
                    "preview2" => "/Layout4/bild_hard_orange.png"
	                ),
	            array(
                    "id" => 442,
                    "name" => "brightblue",
                    "code" => "43C08E",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-4.png",
                    "preview1" => "/Layout4/bild_spiral_hellgruen.png",
                    "preview2" => "/Layout4/bild_hard_hellgruen.png"
	                ),
	            array(
                    "id" => 443,
                    "name" => "green",
                    "code" => "387C5B",	
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-5.png",
                    "preview1" => "/Layout4/bild_spiral_gruen.png",
                    "preview2" => "/Layout4/bild_hard_gruen.png"
	                ),
	            array(
                    "id" => 444,
                    "name" => "tuerkis",
                    "code" => "22F1B2",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-6.png",
                    "preview1" => "/Layout4/bild_spiral_tuerkis.png",
                    "preview2" => "/Layout4/bild_hard_tuerkis.png"
	                ),
	            array(
                    "id" => 445,
                    "name" => "petrol",
                    "code" => "1589AD",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-7.png",
                    "preview1" => "/Layout4/bild_spiral_petrol.png",
                    "preview2" => "/Layout4/bild_hard_petrol.png"
	                ),
	            array(
                    "id" => 446,
                    "name" => "blue",
                    "code" => "0202FC",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-8.png",
                    "preview1" => "/Layout4/bild_spiral_blau.png",
                    "preview2" => "/Layout4/bild_hard_blau.png"
	                ),
	            array(
                    "id" => 447,
                    "name" => "lila",
                    "code" => "A747AF",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-9.png",
                    "preview1" => "/Layout4/bild_spiral_lila.png",
                    "preview2" => "/Layout4/bild_hard_lila.png"
	                ),
	            array(
                    "id" => 448,
                    "name" => "brown",
                    "code" => "5E5E5F",
                    "image" => "/system/modules/tt_manage_notebooks/html/img/step5/color-10.png",
                    "preview1" => "/Layout4/bild_spiral_grau.png",
                    "preview2" => "/Layout4/bild_hard_grau.png"
	                )
	            )
	        );
	}
}

?>
