<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');


require_once("class/class.pn.calendar.php");

class ModuleNotebooksOrder extends Module
{
        protected $strTemplate          = 'mod_notebooks_order';
        protected $config;
        public    $domain;
        public function generate()
        {
            $this->config = new PnConfig();
            
                if (TL_MODE == 'BE')
                {
                        $objTemplate = new BackendTemplate('be_wildcard');
        
                        $objTemplate->wildcard = '### ORDER STEPS NOTEBOOKS ###';
                        $objTemplate->title = $this->headline;
                        $objTemplate->id = $this->id;
                        $objTemplate->link = $this->name;
                        $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
        
                        return $objTemplate->parse();
                }
                
                if(TL_MODE == 'FE')
                {
                        $GLOBALS['TL_CSS'][]            = '/system/modules/tt_manage_notebooks/html/css/order.css|screen';
                        
                        if($_GET["step"] == "upload")
                        {
                            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery-1.9.1.min.js';
                            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.Jcrop.min.js';
                        }
                        else
                        {
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery-1.9.1.min.js';
                        
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/jquery-ui-1.9.2.custom.min.js';
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery.preload-min.js';
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/jquery.pn.buttonex.js';
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/jquery.pn.panel.js';
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/jquery.pn.groupbuttonex.js';
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/jquery.pn.preview.js';
                        $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/order.js';
                        }
                }
        
                return parent::generate();
        }
        
        protected function compile()
        {
            $upload_dir = $GLOBALS['TL_CONFIG']['customer_images_dir'] ? $GLOBALS['TL_CONFIG']['customer_images_dir'] : "/var/www/production/shared/customer-images/";
            $upload_dir_frontend = $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] ? $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] : "/var/www/personalnovel/htdocs/tl_files/customer-images/";
            $customize_page = $GLOBALS['TL_CONFIG']['customize_page_notebook'] ? $GLOBALS['TL_CONFIG']['customize_page_notebook'] : "/customize";
            $this->Template->customize_page =  $customize_page;
            $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
            $this->domain   = $GLOBALS['TL_CONFIG']['cookie_domain']? $GLOBALS['TL_CONFIG']['cookie_domain'] : "web.personalnovel.local";
            $myorder_page = $GLOBALS['TL_CONFIG']['myorder_page'] ? $GLOBALS['TL_CONFIG']['myorder_page'] : "/myorder";
            $step = $_GET['step'] ? $_GET['step'] : $_POST['step'];
            $step = $step ? $step : 1;
            $this->Template->step = $step;
            /*
            *Clear logo
            */
            if($_COOKIE['urllogo'])
            	setcookie('urllogo', "", time() - (86400 * 3), "/",$this->domain);
            /*
             * goto MyOrderPage if order's done
             */
             if($_GET["order"])
            {
                $testorder = new PnOrder();
                $testorder->initWithId($_GET["order"]);
                if(($testorder->step == "done" && $_GET["fedit"] != 1) || ($_GET["fedit"] == 1 && !in_array($step, array(1, 3, 5,'upload'))))
                {
                    header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
                }
                
                if($_GET["fedit"] == 1 && $step == 1 && $testorder->print)
                {
                    header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
                }
                
                if($_GET["fedit"] == 1 && $step == 3 && $testorder->paid)
                {
                    header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
                }
                
                if($_GET["fedit"] == 1 && $step == 5 && $testorder->print)
                {
                    header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
                }
            }
            /* step order----- */
            if($step == 1)
        {
            
            if($_GET['book'])
            {
                $book = new PnBook();
                $book->current($_GET['book']);
                if($book->type_id == 6)
                {
					$order = $this->getOrCreateOrder();
                    $bookItem         = $this->createBookItem($order, $book);
                    if($_GET['layout']){
                    	$this->setCookieLayout($_GET['layout'],1);
                    }
                    
                    if($bookItem)
                    {
                        $widmungItem  = $this->createWidmungItem($order, $bookItem);
                        $designItem   = $this->createDesignItem($order, $bookItem);
                        $order->edit(array("order_step" => "calendar step 1"));
                        header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", $customize_page, $order->id, $bookItem->id, 1));
                    }
                }
                else
                {
                    gotoErrorPage();
                }
            }
            
            $this->step1();
            
            if($_POST['submitter'])
                {
                    if($_GET["bookitem"])
                    {
                        $bookItem = new PnBookItem();
                        $bookItem->initWithId($_GET["bookitem"]);
                        
                        $versions        = PnCalendar::getCalendarVersions();
						$version_default = $versions["130x190"]["Druck"][0];
						$bookVersion     = $bookItem->getBookVersion();
						$material        = PnCalendar::getSelectedCalendarMaterial($bookVersion->id);
						
						if($_POST["Notebooks"])
						{
							$version_default = $versions[$_POST["Notebooks"]][$material][$_POST["Einband"]];
						}
						
						$bookArticle = new PnBookArticle();
						$bookArticle->initWithBookAndVersion($bookItem->book_id, $version_default);
							
						$bookItem->edit(
							array(
								"artikel_id"     => $bookArticle->id,
								"item_updated"   => date('Y/m/d H:i:s a', time()),
								"item_touched"   => date('Y/m/d H:i:s a', time())
							)
						);
						
						//delete cache preview bookItem
						$this->deleteCacheImage();
						
						$bookFields = json_decode($bookItem->fields);
						$paper  = $_POST["paper-option"];
						$bookFields->paper = $paper;
						$bookFields = json_encode($bookFields);
					
						$bookItem->edit(array("item_fields" => $bookFields));
						
						$this->saveCallBack();
						
						//select layout default
						if($_COOKIE['orderLayoutKalender']){
							$decocover_default 	= $_POST["Einband"]!=2?414:449;
							$decocover_id 		= $bookItem->getDecocover() ? $bookItem->getDecocover() : $decocover_default;
							//check orderLayoutKalender
							$layout_choose = $_COOKIE['orderLayoutKalender'];
							if(is_numeric($layout_choose)){
								$bookItem2 = new PnBookItem();
								$bookItem2->initWithId($_GET["bookitem"]);
								$designs 			= PnCalendar::getCalendarDesignsDecocover($_GET["bookitem"],$decocover_id);
								$designItem 		= $bookItem2->getDesignItem();
								$selected_design 	= $designs[$layout_choose];
								if($selected_design){
									$designItem->edit(array("artikel_id" => $selected_design->artikel_id));
								}
							}
						    $this->setCookieLayout("",1);
						}
						
						if($_POST["Einband"] == 0){
							$addons = $bookItem->getAddonItems();
							$addon_cached           = array();
							foreach($addons as $k => $v)
							{
								$addon_cached[] = $v->article_id;
							}
							if(count($addon_cached) > 0)
							{
								$bookItem->removeChildItems($addon_cached);
							}
						}
						
						if($_GET['edit'] && $_GET['edit']==1)
						{
							/* header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id)); */
							header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&edit=1", $customize_page, $bookItem->order_id, $bookItem->id, 5));
						}
						elseif($_GET['fedit'] && $_GET['fedit']==1)
						{
							/* header(sprintf("Location: %s?order=%s&step=6", $checkout_page, $bookItem->order_id)); */
							header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s&fedit=1", $customize_page, $bookItem->order_id, $bookItem->id, 5));
						}
						else 
						{
						    $order = new PnOrder();
						    $order->initWithId($bookItem->order_id);
						    $order->edit(array("order_step" => "calendar step 2"));
							header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 2));
						}
                }
            }
        } 
        elseif($step == 2)
        {
            $this->step2();
            
            if($_POST["submitter"])
            {
                $bookItem 			= new PnBookItem();
                $bookItem->initWithId($_GET["bookitem"]);
                
                $fontId 			= $_POST["font-option"];
                $layout 			= $_POST["layout-option"];
                
                $bookFields 		= json_decode($bookItem->fields);
                
                $bookVersion 		= $bookItem->getBookVersion();
				$bookDesign  		= $bookItem->getDesignItem();
				$bindings 			= PnCalendar::getCalendarBindings();
				$bindung 			= $bindings[$bookVersion->binding];
                //delete cache preview bookItem
                $this->deleteCacheImage();
                $decocover_default 	= $bindings!=2?414:449;
                $decocover_id 		= $bookItem->getDecocover() ? $bookItem->getDecocover() : $decocover_default; 
                
                $designs 			= PnCalendar::getCalendarDesignsDecocover($_GET["bookitem"],$decocover_id);
                $designItem 		= $bookItem->getDesignItem();
                $selected_design 	= $designs[$_POST['Layout']?$_POST['Layout']:1];
                $designItem->edit(array("artikel_id" => $selected_design->artikel_id));
                
                $bookItem->addDecocover(array(
                                        'decocover_id' => $decocover_id,
                                        'item_id'          => $bookItem->id
                                        )
                                );
                
                $doublepages     = PnCalendar::getCalendarDoublePages();
				$size_selected   = PnCalendar::getSelectedCalendarSize($bookVersion->id);
				$design_selected = PnCalendar::getSelectedCalendarDesignId($selected_design->artikel_id, $_GET["bookitem"],$decocover_id);
				$style_options   = $doublepages[$size_selected]["day"][$design_selected];
				foreach($style_options as $k => $v)
				{
						if($k == $layout){
								$layout = $v;
						}
				}
                                
                $bookFields->sections->day->day = $layout;
                $bookFields->font = $fontId;
                
                $bookFields = json_encode($bookFields);
                
                $bookItem->edit(array("item_fields" => $bookFields, "font_id" => $fontId));
                
                $widmungItem = $bookItem->getWidmungItem();
                
                if($_GET['edit'] && $_GET['edit']==1)
                {
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                    }
                    
                }
                else
                {
                    $order = new PnOrder();
                    $order->initWithId($bookItem->order_id);
                    $order->edit(array("order_step" => "calendar step 3"));
                    header(sprintf("Location: %s?&order=%s&bookitem=%s&widmungitem=%s&step=%s", $customize_page,
                        $bookItem->order_id, $bookItem->id, $widmungItem->id, 3));
                }
                
            }
        }
        
        elseif($step == 3)
        {
            $this->step3();
            
            if($_POST["submitter"])
            {
                $bookItem 	= new PnBookItem();
                $bookItem->initWithId($_GET["bookitem"]);
                
                $widmung 	= $bookItem->getWidmungItem();
                
                //delete cache preview bookItem
                $this->deleteCacheImage();
                
                $widmungItem = new PnWidmungItem();
                    $widmungItem->initWithId($widmung->id);
                    
                    if($widmungItem->id == "" || $widmungItem->id == 0)
                {
                    gotoErrorPage();
                }
                else
                {
                    $bookFields                 = json_decode($bookItem->fields);
                    $bookFields->Buchtitel      = $_POST["kopfzeile"];
                    $bookFields->Hauptperson_NN = $_POST["fubzeile"];
                    $bookFields = json_encode($bookFields);
                    $bookItem->edit(array("item_fields" => $bookFields));
                    $fields = array(
                                    "text" => $_POST["var_text"],
                                    "fontface" => $_POST["option-font"]?$_POST["option-font"]:"Century Schoolbook L",
                                    "fontcolor" => $_POST["color-option"]
                                    );
                    $fields = json_encode($fields);
                    if($_POST["color-option"] && $_POST["color-option"]!='black')
                        {
                                $widmung_artikel_id = $this->getWiddmungColor();
                        }
                        else
                        {
                                $widmung_artikel_id = 8830;
                        }
                    $widmungItem->edit(
                                    array(
                                        "item_fields"  => $fields,
                                        "artikel_id"   => $widmung_artikel_id
                                        )
                                    );
                    
                     if($_GET['edit'] && $_GET['edit']==1)
                     {
                        if($GLOBALS['TL_CONFIG']['use_url_token'])
                        {
                            $token = generateToken($bookItem->order_id);
                            header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                        }
                        else
                        {
                            header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                        }
                        
                     }
                     elseif($_GET['fedit'] && $_GET['fedit']==1)
                     {
                         header(sprintf("Location: %s?order=%s&step=6", $checkout_page, $bookItem->order_id));
                     }
                     else
                     {
                         $order = new PnOrder();
                         $order->initWithId($bookItem->order_id);
                         $order->edit(array("order_step" => "calendar step 4"));
                         header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", 
                                $customize_page, $bookItem->order_id, $bookItem->id, 4));
                     }
                }
            }
        }
        
        elseif($step == 4)
        {
            $this->step4();
            
            if($_POST["submitter"])
            {
                $bookItem 	= new PnBookItem();
                $bookItem->initWithId($_GET["bookitem"]);
                
                $startDate  = $_POST["startmonat"];
                $noteStyle  = $_POST["Layout"];
                $interessen = $_POST["interesenv_value"];
                $land 		= $_POST["country_id"];
                $state 		= $_POST["state_id"];
                $module     = $_POST["weitere-inhalte"];
                
                $bookFields = json_decode($bookItem->fields);
                $bookFields->startdate 	= $startDate;
                $bookFields->country_id = $land;
                $bookFields->state_id 	= $state;
                $events 	= array();
                if($noteStyle)
                {
					$bookFields->sections->note->note = $noteStyle;
				}
                if($interessen)
                {
					$events[0] = $interessen;
                }
                if($module)
                {
					$events[1] = $module;
                }
                if(count($events) > 0 )
                {
                    $bookFields->calendar_event_categories = $events;
                }
                $bookFields 	= json_encode($bookFields);
                $bookItem->edit(array("item_fields" => $bookFields));
                
                 //delete cache preview bookItem
                 $this->deleteCacheImage();
				 if($_GET['edit'] && $_GET['edit']==1)
				 {
				    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                    }
				 }
				 else
				 {
				    $order = new PnOrder();
                    $order->initWithId($bookItem->order_id);
                    $order->edit(array("order_step" => "calendar step 5"));
					header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 5));
				 }
            }
        }
        elseif($step == 5)
        {
        	
            $this->step5();
            if($_POST["uploadImg"])
            {
            	if($_POST['order-year'] && $_POST['order-year'] != 'Jahr' && $_POST['order-year'] != ''){
                $bookItem 			= new PnBookItem();
                $bookItem->initWithId($_GET["bookitem"]);
                $designItem 		= $bookItem->getDesignItem();
                $design_select 		= $bookItem->getDecocover();
                $design  			= $_POST["Design"]!=''?$_POST["Design"]:$design_select;
                
                $designItem = $bookItem->getDesignItem();
				$subtitle 		= $_POST["Beschriftung"];
				$designItem->edit(array("item_subtitle" => $subtitle));
				
				$bookFields 	= json_decode($bookItem->fields);
				$bookFields->note_order = $_POST['order-note']?$_POST['order-note']:"Ihr Name oder Titel";
				$yearNow =  date('Y'); 
				$valueDefault = $_POST['order-year-default']?$yearNow:"";
				$year_order 	= $_POST['order-year']?$_POST['order-year']:$valueDefault;
				$bookFields->year_order = $year_order != "Jahr"?$year_order:$valueDefault;
				
				$bookFields 	= json_encode($bookFields);
				$bookItem->edit(array("item_fields" => $bookFields));
                
                $book 				= new PnBook();
                $book->initWithId($bookItem->book_id);
                $decocovers 		= $book->getDecocovers();
                
                $design  			= $_POST["Design"]!=''?$_POST["Design"]:$design_select;
				foreach($decocovers as $key=>$value){
					$design = $design ? $design : $key;
					break;
				}
                $designQuery 		= PnCalendar::getDesign($design);
                $designs 			= PnCalendar::getCalendarDesignsDecocover($_GET["bookitem"],$design);
                $designItem 		= $bookItem->getDesignItem();
                
                $decocover_default 	= $bindings!=2?414:449;
                $decocover_id 		= $bookItem->getDecocover() ? $bookItem->getDecocover() : $decocover_default;
                $design_selected 	= PnCalendar::getSelectedCalendarDesignId($designItem->article_id, $_GET["bookitem"],$decocover_id);
                
                $selected_design 	= $designs[$design_selected];
                $designItem->edit(array("artikel_id" => $selected_design->artikel_id));
                
                $bookItem->addDecocover(array(
                                        'decocover_id' => $design,
                                        'item_id'          => $bookItem->id
                                        )
                                );
                if($_GET['fedit'] && $_GET['fedit']==1)
                      header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=upload&fedit=1", $customize_page, $bookItem->order_id, $bookItem->id,$designItem->id));
                else
                      header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=upload", $customize_page, $bookItem->order_id, $bookItem->id,$designItem->id));
            
            }
            }
            if($_POST["submitter"])
            {
            if($_POST['order-year'] && $_POST['order-year'] != 'Jahr' && $_POST['order-year'] != ''){
                $bookItem = new PnBookItem();
                $bookItem->initWithId($_GET["bookitem"]);
                
                //delete cache preview bookItem
                $this->deleteCacheImage();
                //
                $designItem = $bookItem->getDesignItem();
                $design_select = $bookItem->getDecocover();
                
                $book = new PnBook();
                $book->initWithId($bookItem->book_id);
                $decocovers = $book->getDecocovers();
                
                $design  = $_POST["Design"]!=''?$_POST["Design"]:$design_select;
                                        foreach($decocovers as $key=>$value){
                                                $design = $design ? $design : $key;
                                                break;
                                        }
                //calback edit design
                $designQuery = PnCalendar::getDesign($design);
                $designs = PnCalendar::getCalendarDesignsDecocover($_GET["bookitem"],$design);
                $designItem = $bookItem->getDesignItem();
                
                $decocover_id = $bookItem->getDecocover() ? $bookItem->getDecocover() : 449;
                $design_selected = PnCalendar::getSelectedCalendarDesignId($designItem->article_id, $_GET["bookitem"],$decocover_id);
                
                $selected_design = $designs[$design_selected];
                $designItem->edit(array("artikel_id" => $selected_design->artikel_id));
                
                $bookItem->addDecocover(array(
                                        'decocover_id' => $design,
                                        'item_id'          => $bookItem->id
                                        )
                                );
                
                $bookFields 	= json_decode($bookItem->fields);
                $bookFields->note_order = $_POST['order-note']?$_POST['order-note']:"Ihr Name oder Titel";
                $yearNow =  date('Y'); 
                $valueDefault = $_POST['order-year-default']?$yearNow:"";
                $year_order 	= $_POST['order-year']?$_POST['order-year']:$valueDefault;
                $bookFields->year_order = $year_order != "Jahr"?$year_order:$valueDefault;
                
                $bookFields 	= json_encode($bookFields);
                $bookItem->edit(array("item_fields" => $bookFields));
                
                $subtitle 		= $_POST["Beschriftung"];
                $color_id 		= $_POST["Farbe"];
                
                $bookItem->edit(array("covercolor_id" => $color_id));
                $designItem->edit(array("item_subtitle" => $subtitle));
                
                $bookVersion   	= $bookItem->getBookVersion();
                $doublepages   	= PnCalendar::getCalendarDoublePages();
                $size_selected 	= PnCalendar::getSelectedCalendarSize($bookVersion->id);
            
                $material 		= $_POST["Umschlag"];
                $versions 		= PnCalendar::getCalendarVersions();
                
                $bindings 		= PnCalendar::getCalendarBindings();
                $binding  		= $bindings[$bookVersion->binding]?$bindings[$bookVersion->binding]:"0";
                
                $version 		= $versions[$size_selected][$material][$binding];
                
                $bookArticle 	= new PnBookArticle();
                $bookArticle->initWithBookAndVersion($bookItem->book_id, $version);
                
                $bookItem->edit(array("artikel_id" => $bookArticle->id));
                //Load array decocover -> if id value decocover in array parent -> if parent == 4 ? give image Upload : delete image Upload 
                //check decocover now same upload : if don't upload -> delete image cache
                $this->CheckImageServer($bookItem);
                
               if($_GET['edit'] && $_GET['edit']==1)
               {
                    if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                    }
                    
               }
               elseif($_GET['fedit'] && $_GET['fedit']==1)
               {
                   header(sprintf("Location: %s?order=%s&step=6", $checkout_page, $bookItem->order_id));
               }
               else
               {
                   $order = new PnOrder();
                   $order->initWithId($bookItem->order_id);
                   $order->edit(array("order_step" => "calendar step 6"));
                   header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 6));
               }
                
            }
            }
        }
        
        elseif($step == 6)
        {
            $this->step6();
            if($_POST["submitter"])
            {
                    $bookItem = new PnBookItem();
                    $bookItem->initWithId($_GET["bookitem"]);
                    
                    $addons = $bookItem->getAddonItems();
                                
					$addon_cached           = array();
					$addon_selected         = array();
					$addon_exists           = array();
					$addon_will_remove      = array();
					$addon_will_addnew      = array();
					
					//TODO : optimize later
					foreach($addons as $k => $v)
					{
						$addon_cached[] = $v->article_id;
					}
					
					if($_POST['Lesezeichen'] && $_POST['Lesezeichen']!="NULL")
					{
						$addon_selected[] = $_POST['Lesezeichen'];
					}
					if($_POST['Stiftschlaufe'] && $_POST['Stiftschlaufe']!="NULL")
					{
						$addon_selected[] = $_POST['Stiftschlaufe'];
					}
					if($_POST['Gummiband'] && $_POST['Gummiband']!="NULL")
					{
						$addon_selected[] = $_POST['Gummiband'];
					}
					if($_POST['Froschtasche'] && $_POST['Froschtasche']!="NULL")
					{
						$addon_selected[] = $_POST['Froschtasche'];
					}
					
					foreach($addon_cached as $old_addon)
					{
						if(in_array($old_addon, $addon_selected))
						{
							$addon_exists[] = $old_addon;
						}
						else
						{
							$addon_will_remove[] = $old_addon;
						}
					}
					
					foreach($addon_selected as $new_addon)
					{
						if(!in_array($new_addon, $addon_cached))
						{
							$addon_will_addnew[] = $new_addon;
						}
					}
					
					//Remove addon
					if(count($addon_will_remove) > 0)
					{
						$bookItem->removeChildItems($addon_will_remove);
					}
					
					//Add new 
					$item = new PnItem();
					foreach($addon_will_addnew as $addon_new)
					{
						$article = new PnArticle();
						$article->initWithId($addon_new);
						
						$item->add(array(
										"artikel_id"    => $article->id,
										"order_id"              => $bookItem->order_id,
										"parent_id"     => $bookItem->id,
										"item_pieces"   => 1,
										"class_id"              => $article->class_id
								));
					}
					// header(sprintf("Location: %s?order=%s&bookitem=%s&step=%s", $customize_page, $_GET["order"], $bookItem->id, 6));
					
					if($GLOBALS['TL_CONFIG']['use_url_token'])
                    {
                        $token = generateToken($bookItem->order_id);
                        $order = new PnOrder();
                        $order->initWithId($bookItem->order_id);
                        $order->edit(array("order_step" => "basket"));
                        header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $bookItem->order_id, $token));
                    }
                    else
                    {
                        header(sprintf("Location: %s?order=%s", $checkout_page, $bookItem->order_id));
                    }
					
				}
        }
        elseif($step == "upload" && $_GET["order"] && $_GET["designitem"])
                {
                        $this->step_upload();
                        if($_POST["uploader"])
                                {
                                        $file = str_replace(' ', '_', $_FILES['imagefile']['name']);
                                        $permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
                                        
                                        $order_id  = $_GET["order"];
                                        $design_id = $_GET["designitem"];
                                        
                                        $file_extension = ".png.org.png";
                                        $file_short_extension = ".jpg";
                                        
                                        if($_FILES['imagefile']['type'] == 'image/gif')
                                        {
                                                $file_extension = ".gif.org.gif";
                                        }
                                        if($_FILES['imagefile']['type'] == 'image/jpeg' || $_FILES['imagefile']['type'] == 'image/pjpeg')
                                        {
                                                $file_extension = ".jpg.org.jpg";
                                        }
                                        
                                        $file = $order_id . "-" . $design_id . $file_extension;
                                        if (in_array($_FILES['imagefile']['type'], $permitted))
                                        {
                                                //Customer images
                                                $cachefiles = glob($upload_dir . $order_id . "-" . $design_id . "*");
                                                array_walk($cachefiles, function ($cachefile) {
                                                        unlink($cachefile);
                                                });
                                                
                                                //Customer frontend images
                                                $cachefiles = glob($upload_dir_frontend . $order_id . "-" . $design_id . "*");
                                                array_walk($cachefiles, function ($cachefile) {
                                                        unlink($cachefile);
                                                });
                                                
                                                //Upload to folder print
                                                if(move_uploaded_file($_FILES['imagefile']['tmp_name'], $upload_dir_frontend . $file))
                                                {
                                                        //Default crop file
                                                        $filename  = pathinfo($upload_dir_frontend . $file);
                                                        $extension = $filename['extension'];
                                                        /*$imagesize = getimagesize($upload_dir_frontend . $file);
                                                        $imagew    = $imagesize[0];
                                                        $imageh    = $imagesize[1];*/
                                                        
                                                        $targ_h = $_POST['targ_h'];
                                                        $targ_w = $_POST['targ_w'];
                                                        $crop_x = 0;
                                                        $crop_w = $targ_w;
                                                        $crop_y = 0;
                                                        $crop_h = $targ_h;
                                                        
                                                        switch($extension)
                                                        {
                                                                case "png":
                                                                        $img_r = imagecreatefrompng($upload_dir_frontend . $file);
                                                                        break;
                                                                case "gif":
                                                                        $img_r = imagecreatefromgif($upload_dir_frontend . $file);
                                                                        break;
                                                                default:
                                                                        $img_r = imagecreatefromjpeg($upload_dir_frontend . $file);
                                                                        break;
                                                        }
                                                        
                                                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                                                        imagecopyresampled($dst_r, $img_r, 0, 0, $crop_x, $crop_y, $targ_w, $targ_h, $crop_w, $crop_h);
                                                        imagejpeg($dst_r, $upload_dir . $order_id . "-" . $design_id . $file_short_extension);
                                                        
                                                        imagejpeg($dst_r, $upload_dir_frontend . $order_id . "-" . $design_id . $file_short_extension);
                                                        
                                                }
                                                
                                                $designItem = new PnDesignItem();
                                                $designItem->initWithId($_GET["designitem"]);
                                                
                                                $designItem->edit(
                                                                                array (
                                                                                   "item_image1_present"  => 1,
                                                                                   "item_image1_cropped"   => 1
                                                                                )
                                                                        );
                                                if($_GET["edit"] == 1) $fla = "&edit=1"; else $fla = '';
                                                if($_GET["fedit"] == 1) $fla = "&fedit=1"; else $fla = "";
                                                
                                                if($fla == "")
                                                {
                                                    $order = new PnOrder();
                                                    $order->initWithId($designItem->order_id);
                                                    $order->edit(array("order_step" => "calendar step upload"));
                                                }
                                   
                                                header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s%s", 
                                                        $customize_page, $designItem->order_id, $designItem->parent_id, $designItem->id, "upload",$fla));
                                        }
                                }
                                
                                
                                if($_POST["cropimage"])
                                {
                                        //Crop image
                                        $upload_filename = "";
                                        $order_id = $_GET["order"];
                                        $design_id = $_GET["designitem"];
                                        $crop_filename   = $upload_dir . $order_id . "-" . $design_id . ".jpg";
                                        $crop_filename2   = $upload_dir_frontend . $order_id . "-" . $design_id . ".jpg";
                                        if(file_exists($upload_dir_frontend . $order_id . "-" . $design_id . ".png.org.png"))
                                        {
                                                $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".png.org.png";
                                        }
                                        else if(file_exists($upload_dir_frontend . $order_id . "-" . $design_id . ".gif.org.gif"))
                                        {
                                                $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".gif.org.gif";
                                        }
                                        else
                                        {
                                                $upload_filename = $upload_dir_frontend . $order_id . "-" . $design_id . ".jpg.org.jpg";
                                        }
                                        
                                        $filename  = pathinfo($upload_filename);
                                        $extension = $filename['extension'];
                                        $imagesize = getimagesize($upload_filename);
                                        $imagew    = $imagesize[0];
                                        $imageh    = $imagesize[1];
                                        
                                        $targ_h = $_POST['targ_h'];
                                        $targ_w = $_POST['targ_w'];
                                        if($imagew > $imageh)
                                        {
                                                $targ_w = $imageh / $targ_h * $targ_w;
                                                $targ_h = $imageh;
                                        } else {
                                                $targ_h = $imagew / $targ_w * $targ_h;
                                                $targ_w = $imagew;
                                        }
                                        
                                        //$quality = 90;
                                        $src = $upload_filename;
                                        $dst = $crop_filename;
                                        $dst2 = $crop_filename2;
                                        switch($extension)
                                        {
                                                case "png":
                                                        $img_r = imagecreatefrompng($src);
                                                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                                                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                                                                $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                                                        imagejpeg($dst_r, $dst);
                                                        imagejpeg($dst_r, $dst2);
                                                        break;
                                                case "gif":
                                                        $img_r = imagecreatefromgif($src);
                                                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                                                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                                                                $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                                                        imagejpeg($dst_r, $dst);
                                                        imagejpeg($dst_r, $dst2);
                                                        break;
                                                default:
                                                        $img_r = imagecreatefromjpeg($src);
                                                        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                                                        imagecopyresampled($dst_r,$img_r,0,0,$_POST['crop_x'],$_POST['crop_y'],
                                                                $targ_w,$targ_h,$_POST['crop_w'],$_POST['crop_h']);
                                                        imagejpeg($dst_r, $dst);
                                                        imagejpeg($dst_r, $dst2);
                                                        break;
                                        }
                                        
                                        if($_GET["edit"] == 1)
                                        {
                                            if($GLOBALS['TL_CONFIG']['use_url_token'])
                                            {
                                                $token = generateToken($_GET['order']);
                                                header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                                            }
                                            else
                                            {
                                                header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
                                            }
                                            
                                        }
                                        else if($_GET['fedit'] && $_GET['fedit']==1){
                                        	header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s&fedit=1", 
                                                        $customize_page, $_GET["order"], $_GET["bookitem"], $_GET["designitem"], 5));
                                        }
                                        else
                                        {
                                                header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                                                        $customize_page, $_GET["order"], $_GET["bookitem"], $_GET["designitem"], 5));
                                        }
                                }
                                
                                if($_POST["dochkein"] || $_POST["nextpage"])
                                {
                                        if($_GET["edit"] && $_GET["edit"] == 1)
                                        {
                                            if($GLOBALS['TL_CONFIG']['use_url_token'])
                                            {
                                                $token = generateToken($_GET['order']);
                                                header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                                            }
                                            else
                                            {
                                                header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
                                            }
                                        }
                                        else
                                        {
                                            $order = new PnOrder();
                                            $order->initWithId($_GET["order"]);
                                            $order->edit(array("order_step" => "calendar step 5"));
                                            header(sprintf("Location: %s?order=%s&bookitem=%s&designitem=%s&step=%s", 
                                                $customize_page, $_GET["order"], $_GET["bookitem"], $_GET["designitem"], 5));
                                        }
                                }
                }
           
        }
        
        function step1()
        {
            $json_size = json_encode(array(
                                array(
                                    "classDiv" => "border_select size_s",
                                    "label" => "",
                                    "value" => "130x190",
                                    "description" => "0,00",
                                    "price" => "0.00"
                                ),
                                array(
                                    "classDiv" => "border_select size_m",
                                    "label" => "",
                                    "value" => "148x210",
                                    "description" => "+ 3,00",
                                    "price" => "3.00"
                                ),
                                array(
                                    "classDiv" => "border_select size_l node3",
                                    "label" => "",
                                    "value" => "167x244",
                                    "description" => "+ 4,00",
                                    "price" => "4.00"
                                ),
                                array(
                                    "classDiv" => "border_select size_xl node4",
                                    "label" => "",
                                    "value" => "185x262",
                                    "description" => "+ 5,00",
                                    "price" => "5.00"
                                )
                            ));
            
        $json_binding = json_encode(array(
                                        array(
                                            "classDiv" => "border_select spiral",
                                            "label" => "",
                                            "value" => "0",
                                            "description" => "0,00",
                                            "price" => "0.00"
                                        ),
                                        array(
                                            "classDiv" => "border_select hard",
                                            "label" => "",
                                            "value" => "1",
                                            "description" => "+ 3,00",
                                            "price" => "3.00"
                                        ),
                                        array(
                                            "classDiv" => "border_select paegung",
                                            "label" => "",
                                            "value" => "2",
                                            "description" => "+ 3,00",
                                            "price" => "3.00"
                                        )
                                        ));
            
       
        
            $this->Template->sizes    = $json_size;
            $this->Template->bindings = $json_binding;
           
            
            //get book item
            if($_GET["bookitem"])
            {
                $bookItem 			= new PnBookItem();
                $bookItem->initWithId($_GET["bookitem"]);
                
                $bookVersion 		= $bookItem->getBookVersion();
                //default 24.95 -> Version 2001
                $price_old 			= number_format($bookVersion->price, 2, ".", "") - 24.95;
                $bookDesign  		= $bookItem->getDesignItem();
                $selected_size 		= "130x190";
                $selected_binding 	= 0;
                $versions 			= PnCalendar::getCalendarVersions();
                foreach($versions as $k => $v)
                {
                    foreach($v as $k1 => $v1)
                    {
                        foreach($v1 as $k2 => $v2)
                        {
							$selected_size = $bookVersion->id == $v2?$k:$selected_size;
                        }
                    }
                }
                
                $arraySize = json_decode($json_size);
                foreach($arraySize as $size)
                {
                        if($size->value == $selected_size)
                        {
							$select_size_price = $size->price;
                        }
                }
                $this->Template->selected_size_price = $select_size_price?$select_size_price:"0.00";
                $bindings = PnCalendar::getCalendarBindings();
                $arrayBindings = json_decode($json_binding);
                foreach($arrayBindings as $Bindings)
                {
                        if($Bindings->value == $bindings[$bookVersion->binding])
                        {
							$select_bindings_price = $Bindings->price;
                        }
                }
                
                $this->Template->selected_binding_price = $select_bindings_price?$select_bindings_price:"0.00";
                
                $bookFields 						= json_decode($bookItem->fields);
                $this->Template->headerText    		= $bookFields->Buchtitel;
                $this->Template->footerText    		= $bookFields->Hauptperson_NN;
                $this->Template->selected_binding 	= $bindings[$bookVersion->binding];
                $this->Template->selected_size 		= $selected_size;
                $this->Template->totalPrice     	= $bookItem->getTotalPrice();
                $this->Template->oldPrice       	= $price_old;
                $this->Template->selectedPaper  	= $bookFields->paper;
            }
            
        }
        
        function step2()
        {
                
             $json_layout = json_encode(array(
                                                                        array(
                                            "classDiv" => "border_select btnlayout1",
                                            "label" => "",
                                            "value" => "1",
                                            "description" => "0,00",
                                            "price" => "0.00"
                                        ),
                                        array(
                                            "classDiv" => "border_select btnlayout2",
                                            "label" => "",
                                            "value" => "2",
                                            "description" => "+ 3,00",
                                            "price" => "3.00"
                                        ),
                                        array(
                                            "classDiv" => "border_select btnlayout3",
                                            "label" => "",
                                            "value" => "3",
                                            "description" => "+ 3,00",
                                            "price" => "3.00"
                                        ),
                                        array(
                                            "classDiv" => "border_select btnlayout4",
                                            "label" => "",
                                            "value" => "4",
                                            "description" => "+ 5,00",
                                            "price" => "5.00"
                                        )
                                    ));
            $this->Template->layouts  = $json_layout;
            
            $bookItem 			= new PnBookItem();
            $bookItem->initWithId($_GET["bookitem"]);
            
            $bookFields 		= json_decode($bookItem->fields);
            
            $bookDesign  		= $bookItem->getDesignItem();
            $bookVersion 		= $bookItem->getBookVersion();
            $bindings 			= PnCalendar::getCalendarBindings();
            $this->Template->selected_binding = $bindings[$bookVersion->binding];
            
            $doublepages     	= PnCalendar::getCalendarDoublePages();
            $size_selected   	= PnCalendar::getSelectedCalendarSize($bookVersion->id);
            $decocover_default 	= $bindings!=2?414:449;
            $decocover_id 		= $bookItem->getDecocover() ? $bookItem->getDecocover() : $decocover_default;
            $design_selected 	= PnCalendar::getSelectedCalendarDesignId($bookDesign->article_id, $_GET["bookitem"],$decocover_id);
            $style_options   	= $doublepages[$size_selected]["day"][$design_selected];
            $selectStyle 		= $bookFields->sections->day->day;
            foreach($doublepages as $k=>$v)
            {
                foreach($v as $k1=>$v1)
                {
                        if($k1 == "day")
                        {
                                foreach($v1 as $k2=>$v2)
                                {
                                        foreach($v2 as $k3=>$v3)
                                        {
											$selectStyle = $selectStyle == $v3?$k3:$selectStyle;
                                        }
                                }
                        }
                }
            }
            $styles = array();
            $styles_value = array();
            foreach($style_options as $k => $v)
            {
                $styles[] = array(
                                 "classDiv" => sprintf("border_select %s",$k) ,
                                 "label" => "",
                                 "value" => "$k",
                                 "description" =>"&nbsp;"
                             );
                $styles_value[] = array(
                        		$k =>$k
                        	);
            }
                
			$arrayLayouts = json_decode($json_layout);
			foreach($arrayLayouts as $layout)
			{
					if($layout->value == $selected_layout)
					{
						$selected_layout_price = $layout->price;
					}
			}
			$this->Template->selected_layout_price = $selected_layout_price?$selected_layout_price:"0.00";
            $this->Template->selected_layout  	= $design_selected;
            $this->Template->layouts_1        	= json_encode($styles);
            $this->Template->layouts_1_value  	= preg_replace("/\{|\}|\[|\]/","",json_encode($styles_value));
            $this->Template->selectedFont   	= $bookItem->font_id;
            $this->Template->selectedLayout 	= $selectStyle;
            $this->Template->headerText     	= $bookFields->Buchtitel;
            $this->Template->footerText     	= $bookFields->Hauptperson_NN;
            $this->Template->totalPrice     	= $bookItem->getTotalPrice();
            $this->Template->selectedPaper  	= $bookFields->paper;
            
            $customize_page = $GLOBALS['TL_CONFIG']['customize_page_notebook'] ? $GLOBALS['TL_CONFIG']['customize_page_notebook'] : "/customize";
            $this->Template->preview        	= sprintf("%s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 1);
        }
        
        function step3()
        {
            $bookItem 		= new PnBookItem();
            $bookItem->initWithId($_GET["bookitem"]);
            $bookVersion 	= $bookItem->getBookVersion();
            $bindings 		= PnCalendar::getCalendarBindings();
            $this->Template->selected_binding = $bindings[$bookVersion->binding];
            
            $widmung 		= $bookItem->getWidmungItem();
            
            $widmungItem 	= new PnWidmungItem();
            $widmungItem->initWithId($widmung->id);
            
            $bookFields 	= json_decode($bookItem->fields);
            $widmungFields 	= json_decode($widmungItem->fields);
            
            $this->Template->selectedText  		= $widmungFields->text;
            $this->Template->selectedFont  		= $widmungFields->fontface?$widmungFields->fontface:"Century Schoolbook L";
            $this->Template->selectedColor 		= $widmungFields->fontcolor;
            $this->Template->headerText    		= $bookFields->Buchtitel;
            
            $this->Template->footerText    		= $bookFields->Hauptperson_NN;
            $this->Template->selectedFontMain   = $bookItem->font_id;
            $this->Template->widmungPrice  		= $widmungItem->getPrice();
            $this->Template->totalPrice     	= $bookItem->getTotalPrice();
            $this->Template->selectedPaper  	= $bookFields->paper;
            
            $customize_page = $GLOBALS['TL_CONFIG']['customize_page_notebook'] ? $GLOBALS['TL_CONFIG']['customize_page_notebook'] : "/customize";
            $this->Template->preview        	= sprintf("%s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 2);
        }
        
        function step4()
        {
            $bookItem 			= new PnBookItem();
            $bookItem->initWithId($_GET["bookitem"]);
            
            $bookFields 		= json_decode($bookItem->fields);
            $bookVersion   		= $bookItem->getBookVersion();
            $doublepages   		= PnCalendar::getCalendarDoublePages();
            $size_selected 		= PnCalendar::getSelectedCalendarSize($bookVersion->id);
            $note_options  		= $doublepages[$size_selected]["note"];
            $events 			= $bookFields->calendar_event_categories;
            $module_select 		= 10;
            $interessen_select 	= '';
            if($events)
            {
                foreach($events as $k => $v)
                {
                    if(in_array($v,array(1,10,12)))
                    {
                        $module_select = $v;
                    }
                    else
                    {
                        $interessen_select = $v;
                    }
                }
            }
            
            $bindings 		= PnCalendar::getCalendarBindings();
            $this->Template->selected_binding = $bindings[$bookVersion->binding];
            $notes 			= array();
            $note_values 	= array();
            foreach($note_options as $k => $v)
            {
                $notes[] = array(
                                 "classDiv" => "border_select $k",
                                 "label" => "",
                                 "value" => "$v"
                             );
                $note_values[]=	array("$v" => "$k");
            }
            $interesens = array(
                        array(
                                "id"   => 2,
                                "name" => "Geschichte",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 6,
                                "name" => "Sport Allgemein",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 3,
                                "name" => "Wirtschaft International",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 7,
                                "name" => "Sport Fussball",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 4,
                                "name" => "Wirtschaft Deutsch",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 5,
                                "name" => "Musik & Kultur",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 9,
                                "name" => "Literatur & Medien",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 11,
                                "name" => "Bauern kalender",
                                "price"=> "3.00"
                                ),
                        array(
                                "id"   => 8,
                                "name" => "Wissenschaft & Technik",
                                "price"=> "3.00"
                                )
                        );
            $event = array();
            foreach($interesens as $value)
            {
            	   $event[$value['id']] = $bookItem->getEventCategory($value['id']);
            }
            
            $selectStyle 	= $bookFields->sections->day->day;
            foreach($doublepages as $k=>$v)
            {
                foreach($v as $k1=>$v1)
                {
                        if($k1 == "day"){
                                foreach($v1 as $k2=>$v2)
                                {
                                        foreach($v2 as $k3=>$v3)
                                        {
											$selectStyle = $selectStyle == $v3?$k3:$selectStyle;
                                        }
                                }
                        }
                }
            }
            
            $bookDesign  		= $bookItem->getDesignItem();
            $decocover_id 		= $bookItem->getDecocover() ? $bookItem->getDecocover() : 449;
            $design_selected 	= PnCalendar::getSelectedCalendarDesignId($bookDesign->article_id, $_GET["bookitem"],$decocover_id);
            
            $this->Template->interesens 		= $interesens;
            $this->Template->events 			= json_encode($event);
            $this->Template->notes      		= json_encode($notes);
            $this->Template->headerText 		= $bookFields->Buchtitel;
            $this->Template->footerText 		= $bookFields->Hauptperson_NN;
            $this->Template->selectedStartDate 	= $bookFields->startdate;
            $this->Template->selectedNoteStyle 	= $bookFields->sections->note->note;
            $this->Template->selectedFontMain   = $bookItem->font_id;
            $this->Template->module_select 		= $module_select;
            $this->Template->interessen_select 	= $interessen_select;
            $this->Template->selected_layout  	= $design_selected;
            $this->Template->selectedLayout 	= $selectStyle;
            $this->Template->notes_value  		= preg_replace("/\{|\}|\[|\]/","",json_encode($note_values));
            $this->Template->selectedPaper  	= $bookFields->paper;
            $this->Template->note_options 		= $note_options;
            $this->Template->totalPrice     	= $bookItem->getTotalPrice();
            
            $customize_page = $GLOBALS['TL_CONFIG']['customize_page_notebook'] ? $GLOBALS['TL_CONFIG']['customize_page_notebook'] : "/customize";
            $this->Template->preview        	= sprintf("%s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 3);
        }
        
        function step5()
        {
            $bookItem 		= new PnBookItem();
            $bookItem->initWithId($_GET["bookitem"]);
            $designItem 	= $bookItem->getDesignItem();
            $bookVersion   	= $bookItem->getBookVersion();
            $bindings 		= PnCalendar::getCalendarBindings();
            $bookFields 	= json_decode($bookItem->fields);
            $verionColor 	= array();
            $designs_upload = array();
            $imageDesignsUpload = array(
                        0 => array(
                                0 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/upload-bindung-spiral_10.png'>",
                                1 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/upload-bindung-spiral_12.png'>",
                                2 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/upload-bindung-spiral_14.png'>",
                                3 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/bild-spiral_1.png'>"
                                ),
                        1 => array(
                                0 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/upload-bindung-hard_10.png'>",
                                1 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/upload-bindung-hard_12.png'>",
                                2 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/upload-bindung-hard_14.png'>",
                                3 => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/bild-hard_1.png'>"
                                )
                        );
            $materials = array();
            $Price_step5_versions = PnCalendar::getPriceVersionsStep5();
            $tmpList = 	array(
							/*array(
								"name"  => "Druck",
								"price" => $Price_step5_versions["Druck"],
								"desc"  => number_format($Price_step5_versions["Druck"], 2, ",", "")
							),*/
							array(
								"name"  => "Leinen",
								"price" => $Price_step5_versions["Leinen"],
								"desc"  => "+ ".number_format($Price_step5_versions["Leinen"], 2, ",", "")
							),
						
							array(
								"name"  => "Nabuka",
								"price" => $Price_step5_versions["Nabuka"],
								"desc"  => "+ ".number_format($Price_step5_versions["Nabuka"], 2, ",", "")
							),
						
							array(
								"name"  => "Seide",
								"price" => $Price_step5_versions["Seide"],
								"desc"  => "+ ".number_format($Price_step5_versions["Seide"], 2, ",", "")
							)
						);
            
        if($bindings[$bookVersion->binding] == 2){
			$verionColor = PnCalendar::getCalendarColorsV1();
			foreach($tmpList as $k => $v)
			{
					
				$materials[] = array(
								 "classDiv" => sprintf("border_select %s",$v["name"]),
								 "label" => "",
								 "value" => $v["name"],
								 "price" => $v["price"],
								 "description" => $v["desc"]
								 );
			}
        }
        else
        {
			foreach($tmpList as $k => $v)
			{
					$materials[] = array(
						 "classDiv" => "border_select",
						 "label" => "<img src='/system/modules/tt_manage_notebooks/html/img/step5/img-upload-new.png'>",
						 "value" => $v["name"],
						 "price" => "0.00"
						 );
			}
           $designs_upload = array(
				  array(
					 "classDiv" => "border_select",
					 "label" => sprintf("%s",$imageDesignsUpload[$bindings[$bookVersion->binding]?$bindings[$bookVersion->binding]:0][0]),
					 "value" => 1,
					 "price" => "0.00"
					 ),
				  array(
					 "classDiv" => "border_select",
					 "label" => sprintf("%s",$imageDesignsUpload[$bindings[$bookVersion->binding]?$bindings[$bookVersion->binding]:0][1]),
					 "value" => 2,
					 "price" => "0.00"
					 ),
				  array(
					 "classDiv" => "border_select",
					 "label" => sprintf("%s",$imageDesignsUpload[$bindings[$bookVersion->binding]?$bindings[$bookVersion->binding]:0][2]),
					 "value" => 3,
					 "price" => "0.00"
					 )
                );
           if(!$design_bild_default){
                   $design_bild_default = $imageDesignsUpload[$bindings[$bookVersion->binding]?$bindings[$bookVersion->binding]:0][3];
           }
        }
            
            $colors = array();
            $colectColorDesign = PnCalendar::getCalendarColors();
            foreach($colectColorDesign as $k => $v)
            {
                foreach($v as $k2 => $v2)
                {
                    $imgImage = $k==1?"<img src='".$v2['image']."'>":"";
                    $colors[$k][] = array(
                             "classDiv" => sprintf("border_select %s",$v2['name']),
                             "label" => $imgImage,
                             "value" => $v2["id"],
                             "description" => "",
                             "background" => "#".$v2['code']
                         );
                }
            }
            
            $bookVersion = $bookItem->getBookVersion();
            $selected_version = $bookVersion->id;
            
            $versions = PnCalendar::getCalendarVersions();
            
            $selected_material = "Leinen";
            
            foreach($versions as $k => $v)
            {
                foreach($v as $k1 => $v1)
                {
                    foreach($v1 as $k2 => $v2)
                    {
                        if($selected_version == $v2)
                        {
                            $selected_material = $k1;
                        }
                    }
                }
            }
            
            $selected_material_price = $Price_step5_versions[$selected_material];
            $book = new PnBook();
            $book->initWithId($bookItem->book_id);
            $decocovers = $book->getDecocovers();
            $cover1 = array();
            $cover2 = array();
            
            $design_select = $bookItem->getDecocover();
           
            $this->Template->bookItem  			= $bookItem;
            $this->Template->priceMaterial 		= $Price_step5_versions[$selected_material];
            $this->Template->material   		= $selected_material;
            $this->Template->material_price   	= $selected_material_price;
            $this->Template->materials  		= json_encode($materials);
            $this->Template->designs_upload  	= json_encode($designs_upload);
            $this->Template->designs_uploads  	= $designs_upload;
            $this->Template->design_bild_default= $design_bild_default;
            $this->Template->colors     		= $colors;
            $this->Template->colectColorDesign 	= $colectColorDesign;
            $this->Template->buchtitel  		= "Kalender";
            $this->Template->notecover  		= $bookFields->note_order;
            $this->Template->yearcover  		= $bookFields->year_order;
            $this->Template->selected_binding 	= $bindings[$bookVersion->binding];
            $this->Template->subtitle   		= $designItem->subtitle;
            $this->Template->cover1     		= json_encode($cover1);
            $this->Template->color      		= $bookItem->covercolor_id;
            $this->Template->totalPrice 		= $bookItem->getTotalPrice();
            $this->Template->cover_select     	= $design_select;
            $this->Template->verionColor        = $verionColor;
            $this->Template->cover2 = $cover2;
            $this->Template->book   = $book;
            $customize_page 		= $GLOBALS['TL_CONFIG']['customize_page_notebook'] ? $GLOBALS['TL_CONFIG']['customize_page_notebook'] : "/customize";
            $this->Template->preview= sprintf("%s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 4);
            
            $upload_dir_frontend 	= $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] ? $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] : "/var/www/personalnovel/htdocs/tl_files/customer-images/";
            $filename   			= $upload_dir_frontend . $bookItem->order_id . "-" . $designItem->id . ".jpg";
            $filenameFrontend 		="/tl_files/customer-images/" . $bookItem->order_id . "-" . $designItem->id . ".jpg"."?".time();
            
            if(file_exists($filename)) 
            {
                $this->Template->previewImgUpload = $filenameFrontend;
            }
            else
            {
                $this->Template->previewImgUpload = "";
            }
               
                
        }
        function step_upload(){
            $scale_preview = 0.8;    
            $designItem   = new PnDesignItem();
            $designItem->initWithId($_GET["designitem"]);
            
            $bookItem     = new PnBookItem();
            $bookItem->initWithId($designItem->parent_id);
            
            $decocover_select =  $bookItem->getDecocover();
            $colectColorDesign = PnCalendar::getCalendarColors();
            $designCodes = $colectColorDesign[4];
            $Decocover = array();
            foreach($designCodes as $k => $v)
            {
                if($decocover_select == $v['id']){
                		$Decocover = $v;
                }
            }
            
            $bookFields = json_decode($bookItem->fields);
            $this->Template->titleCover = $designItem->subtitle;
            $this->Template->yearCover = $bookFields->year_order;
            $this->Template->notesCover = $bookFields->note_order;
            $this->Template->decocover = $Decocover;
            
            $book         = new PnBook();
            $book->initWithId($bookItem->book_id);
            
            $bookVersion = $book->getBookVersion($bookItem->article_id);
            
            $version      = new PnVersion();
        
        if(count($bookVersion) > 0)
        {
            $version = $bookVersion[0];
        }
        
        $this->Template->size_select = "size_".$version->width."x".$version->height;
        
        $version->width =  $version->width / floatval($version->picscale_x) * $scale_preview;
        $version->height = $version->height / floatval($version->picscale_y) * $scale_preview;
        
        
        $preview_dpi        = 72;
        $shadow             = 2;
        $page_w             = mmtopx($version->width, $preview_dpi) - 1;
        $page_h             = mmtopx($version->height, $preview_dpi) - 1;
        $coverpic_w         = cmtopx($version->picwidth  * $scale_preview, $preview_dpi);
        $coverpic_h         = cmtopx($version->picheight * $scale_preview, $preview_dpi);
        
        $coverpic_h_mask = 1 ;
        $coverpic_w_mask = 1.007 ;
        
        $mask_x = 0;
        $mask_y = 0;
        $mask_w = $coverpic_w * $coverpic_w_mask;
        $mask_h = $coverpic_h * $coverpic_h_mask;
        $preview_overflow = 50;
        
        $this->Template->color_background_id     = $bookItem->getDecocover();
        
        $this->Template->vcover_w   = $page_w + $shadow;
        $this->Template->vcover_h   = $page_h + $shadow;
        $this->Template->mask_w     = $mask_w;
        $this->Template->mask_h     = $mask_h;
        $this->Template->mask_x     = $mask_x;
        $this->Template->mask_y     = $mask_y;
        $this->Template->aspect     = floatval($version->picwidth / $version->picheight);
        $this->Template->version_id = $version->id;
        $this->Template->article_id = $designItem->article_id;
        $this->Template->config     = $this->config;
        $this->Template->book       = $book;
        
        
        if($designItem->item_image1_present > 0)
        {
            $upload_dir_frontend = $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] ? $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] : "/var/www/personalnovel/htdocs/tl_files/customer-images/";
                                
            $preview_folder_cache = "tl_files/customer-images/";
            $this->Template->isImageCached  = true;
            if(file_exists($upload_dir_frontend . $_GET["order"] . "-" . $designItem->id . ".png.org.png"))
            {
                $imageCached = $preview_folder_cache . $_GET["order"] . "-" . $designItem->id . ".png.org.png";
            }
            else if(file_exists($upload_dir_frontend . $_GET["order"] . "-" . $designItem->id . ".gif.org.gif"))
            {
                $imageCached = $preview_folder_cache . $_GET["order"] . "-" . $designItem->id . ".gif.org.gif";
            }
            else
            {
                $imageCached = $preview_folder_cache . $_GET["order"] . "-" . $designItem->id . ".jpg.org.jpg";
            }
            $this->Template->imageCached    = $imageCached;
            
        }
        }
        
        function step6()
        {
            $bookItem = new PnBookItem();
            $bookItem->initWithId($_GET["bookitem"]);
            $this->CheckImageServer($bookItem);
            $designItem = $bookItem->getDesignItem();
            $bookVersion   = $bookItem->getBookVersion();
            $bindings = PnCalendar::getCalendarBindings();
            $bookFields = json_decode($bookItem->fields);
            
            if($bindings[$bookVersion->binding] == 0){
                // redirect checkout, BoD don't accept
                $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout";
                
                if($GLOBALS['TL_CONFIG']['use_url_token'])
                {
                    $token = generateToken($_GET['order']);
                    $order = new PnOrder();
                    $order->initWithId($_GET['order']);
                    $order->edit(array("order_step" => "basket"));
                    header(sprintf("Location: %s?order=%s&token=%s", $checkout_page, $_GET["order"], $token));
                }
                else
                {
                    header(sprintf("Location: %s?order=%s", $checkout_page, $_GET["order"]));
                }
            }
            //Load cached addons
                $article = new PnArticle();
                $article->initWithId($bookItem->article_id);
                
                $addon_Lesezeichen = array();
                $addon_Stiftschlaufe = array();
                $addon_Gummiband = array();
                $addon_Froschtasche = array();
                
                $addon_Lesezeichen_current = '';
                $addon_Stiftschlaufe_current = '';
                $addon_Gummiband_current = '';
                $addon_Froschtasche_current = '';
                
                foreach($article->getAddons() as $addon_list)
                {
                   $nameValue = explode(":", $addon_list->name);
                   $classDiv = trim($nameValue[1]);
                   $valueDiv = $addon_list->id;
                   $priceDiv    = $addon_list->getPrice();
                   if($nameValue[0]=="Lesezeichen"){
                           $addon_Lesezeichen[] = array(
                                                        "classDiv" => sprintf("border_select %s",$classDiv),
                                                        "label" => "",
                                                        "value" => "$valueDiv",
                                                        "price" => "$priceDiv"
                                                        );
                   }
                   if($nameValue[0]=="Stiftschlaufe"){
                           $addon_Stiftschlaufe[] = array(
                                                        "classDiv" => sprintf("border_select %s",$classDiv),
                                                        "label" => "",
                                                        "value" => "$valueDiv",
                                                        "price" => "$priceDiv"
                                                        );
                   }
                   if($nameValue[0]=="Gummiband"){
                           $addon_Gummiband[] = array(
                                                        "classDiv" => sprintf("border_select %s",$classDiv),
                                                        "label" => "",
                                                        "value" => "$valueDiv",
                                                        "price" => "$priceDiv"
                                                        );
                   }
                   if($nameValue[0]=="Froschtasche"){
                           $addon_Froschtasche[] = array(
                                                        "classDiv" => sprintf("border_select %s",$classDiv),
                                                        "label" => "",
                                                        "value" => "$valueDiv",
                                                        "price" => "$priceDiv"
                                                        );
                   }
                }
                
                //add addon free
                $addon_Stiftschlaufe[] = array(
										"classDiv" => "border_select transparent",
										"label" => "",
										"value" => "NULL",
										"price" => "0.00"
										);
                $addon_Gummiband[] = array(
										"classDiv" => "border_select transparent",
										"label" => "",
										"value" => "NULL",
										"price" => "0.00"
										);
                $addon_Lesezeichen[] = array(
										"classDiv" => "border_select transparent",
										"label" => "",
										"value" => "NULL",
										"price" => "0.00"
										);
                $addon_Froschtasche[] = array(
										"classDiv" => "border_select transparent",
										"label" => "",
										"value" => "NULL",
										"price" => "0.00"
										);
                //curren addon checked
                
                $addon_cached = array();
                $design_article = new PnArticle();
                foreach($bookItem->getAddonItems() as $addon)
                {
                    $addon_cached[] = $addon->article_id;
                    $design_article->initWithId($addon->article_id);
                    $Array_select =  explode(":", $design_article->name);
                    if($Array_select[0]=="Lesezeichen")
                    {
                           $addon_Lesezeichen_current = $addon->article_id;
                           $addon_Lesezeichen_current2 =  str_replace("-","",$Array_select[1]);
                    }
                   if($Array_select[0]=="Stiftschlaufe")
                   {
                          $addon_Stiftschlaufe_current = $addon->article_id;
                          $addon_Stiftschlaufe_current2 =  str_replace("-","",$Array_select[1]);
                   }
                   if($Array_select[0]=="Gummiband")
                   {
                         $addon_Gummiband_current = $addon->article_id;
                         $addon_Gummiband_current2 =  str_replace("-","",$Array_select[1]);
                   }
                   if($Array_select[0]=="Froschtasche")
                   {
                         $addon_Froschtasche_current = $addon->article_id;
                   }
                }
                //total price addon
                $addon_price = 0;
                
                foreach($addon_cached as $cache)
                {
                        $tmp = new PnArticle();
                        $tmp->initWithId($cache);
                        $addon_price += $tmp->getPrice();
                }
                $book = new PnBook();
                $book->initWithId($bookItem->book_id);
                $coverItem = $bookItem->getDecocover();
                
                $this->Template->config                 = $this->config;
                $this->Template->book_id                = $bookItem->book_id;
                $this->Template->totalPrice     		= $bookItem->getTotalPrice();
                $this->Template->addon_price     		= $addon_price;
                $this->Template->Lesezeichen         	= json_encode($addon_Lesezeichen);
                $this->Template->Stiftschlaufe       	= json_encode($addon_Stiftschlaufe);
                $this->Template->Gummiband           	= json_encode($addon_Gummiband);
                $this->Template->Froschtasche           = json_encode($addon_Froschtasche);
                $this->Template->Lesezeichen_current2   = $addon_Lesezeichen_current2?$addon_Lesezeichen_current2:"transparent";
                $this->Template->Stiftschlaufe_current2 = $addon_Stiftschlaufe_current2?$addon_Stiftschlaufe_current2:"transparent";
                $this->Template->Gummiband_current2     = $addon_Gummiband_current2?$addon_Gummiband_current2:"transparent";
                
                $this->Template->Froschtasche_current   = $addon_Froschtasche_current;
                $this->Template->Lesezeichen_current    = $addon_Lesezeichen_current;
                $this->Template->Stiftschlaufe_current  = $addon_Stiftschlaufe_current;
                $this->Template->Gummiband_current      = $addon_Gummiband_current;
                
                if($bindings[$bookVersion->binding] == 2){
                        $colectColorDesign = PnCalendar::getCalendarColorsV1();
                        foreach($colectColorDesign as $k => $v){
                                foreach($v as $k2=>$v2){
                                        if($bookItem->covercolor_id == $v2['value']){
                                                $this->Template->previewImg = $v2['preview'];
                                                $this->Template->typecover      = "style-graegung";
                                                $this->Template->cover_select   = $bookItem->covercolor_id;
                                        }
                                }
                        }
                }
                else
                {
                        $colectColorDesign = PnCalendar::getCalendarColors();
                        
                        foreach($colectColorDesign as $k => $v){
                                foreach($v as $k2=>$v2){
                                        if($bookItem->getDecocover() == $v2['id']){
                                                if($bindings[$bookVersion->binding] == 1){
                                                        $this->Template->previewImg = $v2['preview2'];
                                                }
                                                else
                                                {
                                                        $this->Template->previewImg = $v2['preview1'];
                                                }
                                                if($k == 1 || $k == 3 || $k == 4)
                                                {
                                                        $this->Template->bookmask = 'NULL';
                                                }
                                                else 
                                                {
                                                        $this->Template->bookmask = 1;
                                                }
                                                $this->Template->typeUp         = $k;        
                                                $this->Template->typecover      = "style".$k;
                                                $this->Template->cover_select   = $bookItem->getDecocover();
                                                $this->Template->colorUp        = $v2['code'];
                                        }
                                }
                        }
                }
                
                $this->Template->buchtitel  		= "Kalender";
                $this->Template->notecover  		= $bookFields->note_order;
                $this->Template->yearcover  		= $bookFields->year_order;
                $this->Template->selected_binding 	= $bindings[$bookVersion->binding];
                $this->Template->subtitle   		= $designItem->subtitle;
                
                $this->Template->bookItem       	= $bookItem;
                $customize_page = $GLOBALS['TL_CONFIG']['customize_page_notebook'] ? $GLOBALS['TL_CONFIG']['customize_page_notebook'] : "/customize";
                $this->Template->preview        	= sprintf("%s?order=%s&bookitem=%s&step=%s", $customize_page, $bookItem->order_id, $bookItem->id, 5);
            
                $upload_dir_frontend = $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] ? $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] : "/var/www/personalnovel/htdocs/tl_files/customer-images/";
                $filename   = $upload_dir_frontend . $bookItem->order_id . "-" . $designItem->id . ".jpg";
                $filenameFrontend ="/tl_files/customer-images/" . $bookItem->order_id . "-" . $designItem->id . ".jpg"."?".time();
                    
                if(file_exists($filename)) 
                {
					$this->Template->previewImgUpload = $filenameFrontend;
                }
				else
				{
					$this->Template->previewImgUpload = "";
				}
        
        }
        
        function getWiddmungColor(){
                $widmung = new PnWidmung();
                return $widmung->getWidmungColorKalendar();
        }
        function getOrCreateOrder()
        {
            $order = new PnOrder();
            
            if($_GET['order'])
            {
                $order->initWithId($_GET['order']);
                
                if($order->id == "" || $order->id == 0)
                {
                    gotoErrorPage();
                }
            }
            else if($_COOKIE['PN_order_id'] != '')
                {
                        $order->initWithId($_COOKIE['PN_order_id']);
                }
                
                if($order->id == "" || $order->id == 0)
                {
                    $id = $order->add(
                                    array(
                                        "order_status"      => 0,
                                "order_date"        => date('Y/m/d H:i:s a', time()),
                                "order_created"     => date('Y/m/d H:i:s a', time()),
                                "order_updated"     => date('Y/m/d H:i:s a', time()),
                                "order_converted"   => 1,
                                "order_version"     => 4,
                                "order_invoice"     => 1,
                                "locale_id"         => 1,
                                "mandant_id"        => 1,
                                "konto_id"          => $order->createKonto()->id,
                                "debitor_id"        => 1,
                                "partner_id"        => 1,
                                        "payment_method_id" => 3
                            )
                                                );
                    
                    $order->initWithId($id);
                    $order->edit(array("konto_id" => $order->createKonto()->id));
                    $order->createRandomKey();
                    
                    setcookie('PN_order_id', $order->id, time() + (86400 * 7), "/",$this->domain);
                }
                
                return $order;
        }
        
        function createWidmungItem($order, $bookItem)
        {
            $item     = new PnItem();
            $item_id  = $item->add(
                       array(
                           "order_id"       => $order->id,
                           "class_id"       => 10,
                           "artikel_id"     => 8830,
                           "parent_id"      => $bookItem->id,
                           "item_pieces"    => 1,
                           "item_print"         => 0,
                           "item_testprint"     => 0,
                           "item_fields"        => '{"text": "", "fontface": "Century Schoolbook L", "fontcolor": "black"}',
                           "item_created"   => date('Y/m/d H:i:s a', time()),
                           "item_updated"   => date('Y/m/d H:i:s a', time()),
                           "item_touched"   => date('Y/m/d H:i:s a', time())
                       )
                       );
            
            $item->initWithId($item_id);
            return $item;
        }
        
        function createDesignItem($order, $bookItem)
        {
            $book    = new PnBook();
            $designs = $book->getBookDesigns($bookItem->article_id);
            
            if(count($designs) > 0)
                $design  = $designs[0];
            else
                gotoErrorPage();
            
            $item    = new PnDesignItem();
            $item_id = $item->add(
                        array(
                            "order_id"       => $order->id,
                            "class_id"       => 3,
                            "artikel_id"     => $design->artikel_id,
                            "parent_id"      => $bookItem->id,
                            "item_pieces"    => 1,
                            "item_print"         => 0,
                            "item_testprint" => 0,
                            "item_fields"        => '{}',
                            "item_created"   => date('Y/m/d H:i:s a', time()),
                            "item_updated"   => date('Y/m/d H:i:s a', time()),
                            "item_touched"   => date('Y/m/d H:i:s a', time())
                        )
                    );
        }
        
        function createBookItem($order, $book)
        {
            $versions = PnCalendar::getCalendarVersions();
            $version_default = $versions["130x190"]["Druck"][0];
            
            if($_POST["Notebooks"])
            {
                $version_default = $versions[$_POST["Notebooks"]][$_POST["Einband"]];
            }
            
            $bookArticle = new PnBookArticle();
            $bookArticle->initWithBookAndVersion($book->id, $version_default);
            
            $fields = array(
                        "Buchtitel" => "Kalender",
                        "font"      => "5",
                        "sections"  => array(
                                            "note" => array("note" => 461),
                                            "individual" => array("individual" => 401),
                                            "legend" => array("legend" => 409),
                                            "month" => array("month" => 405),
                                            "day" => array("day" => 413)
                                            ),
                        "Hauptperson_NN" => "Max Musterman",
                        "Hauptperson_VN" => "",
                        "calendar_event_categories" => array(),
                        "year_order"    => "",
                        "note_order"    => "",
                        "locale_id"		=>1
                        );
        
        $bookDocuments = $book->getBookDocuments();
        if(count($bookDocuments) > 0)
        {
            $bookDocument = $bookDocuments[0];
            $bookDocumentId = $bookDocument->id;
            
            if($_POST["document"])
            {
                foreach($bookDocuments as $doc)
                {
                    if($_POST["document"] == $doc->id)
                        $bookDocumentId = $_POST["document"];
                }
            }
        }
        
        $fields   = json_encode($fields);
        $bookItem = new PnBookItem();            
            $book_id  = $bookItem->add(
                              array(
                                    'order_id'                  => $order->id,
                                    'buch_id'                   => $book->id,
                                    'class_id'                  => 2,
                                    'artikel_id'                => $bookArticle->id,
                                    'font_id'                   => 5,
                                    'document_id'       => $bookDocumentId,
                                    'item_pieces'               => 1,
                                    'item_print'                => 0,
                                    'item_testprint'    => 0,
                                    'item_fields'               => $fields,
                                    'item_created'      => date('Y/m/d H:i:s a', time()),
                                    'item_updated'      => date('Y/m/d H:i:s a', time()),
                                    'item_touched'      => date('Y/m/d H:i:s a', time())
                              )
                      );
        
        $bookItem->initWithId($book_id);
        $bookItem->edit(array("item_fields" => $fields)); //Fix bug encoding. Don't know why?
        
        return $bookItem;
    }
    
    function saveCallBack()
    {
        $bookItem          = new PnBookItem();
        $bookItem->initWithId($_GET["bookitem"]); //Prevent cached
        
        $itemFields        = json_decode($bookItem->fields);
        $cachedIndividual  = $itemFields->sections->individual->individual;
        $cachedMonthDesign = $itemFields->sections->month->month;
        $cachedLegend      = $itemFields->sections->legend->legend;
        $cachedDayDesign   = $itemFields->sections->day->day;
        $cachedNoteDesign  = $itemFields->sections->note->note;
        
        $fixedIndividual   = $cachedIndividual;
        $fixedMonthDesign  = $cachedMonthDesign;
        $fixedLegend       = $cachedLegend;
        $fixedDayDesign    = $cachedDayDesign;
        $fixedNoteDesign   = $cachedNoteDesign;
        
        $bookVersion       = $bookItem->getBookVersion();
        $selectedSize      = PnCalendar::getSelectedCalendarSize($bookVersion->id);
        
        $decocover_id = $bookItem->getDecocover()?$bookItem->getDecocover():449;
        $bookDesign        = $bookItem->getDesignItem();
        $selectedDesign    = PnCalendar::getSelectedCalendarDesignId($bookDesign->article_id, $_GET["bookitem"],$decocover_id);
        
        $doublepages       = PnCalendar::getCalendarDoublePages();
        
        $fixedIndividual   = $doublepages[$selectedSize]["individual"];
        $fixedMonthDesign  = $doublepages[$selectedSize]["month"];
        $fixedLegend       = $doublepages[$selectedSize]["legend"];
        foreach($doublepages as $key_size => $value_size)
        {
            foreach($value_size["day"] as $key_design => $value_design)
            {
                foreach($value_design as $key_day_layout => $value_day_layout)
                {
                    if($value_day_layout == $cachedDayDesign)
                        $fixedDayDesign = $doublepages[$selectedSize]["day"][$selectedDesign][$key_day_layout];
                }
            }
            
            foreach($value_size["note"] as $key_note_layout => $value_note_layout)
            {
                if($value_note_layout == $cachedNoteDesign)
                    $fixedNoteDesign = $doublepages[$selectedSize]["note"][$key_note_layout];
            }
        }
        
        $itemFields->sections->individual->individual = $fixedIndividual;
        $itemFields->sections->month->month     = $fixedMonthDesign;
        $itemFields->sections->legend->legend   = $fixedLegend;
        $itemFields->sections->day->day         = $fixedDayDesign;
        $itemFields->sections->note->note       = $fixedNoteDesign;
        
        $bookItem->edit(array("item_fields" => json_encode($itemFields)));
    }
    public function getImageUrl($id,$cover, $etx, $url_img) {
        $title = "kalendar-cover";
		if(!in_array($etx, array('thumbs','full'))){
				$etx = "detail";
		}
		$check = true;
		$pageURL = 'http';
		
		if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}}
		
		$pageURL .= "://";
		$file_image = TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$cover."-".$etx.".png";
		
		if(!is_file($file_image)){
				try{
				$file_headers = @get_headers($url_img);
				if($file_headers[0] != 'HTTP/1.1 404 Not Found' and $file_headers[0] != 'HTTP/1.1 500 INTERNAL SERVER ERROR')
						copy($url_img, TL_ROOT."/tl_files/thumbnail-book/".$title."-".$id."-".$cover."-".$etx.".png");
						else{
								$check = false;
						}
				$this->flaLimit = $this->flaLimit + 1;
				}catch (Exception $e) {
						$check = false;
				}
		}
		if($check != false )
		$url_img = $pageURL.$_SERVER["SERVER_NAME"]."/tl_files/thumbnail-book/".$title."-".$id."-".$cover."-".$etx.".png";
		
		return $url_img;
    }
	public function CheckImageServer($bookItem)
	{
		$decocovers  = PnCalendar::getCalendarColors();
		$type = 1;
		$design_select = $bookItem->getDecocover();
		foreach($decocovers as $k => $v)
		{
				foreach($v as $k2 => $v2){
						if($design_select == $v2['id'])
								$type = $k;
				}
		}
		if($type != 4)
		{
			$upload_dir = $GLOBALS['TL_CONFIG']['customer_images_dir'] ? $GLOBALS['TL_CONFIG']['customer_images_dir'] : "/var/www/production/shared/customer-images/";
			$upload_dir_frontend = $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] ? $GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] : "/var/www/personalnovel/htdocs/tl_files/customer-images/";
		
			$designItems = $bookItem->getDesignItem();
			$design_id = $designItems->id;
			$order_id = $bookItem->order_id;
			$designItem = new PnDesignItem();
			$designItem->initWithId($design_id);
			$designItem->editImageCoverNull();
			//Customer images
			$cachefiles = glob($upload_dir . $order_id . "-" . $design_id . "*");
			array_walk($cachefiles, function ($cachefile) {
					unlink($cachefile);
			});
			
			//Customer frontend images
			if(file_exists($upload_dir_frontend . $order_id . "-" . $design_id . "jpg"))
				unlink($upload_dir_frontend . $order_id . "-" . $design_id . ".jpg");
			
			$cachefiles = glob($upload_dir_frontend . $order_id . "-" . $design_id . "*");
			array_walk($cachefiles, function ($cachefile) {
					unlink($cachefile);
			});  
		}
    }
	public function deleteCacheImage()
	{
		//delete cache preview bookItem
		$preview_cache = $GLOBALS['TL_CONFIG']['preview_cache'] ? $GLOBALS['TL_CONFIG']['preview_cache']
							: "/var/www/Staging_Backend_Personalnovel/production/shared/cache/preview/";
		if(file_exists($preview_cache . $_GET['bookitem'] . '-' . $_GET['order'] . "-u1.png"))
		{
			unlink($preview_cache . $_GET['bookitem'] . '-' . $_GET['order'] . "-u1.png");
		}
	}
	public function setCookieLayout($value,$param=0)
	{
		$domain_cookie = $GLOBALS['TL_CONFIG']['cookie_domain']	? $GLOBALS['TL_CONFIG']['cookie_domain'] : "personalnovel.local";
		if($param==1){
			setcookie('orderLayoutKalender', $value, time() + (86400 * 3), "/",$domain_cookie);
		}
		else{
			if($_COOKIE['orderLayoutKalender']){
				setcookie ("orderLayoutKalender", "", time() - 3600, "/", $domain_cookie);
			}
		}
	}

}

?>