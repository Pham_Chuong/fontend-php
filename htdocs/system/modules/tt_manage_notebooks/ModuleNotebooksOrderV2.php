<?php 
class ModuleNotebooksOrderV2 extends Module
{
    protected $strTemplate          = 'mod_notebooks_order_v2';
    
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
                $objTemplate = new BackendTemplate('be_wildcard');

                $objTemplate->wildcard = '### ORDER STEPS NOTEBOOKS ###';
                $objTemplate->title = $this->headline;
                $objTemplate->id = $this->id;
                $objTemplate->link = $this->name;
                $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

                return $objTemplate->parse();
        }
        
        if(TL_MODE == 'FE')
        {
            $GLOBALS['TL_CSS'][] = '/system/modules/tt_manage_notebooks/html/css/wow_book.css';
            $GLOBALS['TL_CSS'][] = '/system/modules/tt_manage_notebooks/html/css/wedding_album.css';
	
            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_books/html/js/jquery-1.9.1.min.js';
            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/notizbuch.js';
            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/wow_book.min.js';
	
            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/calendar.datakeeper.js';
            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/calendar.tree.js';
            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/jquery.colorbox.js';
            $GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_manage_notebooks/html/js/jquery.uploadfile.min.js';
    
        }
        
        return parent::generate();
    }
    
    protected function compile()
    {
        
    }
}
