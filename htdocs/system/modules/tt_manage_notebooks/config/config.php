<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

array_insert($GLOBALS['FE_MOD'], 0, array(
	"mod_manage_notebooks" => array(
		"notebooks_order_v2"			=> "ModuleNotebooksOrderV2",
	),
));

array_insert($GLOBALS['FE_MOD'], 0, array(
	"mod_manage_notebooks" => array(
		"notebooks_order"			=> "ModuleNotebooksOrder",
	),
));

/**
 * Content elements
 */
//$GLOBALS['TL_CTE']['includes']['pnovelmodule'] = 'PersonalnovelContentModule';

$GLOBALS['TL_CONFIG']['site_secret'] = 'nilfEvdupjevOdMacKihikGedTymFemhebliesfabOjibyekew';

$GLOBALS['TL_CONFIG']['disableRefererCheck'] = true;

//relpace in filter spanel
$GLOBALS['TL_CONFIG']['bis'] = ' - ';
$GLOBALS['TL_CONFIG']['ja'] = ' Mit Liebesgeschichte ';
$GLOBALS['TL_CONFIG']['nein'] = ' Ohne Liebesgeschichte ';
$GLOBALS['TL_CONFIG']['ihn'] = ' Für Männer ';
$GLOBALS['TL_CONFIG']['sie'] = '  Für Frauen ';
$GLOBALS['TL_CONFIG']['error_page'] = '/';
$GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN'] = 50;
$GLOBALS['TL_CONFIG']['customer_images_dir'] = "/var/www/Personalnovel/htdocs/tl_files/customer-images/";
$GLOBALS['TL_CONFIG']['customer_images_dir_frontend'] = "/var/www/Personalnovel/htdocs/tl_files/customer-images/";
$GLOBALS['TL_CONFIG']['image_root'] = "http://pnovel.local/";//"http://live-backend.personalnovel.net/";//"http://pnovel.local/";//
$GLOBALS['TL_CONFIG']['TESTORDER_ID_START'] = 1000;
$GLOBALS['TL_CONFIG']['customize_page_notebook'] = '/customize-notebooks';


$GLOBALS['TL_CONFIG']['order_steps'] = array
(
    'felder'    => 1,
    'widmung'   => 2,
    'version'   => 3,
    'design'    => 4,
    'font'      => 5,
    'addons'    => 6,
    'basket'    => 11,
    'order'     => 12,
    'shipping'  => 13,
    'payment'   => 14,
    'confirm'   => 15,
    'done'      => 16
);



?>
