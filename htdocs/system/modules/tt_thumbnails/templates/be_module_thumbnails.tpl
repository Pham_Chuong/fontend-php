<!-- start -->
<div class="tt_thumbnails">
	
	<div class="tt_search_box">
		<form method="get">
			<label><?php echo $this->txt_search_id ?> </label><br />
			<input type='hidden' name='do' value='<?php echo $_GET['do']?$_GET['do']:'' ?>'>
			<input type='text' name='tt_id_thumbnail' value='<?php echo $_GET['tt_id_thumbnail']?$_GET['tt_id_thumbnail']:'' ?>' class='tt_id_thumbnail'>
			<button type='submit' class='tt_submit_id_thumbnail'><?php echo $this->txt_search ?></button> 
		</form>
	</div>
	
	<div class="tt_show_thumbnails">
		<?php if($this->datathumbnail): ?>
		<form method="POST">
		<table class='tt_datathumbnail'>
			<tr>
				<th><?php echo $this->txt_image ?></th>
				<th><?php echo $this->txt_name_image ?></td>
				<th style="text-align:right"><?php echo $this->txt_select_delete ?></th>
			</tr>
			<?php foreach($this->datathumbnail as $img): ?>
				
				<tr>
				<td><img width='100' src="<?php echo $this->path_img.$img ?>" class='tt_thumbnail_img'></td>
				<td><?php echo $img ?></td>
				<td style="text-align:right"><input type="checkbox" name="array_img[]" value="<?php echo $img ?>"></td>
				</tr>
				
			<?php endforeach; ?>
			<tr>
				<td colspan='3' class="delete_thumbnail">
					<input type='hidden' name='do' value='<?php echo $_GET['do']?$_GET['do']:'' ?>'>
					<input type='hidden' name='delete' value='delete'>
					<button type='submit' class='tt_submit_delete_thumbnail'><?php echo $this->txt_delete ?></button> 
				</td>
			</tr>
		</table>
		</form>	
		<?php else: ?>
			<?php if($this->done): ?>
				<span class="notdata"><?php echo $this->txt_deletedone ?></span>
			<?php else:?>
				<span class="notdata"><?php echo $this->txt_notthumbnail ?></span>
			<?php endif ?>
		<?php endif ?>
	</div>
</div>
<!-- end -->