<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
class ModuleThumbnails extends BackendModule
{

	protected $strTemplate = 'be_module_thumbnails';
	public    $path_folder_thumnails = "tl_files/thumbnail-book/";
	protected function compile()
	{
		$this->loadLanguageFile('tt_thumbnails');
		$this->Template->txt_search = $GLOBALS['TL_LANG']['tt_thumbnails']['search'];
		$this->Template->txt_search_id = $GLOBALS['TL_LANG']['tt_thumbnails']['search_id'];
		$this->Template->txt_image = $GLOBALS['TL_LANG']['tt_thumbnails']['image'];
		$this->Template->txt_name_image = $GLOBALS['TL_LANG']['tt_thumbnails']['name_image'];
		$this->Template->txt_select_delete = $GLOBALS['TL_LANG']['tt_thumbnails']['select_delete'];
		$this->Template->txt_delete = $GLOBALS['TL_LANG']['tt_thumbnails']['delete'];
		$this->Template->txt_notthumbnail = $GLOBALS['TL_LANG']['tt_thumbnails']['notthumbnail'];
		$this->Template->txt_deletedone = $GLOBALS['TL_LANG']['tt_thumbnails']['deletedone'];
		
		$done = false;
		if($_POST['delete'])
		{
			$array_img = $_POST['array_img'] ? $_POST['array_img'] : array();
			$this->deleteThumbnail($array_img);
			$done 					= true;
			$this->Template->done 	= "ok";
		}
		
		if($_GET['tt_id_thumbnail'] && $done==false){
			$id_book = $_GET['tt_id_thumbnail'];
			$array_img = array();
			
			if (is_dir(TL_ROOT . '/' . $this->path_folder_thumnails))
			{
				$subfiles = scan(TL_ROOT . '/' . $this->path_folder_thumnails);
				$fla	  =true;
				if(count($subfiles) > 0){

					foreach ($subfiles as $thisfile){
						if(strpos( $thisfile, "".$id_book."" ) > 1){
							$array_img[] = $thisfile;
						}
					}
					
				}
					
			}
			if( $fla ){
				$this->Template->path_img			= '/' . $this->path_folder_thumnails;
				$this->Template->datathumbnail 		= $array_img;
				
			}else{
				echo "Please config path folder !";
			}
		}
		
	}
	function deleteThumbnail($array_img=array()){
		foreach($array_img as $img){
			if(is_file(TL_ROOT . '/' . $this->path_folder_thumnails.$img))
				unlink(TL_ROOT . '/' . $this->path_folder_thumnails.$img);
		}
	}
	 
}
?>