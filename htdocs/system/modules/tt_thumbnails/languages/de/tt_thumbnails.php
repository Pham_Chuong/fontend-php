<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['search'] 			= "Suche Vorschaubild";
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['search_id'] 		= "Input ID Suche Buch:";
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['image'] 			= "Bilder";
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['name_image'] 		= "Name oder Bilder";
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['select_delete'] 	= "W&auml;hlen Sie Delete";
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['delete'] 			= "Cache l&ouml;schen Thumbnail";
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['notthumbnail'] 	= "Nicht Thumbnail";
$GLOBALS ['TL_LANG'] ['tt_thumbnails'] ['deletedone'] 		= "L&ouml;schen Thumbnail getan!";
?>