<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
$GLOBALS['TL_LANG']['tt_thumbnails']['search']			="Search Thumbnail";
$GLOBALS['TL_LANG']['tt_thumbnails']['search_id']		="Input Search ID Book :";
$GLOBALS['TL_LANG']['tt_thumbnails']['image']			="Images";
$GLOBALS['TL_LANG']['tt_thumbnails']['name_image']		="Name Images";
$GLOBALS['TL_LANG']['tt_thumbnails']['select_delete']	="Select Delete";
$GLOBALS['TL_LANG']['tt_thumbnails']['delete']			="Delete Cache Thumbnail";
$GLOBALS['TL_LANG']['tt_thumbnails']['notthumbnail']	="Not thumbnail !";
$GLOBALS['TL_LANG']['tt_thumbnails']['deletedone']		="Delete thumbnail done !";
?>