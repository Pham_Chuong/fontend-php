<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
// Back end module
	$GLOBALS['BE_MOD']['system']['tt_thumbnails'] = array
	  (
	  	'callback'		=> 'ModuleThumbnails',
	    'icon'   		=> 'system/themes/default/images/delete.gif',
	  	'stylesheet'	=> 'system/modules/tt_thumbnails/html/css_delete.css'
	  );
	
?>