<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazonService.config.inc.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Interface.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Model.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Exception.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/RequestException.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Client.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/MerchantValues.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/CBAPurchaseContract.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Model/ItemList.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Model/PurchaseItem.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Model/Price.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Model/SetPurchaseItemsRequest.php'));
require_once(realpath(dirname(__FILE__) . '/CheckoutByAmazon/Service/Model/CompletePurchaseContractRequest.php'));

class AmazonCheckoutServiceElement extends ContentElement
{
    protected $strTemplate = 'ce_amazon_checkout';
    protected $config;
    
    public function generate()
	{
	    if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
			$objTemplate->wildcard = '### Amazon Checkout Service Element ###';
			$objTemplate->title = 'Amazon Checkout Service Element';
			return $objTemplate->parse();
		}
		
		if(TL_MODE == 'FE')
		{
		    $this->config = new PnConfig();
		}
		
		return parent::generate();
	}
	
	protected function compile()
	{
	    $purchaseContractId = $_GET["sesson"];
	    
	    $lib = new CheckoutByAmazon_Service_CBAPurchaseContract();
	    $itemList = new CheckoutByAmazon_Service_Model_ItemList();
	    
	    $this->Template->address = "";
	}
}