<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

$GLOBALS['TL_CTE']['pnovel_element']['amazon_checkout_service'] = 'AmazonCheckoutServiceElement';

$GLOBALS['TL_CONFIG']['use_amazon_payment_service'] = false;
$GLOBALS['TL_CONFIG']['use_amazon_payment_sandbox'] = false;
$GLOBALS['TL_CONFIG']['amazon_payment_merchant_id'] = 'A3LK15BEHXD69X';
