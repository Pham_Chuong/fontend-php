<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

$GLOBALS['TL_DCA']['tl_module']['palettes']['book_listing'] 		= '{title_legend},name,type,perPage,pn_type,pn_image_height,pn_image_shadow,pn_template,{redirect_legend},jumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_filter_panel'] 	= '{title_legend},name,headline,type';
$GLOBALS['TL_DCA']['tl_module']['palettes']['wine_listing']			= '{title_legend},name,type,perPage';
$GLOBALS['TL_DCA']['tl_module']['palettes']['book_genre']           = '{title_legend},name,type,headline,pn_image_height,pn_image_shadow,pn_genreid,pn_booktitle,{redirect_legend},jumpTo';

/*
 * new fields
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['pn_type'] = array
(
	'label'                 => &$GLOBALS['TL_LANG']['tl_module']['pn_type'],
	'inputType'             => 'select',
	'options_callback'      => array('tl_module_book_listing', 'getPnType'),
	'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_booklang'] = array(
	'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_booklang'],
	'inputType'             => 'select',
	'options_callback'      => array('tl_module_book_listing', 'getPnLanguage'),
	'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_image_height'] = array(
		'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_image_height'],
		'inputType'             => 'text',
		'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_image_shadow'] = array(
		'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_image_shadow'],
		'inputType'             => 'text',
		'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_template'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_module']['pn_template'],
		'default'                 => 'mod_book_listing',
		'exclude'                 => true,
		'inputType'               => 'select',
		'options_callback'        => array('tl_module_book_listing', 'getBooksListingTemplates'),
		'eval'                    => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_genreid'] = array(
		'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_genreid'],
		'inputType'             => 'text',
		'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_module']['fields']['pn_booktitle'] = array(
		'label'					=> &$GLOBALS['TL_LANG']['tl_module']['pn_booktitle'],
		'inputType'             => 'text',
		'eval'                  => array('mandatory'=>true, 'tl_class'=>'w50')
);

class tl_module_book_listing extends Backend
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function getPnType()
	{
		/*
			Romane = constructor(type_id=1)
			Bilderbuecher = constructor(type_id=2)
			FrecheMaedchen = constructor(type_id=3)
			Cora = constructor(type_id=4)
			ThienemannBuchpiraten = constructor(type_id=5)
			PrivateNOTE = constructor(type_id=6)
			ThienemannBuchpiratenJugendbuecher = constructor(type_id=7)
			PersonalBiz = constructor(type_id=8)
			CoraAndPersonalNovel = constructor(type_id=9)
			NovelOnDemand = constructor(type_id=10)
			Geschenkbuch = constructor(type_id=11)
			Geschenkbuch22x22 = constructor(type_id=12)
			Kinderbuch19x23 = constructor(type_id=13)
			MusterdruckRomane = constructor(type_id=14)
			Klassiker = constructor(type_id=15)
			StatischesCover = constructor(type_id=16)
			Montana = constructor(type_id=17)
			ClassicNovels = constructor(type_id=18)
			SkalierteRomane = constructor(type_id=20)
			PersonalPockets = constructor(type_id=21)
			StatischePDFs = constructor(type_id=22)
			CameoClassicNovels = constructor(type_id=23)
			
			# Type-ID-HACKs zentralisiert, um sie irgendwann
			# in eine bessere Abstraktion umzuwandeln.
			ROMANOID_TYPE_IDS = frozenset([1, 3, 4, 5, 7, 9, 14, 15, 16, 17, 18, 19,
			                               20, 21, 22, 23])
			KINDERBUCH_TYPE_IDS = frozenset([2, 13])
			JUGENDBUCH_TYPE_IDS = frozenset([3, 5, 7])
			GESCHENKBUCH_TYPE_IDS = frozenset([11, 12])
			KLASSIKER_TYPE_IDS = frozenset([15, 18, 23])
			
			NO_PHOTO_UPGRADE_TYPE_IDS = frozenset([3, 4, 5, 7, 9, 15])
			
			# Types, deren B�cher das ProofBook unterst�tzen
			# (Weltbild und Montana haben keine Taschenb�cher)
			PROOFABLE_TYPE_IDS = ROMANOID_TYPE_IDS - frozenset([16, 17])
			
			EBOOKABLE_TYPE_IDS = ROMANOID_TYPE_IDS - set([18, 23])
			
			# Types, welche die Widmung in Handschrift und Farbe unterst�tzen
			FANCY_WIDMUNG_TYPE_IDS = ROMANOID_TYPE_IDS | GESCHENKBUCH_TYPE_IDS
		 */
		
		return array(
			"1"	=> $GLOBALS['TL_LANG']['tl_module']['pn_type']['novels'],
			"2"	=> $GLOBALS['TL_LANG']['tl_module']['pn_type']['children_books'],
			"3" => $GLOBALS['TL_LANG']['tl_module']['pn_type']['gift_books'],
			"4" => $GLOBALS['TL_LANG']['tl_module']['pn_type']['classic_literature'],
			"5" => $GLOBALS['TL_LANG']['tl_module']['pn_type']['e_books']
		);
	}
	
	public function getPnLanguage()
	{
		return array(
			'de' 	=> "DE",
			'en' 	=> "EN",
			'bo'	=> "Both"
		);
	}
	
	public function getBooksListingTemplates(DataContainer $dc)
	{
		$intPid = $dc->activeRecord->pid;
	
		if ($this->Input->get('act') == 'overrideAll')
		{
			$intPid = $this->Input->get('id');
		}
	
		return $this->getTemplateGroup('mod_book_listing', $intPid);
	}
}

?>