<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

require_once("class/class.pn.php");
require_once("class/class.verhoeff.php");

class ModuleBookCheckout extends Module
{
	protected $strTemplate = 'mod_book_checkout';
	
	public function generate()
	{
	    $this->config = new PnConfig();
	    
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');
	
			$objTemplate->wildcard = '### BOOK CHECKOUT ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	
			return $objTemplate->parse();
		}
	
		if(TL_MODE == 'FE')
		{
			$GLOBALS['TL_CSS'][] 		= '/system/modules/tt_manage_books/html/css/checkout.css|screen';
				
			$GLOBALS['TL_JAVASCRIPT'][] = '/system/modules/tt_personalnovel/html/js/jquery-1.4.1.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/jquery-ui-1.8.16.custom.min.js';
			$GLOBALS['TL_JAVASCRIPT'][]	= '/system/modules/tt_manage_books/html/js/checkout.js';
		}
	
		return parent::generate();
	}
	
	protected function compile()
	{
	    $customize_page = $GLOBALS['TL_CONFIG']['customize_page'] ? $GLOBALS['TL_CONFIG']['customize_page'] : "/customize.html";
	    $checkout_page  = $GLOBALS['TL_CONFIG']['checkout_page'] ? $GLOBALS['TL_CONFIG']['checkout_page'] : "/checkout.html";
	    
	    $step = $_GET['step'] ? $_GET['step'] : $_POST['step'];
	    $step = $step ? $step : 1;
		$this->Template->step = $step;
		
		$post_order = $_POST['order'] ? $_POST['order'] : $_COOKIE['PN_order_id'];
		$order_id = $_GET['order'] ? $_GET['order'] : $post_order;
		
		$order = new PnOrder();
		$order->initWithId($order_id);
		
		/*
	     * goto MyOrderPage if order's done
	     */
	    if($order->id)
	    {
	        $myorder_page = $GLOBALS['TL_CONFIG']['myorder_page'] ? $GLOBALS['TL_CONFIG']['myorder_page'] : "/myorder.html";
	        if($order->step == "done")
	        {
	            header(sprintf("Location: %s?id=%s", $myorder_page, $_GET["order"]));
	        }
	    }
		
		if($step == 1)
		{
			if($_GET['action'] == "remove")
			{
			    if($_GET['bookitem'])
			    {
                    $item_id = $_GET['bookitem'];
                    $bookItem = new PnBookItem();
                    $bookItem->initWithId($item_id);
                    $bookItem->removeChildItem();
                    $bookItem->remove();
				}
				
				if($_GET['wineitem'])
				{
				    $item_id = $_GET['wineitem'];
                    $wineItem = new PnWineItem();
                    $wineItem->initWithId($item_id);
                    $wineItem->removeChildItem();
                    $wineItem->remove();
				}
				
				header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, 1));
			}
			
			if($_POST)
			{
				foreach($_POST as $k => $v)
				{
					if($this->config->startsWith($k, 'item_pieces_'))
					{
						$item_id = str_replace('item_pieces_', '', $k);
						$item = new PnItem();
						
						$item->select($item_id);
						$item->edit(array("item_pieces" => $v));
					}
				}
				
				header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, 2));
			}
			
			//Get Order Items
			$this->Template->bookItems   = $order->getBookItems();
			$this->Template->wineItems   = $order->getWineItems();
			$this->Template->shipping_id = $order->shipping_id;
			if($order->shipping_id && $order->shipping_id > 0)
			{
			    $shipping = new PnShipping();
			    $shipping->initWithId($order->shipping_id);
			    $this->Template->shipping = $shipping;
			}
			$this->Template->totalPrice  = $order->getTotalPrice("", false); //Sum without shipping price
			$this->Template->order       = $order;
		}
		
		if($step == 2)
		{
			if($_POST['step'] == 2)
			{
			    $error = false;
			    
			    //Validate Email
			    if(!$_POST['customer-customer_email'] || preg_match("/[.+a-zA-Z0-9_-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $_POST['customer-customer_email']) == 0)
			    {
			        $error = true;
			        $this->Template->errorEmail = true;
			        $this->Template->email = $_POST['customer-customer_email'];
			    }
			    else
			    {
			        $this->Template->email = $_POST['customer-customer_email'];
			    }
			    
			    //Validate Vorname
			    if(!$_POST['shipping_address-address_vorname'])
			    {
			        $error = true;
			        $this->Template->errorFirstname = true;
			    }
			    else
			    {
			        if(preg_match('/[^A-Za-z0-9.\-öÖüÜäÄß\ ]/i', $_POST['shipping_address-address_vorname']))
			        {
			            $error = true;
			            $this->Template->invalidFirstname = true;
			        }
			        else
			        {
			            $this->Template->firstName = $_POST['shipping_address-address_vorname'];
			        }
			    }
			    
			    //Validate Nachname
			    if(!$_POST['shipping_address-address_nachname'])
			    {
			        $error = true;
			        $this->Template->errorLastname = true;
			    }
			    else
			    {
			        if(preg_match('/[^A-Za-z0-9.\-öÖüÜäÄß\ ]/i', $_POST['shipping_address-address_nachname']))
			        {
			            $error = true;
			            $this->Template->invalidLastname = true;
			        }
			        else
			        {
			            $this->Template->lastName = $_POST['shipping_address-address_nachname'];
			        }
			    }
			    
			    //Validate Street
			    if(!$_POST['shipping_address-address_address'])
			    {
			        $error = true;
			        $this->Template->errorAddress = true;
			    }
			    else
			    {
			        if(preg_match('/[^A-Za-z0-9.\-öÖüÜäÄß\ ]/i', $_POST['shipping_address-address_address']))
			        {
			            $error = true;
			            $this->Template->invalidAddress = true;
			        }
			        else
			        {
			            $this->Template->address = $_POST['shipping_address-address_address'];
			        }
			    }
			    
			    //Validate plz
			    if(!$_POST['shipping_address-address_zip'])
			    {
			        $error = true;
			        $this->Template->errorZip = true;
			    }
			    else
			    {
			        $this->Template->zip = $_POST['shipping_address-address_zip'];
			    }
			    
			    //Validate ort
			    if(!$_POST['shipping_address-address_city'])
			    {
			        $error = true;
			        $this->Template->errorCity = true;
			    }
			    else
			    {
			        if(preg_match('/[^A-Za-z0-9.\-öÖüÜäÄß\ ]/i', $_POST['shipping_address-address_city']))
			        {
			            $error = true;
			            $this->Template->invalidCity = true;
			        }
			        else
			        {
			            $this->Template->city = $_POST['shipping_address-address_city'];
			        }
			    }
			    
			    if(preg_match('/[^A-Za-z0-9.\-öÖüÜäÄß\ ]/i', $_POST['shipping_address-address_firma']))
                {
                    $error = true;
                    $this->Template->invalidCompany = true;
                }
			    
			    //Validate land
			    if($_POST['shipping_address-country'] == "__None")
			    {
			        $error = true;
			        $this->Template->errorCountry = true;
			    }
			    else
			    {
			        if($_POST['shipping_address-address_zip'])
			        {
			            $zipstr = $_POST['shipping_address-address_zip'];
			            $zipreg = $GLOBALS['TL_CONFIG']['zipcode_pattern'];
			            $country = new PnCountry();
			            $country->initWithId($_POST['shipping_address-country']);
			            $pattern = $zipreg[$country->code];
			            
			            if(!preg_match("/^" . $pattern . "$/i", $zipstr))
			            {
			                $error = true;
			                $this->Template->invalidZipcode = true;
			            }
			        }
			        
			        $this->Template->country = $_POST['shipping_address-country'];
			    }
			    
			    if(!$error)
			    {
			        $order->update(array('payment_method_id' => $_POST['payment_method']));
			        $email 			= $_POST['customer-customer_email'];
					$anrede 		= $_POST['shipping_address-address_anrede'];
					$firstName 		= $_POST['shipping_address-address_vorname'];
					$lastName 		= $_POST['shipping_address-address_nachname'];
					$company 		= $_POST['shipping_address-address_firma'];
					$address 		= $_POST['shipping_address-address_address'];
					$zip 			= $_POST['shipping_address-address_zip'];
					$city 			= $_POST['shipping_address-address_city'];
					$country_id 	= $_POST['shipping_address-country'];
					
					$customer = new PnCustomer();
					if($order->customer_id != '' && $order->customer_id > 0)
					{
						$customer_id = $order->customer_id;
					}
					else
					{
						if($customer->existEmail($email))
						{
							$customer_id = $customer->id;
						}
						else
						{
							$customer_id = $customer->add(array(
									"customer_vorname" 	=> $firstName,
									"customer_nachname"	=> $lastName,
									"customer_email" 	=> $email,
									"customer_anrede" 	=> $anrede
								));
						}
					}
					
					$objAddress = new PnAddress();
					if($order->shipping_address_id != '' && $order->shipping_address_id > 0)
					{
						//edit
						$shipping = new PnAddress();
						$shipping->initWithId($order->shipping_address_id);
						
						$shipping->edit(array(
								"address_vorname" 		=> $firstName,
								"address_nachname" 		=> $lastName,
								"address_firma" 		=> $company,
								"address_address" 		=> $address,
								"address_city" 			=> $city,
								"address_zip" 			=> $zip,
								"country_id" 			=> $country_id,
								"address_anrede" 		=> $anrede
							));
						
						if($_POST["billing_address-shipping_is_billing_address_"])
						{
						    $order->update(array(
						        "billing_address_id"	=> $order->shipping_address_id
							));
						}
						else
						{
						    $objBillingAddress = new PnAddress();
						    if($order->billing_address_id && $order->billing_address_id > 0 
						        && $order->billing_address_id != $order->shipping_address_id)
						    {
						        $objBillingAddress->initWithId($order->billing_address_id);
						        $objBillingAddress->edit(array(
                                    "address_vorname" 		=> $_POST["billing_address-address_vorname"],
                                    "address_nachname" 		=> $_POST["billing_address-address_nachname"],
                                    "address_firma" 		=> $_POST["billing_address-address_firma"],
                                    "address_address" 		=> $_POST["billing_address-address_address"],
                                    "address_city" 			=> $_POST["billing_address-address_city"],
                                    "address_zip" 			=> $_POST["billing_address-address_zip"],
                                    "country_id" 			=> $_POST["billing_address-country"],
                                    "address_anrede" 		=> $_POST["billing_address-address_anrede"]
                                ));
						    }
						    else
						    {
                                $billing_id = $objBillingAddress->add(array(
                                    "address_vorname" 		=> $_POST["billing_address-address_vorname"],
                                    "address_nachname" 		=> $_POST["billing_address-address_nachname"],
                                    "address_firma" 		=> $_POST["billing_address-address_firma"],
                                    "address_address" 		=> $_POST["billing_address-address_address"],
                                    "address_city" 			=> $_POST["billing_address-address_city"],
                                    "address_zip" 			=> $_POST["billing_address-address_zip"],
                                    "country_id" 			=> $_POST["billing_address-country"],
                                    "address_anrede" 		=> $_POST["billing_address-address_anrede"]
                                ));
                                
                                $order->update(array(
                                    "billing_address_id"	=> $billing_id
                                ));
                            }
						}
					}
					else
					{
						//add
						$shipping_id = $objAddress->add(array(
								"address_vorname" 		=> $firstName,
								"address_nachname" 		=> $lastName,
								"address_firma" 		=> $company,
								"address_address" 		=> $address,
								"address_city" 			=> $city,
								"address_zip" 			=> $zip,
								"country_id" 			=> $country_id,
								"address_anrede" 		=> $anrede
							));
						
						if($_POST["billing_address-shipping_is_billing_address_"])
						{
						    $billing_id = $shipping_id;
						}
						else
						{
						    $objBillingAddress = new PnAddress();
						    $billing_id = $objBillingAddress->add(array(
						        "address_vorname" 		=> $_POST["billing_address-address_vorname"],
								"address_nachname" 		=> $_POST["billing_address-address_nachname"],
								"address_firma" 		=> $_POST["billing_address-address_firma"],
								"address_address" 		=> $_POST["billing_address-address_address"],
								"address_city" 			=> $_POST["billing_address-address_city"],
								"address_zip" 			=> $_POST["billing_address-address_zip"],
								"country_id" 			=> $_POST["billing_address-country"],
								"address_anrede" 		=> $_POST["billing_address-address_anrede"]
                            ));
						}
						
						$order->update(array(
							"shipping_address_id" 	=> $shipping_id,
							"billing_address_id"	=> $billing_id,
							"customer_id" 			=> $customer_id
						));
						
					}
					
					/*
					 * prepare shipping options
					 */
                    $shipOptions = array();
                    $shippingOption = new PnShipping();
                    $shippingAddr = new PnAddress();
                    $shippingAddr->initWithId($shipping_id);
                    foreach($shippingOption->get() as $shipOpt)
                    {
                        if($shipOpt->id == 1)
                        {
                            if($shippingAddr->country_id == 1)
                            {
                                $shipOptions[] = $shipOpt->id;
                            }   
                        }
                        else
                        {
                            $shipOptions[] = $shipOpt->id;
                        }
                    }
                    
                    if(!in_array($order->shipping_address_id, $shipOptions))
                    {
                        $order->update(array("shipping_id" => $shipOptions[0]));
                    }
                    
                    $order->update(array("order_found" => $_POST["order_found"] ));
                    
                    header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, 3));
			    }
			}
			
			if($order->customer_id && $order->customer_id > 0)
			{
			    $customer = new PnCustomer();
			    $customer->initWithId($order->customer_id);
			    $this->Template->email = $customer->email;
			}
			
			//Load cached info
            if($order->shipping_address_id && $order->shipping_address_id > 0)
            {
                $cached_shipping = new PnAddress();
                $cached_shipping->initWithId($order->shipping_address_id);
                
                $this->Template->shipping_address_id = $cached_shipping->id;
                $this->Template->firstName           = $cached_shipping->firstName;
                $this->Template->lastName            = $cached_shipping->lastName;
                $this->Template->firma               = $cached_shipping->company;
                $this->Template->address             = $cached_shipping->address;
                $this->Template->address2            = $cached_shipping->address2;
                $this->Template->city                = $cached_shipping->city;
                $this->Template->zip                 = $cached_shipping->zip;
                $this->Template->phone               = $cached_shipping->phone;
                $this->Template->fax                 = $cached_shipping->fax;
                $this->Template->country_id          = $cached_shipping->country_id;
                $this->Template->anrede              = $cached_shipping->anrede;
                
                $cached_billing = new PnAddress();
                $cached_billing->initWithId($order->billing_address_id);
                
                $this->Template->billing_address_id = $cached_billing->id;
                $this->Template->billing_firstName  = $cached_billing->firstName;
                $this->Template->billing_lastName   = $cached_billing->lastName;
                $this->Template->billing_firma      = $cached_billing->company;
                $this->Template->billing_address    = $cached_billing->address;
                $this->Template->billing_address2   = $cached_billing->address2;
                $this->Template->billing_city       = $cached_billing->city;
                $this->Template->billing_zip        = $cached_billing->zip;
                $this->Template->billing_phone      = $cached_billing->phone;
                $this->Template->billing_fax        = $cached_billing->fax;
                $this->Template->billing_country_id = $cached_billing->country_id;
                $this->Template->billing_anrede     = $cached_billing->anrede;
            }
			
			$payment = new PnPayment();
			$this->Template->bookItems     = $order->getBookItems();
			$this->Template->payments      = $payment->getPayments();
			$this->Template->payment_id    = $order->payment_method_id;
			$this->Template->totalPrice    = $order->getTotalPrice("", false);
			$this->Template->wineItems     = $order->getWineItems();
			$this->Template->shippingPrice = $order->getShippingPrice();
			$this->Template->order_found   = $order->found;
		}
		
		if($step == 3)
		{
			if($_POST['shipping'])
			{
				$order->update(array("shipping_id" => $_POST['shipping']));
				header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, 4));
			}
			
			$shipping 	                = new PnShipping();
			$this->Template->shippings  = $shipping->get($order);
			
			$this->Template->shipping   = $order->shipping_id;
			$this->Template->totalPrice = $order->getTotalPrice("", false);
			$this->Template->order      = $order;
			
			$address = new PnAddress();
			$address->initWithId($order->shipping_address_id);
			$country = new PnCountry();
			$country->initWithId($address->country_id);
			$this->Template->shippingCountry = $country->name;
		}
		
		if($step == 4)
		{
			//Load address
			$objAddress = new PnAddress();
			
			$objAddress->initWithId($order->shipping_address_id);
			$this->Template->ShippingAddress = $objAddress;
			
			$objBillingAddr = new PnAddress();
			$objBillingAddr->initWithId($order->billing_address_id);
			
			$this->Template->BillingAddress	   = $objBillingAddr;
			$this->Template->bookItems         = $order->getBookItems();
			$this->Template->shippingPrice     = $order->getShippingPrice();
			$this->Template->totalPrice        = $order->getTotalPrice("", false);
			$this->Template->wineItems         = $order->getWineItems();
			$this->Template->orderid           = $order->id;
			
			$shipping = new PnShipping();
			$shipping->initWithId($order->shipping_id);
			$this->Template->shipping = $shipping;
			
			$payment = new PnPayment();
			$payment->initWithId($order->payment_method_id);
			$this->Template->payment = $payment;
			
			if($_POST['step'])
			{
			    if(!$_POST["datenschutz_"])
			    {
			        $this->Template->error_datenschutz_ = true;
			    }
			    
                if(!$_POST["agb_"])
                {
                    $this->Template->error_agb_ = true;
                }
                
                if(!$_POST["widerruf_"])
                {
                    $this->Template->error_widerruf_ = true;
                }
			    
			    if($_POST["datenschutz_"] && $_POST["agb_"] && $_POST["widerruf_"])
			    {
			        $order->recalc();
                    $order->createOrderNumber();
                    $order->edit(array("order_status" => 1));
                    
                    setcookie ("PN_order_id", "", time() - 3600);
                    
                    header(sprintf("Location: %s?order=%s&step=%s", $checkout_page, $order->id, 5));
			    }
			    else
			    {
			        $this->Template->error = true;
			    }
			}
		}
		
		if($step == 5)
		{
		    $amount = $order->getTotalPrice() * 100;
            $currency = $GLOBALS['TL_CONFIG']['currency'] ? $GLOBALS['TL_CONFIG']['currency'] : "EUR";
            $shipBeforeDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")+1));
            $sessionValidity = date(DATE_ATOM, mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y")+1));
            $merchantref = "Order".$order->id;
            $skinCode = $GLOBALS['TL_CONFIG']['skinCode'] ? $GLOBALS['TL_CONFIG']['skinCode'] : "9Bg7A81t";
            $merchantAccount = $GLOBALS['TL_CONFIG']['merchantAccount'] ? $GLOBALS['TL_CONFIG']['merchantAccount'] : "PersonalNOVELDE";
            $shopperEmail = "";
            $allowedMethods = "visa,paypal,mc,bankTransfer";
            $blockedMethods = "";
            $signature = $GLOBALS['TL_CONFIG']['signature'] ? $GLOBALS['TL_CONFIG']['signature'] : "pnovel.2012";
            $shopperLocale = "DE";
            $countryCode = $GLOBALS['TL_CONFIG']['countryCode'] ? $GLOBALS['TL_CONFIG']['countryCode'] : "DE";
            $testOrLive = $GLOBALS['TL_CONFIG']['adyen_test_live'] ? $GLOBALS['TL_CONFIG']['adyen_test_live'] : "test";
            
            $Crypt_HMAC = new Crypt_HMAC($signature, 'sha1');
            
			$payment_id = $order->payment_method_id;
			if($payment_id == 1)
			{
			    $allowedMethods = "bankTransfer";
			    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
    
			    header("Location: " . $url);
			}
			
			if($payment_id == 2)
			{
			    $allowedMethods = "visa,mc";
			    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
    
			    header("Location: " . $url);
			}
			
			if($payment_id == 3)
			{
			    $allowedMethods = "paypal";
			    $sign = $amount . $currency . $shipBeforeDate .  $merchantref . $skinCode .  $merchantAccount . $sessionValidity . $shopperEmail . $allowedMethods . $blockedMethods;
                $merchantsig =  base64_encode(pack('H*',$Crypt_HMAC->hash($sign)));
                $url = "https://" . $testOrLive . ".adyen.com/hpp/pay.shtml?merchantReference=".urlencode($merchantref)."&paymentAmount=".urlencode($amount)."&currencyCode=".urlencode($currency)."&shipBeforeDate=".urlencode($shipBeforeDate)."&skinCode=".urlencode($skinCode)."&merchantAccount=".urlencode($merchantAccount)."&shopperLocale=".urlencode($shopperLocale)."&orderData=".urlencode($orderData)."&sessionValidity=".urlencode($sessionValidity)."&shopperEmail=".$shopperEmail."&shopperReference=&recurringContract=&allowedMethods=".urlencode($allowedMethods)."&blockedMethods=".urlencode($blockedMethods)."&skipSelection=".urlencode($skipSelection)."&countryCode=".urlencode($countryCode)."&merchantSig=".urlencode($merchantsig)."&numOfArticle=".$order->getNumOfBookItems()."&numOfAddon=".$order->getNumOfAddonItems();
    
			    header("Location: " . $url);
			}
		}
		
		if($step == 6)
		{
		    if($_GET["merchantReference"])
		    {
		        $orderid = str_replace("Order", "", $_GET["merchantReference"]);
		        $order->initWithId($orderid);
		    }
		    else if($_GET["order"])
		    {
		        $order->initWithId($_GET["order"]);
		    }
		    else
		    {
		        gotoErrorPage();
		    }
		    
		    $address = new PnAddress();
		    $address->initWithId($order->shipping_address_id);
		    
		    $order_cid = Verhoeff::generate($order->number);
		    
		    $order->update(array("order_step" => "done"));
		    
			$this->Template->order                = $order;
			$this->Template->order_cid            = $order_cid;
			$this->Template->customer_firstName   = $address->firstName;
            $this->Template->customer_lastName    = $address->lastName;
            $this->Template->customer_address     = $address->address;
            $this->Template->customer_company     = $address->company;
            $this->Template->customer_city        = $address->city;
            $this->Template->customer_zip         = $address->zip;
            $this->Template->customer_country     = $address->getCountry();
		}
	}
}

?>