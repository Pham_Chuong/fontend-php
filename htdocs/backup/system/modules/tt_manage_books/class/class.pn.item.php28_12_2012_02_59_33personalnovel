<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnItem extends Controller
{
	public $id;
	public $order_id;
	public $book_id;
	public $class_id;
	public $article_id;
	public $font_id;
	public $document_id;
	public $design_id;
	public $version_id;
	public $fields = array();
	public $price_book;
	public $price_bind;
	public $price_paper;
	public $color;
	public $covercolor_id;
	public $created;
	public $giftbox_id;
	public $subtitle;
	public $item_pieces;
	public $config;
	
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function select($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 			= $object->item_id;
			$this->order_id 	= $object->order_id;
			$this->parent_id    = $object->parent_id;
			$this->book_id 		= $object->buch_id;
			$this->article_id 	= $object->artikel_id;
			$this->fields 		= $object->item_fields;
			$this->item_pieces 	= $object->item_pieces;
		}
	}
	
	public function initWithId($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 			= $object->item_id;
			$this->order_id 	= $object->order_id;
			$this->parent_id    = $object->parent_id;
			$this->book_id 		= $object->buch_id;
			$this->article_id 	= $object->artikel_id;
			$this->fields 		= $object->item_fields;
			$this->font_id      = $object->font_id;
			$this->item_pieces 	= $object->item_pieces;
			$this->subtitle		= $object->item_subtitle;
			$this->etiquette_id = $object->etiquette_id;
			$this->item_image1_present = $object->item_image1_present;
			$this->item_image1_cropped = $object->item_image1_cropped;
		}
	}

	public function add($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = $v;
		}
		
		$object = $this->Database->prepare("
				INSERT INTO " . $this->config->database . ".items (" . implode(", ", $key) .") VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $object->insertId;
	}
	
	public function edit($param = array())
	{
		$set = array();
		foreach ($param as $k => $v)
		{
			$set[] = $k . "  =  '" . mysql_real_escape_string($v) . "'";
		}
		
		$object = $this->Database->prepare("UPDATE " . $this->config->database . ".items SET " . implode(", ", $set) . " WHERE item_id = ?" )->execute($this->id);
		
		return $object->affectedRows;
	}
	
	public function remove($params = array())
	{
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($this->id);
	}
	
	public function removeChildItem()
	{
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items
				WHERE parent_id = ?
			")->execute($this->id);
			
		return $object->affectedRows;
	}
	
	public function getPrice($locale="de")
	{
		$object = $this->Database->prepare("
				SELECT price_amount, price_amount_uk FROM " . $this->config->database . ".prices
				WHERE artikel_id = ?  
				AND (isnull(price_validuntil) OR price_validuntil > now())
			")->executeUncached($this->article_id);
			
		while($object->next())
		{
			if($locale == "de")
				return $object->price_amount;
			return $object->price_amount_uk;
		}
		
		return 0;
	}
	
	public function getName()
	{
		$object = $this->Database->prepare("
					SELECT artikel_name FROM " . $this->config->database . ".finanz_artikel
					WHERE artikel_id = ?
				")->execute($this->article_id);
				
		while($object->next())
		{
			return $object->artikel_name;
		}
		
		return "";
	}
	
	public function getTypeId()
	{
		$object = $this->Database->prepare("
					SELECT type_id FROM " . $this->config->database . ".finanz_artikel
					WHERE artikel_id = ?
				")->execute($this->article_id);
				
		while($object->next())
		{
			return $object->type_id;
		}
		
		return "";
	}
	
	public function getAsset()
	{
	    $asset_id = "";
	    $objDb = $this->Database->prepare("
		                SELECT * FROM " . $this->config->database . ".items_assets
		                WHERE item_id = ?
		            ")->execute($this->id);
		while($objDb->next())
        {
            $asset_id = $objDb->asset_id;
        }
        
        return $asset_id;
	}
	
	public function getWineGravurItems()
	{
	    $gravurs = array();
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".items
	            WHERE class_id = 20
	            AND parent_id = ?
	        ")->execute($this->id);
	        
	    while($objDb->next())
	    {
	        $item                 = new PnItem();
	        $item->id             = $objDb->item_id;
	        $item->article_id     = $objDb->artikel_id;
	        $item->class_id       = $objDb->class_id;
	        $item->item_fields    = $objDb->item_fields;
	        $item->item_pieces    = $objDb->item_pieces;
	        $gravurs[]            = $item;
	    }
	    
	    return $gravurs;
	}
	
	public function getPnArticle()
	{
	    $article = new PnArticle();
	    $article->initWithId($this->article_id);
	    return $article;
	}
	
	//Fix item_pieces ->because column item_pieces is wrong
	public function fixItemPieces()
	{
	    $article = new PnArticle();
	    $article->initWithId($this->article_id);
	    
	    if(!$this->parent_id || $this->parent_id == "")
	    {
	        return $this->item_pieces;
	    }
	    else
	    {
	        if($article->pieces_from_parent > 0)
	        {
	            $parent = new PnItem();
                $parent->initWithId($this->parent_id);
                
                return $parent->item_pieces;
	        }
	        else
	        {
	            return $this->item_pieces;
	        }
	    }
	}
	
	public function getChildren()
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".items
	            WHERE parent_id = ?
	        ")->execute($this->id);
	        
	    $items = array();
	    while($objDb->next())
	    {
	        $item = new PnItem();
	        $item->id                = $objDb->item_id;
	        $item->parent_id         = $objDb->parent_id;
	        $item->order_id          = $objDb->order_id;
	        $item->class_id          = $objDb->class_id;
	        $item->article_id        = $objDb->artikel_id;
	        $item->font_id           = $objDb->font_id;
	        $item->document_id       = $objDb->document_id;
	        $item->design_id         = $objDb->design_id;
	        $item->version_id        = $objDb->version_id;
	        $item->fields            = json_decode($objDb->item_fields);
	        $item->price_book        = $objDb->price_book;
	        $item->price_bind        = $objDb->price_bind;
	        $item->price_paper       = $objDb->price_paper;
	        $item->color             = $objDb->color;
	        $item->covercolor_id     = $objDb->covercolor_id;
	        $item->created           = $objDb->created;
	        $item->giftbox_id        = $objDb->giftbox_id;
	        $item->subtitle          = $objDb->item_subtitle;
	        $item->item_pieces       = $objDb->item_pieces;
	        
	        $items[]                 = $item;
	    }
	    
	    return $items;
	}
	
	//Saldo
	public function recalc($konto_id)
	{
	    $posten = new PnPosten();
	    $zahlungen = new PnZahlungen();
	    
	    $posten->initWithItemId($this->id);
	    
	    $quantity = $this->fixItemPieces();
	    $article = $this->getPnArticle();
	    
	    if($posten->id !="" || $posten->id > 0)
	    {
	        $posten->edit(
	                array(
                        "steuerart_id" => 1,
                        "artikel_id" => $this->article_id,
                        "posten_anzahl" => $quantity,
                        "posten_einzelbetrag" => $article->getPrice(),
                        "posten_netto" => 0,
                        "posten_text" => mysql_real_escape_string($article->name),
                        "posten_manuell" => 0,
                        "posten_updated" => date('Y/m/d h:i:s a', time()),
                        "item_id" => $this->id,
                        "order_id" => $this->order_id,
                        "currency_id" => 1,
                        "steuersatz_id" => 1
                    )
	            );
	        
	        $zahlungen->initWithPostenId($posten->id);
	        $zahlungen->edit(
	                array(
	                    "steuerart_id" => 1,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "ust_id" => 1,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => $article->getPrice(),
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($article->name),
                        "zahlung_updated" => date('Y/m/d h:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => 1,
	                    )
	            );
	        
	        $zahlungen->initChild();
	        $zahlungen->edit(
	                array(
	                    "steuerart_id" => 1,
                        "journal_id" => 1,
                        "stapel_id" => 3,
                        "order_id" => $this->order_id,
                        "ust_id" => 1,
                        "zahlung_anzahl" => $quantity,
                        "zahlung_typ" => 9,
                        "zahlung_andereskonto" => 0,
                        "zahlung_stapel" => 1,
                        "zahlung_betrag" => $article->getPrice(),
                        "zahlung_netto" => 0,
                        "zahlung_text" => mysql_real_escape_string($article->name),
                        "zahlung_updated" => date('Y/m/d h:i:s a', time()),
                        "currency_id" => 1,
                        "zahlung_steuersatz" => 7,
                        "steuersatz_id" => 1,
	                    )
	            );
	    }
	    else
	    {
	        $posten_id = $posten->add(
                    array(
                        "steuerart_id" => 1,
                        //"posten_zielkonto" => null,
                        "konto_id" => $konto_id,
                        "artikel_id" => $this->article_id,
                        "posten_anzahl" => $quantity,
                        "posten_einzelbetrag" => $article->getPrice(),
                        "posten_netto" => 0,
                        "posten_text" => mysql_real_escape_string($article->name),
                        //"posten_description" => null,
                        "posten_datum" => date('Y/m/d h:i:s a', time()),
                        "posten_manuell" => 0,
                        "posten_created" => date('Y/m/d h:i:s a', time()),
                        "posten_updated" => date('Y/m/d h:i:s a', time()),
                        //"acc_id" => null,
                        "item_id" => $this->id,
                        "order_id" => $this->order_id,
                        //"rechnung_id" => null,
                        //"coupon_id" => null,
                        //"culprit_id" => null,
                        //"ust_id" => null,
                        "currency_id" => 1,
                        "steuersatz_id" => 1
                    )
	            );
	        
	        $zahlung_parent_id = $zahlungen->add(
                        array(
                            "steuerart_id" => 1,
                            "journal_id" => 1,
                            "stapel_id" => 3,
                            //"protokoll_id" => null,
                            //"parent_id" => null,
                            "posten_id" => $posten_id,
                            "order_id" => $this->order_id,
                            "ust_id" => 1,
                            "konto_id" => $article->konto_id,
                            //"coupon_id" => null,
                            "zahlung_anzahl" => $quantity,
                            "zahlung_typ" => 9,
                            "zahlung_andereskonto" => 0,
                            "zahlung_stapel" => 1,
                            "zahlung_betrag" => $article->getPrice(),
                            "zahlung_netto" => 0,
                            //"zahlung_name" => null,
                            "zahlung_text" => mysql_real_escape_string($article->name),
                            "zahlung_datum" => date('Y/m/d h:i:s a', time()),
                            "zahlung_valuta" => date('Y/m/d h:i:s a', time()),
                            "zahlung_created" => date('Y/m/d h:i:s a', time()),
                            "zahlung_updated" => date('Y/m/d h:i:s a', time()),
                            "currency_id" => 1,
                            "zahlung_steuersatz" => 7,
                            //"zahlung_total_netto" => null,
                            //"zahlung_total_brutto" => null,
                            "steuersatz_id" => 1,
                            //"zahlung_otherorder" => null,
                            )
                    );
            
            $zahlung_child_id = $zahlungen->add(
                        array(
                            "steuerart_id" => 1,
                            "journal_id" => 1,
                            "stapel_id" => 3,
                            //"protokoll_id" => null,
                            "parent_id" => $zahlung_parent_id,
                            "posten_id" => $posten_id,
                            "order_id" => $this->order_id,
                            "ust_id" => 1,
                            "konto_id" => $konto_id,
                            //"coupon_id" => null,
                            "zahlung_anzahl" => $quantity,
                            "zahlung_typ" => 9,
                            "zahlung_andereskonto" => 0,
                            "zahlung_stapel" => 1,
                            "zahlung_betrag" => -1 * floatVal($article->getPrice()),
                            "zahlung_netto" => 0,
                            //"zahlung_name" => null,
                            "zahlung_text" => mysql_real_escape_string($article->name),
                            "zahlung_datum" => date('Y/m/d h:i:s a', time()),
                            "zahlung_valuta" => date('Y/m/d h:i:s a', time()),
                            "zahlung_created" => date('Y/m/d h:i:s a', time()),
                            "zahlung_updated" => date('Y/m/d h:i:s a', time()),
                            "currency_id" => 1,
                            "zahlung_steuersatz" => 7,
                            //"zahlung_total_netto" => null,
                            //"zahlung_total_brutto" => null,
                            "steuersatz_id" => 1,
                            //"zahlung_otherorder" => null,
                            )
                    );
	    }
	    
	}
}

class PnBookItem extends PnItem
{
	public function getWidmungItem()
	{
		$item = new PnItem();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 10
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->item_fields = $object->item_fields;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getName()
	{
		$object = $this->Database->prepare("
					SELECT `buch_titel`
                    FROM " . $this->config->database . ".buecher b
                    LEFT JOIN " . $this->config->database . ".finanz_artikel fa
                    ON b.buch_id = fa.buch_id
                    WHERE fa.artikel_id = ?
				")->execute($this->article_id);
				
		while($object->next())
		{
			return $object->buch_titel;
		}
		
		return "";
	}
	
	public function getTextvorschau()
	{
		
	}
	
	public function getFontName($locate="de")
	{
		$object = $this->Database->prepare("
				SELECT t.font_title, t.font_title_en FROM " . $this->config->database . ".items i
				LEFT JOIN " . $this->config->database . ".tex_fonts t
				ON i.font_id = t.font_id
				WHERE i.item_id = ?
			")->execute($this->id);
			
		while($object->next())
		{
			if($locate=="de")
				return $object->font_title;
			return $object->font_title_en;
		}
		
		return "";
	}
	
	public function getBookEntities()
	{
		$arrEntities 	= array();
		$arrFields	 	= array();
		
		$entities = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".buch_entities be
				LEFT JOIN " . $this->config->database . ".buch_fields bf
				ON be.entity_id = bf.entity_id
				WHERE be.variant_id = 
				(
				SELECT bb.variant_id
				FROM " . $this->config->database . ".buch_buchvariant bb
				INNER JOIN " . $this->config->database . ".buch_variant bv
				ON bb.variant_id = bv.variant_id
				WHERE bb.buch_id = ?
				AND bv.variant_public = 1
				)
				ORDER BY bf.field_position
			")->execute($this->book_id);
		
		$item_fields = $this->Database->prepare("
				SELECT item_fields FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($this->id);
			
		$fields = array();
		while($item_fields->next())
		{
			$fields = json_decode($item_fields->item_fields);
		}
		
		while($entities->next())
		{
			$arrEntities[$entities->entity_id]['name'] = $entities->entity_name;
			$arrFields[] = array(
					"entity_id"			=> $entities->entity_id,
					"field_name"			=> $entities->field_name,
					"field_description"	=> $entities->field_description,
					"field_default"		=> $entities->field_default,
					"field_count"		=> $entities->field_count
			);
		}
		
		foreach($fields as $k => $v)
		{
			foreach($arrFields as $k1 => $v1)
			{
				if($v1['field_name'] == $k)
					$arrFields[$k1]['field_name'] = $v;
			}
		}
		
		foreach($arrEntities as $k => $v)
		{
			foreach($arrFields as $field)
			{
				if($field['entity_id'] == $k)
				{
					$arrEntities[$k]['fields'][] = $field;
				}
			}
		}
		
		return $arrEntities;
	}
	
	public function getDesignItem()
	{
		$item = new PnItem();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 3
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->item_fields = $object->item_fields;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getDesignGrafik()
	{
		$item = new PnItem();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 2
					AND item_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->item_fields = $object->item_fields;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getAddonItems()
	{
		$items = array();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items i
					LEFT JOIN " . $this->config->database . ".artikel_klassen ak
					ON i.class_id = ak.class_id
					WHERE ak.class_is_addon = 1
					AND ak.parent_id = 2
					AND i.parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item 				= new PnItem();
			$item->id 			= $object->item_id;
			$item->item_fields 	= $object->item_fields;
			$item->article_id 	= $object->artikel_id;
			$item->class_id		= $object->class_id;
			$item->item_pieces	= $object->item_pieces;
			$item->pieces_from_parent = $object->class_pieces_from_parent;
			$items[] 			= $item;
		}
		
		return $items;
	}
	
	public function getVersionName($locate="de")
	{
		$object = $this->Database->prepare("
				SELECT v.version_name, v.version_name_en FROM " . $this->config->database . ".finanz_artikel fa
				LEFT JOIN " . $this->config->database . ".versions v
				ON fa.version_id = v.version_id
				WHERE fa.artikel_id = ?
			")->execute($this->article_id);
			
		while($object->next())
		{
			if($locate=="de")
				return $object->version_name;
			return $object->version_name_en;
		}
		
		return "";
	}
	
	public function getBookVersion()
	{
		$object = $this->Database->prepare("
				SELECT v.version_id, v.version_name, v.version_name_en FROM " . $this->config->database . ".finanz_artikel fa
				LEFT JOIN " . $this->config->database . ".versions v
				ON fa.version_id = v.version_id
				WHERE fa.artikel_id = ?
			")->execute($this->article_id);
		
		$version = new PnVersion();
		while($object->next())
		{
			$version->id = $object->version_id;
			$version->name = $object->version_name;
			$version->name_en = $object->version_name_en;
		}
		
		return $version;
	}
	
	public function getCoverImage($arrParam = array())
	{
		$url = sprintf($this->config->image_root . "/preview/BookArtikel/%s/%s.png",
								$this->article_id, md5($this->config->site_secret . "BookArtikel" . $this->article_id));
		if(count($arrParam) > 0)
		{
			$qstr = array();
			foreach ($arrParam as $k => $v)
			{
				$qstr[] = "$k=$v";
			}
			
			$url .= "?" . implode('&', $qstr);
		}
		
		return $url;
		
	}
	
	public function getDecocover()
	{
		$cache_decocover = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items_decocovers
				WHERE item_id = ?
			")->execute($this->id);
		
		while($cache_decocover->next())
			return $cache_decocover->decocover_id;
		
		return 0;
	}
	
	public function getTotalPrice()
	{
		/*$priceBook 		= $this->getPrice();
		$widmungItem 	= $this->getWidmungItem();
		$priceWidmung	= $widmungItem->getPrice();
		$designItem 	= $this->getDesignItem();
		$priceDesign	= $designItem->getPrice();
		
		$priceAddons = 0;
		$addonItems = $this->getAddonItems();
		foreach($addonItems as $item)
		{
			if($item->pieces_from_parent > 0)
			{
				$priceAddons += $this->item_pieces * $item->getPrice();
			}
			else
			{
				$priceAddons += $item->item_pieces * $item->getPrice();
			}
		}*/
		
		//$bookQuantity = $this->item_pieces;
		$totalPrice = $this->getPrice() * $this->item_pieces;
		foreach($this->getChildren() as $child)
		{
		    $totalPrice += $child->fixItemPieces() * $child->getPrice();
		}
		
		return $totalPrice;
	}
	
	public function isPersonalize()
	{
	    /*$book = new PnBook();
	    $book->current($this->book_id);
	    
	    $defaultFields = $book->getEntities();
	    $fields = json_decode($this->item_fields);
	    
	    return arraysEqual($defaultFields, $fields);*/
	}
	
	public function addDecocover($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = $v;
		}
		
		$cache_decocover = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items_decocovers
				WHERE item_id = ?
			")->execute($this->id);
			
		if($cache_decocover->numRows > 0)
			$this->Database->prepare("
				UPDATE " . $this->config->database . ".items_decocovers
				SET decocover_id = ?
				WHERE item_id = ?
				")->execute($params['decocover_id'], $params['item_id']);
		else
			$this->Database->prepare("
				INSERT INTO " . $this->config->database . ".items_decocovers (" . implode(", ", $key) .") 
				VALUES('" . implode("', '", $value) . "')
				")->execute();
	}
	
	public function removeDecocover()
	{
	    $this->Database->prepare("
	            DELETE FROM " . $this->config->database . ".items_decocovers
	            WHERE item_id = ?
	        ")->execute($this->id);
	}
	
	public function removeChildItem()
	{
	    $this->removeDecocover();
	    
	    $childitem_id = array();
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".items
				WHERE parent_id = ?
	        ")->execute($this->id);
	        
	    while($objDb->next())
	    {
	        $childitem_id[] = $objDb->item_id;
	    }
	    
	    if(count($childitem_id) > 0)
	    {
            //Delete items_decocovers records
            $objDb = $this->Database->prepare("
                    DELETE FROM " . $this->config->database . ".items_decocovers
                    WHERE item_id IN (" . implode(",", $childitem_id) . ")
                ")->execute();
                
            //Delete items_assets records
            $objDb = $this->Database->prepare("
                    DELETE FROM " . $this->config->database . ".items_assets
                    WHERE item_id IN (" . implode(",", $childitem_id) . ")
                ")->execute();
            
            //Delete items records
            $objDb = $this->Database->prepare("
                    DELETE FROM " . $this->config->database . ".items
                    WHERE item_id IN (" . implode(",", $childitem_id) . ")
                ")->execute();
	    }
			
		return $objDb->affectedRows;
	}
	
	public function removeChildItems($param=array())
	{
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items
				WHERE artikel_id IN ('" . implode("', '", $param) . "')
				AND parent_id = ?
			")->execute($this->id);
		
		return $object->affectedRows;
	}
	
	public function getDefaultSubtitle()
	{
	    $book = new PnBook();
	    $book->initWithId($this->book_id);
	    
	    $arrFields = json_decode($this->fields);
	    $coversub  = $book->coversub;
	    if($book->coversub != "")
	    {
	        $coversub = str_replace("[", "", $coversub);
            $coversub = str_replace("]", "", $coversub);
	        foreach($arrFields as $k => $v)
	        {
	            $coversub = str_replace($k, $v, $coversub);
	        }
	    }
	    
	    return $coversub;
	}
	
	public function getBookLockUpgradeItems()
	{
	    $booklocks = array();
	    
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".items
	            WHERE class_id = 12
	            AND parent_id = ?
	        ")->executeUncached($this->id);
	        
	    while($objDb->next())
	    {
	        $booklock               = new PnItem();
	        $booklock->id           = $objDb->item_id;
	        $booklock->article_id   = $objDb->artikel_id;
	        $booklock->item_pieces  = $objDb->item_pieces;
	        $booklock->item_fields  = $objDb->item_fields;
	        
	        $booklocks[]            = $booklock;
	    }
	    
	    return $booklocks;
	}
}

class PnWidmungItem extends PnItem
{
	public function getText()
	{
		$fields = json_decode($this->fields);
		return $fields->text;
	}
	
	public function getFontFace()
	{
		$fields = json_decode($this->fields);
		return $fields->fontface;
	}
	
	public function getFontColor()
	{
		$fields = json_decode($this->fields);
		return $fields->fontcolor;
	}
}

class PnWineItem extends PnItem
{
	public function getDesignItem()
	{
		$item = new PnItem();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 18
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getAddonItems()
	{
		$items = array();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 15
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item 				= new PnItem();
			$item->id 			= $object->item_id;
			$item->article_id 	= $object->artikel_id;
			$item->class_id		= $object->class_id;
			$item->item_pieces	= $object->item_pieces;
			$items[] 			= $item;
		}
		
		return $items;
	}
	
	public function getName()
	{
		$object = $this->Database->prepare("
					SELECT artikel_name FROM " . $this->config->database . ".finanz_artikel
					WHERE artikel_id = ?
				")->execute($this->article_id);
				
		while($object->next())
		{
			return $object->artikel_name;
		}
		
		return "";
	}
	
	public function getTotalPrice()
	{
	    $designitem = $this->getDesignItem();
	    $addonitems = $this->getAddonItems();
	    
	    $price = 0;
	    $designitem_price = $designitem->getPrice();
	    
	    $addonitems_price = 0;
	    $gravuritems_price = 0;
	    foreach($addonitems as $addon)
	    {
	        $addonitems_price += $addon->getPrice() * $addon->item_pieces;
	        $gravurs = $addon->getWineGravurItems();
	        foreach($gravurs as $gravur)
	        {
	            $gravuritems_price += $gravur->getPrice() * $addon->item_pieces;
	        }
	    }
	    
	    $price = ($this->getPrice() + $designitem_price) * $this->item_pieces + $addonitems_price + $gravuritems_price;
	    
	    return $price;
	}
	
	/*public function removeChildItem()
	{
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items
				WHERE parent_id = ?
			")->execute($this->id);
			
		return $object->affectedRows;
	}*/
	
}

class PnDesignItem extends PnItem
{
    //Todo extends
}

?>