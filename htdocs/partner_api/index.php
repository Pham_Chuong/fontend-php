<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Demo Partner API client</title>
    <style>
        input { margin: 10px 0 0 }
        ul, li { margin: 0; padding: 0 }
        ul {padding-left: 15px }
        label { font-weight: bold }
        .clear { clear:both; margin: 0; padding: 0 }
        .mandanten { border: 1px solid #000; width: 200px; padding: 10px; float: left }
        .urlrequest { border: 1px solid #000; width: 550px; padding: 10px; float: left; margin: 0 10px 0 0 }
        .urlrequest li {padding-bottom: 10px }
        .link { color: blue }
        .example { font-style: italic }
        .datarequest, .dataresponse { margin: 0 0 10px 0; width: 800px }
        .datarequest input.url, .datarequest textarea { width: 100% }
        .datarequest input { margin: 10px 0 10px }
        .datarequest textarea { height: 500px; margin: 10px 0 10px }
        .data { width: 800px; border-bottom: 1px solid #000 }
        hr { width: 800px; margin: 10px 0 10px }
    </style>
</head>

<body>
    <div class="datarequest">
        <form method="POST" action="./index.php">
            <label>Request URL</label><br />
            <input class="url" type="text" name="url" value="<?php echo $_POST["url"] ? $_POST["url"] : "http://local-backend.personalnovel.local/partner_api/mandant/20/order/" ?>" /><br />
            <label>Username</label><br />
            <input type="text" name="username" value="<?php echo $_POST["username"] ?>" /><br />
            <label>Password</label><br />
            <input type="password" name="password" value="<?php echo $_POST["password"] ?>" /><br />
            <label>JSON Request</label><br />
            <textarea name="jsondata"><?php echo $_POST['jsondata'] ? $_POST['jsondata'] : "" ?></textarea><br />
            <input type="submit" value="send request" />
        </form>
        
        <?php
            $json_url = $_POST['url'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            
            $ch = curl_init( $json_url );
            
            $json_string = json_encode(json_decode($_POST['jsondata']));
            
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERPWD => $username . ":" . $password,
                CURLOPT_HTTPHEADER => array('Content-type: application/json', 'Content-Length: ' . strlen($json_string), "Accept: application/json") ,
                CURLOPT_POSTFIELDS => $json_string,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_FOLLOWLOCATION => true,
            );
            
            curl_setopt_array( $ch, $options );
        ?>
    </div>
    
    <?php 
        $result =  curl_exec($ch);
        if ($result)
        {
            echo '<div class="dataresponse">';
            echo '<label>JSON Response</label><br /><br />';
            print_r($result);
            echo '</div>';
        }
        
        curl_close($ch);
    ?>
    
</body>
</html>
