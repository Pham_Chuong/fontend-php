<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnShipping extends Controller
{
	public $id;
	public $name;
	public $title;
	public $title_en;
	public $price;
	public $description;
	public $description_en;
	protected $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function all()
	{
		$shippings = array();
		$objects = $this->Database->prepare("
				
					SELECT * FROM " . $this->config->database . ".shipping_options
					WHERE shipping_active = 1
				
				")->execute();
				
		while($objects->next())
		{
			$shipping 					= new PnShipping();
			$shipping->id 				= $objects->shipping_id;
			$shipping->name 				= $objects->shipping_name;
			$shipping->title 			= $objects->shipping_title;
			$shipping->title_en 			= $objects->shipping_title_en;
			$shipping->description 		= $objects->description;
			$shipping->description_en 	= $objects->description_en;
			
			$shippings[] 				= $shipping;
		}
		
		return $shippings;
	}
	
	public function select($id)
	{
		$object = $this->Database->prepare("
			
					SELECT * FROM " . $this->config->database . ".shipping_options
					WHERE shipping_id = ?
				
				")->execute($id);
				
		while($object->next())
		{
			$this->id = $object->shipping_id;
			$this->name = $object->shipping_name;
			$this->title = $object->shipping_title;
			$this->title_en = $object->shipping_title_en;
			$this->description = $object->shipping_description;
			$this->description_en = $object->shipping_description_en;
		}
	}
	
	public function initWithId($id)
	{
	    $object = $this->Database->prepare("
			
					SELECT * FROM " . $this->config->database . ".shipping_options
					WHERE shipping_id = ?
				
				")->execute($id);
				
		while($object->next())
		{
			$this->id = $object->shipping_id;
			$this->name = $object->shipping_name;
			$this->title = $object->shipping_title;
			$this->title_en = $object->shipping_title_en;
			$this->description = $object->shipping_description;
			$this->description_en = $object->shipping_description_en;
		}
	}
	
	public function get($order)
	{
		$shippings = array();
		$shippingAddr = new PnAddress();
        $shippingAddr->initWithId($order->shipping_address_id);
        $country_id = $shippingAddr->country_id;
		
		$query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND (
                        shipping_options.shipping_id IN (1, 2, 12) 
                        OR shipping_options.shipping_id = 11
                        OR shipping_options.shipping_id = 21
                        ) 
                    AND shipping_options.shipping_id != 2 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 11 
                    AND shipping_options.shipping_id != 12 
                    ORDER BY shipping_pos
                ";
                
        if($country_id == 1)
        {
            $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 12 
                    AND shipping_options.shipping_id != 11 
                    ORDER BY shipping_pos
                ";
                
            if($order->getTotalPrice("de", false) > $GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN'])
            {
                $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 1 
                    AND shipping_options.shipping_id != 11 
                    ORDER BY shipping_pos
                ";
            }
        }
		
		$objects = $this->Database->prepare($query)->execute();
		
		while($objects->next())
		{
			$shipping 					= new PnShipping();
			$shipping->id 				= $objects->shipping_id;
			$shipping->name 			= $objects->shipping_name;
			$shipping->title 			= $objects->shipping_title;
			$shipping->title_en 		= $objects->shipping_title_en;
			$shipping->description 		= $objects->shipping_description;
			$shipping->description_en 	= $objects->shipping_description_en;
			
			$shippings[]				= $shipping;
		}
		
		return $shippings;
	}
	
	public function getPrice($order, $locate="de")
	{
	    $shippingAddr = new PnAddress();
        $shippingAddr->initWithId($order->shipping_address_id);
        $country_id = $shippingAddr->country_id;
        
		$object = $this->Database->prepare("
				
					SELECT p.`price_amount`, p.`price_amount_uk`
					FROM " . $this->config->database . ".finanz_artikel fa
					LEFT JOIN " . $this->config->database . ".prices p
					ON fa.`artikel_id` = p.`artikel_id`
					WHERE fa.`class_id` = 7
					AND fa.`country_id` = ?
					AND fa.`shipping_id` = ?
					AND fa.`artikel_active` = 1
					AND ( ISNULL(p.`price_validuntil`) OR p.`price_validuntil` > now() )
				
				")->execute($country_id, $this->id);
		
		while($object->next())
		{
			if($locate == "de")
				return $object->price_amount;
			return $object->price_amount_uk;
		}
		
	}
}

?>