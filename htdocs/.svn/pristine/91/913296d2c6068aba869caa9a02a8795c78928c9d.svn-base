<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnOrder extends Controller
{
	public $id = 0;
	public $number = 0;
	public $customer_id = 0;
	public $shipping_id = 0;
	public $payment_method_id = 1;
	public $shipping_address_id;
	public $billing_address_id;
	public $status = 0;
	protected $config;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function select($id)
	{
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".orders
					WHERE order_id = ?
				")->execute($id);
		while($object->next())
		{
			$this->id 				= $object->order_id;
			$this->number 			= $object->order_number;
			$this->customer_id 		= $object->customer_id;
			$this->shipping_id 		= $object->shipping_id;
			$this->status  			= $object->order_status;
			$this->payment_method_id = $object->payment_method_id;
			$this->shipping_address_id 	= $object->shipping_address_id;
			$this->billing_address_id 	= $object->billing_address_id;
		}
	}
	
	public function all()
	{
		$orders = array();
		
		$objects = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".orders
				")->execute();
		
		while($objects->next())
		{
			$order 				= new PnOrder();
			$order->id 			= $objects->order_id;
			$order->number 		= $objects->order_number;
			$order->customer_id 	= $objects->customer_id;
			$order->shipping_id 	= $objects->shipping_id;
			$order->status 		= $objects->status;
			$order->payment_method_id 	= $objects->payment_method_id;
			$this->shipping_address_id 	= $objects->shipping_address_id;
			$this->billing_address_id 	= $objects->billing_address_id;
			
			$orders[]			= $order;
		}
		
		return $orders;
	}
	
	public function getBookItem($bookId)
	{
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND buch_id = ?
					AND class_id = 2
				")->execute($this->id, $bookId);
	
		$item = new PnBookItem();
		while($object->next())
		{
			$item->id 			= $object->item_id;
			$item->order_id 		= $object->order_id;
			$item->book_id 		= $object->buch_id;
			$item->item_fields 	= $object->item_fields;
			$item->font_id 		= $object->font_id;
			$item->article_id	= $object->artikel_id;
			$item->covercolor_id = $object->covercolor_id;
		}
		
		return $item;
	}
	
	public function getBookItemTotalPrice()
	{
		$price = 0;
		foreach($this->getBookItems() as $bookItem)
		{
			$price += $bookItem->item_pieces * $bookItem->getPrice();
		}
		
		return $bookItem->getPrice();
	}
	
	public function getShippingPrice()
	{
		$price = 0;
		$shipping = new PnShipping();
		$shipping->select($this->shipping_id);
		$price = $shipping->getPrice();
		return $price;
	}
	
	public function getBookItems()
	{
		$bookItems = array();
		$objects = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND class_id = 2
				")->executeUncached($this->id);
				
		while($objects->next())
		{
			$bookItem 				= new PnBookItem();
			$bookItem->id 			= $objects->item_id;
			$bookItem->order_id 		= $objects->order_id;
			$bookItem->item_fields 	= $objects->item_fields;
			$bookItem->font_id 		= $objects->font_id;
			$bookItem->article_id 	= $objects->artikel_id;
			$bookItem->item_pieces	= $objects->item_pieces;
			$bookItem->book_id		= $objects->buch_id;
			
			$bookItems[]				= $bookItem;
		}
		
		return $bookItems;
	}
	
	public function getWidmungItem($parent_id)
	{
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND parent_id = ?
					AND class_id = 10
				")->execute($this->id, $parent_id);
		
		$item = new PnWidmungItem();
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->order_id = $object->order_id;
			$item->book_id = $object->buch_id;
			$item->item_fields = $object->item_fields;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getDesignItem($parent_id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items
				WHERE order_id = ?
				AND parent_id = ?
				AND class_id = 3
				")->execute($this->id, $parent_id);
		
		$item = new PnItem();
		while($object->next())
		{
			$item->id 			= $object->item_id;
			$item->order_id 		= $object->order_id;
			$item->book_id 		= $object->buch_id;
			$item->item_fields 	= $object->item_fields;
			$item->article_id	= $object->artikel_id;
			$item->subtitle		= $object->item_subtitle;
		}
		
		return $item;
	}
	
	public function getNumOfBookItems()
	{
		$num = 0;
		foreach($this->getBookItems() as $bookItem)
		{
			$num += $bookItem->item_pieces;
		}
		
		return $num;
	}
	
	public function getNumOfAddonItems()
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items i
				LEFT JOIN " . $this->config->database . ".artikel_klassen ak
				ON i.class_id = ak.class_id
				WHERE ak.class_is_addon = 1
				AND i.order_id = ?
			")->executeUncached($this->id);
		
		return $object->numRows;
	}
	
	public function getTotalPrice($locale="de")
	{
		$totalPrice = 0;
		foreach($this->getBookItems() as $bookItem)
		{
			$totalPrice += $bookItem->getTotalPrice();
		}
		
		foreach($this->getWineItems() as $wineItem)
		{
		    $totalPrice += $wineItem->getTotalPrice();
		}
		
		return $totalPrice;
	}
	
	public function add($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = $v;
		}
		
		$object = $this->Database->prepare("
					INSERT INTO " . $this->config->database . ".orders (". implode(", ", $key) .") VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $object->insertId;
	}
	
	public function update($params = array())
	{
		$set = array();
		foreach ($params as $k => $v)
		{
			$set[] = $k . "  =  '" . $v . "'";
		}
		
		$object = $this->Database->prepare("UPDATE " . $this->config->database . ".orders SET " . implode(", ", $set) . " WHERE order_id = ?" )->execute($this->id);
		
		return $object->affectedRows;
	}
	
	public function exist($id)
	{
		return false;
	}
	
	public function getWineItems()
	{
		$wineItems = array();
		$objDb = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND class_id = 14
				")->executeUncached($this->id);
				
		while($objDb->next())
		{
			$wineItem				= new PnWineItem();
			$wineItem->id			= $objDb->item_id;
			$wineItem->order_id		= $objDb->order_id;
			$wineItem->article_id	= $objDb->artikel_id;
			$wineItem->item_pieces	= $objDb->item_pieces;
			$wineItem->item_fields  = $objDb->item_fields;
			$wineItem->etiquette_id = $objDb->etiquette_id;
			
			$wineItems[]			= $wineItem;
		}
		
		return $wineItems;
	}
	
}

?>