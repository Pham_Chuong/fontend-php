<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnBook extends Controller
{
	public $id;
	public $title;
	public $title_en;
	public $author;
	public $type_id;
	public $article_id;
	public $preview_url;
	public $show_preview;
	public $default_decocover_id;
	public $coverText_default;
	public $illustraion;
	public $pages;
	protected $book_text;
	protected $price;
	protected $defaultFields = array();
	protected $config;

	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function select($id)
	{
		$book = $this->Database->prepare("SELECT * FROM " . $this->config->database . ".buecher WHERE buch_id = ?")->execute($id);
		while($book->next())
		{
			$this->id					= $book->buch_id;
			$this->title					= $book->buch_titel;
			$this->author				= $book->buch_autor;
			$this->type_id				= $book->type_id;
			$this->coverText_default 	= $book->buch_covertext_default;
			$this->illustration			= $book->buch_illus;
			$this->pages					= $book->buch_pages;
			$this->article_id 			= $book->artikel_id;
			$this->show_preview			= in_array($book->type_id, array(2, 11, 12, 13)) ? true : false;
		}
	}
	
	public function all()
	{
		$books = $this->Database->prepare("SELECT * FROM " . $this->config->database . ".buecher")->execute();
		
		$arrBook = array();
		while($books->next())
		{
			$book = new PnBook();
			$book->id			= $books->buch_id;
			$book->title			= $books->buch_titel;
			$book->author		= $books->buch_autor;
			$this->type_id		= $book->type_id;
			$arrBook[]			= $book;
		}
		
		return $arrBook;
	}
	
	public function current($id)
	{
		$book = $this->Database->prepare("
					SELECT f.`artikel_id` as artikel_id, b.buch_id as buch_id, b.`buch_titel` as buch_titel, 
							b.`buch_autor` as buch_autor, b.`buch_text` as buch_text, b.`type_id`, b.`default_decocover_id`,
							b.`buch_covertext_default`, b.`buch_illus`, b.`buch_pages`
					FROM " . $this->config->database . ".finanz_artikel as f
					LEFT OUTER JOIN " . $this->config->database . ".artikel_klassen as a on f.`class_id` = a.`class_id` 
					LEFT OUTER JOIN " . $this->config->database . ".buecher as b on f.`buch_id` = b.`buch_id` 
					LEFT OUTER JOIN " . $this->config->database . ".site_book as s on b.`buch_id` = s.`buch_id`
					LEFT OUTER JOIN " . $this->config->database . ".sites as s2 on s.`site_id` = s2.`site_id` 
					WHERE b.`buch_id` = '' or (b.`buch_pub` = 1 and b.`buch_hidden` = 0
					AND f.`version_id` not in (4, 5, 6, 1303, 1304, 1305) 
					AND b.`type_id` not in (8, 10, 21) and b.`buch_id` = ?)
					AND f.`artikel_active` = 1 
					AND a.`class_id` = 2 
					GROUP BY b.`buch_id`
				")->execute($id);
		
		while($book->next())
		{
			$this->id			= $book->buch_id;
			$this->title			= $book->buch_titel;
			$this->author		= $book->buch_autor;
			$this->type_id		= $book->type_id;
			$this->article_id	= $book->artikel_id;
			$this->book_text		= $book->buch_text;
			$this->coverText_default 	= $book->buch_covertext_default;
			$this->illustration			= $book->buch_illus;
			$this->pages					= $book->buch_pages;
			$this->default_decocover_id 	= $book->default_decocover_id;
			$this->show_preview			= in_array($book->type_id, array(2, 11, 12, 13)) ? true : false;
		}
		
	}
	
	public function getExp_Text()
	{
		$object = $this->Database->prepare("
				SELECT expose.exp_id AS expose_exp_id, 
				expose.reiter_id AS expose_reiter_id, 
				expose.buch_id AS expose_buch_id,
				expose.exp_name AS expose_exp_name, 
				expose.exp_sort AS expose_exp_sort, 
				expose.exp_text AS expose_exp_text 
				FROM " . $this->config->database . ".expose
				WHERE expose.buch_id = ?
				AND expose.exp_name = 'Teaser' 
				ORDER BY if(expose.reiter_id = 2, -1, expose.exp_sort)
			")->execute($this->id);
			
		while($object->next())
		{
			return $object->expose_exp_text;
		}
		
		return "";
	}
	
	public function getPreviewUrl($arrParam = array())
	{
		if($this->id != "" && $this->article_id)
		{
			$this->preview_url = sprintf($this->config->image_root . "/preview/BookArtikel/%s/%s.png",
								$this->article_id, md5($this->config->site_secret . "BookArtikel" . $this->article_id));
			
			if(count($arrParam) > 0)
			{
				$qstr = array();
				foreach ($arrParam as $k => $v)
				{
					$qstr[] = "$k=$v";
				}
				
				$this->preview_url .= "?" . implode('&', $qstr);
			}
			
			return $this->preview_url;
		}
	
		return "";
	}
	
	public function getAuthor()
	{
		$author = new PnAuthor();
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".autoren a
				INNER JOIN " . $this->config->database . ".buch_autoren ba
				ON a.autor_id = ba.autor_id
				WHERE ba.buch_id = ? 
			")->execute($this->id);
			
		while($object->next())
		{
			$author->id 					= $object->autor_id;
			$author->name 				= $object->autor_name;
			$author->email 				= $object->autor_email;
			$author->cc 					= $object->autor_cc;
			$author->gender 				= $object->autor_sex;
			$author->mwst 				= $object->autor_mwst;
			$author->autor_per_book 		= $object->autor_per_book;
			$author->pic_asset_id 		= $object->autor_pic_asset_id;
			$author->description 		= $object->autor_description;
			$author->catalogue 			= $object->autor_catalogue;
			$author->ustid 				= $object->autor_ustid;
			$author->steuernummer 		= $object->autor_steuernummer;
			$author->page_public 		= $object->autor_page_public;
			$author->pseudonym 			= $object->autor_pseudonym;
			$author->mailText 			= $object->autor_mailtext;
			$author->lieferantenummer 	= $object->autor_lieferantenummer;
		}
		
		return $author;
	}
	
	public function getLowestPrice()
	{
		if($this->id != "")
		{
			$price = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".prices p
					WHERE p.artikel_id = ?
					AND (isnull(price_validuntil) OR price_validuntil > now())
				")->execute($this->article_id);
				
			while($price->next())
			{
				$this->price = $price->price_amount;
				return $price->price_amount;
			}
		}

		return 0;
	}
	
	public function getReiter()
	{
		$arr_reiter_namen = array();
		
		$object = $this->Database->prepare("
				SELECT * 
				FROM " . $this->config->database . ".expose_reiter er 
				INNER JOIN " . $this->config->database . ".expose e 
				ON er.`reiter_id` = e.`reiter_id` 
				WHERE e.buch_id = ? 
				AND e.exp_name <> 'Teaser' 
				ORDER BY er.reiter_name DESC
			")->execute($this->id);
		
		while($object->next())
		{
			$arr_reiter_namen[$object->reiter_name][$object->exp_name] = str_replace('\n', "<br />", $object->exp_text);
		}
			
		return $arr_reiter_namen;
	}
	
	public function getDefaultFields()
	{
		$this->getEntities();
		return $this->defaultFields;
	}
	
	public function getEntities()
	{
		$arrEntities 	= array();
		$arrFields	 	= array();
		$this->defaultFields = array();
		if($this->id != "")
		{
			$entities = $this->Database->prepare("
							select * from " . $this->config->database . ".buch_entities be 
							left join " . $this->config->database . ".buch_fields bf 
							on be.`entity_id` = bf.`entity_id` 
							where be.`variant_id` = 
							(select bb.`variant_id` 
							from " . $this->config->database . ".buch_buchvariant bb 
							inner join " . $this->config->database . ".buch_variant bv 
							on bb.`variant_id` = bv.`variant_id` 
							where bb.`buch_id` = ? and bv.variant_public = 1) 
							order by bf.`field_position`
						")->execute($this->id);
			
			while($entities->next())
			{
				$arrEntities[$entities->entity_id]['name'] = $entities->entity_name;
				$arrEntities[$entities->entity_id]['description'] = $entities->entity_description;
				$arrFields[] = array(
						"entity_id"			=> $entities->entity_id,
						"field_name"		=> $entities->field_name,
						"field_description"	=> $entities->field_description,
						"field_default"		=> $entities->field_default,
						"field_count"		=> $entities->field_count
				);
				
				$this->defaultFields[$entities->field_name] = $entities->field_default;
			}
			
			foreach($arrEntities as $k => $v)
			{
				foreach($arrFields as $field)
				{
					if($field['entity_id'] == $k)
					{
						$arrEntities[$k]['fields'][] = $field;
					}
				}
			}
		}
		
		return $arrEntities;
		
	}
	
	public function getBookText()
	{
		$fields = array();
		preg_match_all("|\[(.+?)\]|", $this->book_text, $fields);
		$book_text = $this->book_text;
		
		foreach($fields[1] as $field)
		{
			$this->getDefaultFields();
			$book_text = str_replace("[".$field."]", "<span class='default " . $field . "'>". $this->defaultFields[$field] . "</span>", $book_text);
		}

		return $book_text;
	}
	
	public function getDecocovers()
	{
		$arrDecocovers = array();
		if($this->id != "")
		{
			$decocovers = $this->Database->prepare("
							select decocover_id from " . $this->config->database . ".items_decocovers where decocover_id in
							(select decocover_id from " . $this->config->database . ".decocovers where decocover_id in
							(select decocover_id from " . $this->config->database . ".buch_decocovers where buch_id = ?
							) order by `background_asset_id` desc
							) group by decocover_id order by item_id
						")->execute($this->id);
			
			while($decocovers->next())
			{
				$arrDecocovers[$decocovers->decocover_id] = sprintf($this->config->image_root . "/preview/BookArtikel/%s/%s.png?cover=%s",
															$this->article_id, md5($this->config->site_secret
															. "BookArtikel" . $this->article_id), $decocovers->decocover_id);
			}
		}
		
		return $arrDecocovers;
	}
	
	public function getBookVersions()
	{
		$articles = $this->Database->prepare("
								SELECT fa.`artikel_id`, fa.`version_id`, fa.`artikel_name`, fa.`artikel_name_en`, p.`price_amount`, p.`price_amount_uk`,
										v.`version_name`, v.`version_name_en`, v.`version_description`, v.`version_productiondays`
								FROM " . $this->config->database . ".finanz_artikel fa
								LEFT JOIN " . $this->config->database . ".prices p
								ON fa.`artikel_id` = p.`artikel_id`
								LEFT JOIN " . $this->config->database . ".versions v
								ON fa.`version_id` = v.`version_id`
								WHERE fa.`buch_id` = ?
								AND fa.`class_id` = 2
								AND fa.`artikel_active` = 1
								GROUP BY fa.`version_id`
								ORDER BY p.`price_amount`
							")->execute($this->id);
	
		$versions = array();
		while($articles->next())
		{
			$version					= new PnVersion();
			$version->article_id 		= $articles->artikel_id;
			$version->article_name 		= $articles->artikel_name;
			$version->article_name_en 	= $articles->artikel_name_en;
			$version->id 				= $articles->version_id;
			$version->name 				= $articles->version_name;
			$version->name_en			= $articles->version_name_en;
			$version->productions_day 	= $articles->version_productiondays;
			$version->description		= $articles->version_description;
			$version->price				= $articles->price_amount;
			$version->price_uk			= $articles->price_amount_uk;
			$versions[]					= $version;
		}
		
		return $versions;
	}
	
	public function getBookDesigns($version_id)
	{
		$objects = $this->Database->prepare("
					SELECT fa.`artikel_id`, fa.`artikel_name`, p.`price_amount`, fa.`artikel_desc`, fa.`design_id` 
					FROM " . $this->config->database . ".finanz_artikel fa 
					LEFT JOIN " . $this->config->database . ".artikel_version av ON fa.`artikel_id` = av.`artikel_id`
					LEFT JOIN " . $this->config->database . ".prices p ON fa.`artikel_id` = p.`artikel_id`
					WHERE fa.`artikel_active` = 1 
					AND fa.`mandant_id` = 1
					AND (p.`price_validuntil` > now() OR ISNULL(p.`price_validuntil`))
					AND av.`version_id` = ? 
				")->execute($version_id);
		
		$designs = array();
		while($objects->next())
		{
			$design 					= new PnDesign();
			$design->id 				= $objects->design_id;
			$design->name 			= $objects->artikel_name;
			$design->title 			= $objects->artikel_name;
			$design->title_en 		= $objects->artikel_name_en;
			$design->description 	= $objects->artikel_desc;
			$design->price 			= $objects->price_amount;
			$design->image 			= $objects->design_image;
			$design->artikel_id		= $objects->artikel_id;
			$designs[]				= $design;
		}
		
		return $designs;
	}
	
	public function getPreviewPages()
	{
		$preview_double_pages = array(
				"233" => array(11, 12, 13),
				"234" => array(4, 12, 15),
				"235" => array(4, 12, 15),
				"236" => array(5,  7, 15),
				"default" => array(4, 5, 6, 9)
			);
		
		$double_pages = $preview_double_pages[$this->id] ? $preview_double_pages[$this->id] : $preview_double_pages["default"];
		$pages = array();
		foreach($double_pages as $k => $v)
		{
			$pages[] = array($v * 2 , $v * 2 + 1);
		}
		
		return $pages;
	}
	
	public function getBookVariants()
	{
	    $objects = $this->Database->prepare("
	            SELECT bbv.variant_id 
	            FROM " . $this->config->database . ".buch_buchvariant bbv
	            INNER JOIN " . $this->config->database . ".buch_variant bv
	            ON bbv.variant_id = bv.variant_id
	            WHERE bv.variant_public = 1
	            AND bbv.buch_id = ?
	        ")->execute($this->id);
	    
	    $variants = array();
	    while($objects->next())
	    {
	        $variants[] = $objects->variant_id;
	    }
	    
	    return $variants;
	}
	
	public function getBookDocuments()
	{
	    $variants = $this->getBookVariants();
	    
	    $object = $this->Database->prepare("
	            SELECT * 
	            FROM " . $this->config->database . ".buch_document
	            WHERE variant_id IN (" . implode(",", $variants) . ")
	        ")->execute();
	        
	    $documents = array();
	    while($object->next())
	    {
	        $document                = new PnDocument();
	        $document->id 			 = $object->document_id;
			$document->title 		 = $object->document_title;
			$document->name 		 = $object->document_name;
			$readingSample           = preg_replace('/\n/', "<br />", $object->document_readingsample); 
			$document->readingSample = $readingSample;
			$document->file 		 = $object->document_file;
			$document->created 		 = $object->document_created;
			$document->updated 		 = $object->document_updated;
			$document->type 		 = $object->document_type;
			$document->decisions 	 = $object->document_decisions;
			$document->variant_id 	 = $object->variant_id;
			
			$documents[] = $document;
	    }
	    
	    return $documents;
	}
	
}

?>