<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnOrder extends Controller
{
	

	public $id = 0;
	public $number = 0;
	public $customer_id = 0;
	public $shipping_id = 0;
	public $payment_method_id = 1;
	public $shipping_address_id;
	public $billing_address_id;
	public $status = 0;
	protected $config;
	//config for hasCoupon
	public $coupon_prices_de = 6.99;
	public $coupon_name_de = "Geschenkbox";
	public $coupon_item_id = 610000;
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function select($id)
	{
		$this->initWithId($id);
	}
	
	public function initWithId($id)
	{
	    $object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".orders
					WHERE order_id = ?
				")->executeUncached($id);
				
		while($object->next())
		{
			$this->id 				    = $object->order_id;
			$this->number 			    = $object->order_number;
			$this->customer_id 		    = $object->customer_id;
			$this->shipping_id 		    = $object->shipping_id;
			$this->status  			    = $object->order_status;
			$this->payment_method_id    = $object->payment_method_id;
			$this->shipping_address_id 	= $object->shipping_address_id;
			$this->billing_address_id 	= $object->billing_address_id;
			$this->konto_id             = $object->konto_id;
			$this->date                 = $object->order_date;
			$this->created              = $object->order_created;
			$this->paid                 = $object->order_paid;
			$this->updated              = $object->order_updated;
			$this->found                = $object->order_found;
			$this->step                 = $object->order_step;
			$this->print                = $object->order_print;
			$this->blob                 = $object->order_blob;
			$this->invoice              = $object->order_invoice;
			$this->tracking             = $object->order_finis;
			$this->ok2go                = $object->order_ok2go;
			$this->locale_id            = $object->locale_id;
			$this->foreign_code         = $object->order_foreign_code;
		}

	}
	
	public function initWithAmazonOrderId($amazon_order_id)
	{
	    $object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".orders
					WHERE order_foreign_code = ?
				")->executeUncached("AMAZON-$amazon_order_id");
				
		while($object->next())
		{
			$this->id 				    = $object->order_id;
			$this->number 			    = $object->order_number;
			$this->customer_id 		    = $object->customer_id;
			$this->shipping_id 		    = $object->shipping_id;
			$this->status  			    = $object->order_status;
			$this->payment_method_id    = $object->payment_method_id;
			$this->shipping_address_id 	= $object->shipping_address_id;
			$this->billing_address_id 	= $object->billing_address_id;
			$this->konto_id             = $object->konto_id;
			$this->date                 = $object->order_date;
			$this->created              = $object->order_created;
			$this->paid                 = $object->order_paid;
			$this->updated              = $object->order_updated;
			$this->found                = $object->order_found;
			$this->step                 = $object->order_step;
			$this->print                = $object->order_print;
			$this->blob                 = $object->order_blob;
			$this->invoice              = $object->order_invoice;
			$this->tracking             = $object->order_finis;
			$this->ok2go                = $object->order_ok2go;
			$this->locale_id            = $object->locale_id;
		}
	}
	
	public function all()
	{
		$orders = array();
		
		$objects = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".orders
				")->execute();
		
		while($objects->next())
		{
			$order 				= new PnOrder();
			$order->id 			= $objects->order_id;
			$order->number 		= $objects->order_number;
			$order->customer_id 	= $objects->customer_id;
			$order->shipping_id 	= $objects->shipping_id;
			$order->status 		= $objects->status;
			$order->payment_method_id 	= $objects->payment_method_id;
			$this->shipping_address_id 	= $objects->shipping_address_id;
			$this->billing_address_id 	= $objects->billing_address_id;
			
			$orders[]			= $order;
		}
		
		return $orders;
	}
	
	public function getItems()
	{
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".items
	            WHERE order_id = ?
	        ")->execute($this->id);
	        
	    $items = array();
	    while($objDb->next())
	    {
	        $item = new PnItem();
	        $item->id                = $objDb->item_id;
	        $item->parent_id         = $objDb->parent_id;
	        $item->order_id          = $objDb->order_id;
	        $item->class_id          = $objDb->class_id;
	        $item->article_id        = $objDb->artikel_id;
	        $item->font_id           = $objDb->font_id;
	        $item->document_id       = $objDb->document_id;
	        $item->design_id         = $objDb->design_id;
	        $item->version_id        = $objDb->version_id;
	        $item->fields            = json_decode($objDb->item_fields);
	        $item->price_book        = $objDb->price_book;
	        $item->price_bind        = $objDb->price_bind;
	        $item->price_paper       = $objDb->price_paper;
	        $item->color             = $objDb->color;
	        $item->covercolor_id     = $objDb->covercolor_id;
	        $item->created           = $objDb->created;
	        $item->giftbox_id        = $objDb->giftbox_id;
	        $item->subtitle          = $objDb->item_subtitle;
	        $item->item_pieces       = $objDb->item_pieces;
	        
	        $items[]                 = $item;
	    }
	    
	    return $items;
	}
	
	public function getBookItem($bookId)
	{
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND buch_id = ?
					AND class_id = 2
				")->execute($this->id, $bookId);
	
		$item = new PnBookItem();
		while($object->next())
		{
			$item->id 			 = $object->item_id;
			$item->order_id 	 = $object->order_id;
			$item->book_id 		 = $object->buch_id;
			$item->item_fields 	 = $object->item_fields;
			$item->font_id 		 = $object->font_id;
			$item->article_id	 = $object->artikel_id;
			$item->covercolor_id = $object->covercolor_id;
			$item->class_id      = $object->class_id; 
		}
		
		return $item;
	}
	
	/*public function getBookItemTotalPrice()
	{
		$price = 0;
		foreach($this->getBookItems() as $bookItem)
		{
			$price += $bookItem->item_pieces * $bookItem->getPrice();
		}
		
		return $bookItem->getPrice();
	}*/
	
	public function getShippingOptions()
	{
	    $shipping = new PnAddress();
	    $shipping->initWithId($this->shipping_address_id);
	    $shipping_country_id = $shipping->country_id;
	    
	    $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND (
                        shipping_options.shipping_id IN (1, 2, 12) 
                        OR shipping_options.shipping_id = 11
                        OR shipping_options.shipping_id = 21
                        ) 
                    AND shipping_options.shipping_id != 2 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 11 
                    AND shipping_options.shipping_id != 12 
                    ORDER BY shipping_pos
                ";
        if($order->getTotalPriceWithoutCoupon("de", false) > $GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN'] && $shipping_country_id == 2){
        	$country2 = true;
        }     
        if($shipping_country_id == 1 || $country2)
        {
        	$checkCountry = $shipping_country_id != 2?"AND shipping_options.shipping_id != 11":"AND shipping_options.shipping_id != 12 AND shipping_options.shipping_id != 2 AND shipping_options.shipping_id != 21";
            $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 11
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 12 
                    ORDER BY shipping_pos
                ";
                
            if($this->getTotalPrice("de", false) > $GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN'])
            {
                $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 21 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    ".$checkCountry."
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 1 
                    ORDER BY shipping_pos
                ";
            }
            
            $flag = true;
            
            foreach($this->getTopLevelItems() as $item)
            {
                if($item->class_id != 14)
                    $flag = false;
            }
            
            if($flag)
            {
                $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 11
                    AND shipping_options.shipping_id != 4 
                    AND shipping_options.shipping_id != 12 
                    ORDER BY shipping_pos
                ";
            }
            if($this->getTotalPrice("de", false) > $GLOBALS['TL_CONFIG']['FREE_SHIPPING_MIN'] && $flag)
            {
                $query = "
                    SELECT * 
                    FROM " . $this->config->database . ".shipping_options 
                    WHERE shipping_options.shipping_active = 1 
                    AND shipping_options.shipping_id != 6 
                    AND shipping_options.shipping_id NOT IN (5, 6,7) 
                    AND shipping_options.shipping_id != 7 
                    AND shipping_options.shipping_id != 4 
                    ".$checkCountry."
                    AND shipping_options.shipping_id != 1 
                    ORDER BY shipping_pos
                ";
            }
        }
        
	    $objDb = $this->Database->prepare($query)->execute();
	    $options = array();
	    while($objDb->next())
	    {
	        $shipping 					= new PnShipping();
			$shipping->id 				= $objDb->shipping_id;
			$shipping->name 			= $objDb->shipping_name;
			$shipping->title 			= $objDb->shipping_title;
			$shipping->title_en 		= $objDb->shipping_title_en;
			$shipping->description 		= $objDb->shipping_description;
			$shipping->description_en 	= $objDb->shipping_description_en;
			
			$options[] = $shipping;
	    }
	    
	    return $options;
	}
	
	public function getShippingPrice()
	{
		$price = 0;
		$shipping = new PnShipping();
		$shipping->select($this->shipping_id);
		$price = $shipping->getPrice($this);
		return $price;
	}
	
	public function getBookItems($except_ebook = false)
	{
	    $query = "SELECT * FROM " . $this->config->database . ".items
                            WHERE order_id = ?
                            AND class_id in (2, 21)";
                            
	    if($except_ebook)
	        $query = "SELECT * FROM " . $this->config->database . ".items
                            WHERE order_id = ?
                            AND class_id = 2";
	        
		$bookItems = array();
		$objects = $this->Database->prepare($query)->executeUncached($this->id);
				
		while($objects->next())
		{
			$bookItem 				= new PnBookItem();
			$bookItem->id 			= $objects->item_id;
			$bookItem->order_id 	= $objects->order_id;
			$bookItem->item_fields 	= $objects->item_fields;
			$bookItem->font_id 		= $objects->font_id;
			$bookItem->article_id 	= $objects->artikel_id;
			$bookItem->item_pieces	= $objects->item_pieces;
			$bookItem->book_id		= $objects->buch_id;
			$bookItem->class_id     = $objects->class_id;
			$bookItem->item_paper   = $objects->item_paper;
			
			$bookItems[]			= $bookItem;
		}
		
		return $bookItems;
	}
	
	public function getEbookItems()
	{
	    $query = "SELECT * FROM " . $this->config->database . ".items
                            WHERE order_id = ?
                            AND class_id in (21)";
                            
        $objects = $this->Database->prepare($query)->executeUncached($this->id);
        
        $ebooks = array();
        
        while($objects->next())
		{
			$bookItem 				= new PnBookItem();
			$bookItem->id 			= $objects->item_id;
			$bookItem->order_id 	= $objects->order_id;
			$bookItem->item_fields 	= $objects->item_fields;
			$bookItem->font_id 		= $objects->font_id;
			$bookItem->article_id 	= $objects->artikel_id;
			$bookItem->item_pieces	= $objects->item_pieces;
			$bookItem->book_id		= $objects->buch_id;
			$bookItem->class_id     = $objects->class_id;
			
			$ebooks[]			    = $bookItem;
		}
		
		return $ebooks;
	}
	
	/*public function getWidmungItem($parent_id)
	{
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND parent_id = ?
					AND class_id = 10
				")->execute($this->id, $parent_id);
		
		$item = new PnWidmungItem();
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->order_id = $object->order_id;
			$item->book_id = $object->buch_id;
			$item->item_fields = $object->item_fields;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}*/
	
	/*public function getDesignItem($parent_id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items
				WHERE order_id = ?
				AND parent_id = ?
				AND class_id = 3
				")->execute($this->id, $parent_id);
		
		$item = new PnItem();
		while($object->next())
		{
			$item->id 			= $object->item_id;
			$item->order_id 	= $object->order_id;
			$item->book_id 		= $object->buch_id;
			$item->item_fields 	= $object->item_fields;
			$item->article_id	= $object->artikel_id;
			$item->subtitle		= $object->item_subtitle;
			$item->item_image1_present = $object->item_image1_present;
			$item->item_image1_droped  = $object->item_image1_droped;
		}
		
		return $item;
	}*/
	
	public function getNumOfBookItems()
	{
		$num = 0;
		foreach($this->getBookItems() as $bookItem)
		{
			$num += $bookItem->item_pieces;
		}
		
		return $num;
	}
	
	public function getNumOfWineItems()
	{
	    $num = 0;
		foreach($this->getWineItems() as $item)
		{
			$num += $item->item_pieces;
		}
		
		return $num;
	}
	

	public function getNumOfAddonItems()
	{
		$objDb = $this->Database->prepare("
				SELECT *, i.parent_id as parent_id FROM " . $this->config->database . ".items i
				LEFT JOIN " . $this->config->database . ".artikel_klassen ak
				ON i.class_id = ak.class_id
				WHERE ak.class_is_addon = 1
				AND i.order_id = ?
			")->executeUncached($this->id);
		
        $items = array();
		while($objDb->next())
		{
		    $item = new PnItem();
		    $item->id           = $objDb->item_id;
		    $item->order_id     = $objDb->order_id;
		    $item->class_id     = $objDb->class_id;
		    $item->item_pieces  = $objDb->item_pieces;
		    $item->article_id   = $objDb->artikel_id;
		    $item->parent_id    = $objDb->parent_id;
		    
		    $items[] = $item;
		}
		
		$num_of_addon = 0;
		foreach($items as $item)
		{
		    $num_of_addon += $item->fixItemPieces();
		}
		
		return $num_of_addon;
	}
	
	public function checkBlackFriday(){
		if ($GLOBALS['TL_CONFIG']['black_friday']){
				//List items in array
				    $array_list_35 = $GLOBALS['TL_CONFIG']['list_book_black_friday'];
					$listItems = $this->getBookItems();
					if(count($listItems)>0){
						foreach($listItems as $items_new){
							if(in_array($items_new->book_id,$array_list_35)){
								return true;
							}
						}
					}
			}
		 return false;
	}
	
	public function getTotalPrice($locale="de", $withShipping=true)
	{
		$totalPrice = 0;
		foreach($this->getItems() as $item)
		{
		    
		    $totalPrice += $item->fixItemPieces() * $item->getPrice() ;
		}
		if($withShipping)
		    $totalPrice += $this->getShippingPrice();
		
		$objDb = $this->Database->prepare("
		        SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
		        WHERE order_id = ?
		    ")->execute($this->id); 
		    
		if($objDb->numRows > 0)
		{
		    while($objDb->next())
		    {
		        $coupon_id = $objDb->coupon_id;
		    }
		    if($GLOBALS['TL_CONFIG']['Counpon_Free'] || $this->checkBlackFriday())
		    {
			    if($this->hasCouponNotGiftboxFree() && $this->getNumOfBookItems() > 0)
			    {
				$totalPrice -= $this->coupon_prices_de;
			    }
		    }
		    
		    $coupon = new PnCoupon();
		    $coupon->initWithId($coupon_id);
		    
		    $couponValue = $coupon->wert;
		    if($coupon->coupontype_id == 3){
		    	$couponValue = $coupon->wert + $coupon->wert2;
	    	}
		    if($coupon->prozent > 0)
		    {
		        $couponValue = round($this->getTotalPriceWithoutCoupon("de", true) * min(100, max(0, $coupon->prozent)) / 100, 2);          
		    }
		   
		    if($couponValue > $totalPrice)
		    {
		        $couponValue = $totalPrice;
		    }
		    
		    /* Su dung tam thoi cho rabatt PN50RABATT */
		    if($coupon->token == "PN50RABATT")
		    {
		        $couponValue = round($this->getSecondBookItemPrice() * 0.5, 2);
		    }
		    
		    $totalPrice -= $couponValue;
		}
		
		return $totalPrice;
	}
	public function hasGiftbox()
	{
		$Items = 0;
		$objects = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND class_id = 4
					AND artikel_id <> ?
				")->executeUncached($this->id,$this->coupon_item_id);
				
		while($objects->next())
		{
			$Items++;
		}

		if($Items > 0) 
			return true;

		return false;
	}
	public function hasCouponNotGiftboxFree()
	{
		$Items = 0;
		$objects = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND class_id = 4
					AND artikel_id = ?
				")->executeUncached($this->id,$this->coupon_item_id);
				
		while($objects->next())
		{
			$Items++;
		}

		if($Items > 0) 
			return false;

		return true;
	}

	public function getTotalPriceWithoutCoupon($locale="de", $withShipping=true)
	{
		$totalPrice = 0;
		
	    foreach($this->getItems() as $item)
		{
		    $totalPrice += $item->fixItemPieces() * $item->getPrice();
		}
	
		if($withShipping)
		    $totalPrice += $this->getShippingPrice();
	    if($GLOBALS['TL_CONFIG']['Counpon_Free'] || $this->checkBlackFriday()){
		if($this->hasCoupon())
		{
			if($this->hasCouponNotGiftboxFree() && $this->getNumOfBookItems() > 0)
			{
					//$totalPrice -= $this->coupon_prices_de;
			}
		}
	    }
		
		return $totalPrice;
	}
	
	public function add($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = mysql_real_escape_string($v);
		}
		
		$object = $this->Database->prepare("
					INSERT INTO " . $this->config->database . ".orders (". implode(", ", $key) .") VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $object->insertId;
	}
	
	public function edit($params = array())
	{
	    $set = array();
		foreach ($params as $k => $v)
		{
			$set[] = $k . "  =  '" . mysql_real_escape_string($v) . "'";
		}
		
		$object = $this->Database->prepare("UPDATE " . $this->config->database . ".orders SET " . implode(", ", $set) . " WHERE order_id = ?" )->executeUncached($this->id);
		
		return $object->affectedRows;
	}
	
	public function update($params = array())
	{
	    $this->edit($params);
	}
	
	public function exist($id)
	{
		return false;
	}
	
	public function getWineItems()
	{
		$wineItems = array();
		$objDb = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND class_id = 14
				")->executeUncached($this->id);
				
		while($objDb->next())
		{
			$wineItem				= new PnWineItem();
			$wineItem->id			= $objDb->item_id;
			$wineItem->order_id		= $objDb->order_id;
			$wineItem->article_id	= $objDb->artikel_id;
			$wineItem->item_pieces	= $objDb->item_pieces;
			$wineItem->item_fields  = $objDb->item_fields;
			$wineItem->etiquette_id = $objDb->etiquette_id;
			$wineItem->item_paper   = $objDb->item_paper;
			
			$wineItems[]			= $wineItem;
		}
		
		return $wineItems;
	}
	public function getWineItemsNomal()
	{
		$wineItems = array();
		$objDb = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE order_id = ?
					AND class_id = 14
				")->executeUncached($this->id);
		while($objDb->next())
		{
			$wineItem 		= array(); 
			$wineItem['article_id']	= $objDb->artikel_id;
			$wineItem['item_pieces']= $objDb->item_pieces;
	
			$wineItems[]		= $wineItem;
		}
		
		return $wineItems;
	}
	public function getTopLevelItems()
	{
	    $items = array();
	    $objDb = $this->Database->prepare("
	        SELECT * FROM " . $this->config->database . ".items
	        WHERE order_id = ?
	        AND parent_id is NULL
        ")->executeUncached($this->id);
        
        while($objDb->next())
		{
		    $item                = new PnItem();
		    $item->id            = $objDb->item_id;
            $item->order_id      = $objDb->order_id;
            $item->book_id       = $objDb->buch_id;
            $item->class_id      = $objDb->class_id;
            $item->article_id    = $objDb->artikel_id;
            $item->font_id       = $objDb->font_id;
            $item->document_id   = $objDb->document_id;
            $item->design_id     = $objDb->design_id;
            $item->version_id    = $objDb->version_id;
            $item->fields        = $objDb->item_fields;
            $item->price_book    = $objDb->price_book;
            $item->price_bind    = $objDb->price_bind;
            $item->price_paper   = $objDb->price_paper;
            $item->color         = $objDb->item_color;
            $item->covercolor_id = $objDb->covercolor_id;
            $item->created       = $objDb->item_created;
            $item->giftbox_id    = $objDb->giftbox_id;
            $item->subtitle      = $objDb->item_subtitle;
            $item->item_pieces   = $objDb->item_pieces;
            
            $items[]             = $item;
		}
		
		return $items;
	}
	
	public function createKonto()
	{
	    $konto_name = "";
	    if($this->number)
	    {
	        $konto_name = "Bestellung " . $this->id;
	    }
	    else
	    {
	        $konto_name = "Warenkorb " . $this->id;
	    }
	    
	    $konto  = new PnKonto();
	    $id     = $konto->add(
                        array(
                                "konto_name"    => $konto_name,
                                "currency_id"   => 1,
                                "konto_typ"     => 3
                            )
	                );
	    
	    $konto->initWithId($id);
	    
	    return $konto;
	}
	
	public function createRandomKey()
	{
	    $key = $this->config->random_string(32);
	    
	    $objDb = $this->Database->prepare("
	            INSERT INTO " . $this->config->database . ".random_keys(`random_key_text`, `order_id`) 
	            VALUES (? , ?)
	        ")->execute($key, $this->id);
	        
	    return $objDb->InsertId;
	}
	
	public function recalc()
	{
	    $postens = $this->getPosten();
	    foreach($postens as $posten)
	    {
	        $posten->remove();
	    }

	    $items = $this->getItems();
	    foreach($items as $item)
	    {
	        $item->recalc($this->konto_id);
	    }
	    
	    /*
	     * Reset shipping option
	     */
	    $shipOptions = array();
        $shippingOption = new PnShipping();
        $shippingAddr = new PnAddress();
        $shippingAddr->initWithId($this->shipping_address_id);
        foreach($shippingOption->get($this, $shippingAddr->country_id) as $shipOpt)
        {
            $shipOptions[] = $shipOpt->id;
        }
        
        if(!in_array($this->shipping_id, $shipOptions))
        {
            $this->update(array("shipping_id" => $shipOptions[0]));
            $this->shipping_id = $shipOptions[0];
        }
        
        $num_of_books = count($this->getBookItems(true));
        $num_of_wines = count($this->getWineItems());
        
        if($num_of_books == 0 && $num_of_wines == 0)
        {
            $this->update(array("shipping_id" => 11));
            $this->shipping_id = 11;
        }
	    
	    $shipping = new PnShipping();
	    $shipping->initWithId($this->shipping_id);
	    $shipping->recalc($this);
	    
	    //reset coupon giftbox
	    $this->Database->prepare("
                DELETE FROM " . $this->config->database .".finanz_zahlungen
                WHERE order_id = ?
                AND zahlung_text LIKE 'Coupon: giftbox free'
            ")->execute($this->id);
            
        $this->Database->prepare("
                DELETE FROM " . $this->config->database .".finanz_posten
                WHERE order_id = ?
                AND artikel_id = ?
            ")->execute($this->id, $this->coupon_item_id);
                
	    $coupon = new PnCoupon();
        $objCoupon = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
                WHERE order_id = ?
            ")->execute($this->id);
        while($objCoupon->next())
        {
            $coupon_id = $objCoupon->coupon_id;
        }
        
        if($coupon_id > 0)
        {
            $coupon->initWithId($coupon_id);
            $coupon->recalc($this);
        }
	}
	
	public function cleanPostenAndZahlung($exist_postens)
	{
	    $objDb = $this->Database->prepare("
	        SELECT *
	        FROM " . $this->config->database . ".finanz_posten
	        WHERE order_id = ?
	        AND NOT isnull(item_id)
	    ")->execute($this->id);
	    
	    $postens = array();
	    while($objDb->next())
	    {
	        $posten = new PnPosten();
	        $posten->id            = $objDb->posten_id;
            $posten->steuerart_id  = $objDb->steuerart_id;
            $posten->zielkonto     = $objDb->posten_zielkonto;
            $posten->konto_id      = $objDb->konto_id;
            $posten->artikel_id    = $objDb->artikel_id;
            $posten->anzahl        = $objDb->posten_anzahl;
            $posten->einzelbetrag  = $objDb->posten_einzelbetrag;
            $posten->netto         = $objDb->posten_netto;
            $posten->text          = $objDb->posten_text;
            $posten->description   = $objDb->posten_description;
            $posten->datum         = $objDb->posten_datum;
            $posten->manuell       = $objDb->posten_manuell;
            $posten->created       = $objDb->posten_created;
            $posten->updated       = $objDb->posten_updated;
            $posten->acc_id        = $objDb->acc_id;
            $posten->item_id       = $objDb->item_id;
            $posten->order_id      = $objDb->order_id;
            $posten->rechnung_id   = $objDb->rechnung_id;
            $posten->coupon_id     = $objDb->coupon_id;
            $posten->culprit_id    = $objDb->culprit_id;
            $posten->ust_id        = $objDb->ust_id;
            $posten->currency_id   = $objDb->currency_id;
            $posten->steuersatz_id = $objDb->steuersatz_id;
	    }
	    
	    foreach($postens as $posten)
	    {
	        if(!in_array($posten->id, $exist_postens))
	        {
	            $posten->remove();
	        }
	    }
	}
	
	public function getPosten()
	{
	    $objDb = $this->Database->prepare("
                SELECT * FROM " . $this->config->database . ".finanz_posten
                WHERE order_id = ?
            ")->execute($this->id);
            
        $postens = array();
        while($objDb->next())
        {
            $posten                = new PnPosten();
            $posten->id            = $objDb->posten_id;
            $posten->steuerart_id  = $objDb->steuerart_id;
            $posten->zielkonto     = $objDb->posten_zielkonto;
            $posten->konto_id      = $objDb->konto_id;
            $posten->artikel_id    = $objDb->artikel_id;
            $posten->anzahl        = $objDb->posten_anzahl;
            $posten->einzelbetrag  = $objDb->posten_einzelbetrag;
            $posten->netto         = $objDb->posten_netto;
            $posten->text          = $objDb->posten_text;
            $posten->description   = $objDb->posten_description;
            $posten->datum         = $objDb->posten_datum;
            $posten->manuell       = $objDb->posten_manuell;
            $posten->created       = $objDb->posten_created;
            $posten->updated       = $objDb->posten_updated;
            $posten->acc_id        = $objDb->acc_id;
            $posten->item_id       = $objDb->item_id;
            $posten->order_id      = $objDb->order_id;
            $posten->rechnung_id   = $objDb->rechnung_id;
            $posten->coupon_id     = $objDb->coupon_id;
            $posten->culprit_id    = $objDb->culprit_id;
            $posten->ust_id        = $objDb->ust_id;
            $posten->currency_id   = $objDb->currency_id;
            $posten->steuersatz_id = $objDb->steuersatz_id;
            
            $postens[]             = $posten;
        }
        
        return $postens;
	}
	
	public function pay($price)
	{
	    if($this->paid)
	    {
	        $this->edit(array("order_blob" => $this->blob . date('Y-m-d', time()) . ": Paid already!\n"));
	    }
	    
	    $this->edit(array("order_paid" => date('Y/m/d H:i:s a', time())));
	    
	    $zahlung_text = "Adyen: online payment";
	    if($this->payment_method_id == 12)
	    {
	        $zahlung_text = "Amazon Payment: online payment";
	    }
	    
	    //Calculate Saldo
	    $zahlungen = new PnZahlungen();
	    $zahlung_parent_id = $zahlungen->add(
                        array(
                            "steuerart_id" => 3,
                            "journal_id" => 1,
                            "stapel_id" => 3,
                            //"protokoll_id" => null,
                            //"parent_id" => null,
                            //"posten_id" => null,
                            "order_id" => $this->id,
                            "ust_id" => 1,
                            "konto_id" => 15526, //DUMMY KONTO
                            //"coupon_id" => null,
                            "zahlung_anzahl" => 1,
                            "zahlung_typ" => 12,
                            "zahlung_andereskonto" => 0,
                            "zahlung_stapel" => 1,
                            "zahlung_betrag" => -1 * floatVal($price),
                            "zahlung_netto" => 0,
                            //"zahlung_name" => null,
                            "zahlung_text" => $zahlung_text,
                            "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                            "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                            "zahlung_created" => date('Y/m/d H:i:s a', time()),
                            "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                            "currency_id" => 1,
                            "zahlung_steuersatz" => 0,
                            //"zahlung_total_netto" => null,
                            //"zahlung_total_brutto" => null,
                            "steuersatz_id" => 4,
                            //"zahlung_otherorder" => null,
                            )
                    );
        
	    $zahlung_child_id = $zahlungen->add(
                        array(
                            "steuerart_id" => 3,
                            "journal_id" => 1,
                            "stapel_id" => 3,
                            //"protokoll_id" => null,
                            "parent_id" => $zahlung_parent_id,
                            //"posten_id" => null,
                            "order_id" => $this->id,
                            "ust_id" => 1,
                            "konto_id" => $this->konto_id,
                            //"coupon_id" => null,
                            "zahlung_anzahl" => 1,
                            "zahlung_typ" => 12,
                            "zahlung_andereskonto" => 0,
                            "zahlung_stapel" => 1,
                            "zahlung_betrag" => $price,
                            "zahlung_netto" => 0,
                            //"zahlung_name" => null,
                            "zahlung_text" => $zahlung_text,
                            "zahlung_datum" => date('Y/m/d H:i:s a', time()),
                            "zahlung_valuta" => date('Y/m/d H:i:s a', time()),
                            "zahlung_created" => date('Y/m/d H:i:s a', time()),
                            "zahlung_updated" => date('Y/m/d H:i:s a', time()),
                            "currency_id" => 1,
                            "zahlung_steuersatz" => 0,
                            //"zahlung_total_netto" => null,
                            //"zahlung_total_brutto" => null,
                            "steuersatz_id" => 4,
                            //"zahlung_otherorder" => null,
                            )
                    );        
	}
	
	public function createOrderNumber()
	{
	    $orderNumber = 0;
	    
	    $objDb = $this->Database->prepare("
	            SELECT * FROM " . $this->config->database . ".order_numbers
	        ")->execute();
	        
	    while($objDb->next())
	    {
	        $orderNumber = $objDb->next_order_number;
	    }
	    
	    $retry = 5;
	    while($retry > 0)
	    {
	        $this->Database->prepare("
                        UPDATE " . $this->config->database . ".order_numbers
                        SET next_order_number = ?
                        WHERE id = 1
                    ")->execute($orderNumber + 1);
                    
            try
            {
                $this->edit(array("order_number" => $orderNumber));
                $retry = 0;
            }
            catch(Exception $e)
            {
                $retry -= 1;
            }
	    }
	    
	    return $orderNumber;
	}
	
	public function getTaxPrice($locale="de", $withShipping=true)
	{
	    $tax = 0;
	    foreach($this->getItems() as $item)
		{
		    if(in_array($item->class_id, array(14, 15, 16, 18)))
		        $tax += $item->fixItemPieces() * $item->getPrice() / 119 * 19;
		    else
		        $tax += $item->fixItemPieces() * $item->getPrice() / 107 * 7;
		    
		}
		
		if($withShipping)
		    $tax     += $this->getShippingPrice() / 107 * 7;
		
		return $tax;
	}
	
	public function getTotalNetto()
	{
	    $postens    = $this->getPosten();
	    $tax        = new Steuersatz();
	    $totalNetto = 0;
	    foreach($postens as $posten)
	    {
	        $tax->initWithId($posten->steuersatz_id);
	        $rate        = $tax->rate;
	        $totalNetto += $posten->anzahl * $posten->einzelbetrag / (100 + $rate) * 100;
	    }
	    
	    return $totalNetto;
	}
	
	public function getShippingNetto()
	{
	    $postens    = $this->getPosten();
	    $tax        = new Steuersatz();
	    $shippingNetto = 0;
	    foreach($postens as $posten)
	    {
	        $article = new PnArticle();
	        $article->initWithId($posten->artikel_id);
	        if($article->class_id == 7)
	        {
	            $tax->initWithId($posten->steuersatz_id);
	            $rate          = $tax->rate;
	            $shippingNetto = $posten->anzahl * $posten->einzelbetrag / (100 + $rate) * 100;
	            break;
	        }
	    }
	    
	    return $shippingNetto;
	}
	
	public function getTrackingUrl()
	{
	    $topLvItems = $this->getTopLevelItems();
	    foreach($topLvItems as $item)
	    {
	        $objDb = $this->Database->prepare("
                    SELECT shipment_id FROM " . $this->config->database . ".shipment_item
                    WHERE item_id = ?
	            ")->execute($item->id);
	            
            while($objDb->next())
            {
                $objTracking = $this->Database->prepare("
                        SELECT * FROM " . $this->config->database . ".shipments
                        WHERE shipment_id = ?
                    ")->execute($objDb->shipment_id);
                    
                while($objTracking->next())
                {
                    if($objTracking->shipment_tracking != "")
                    {
                        return $objTracking->shipment_tracking;
                    }
                }
            }
	    }
	    
	    return '';
	}
	
	public function hasCoupon()
	{
	    $objDb = $this->Database->prepare("
		        SELECT * FROM " . $this->config->database . ".coupons_used_on_orders
		        WHERE order_id = ?
		    ")->executeUncached($this->id);
		    
		if($objDb->numRows > 0)
		    return true;
		
	    return false;
	}
	
	public function getFreeCoupon()
	{
	    $objDb = $this->Database->prepare("
		        SELECT coupon_id, coupon_token FROM " . $this->config->database . ".coupons
		        WHERE barcode_id = ?
		    ")->executeUncached($this->id);
		    
        if($objDb->numRows > 0)
        {
            while($objDb->next())
            {
                return $objDb->coupon_token;
            }
        }
        return "";
	}
	
	public function addMessageCheckSendMail(){
		$oldBlob = $this->blob ? $this->blob."\r\n" :''; 
		$param = array('order_blob'=>$oldBlob.date('Y-m-d :', time())."Fregmail Bestellbestätigung gesendet.\r\n");
		$this->edit($param);
	}
	
	/* Su dung tam thoi cho rabatt PN50RABATT */
	public function getSecondBookItemPrice()
	{
	    $book_items = $this->getBookItems();
	    
	    if(count($book_items) > 1)
	    {
	        $second_item = $book_items[1];
	    }
	    elseif($this->getNumOfBookItems() > 1)
	    {
	        $second_item = $book_items[0];
	    }
	    else
	    {
	        return 0;
	    }
	    
	    $second_item_price = $second_item->getPrice();
	    
	    return $second_item_price;
	}
	
	public function getAmazonOrderId()
	{
	    return str_replace('AMAZON-', '', $this->foreign_code);
	}
}

?>
