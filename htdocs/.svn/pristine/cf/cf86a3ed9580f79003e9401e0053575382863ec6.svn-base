<?php if (!defined('TL_ROOT')) die('You cannot access this file directly!');

class PnItem extends Controller
{
	public $id;
	public $order_id;
	public $book_id;
	public $class_id;
	public $article_id;
	public $font_id;
	public $document_id;
	public $design_id;
	public $version_id;
	public $fields = array();
	public $price_book;
	public $price_bind;
	public $price_paper;
	public $color;
	public $covercolor_id;
	public $created;
	public $giftbox_id;
	public $subtitle;
	public $item_pieces;
	public $config;
	
	
	function __construct()
	{
		parent::__construct();
		$this->import('Database');
		$this->config = new PnConfig();
	}
	
	public function select($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 			= $object->item_id;
			$this->order_id 		= $object->order_id;
			$this->book_id 		= $object->book_id;
			$this->article_id 	= $object->artikel_id;
			$this->fields 		= $object->item_fields;
			$this->item_pieces 	= $object->item_pieces;
		}
	}
	
	public function initWithId($id)
	{
		$object = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($id);
			
		while($object->next())
		{
			$this->id 			= $object->item_id;
			$this->order_id 		= $object->order_id;
			$this->book_id 		= $object->book_id;
			$this->article_id 	= $object->artikel_id;
			$this->fields 		= $object->item_fields;
			$this->item_pieces 	= $object->item_pieces;
			$this->etiquette_id = $object->etiquette_id;
		}
	}

	public function add($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = $v;
		}
		
		$object = $this->Database->prepare("
				INSERT INTO " . $this->config->database . ".items (" . implode(", ", $key) .") VALUES('" . implode("', '", $value) . "')
				")->execute();
		
		return $object->insertId;
	}
	
	public function edit($param = array())
	{
		$set = array();
		foreach ($param as $k => $v)
		{
			$set[] = $k . "  =  '" . mysql_real_escape_string($v) . "'";
		}
		
		$object = $this->Database->prepare("UPDATE " . $this->config->database . ".items SET " . implode(", ", $set) . " WHERE item_id = ?" )->execute($this->id);
		
		return $object->affectedRows;
	}
	
	public function remove($params = array())
	{
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($this->id);
	}
	
	public function getPrice($locale="de")
	{
		$object = $this->Database->prepare("
				SELECT price_amount, price_amount_uk FROM " . $this->config->database . ".prices
				WHERE artikel_id = ?  
				AND (isnull(price_validuntil) OR price_validuntil > now())
			")->executeUncached($this->article_id);
			
		while($object->next())
		{
			if($locale == "de")
				return $object->price_amount;
			return $object->price_amount_uk;
		}
		
		return 0;
	}
	
	public function getName()
	{
		$object = $this->Database->prepare("
					SELECT artikel_name FROM " . $this->config->database . ".finanz_artikel
					WHERE artikel_id = ?
				")->execute($this->article_id);
				
		while($object->next())
		{
			return $object->artikel_name;
		}
		
		return "";
	}
	
}

class PnBookItem extends PnItem
{
	public function getWidmungItem()
	{
		$item = new PnItem();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 10
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->item_fields = $object->item_fields;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getName()
	{
		$object = $this->Database->prepare("
					SELECT artikel_name FROM " . $this->config->database . ".finanz_artikel
					WHERE artikel_id = ?
				")->execute($this->article_id);
				
		while($object->next())
		{
			return $object->artikel_name;
		}
		
		return "";
	}
	
	public function getTextvorschau()
	{
		
	}
	
	public function getFontName($locate="de")
	{
		$object = $this->Database->prepare("
				SELECT t.font_title, t.font_title_en FROM " . $this->config->database . ".items i
				LEFT JOIN " . $this->config->database . ".tex_fonts t
				ON i.font_id = t.font_id
				WHERE i.item_id = ?
			")->execute($this->id);
			
		while($object->next())
		{
			if($locate=="de")
				return $object->font_title;
			return $object->font_title_en;
		}
		
		return "";
	}
	
	public function getBookEntities()
	{
		$arrEntities 	= array();
		$arrFields	 	= array();
		
		$entities = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".buch_entities be
				LEFT JOIN " . $this->config->database . ".buch_fields bf
				ON be.entity_id = bf.entity_id
				WHERE be.variant_id = 
				(
				SELECT bb.variant_id
				FROM " . $this->config->database . ".buch_buchvariant bb
				INNER JOIN " . $this->config->database . ".buch_variant bv
				ON bb.variant_id = bv.variant_id
				WHERE bb.buch_id = ?
				AND bv.variant_public = 1
				)
				ORDER BY bf.field_position
			")->execute($this->book_id);
		
		$item_fields = $this->Database->prepare("
				SELECT item_fields FROM " . $this->config->database . ".items
				WHERE item_id = ?
			")->execute($this->id);
			
		$fields = array();
		while($item_fields->next())
		{
			$fields = json_decode($item_fields->item_fields);
		}
		
		while($entities->next())
		{
			$arrEntities[$entities->entity_id]['name'] = $entities->entity_name;
			$arrFields[] = array(
					"entity_id"			=> $entities->entity_id,
					"field_name"			=> $entities->field_name,
					"field_description"	=> $entities->field_description,
					"field_default"		=> $entities->field_default,
					"field_count"		=> $entities->field_count
			);
		}
		
		foreach($fields as $k => $v)
		{
			foreach($arrFields as $k1 => $v1)
			{
				if($v1['field_name'] == $k)
					$arrFields[$k1]['field_name'] = $v;
			}
		}
		
		foreach($arrEntities as $k => $v)
		{
			foreach($arrFields as $field)
			{
				if($field['entity_id'] == $k)
				{
					$arrEntities[$k]['fields'][] = $field;
				}
			}
		}
		
		return $arrEntities;
	}
	
	public function getDesignItem()
	{
		$item = new PnItem();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 3
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->item_fields = $object->item_fields;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getAddonItems()
	{
		$items = array();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items i
					LEFT JOIN " . $this->config->database . ".artikel_klassen ak
					ON i.class_id = ak.class_id
					WHERE ak.class_is_addon = 1
					AND ak.parent_id = 2
					AND i.parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item 				= new PnItem();
			$item->id 			= $object->item_id;
			$item->item_fields 	= $object->item_fields;
			$item->article_id 	= $object->artikel_id;
			$item->class_id		= $object->class_id;
			$item->item_pieces	= $object->item_pieces;
			$item->pieces_from_parent = $object->class_pieces_from_parent;
			$items[] 			= $item;
		}
		
		return $items;
	}
	
	public function getVersionName($locate="de")
	{
		$object = $this->Database->prepare("
				SELECT v.version_name, v.version_name_en FROM " . $this->config->database . ".finanz_artikel fa
				LEFT JOIN " . $this->config->database . ".versions v
				ON fa.version_id = v.version_id
				WHERE fa.artikel_id = ?
			")->execute($this->article_id);
			
		while($object->next())
		{
			if($locate=="de")
				return $object->version_name;
			return $object->version_name_en;
		}
		
		return "";
	}
	
	public function getBookVersion()
	{
		$object = $this->Database->prepare("
				SELECT v.version_id, v.version_name, v.version_name_en FROM " . $this->config->database . ".finanz_artikel fa
				LEFT JOIN " . $this->config->database . ".versions v
				ON fa.version_id = v.version_id
				WHERE fa.artikel_id = ?
			")->execute($this->article_id);
		
		$version = new PnVersion();
		while($object->next())
		{
			$version->id = $object->version_id;
			$version->name = $object->version_name;
			$version->name_en = $object->version_name_en;
		}
		
		return $version;
	}
	
	public function getCoverImage($arrParam = array())
	{
		$url = sprintf($this->config->image_root . "/preview/BookArtikel/%s/%s.png",
								$this->article_id, md5($this->config->site_secret . "BookArtikel" . $this->article_id));
		if(count($arrParam) > 0)
		{
			$qstr = array();
			foreach ($arrParam as $k => $v)
			{
				$qstr[] = "$k=$v";
			}
			
			$url .= "?" . implode('&', $qstr);
		}
		
		return $url;
		
	}
	
	public function getDecocover()
	{
		$cache_decocover = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items_decocovers
				WHERE item_id = ?
			")->execute($this->id);
		
		while($cache_decocover->next())
			return $cache_decocover->decocover_id;
		
		return 0;
	}
	
	public function getTotalPrice()
	{
		$priceBook 		= $this->getPrice();
		$widmungItem 	= $this->getWidmungItem();
		$priceWidmung	= $widmungItem->getPrice();
		$designItem 	= $this->getDesignItem();
		$priceDesign	= $designItem->getPrice();
		
		$priceAddons = 0;
		$addonItems = $this->getAddonItems();
		foreach($addonItems as $item)
		{
			if($item->pieces_from_parent > 0)
			{
				$priceAddons += $this->item_pieces * $item->getPrice();
			}
			else
			{
				$priceAddons += $item->item_pieces * $item->getPrice();
			}
		}
		
		return $this->item_pieces * ($priceBook + $priceWidmung + $priceDesign) + $priceAddons;
	}
	
	public function addDecocover($params = array())
	{
		$key = array();
		$value = array();
		
		foreach ($params as $k => $v)
		{
			$key[] = $k;
			$value[] = $v;
		}
		
		$cache_decocover = $this->Database->prepare("
				SELECT * FROM " . $this->config->database . ".items_decocovers
				WHERE item_id = ?
			")->execute($this->id);
			
		if($cache_decocover->numRows > 0)
			$this->Database->prepare("
				UPDATE " . $this->config->database . ".items_decocovers
				SET decocover_id = ?
				WHERE item_id = ?
				")->execute($params['decocover_id'], $params['item_id']);
		else
			$this->Database->prepare("
				INSERT INTO " . $this->config->database . ".items_decocovers (" . implode(", ", $key) .") 
				VALUES('" . implode("', '", $value) . "')
				")->execute();
	}
	
	public function removeChildItem()
	{
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items_decocovers
				WHERE item_id = ?
			")->execute($this->id);
			
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items
				WHERE parent_id = ?
			")->execute($this->id);
			
		return $object->affectedRows;
	}
	
	public function removeChildItems($param=array())
	{
		$object = $this->Database->prepare("
				DELETE FROM " . $this->config->database . ".items
				WHERE artikel_id IN ('" . implode("', '", $param) . "')
				AND parent_id = ?
			")->execute($this->id);
		
		return $object->affectedRows;
	}
}

class PnWidmungItem extends PnItem
{
	public function getText()
	{
		$fields = json_decode($this->item_fields);
		return $fields->text;
	}
	
	public function getFontFace()
	{
		$fields = json_decode($this->item_fields);
		return $fields->fontface;
	}
	
	public function getFontColor()
	{
		$fields = json_decode($this->item_fields);
		return $fields->fontcolor;
	}
}

class PnWineItem extends PnItem
{
	public function getDesignItem()
	{
		$item = new PnItem();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 18
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item->id = $object->item_id;
			$item->article_id = $object->artikel_id;
		}
		
		return $item;
	}
	
	public function getAddonItems()
	{
		$items = array();
		$object = $this->Database->prepare("
					SELECT * FROM " . $this->config->database . ".items
					WHERE class_id = 15
					AND parent_id = ?
				")->execute($this->id);
		
		while($object->next())
		{
			$item 				= new PnItem();
			$item->id 			= $object->item_id;
			$item->article_id 	= $object->artikel_id;
			$item->class_id		= $object->class_id;
			$item->item_pieces	= $object->item_pieces;
			$items[] 			= $item;
		}
		
		return $items;
	}
	
	public function getName()
	{
		$object = $this->Database->prepare("
					SELECT artikel_name FROM " . $this->config->database . ".finanz_artikel
					WHERE artikel_id = ?
				")->execute($this->article_id);
				
		while($object->next())
		{
			return $object->artikel_name;
		}
		
		return "";
	}
}

?>